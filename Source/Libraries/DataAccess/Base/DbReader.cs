﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;

namespace DataAccess.Base
{
    public class DbReader
    {
        #region Properties

        private DbDataReader _r;

        public DbDataReader R { get { return _r; } set { _r = value; } }

        public bool IsValid { get; set; }

        #endregion

        #region Methods

        // Constructors

        public DbReader(string procedureName)
        {
            R = DbFactory.ExecuteReader(procedureName);
            SetReaderValidity();
        }

        public DbReader(string procedureName, Dictionary<string, object> parameters)
        {
            R = DbFactory.ExecuteReader(procedureName, parameters);
            SetReaderValidity();
        }

        public DbReader(string procedureName, DbConnection connection)
        {

            R = DbFactory.ExecuteReader(procedureName, connection);
            SetReaderValidity();
        }                

        public DbReader(string procedureName, DbConnection connection, Dictionary<string, object> parameters)
        {
            R = DbFactory.ExecuteReader(procedureName, connection, parameters);
            SetReaderValidity();
        }

        public DbReader(string procedureName, DbConnection connection, DbTransaction transaction)
        {
            R = DbFactory.ExecuteReader(procedureName, connection, transaction);
            SetReaderValidity();
        }

        public DbReader(string procedureName, DbConnection connection, DbTransaction transaction, Dictionary<string, object> parameters)
        {
            R = DbFactory.ExecuteReader(procedureName, connection, transaction, parameters);
            SetReaderValidity();
        }


        // Standart Reader logic

        protected void SetReaderValidity()
        {
            IsValid = (_r != null && _r.HasRows);
        }

        public bool Read()
        {
            return (IsValid && _r.Read());
        }

        public bool NextResult()
        {
            return (IsValid && _r.NextResult());
        }

        public void Close()
        {
            if (_r != null && !_r.IsClosed)
                _r.Close();
        }


        // Read Helpers 

        public T Get<T>(string columnName)
        {
            return Get<T>(columnName, default(T));
        }

        public T Get<T>(string columnName, T defaultValue)
        {
            object result = _r[columnName];

            if (typeof(T).IsEnum)
                return (T)Enum.Parse(typeof(T), _r[columnName].ToString(), true);
            if (result == DBNull.Value)
                return defaultValue;
            if (typeof(T) == typeof(Guid))
                result = new Guid(result.ToString());

            return (T)Convert.ChangeType(result, typeof(T));
        }

        public T Get<T>(int ordinal)
        {
            return Get<T>(ordinal, default(T));
        }

        public T Get<T>(int ordinal, T defaultValue)
        {
            object result = _r[ordinal];

            if (typeof(T).IsEnum)
                return (T)Enum.Parse(typeof(T), result.ToString(), true);
            if (result == DBNull.Value)
                return defaultValue;
            if (typeof(T) == typeof(Guid))
                result = new Guid(result.ToString());

            return (T)Convert.ChangeType(result, typeof(T));
        }

        #endregion

    }
}
