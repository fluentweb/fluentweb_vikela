﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Collections.Specialized;
using Common.Extensions;
using System.Web;

namespace Common.HttpUtils
{
    public class HttpUtilsManager
    {
        public static string GetBaseURL()
        {
            HttpContext context = HttpContext.Current;
            string baseUrl = context.Request.Url.Scheme + "://" + context.Request.Url.Authority + context.Request.ApplicationPath.TrimEnd('/') + '/';
            return baseUrl;
        }

		public static string GetSubDomain(Uri url)
        {
            if (url.HostNameType == UriHostNameType.Dns)
            {
                string host = url.Host;
                if (host.Split('.').Length > 2)
                {
                    int lastIndex = host.LastIndexOf(".");
                    int index = host.LastIndexOf(".", lastIndex - 1);
                    return host.Substring(0, index);
                }
            }

            return null;
        }
		
        public static PostResult Post(string url, NameValueCollection dataToPost)
        {
            PostResult r = new PostResult();

            string dataString = String.Empty;


            WebRequest request = HttpWebRequest.Create(url);
            request.Method = WebRequestMethods.Http.Post;
            request.ContentType = "application/x-www-form-urlencoded";

            Dictionary<string, string> responseValues = new Dictionary<string, string>();
            dataString = dataToPost.ToQueryString(true);

            byte[] data = Encoding.UTF8.GetBytes(dataString);
            request.ContentLength = data.Length;

            using (Stream requestStream = request.GetRequestStream())
            {
                requestStream.Write(data, 0, data.Length);
            }

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                r.StatusCode = response.StatusCode;
                r.Server = response.Server;
                r.StatusDescription = response.StatusDescription;
                r.ContentType = response.ContentType;
                r.ResponseUri = response.ResponseUri;
                using (Stream responseStream = response.GetResponseStream())
                {
                    using (StreamReader sr = new StreamReader(responseStream, Encoding.UTF8))
                    {
                        if (!sr.EndOfStream)
                            r.ResponseString = sr.ReadToEnd();
                    }
                }
            }

            return r;
        }        
    }
}
