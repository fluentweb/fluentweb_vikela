﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace Common.HttpUtils
{
    public class PostResult
    {
        public HttpStatusCode StatusCode { get; set; }
        public string StatusDescription { get; set; }
        public string Server { get; set; }
        public string ContentType { get; set; }
        public Uri ResponseUri { get; set; }
        public string ResponseString { get; set; }
    }    
}
