﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using DataAccess;
using Common.Email.Entities;

namespace Common.Email.DAC
{
    public class DAC_FailedEmail
    {
        public static long InsertFailedEmail(DbConnection connection, DbTransaction transaction, FailedEmail failedEmail)
        {
            return DbFactory.ExecuteScalar<long>("spInsertFailedEmail", connection, transaction, new Dictionary<string, object> 
            { 
                {"Sender", failedEmail.Sender },
                {"Recipient", failedEmail.Recipient },
                {"EmailMessage", failedEmail.EmailMessage },
                {"EmailSubject", failedEmail.EmailSubject },
                {"ErrorMessage", failedEmail.ErrorMessage }
            });
        }
    }
}
