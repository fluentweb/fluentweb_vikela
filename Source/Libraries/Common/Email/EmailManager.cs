﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using DataAccess;
using System.Data.Common;
using Common.Email.Entities;
using Common.Email.DAC;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Configuration;
using System.Configuration;
using System.Web.Configuration;
using System.Web;

namespace Common.Email
{
    public class EmailManager
    {


        /// <summary>
        /// Send email to recipients, cc, bcc and attachments
        /// </summary>
        /// <param name="recipients">List of recipients</param>
        /// <param name="sender">The sender email</param>
        /// <param name="displayName">Sender display name</param>
        /// <param name="subject">Subject of email</param>
        /// <param name="body">Email body</param>
        /// <param name="isHtml">Set to true if email is in HTML format</param>
        /// <param name="cc">List of carbon copy emails</param>
        /// <param name="bcc">List of blind carbon copy recipients</param>
        /// <param name="attachments">List of Attachments; pass null if no attachments are available </param>
        /// <returns></returns>
        public static bool SendEmail(List<string> recipients, string sender, string displayName, string subject, string body, bool
            isHtml, List<string> cc, List<string> bcc, List<Attachment> attachments, NetworkCredential credentials)
        {
            bool sent = false;

            try
            {
                MailMessage mail = new MailMessage();
                mail.IsBodyHtml = isHtml;
                mail.From = new MailAddress(sender, displayName);
                mail.Body = body;
                //mail.BodyEncoding = System.Text.Encoding.UTF8;                
                mail.Subject = subject;

                //add recipients
                if (recipients != null)
                {
                    foreach (string recipient in recipients)
                    {
                        if (recipient != String.Empty)
                            mail.To.Add(recipient);
                    }
                }

                //add carbon copy recipients
                if (cc != null)
                {
                    foreach (string ccRecipient in cc)
                    {
                        if (ccRecipient != String.Empty)
                            mail.CC.Add(ccRecipient);
                    }
                }

                //add blind carbon copy recipients
                if (bcc != null)
                {
                    foreach (string bccRecipient in bcc)
                    {
                        if (bccRecipient != String.Empty)
                            mail.Bcc.Add(bccRecipient);
                    }
                }

                //add attachments 
                if (attachments != null)
                {
                    foreach (Attachment att in attachments)
                    {
                        if (att != null)
                            mail.Attachments.Add(att);
                    }
                }
                Configuration c = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
                MailSettingsSectionGroup settings = (MailSettingsSectionGroup)c.GetSectionGroup("system.net/mailSettings");

                SmtpClient smtpClient = new SmtpClient(settings.Smtp.Network.Host);

                if (credentials != null && !String.IsNullOrEmpty(credentials.UserName) && !String.IsNullOrEmpty(credentials.Password))
                {
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = new NetworkCredential(credentials.UserName, credentials.Password); //credentials;
                }
                else
                {
                    smtpClient.UseDefaultCredentials = true;
                    if (!String.IsNullOrEmpty(settings.Smtp.Network.UserName) && !String.IsNullOrEmpty(settings.Smtp.Network.Password))
                    {
                        smtpClient.Credentials = new NetworkCredential(settings.Smtp.Network.UserName, settings.Smtp.Network.Password);
                    }
                }
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                if (recipients != null && recipients.Count > 0)
                {
                    smtpClient.Send(mail);
                    sent = true;
                }
                else
                {
                    sent = false;
                }
            }
            catch (Exception ex)
            {
                string tos = String.Empty;

                tos = String.Join(", ", recipients.ToArray());

                DbFactory.Connect<long, FailedEmail>(DAC_FailedEmail.InsertFailedEmail, new FailedEmail
                {
                    EmailMessage = body,
                    EmailSubject = subject,
                    ErrorMessage = String.Format("Error: {0} StackTrace: {1}", ex.Message, ex.StackTrace),
                    Recipient = tos,
                    Sender = sender
                });
                throw;
            }
            return sent;
        }


        /// <summary>
        /// Send email to recipients, cc, bcc and attachments
        /// </summary>
        /// <param name="recipients">Semicolon delimited recipients</param>
        /// <param name="sender">The sender email</param>
        /// <param name="displayName">Sender display name</param>
        /// <param name="subject">Subject of email</param>
        /// <param name="body">Email body</param>
        /// <param name="isHtml">Set to true if email is in HTML format</param>
        /// <param name="cc">Semicolon delimited carbon copy emails</param>
        /// <param name="bcc">Semicolon delimited blind carbon copy recipients</param>
        /// <param name="attachments">List of Attachments; pass null if no attachments are available </param>
        /// <returns></returns>
        public static bool SendEmail(string recipients, string sender, string displayName, string subject, string body, bool isHtml, string cc, string bcc, List<Attachment> attachments, NetworkCredential credential)
        {
            List<string> recipientList = new List<string>();
            List<string> ccList = new List<string>();
            List<string> bccList = new List<string>();
            Regex regex = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");

            if (!String.IsNullOrEmpty(recipients))
            {
                string[] rArr = recipients.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var email in rArr)
                {
                    if (regex.IsMatch(email))
                        recipientList.Add(email);
                }
            }
            if (!String.IsNullOrEmpty(bcc))
            {
                string[] ccArr = cc.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var email in ccArr)
                {
                    if (regex.IsMatch(email))
                        ccList.Add(email);
                }

            }
            if (!String.IsNullOrEmpty(bcc))
            {
                string[] bccArr = bcc.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var email in bccArr)
                {
                    if (regex.IsMatch(email))
                        bccList.Add(email);
                }
            }

            return SendEmail(recipientList, sender, displayName, subject, body, isHtml, ccList, bccList, attachments, credential);
        }

        /// <summary>
        /// Replaces placeholders in a string with custom values
        /// </summary>
        /// <param name="emailBody">The email body html or text</param>
        /// <param name="values">A Dictionary of strings to replace.</param>
        /// <returns></returns>
        public static string ReplaceKeys(string emailBody, Dictionary<string, string> values)
        {
            StringBuilder sbEmail = new StringBuilder(emailBody);

            foreach (string key in values.Keys)
            {
                if (sbEmail.ToString().Contains(key))
                {
                    sbEmail.Replace(key, values[key]);
                }
            }
            return sbEmail.ToString();
        }


    }
}
