﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Net;

namespace Common.Email.Entities
{
    public class EmailTemplate
    {
        public int ID { get; set; }
        public string Key { get; set; }
        public string From { get; set; }
        
        /// <summary>
        /// Semicolon delimited recipient email addresses
        /// </summary>
        public string To { get; set; }

        /// <summary>
        /// Semicolon delimited carbon copy email addresses
        /// </summary>
        public string Cc { get; set; }

        /// <summary>
        /// Semicolon delimited blind carbon copy email addresses
        /// </summary>
        public string Bcc { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string DisplayName { get; set; }

        public NetworkCredential Credentials { get; set; }
    }
}
