﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Email.Entities
{
    public class FailedEmail
    {
        public string EmailMessage { get; set; }
        public string ErrorMessage { get; set; }
        public string Sender { get; set; }
        public string Recipient { get; set; }
        public string EmailSubject { get; set; }
    }
}
