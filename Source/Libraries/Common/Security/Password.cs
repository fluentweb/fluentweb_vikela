﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Web.Security;

namespace Common.Security
{
    public class Password
    {
        private static string CreateRandomSalt(int saltSize)
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] buffer = new byte[saltSize];
            rng.GetBytes(buffer);
            return Convert.ToBase64String(buffer);
        }

        public static string CreatePasswordHash(string password, out string salt)
        {
            salt = CreateRandomSalt(6);
            string toHash = String.Format("{0}{1}", password, salt);            
            return Common.Security.Cryprography.GetSHA1Hash(toHash);
        }

        public static bool PasswordMatchesHash(string password, string salt, string hash)
        {
            string toHash = String.Format("{0}{1}", password, salt);            
            return hash == Common.Security.Cryprography.GetSHA1Hash(toHash);
        }

        public static string CreateRandomPassword(int length)
        {
            string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";            
            Random rnd = new Random();
            int seed = rnd.Next(Int32.MinValue, Int32.MaxValue);
            rnd = new Random(seed);

            char[] randomChars = new char[length];
            for (int i = 0; i < length; i++)
            {
                randomChars[i] = allowedChars[rnd.Next(allowedChars.Length)];
            }
            return new String(randomChars);
        }
    }
}
