﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Security
{
    public class SecureLayer
    {
        /// <summary>
        /// Requires setting <add key="UseSecureLayer" value="false" /> from Web.config and init in Global.asax
        /// </summary>
        public static void SetHttpProtocol()
        {
            if (Common.Setup.CommonSettings.UseSecureLayer)
                ForceSSL();
            else
                ForceNoSSL();
        }

        public static void ForceSSL()
        {
            System.Uri currentUrl = System.Web.HttpContext.Current.Request.Url;
            if (!currentUrl.IsLoopback)
            {
                if (!currentUrl.Scheme.Equals(Uri.UriSchemeHttps, StringComparison.CurrentCultureIgnoreCase))
                {
                    System.UriBuilder secureUrlBuilder = new UriBuilder(currentUrl);
                    secureUrlBuilder.Scheme = Uri.UriSchemeHttps;
                    secureUrlBuilder.Port = -1;
                    System.Web.HttpContext.Current.Response.Redirect(secureUrlBuilder.Uri.ToString());
                }
            }
        }

        public static void ForceNoSSL()
        {
            System.Uri currentUrl = System.Web.HttpContext.Current.Request.Url;
            if (!currentUrl.IsLoopback)
            {
                if (currentUrl.Scheme.Equals(Uri.UriSchemeHttps, StringComparison.CurrentCultureIgnoreCase))
                {
                    System.UriBuilder unsecureUrlBuilder = new UriBuilder(currentUrl);
                    unsecureUrlBuilder.Scheme = Uri.UriSchemeHttp;
                    unsecureUrlBuilder.Port = -1;
                    System.Web.HttpContext.Current.Response.Redirect(unsecureUrlBuilder.Uri.ToString());
                }
            }
        }
    }
}
