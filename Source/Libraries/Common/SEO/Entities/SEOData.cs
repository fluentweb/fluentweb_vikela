﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.SEO.Entities
{
    public class SEOData
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Keywords { get; set; }
    }
}
