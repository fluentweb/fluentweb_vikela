﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using DataAccess;
using Common.Payment.SagePay.Entities;

namespace Common.Payment.SagePay.DAC
{
    public class DAC_SagePay
    {
        /// <summary>
        /// Inserts a log record, used for failed requests
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="response"></param>
        /// <returns></returns>
        public static long InsertLog(DbConnection connection, DbTransaction transaction, SagePayResponse response)
        {
            return DbFactory.ExecuteScalar<long>("spInsertSagePayLog", connection, transaction, new Dictionary<string, object> 
            {                 
                {"RequestData", response.RequestData },
                {"Status", response.Status },
                {"StatusDetail", response.StatusDetail },
                {"PaymentReference", response.VendorTxCode }
            });
        }

        /// <summary>
        /// Wrapper for InsertLog method
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        public static long InsertLog(SagePayResponse response)
        {
            return DbFactory.Connect<long, SagePayResponse>(InsertLog, response);
        }
    }
}
