﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Payment.SagePay.Enums
{
    public enum SagePayAcknowlegementAction
    {
        PROCESS_PAYMENT,
        SUCCESS,
        FAILURE,
        INVALID,
        CANCEL
    }
}
