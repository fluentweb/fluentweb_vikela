﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Payment.SagePay.Enums
{
    public enum SagePayNotificationStatus
    {
        /// <summary>
        ///  Send this if you successfully received the notification Post.  Send this unless an error occurs during notification.
        /// </summary>
        OK,

        /// <summary>
        ///  send INVALID if the details you received in the 
        ///  A3 post were  consistent with expectations for this 
        ///  Transaction. The RedirectURL must still be provided, and 
        ///  Sage Pay   will still redirect the Customer back to your site, but 
        ///  the transaction will NOT be settled with the bank.  Only send 
        ///  this result if you want to cancel the transaction
        /// </summary>
        INVALID,

        /// <summary>
        ///  An error has occurred during your Notification 
        ///  processing.  The Sage Pay   system  will check for a 
        ///  RedirectURL, and if one is provided the Customer will be 
        ///  redirected  to your site, but the transaction will NOT be 
        ///  settled with the bank.  Only send this result if you want to 
        ///  cancel the transaction and report an ERROR to Sage Pay.
        /// </summary>
        ERROR
    }
}
