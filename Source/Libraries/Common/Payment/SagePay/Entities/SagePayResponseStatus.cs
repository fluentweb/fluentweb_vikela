﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Payment.SagePay.Entities
{
    public class SagePayResponseStatus
    {
        public const string OK = "OK";
        public const string OK_REPEATED = "OK REPEATED";
        public const string MALFORMED = "MALFORMED";
        public const string INVALID = "INVALID";
        public const string ERROR = "ERROR";
        public const string NOT_AUTHED = "NOTAUTHED";

        /// <summary>
        /// Custom status for any website related error
        /// </summary>
        public const string WEBSITE_ERROR = "WEBSITE ERROR";


    }
}
