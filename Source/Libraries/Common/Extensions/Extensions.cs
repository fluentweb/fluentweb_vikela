﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Web;
using System.Collections.Specialized;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Reflection;
using System.Data;
using System.Xml.Serialization;
using System.IO;
using System.Text.RegularExpressions;

namespace Common.Extensions
{
    public static class Extensions
    {
        #region String Extensions

        public static bool IsValidDate(this string value, string format)
        {
            DateTime dtResult = DateTime.MinValue;
            return DateTime.TryParseExact(value, format, CultureInfo.CurrentCulture, DateTimeStyles.None, out dtResult);
        }

        /// <summary>
        /// Converts a valid string of specified format to DateTime
        /// </summary>
        /// <param name="value"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static DateTime GetDateTime(this string value, string format)
        {
            if (!IsValidDate(value, format)) return DateTime.MinValue;
            return DateTime.ParseExact(value, format, CultureInfo.CurrentCulture);
        }

        public static int ToInt32(this string value)
        {
            return ToInt32(value, default(int));
        }

        public static int ToInt32(this string value, int defaultValue)
        {
            int result = default(int);
            if (Int32.TryParse(value, out result))
                return result;
            return defaultValue;
        }


        public static Int64 ToInt64(this string value)
        {
            return ToInt64(value, default(Int64));
        }

        public static Int64 ToInt64(this string value, Int64 defaultValue)
        {
            Int64 result = default(Int64);
            if (Int64.TryParse(value, out result))
                return result;
            return defaultValue;
        }

        public static string TextIfNullOrEmpty(this string value, string textToShow)
        {
            return String.IsNullOrEmpty(value) ? textToShow : value;
        }

        public static string TrimDownTo(this string s, int charCount, string padding)
        {
            if (s.Trim().Length > charCount)
            {
                return s.Substring(0, charCount - (padding.Length)) + padding;
            }
            return s;
        }

        public static string UrlEncode(this string s)
        {
            return HttpUtility.UrlEncode(s);
        }

        public static string UrlDecode(this string s)
        {
            return HttpUtility.UrlDecode(s);
        }

        public static string CleanForSEO(this string s)
        {
            string str = s.RemoveAccent().ToLower();

            str = Regex.Replace(str, @"[^a-z0-9\s-_]", ""); // remove invalid chars           
            str = Regex.Replace(str, @"\s+", " ").Trim(); // replace multiple spaces into single space     
            str = str.Replace("_", "-");
            str = Regex.Replace(str, @"\s", "-"); // replace spaces to dashes   
            str = Regex.Replace(str, @"^-*|-*$", String.Empty);// remove trailing and leading dashes           
            str = Regex.Replace(str, @"(-)\1+", "-"); // remove repeated dashes
            return str;
        }

        public static string RemoveAccent(this string s)
        {
            byte[] bytes = System.Text.Encoding.GetEncoding("Cyrillic").GetBytes(s);
            return System.Text.Encoding.ASCII.GetString(bytes);
        }



        #endregion

        #region Integer Extensions

        public static string TextIfNullOrEmpty(this int value, string textToShow)
        {
            return value == 0 ? textToShow : value.ToString();
        }

        #endregion

        #region Generic Extensions

        public static T[] RemoveEmptyEntries<T>(this T[] array)
        {
            if (array == null) return null;
            List<T> newList = new List<T>();
            for (int i = 0; i < array.Length; i++)
            {
                T currentItem = array[i];
                if (currentItem == null) continue;
                if (currentItem is String && currentItem.ToString() == String.Empty) continue;
                if (array[i] != null) newList.Add(array[i]);
            }
            return newList.ToArray();
        }

        /// <summary>
        /// Returns a typed querystring value, if this fails the default of the type is returned
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="request"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static T GetQueryString<T>(this HttpRequest request, string key)
        {
            return GetQueryString<T>(request, key, default(T));
        }

        /// <summary>
        /// Returns a typed querystring value, if this fails the defaultValue is returned
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="request"></param>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static T GetQueryString<T>(this HttpRequest request, string key, T defaultValue)
        {
            string val = null;

            if (!String.IsNullOrEmpty(request.QueryString[key]))
                val = request.QueryString[key];

            if (val == null) return defaultValue;

            try
            {
                if (typeof(T).IsEnum) return (T)Enum.Parse(typeof(T), val, true);
                return (T)Convert.ChangeType(val, typeof(T));
            }
            catch { }

            return defaultValue;
        }


        /// <summary>
        /// Returns a typed request value, if this fails the default of the type is returned
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="request"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static T GetRequestValue<T>(this HttpRequest request, string key)
        {
            return GetRequestValue<T>(request, key, default(T));
        }

        /// <summary>
        /// Returns a typed request value, if this fails the defaultValue is returned
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="request"></param>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static T GetRequestValue<T>(this HttpRequest request, string key, T defaultValue)
        {
            string val = null;

            if (!String.IsNullOrEmpty(request[key]))
                val = request[key];

            if (val == null) return defaultValue;

            try
            {
                if (typeof(T).IsEnum) return (T)Enum.Parse(typeof(T), val, true);
                return (T)Convert.ChangeType(val, typeof(T));
            }
            catch { }

            return defaultValue;
        }


        public static T GetDataTableValue<T>(this DataTable table, int rowIndex, string columnName)
        {
            return GetDataTableValue<T>(table, rowIndex, columnName, default(T));
        }

        public static T GetDataTableValue<T>(this DataTable table, int rowIndex, string columnName, T defaultValue)
        {
            try
            {
                string value = table.Rows[rowIndex][columnName].ToString();
                if (value == null) return defaultValue;

                if (typeof(T).IsEnum) return (T)Enum.Parse(typeof(T), value, true);
                return (T)Convert.ChangeType(value, typeof(T));
            }
            catch { }

            return defaultValue;
        }

        public static string Serialize<T>(this T obj)
        {
            string result = String.Empty;
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            using (StringWriter writer = new StringWriter())
            {
                serializer.Serialize(writer, obj);
                result = writer.ToString();
            }

            return result;
        }

        public static T Deserialize<T>(this string xmlObject)
        {
            T obj = default(T);
            string result = String.Empty;
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            using (StringReader sr = new StringReader(xmlObject))
            {
                obj = (T)serializer.Deserialize(sr);
            };

            return obj;
        }

        #endregion

        #region Date and Time Extensions

        public static string FormatTimeSpan(this TimeSpan ts, string format)
        {
            DateTime dt = new DateTime(ts.Ticks);
            return dt.ToString(format);
        }

        #endregion

        #region Collection Extensions

        //OIS.MHS 11-08-2016
        public static string ToQueryString(this NameValueCollection collection)
        {
            List<string> items = new List<string>();

            foreach (string key in collection.Keys)
            {
                items.Add(String.Concat(key, "=", HttpUtility.UrlEncode(collection[key])));
            }

            return String.Join("&", items.ToArray());
        }

        public static string ToQueryString(this NameValueCollection collection, bool urlEncode)
        {
            List<string> items = new List<string>();

            foreach (string key in collection.Keys)
            {
                if (urlEncode)
                    items.Add(String.Concat(key, "=", HttpUtility.UrlEncode(collection[key])));
                else
                    items.Add(String.Concat(key, "=", collection[key]));
            }

            return String.Join("&", items.ToArray());
        }



        public static T GetDictionaryValue<K, T>(this Dictionary<K, T> dict, K key)
        {
            return GetDictionaryValue<K, T>(dict, key);
        }

        public static T GetDictionaryValue<K, T>(this Dictionary<K, T> dict, K key, T defaultValue)
        {
            T value;
            if (dict.TryGetValue(key, out value))
                return value;
            return defaultValue;
        }

        /// <summary>
        /// Converts object first level properties to NameValueCollection 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static NameValueCollection ToNameValueCollection<T>(this T obj) where T : class
        {
            NameValueCollection fields = new NameValueCollection();
            PropertyInfo[] properties = typeof(T).GetProperties();
            if (properties == null || properties.Length == 0) return fields;

            foreach (var pi in properties)
            {
                object currentPropValue = pi.GetValue(obj, null);
                if (currentPropValue == null) continue;
                fields.Add(pi.Name, currentPropValue.ToString());
            }

            return fields;
        }

        public static NameValueCollection ToNameValueCollection<T>(this T obj, bool ignoreEmptyAndDefaultValues) where T : class
        {
            if (!ignoreEmptyAndDefaultValues) return ToNameValueCollection(obj);

            NameValueCollection fields = new NameValueCollection();
            PropertyInfo[] properties = typeof(T).GetProperties();
            if (properties == null || properties.Length == 0) return fields;

            foreach (var pi in properties)
            {
                object currentPropValue = pi.GetValue(obj, null);
                if (currentPropValue is ValueType)
                {
                    object temp = Activator.CreateInstance(currentPropValue.GetType(), true);
                    if (currentPropValue.Equals(temp)) continue;
                    fields.Add(pi.Name, currentPropValue.ToString());
                }
                else
                {
                    if (currentPropValue == null) continue;
                    if (currentPropValue is String)
                    {
                        if (String.IsNullOrEmpty((string)currentPropValue)) continue;
                        fields.Add(pi.Name, currentPropValue.ToString());
                    }
                }

            }

            return fields;
        }



        #endregion

        #region Ajax Extensions

        /// <summary>
        /// Converts object to JSON format
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToJSON(this object obj)
        {
            JavaScriptSerializer s = new JavaScriptSerializer();
            return s.Serialize(obj);
        }

        /// <summary>
        /// Converts object to JSON format
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="recursionDepth">Limit number of object leves to process</param>
        /// <returns></returns>
        public static string ToJSON(this object obj, int recursionDepth)
        {
            JavaScriptSerializer s = new JavaScriptSerializer();
            s.RecursionLimit = recursionDepth;
            return s.Serialize(obj);
        }

        /// <summary>
        /// Convert json string object to object of type T
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="jsonString"></param>
        /// <returns></returns>
        public static T JSONToObject<T>(this string jsonString)
        {
            JavaScriptSerializer s = new JavaScriptSerializer();
            return s.Deserialize<T>(jsonString);
        }

        #endregion

        #region Object extensions
        public static int ToInt32(this object value)
        {
            return ToInt32(value, default(int));
        }

        public static int ToInt32(this object value, int defaultValue)
        {
            int result = default(int);
            if (value == null) return result;

            if (Int32.TryParse(value.ToString(), out result))
                return result;
            return defaultValue;
        }

        public static Int64 ToInt64(this object value)
        {
            return ToInt64(value, default(Int64));
        }

        public static Int64 ToInt64(this object value, Int64 defaultValue)
        {
            Int64 result = default(Int64);
            if (value == null) return result;

            if (Int64.TryParse(value.ToString(), out result))
                return result;
            return defaultValue;
        }
        #endregion

        public static void InsertError(this Page page, string errorMessage)
        {
            CustomValidator validator = new CustomValidator();
            validator.Display = ValidatorDisplay.None;
            validator.IsValid = false;
            validator.ErrorMessage = errorMessage;
            page.Validators.Add(validator);
        }

        public static void InsertError(this Page page, string errorMessage, string validationGroup)
        {
            CustomValidator validator = new CustomValidator();
            validator.Display = ValidatorDisplay.None;
            validator.IsValid = false;
            validator.ErrorMessage = errorMessage;
            validator.ValidationGroup = validationGroup;
            page.Validators.Add(validator);
        }

        #region Control Extensions

        public static void ClearText(this TextBox txt)
        {
            txt.Text = String.Empty;
        }

        public static void DataBindWithDataSource(this DropDownList ddl, object dataSource, string dataTextField, string dataValueField)
        {
            ddl.DataSource = dataSource;
            ddl.DataTextField = dataTextField;
            ddl.DataValueField = dataValueField;
            ddl.DataBind();
        }

        public static void SetDefaultValue(this DropDownList ddl, string text, string value, bool disableIfNoData)
        {
            ddl.Items.Insert(0, new ListItem(text, value));
            if (ddl.Items.Count <= 1)
                ddl.Enabled = !disableIfNoData;
            else
                ddl.Enabled = true;
        }

        #endregion

        #region ADO Extensions

        public static bool HasRows(this DataSet ds, int dataTableIndex)
        {
            if (ds == null) return false;
            if (ds.Tables == null) return false;
            if (ds.Tables[dataTableIndex].Rows.Count > 0) return true;
            return false;
        }

        #endregion

        public static void RedirectFormat(this HttpResponse response, string redirectionUrlWithFormat, params object[] parameters)
        {
            response.Redirect(String.Format(redirectionUrlWithFormat, parameters));
        }

        public static void RedirectFormat(this HttpResponse response, string redirectionUrlWithFormat, bool endResponse, params object[] parameters)
        {
            response.Redirect(String.Format(redirectionUrlWithFormat, parameters), endResponse);
        }

    }
}
