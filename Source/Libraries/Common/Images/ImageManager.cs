﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;

namespace Common.Images
{
    public enum AnchorPosition
    {
        TOP,
        BOTTOM,
        LEFT,
        RIGHT,
        CENTER,
        NONE
    };

    public class ImageManager
    {
        /*http://www.codeproject.com/KB/GDI-plus/imageresize.aspx*/
        public static Image ResizeImageCrop(Image imgPhoto, int width, int height, AnchorPosition anchor)
        {
            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            if (width == 0 && height == 0)
            {
                width = imgPhoto.Width;
                height = imgPhoto.Height;
            }


            nPercentW = ((float)width / (float)sourceWidth);
            nPercentH = ((float)height / (float)sourceHeight);

            if (nPercentH < nPercentW)
            {
                nPercent = nPercentW;
                switch (anchor)
                {
                    case AnchorPosition.TOP:
                        destY = 0;
                        break;
                    case AnchorPosition.BOTTOM:
                        destY = (int)(height - (sourceHeight * nPercent));
                        break;
                    default:
                        destY = (int)((height - (sourceHeight * nPercent)) / 2);
                        break;
                }
            }
            else
            {
                nPercent = nPercentH;
                switch (anchor)
                {
                    case AnchorPosition.LEFT:
                        destX = 0;
                        break;
                    case AnchorPosition.RIGHT:
                        destX = (int)(width - (sourceWidth * nPercent));
                        break;
                    default:
                        destX = (int)((width - (sourceWidth * nPercent)) / 2);
                        break;
                }

            }

            destX--;
            destY--;
            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            try
            {
                Bitmap bmPhoto = new Bitmap(width, height,
                        PixelFormat.Format24bppRgb);
                bmPhoto.SetResolution(imgPhoto.HorizontalResolution,
                    imgPhoto.VerticalResolution);

                Graphics grPhoto = Graphics.FromImage(bmPhoto);
                grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
                grPhoto.Clear(Color.Transparent);
                grPhoto.DrawImage(imgPhoto,
                    new Rectangle(destX, destY, destWidth + 1, destHeight + 1),
                    new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                    GraphicsUnit.Pixel);

                MemoryStream ms = new MemoryStream();
                bmPhoto.Save(ms, ImageFormat.Png);

                grPhoto.Dispose();
                return new Bitmap(ms);
            }
            catch
            {
                throw;
            }

        }
    }
}
