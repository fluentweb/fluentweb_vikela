﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;

namespace Common.FtpUtils
{
    public class FtpUtilsManager
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public string Directory { get; set; }

        public FtpUtilsManager()
        {
            Port = 21;
            Host = String.Empty;
            Username = "anonymous";
            Password = "anonymous@internet.com";
            Directory = String.Empty;
        }

        public FtpUtilsManager(string username, string password, string host, string directory)
        {
            Port = 21;
            Host = host;
            Username = username;
            Password = password;
            Directory = directory;
        }


        private Uri BuildServerUri(string filename)
        {
            return new Uri(String.Format("ftp://{0}:{1}/{2}{3}", Host, Port, Directory != String.Empty ? "/" + Directory + "/" : Directory, filename));
        }

        public byte[] UploadData(string filename, byte[] data)
        {
            // Get the object used to communicate with the server.
            WebClient request = new WebClient();

            // Logon to the server using username + password
            request.Credentials = new NetworkCredential(Username, Password);
            return request.UploadData(BuildServerUri(filename), data);
        }


    }
}
