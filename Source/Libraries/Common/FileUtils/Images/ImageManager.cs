﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

namespace Common.FileUtils.Images
{
    public class ImageManager
    {
        public static Image ResizeFixed(Image i, int maxWidth, int maxHeight)
        {
            // if same size don't resize
            if (i.Width == maxWidth && i.Height == maxHeight) return i;


            int originalWidth = i.Width;
            int originalHeight = i.Height;

            double aspectRatio = ((double)originalWidth / (double)originalHeight);
            double boxRatio = ((double)maxWidth / (double)maxHeight);

            double scaleFactor = 0;

            if (boxRatio > aspectRatio) 
                scaleFactor = ((double)maxHeight / (double)originalHeight);// Height is largest
            else
                scaleFactor = ((double)maxWidth / (double)originalWidth); // Width is largest

            double newWidth = originalWidth * scaleFactor;
            double newHeight = originalHeight * scaleFactor;

            int intNewWidth = Convert.ToInt32(newWidth);
            int intNewHeight = Convert.ToInt32(newHeight);

            Bitmap bmNewImage = new Bitmap(intNewWidth, intNewHeight, PixelFormat.Format24bppRgb);

            bmNewImage.SetResolution(i.HorizontalResolution, i.VerticalResolution);

            Graphics gfx = Graphics.FromImage(bmNewImage);
            gfx.Clear(Color.Transparent);

            gfx.InterpolationMode = InterpolationMode.HighQualityBicubic;
            gfx.SmoothingMode = SmoothingMode.HighQuality;
            gfx.PixelOffsetMode = PixelOffsetMode.HighQuality;
            gfx.CompositingQuality = CompositingQuality.HighQuality;

            ImageAttributes imgAttributes = new ImageAttributes();
            imgAttributes.SetWrapMode(WrapMode.TileFlipXY);

            gfx.DrawImage(i, new Rectangle(0, 0, intNewWidth, intNewHeight), 0, 0, i.Width, i.Height, GraphicsUnit.Pixel, imgAttributes);
            gfx.Dispose();
            
            return bmNewImage;
        }

        public static string GetImageContentType(Image i)
        {
            foreach (ImageCodecInfo ci in ImageCodecInfo.GetImageDecoders())
            {
                if (ci.FormatID != i.RawFormat.Guid) continue;
                return ci.MimeType;
            }
            return "image/unknown";
        }


        // Validation

        public static bool ValidDimensions(HttpPostedFile file, int minWidth, int minHeight, int maxWidth, int maxHeight)
        {
            try
            {
                using (Image i = Image.FromStream(file.InputStream))
                {
                    return ValidDimensions(i, minWidth, minHeight, maxWidth, maxHeight);
                }
            }
            catch
            {
                return false;
            }
        }

        public static bool ValidDimensions(Image i, int minWidth, int minHeight, int maxWidth, int maxHeight)
        {
            if (i == null) return false;
            if (i.Width >= minWidth && i.Width <= maxWidth && i.Height >= minHeight && i.Height <= maxHeight) return true;
            return false;
        }
    }
}
