﻿using Common.Payment.SagePay.Enums;
using DataAccess;
using DataAccess.Base;
using FluentContent.Entities;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.DAC
{
    public class DAC_WebsiteOrderTransaction
    {
        public static WebsiteOrderTransaction Get(long websiteOrderID, long? parentTransactionID = null)
        {
            return DbFactory.Connect<WebsiteOrderTransaction, long, long?>(Get, websiteOrderID, parentTransactionID);
        }

        public static WebsiteOrderTransaction Get(DbConnection connection, DbTransaction transaction, long websiteOrderID, long? parentTransactionID = null)
        {
            WebsiteOrderTransaction item = null;
            DbReader r = null;

            try
            {
                r = new DbReader("spWebsiteOrderTransaction_Select", connection, transaction, new Dictionary<string, object> 
                {
                    { "intWebsiteOrderID", websiteOrderID },
                    { "intParentTransactionID", parentTransactionID }
                });

                if (r.Read())
                {
                    item = ReadWebsiteOrderTransaction(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return item;
        }

        public static WebsiteOrderTransaction Insert(WebsiteOrderTransaction t)
        {
            return DbFactory.Connect<WebsiteOrderTransaction, WebsiteOrderTransaction>(Insert, t);
        }

        public static WebsiteOrderTransaction Insert(DbConnection connection, DbTransaction transaction, WebsiteOrderTransaction t)
        {
            WebsiteOrderTransaction item = null;
            DbReader r = null;
            try
            {
                r = new DbReader("spWebsiteOrderTransaction_Insert", connection, transaction, new Dictionary<string, object> 
                {
                    { "intWebsiteOrderID", t.WebsiteOrderID },                    
                    { "intParentTransaction", t.ParentTransactionID },
                    { "chrTransactionType", t.TransactionType.ToString() },
                    { "chrVendorTxCode", t.VendorTxCode },
                    { "chrVPSTxId", t.VPSTxId },
                    { "chtTxAuthNo", t.TxAuthNo },
                    { "chrSecurityKey", t.SecurityKey },
                    
                });

                if (r.Read())
                {
                    item = ReadWebsiteOrderTransaction(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return item;
        }

        private static WebsiteOrderTransaction ReadWebsiteOrderTransaction(DbReader r)
        {            
            return new WebsiteOrderTransaction
            {
                CreatedOn = r.Get<DateTime>("datCreatedOn"),
                ID = r.Get<long>("intWebsiteOrderTransactionID"),
                ParentTransactionID = r.Get<long>("intParentTransaction"),
                SecurityKey = r.Get<string>("chrSecurityKey"),
                WebsiteOrderID = r.Get<long>("intWebsiteOrderID"),
                TransactionType = r.Get<SagePayTxType>("chrTransactionType"),
                TxAuthNo = r.Get<string>("chtTxAuthNo"),
                VendorTxCode = r.Get<string>("chrVendorTxCode"),
                VPSTxId = r.Get<string>("chrVPSTxId")                 
            };
        }
    }
}
