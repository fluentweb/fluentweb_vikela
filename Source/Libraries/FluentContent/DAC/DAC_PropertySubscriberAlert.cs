﻿using DataAccess;
using DataAccess.Base;
using FluentContent.Entities;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.DAC
{
    public class DAC_PropertySubscriberAlert
    {

        public static PropertySubscriberAlert Insert(PropertySubscriberAlert a)
        {
            return DbFactory.Connect<PropertySubscriberAlert, PropertySubscriberAlert>(Insert, a);
        }

        public static PropertySubscriberAlert Insert(DbConnection connection, DbTransaction transaction, PropertySubscriberAlert a)
        {
            DbReader r = null;

            try
            {
                r = new DbReader("spPropertySubscriberAlerts_Insert", connection, transaction, new Dictionary<string, object>
                {
                    { "intActivatedByCMSUserID", a.ActivatedByCMSUserID == 0 ?(object)DBNull.Value : a.ActivatedByCMSUserID },
                    { "intPropertySubscriberUserID",a.PropertySubscriberUserID },
                    { "intPropertyID", a.PropertyID },
                    { "intEmailTypeItemID", a.EmailAlertItemID == 0 ? (object)DBNull.Value : a.EmailAlertItemID }
                });

                if (r.Read())
                {
                    a = new PropertySubscriberAlert(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return a;
        }
    }
}
