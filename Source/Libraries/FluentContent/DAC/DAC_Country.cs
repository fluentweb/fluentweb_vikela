﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentContent.Entities;
using DataAccess.Base;
using System.Data.Common;
using DataAccess;

namespace FluentContent.DAC
{
    public class DAC_Country
    {
        public static List<Country> Get()
        {
            List<Country> list = new List<Country>();
            DbReader r = null;
            try
            {
                r = new DbReader("spGetCountryList");
                while (r.Read())
                {
                    list.Add(ReadCountry(r));
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return list;
        }

        public static Country Get(DbConnection connection, DbTransaction transaction, long countryID)
        {
            Country item = null;
            DbReader r = null;
            try
            {
                r = new DbReader("spGetCountry", connection, transaction, new Dictionary<string, object> 
                {
                    {"intCountryID", countryID }
                });
                if (r.Read())
                {
                    item = ReadCountry(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return item;
        }
        public static Country Get(long countryID)
        {
            return DbFactory.Connect<Country, long>(Get, countryID);
        }

        private static Country ReadCountry(DbReader r)
        {
            return new Country
            {
                Code = r.Get<string>("chrCountryCode"),
                ID = r.Get<long>("intCountryID"),
                ISO = r.Get<int>("intCountryISO"),
                Name = r.Get<string>("chrCountry"),
                ShortCode = r.Get<string>("chrCountryCodeShort")
            };
        }
    }
}
