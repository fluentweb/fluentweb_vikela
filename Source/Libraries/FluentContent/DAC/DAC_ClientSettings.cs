﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentContent.Entities;
using DataAccess.Base;
using System.Data.Common;
using DataAccess;

namespace FluentContent.DAC
{
    public class DAC_ClientSettings
    {

        public static List<ClientSetting> GetAll()
        {
            return DbFactory.Connect<List<ClientSetting>>(GetAll);
        }

        public static List<ClientSetting> GetAll(DbConnection connection, DbTransaction transaction)
        {
            List<ClientSetting> items = new List<ClientSetting>();
            DbReader r = null;
            try
            {
                r = new DbReader("spClientSettings_Select", connection, transaction, new Dictionary<string, object> 
                {
                    { "ClientID", FluentContent.Settings.ClientID }
                });
                while (r.Read())
                {
                    items.Add(ReadClientSetting(r));
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return items;
        }

        public static ClientSetting Get(string key)
        {
            return DbFactory.Connect<ClientSetting, string>(Get, key);
        }

        public static ClientSetting Get(DbConnection connection, DbTransaction transaction, string key)
        {
            ClientSetting item = null;
            DbReader r = null;
            try
            {
                r = new DbReader("spClientSetting_Select", connection, transaction, new Dictionary<string, object> 
                {
                    { "ClientID", FluentContent.Settings.ClientID },
                    { "Key", key }
                });
                while (r.Read())
                {
                    item = ReadClientSetting(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return item;
        }

        public static int UpdateAllSettings(List<ClientSetting> settings)
        {
            return DbFactory.Connect<int, List<ClientSetting>>(UpdateAllSettings, settings);
        }

        public static int UpdateAllSettings(DbConnection connection, DbTransaction transaction, List<ClientSetting> settings)
        {
            int countUpdated = 0;
            foreach (var s in settings)
            {
                if (UpdateSetting(connection, transaction, s))
                    countUpdated++;
            }

            return countUpdated;
        }

        public static bool UpdateSetting(DbConnection connection, DbTransaction transaction, ClientSetting setting)
        {
            return DbFactory.ExecuteNonQuery("spClientSettings_Update", connection, transaction, new Dictionary<string, object> 
            { 
                { "ClientID", FluentContent.Settings.ClientID },
                { "Value", setting.Value },
                { "Key", setting.Key }
            }) > 0;
        }

        // Helpers

        private static ClientSetting ReadClientSetting(DbReader r)
        {
            return new ClientSetting
            {
                ClientID = r.Get<int>("intClientID"),
                Key = r.Get<string>("chrSettingKey"),
                Value = r.Get<string>("chrSettingValue"),
                Label = r.Get<string>("chrSettingLabel")
            };
        }
    }
}
