﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentContent.Entities;
using System.Data.Common;
using System.Data;
using DataAccess;

namespace FluentContent.DAC
{
    public class DAC_ItemFormRelation
    {
        public static DataTable GetAvailableRelatedForms(int itemID, int formTypeID)
        {
            return DbFactory.Connect<DataTable, int, int>(GetAvailableRelatedForms, itemID, formTypeID);
        }

        public static DataTable GetAvailableRelatedForms(DbConnection connection, DbTransaction transaction, int itemID, int formTypeID)
        {
            return DbFactory.GetDataTable("spRelatedFormsAvailable_Select", connection, transaction, new Dictionary<string, object> 
            {
                { "intItemID", itemID },
                { "intFormTypeID", formTypeID }
            });
        }

        public static DataTable GetItemRelatedForms(int itemID, int formTypeID)
        {
            return DbFactory.Connect<DataTable, int, int>(GetItemRelatedForms, itemID, formTypeID);
        }

        public static DataTable GetItemRelatedForms(DbConnection connection, DbTransaction transaction, int itemID, int formTypeID)
        {
            return DbFactory.GetDataTable("spRelatedForms_Select", connection, transaction, new Dictionary<string, object> 
            {
                { "intItemID", itemID },
                { "intFormTypeID", formTypeID == 0 ? (object)DBNull.Value : formTypeID }
            });
        }

        public static int Insert(int itemID, int formID)
        {
            return DbFactory.Connect<int, int, int>(Insert, itemID, formID);
        }

        public static int Insert(DbConnection connection, DbTransaction transaction, int itemID, int formID)
        {
            return DbFactory.ExecuteScalar<int>("spRelatedForm_Insert", connection, transaction, new Dictionary<string, object> 
            {
                { "intItemID", itemID },
                { "intFormID", formID }
            });
        }

        public static int Delete(int itemFormRelationID)
        {
            return DbFactory.Connect<int, int>(Delete, itemFormRelationID);
        }

        public static int Delete(DbConnection connection, DbTransaction transaction, int itemFormRelationID)
        {
            return DbFactory.ExecuteScalar<int>("spRelatedForm_Delete", connection, transaction, new Dictionary<string, object> 
            {
                { "intItemFormRelateID", itemFormRelationID },
            });
        }
    }
}
