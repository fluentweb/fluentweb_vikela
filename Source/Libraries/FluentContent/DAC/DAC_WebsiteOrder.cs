﻿using DataAccess;
using DataAccess.Base;
using FluentContent.Entities;
using FluentContent.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Extensions;
using System.Net;
using Common.Email;

namespace FluentContent.DAC
{
    public class DAC_WebsiteOrder
    {

        // INSERT

        public static WebsiteOrder Insert(WebsiteOrder o)
        {
            return DbFactory.Connect<WebsiteOrder, WebsiteOrder>(Insert, o);
        }

        public static WebsiteOrder Insert(DbConnection connection, DbTransaction transaction, WebsiteOrder o)
        {            
            DbReader r = null;
            var orderItems = o.OrderItems;

            try
            {
                r = new DbReader("spWebsiteOrder_Insert", connection, transaction, new Dictionary<string, object> 
                {
                    { "chrPaymentRef", o.PaymentReference },
                    { "chrPaymentSecurityKey", o.PaymentSecurityKey },
                    { "chrPayment3DSecureStatus", o.Payment3DSecureStatus },
                    { "chrPaymentCAVV", o.PaymentCAVV },
                    { "intWebUserID", o.WebUser.ID },
                    { "intWebUserAddressID", o.WebUserAddressID },
                    { "intOrderStatus", o.Status },
                    { "decVATRate", o.VATRate },
                    { "decTotalExcludingVat", o.TotalExcludingVAT },
                    { "decTotalIncludingVat", o.TotalIncludingVAT },
                    { "intClientID", Settings.ClientID },                    
                    { "chrOrderTypeKey", o.OrderTypeKey }
                });

                if (r.Read())
                {
                    o = ReadWebsiteOrder(r);
                    if (r != null) r.Close();
                    if (o != null)
                    {
                        o.WebUser = DAC_WebUser.Get(connection, transaction, o.WebUser.ID);
                        if (orderItems.Any())
                        {
                            foreach (var item in orderItems)
                            {
                                item.WebsiteOrderID = o.ID;
                            }

                            o.OrderItems = DAC_WebsiteOrderItem.InsertAll(connection, transaction, orderItems);
                        }
                    }
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return o;
        }


        // SELECT

        public static WebsiteOrder GetByReference(string paymentReference, string transactionGUID)
        {
            return DbFactory.Connect<WebsiteOrder, string, string>(GetByReference, paymentReference, transactionGUID);
        }

        public static WebsiteOrder GetByReference(DbConnection connection, DbTransaction transaction, string paymentReference, string transactionGUID)
        {
            WebsiteOrder item = null;
            DbReader r = null;

            try
            {
                r = new DbReader("spWebsiteOrderByReference_Select", connection, transaction, new Dictionary<string, object> 
                {
                    { "chrPaymentRef", paymentReference },
                    { "chrTransactionGUID", transactionGUID }
                });

                if (r.Read())
                {
                    item = ReadWebsiteOrder(r);
                    if (r != null) r.Close();
                    if (item != null)
                    {
                        item.WebUser = DAC_WebUser.Get(connection, transaction, item.WebUser.ID);
                        item.OrderItems = DAC_WebsiteOrderItem.GetAll(connection, transaction, item.ID);
                    }
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return item;
        }

        public static WebsiteOrder GetByTransactionGUID(string transactionGUID)
        {
            return DbFactory.Connect<WebsiteOrder, string>(GetByTransactionGUID, transactionGUID);
        }

        public static WebsiteOrder GetByTransactionGUID(DbConnection connection, DbTransaction transaction, string transactionGUID)
        {
            WebsiteOrder item = null;
            DbReader r = null;

            try
            {
                r = new DbReader("spWebsiteOrderByTransactionGUID_Select", connection, transaction, new Dictionary<string, object> 
                {                    
                    { "chrTransactionGUID", transactionGUID }
                });

                if (r.Read())
                {
                    item = ReadWebsiteOrder(r);
                    if (r != null) r.Close();
                    if (item != null)
                    {
                        item.WebUser = DAC_WebUser.Get(connection, transaction, item.WebUser.ID);
                        item.OrderItems = DAC_WebsiteOrderItem.GetAll(connection, transaction, item.ID);
                    }
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return item;
        }

        public static WebsiteOrder GetByID(long id)
        {
            return DbFactory.Connect<WebsiteOrder, long>(GetByID, id);
        }

        public static WebsiteOrder GetByID(DbConnection connection, DbTransaction transaction, long id)
        {
            WebsiteOrder item = null;
            DbReader r = null;

            try
            {
                r = new DbReader("spWebsiteOrder_Select", connection, transaction, new Dictionary<string, object> 
                {
                    { "intWebsiteOrderID", id }                  
                });

                if (r.Read())
                {
                    item = ReadWebsiteOrder(r);
                    if (r != null) r.Close();
                    if (item != null)
                    {
                        item.WebUser = DAC_WebUser.Get(connection, transaction,item.WebUser.ID);
                        item.OrderItems = DAC_WebsiteOrderItem.GetAll(connection, transaction, item.ID);
                    }
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return item;
        }

        // UPDATE

        public static WebsiteOrder Update(WebsiteOrder o)
        {
            return DbFactory.Connect<WebsiteOrder, WebsiteOrder>(Update, o);
        }

        public static WebsiteOrder Update(DbConnection connection, DbTransaction transaction, WebsiteOrder o)
        {
            WebsiteOrder item = null;
            DbReader r = null;

            try
            {
                r = new DbReader("spWebsiteOrder_Update", connection, transaction, new Dictionary<string, object> 
                {
                    { "intWebsiteOrderID", o.ID },
                    { "chrPaymentRef", o.PaymentReference },
                    { "chrPaymentSecurityKey", o.PaymentSecurityKey },
                    { "chrPayment3DSecureStatus", o.Payment3DSecureStatus },
                    { "chrPaymentCAVV", o.PaymentCAVV },
                    { "intWebUserID", o.WebUser.ID },
                    { "intWebUserAddressID", o.WebUserAddressID },
                    { "intOrderStatus", o.Status },
                    { "decVATRate",  o.VATRate > 0 ? o.VATRate : (object)DBNull.Value },
                    { "decTotalExcludingVat", o.TotalExcludingVAT > 0 ? o.TotalExcludingVAT : (object)DBNull.Value },  
                    { "decTotalIncludingVat", o.TotalIncludingVAT > 0 ? o.TotalIncludingVAT : (object)DBNull.Value },  
                    { "decRefundAmount", o.RefundAmount > 0 ? o.RefundAmount : (object)DBNull.Value },                    
                    { "chrOrderTypeKey", o.OrderTypeKey }
                });

                if (r.Read())
                {
                    item = ReadWebsiteOrder(r);
                    if (r != null) r.Close();
                    if (item != null)
                    {
                        item.WebUser = DAC_WebUser.Get(connection, transaction, item.WebUser.ID);
                        item.OrderItems = DAC_WebsiteOrderItem.GetAll(connection, transaction, item.ID);
                    }
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return item;
        }

        // HELPERS

        private static WebsiteOrder ReadWebsiteOrder(DbReader r)
        {
            return new WebsiteOrder
            {
                CreatedOn = r.Get<DateTime>("datCreatedOn"),
                ID = r.Get<int>("intWebsiteOrderID"),
                Payment3DSecureStatus = r.Get<string>("chrPayment3DSecureStatus"),
                PaymentCAVV = r.Get<string>("chrPaymentCAVV"),
                PaymentReference = r.Get<string>("chrPaymentRef"),
                PaymentSecurityKey = r.Get<string>("chrPaymentSecurityKey"),
                RefundedOn = r.Get<DateTime>("datRefundedOn"),
                RefundAmount = r.Get<decimal>("decRefundAmount"),
                Status = r.Get<WebsiteOrderStatus>("intOrderStatus"),
                TotalExcludingVAT = r.Get<decimal>("decTotalExcludingVat"),
                TransactionGUID = r.Get<string>("chrTransactionGUID"),
                UpdatedOn = r.Get<DateTime>("datUpdatedOn"),
                VATRate = r.Get<decimal>("decVATRate"),
                WebUserAddressID = r.Get<int>("intWebUserAddressID"),
                WebUser = new WebUser { ID = r.Get<int>("intWebUserID") },                
                ClientID = r.Get<int>("intClientID"),
                OrderTypeKey = r.Get<string>("chrOrderTypeKey")
            };
        }

        public static bool SendOrderConfirmationEmail(WebsiteOrder o)
        {
            DataTable dtRegTemplate = DAC_Item.GetItemByTitle("Email Templates", "Purchase Confirmation");
            if (dtRegTemplate == null || dtRegTemplate.Rows.Count == 0) return false;

            string recipients = String.Format("{0};{1}", o.WebUser.Email, dtRegTemplate.GetDataTableValue(0, "chrText1", String.Empty));
            string from = dtRegTemplate.GetDataTableValue(0, "chrText2", String.Empty);
            string displayName = dtRegTemplate.GetDataTableValue(0, "chrText3", String.Empty);
            string cc = dtRegTemplate.GetDataTableValue(0, "chrText4", String.Empty);
            string bcc = dtRegTemplate.GetDataTableValue(0, "chrText5", String.Empty);
            string body = dtRegTemplate.GetDataTableValue(0, "chrText11", String.Empty);
            string subject = dtRegTemplate.GetDataTableValue(0, "chrText6", String.Empty);
            string password = dtRegTemplate.GetDataTableValue(0, "chrText7", String.Empty);

            // set default sender in case it is empty
            if (from == String.Empty) from = FluentContent.Settings.FailSafeEmail;

            NetworkCredential cred = new NetworkCredential(from, password);

            // link 
            string domainName = Common.HttpUtils.HttpUtilsManager.GetBaseURL(); //ConfigurationManager.AppSettings["WebsiteRoot"];            

            Dictionary<string, string> replaceKeys = new Dictionary<string, string>();

            // replace placeholders
            replaceKeys.Add("##USER_FIRST_NAME##", o.WebUser.FirstName);
            replaceKeys.Add("##AMOUNT##", o.TotalIncludingVAT.ToString("F2"));
            replaceKeys.Add("##ORDER_REFERENCE##", o.TransactionGUID);

            body = EmailManager.ReplaceKeys(body, replaceKeys);
            body = String.Format("<html><body>{0}</body></html>", body);


            try
            {
                return EmailManager.SendEmail(recipients, from, displayName, subject, body, true, cc, bcc, null, cred);
            }
            catch { }

            return false;

        }

    }
}
