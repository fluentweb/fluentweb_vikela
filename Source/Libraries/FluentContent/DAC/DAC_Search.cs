﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;
using DataAccess;

namespace FluentContent.DAC
{
    public class DAC_Search
    {
        public static DataTable SearchItemsAndPages(string query)
        {
            return DbFactory.Connect<DataTable, string>(SearchItemsAndPages, query);
        }

        public static DataTable SearchItemsAndPages(DbConnection connection, DbTransaction transaction, string query)
        {
            return DbFactory.GetDataTable("spSearchItemsPages", connection, transaction, new Dictionary<string, object> 
            {
                { "ClientID", Settings.ClientID },
                { "Query", query }
            });
        }
    }
}
