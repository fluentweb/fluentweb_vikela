﻿using DataAccess;
using DataAccess.Base;
using FluentContent.Entities;
using FluentContent.Entities.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.DAC
{
    public class DAC_TMCountryAdditionalRequirementChoiceType
    {
        public static DataTable GetCountryAdditionalRequirementChoiceType()
        {
            return DbFactory.Connect<DataTable>(GetCountryAdditionalRequirementChoiceType);
        }

        public static DataTable GetCountryAdditionalRequirementChoiceType(DbConnection connection, DbTransaction transaction)
        {
            return DbFactory.GetDataTable("spTMtCountryAdditionalRequirementChoiceType_Select", connection, transaction, new Dictionary<string, object> 
            {
                { "QuestionType", (object)DBNull.Value }
            });
        }


        public static TMCountryAdditionalRequirementChoiceType GetCountryAdditionalRequirementChoiceType(string questionType)
        {
            return DbFactory.Connect<TMCountryAdditionalRequirementChoiceType, string>(GetCountryAdditionalRequirementChoiceType, questionType);
        }

        public static TMCountryAdditionalRequirementChoiceType GetCountryAdditionalRequirementChoiceType(DbConnection connection, DbTransaction transaction, string questionType)
        {
            TMCountryAdditionalRequirementChoiceType item = null;
            DbReader r = null;

            try
            {
                r = new DbReader("spTMtCountryAdditionalRequirementChoiceType_Select", connection, transaction, new Dictionary<string, object>
                {
                    { "QuestionType", questionType }
                });

                if (r.Read())
                {
                    item = new TMCountryAdditionalRequirementChoiceType(r);
                }

                if (r != null) r.Close();
            }
            finally
            {
                if (r != null) r.Close();
            }

            return item;
        }
        public static TMCountryAdditionalRequirementChoiceType ReadProperty(DbReader r)
        {
            return new TMCountryAdditionalRequirementChoiceType(r);
        }
    }
}
