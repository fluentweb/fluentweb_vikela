﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentContent.Entities;
using System.Data.Common;
using DataAccess.Base;
using DataAccess;

namespace FluentContent.DAC
{
    public class DAC_MenuLocation
    {
        /// <summary>
        ///  Returns a list of all Link locations
        /// </summary>
        /// <returns></returns>
        public static List<MenuLocation> Get()
        {
            return DbFactory.Connect<List<MenuLocation>>(Get);
        }

        /// <summary>
        /// Returns a list of all Link locations
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public static List<MenuLocation> Get(DbConnection connection, DbTransaction transaction)
        {
            List<MenuLocation> items = new List<MenuLocation>();
            DbReader r = null;
            try
            {
                r = new DbReader("spLinkLocation_Select", connection, transaction, new Dictionary<string, object> { { "ClientID", Settings.ClientID } });
                while (r.Read())
                {
                    items.Add(ReadMenuLocation(r));
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return items;
        }

        private static MenuLocation ReadMenuLocation(DbReader r)
        {
            return new MenuLocation
            {
                ClientID = r.Get<int>("intClientID"),
                ID = r.Get<int>("intLinkLocationID"),
                Location = r.Get<string>("chrLinkLocation")
            };
        }



    }
}
