﻿using DataAccess;
using DataAccess.Base;
using FluentContent.Entities;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.DAC
{
    public class DAC_WebsiteOrderItem
    {
        
        public static WebsiteOrderItem Get(long id)
        {
            return DbFactory.Connect<WebsiteOrderItem, long>(Get, id);
        }

        public static WebsiteOrderItem Get(DbConnection connection, DbTransaction transaction, long id)
        {
            WebsiteOrderItem item = null;
            DbReader r = null;

            try
            {
                r = new DbReader("spWebsiteOrderItem_Select", connection, transaction, new Dictionary<string, object>
                {
                    { "intWebsiteOrderItemID", id }
                });

                if (r.Read())
                {
                    item = ReadWebsiteOrder(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return item;
        }


        public static List<WebsiteOrderItem> GetAll(long websiteOrderID)
        {
            return DbFactory.Connect<List<WebsiteOrderItem>, long>(GetAll, websiteOrderID);            
        }

        public static List<WebsiteOrderItem> GetAll(DbConnection connection, DbTransaction transaction, long websiteOrderID)
        {
            List<WebsiteOrderItem> items = new List<WebsiteOrderItem>();
            DbReader r = null;

            try
            {
                r = new DbReader("spWebsiteOrderItems_Select", connection, transaction, new Dictionary<string, object>
                {
                    { "intWebsiteOrderID", websiteOrderID }
                });

                while (r.Read())
                {
                    items.Add(ReadWebsiteOrder(r));
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return items;
        }

        
        public static WebsiteOrderItem Insert(WebsiteOrderItem item)
        {
            return DbFactory.Connect<WebsiteOrderItem, WebsiteOrderItem>(Insert, item);
        }

        public static WebsiteOrderItem Insert(DbConnection connection, DbTransaction transaction, WebsiteOrderItem item)
        {            
            DbReader r = null;
            try
            {
                r = new DbReader("spWebsiteOrderItem_Insert", connection, transaction, new Dictionary<string, object>
                {
                    { "intWebsiteOrderID", item.WebsiteOrderID },
                    { "intQuantity", item.Quantity },
                    { "intExternalID", item.ExternalID },
                    { "numPriceExcludingVAT", item.PriceExcludingVAT },
                    { "chrDescription", item.Description }
                });

                if (r.Read())
                {
                    item = ReadWebsiteOrder(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return item;
        }
        
        
        public static WebsiteOrderItem Update(WebsiteOrderItem item)
        {
            return DbFactory.Connect<WebsiteOrderItem, WebsiteOrderItem>(Update, item);
        }

        public static WebsiteOrderItem Update(DbConnection connection, DbTransaction transaction, WebsiteOrderItem item)
        {
            DbReader r = null;

            try
            {
                r = new DbReader("spWebsiteOrderItem_Update", connection, transaction, new Dictionary<string, object>
                {
                    { "intWebsiteOrderItemID", item.ID },
                    { "intWebsiteOrderID", item.WebsiteOrderID },
                    { "intQuantity", item.Quantity },
                    { "intExternalID", item.ExternalID },
                    { "numPriceExludingVAT", item.PriceExcludingVAT },
                    { "chrDescription", item.Description }
                });

                if (r.Read())
                {
                    item = ReadWebsiteOrder(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return item;
        }


        public static List<WebsiteOrderItem> InsertAll(DbConnection connection, DbTransaction transaction, List<WebsiteOrderItem> items)
        {
            if (items == null) return null;
            List<WebsiteOrderItem> insertedItems = new List<WebsiteOrderItem>();
            foreach (var item in items)
            {
                var result = Insert(connection, transaction, item);
                if (result != null)
                    insertedItems.Add(result);
            }

            return insertedItems;
        }

        private static WebsiteOrderItem ReadWebsiteOrder(DbReader r)
        {
            return new WebsiteOrderItem
            {
                DateCreated = r.Get<DateTime>("datCreated"),
                DateUpdated = r.Get<DateTime>("datUpdated"),
                Description = r.Get<string>("chrDescription"),
                ExternalID = r.Get<long>("intExternalID"),
                ID = r.Get<long>("intWebsiteOrderItemID"),
                PriceExcludingVAT = r.Get<decimal>("numPriceExcludingVAT"),
                Quantity = r.Get<int>("intQuantity"),
                WebsiteOrderID = r.Get<long>("intWebsiteOrderID")
            };
        }
    }
}
