﻿using DataAccess;
using DataAccess.Base;
using FluentContent.Entities;
using FluentContent.Entities.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.DAC
{
    public class DAC_TMClassificationClassProduct
    {
        public static DataTable GetClassificationClassProductByClass(int ClassID)
        {
            return DbFactory.Connect<DataTable, int>(GetClassificationClassProductByClass, ClassID);
        }

        public static DataTable GetClassificationClassProductByClass(DbConnection connection, DbTransaction transaction, int ClassID)
        {
            return DbFactory.GetDataTable("spTMtClassificationClassProduct_Select", connection, transaction, new Dictionary<string, object> 
            {
                { "ClassProductID", (object)DBNull.Value },
                { "ClassID",  ClassID == 0 ? (object)DBNull.Value :  ClassID }
            });
        }

        public static TMClassificationClassProduct GetClassificationByProduct(int classProductId)
        {
            return DbFactory.Connect<TMClassificationClassProduct, int>(GetClassificationByProduct, classProductId);
        }

        public static TMClassificationClassProduct GetClassificationByProduct(DbConnection connection, DbTransaction transaction, int classProductId)
        {
            DbReader r = null;
            TMClassificationClassProduct result = null;
            try
            {
                r = new DbReader("spTMtClassificationClassProduct_Select", connection, transaction, new Dictionary<string, object>
                {
                    { "ClassProductID", classProductId },
                    { "ClassID", (object)DBNull.Value },
                });

                if (r.Read())
                {
                    result = new TMClassificationClassProduct(r);
                }

                if (r != null) r.Close();
            }
            finally
            {
                if (r != null) r.Close();
            }

            return result;
        }

        public static TMClassificationClassProduct Insert(TMClassificationClassProduct tmc)
        {
            return DbFactory.Connect<TMClassificationClassProduct, TMClassificationClassProduct>(Insert, tmc);
        }

        public static TMClassificationClassProduct Insert(DbConnection connection, DbTransaction transaction, TMClassificationClassProduct tmc)
        {
            DbReader r = null;

            try
            {
                r = new DbReader("spTMtClassificationClassProduct_Insert", connection, transaction, new Dictionary<string, object>
                {                
                    {"ClassificationClassID",tmc.TMClassificationClassID},
                    {"ClassProductCode",tmc.ClassificationClassProductCode},
                    {"ClassProductNo",tmc.ClassificationClassProductNo  == 0 ? (object)DBNull.Value : tmc.ClassificationClassProductNo},
                    {"ClassProductName",tmc.ClassificationClassProduct}
                });

                if (r.Read())
                {
                    tmc = new TMClassificationClassProduct(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return tmc;
        }

        public static TMClassificationClassProduct Update(TMClassificationClassProduct tmc)
        {
            return DbFactory.Connect<TMClassificationClassProduct, TMClassificationClassProduct>(Update, tmc);
        }

        public static TMClassificationClassProduct Update(DbConnection connection, DbTransaction transaction, TMClassificationClassProduct tmc)
        {
            DbReader r = null;

            try
            {
                r = new DbReader("spTMtClassificationClassProduct_Update", connection, transaction, new Dictionary<string, object>
                {
                    {"ClassificationClassID",tmc.TMClassificationClassID},
                    {"ClassProductCode",tmc.ClassificationClassProductCode},
                    {"ClassProductNo",tmc.ClassificationClassProductNo  == 0 ? (object)DBNull.Value : tmc.ClassificationClassProductNo},
                    {"ClassProductName",tmc.ClassificationClassProduct},
                    {"ClassProductID",tmc.TMClassificationClassProductID}
                });

                if (r.Read())
                {
                    tmc = new TMClassificationClassProduct(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return tmc;
        }

        public static bool Delete(TMClassificationClassProduct tmc)
        {
            return DbFactory.Connect<bool, TMClassificationClassProduct>(Delete, tmc);
        }

        public static bool Delete(DbConnection connection, DbTransaction transaction, TMClassificationClassProduct tmc)
        {
            return DbFactory.ExecuteNonQuery("spTMtClassificationClassProduct_Delete", connection, transaction, new Dictionary<string, object> 
            {
                { "ClassProductID", tmc.TMClassificationClassProductID }
            }) > 0;
        }

        public static TMClassification ReadProperty(DbReader r)
        {
            return new TMClassification(r);
        }
    }
}
