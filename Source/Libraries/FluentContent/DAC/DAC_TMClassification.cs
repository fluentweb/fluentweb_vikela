﻿using DataAccess;
using DataAccess.Base;
using FluentContent.Entities;
using FluentContent.Entities.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.DAC
{
    public class DAC_TMClassification
    {
        public static DataTable GetClassificationByClassification()
        {
            return DbFactory.Connect<DataTable>(GetClassificationByClassification);
        }

        public static DataTable GetClassificationByClassification(DbConnection connection, DbTransaction transaction)
        {
            return DbFactory.GetDataTable("spTMClassificationByClassification_Select", connection, transaction, new Dictionary<string, object> 
            {
                { "ClassificationID",  (object)DBNull.Value  },
            });
        }

        public static TMClassification GetClassificationByClassification(int classificationId)
        {
            return DbFactory.Connect<TMClassification, int>(GetClassificationByClassification, classificationId);
        }

        public static TMClassification GetClassificationByClassification(DbConnection connection, DbTransaction transaction, int classificationId)
        {
            DbReader r = null;
            TMClassification result = null;
            try
            {
                r = new DbReader("spTMClassificationByClassification_Select", connection, transaction, new Dictionary<string, object>
                {
                    { "ClassificationID", classificationId }
                });

                if (r.Read())
                {
                    result = new TMClassification(r);
                }

                if (r != null) r.Close();
            }
            finally
            {
                if (r != null) r.Close();
            }

            return result;
        }

        public static TMClassification Insert(TMClassification tmc)
        {
            return DbFactory.Connect<TMClassification, TMClassification>(Insert, tmc);
        }

        public static TMClassification Insert(DbConnection connection, DbTransaction transaction, TMClassification tmc)
        {
            DbReader r = null;

            try
            {
                r = new DbReader("spTMtClassification_Insert", connection, transaction, new Dictionary<string, object>
                {
                    {"ClassificationTitle",tmc.ClassificationTitle},
                    {"Classification",tmc.Classification}
                });

                if (r.Read())
                {
                    tmc = new TMClassification(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return tmc;
        }

        public static TMClassification Update(TMClassification tmc)
        {
            return DbFactory.Connect<TMClassification, TMClassification>(Update, tmc);
        }

        public static TMClassification Update(DbConnection connection, DbTransaction transaction, TMClassification tmc)
        {
            DbReader r = null;

            try
            {
                r = new DbReader("spTMtClassification_Update", connection, transaction, new Dictionary<string, object>
                {
                    {"ClassificationTitle",tmc.ClassificationTitle},
                    {"Classification",tmc.Classification},
                    {"ClassificationID",tmc.TMClassificationID}
                });

                if (r.Read())
                {
                    tmc = new TMClassification(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return tmc;
        }

        public static bool Delete(TMClassification tmc)
        {
            return DbFactory.Connect<bool, TMClassification>(Delete, tmc);
        }

        public static bool Delete(DbConnection connection, DbTransaction transaction, TMClassification tmc)
        {
            return DbFactory.ExecuteNonQuery("spTMtClassification_Delete", connection, transaction, new Dictionary<string, object> 
            {
                { "ClassificationID", tmc.TMClassificationID }
            }) > 0;
        }

        public static TMClassification ReadProperty(DbReader r)
        {
            return new TMClassification(r);
        }
    }
}
