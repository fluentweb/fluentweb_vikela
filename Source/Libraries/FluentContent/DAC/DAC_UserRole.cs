﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentContent.Entities;
using System.Data.Common;
using DataAccess.Base;
using DataAccess;

namespace FluentContent.DAC
{
    public class DAC_UserRole
    {
        // select

        public static List<UserRole> Get(DbConnection connection, DbTransaction transaction, int roleHierarchy, int clientID)
        {
            List<UserRole> items = new List<UserRole>();
            DbReader r = null;
            try
            {
                r = new DbReader("spUserRole_Select", connection, transaction, new Dictionary<string, object> 
                {
                    {"RoleHierarchy", roleHierarchy },
                    {"intClientID", clientID == 0 ? (object)DBNull.Value : clientID }
                });
                while (r.Read())
                {
                    items.Add(ReadUserRole(r));
                }
            }
            finally
            {
                if (r != null) r.Close();
            }
            return items;
        }

        public static List<UserRole> Get(int roleHierarchy, int clientID)
        {
            return DbFactory.Connect<List<UserRole>, int, int>(Get, roleHierarchy, clientID);
        }

        public static UserRole GetByID(DbConnection connection, DbTransaction transaction, int id, int clientID)
        {
            UserRole item = null;
            DbReader r = null;
            try
            {
                r = new DbReader("spUserRoleByID_Select", connection, transaction, new Dictionary<string, object> 
                {
                    {"intRoleID", id },
                    {"intClientID", clientID == 0 ? (object)DBNull.Value : clientID }
                });

                if (r.Read())
                {
                    item = ReadUserRole(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }
            return item;
        }

        public static UserRole GetByID(int id, int clientID)
        {
            return DbFactory.Connect<UserRole, int, int>(GetByID, id, clientID);
        }


        // helpers

        private static UserRole ReadUserRole(DbReader r)
        {
            return new UserRole
            {
                ClientID = r.Get<int>("intClientID"),
                CMSAccessLevel = r.Get<char>("chrCMSAccessLevel"),
                Description = r.Get<string>("chrRoleDescription"),
                EntityRequired = r.Get<bool>("bitEntityRequired"),
                ID = r.Get<int>("intRoleID"),
                Name = r.Get<string>("chrRoleName"),
                RoleHierarchy = r.Get<int>("intRoleHierarchy")
            };
        }
    }
}
