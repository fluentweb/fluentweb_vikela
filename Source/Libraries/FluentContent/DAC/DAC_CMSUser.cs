﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using FluentContent.Entities;
using DataAccess;
using DataAccess.Base;
using FluentContent.Entities.Helpers;
using System.Data;

namespace FluentContent.DAC
{
    public class DAC_CMSUser
    {
        // Standard CRUD

        public static int Insert(DbConnection connection, DbTransaction transaction, CMSUser user)
        {
            return DbFactory.ExecuteScalar<int>("spInsertCMSUser", connection, transaction, new Dictionary<string, object> 
            {
                {"Username", user.Username },
                {"Name", user.Name },
                {"Surname", user.Surname },
                {"Email", user.Email },
                {"Password", user.Password },
                {"Salt", user.Salt },
                {"CreatedBy", user.CreatedBy },
                {"RoleID", user.Role.ID },
                {"EntityID", user.EntityID },
                {"DepartmentID", user.DepartmentID },
                {"Locked", user.Locked },
                {"ClientID", user.ClientID == 0 ? (object)DBNull.Value : user.ClientID }
            });
        }

        public static int Insert(CMSUser user)
        {
            return DbFactory.Connect<int, CMSUser>(Insert, user);
        }


        public static bool Update(DbConnection connection, DbTransaction transaction, CMSUser user)
        {
            return DbFactory.ExecuteNonQuery("spUpdateCMSUser", connection, transaction, new Dictionary<string, object> 
            {
                {"UserID", user.ID },	
                {"Name", user.Name },	
                {"Surname", user.Surname },	                                
                {"ModifiedBy", user.ModifiedBy },	
                {"RoleID", user.Role.ID },
	            {"EntityID", user.EntityID },
                {"DepartmentID", user.DepartmentID },
                {"Email", user.Email },
                {"Locked", user.Locked },
                {"ClientID", user.ClientID == 0 ? (object)DBNull.Value : user.ClientID },
                {"bitPreference1", user.Preference1 ? user.Preference1 : (object)DBNull.Value }
            }) > 0;
        }

        public static bool Update(CMSUser user)
        {
            return DbFactory.Connect<bool, CMSUser>(Update, user);
        }


        public static CMSUser Get(DbConnection connection, DbTransaction transaction, int userID)
        {
            DbReader r = null;
            try
            {
                r = new DbReader("spGetCMSUser", connection, transaction, new Dictionary<string, object> { { "UserID", userID } });
                while (r.Read())
                {
                    return ReadCMSUser(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return null;
        }

        public static CMSUser Get(int userID)
        {
            return DbFactory.Connect<CMSUser, int>(Get, userID);
        }

        public static CMSUser Get(DbConnection connection, DbTransaction transaction, string username)
        {
            DbReader r = null;
            try
            {
                r = new DbReader("spGetCMSUserByUsername", connection, transaction, new Dictionary<string, object> { { "Username", username } });
                while (r.Read())
                {
                    return ReadCMSUser(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return null;
        }

        public static CMSUser Get(string username)
        {
            return DbFactory.Connect<CMSUser, string>(Get, username);
        }

        public static CMSUser GetByEmail(DbConnection connection, DbTransaction transaction, string email)
        {
            DbReader r = null;
            try
            {
                r = new DbReader("spGetCMSUserByEmail", connection, transaction, new Dictionary<string, object> { { "Email", email } });
                while (r.Read())
                {
                    return ReadCMSUser(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return null;
        }

        public static CMSUser GetByEmail(string email)
        {
            return DbFactory.Connect<CMSUser, string>(GetByEmail, email);
        }

        public static List<CMSUser> GetAllByEntityIDs(DbConnection connection, DbTransaction transaction, List<string> entityIDs)
        {
            List<CMSUser> items = new List<CMSUser>();
            DbReader r = null;
            try
            {
                r = new DbReader("spCMSUsersByEntityIDs_Select", connection, transaction, new Dictionary<string, object> 
                {
                    { "ClientID",Settings.ClientID },
                    { "EntityIDs", String.Join(",",entityIDs != null ? entityIDs.ToArray() : null) },
                 
                });
                while (r.Read())
                {
                    CMSUser u = ReadCMSUser(r);
                    if (u != null)
                    {
                        u.EntityName = r.Get<string>("chrEntityName");
                        items.Add(u);
                    }
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return items;
        }

        public static List<CMSUser> GetAllByEntityIDs(List<string> entityIDs)
        {
            return DbFactory.Connect<List<CMSUser>, List<string>>(GetAllByEntityIDs, entityIDs);
        }

        // user items

        public static DataTable GetUserItemAttachmentTable(DbConnection connection, DbTransaction transaction, int userID)
        {
            DataTable dtResults = null;


            dtResults = DbFactory.GetDataTable("spUserItems_Select", connection, transaction, new Dictionary<string, object> 
            {
                { "intUserID", userID },                    
            });

            return dtResults;
        }

        public static DataTable GetUserItemAttachmentTable(int userID)
        {
            return DbFactory.Connect<DataTable, int>(GetUserItemAttachmentTable, userID);
        }

        public static List<UserItem> GetUserItemAttachment(DbConnection connection, DbTransaction transaction, int userID)
        {
            List<UserItem> items = new List<UserItem>();
            DbReader r = null;

            try
            {
                r = new DbReader("spUserItems_Select", connection, transaction, new Dictionary<string, object> 
                {
                    { "intUserID", userID },                    
                });

                while (r.Read())
                {
                    items.Add(new UserItem
                    {
                        IsAttached = r.Get<bool>("bitIsAttached"),
                        ItemID = r.Get<int>("intItemID"),
                        ItemName = r.Get<string>("chrItemTitle"),
                        ItemType = r.Get<string>("chrItemTypeChr"),
                        UserID = r.Get<int>("intUserID")
                    });
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return items;
        }

        public static List<UserItem> GetUserItemAttachment(int userID)
        {
            return DbFactory.Connect<List<UserItem>, int>(GetUserItemAttachment, userID);
        }

        public static bool DeleteUserItems(DbConnection connection, DbTransaction transaction, int userID)
        {
            return DbFactory.ExecuteNonQuery("spUserItems_Delete", connection, transaction, new Dictionary<string, object> { { "intUserID", userID } }) > 0;
        }

        public static bool DeleteUserItems(int userID)
        {
            return DbFactory.Connect<bool, int>(DeleteUserItems, userID);
        }

        public static bool InsertUserItems(DbConnection connection, DbTransaction transaction, List<UserItem> userItems, int userID)
        {
            // delete existing user items and re-insert new record set
            DeleteUserItems(userID);

            foreach (UserItem item in userItems)
            {
                DbFactory.ExecuteNonQuery("spUserItem_Insert", connection, transaction, new Dictionary<string, object> 
                {
                    {"intUserID", item.UserID },
                    {"intItemID", item.ItemID },
                });
            }

            return true;
        }

        public static bool InsertUserItems(List<UserItem> userItems, int userID)
        {
            return DbFactory.Connect<bool, List<UserItem>, int>(InsertUserItems, userItems, userID);
        }

        /// <summary>
        /// Returns all users except for active user
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="currentUserID"></param>
        /// <returns></returns>
        public static List<CMSUser> GetAll(DbConnection connection, DbTransaction transaction, CMSUser user)
        {
            List<CMSUser> items = new List<CMSUser>();
            DbReader r = null;
            try
            {
                r = new DbReader("spGetCMSUsers", connection, transaction, new Dictionary<string, object> 
                {
                    { "DepartmentID",user.DepartmentID },
                    { "EntityID", user.EntityID },
                    { "RoleHierarchy", user.Role.RoleHierarchy }
                });
                while (r.Read())
                {
                    items.Add(ReadCMSUser(r));
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return items;
        }

        public static List<CMSUser> GetAll(CMSUser u)
        {
            return DbFactory.Connect<List<CMSUser>, CMSUser>(GetAll, u);
        }


        public static bool Delete(DbConnection connection, DbTransaction transaction, int userID)
        {
            return DbFactory.ExecuteNonQuery("spToggleCMSUser", connection, transaction, new Dictionary<string, object> 
            { 
                { "Enable", false }, 
                { "UserID", userID } 
            }) > 0;
        }

        public static bool Delete(int userID)
        {
            return DbFactory.Connect<bool, int>(Delete, userID);
        }

        public static bool ChangePassword(DbConnection connection, DbTransaction transaction, string passwordHash, int userID, string salt, string modifiedBy)
        {
            return DbFactory.ExecuteNonQuery("spUpdateCMSUserPassword", connection, transaction, new Dictionary<string, object> 
            {
                {"UserID", userID },	
                {"Password", passwordHash },	
                {"Salt", salt },
                {"ModifiedBy", modifiedBy}
            }) > 0;
        }


        // Validation 


        public static bool CMSAdminExists(DbConnection connection, DbTransaction transaction)
        {
            return DbFactory.ExecuteScalar<int>("spCheckCMSUserAdminExists", connection, transaction) > 0;
        }

        public static bool CMSAdminExists()
        {
            return DbFactory.Connect<bool>(CMSAdminExists);
        }

        public static bool UserUsernameExists(DbConnection connection, DbTransaction transaction, string username)
        {
            return DbFactory.ExecuteScalar<int>("spCheckCMSUserUsernameExists", connection, transaction, new Dictionary<string, object> { { "Username", username } }) > 0;
        }

        public static bool UserEmailExists(DbConnection connection, DbTransaction transaction, string email, int currentUserID)
        {
            return DbFactory.ExecuteScalar<int>("spCheckCMSUserEmailExists", connection, transaction, new Dictionary<string, object> 
            {
                { "Email", email },
                { "CurrentUserID", currentUserID }
            }) > 0;
        }

        public static bool UserEmailExists(string email, int currentUserID)
        {
            return DbFactory.Connect<bool, string, int>(UserEmailExists, email, currentUserID);
        }


        // Helpers

        private static CMSUser ReadCMSUser(DbReader r)
        {
            return new CMSUser
            {
                ID = r.Get<int>("intUserID"),
                CreatedBy = r.Get<string>("chrCreatedBy"),
                Email = r.Get<string>("chrEmail"),
                ModifiedBy = r.Get<string>("chrModifiedBy"),
                Name = r.Get<string>("chrName"),
                Password = r.Get<string>("chrPassword"),
                Salt = r.Get<string>("chrSalt"),
                Surname = r.Get<string>("chrSurname"),
                Username = r.Get<string>("chrUsername"),
                CreatedOn = r.Get<DateTime>("datCreated"),
                ModifiedOn = r.Get<DateTime>("datModified"),
                Role = new UserRole
                {
                    ID = r.Get<int>("intUserRoleID"),
                    ClientID = r.Get<int>("intClientID"),
                    CMSAccessLevel = r.Get<char>("chrCMSAccessLevel"),
                    Description = r.Get<string>("chrRoleDescription"),
                    EntityRequired = r.Get<bool>("bitEntityRequired"),
                    Name = r.Get<string>("chrRoleName"),
                    RoleHierarchy = r.Get<int>("intRoleHierarchy")
                },
                DepartmentID = r.Get<int>("intDepartmentID"),
                EntityID = r.Get<int>("intEntityID"),
                DepartmentName = r.Get<string>("chrDepartmentName"),
                Locked = r.Get<bool>("bitLocked"),
                ClientID = r.Get<int>("intClientID"),
                Preference1 = r.Get<bool>("bitPreference1")
            };
        }
    }
}
