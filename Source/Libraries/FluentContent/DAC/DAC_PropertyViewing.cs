﻿using DataAccess;
using DataAccess.Base;
using FluentContent.Entities;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.DAC
{
    public class DAC_PropertyViewing
    {
        public static int Insert(PropertyViewing f)
        {
            return DbFactory.Connect<int, PropertyViewing>(Insert, f);
        }

        public static int Insert(DbConnection connection, DbTransaction transaction, PropertyViewing a)
        {
            return DbFactory.ExecuteScalar<int>("spPropertyViewing_INSERT", connection, transaction, new Dictionary<string, object>
                {
                    //{ "intPropertyViewingID" , a.intPropertyViewingID},
                    { "intPropertyID" ,a.intPropertyID},
                    { "datViewing" ,a.datViewing},
                    { "timTimeFrom" , a.timTimeFrom},
                    { "timTimeTo" , a.timTimeTo}
                });
        }

        // BY ID
        public static PropertyViewing GetPropertyViewingByPropertyID(int intPropertyID)
        {
            return DbFactory.Connect<PropertyViewing, int>(GetPropertyViewingByPropertyID, intPropertyID);
        }

        public static PropertyViewing GetPropertyViewingByPropertyID(DbConnection connection, DbTransaction transaction, int intPropertyID)
        {
            PropertyViewing item = null;
            DbReader r = null;

            try
            {
                r = new DbReader("spPropertyViewing_GetByPropertyID_SELECT", connection, transaction, new Dictionary<string, object> 
                {
                    { "intPropertyID", intPropertyID } 
                });

                if (r.Read())
                {
                    item = ReadPropertyDiaryEntry(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return item;
        }

        // DELETE
        public static bool Delete(int intPropertyViewingID)
        {
            return DbFactory.Connect<bool, int>(Delete, intPropertyViewingID);
        }

        public static bool Delete(DbConnection connection, DbTransaction transaction, int intPropertyViewingID)
        {
            return DbFactory.ExecuteNonQuery("spPropertyViewing_DELETE", connection, transaction, new Dictionary<string, object> 
            {
                {"intPropertyViewingID", intPropertyViewingID }
            }) > 0;
        }


        //Update

        public static int Update(PropertyViewing f)
        {
            return DbFactory.Connect<int, PropertyViewing>(Update, f);
        }

        public static int Update(DbConnection connection, DbTransaction transaction, PropertyViewing a)
        {
            return DbFactory.ExecuteScalar<int>("", connection, transaction, new Dictionary<string, object>
                {
                    { "intPropertyViewingID" , a.intPropertyViewingID},
                    { "intPropertyID" ,a.intPropertyID},
                    { "datViewing" ,a.datViewing},
                    { "timTimeFrom" , a.timTimeFrom},
                    { "timTimeTo" , a.timTimeTo}
                });
        }


        // HELPERS
        private static PropertyViewing ReadPropertyDiaryEntry(DbReader r)
        {
            return new PropertyViewing
            {
                intPropertyViewingID = r.Get<int>("intPropertyViewingID"),
                intPropertyID = r.Get<int>("intPropertyID"),
                datViewing = r.Get<DateTime>("datViewing"),
                timTimeFrom = r.Get<string>("timTimeFrom"),
                timTimeTo = r.Get<string>("timTimeTo")

            };
        }
    }
}
