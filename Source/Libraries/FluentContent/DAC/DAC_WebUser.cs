﻿using DataAccess;
using DataAccess.Base;
using FluentContent.Entities;
using FluentContent.Entities.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using Common.Extensions;
using Common.Email;
using System.Net;
using Common.Email.Entities;
using Common.Email.DAC;
using System.Web.Security;
using Common.Security;

namespace FluentContent.DAC
{
    public class DAC_WebUser
    {
        // INSERT
        public static WebUser Create(WebUser u, WebUserCreationOptions o = null)
        {
            return DbFactory.Connect<WebUser, WebUser, WebUserCreationOptions>(Create, u, o);
        }

        public static WebUser Create(DbConnection connection, DbTransaction transaction, WebUser u, WebUserCreationOptions o)
        {
            WebUser item = null;
            DbReader r = null;

            try
            {
                string salt = null;
                string passwordHash = null;

                if (o.GeneratePassword)
                {
                    string randomPass = Common.Security.Password.CreateRandomPassword(8);
                    passwordHash = Common.Security.Password.CreatePasswordHash(randomPass, out salt);
                    u.ClearTextPassword = randomPass;
                }
                else
                {
                    passwordHash = Common.Security.Password.CreatePasswordHash(u.Password, out salt);
                    u.ClearTextPassword = u.Password;
                }

                u.Salt = salt;
                u.Password = passwordHash;

                r = new DbReader("spWebUser_Insert", connection, transaction, new Dictionary<string, object> 
                {
                    { "chrFirstName", u.FirstName },
                    { "chrLastName", u.LastName },
                    { "intTitleID", u.Title != null ? u.Title.ID : (object) DBNull.Value },
                    { "chrEmail", u.Email},
                    { "chrUsername", u.Username },
                    { "chrSalt", u.Salt },
                    { "chrPassword", u.Password },
                    { "chrLandline", u.Landline},
                    { "chrMobile", u.Mobile },
                    { "chrCompanyName", u.CompanyName },
                    { "chrContactNo", u.ContactNo },
                    { "chrGender", u.Gender },
                    { "datDOB", u.DOB == DateTime.MinValue ? (object)DBNull.Value : u.DOB},
                    { "bitLocked", u.Locked },
                    { "bitAccountConfirmed", u.AcccountConfirmed },
                    { "bitOption1", u.Option1 },
                    { "bitOption2", u.Option2 },
                    { "bitOption3", u.Option3 },
                    { "chrCustom1", u.Custom1 },
                    { "chrCustom2", u.Custom2 },
                    { "chrCustom3", u.Custom3 },
                    { "chrContactPerson", u.ContactPerson }
                });

                if (r.Read())
                {
                    item = ReadWebUser(r);
                    if (item != null)
                        item.ClearTextPassword = u.ClearTextPassword;
                }

                // close reader
                if (r != null) r.Close();

                if (u.Addresses.Count > 0)
                {
                    foreach (var a in u.Addresses)
                    {
                        a.WebUserID = item.ID;
                    }
                    item.Addresses = DAC_WebUserAddress.InsertUpdateAddresses(connection, transaction, u.Addresses);
                }

                // close reader
                if (r != null) r.Close();

                // send necessary emails
                bool sent = SendRegistrationEmail(connection, transaction, o, item);

            }
            finally
            {
                if (r != null) r.Close();
            }
            return item;
        }

        // UPDATE
        public static WebUser Update(WebUser u, bool generatePassword)
        {
            return DbFactory.Connect<WebUser, WebUser, bool>(Update, u, generatePassword);
        }

        public static WebUser Update(DbConnection connection, DbTransaction transaction, WebUser u, bool generatePassword = false)
        {
            WebUser item = null;
            DbReader r = null;

            try
            {

                string salt = null;
                string passwordHash = null;

                // generate password else use already set hash and salt
                if (generatePassword)
                {
                    string randomPass = Common.Security.Password.CreateRandomPassword(8);
                    passwordHash = Common.Security.Password.CreatePasswordHash(randomPass, out salt);

                    u.Salt = salt;
                    u.Password = passwordHash;
                    u.ClearTextPassword = randomPass;
                }

                r = new DbReader("spWebUser_Update", connection, transaction, new Dictionary<string, object> 
                {
                    { "intWebUserID", u.ID },
                    { "chrFirstName", u.FirstName },
                    { "chrLastName", u.LastName },
                    { "intTitleID", u.Title != null ? u.Title.ID : (object) DBNull.Value },
                    { "chrEmail", u.Email},
                    { "chrUsername", u.Username },
                    { "chrSalt", u.Salt },
                    { "chrPassword", u.Password },
                    { "chrLandline", u.Landline},
                    { "chrMobile", u.Mobile },
                    { "chrCompanyName", u.CompanyName },
                    { "chrContactNo", u.ContactNo },
                    { "chrGender", u.Gender },
                    { "datDOB", u.DOB == DateTime.MinValue ? (object)DBNull.Value : u.DOB},
                    { "bitLocked", u.Locked },
                    { "bitAccountConfirmed", u.AcccountConfirmed },
                    { "bitOption1", u.Option1 },
                    { "bitOption2", u.Option2 },
                    { "bitOption3", u.Option3 },
                    { "chrCustom1", u.Custom1 },
                    { "chrCustom2", u.Custom2 },
                    { "chrCustom3", u.Custom3 },
                    { "chrContactPerson", u.ContactPerson }
                });

                if (r.Read())
                {
                    item = ReadWebUser(r);
                    if (item != null)
                        item.ClearTextPassword = u.ClearTextPassword;
                }

                if (r != null) r.Close();

                // update addresses
                if (u.Addresses.Count > 0)
                {
                    foreach (var a in u.Addresses)
                    {
                        a.WebUserID = item.ID;
                    }
                    item.Addresses = DAC_WebUserAddress.InsertUpdateAddresses(connection, transaction, u.Addresses);
                }

                // close reader
                if (r != null) r.Close();

                if (generatePassword)
                {
                    bool sent = SendNewPasswordEmail(connection, transaction, item);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }
            return item;
        }

        //DELETE
        public static bool Delete(int webUserID)
        {
            return DbFactory.Connect<bool, int>(Delete, webUserID);
        }

        public static bool Delete(DbConnection connection, DbTransaction transaction, int webUserID)
        {
            return DbFactory.ExecuteNonQuery("spWebUser_Delete", connection, transaction, new Dictionary<string, object> 
            {
                { "intWebUserID", webUserID }
            }) > 0;
        }

        // SELECT
        public static WebUser Get(int webUserID)
        {
            return DbFactory.Connect<WebUser, int>(Get, webUserID);
        }

        public static WebUser Get(DbConnection connection, DbTransaction transaction, int webUserID)
        {
            WebUser item = null;
            DbReader r = null;

            try
            {
                r = new DbReader("spWebUser_Select", connection, transaction, new Dictionary<string, object> 
                {
                    { "intWebUserID", webUserID }
                });

                if (r.Read())
                {
                    item = ReadWebUser(r);
                }

                if (r != null) r.Close();
                if (item != null)
                    item.Addresses = DAC_WebUserAddress.Get(item.ID);
            }
            finally
            {
                if (r != null) r.Close();
            }

            return item;
        }

        public static WebUser GetByEmail(string email)
        {
            return DbFactory.Connect<WebUser, string>(GetByEmail, email);
        }

        public static WebUser GetByEmail(DbConnection connection, DbTransaction transaction, string email)
        {
            WebUser item = null;
            DbReader r = null;

            try
            {
                r = new DbReader("spWebUserByEmail_Select", connection, transaction, new Dictionary<string, object> 
                {
                    { "chrEmail", email }
                });

                if (r.Read())
                {
                    item = ReadWebUser(r);
                }

                if (r != null) r.Close();
                if (item != null)
                    item.Addresses = DAC_WebUserAddress.Get(item.ID);
            }
            finally
            {
                if (r != null) r.Close();
            }

            return item;
        }


        public static WebUser GetByUsername(string username)
        {
            return DbFactory.Connect<WebUser, string>(GetByUsername, username);
        }

        public static WebUser GetByUsername(DbConnection connection, DbTransaction transaction, string username)
        {
            WebUser item = null;
            DbReader r = null;

            try
            {
                r = new DbReader("spWebUserByUsername_Select", connection, transaction, new Dictionary<string, object> 
                {
                    { "chrUsername", username }
                });

                if (r.Read())
                {
                    item = ReadWebUser(r);
                }

                if (r != null) r.Close();

                if (item != null)
                    item.Addresses = DAC_WebUserAddress.Get(item.ID);
            }
            finally
            {
                if (r != null) r.Close();
            }

            return item;
        }


        // HELPERS

        private static WebUser ReadWebUser(DbReader r)
        {
            return new WebUser
            {
                AcccountConfirmed = r.Get<bool>("bitAccountConfirmed"),
                CompanyName = r.Get<string>("chrCompanyName"),
                ContactNo = r.Get<string>("chrContactNo"),
                ContactPerson = r.Get<string>("chrContactPerson"),
                Custom1 = r.Get<string>("chrCustom1"),
                Custom2 = r.Get<string>("chrCustom2"),
                Custom3 = r.Get<string>("chrCustom3"),
                DateCreated = r.Get<DateTime>("datCreated"),
                DateUpdated = r.Get<DateTime>("datUpdated"),
                DOB = r.Get<DateTime>("datDOB"),
                Email = r.Get<string>("chrEmail"),
                FirstName = r.Get<string>("chrFirstName"),
                Gender = r.Get<string>("chrGender"),
                ID = r.Get<int>("intWebUserID"),
                Landline = r.Get<string>("chrLandline"),
                LastName = r.Get<string>("chrLastName"),
                Locked = r.Get<bool>("bitLocked"),
                Mobile = r.Get<string>("chrMobile"),
                Option1 = r.Get<bool>("bitOption1"),
                Option2 = r.Get<bool>("bitOption2"),
                Option3 = r.Get<bool>("bitOption3"),
                Password = r.Get<string>("chrPassword"),
                Salt = r.Get<string>("chrSalt"),
                Title = new FluentContent.Entities.Title
                {
                    Description = r.Get<string>("chrTitle"),
                    ID = r.Get<int>("intTitleID"),
                },
                Username = r.Get<string>("chrUsername")
            };
        }

        private static bool SendRegistrationEmail(DbConnection connection, DbTransaction transaction, WebUserCreationOptions o, WebUser user)
        {
            bool sent = false;

            if (o.RequiresActivation)
            {
                // send activation email with password
                // if item template exists use it
                // else send default template
                string stringToHash = String.Format("{0}-{1}", user.ID, user.Email);
                string userHash = Common.Security.Cryprography.GetMD5Hash(stringToHash);

                string activationURL = String.Format("{0}/activate-account/{1}/{2}", FluentContent.Settings.WebsiteRoot, userHash, user.Username);
                activationURL = activationURL.Replace("//", "/");
                DataTable dtRegTemplate = DAC_Item.GetItemByTitle("Email templates", "Web User Activation");
                if (dtRegTemplate != null && dtRegTemplate.Rows.Count > 0)
                {
                    string recipients = String.Format("{0};{1}", user.Email, dtRegTemplate.GetDataTableValue(0, "chrText1", String.Empty));
                    string from = dtRegTemplate.GetDataTableValue(0, "chrText2", String.Empty);
                    string displayName = dtRegTemplate.GetDataTableValue(0, "chrText3", String.Empty);
                    string cc = dtRegTemplate.GetDataTableValue(0, "chrText4", String.Empty);
                    string bcc = dtRegTemplate.GetDataTableValue(0, "chrText5", String.Empty);
                    string body = dtRegTemplate.GetDataTableValue(0, "chrText11", String.Empty);
                    string subject = dtRegTemplate.GetDataTableValue(0, "chrText6", String.Empty);
                    string password = dtRegTemplate.GetDataTableValue(0, "chrText7", String.Empty);

                    // set default sender in case it is empty
                    if (from == String.Empty) from = FluentContent.Settings.FailSafeEmail;

                    // replace placeholders
                    body = EmailManager.ReplaceKeys(body, new Dictionary<string, string> 
                        {
                            {"##USERNAME##", user.Username},
                            {"##PASSWORD##", user.ClearTextPassword },
                            {"##FIRSTNAME##", user.FirstName },
                            {"##SURNAME##", user.LastName },
                            {"##ACTIVATION_LINK##", activationURL }
                        });
                    body = String.Format("<html><body>{0}</body></html>", body);
                    NetworkCredential cred = new NetworkCredential(from, password);
                    sent = EmailManager.SendEmail(recipients, from, displayName, subject, body, true, cc, bcc, null, cred);
                }
                else
                {
                    // else send activation email to user using db template
                    EmailTemplate t = DAC_EmailTemplate.Get(connection, transaction, "website_activate_new_account");

                    string recipients = String.Format("{0};{1}", user.Email, t.To);

                    Dictionary<string, string> replaceKeys = new Dictionary<string, string>();

                    replaceKeys.Add("##USERNAME##", user.Username);
                    replaceKeys.Add("##PASSWORD##", user.ClearTextPassword);
                    replaceKeys.Add("##ACTIVATION_LINK##", activationURL);

                    t.Body = EmailManager.ReplaceKeys(t.Body, replaceKeys);
                    sent = EmailManager.SendEmail(recipients, t.From, t.DisplayName, t.Subject, t.Body, true, t.Cc, t.Bcc, null, t.Credentials);
                }
            }
            else
            {
                // send account creation email with password
                // if item template exists use it
                // else send default template

                DataTable dtRegTemplate = DAC_Item.GetItemByTitle("Email templates", "Web User Registration");
                if (dtRegTemplate != null && dtRegTemplate.Rows.Count > 0)
                {
                    string recipients = String.Format("{0};{1}", user.Email, dtRegTemplate.GetDataTableValue(0, "chrText1", String.Empty));
                    string from = dtRegTemplate.GetDataTableValue(0, "chrText2", String.Empty);
                    string displayName = dtRegTemplate.GetDataTableValue(0, "chrText3", String.Empty);
                    string cc = dtRegTemplate.GetDataTableValue(0, "chrText4", String.Empty);
                    string bcc = dtRegTemplate.GetDataTableValue(0, "chrText5", String.Empty);
                    string body = dtRegTemplate.GetDataTableValue(0, "chrText11", String.Empty);
                    string subject = dtRegTemplate.GetDataTableValue(0, "chrText6", String.Empty);
                    string password = dtRegTemplate.GetDataTableValue(0, "chrText7", String.Empty);

                    // set default sender in case it is empty
                    if (from == String.Empty) from = FluentContent.Settings.FailSafeEmail;

                    // replace placeholders
                    body = EmailManager.ReplaceKeys(body, new Dictionary<string, string> 
                        {
                            {"##USERNAME##", user.Username},
                            {"##PASSWORD##", user.ClearTextPassword },
                            {"##FIRSTNAME##", user.FirstName },
                            {"##SURNAME##", user.LastName }
                        });
                    body = String.Format("<html><body>{0}</body></html>", body);
                    NetworkCredential cred = new NetworkCredential(from, password);
                    sent = EmailManager.SendEmail(recipients, from, displayName, subject, body, true, cc, bcc, null, cred);
                }
                else
                {
                    // else send registration email to user using db template
                    EmailTemplate t = DAC_EmailTemplate.Get(connection, transaction, "website_new_user");

                    string recipients = String.Format("{0};{1}", user.Email, t.To);

                    Dictionary<string, string> replaceKeys = new Dictionary<string, string>();

                    replaceKeys.Add("##USERNAME##", user.Username);
                    replaceKeys.Add("##PASSWORD##", user.ClearTextPassword);

                    t.Body = EmailManager.ReplaceKeys(t.Body, replaceKeys);
                    sent = EmailManager.SendEmail(recipients, t.From, t.DisplayName, t.Subject, t.Body, true, t.Cc, t.Bcc, null, t.Credentials);
                }


            }
            return sent;

        }

        private static bool SendNewPasswordEmail(DbConnection connection, DbTransaction transaction, WebUser user)
        {
            bool sent = false;

            // send new password email 
            // if item template exists use it
            // else send default template

            DataTable dtRegTemplate = DAC_Item.GetItemByTitle("Email templates", "Web User Generate New Password");
            if (dtRegTemplate != null && dtRegTemplate.Rows.Count > 0)
            {
                string recipients = String.Format("{0};{1}", user.Email, dtRegTemplate.GetDataTableValue(0, "chrText1", String.Empty));
                string from = dtRegTemplate.GetDataTableValue(0, "chrText2", String.Empty);
                string displayName = dtRegTemplate.GetDataTableValue(0, "chrText3", String.Empty);
                string cc = dtRegTemplate.GetDataTableValue(0, "chrText4", String.Empty);
                string bcc = dtRegTemplate.GetDataTableValue(0, "chrText5", String.Empty);
                string body = dtRegTemplate.GetDataTableValue(0, "chrText11", String.Empty);
                string subject = dtRegTemplate.GetDataTableValue(0, "chrText6", String.Empty);
                string password = dtRegTemplate.GetDataTableValue(0, "chrText7", String.Empty);

                // set default sender in case it is empty
                if (from == String.Empty) from = FluentContent.Settings.FailSafeEmail;

                // replace placeholders
                body = EmailManager.ReplaceKeys(body, new Dictionary<string, string> 
                        {
                            {"##USERNAME##", user.Username},
                            {"##PASSWORD##", user.ClearTextPassword },
                            {"##FIRSTNAME##", user.FirstName },
                            {"##SURNAME##", user.LastName }
                        });
                body = String.Format("<html><body>{0}</body></html>", body);
                NetworkCredential cred = new NetworkCredential(from, password);
                sent = EmailManager.SendEmail(recipients, from, displayName, subject, body, true, cc, bcc, null, cred);
            }
            else
            {
                // else send generated password to user using db template
                EmailTemplate t = DAC_EmailTemplate.Get(connection, transaction, "website_generate_new_password");

                // send email
                string recipients = String.Format("{0};{1}", user.Email, t.To);
                t.Body = EmailManager.ReplaceKeys(t.Body, new Dictionary<string, string>
                {                        
                    {"##PASSWORD##",user.ClearTextPassword }
                });
                sent = EmailManager.SendEmail(recipients, t.From, t.DisplayName, t.Subject, t.Body, true, t.Cc, t.Bcc, null, t.Credentials);

            }
            return sent;

        }
    }
}
