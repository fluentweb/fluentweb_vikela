﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentContent.Entities;
using System.Data.Common;
using DataAccess.Base;

namespace FluentContent.DAC
{
    public class DAC_Title
    {
        /// <summary>
        /// Returns a list of Title objects
        /// </summary>
        /// <returns></returns>
        public static List<Title> Get()
        {
            List<Title> items = new List<Title>();
            DbReader r = null;
            try
            {
                r = new DbReader("spGetTitle");
                while (r.Read())
                {
                    items.Add(ReadTitle(r));
                }
            }
            finally
            {
                if (r != null) r.Close();
            }
            return items;
        }

        private static Title ReadTitle(DbReader r)
        {
            return new Title
            {
                ID = r.Get<int>("intTitleID"),
                Description = r.Get<string>("chrTitle")
            };
        }
    }
}
