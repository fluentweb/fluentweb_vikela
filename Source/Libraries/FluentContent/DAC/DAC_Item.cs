﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using DataAccess;
using FluentContent.Helpers;
using FluentContent.Entities;
using DataAccess.Base;
using System.Data;

namespace FluentContent.DAC
{
    public class DAC_Item
    {
        // select

        public static bool ItemURLExists(string url, ItemParameters parameters)
        {
            return DbFactory.Connect<bool, string, ItemParameters>(ItemURLExists, url, parameters);
        }

        public static bool ItemURLExists(DbConnection connection, DbTransaction transaction, string url, ItemParameters parameters)
        {
            return DbFactory.ExecuteScalar<int>("spItemURLExists", connection, transaction, new Dictionary<string, object> 
            {
                { "URL", url },
                { "ItemTypeID", parameters.ItemTypeID },
                { "ItemID", parameters.ItemID == -1 ? (object)DBNull.Value : parameters.ItemID },
                { "ClientID", Settings.ClientID }
            }) > 0;
        }

        public static List<Entity> GetItemEntities(DbConnection connection, DbTransaction transaction, int itemID, int entityItemTypeID)
        {
            List<Entity> items = new List<Entity>();

            DbReader r = null;

            try
            {
                r = new DbReader("spItemEntityAttachment_Select", connection, transaction, new Dictionary<string, object> 
                {
                    { "intItemID", itemID },
                    { "intEntityTypeID", entityItemTypeID },
                    { "intClientID", Settings.ClientID }
                });

                while (r.Read())
                {
                    items.Add(new Entity
                    {
                        ID = r.Get<int>("intEntityID"),
                        Name = r.Get<string>("chrEntityName"),
                        TypeID = r.Get<int>("intEntityTypeID"),
                        IsAttachedToCurrentItem = r.Get<bool>("bitIsAttached"),
                        DateAttachedToCurrentItem = r.Get<DateTime>("datEntityLinked"),
                        DateExpires = r.Get<DateTime>("LinkExpires")
                    });
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return items;
        }

        public static List<Entity> GetItemEntities(int itemID, int entityItemTypeID)
        {
            return DbFactory.Connect<List<Entity>, int, int>(GetItemEntities, itemID, entityItemTypeID);
        }

        public static DataTable GetItemByTitle(string itemType, string title)
        {
            return DbFactory.Connect<DataTable,string, string>(GetItemByTitle, itemType, title);
        }

        public static DataTable GetItemByTitle(DbConnection connection, DbTransaction transaction,string itemType, string title)
        {
            return DbFactory.GetDataTable("spItemByTitle_Select", connection, transaction, new Dictionary<string, object> 
            {
                { "ItemType", itemType },
                { "Title", title },
                { "Client", FluentContent.Settings.ClientID }
            });
        }

        public static DataTable GetEntityLinkedItems(string itemType, int entityID)
        {
            return DbFactory.Connect<DataTable, string, int>(GetEntityLinkedItems, itemType, entityID);
        }

        public static DataTable GetEntityLinkedItems(DbConnection connection, DbTransaction transaction, string itemType, int entityID)
        {
            return DbFactory.GetDataTable("spEntityLinkedItems_Select", connection, transaction, new Dictionary<string, object> 
            {
                { "ItemType", itemType },
                { "EntityID", entityID },
                { "ClientID", FluentContent.Settings.ClientID }
            });
        }

        // insert

        public static bool InsertItemEntities(DbConnection connection, DbTransaction transaction, List<Entity> entities, int itemID)
        {
            int count = 0;
            if (entities.Count > 0)
            {
                // delete existing item entities first
                DbFactory.ExecuteNonQuery("spItemEntities_Delete", connection, transaction, new Dictionary<string, object> 
                {
                    { "intItemID", itemID }
                });

                foreach (Entity e in entities)
                {
                    int result = DbFactory.ExecuteNonQuery("spItemEntity_Insert", connection, transaction, new Dictionary<string, object> 
                    {
                        {"intEntityID", e.ID },
                        {"intItemID", itemID },
                        {"datEntityLinked", e.DateAttachedToCurrentItem == DateTime.MinValue ? (object)DBNull.Value: e.DateAttachedToCurrentItem },
                        {"datExpires", e.DateExpires == DateTime.MinValue ? (object)DBNull.Value : e.DateExpires }
                    });
                    if (result > 0) count++;
                }
            }
            else
            {
                // delete existing item entities
                DbFactory.ExecuteNonQuery("spItemEntities_Delete", connection, transaction, new Dictionary<string, object> 
                {
                    { "intItemID", itemID }
                });

                count++;
            }
            return count > 0;
        }

        /// <summary>
        /// Replaces existing Item entities with new record sets
        /// </summary>
        /// <param name="entityIDs"></param>
        /// <param name="itemID"></param>
        /// <returns></returns>
        public static bool InsertItemEntities(List<Entity> entities, int itemID)
        {
            return DbFactory.Connect<bool, List<Entity>, int>(InsertItemEntities, entities, itemID);
        }

    //    @Display VARCHAR(150),
    //@Client INT,
    //@ParentItemID INT = 0,
    //@CurrentPage INT = NULL,
    //@CategoryID1 INT = NULL,
    //@CategoryID2 INT = NULL,
    //@TextSearch NVARCHAR(200) = NULL,
    //@Parameter1 NVARCHAR(200) = NULL
        public static DataTable GetItemDisplaySearch(string Display, int ParentItemID, int CurrentPage, string TextSearch, string Parameter1)
        {
            return DbFactory.Connect<DataTable, string, int, int, string, string>(GetItemDisplaySearch, Display, ParentItemID, CurrentPage
           ,TextSearch,  Parameter1);
        }
        public static DataTable GetItemDisplaySearch(DbConnection connection, DbTransaction transaction, string Display, int ParentItemID, int CurrentPage
            , string TextSearch, string Parameter1)
        {
            return DbFactory.GetDataTable("spItemDisplaySearch_Select", connection, transaction, new Dictionary<string, object> 
            {
                { "Display" , Display},
                { "Client", FluentContent.Settings.ClientID },
                { "ParentItemID" , ParentItemID},
                { "CurrentPage", CurrentPage == 0 ? (object)DBNull.Value : CurrentPage},
                { "TextSearch",  string.IsNullOrEmpty(TextSearch) ?(object)DBNull.Value : TextSearch},
                { "Parameter1", string.IsNullOrEmpty(Parameter1) ?(object)DBNull.Value : Parameter1}
            });
        }
    }
}
