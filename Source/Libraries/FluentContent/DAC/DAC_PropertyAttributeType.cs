﻿using DataAccess;
using DataAccess.Base;
using FluentContent.Entities;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.DAC
{
    public class DAC_PropertyAttributeType
    {
        
        public static PropertyAttributeType Get(int id)
        {
            return DbFactory.Connect<PropertyAttributeType, int>(Get, id);
        }

        public static PropertyAttributeType Get(DbConnection connection, DbTransaction transaction, int id)
        {
            PropertyAttributeType item = null;
            DbReader r = null;

            try
            {
                r = new DbReader("spPropertyAttributeTypeByID_Select", connection, transaction, new Dictionary<string, object>
                {
                    { "intPropertyAttributeItemID", id }
                });

                if (r.Read())
                {
                    item = ReadPropertyAttributeType(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return item;
        }


        public static List<PropertyAttributeType> GetAll(int propertyID)
        {
            return DbFactory.Connect<List<PropertyAttributeType>, int>(GetAll, propertyID);
        }

        public static List<PropertyAttributeType> GetAll(DbConnection connection, DbTransaction transaction, int propertyID)
        {
            List<PropertyAttributeType> items = new List<PropertyAttributeType>();
            DbReader r = null;

            try
            {
                r = new DbReader("spPropertyAttributeType_Select", connection, transaction, new Dictionary<string, object>
                {
                    { "intPropertyID", propertyID },
                    { "intClientID", Settings.ClientID }
                });

                while (r.Read())
                {
                    items.Add(ReadPropertyAttributeType(r));
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return items;
        }

        private static PropertyAttributeType ReadPropertyAttributeType(DbReader r)
        {
            return new PropertyAttributeType(r);
        }
    }
}
