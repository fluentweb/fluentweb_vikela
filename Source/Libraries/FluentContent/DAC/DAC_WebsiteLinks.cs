﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;
using DataAccess;

namespace FluentContent.DAC
{
    public class DAC_WebsiteLinks
    {
        /// <summary>
        /// Returns all available links in system
        /// </summary>
        /// <returns></returns>
        public static DataTable GetWebsiteLinkList()
        {
            return DbFactory.Connect<DataTable>(GetWebsiteLinkList);
        }

        /// <summary>
        /// Returns all available links in system
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public static DataTable GetWebsiteLinkList(DbConnection connection, DbTransaction transaction)
        {
            return DbFactory.GetDataTable("spLinks_Select", connection, transaction, new Dictionary<string, object> { { "ClientID", Settings.ClientID } });            
        }
    }
}
