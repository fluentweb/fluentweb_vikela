﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentContent.Entities;
using System.Data.Common;
using DataAccess.Base;
using DataAccess;

namespace FluentContent.DAC
{
    public class DAC_EntityType
    {
        public static List<EntityType> Get(DbConnection connection, DbTransaction transaction)
        {
            List<EntityType> items = new List<EntityType>();
            DbReader r = null;
            try
            {
                r = new DbReader("spEntityType_Select", connection, transaction, new Dictionary<string, object> 
                {
                    { "intClientID", Settings.ClientID }                    
                });

                while (r.Read())
                {
                    items.Add(ReadEntityType(r));
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return items;
        }

        public static List<EntityType> Get()
        {
            return DbFactory.Connect<List<EntityType>>(Get);
        }

        public static EntityType Get(DbConnection connection, DbTransaction transaction, int entityTypeID)
        {
            EntityType t = null;
            DbReader r = null;
            try
            {

                r = new DbReader("spEntityType_Select", connection, transaction, new Dictionary<string, object> 
                {
                     { "intClientID", Settings.ClientID },
                     { "intEntityTypeID", entityTypeID }
                });

                if (r.Read())
                {
                    t = ReadEntityType(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return t;
        }

        public static EntityType Get(int entityTypeID)
        {
            return DbFactory.Connect<EntityType, int>(Get, entityTypeID);
        }

        private static EntityType ReadEntityType(DbReader r)
        {
            return new EntityType
            {
                ID = r.Get<int>("intEntityTypeID"),
                Name = r.Get<string>("chrEntityType"),
                Width = r.Get<int>("intWidth"),
                Height = r.Get<int>("intHeight"),
                ImageWidthRestrict = r.Get<string>("chrImageWidthRestrict"),
                ImageHeightRestrict = r.Get<string>("chrImageHeightRestrict")
            };
        }
    }
}
