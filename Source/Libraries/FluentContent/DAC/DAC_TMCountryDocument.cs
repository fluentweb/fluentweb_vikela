﻿using DataAccess;
using DataAccess.Base;
using FluentContent.Entities;
using FluentContent.Entities.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.DAC
{
    public class DAC_TMCountryDocument
    {
        public static DataTable GetTMCountryDocument()
        {
            return DbFactory.Connect<DataTable>(GetTMCountryDocument);
        }

        public static DataTable GetTMCountryDocument(DbConnection connection, DbTransaction transaction)
        {
            return DbFactory.GetDataTable("spTMtCountryDocument_Select", connection, transaction, new Dictionary<string, object> 
            {
                { "CountryDocumentID", (object)DBNull.Value }
            });
        }

        public static TMCountryDocument GetTMCountryDocument(int countryDocumentID)
        {
            return DbFactory.Connect<TMCountryDocument, int>(GetTMCountryDocument, countryDocumentID);
        }

        public static TMCountryDocument GetTMCountryDocument(DbConnection connection, DbTransaction transaction, int countryDocumentID)
        {
            TMCountryDocument item = null;
            DbReader r = null;

            try
            {
                r = new DbReader("spTMtCountryDocument_Select", connection, transaction, new Dictionary<string, object>
                {
                    { "CountryDocumentID", countryDocumentID }
                });

                if (r.Read())
                {
                    item = new TMCountryDocument(r);
                }

                if (r != null) r.Close();
            }
            finally
            {
                if (r != null) r.Close();
            }

            return item;
        }

        public static TMCountryDocument Insert(TMCountryDocument tmcd)
        {
            return DbFactory.Connect<TMCountryDocument, TMCountryDocument>(Insert, tmcd);
        }

        public static TMCountryDocument Insert(DbConnection connection, DbTransaction transaction, TMCountryDocument tmcd)
        {
            DbReader r = null;

            try
            {
                r = new DbReader("spTMtCountryDocument_Insert", connection, transaction, new Dictionary<string, object>
                {
                    {"CountryID",tmcd.CountryID},
                    {"ConventionPriorityClaimed",tmcd.ConventionPriorityClaimed},
                    {"DocumentDesc",tmcd.DocumentDesc}

                });

                if (r.Read())
                {
                    tmcd = new TMCountryDocument(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return tmcd;
        }

        public static TMCountryDocument Update(TMCountryDocument tmcd)
        {
            return DbFactory.Connect<TMCountryDocument, TMCountryDocument>(Update, tmcd);
        }

        public static TMCountryDocument Update(DbConnection connection, DbTransaction transaction, TMCountryDocument tmcd)
        {
            DbReader r = null;

            try
            {
                r = new DbReader("spTMtCountryDocument_Update", connection, transaction, new Dictionary<string, object>
                {
                    {"CountryDocumentID",tmcd.CountryDocumentID},
                    {"CountryID",tmcd.CountryID},
                    {"ConventionPriorityClaimed",tmcd.ConventionPriorityClaimed},
                    {"DocumentDesc",tmcd.DocumentDesc}

                });

                if (r.Read())
                {
                    tmcd = new TMCountryDocument(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return tmcd;
        }
        public static bool Delete(TMCountryDocument tmcd)
        {
            return DbFactory.Connect<bool, TMCountryDocument>(Delete, tmcd);
        }

        public static bool Delete(DbConnection connection, DbTransaction transaction, TMCountryDocument tmcd)
        {
            return DbFactory.ExecuteNonQuery("spTMtCountryDocument_Delete", connection, transaction, new Dictionary<string, object> 
            {
                { "CountryDocumentID", tmcd.CountryDocumentID }
            }) > 0;
        }

        public static TMCountryDocument ReadProperty(DbReader r)
        {
            return new TMCountryDocument(r);
        }


    }
}
