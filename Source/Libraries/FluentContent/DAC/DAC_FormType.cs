﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentContent.Entities;
using DataAccess;
using System.Data.Common;
using DataAccess.Base;

namespace FluentContent.DAC
{
    public class DAC_FormType
    {
        // BY CLIENT ID

        public static List<FormType> GetFormsTypes()
        {
            return DbFactory.Connect<List<FormType>>(GetFormsTypes);
        }

        public static List<FormType> GetFormsTypes(DbConnection connection, DbTransaction transaction)
        {
            List<FormType> items = new List<FormType>();
            DbReader r = null;

            try
            {
                r = new DbReader("spFormTypeByClientID_Select", connection, transaction, new Dictionary<string, object> 
                {
                    { "intClientID", Settings.ClientID }
                });

                while (r.Read())
                {
                    items.Add(ReadFormType(r));
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return items;
        }

        // HELPERS

        private static FormType ReadFormType(DbReader r)
        {
            return new FormType
            {
                ID = r.Get<int>("intFormTypeID"),
                ClientID = r.Get<int>("intClientID"),
                Description = r.Get<string>("chrFormTypeDesc"),
                Name = r.Get<string>("chrFormType")
            };
        }

    }
}
