﻿using DataAccess;
using DataAccess.Base;
using FluentContent.Entities;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.DAC
{
    public class DAC_PropertyValuation
    {

        public static List<PropertyValuation> GetAll()
        {
            return DbFactory.Connect<List<PropertyValuation>>(GetAll);
        }

        public static List<PropertyValuation> GetAll(DbConnection connection, DbTransaction transaction)
        {
            List<PropertyValuation> items = new List<PropertyValuation>();
            DbReader r = null;

            try
            {
                r = new DbReader("dbo.sptPropertyValuationByClient_Select", connection, transaction, new Dictionary<string, object>
                {
                    {"intClientID", Settings.ClientID }
                });

                while (r.Read())
                {
                    items.Add(ReadPropertyValuation(r));
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return items;
        }


        public static PropertyValuation Get(int valuationID)
        {
            return DbFactory.Connect<PropertyValuation, int>(Get, valuationID);
        }

        public static PropertyValuation Get(DbConnection connection, DbTransaction transaction, int valuationID)
        {
            PropertyValuation item = null;
            DbReader r = null;

            try
            {
                r = new DbReader("sptPropertyValuation_Select", connection, transaction, new Dictionary<string, object>
                {
                    {"intPropertyValuationID", valuationID }
                });

                if (r.Read())
                {
                    item = ReadPropertyValuation(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return item;
        }


        public static PropertyValuation Insert(PropertyValuation valuation)
        {
            return DbFactory.Connect<PropertyValuation, PropertyValuation>(Insert, valuation);
        }

        public static PropertyValuation Insert(DbConnection connection, DbTransaction transaction, PropertyValuation valuation)
        {
            DbReader r = null;

            try
            {
                r = new DbReader("sptPropertyValuation_Insert", connection, transaction, new Dictionary<string, object>
                {
                    { "intTitleID",valuation.Title.ID},
                    { "chrFirstName",valuation.FirstName},
                    { "chrSurname",valuation.Surname},
                    { "chrAddress1",valuation.Address1},
                    { "chrAddress2",valuation.Address2},
                    { "chrAddress3",valuation.Address3},
                    { "chrAddressPostcode",valuation.AddressPostcode},
                    { "intCountryID",valuation.Country.ID},
                    { "intCountyID",valuation.County.ID},
                    { "chrPhoneHome",valuation.PhoneHome},
                    { "chrPhoneMobile",valuation.PhoneMobile},
                    { "chrPhoneWork",valuation.PhoneWork},
                    { "chrEmailAddress",valuation.EmailAddress},
                    { "bitWantsToSell",valuation.WantsToSell},
                    { "bitWantsToRent",valuation.WantsToRent},
                    { "intPropertyTypeItemID",valuation.PropertyTypeItemID},
                    { "intBedrooms",valuation.Bedrooms},
                    { "intBathrooms",valuation.Bathrooms},
                    { "chrAdditionalInformation",valuation.AdditionalInformation},
                    { "numPropertyValue",valuation.PropertyValue},
                    { "chrNotes",valuation.Notes},
                    { "intClientID", Settings.ClientID}
                });

                if (r.Read())
                {
                    valuation = ReadPropertyValuation(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return valuation;
        }


        public static PropertyValuation Update(PropertyValuation valuation)
        {
            return DbFactory.Connect<PropertyValuation, PropertyValuation>(Update, valuation);
        }

        public static PropertyValuation Update(DbConnection connection, DbTransaction transaction, PropertyValuation valuation)
        {
            DbReader r = null;

            try
            {
                r = new DbReader("sptPropertyValuation_Update", connection, transaction, new Dictionary<string, object>
                {
                    { "intTitleID",valuation.Title.ID},
                    { "chrFirstName",valuation.FirstName},
                    { "chrSurname",valuation.Surname},
                    { "chrAddress1",valuation.Address1},
                    { "chrAddress2",valuation.Address2},
                    { "chrAddress3",valuation.Address3},
                    { "chrAddressPostcode",valuation.AddressPostcode},
                    { "intCountryID",valuation.Country.ID},
                    { "intCountyID",valuation.County.ID},
                    { "chrPhoneHome",valuation.PhoneHome},
                    { "chrPhoneMobile",valuation.PhoneMobile},
                    { "chrPhoneWork",valuation.PhoneWork},
                    { "chrEmailAddress",valuation.EmailAddress},
                    { "bitWantsToSell",valuation.WantsToSell},
                    { "bitWantsToRent",valuation.WantsToRent},
                    { "intPropertyTypeItemID",valuation.PropertyTypeItemID},
                    { "intBedrooms",valuation.Bedrooms},
                    { "intBathrooms",valuation.Bathrooms},
                    { "chrAdditionalInformation",valuation.AdditionalInformation},
                    { "numPropertyValue",valuation.PropertyValue},
                    { "chrNotes",valuation.Notes},
                    { "intClientID", Settings.ClientID},
                    { "intPropertyValuationID", valuation.PropertyValuationID}
                });

                if (r.Read())
                {
                    valuation = ReadPropertyValuation(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return valuation;
        }


        public static bool Delete(int valuationID)
        {
            return DbFactory.Connect<bool, int>(Delete, valuationID);
        }

        public static bool Delete(DbConnection connection, DbTransaction transaction, int valuationID)
        {
            return DbFactory.ExecuteNonQuery("sptPropertyValuation_Delete", connection, transaction, new Dictionary<string, object> 
            {
                {"intPropertyValuationID", valuationID}
            }) > 0;
        }


        // Helpers

        public static PropertyValuation ReadPropertyValuation(DbReader r)
        {
            return new PropertyValuation
            {
                PropertyValuationID = r.Get<int>("intPropertyValuationID"),
                Title = new Title { ID = r.Get<int>("intTitleID"), Description = r.Get<string>("chrTitle") },
                FirstName = r.Get<string>("chrFirstName"),
                Surname = r.Get<string>("chrSurname"),
                Address1 = r.Get<string>("chrAddress1"),
                Address2 = r.Get<string>("chrAddress2"),
                Address3 = r.Get<string>("chrAddress3"),
                AddressPostcode = r.Get<string>("chrAddressPostcode"),
                Country = new Country { ID = r.Get<int>("intCountryID"), Name = r.Get<string>("chrCountry") },
                County = new County { ID = r.Get<int>("intCountyID"), CountyName = r.Get<string>("chrCountyName") },
                PhoneHome = r.Get<string>("chrPhoneHome"),
                PhoneMobile = r.Get<string>("chrPhoneMobile"),
                PhoneWork = r.Get<string>("chrPhoneWork"),
                EmailAddress = r.Get<string>("chrEmailAddress"),
                WantsToSell = r.Get<bool>("bitWantsToSell"),
                WantsToRent = r.Get<bool>("bitWantsToRent"),
                PropertyTypeItemID = r.Get<int>("intPropertyTypeItemID"),
                Bedrooms = r.Get<int>("intBedrooms"),
                Bathrooms = r.Get<int>("intBathrooms"),
                AdditionalInformation = r.Get<string>("chrAdditionalInformation"),
                PropertyValue = r.Get<decimal>("numPropertyValue"),
                Notes = r.Get<string>("chrNotes"),
                Created = r.Get<DateTime>("datCreated"),
                PropertySubscribedUserID = r.Get<long>("intPropertySubscribedUserID"),
                ClientID = r.Get<int>("intClientID"),
                PropertyType = r.Get<string>("chrPropertyType")
            };

        }
    }
}
