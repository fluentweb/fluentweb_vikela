﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DataAccess;
using System.Data.Common;

namespace FluentContent.DAC
{
    public class DAC_ItemTypeFormTypeRelation
    {
        public static DataTable GetItemTypeRelatedFormTypes(int itemTypeID) 
        {
            return DbFactory.Connect<DataTable, int>(GetItemTypeRelatedFormTypes, itemTypeID);
        }

        public static DataTable GetItemTypeRelatedFormTypes(DbConnection connection, DbTransaction transaction, int itemTypeID)
        {
            return DbFactory.GetDataTable("spRelatedFormType_Select", connection, transaction, new Dictionary<string, object> 
            {
                { "ItemTypeID", itemTypeID }
            });
        }
    }
}
