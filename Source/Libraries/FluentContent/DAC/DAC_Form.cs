﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentContent.Entities;
using DataAccess;
using System.Data.Common;
using DataAccess.Base;

namespace FluentContent.DAC
{
    public class DAC_Form
    {
        // BY TYPE ID

        public static List<Form> GetFormsByTypeID(int intFormTypeID)
        {
            return DbFactory.Connect<List<Form>, int>(GetFormsByTypeID, intFormTypeID);
        }

        public static List<Form> GetFormsByTypeID(DbConnection connection, DbTransaction transaction, int intFormTypeID)
        {
            List<Form> items = new List<Form>();
            DbReader r = null;

            try
            {
                r = new DbReader("spFormByTypeID_Select", connection, transaction, new Dictionary<string, object> 
                {
                    { "intFormTypeID", intFormTypeID } 
                });

                while (r.Read())
                {
                    items.Add(ReadForm(r));
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return items;
        }

        // BY ID
        public static Form GetFormByID(int intFormID)
        {
            return DbFactory.Connect<Form, int>(GetFormByID, intFormID);
        }

        public static Form GetFormByID(DbConnection connection, DbTransaction transaction, int intFormID)
        {
            Form item = null;
            DbReader r = null;

            try
            {
                r = new DbReader("spFormByID_Select", connection, transaction, new Dictionary<string, object> 
                {
                    { "intFormID", intFormID } 
                });

                if (r.Read())
                {
                    item = ReadForm(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return item;
        }

        // DELETE
        public static bool Delete(int formID)
        {
            return DbFactory.Connect<bool, int>(Delete, formID);
        }

        public static bool Delete(DbConnection connection, DbTransaction transaction, int formID)
        {
            return DbFactory.ExecuteNonQuery("spFormByID_Delete", connection, transaction, new Dictionary<string, object> 
            {
                {"intFormID", formID }
            }) > 0;
        }

        // INSERT

        public static int Insert(Form f)
        {
            return DbFactory.Connect<int, Form>(Insert, f);
        }

        public static int Insert(DbConnection connection, DbTransaction transaction, Form f)
        {
            return DbFactory.ExecuteScalar<int>("spForm_Insert", connection, transaction, new Dictionary<string, object> 
            {
                { "chrForm", f.Name },
                { "chrFormDesc", f.Description },
                { "intFormTypeID", f.Type.ID }
            });
        }


        public static int CloneForm(int formID) 
        {
            return DbFactory.Connect<int, int>(CloneForm, formID);
        }

        public static int CloneForm(DbConnection connection, DbTransaction transaction, int formID)
        {
            return DbFactory.ExecuteNonQuery("spFormWithObjects_Clone", connection, transaction, new Dictionary<string, object> 
            {
                {"FormID", formID }
            });
        }

        public static bool Update(Form f)
        {
            return DbFactory.Connect<bool, Form>(Update, f);
        }

        public static bool Update(DbConnection connection, DbTransaction transaction, Form f)
        {
            return DbFactory.ExecuteNonQuery("spForm_Update", connection, transaction, new Dictionary<string, object> 
            {
                { "chrForm", f.Name },
                { "chrFormDesc", f.Description },
                { "intFormTypeID", f.Type.ID },
                { "intFormID", f.ID }
            }) > 0;
        }

        // HELPERS
        private static Form ReadForm(DbReader r)
        {
            return new Form
            {
                Description = r.Get<string>("chrFormDesc"),
                ID = r.Get<int>("intFormID"),
                Name = r.Get<string>("chrForm"),
                Type = new FormType
                {
                    ID = r.Get<int>("intFormTypeID"),
                    Name = r.Get<string>("chrFormType")
                }
            };
        }
    }
}
