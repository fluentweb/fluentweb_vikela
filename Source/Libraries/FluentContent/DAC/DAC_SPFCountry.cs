﻿using DataAccess;
using FluentContent.Entities.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.DAC
{
    public class DAC_SPFCountry
    {
        public static DataTable GetCountryByCountry(int countryId = 0)
        {
            return DbFactory.Connect<DataTable, int>(GetCountryByCountry, countryId);
        }

        public static DataTable GetCountryByCountry(DbConnection connection, DbTransaction transaction, int countryId)
        {
            return DbFactory.GetDataTable("spSPFCountryByCountry_Select", connection, transaction, new Dictionary<string, object> 
            {
                { "CountryID", countryId == 0 ? (object)DBNull.Value : countryId },
            });
        }

        public static DataTable GetCountryByCountryState(int countryId = 0)
        {
            return DbFactory.Connect<DataTable, int>(GetCountryByCountryState, countryId);
        }

        public static DataTable GetCountryByCountryState(DbConnection connection, DbTransaction transaction, int countryId)
        {
            return DbFactory.GetDataTable("spSPFCountryByCountryState_Select", connection, transaction, new Dictionary<string, object> 
            {
                { "ParentCountryID", countryId == 0 ? (object)DBNull.Value : countryId },
            });
        }

    }
}
