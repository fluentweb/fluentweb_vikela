﻿using DataAccess;
using DataAccess.Base;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.Helpers
{
    public class MenuVisibility
    {
        
        public static List<string> GetAll()
        {
            return DbFactory.Connect<List<string>>(GetAll);
        }

        public static List<string> GetAll(DbConnection connection, DbTransaction transaction)
        {
            List<string> items = new List<string>();
            DbReader r = null;

            try
            {
                r = new DbReader("spMenuItemsHidden_Select", connection, transaction, new Dictionary<string, object>
                {
                    { "intClientID", Settings.ClientID }
                });

                while (r.Read())
                {
                    items.Add(r.Get<string>("chrMenuItemKey"));
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return items;
        }        
    }
}
