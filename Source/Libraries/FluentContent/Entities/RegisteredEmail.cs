﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentContent.Entities.Enums;

namespace FluentContent.Entities
{

    public class RegisteredEmail
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public char Gender { get; set; }
        public string ContactNumber { get; set; }
        public string CompanyName { get; set; }
        public bool ClientEmailSubscribtion { get; set; }
        public bool ThirdPartyEmailSubscription { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
    }
}
