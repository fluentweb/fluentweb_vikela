﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.Entities
{
    public class PropertyAsset
    {
        public int ID { get; set; }
        public string Description { get; set; }
        public int AssetItemID { get; set; }
        public string AssetTypeName { get; set; }
        public double Width { get; set; }
        public double Length { get; set; }
        public int FloorNumber { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public int PropertyID { get; set; }
        public double Size { get; set; }
    }
}
