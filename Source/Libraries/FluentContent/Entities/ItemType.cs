﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FluentContent.Entities
{
    public class ItemType
    {
        public int ID { get; set; }
        public int ClientID { get; set; }
        public string ItemTypeName { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }

        public ItemType()
        {
            ID = -1;
        }
    }
}
