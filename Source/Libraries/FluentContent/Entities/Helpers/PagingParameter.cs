﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.Entities.Helpers
{
    public class PagingParameter
    {
        public int PageNumber { get; set; }
        public int RowsPerPage { get; set; }
        public string SortBy { get; set; }
        public string SortDirection { get; set; }
    }
}
