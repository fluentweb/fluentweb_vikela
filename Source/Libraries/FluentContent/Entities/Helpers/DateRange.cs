﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FluentContent.Entities.Helpers
{
    public class DateRange
    {
        public DateTime From { get; set; }
        public DateTime To { get; set; }

        public DateRange()
        {
            From = DateTime.MinValue;
            To = DateTime.MinValue;
        }
    }
}
