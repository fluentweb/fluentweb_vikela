﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace FluentContent.Entities
{
    public class PropertyImage
    {
        public int ID { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int PropertyID { get; set; }

        public string GUID { get; set; }
        public string Filename { get; set; }
        public string Description { get; set; }

        public bool IsLeadImage { get; set; }

        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }

        public string ImageURL
        {
            get
            {
                return "/properties/" + Filename;
            }
        }

    }
}
