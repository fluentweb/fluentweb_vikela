﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.Entities
{
    public class PropertyDiaryEntry
    {

        #region Declare variables

        private Int64 _intDiaryEntry;
        private DateTime _datEntry;
        private string _chrUser;
        private string _chrNotes;
        private int _intPropertyID;


        #endregion

        #region Declare Properties

        public Int64 intDiaryEntry
        {
            get { return _intDiaryEntry; }
            set { _intDiaryEntry = value; }
        }

        public DateTime datEntry
        {
            get { return _datEntry; }
            set { _datEntry = value; }
        }

        public string chrUser
        {
            get { return _chrUser; }
            set { _chrUser = value; }
        }

        public string chrNotes
        {
            get { return _chrNotes; }
            set { _chrNotes = value; }
        }

        public int intPropertyID
        {
            get { return _intPropertyID; }
            set { _intPropertyID = value; }
        }

        #endregion

    }
}
