﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FluentContent.Entities
{
    public class ContentPage
    {
        public int ID { get; set; }
        public int ClientID { get; set; }
        public string PageName { get; set; }
        public string Title { get; set; }
        public string Keywords { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
        public string PageFullURL { get; set; }
        public string TemplateName { get; set; }
        public bool UrlEditable { get; set; }
        // Constructors

        public ContentPage() : this(-1, -1) { }
        public ContentPage(int id, int clientID) { ID = id; ClientID = clientID; }        
    }
}
