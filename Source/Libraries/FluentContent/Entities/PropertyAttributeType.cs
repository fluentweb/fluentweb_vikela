﻿using DataAccess.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.Entities
{
    public class PropertyAttributeType
    {
        public int ID { get; set; }
        public string DataType { get; set; }
        public bool Visible { get; set; }
        public string Title { get; set; }

        public PropertyAttributeType(DbReader r)       
        {
            this.ID = r.Get<int>("intItemID");
            this.DataType = r.Get<string>("chrDataType");
            this.Visible = r.Get<bool>("bitCheckbox1");
            this.Title = r.Get<string>("chrItemTitle");
        }


    }
}
