﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FluentContent.Entities.Enums
{
    public class CMSAccessLevel
    {
        public const char Full = 'F';
        public const char Entity = 'E';
        public const char None = 'N';
        public const char PropertyEditor = 'P';
    }
}
