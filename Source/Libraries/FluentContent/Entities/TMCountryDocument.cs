﻿using DataAccess.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.Entities
{
    public class TMCountryDocument
    {
        public int CountryDocumentID { get; set; }
        public int CountryID { get; set; }
        public bool ConventionPriorityClaimed { get; set; }
        public string DocumentDesc { get; set; }

        public TMCountryDocument()
        {
        }

        public TMCountryDocument(DbReader r)
        {
            this.CountryDocumentID = r.Get<int>("intCountryDocumentID");
            this.CountryID = r.Get<int>("intCountryID");
            this.ConventionPriorityClaimed = r.Get<bool>("bitConventionPriorityClaimed");
            this.DocumentDesc = r.Get<string>("chrDocumentDesc");
        }

    }
}
