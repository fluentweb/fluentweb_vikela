﻿using FluentContent.Entities;
using FluentContent.Entities.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FluentContent.Entities
{
    public class WebUser
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Title Title { get; set; }

        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        internal string ClearTextPassword { get; set; }
        public string Salt { get; set; }

        public string CompanyName { get; set; }
        public string Landline { get; set; }
        public string Mobile { get; set; }
        public string ContactNo { get; set; }
        public string ContactPerson { get; set; }
        public string Gender { get; set; }
        public DateTime DOB { get; set; }
        public List<WebUserAddress> Addresses { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
        public bool Locked { get; set; }
        public bool AcccountConfirmed { get; set; }

        public bool Option1 { get; set; }
        public bool Option2 { get; set; }
        public bool Option3 { get; set; }

        public string Custom1 { get; set; }
        public string Custom2 { get; set; }
        public string Custom3 { get; set; }

        public WebUserAddress GetDetfaultAddress()
        {
            if (this.Addresses != null)
                return this.Addresses.Find(i => i.IsDefault);
            return null;
        }

        public WebUser()
        {
            this.Addresses = new List<WebUserAddress>();
        }

        public WebUser(WebUser u)
        {
            this.AcccountConfirmed = u.AcccountConfirmed;
            this.Addresses = u.Addresses;
            this.ClearTextPassword = u.ClearTextPassword;
            this.CompanyName = u.CompanyName;
            this.ContactNo = u.ContactNo;
            this.ContactPerson = u.ContactPerson;
            this.Custom1 = u.Custom1;
            this.Custom2 = u.Custom2;
            this.Custom3 = u.Custom3;
            this.DateCreated = u.DateCreated;
            this.DateUpdated = u.DateUpdated;
            this.DOB = u.DOB;
            this.Email = u.Email;
            this.FirstName = u.FirstName;
            this.Gender = u.Gender;
            this.ID = u.ID;
            this.Landline = u.Landline;
            this.LastName = u.LastName;
            this.Locked = u.Locked;
            this.Mobile = u.Mobile;
            this.Option1 = u.Option1;
            this.Option2 = u.Option2;
            this.Option3 = u.Option3;
            this.Password = u.Password;
            this.Salt = u.Salt;
            this.Title = u.Title;
            this.Username = u.Username;
        }
    }
}
