﻿using DataAccess.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.Entities
{
    public class PropertySubscriberAlert
    {
        public int ID { get; set; }

        public int ActivatedByCMSUserID { get; set; }

        public long PropertySubscriberUserID { get; set; }

        public int PropertyID { get; set; }

        public DateTime Sent { get; set; }

        public PropertySubscriberAlert() { }

        public int EmailAlertItemID { get; set; }

        public PropertySubscriberAlert(DbReader r)
        {
            this.ID = r.Get<int>("intPropertySubscriberAlertID");
            this.ActivatedByCMSUserID = r.Get<int>("intActivatedByCMSUserID");
            this.PropertySubscriberUserID = r.Get<long>("intPropertySubscriberUserID");
            this.PropertyID = r.Get<int>("intPropertyID");
            this.Sent = r.Get<DateTime>("datSent");
            this.EmailAlertItemID = r.Get<int>("intEmailTypeItemID");
        }
    }
}
