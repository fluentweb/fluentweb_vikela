﻿using DataAccess.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.Entities
{
    public class TMCostType
    {
        public int CostTypeID { get; set; }
        public string CostTypeCode { get; set; }
        public string CostType { get; set; }
        public bool PerMark { get; set; }
        public bool PerCountry { get; set; }
        public bool PerClass { get; set; }
        public bool PerProduct { get; set; }
        public bool PerState { get; set; }

        public TMCostType()
        {
        }
        public TMCostType(DbReader r)
        {
            this.CostTypeID = r.Get<int>("intCostTypeID");
            this.CostTypeCode = r.Get<string>("chrCostTypeCode");
            this.CostType = r.Get<string>("chrCostType");
            this.PerMark = r.Get<bool>("bitPerMark");
            this.PerCountry = r.Get<bool>("bitPerCountry");
            this.PerClass = r.Get<bool>("bitPerClass");
            this.PerProduct = r.Get<bool>("bitPerProduct");
            this.PerState = r.Get<bool>("bitPerState");
        }
    }
}
