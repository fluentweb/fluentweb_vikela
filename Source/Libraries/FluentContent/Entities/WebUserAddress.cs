﻿using FluentContent.Entities;
using FluentContent.Entities.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FluentContent.Entities
{
    public class WebUserAddress 
    {
        public int ID { get; set; }
        public string Recipient { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string TownCity { get; set; }
        public string County { get; set; }
        public string PostCode { get; set; }
        public Country Country { get; set; }
        public int WebUserID { get; set; }

        public bool IsDefault { get; set; }

        public DateTime DateCreated{ get; set; }
        public DateTime DateUpdated { get; set; }
    }
}
