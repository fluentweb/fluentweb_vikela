﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FluentContent.Entities
{
    public class ClientReport
    {
        public int ID { get; set; }
        public int ClientID { get; set; }
        public int EntityID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string StoredProcedure { get; set; }
        public bool IsSubReport { get; set; }
        public string Parameter1 { get; set; }
        public string Parameter2 { get; set; }
        public string Parameter3 { get; set; }
        public string Parameter4 { get; set; }
        public string Parameter5 { get; set; }
        public string Parameter6 { get; set; }
        
        public bool RequiresDateFields { get; set; }        
        public bool RequiresUserFilter { get; set; }
    }
}
