﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FluentContent.Entities
{
    public class Client
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
