﻿using DataAccess.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.Entities
{
   public class TMCountryAdditionalRequirementChoiceType
    {
        public string QuestionType { get; set; }
        public string QuestionTypeDesc { get; set; }

        public TMCountryAdditionalRequirementChoiceType()
        {
        }
        public TMCountryAdditionalRequirementChoiceType(DbReader r)
        {
            this.QuestionType = r.Get<string>("chrQuestionType");
            this.QuestionTypeDesc = r.Get<string>("chrQuestionTypeDesc");
        }
    }
}
