﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentContent.Entities.Base;
using FluentContent.DAC;
using System.Data.Common;
using FluentContent.Entities;

namespace FluentContent.Entities
{
    public class CMSUser
    {
        public int ID { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public string Email { get; set; }

        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int DepartmentID { get; set; }
        public int EntityID { get; set; }
        public UserRole Role { get; set; }
        public string DepartmentName { get; set; }
        public bool Locked { get; set; }

        public bool Preference1 { get; set; }
        public string EntityName { get; set; }

        /// <summary>
        /// Phase 1 of single sign on, ClientID to filter users in management screen
        /// </summary>
        public int ClientID { get; set; }

        public CMSUser()
        {
            ID = -1;
        }

        public int Save(DbConnection connection, DbTransaction transaction)
        {
            if (ID == -1)
                return DAC_CMSUser.Insert(connection, transaction, this); // insert
            return DAC_CMSUser.Update(connection, transaction, this) ? this.ID : -1; // update 
        }
    }
}
