﻿using DataAccess.Base;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.Entities
{
    public class PropertyAttribute
    {        
        public int ID { get; set; }

        public int AttributeTypeItemID { get; set; }

        public int PropertyID { get; set; }

        public string Value { get; set; }

        public string AttributeTypeItemName { get; set; }
        public string AttributeTypeItemDataType { get; set; }

        public DataTable Item { get; set; }
    }

}
