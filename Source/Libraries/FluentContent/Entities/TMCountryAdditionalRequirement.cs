﻿using DataAccess.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.Entities
{
    public class TMCountryAdditionalRequirement
    {
        public int CountryAdditionalRequirementID { get; set; }
        public int CountryID { get; set; }
        public string RequirementQuestion { get; set; }
        public string QuestionType { get; set; }
        public TMCountryAdditionalRequirement()
        {
        }

        public TMCountryAdditionalRequirement(DbReader r)
        {
            this.CountryAdditionalRequirementID = r.Get<int>("intCountryAdditionalRequirementID");
            this.CountryID = r.Get<int>("intCountryID");
            this.RequirementQuestion = r.Get<string>("chrRequirementQuestion");
            this.QuestionType = r.Get<string>("chrQuestionType");
        }

    }
}
