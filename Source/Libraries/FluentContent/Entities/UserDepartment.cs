﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FluentContent.Entities
{
    public class UserDepartment
    {
        public int ID { get; set; }        
        public string Name { get; set; }
        public int ParentDepartmentID { get; set; }
        public string ParentName { get; set; }
        public int EntityID { get; set; }
        public string EntityName { get; set; }
        public int ClientID { get; set; }
    }
}
