﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace FluentContent.Entities
{
    [DataContract]
    public class FormObjectType
    {
        [DataMember(Name = "id")]
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsContainer { get; set; }
        public int FormTypeID { get; set; }
        public bool Text1Required { get; set; }
        public bool Text2Required { get; set; }
        public bool Text3Required { get; set; }
        public bool DataRequired { get; set; }

        public string Text1Label { get; set; }
        public string Text2Label { get; set; }
        public string Text3Label { get; set; }
        public string DataLabel { get; set; }
    }
}
