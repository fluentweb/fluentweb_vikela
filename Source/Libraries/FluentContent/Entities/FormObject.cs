﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace FluentContent.Entities
{
    [DataContract]
    public class FormObject
    {
        [DataMember(Name = "id")]
        public int ID { get; set; }

        public int Sequence { get; set; }

        [DataMember(Name = "text_1")]
        public string Text1 { get; set; }

        [DataMember(Name = "text_2")]
        public string Text2 { get; set; }

        [DataMember(Name = "text_3")]
        public string Text3 { get; set; }

        [DataMember(Name = "data_1")]
        public string Data { get; set; }

        public int ParentObjectID { get; set; }

        [DataMember(Name = "type")]
        public FormObjectType Type { get; set; }
        public int FormID { get; set; }

        [DataMember(Name = "children")]
        public List<FormObject> Children { get; set; }
        
    }
}
