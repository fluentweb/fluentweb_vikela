﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.Entities
{
    public class County
    {
        public int ID { get; set; }
        public string CountyName { get; set; }
        public int CountryID { get; set; }
    }
}
