﻿using DataAccess.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.Entities
{
    public class TMCountryState
    {
        public int CountryID { get; set; }

        public int MemberCountryID { get; set; }

        public TMCountryState()
        {

        }

        public TMCountryState(DbReader r)
        {
            this.CountryID = r.Get<int>("intCountryID");
            this.MemberCountryID = r.Get<int>("intMemberCountryID");
        }
    }
}
