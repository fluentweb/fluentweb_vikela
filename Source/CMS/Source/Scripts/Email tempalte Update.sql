-- pass required client ID or set to -1 (if facility to reuse among clients is in place)
INSERT INTO tItemType 
	(intClientID, chrItemTypeChr, bitText1Required, bitText2Required, 
		bitText3Required, bitText4Required,bitText5Required, bitText11Required, bitText6Required,
		chrTitleName, chrText1Name, chrText2Name, chrText3Name, 
		chrText4Name, chrText5Name, chrText11Name, chrText6Name)
SELECT 4, 'Email templates', 1, 1, 1, 1,1, 1,1,  'Template Name', 'Recipient (Optional)', 'From', 'From Display Name','CC', 'BCC', 'Template Text', 'Subject';
	
	SELECT * FROM tItemType
-- update video type to include thumbnail

UPDATE tItemType 
SET bitImageRequired = 1,
	chrItemImageName = 'Video Thumbnail',
	intWidth = 140,
	intHeight = 87
WHERE intClientID = 4 
AND intItemTypeID = 17





USE [flcontent]
GO
/****** Object:  StoredProcedure [dbo].[spItemByURL_Select]    Script Date: 10/15/2013 08:51:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ====================================================
-- Author:        David Scott
-- Create date: 15 Oct 2013
-- Description:   SP to select an Item by it's Title
-- ====================================================
CREATE PROCEDURE spItemByTitle_Select
      -- Add the parameters for the stored procedure here
      @Title VARCHAR(250),
      @Client INT,
      @ItemType VARCHAR(150)
AS
BEGIN
      -- SET NOCOUNT ON added to prevent extra result sets from
      -- interfering with SELECT statements.
      SET NOCOUNT ON;
 
    -- Insert statements for procedure here
      SELECT
            tItem.*
      FROM
            tItem
      INNER JOIN
            tItemType
            ON tItemType.intItemTypeID = tItem.intItemTypeID
      WHERE
            tItemType.intClientID = @Client
      AND
            tItem.bitEnabled = 1
      AND
            tItem.chrItemTitle = @Title
      AND 
			tItemType.chrItemTypeChr = @ItemType
 
 
END