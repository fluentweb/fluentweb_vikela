USE [flcontent]

-- TODO: remove foreign key on tUserItem table to be able to drop tUser table
DROP TABLE tUser

-- inserts default roles into the role table
GO
/****** Object:  Table [dbo].[tUserRole]    Script Date: 01/31/2013 16:20:40 ******/
SET IDENTITY_INSERT [dbo].[tUserRole] ON
INSERT [dbo].[tUserRole] ([intRoleID], [intClientID], [chrRoleName], [chrRoleDescription], [chrCMSAccessLevel], [intRoleHierarchy], [bitEntityRequired]) VALUES (1, NULL, N'Super Administrator', N'FluentWeb administrator user role', N'F', 1, 0)
INSERT [dbo].[tUserRole] ([intRoleID], [intClientID], [chrRoleName], [chrRoleDescription], [chrCMSAccessLevel], [intRoleHierarchy], [bitEntityRequired]) VALUES (2, 4, N'ELG Administrator', N'The Learning Grove administrator user role', N'F', 2, 0)
INSERT [dbo].[tUserRole] ([intRoleID], [intClientID], [chrRoleName], [chrRoleDescription], [chrCMSAccessLevel], [intRoleHierarchy], [bitEntityRequired]) VALUES (3, 4, N'University Administrator', N'The university adnimistrator user role', N'E', 3, 1)
INSERT [dbo].[tUserRole] ([intRoleID], [intClientID], [chrRoleName], [chrRoleDescription], [chrCMSAccessLevel], [intRoleHierarchy], [bitEntityRequired]) VALUES (4, 4, N'Trainee', N'The trainee user role', N'N', 4, 1)
SET IDENTITY_INSERT [dbo].[tUserRole] OFF



