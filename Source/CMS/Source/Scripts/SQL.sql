USE flcontent
DROP TABLE tPageTextType;
DROP TABLE tPageImageType;

DROP TABLE tItemImage;
DROP TABLE tItemText;
DROP TABLE tItemImageType;
DROP TABLE tItemTextType;

---------------------------------------------
--Set FullText on tPage (chrPageTitle), tPageText (chrPageText), tItem (title and all text columns)


-- add email templates
----------------------------------------------------------
USE [flcontent]
GO
/****** Object:  Table [dbo].[tEmailTemplate]    Script Date: 01/16/2013 16:06:16 ******/
SET IDENTITY_INSERT [dbo].[tEmailTemplate] ON
INSERT [dbo].[tEmailTemplate] ([intEmailTemplateID], [chrKey], [chrSender], [chrRecipient], [chrBCC], [chrCC], [chrSubject], [chrBody], [chrDisplayName]) VALUES (1, N'cms_new_user', N'noreply@cms.com', N'', N'', N'', N'CMS Registration', N'<html>  <head>   <style type="text/css">       .clear{clear:both;}      .form { width:600px;}      .form p {float:left;width:600px; margin:0;}      p label { float:left; width:160px;}      </style>   </head>  <body>   <h1>Registration Successful</h1>  <p>   You have been successfully registered. Please use the following details to login.  </p>   <div class="form">    <p>     <strong><label>Username</label></strong>     ##USERNAME##    </p>    <p>     <strong><label>Password</label></strong>     ##PASSWORD##    </p>     </div>  </body> </html>', N'CMS Drum Cussac')
INSERT [dbo].[tEmailTemplate] ([intEmailTemplateID], [chrKey], [chrSender], [chrRecipient], [chrBCC], [chrCC], [chrSubject], [chrBody], [chrDisplayName]) VALUES (2, N'cms_generate_new_password', N'noreply@cms.com', N'', N'', N'', N'CMS Password Recovery', N'<html>  <head>   <style type="text/css">       .clear{clear:both;}      .form { width:600px;}      .form p {float:left;width:600px; margin:0;}      p label { float:left; width:160px;}      </style>   </head>  <body>   <h1>Password Recovery Successful</h1>  <p>   A new password has been generated for you  </p>   <div class="form"> <p>     <strong><label>Password</label></strong>     ##PASSWORD##    </p>     </div>  </body> </html>', N'CMS Drum Cussac')
SET IDENTITY_INSERT [dbo].[tEmailTemplate] OFF
----------------------------------------------------------


-- update image restirctions to default value x=exact m=maximum n=minimum 
USE flcontent;

UPDATE tItemType 
SET chrImage1HeightRestrict = 'x',
	chrImage1WidthRestrict = 'x',
	chrImage2HeightRestrict = 'x',
	chrImage2WidthRestrict = 'x'
	
	
USE flcontent
UPDATE tPageTemplateImageType
SET chrImageWidthRestrict ='x',
	chrImageHeightRestrict = 'x'