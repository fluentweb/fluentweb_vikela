﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Configuration;
using System.Text;
using System.Web;
using System.Web.Configuration;

namespace FluentContent
{
    /// <summary>
    /// These values are required for the FluentContent library to function 
    /// </summary>
    public class Settings
    {
        public static int ClientID { get; set; }
        public static string WebsiteRoot { get; set; }
        public static string FailSafeEmail
        {
            get
            {
                System.Configuration.Configuration c = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
                MailSettingsSectionGroup settings = (MailSettingsSectionGroup)c.GetSectionGroup("system.net/mailSettings");
                var username = settings.Smtp.Network.UserName;
                return username;
            }
        }
    }
}
