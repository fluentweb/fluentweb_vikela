﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using FluentContent.Entities;
using System.Data.Common;
using DataAccess.Base;
using DataAccess;

namespace FluentContent.DAC
{
    public class DAC_Client
    {

        public static List<Client> Get()
        {
            return DbFactory.Connect<List<Client>>(Get);
        }

        public static List<Client> Get(DbConnection connection, DbTransaction transaction)
        {
            List<Client> items = new List<Client>();
            DbReader r = null;
            try
            {
                r = new DbReader("spClients_Select", connection, transaction);
                while (r.Read())
                {
                    items.Add(ReadItem(r));
                }
            }
            finally
            {
                if (r != null) r.Close();
            }
            return items;
        }

        private static Client ReadItem(DbReader r)
        {
            return new Client 
            {
                 ID = r.Get<int>("intClientID"),
                 Name = r.Get<string>("chrClientName")
            };
        }
    }
}
