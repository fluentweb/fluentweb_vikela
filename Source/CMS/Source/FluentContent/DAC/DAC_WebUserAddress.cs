﻿using DataAccess;
using DataAccess.Base;
using FluentContent.Entities;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace FluentContent.DAC
{
    public class DAC_WebUserAddress
    {
        // DELETE
        public static bool Delete(int webUserID)
        {
            return DbFactory.Connect<bool, int>(Delete, webUserID);
        }

        public static bool Delete(DbConnection connection, DbTransaction transaction, int webUserAddressID)
        {
            return DbFactory.ExecuteNonQuery("spWebUserAddress_Delete", connection, transaction, new Dictionary<string, object> 
            {
                { "intWebUserAddressID", webUserAddressID }
            }) > 0;
        }

        // INSERT
        public static WebUserAddress Create(WebUserAddress a)
        {
            return DbFactory.Connect<WebUserAddress, WebUserAddress>(Create, a);
        }

        public static WebUserAddress Create(DbConnection connection, DbTransaction transaction, WebUserAddress a)
        {
            WebUserAddress item = null;
            DbReader r = null;

            try
            {
                r = new DbReader("spWebUserAddress_Insert", connection, transaction, new Dictionary<string, object> 
                {
                    { "intWebUserID", a.WebUserID },
                    { "chrRecipient", a.Recipient },
                    { "chrAddressLine1", a.AddressLine1 },
                    { "chrAddressLine2", a.AddressLine2 },
                    { "chrTownCity", a.TownCity },
                    { "chrCounty", a.County },
                    { "chrPostcode", a.PostCode },
                    { "intCountryID", a.Country.ID },
                    { "bitDefault",a.IsDefault }
                });

                if (r.Read())
                {
                    item = ReadWebUserAddress(r);                  
                }
            }
            finally
            {
                if (r != null) r.Close();
            }
            return item;
        }

        // UPDATE
        public static WebUserAddress Update(WebUserAddress a)
        {
            return DbFactory.Connect<WebUserAddress, WebUserAddress>(Update, a);
        }

        public static WebUserAddress Update(DbConnection connection, DbTransaction transaction, WebUserAddress a)
        {
            WebUserAddress item = null;
            DbReader r = null;

            try
            {
                r = new DbReader("spWebUserAddress_Update", connection, transaction, new Dictionary<string, object> 
                {
                    { "intWebUserAddressID", a.ID },
                    { "intWebUserID", a.WebUserID },
                    { "chrRecipient", a.Recipient },
                    { "chrAddressLine1", a.AddressLine1 },
                    { "chrAddressLine2", a.AddressLine2 },
                    { "chrTownCity", a.TownCity },
                    { "chrCounty", a.County },
                    { "chrPostcode", a.PostCode },
                    { "intCountryID", a.Country.ID },
                    { "bitDefault",a.IsDefault }
                });

                if (r.Read())
                {
                    item = ReadWebUserAddress(r);                    
                }
            }
            finally
            {
                if (r != null) r.Close();
            }
            return item;
        }

        // SELECT
        public static List<WebUserAddress> Get(int webUserID)
        {
            return DbFactory.Connect<List<WebUserAddress>, int>(Get, webUserID);
        }

        public static List<WebUserAddress> Get(DbConnection connection, DbTransaction transaction, int webUserID)
        {
            List<WebUserAddress> items = new List<WebUserAddress>();
            DbReader r = null;

            try
            {
                r = new DbReader("spWebUserAddress_Select", connection, transaction, new Dictionary<string, object> 
                {
                    { "intWebUserID", webUserID }
                });

                while (r.Read())
                {
                    items.Add(ReadWebUserAddress(r));
                }
            }
            finally
            {
                if (r != null) r.Close();
            }
            return items;
        }

        // HELPERS

        public static List<WebUserAddress> InsertUpdateAddresses(DbConnection connection, DbTransaction transaction, List<WebUserAddress> addresses)
        {
            List<WebUserAddress> items = new List<WebUserAddress>();

            foreach (var address in addresses)
            {
                if (address.ID > 0)
                    items.Add(DAC_WebUserAddress.Update(connection, transaction, address));
                else
                    items.Add(DAC_WebUserAddress.Create(connection, transaction, address));
            }

            return items;
        }

        private static WebUserAddress ReadWebUserAddress(DbReader r)
        {
            return new WebUserAddress
            {
                AddressLine1 = r.Get<string>("chrAddressLine1"),
                AddressLine2 = r.Get<string>("chrAddressLine2"),
                Country = new FluentContent.Entities.Country
                {
                    ID = r.Get<int>("intCountryID"),
                    Name = r.Get<string>("chrCountry"),
                    Code = r.Get<string>("chrCountryCode"),
                    ISO = r.Get<int>("intCountryISO"),
                    ShortCode = r.Get<string>("chrCountryCodeShort")
                },
                County = r.Get<string>("chrCounty"),
                ID = r.Get<int>("intWebUserAddressID"),
                IsDefault = r.Get<bool>("bitDefault"),
                PostCode = r.Get<string>("chrPostcode"),
                Recipient = r.Get<string>("chrRecipient"),
                TownCity = r.Get<string>("chrTownCity"),
                WebUserID = r.Get<int>("intWebUserID"),
                DateCreated = r.Get<DateTime>("datCreated"),
                DateUpdated = r.Get<DateTime>("datUpdated")
            };
        }
    }
}
