﻿using DataAccess;
using DataAccess.Base;
using FluentContent.Entities;
using FluentContent.Entities.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.DAC
{
    public class DAC_Property
    {

        public static Property Get(int id)
        {
            return DbFactory.Connect<Property, int>(Get, id);
        }

        public static Property Get(DbConnection connection, DbTransaction transaction, int id)
        {
            Property item = null;
            DbReader r = null;

            try
            {
                r = new DbReader("spProperty_Select", connection, transaction, new Dictionary<string, object>
                {
                    { "intPropertyID", id },
                    { "intClientID", Settings.ClientID }
                });

                if (r.Read())
                {
                    item = new Property(r);
                }

                if (r != null) r.Close();
                if (item != null)
                    item.Images = DAC_PropertyImage.GetByPropertyID(connection, transaction, id);
            }
            finally
            {
                if (r != null) r.Close();
            }

            return item;
        }


        public static ResultSet<Property> GetAll(PagingParameter p, int propertyStatusID)
        {
            return DbFactory.Connect<ResultSet<Property>, PagingParameter, int>(GetAll, p, propertyStatusID);
        }

        public static ResultSet<Property> GetAll(DbConnection connection, DbTransaction transaction, PagingParameter p, int propertyStatusID)
        {
            DbReader r = null;
            ResultSet<Property> result = null;

            try
            {
                r = new DbReader("spPropertyListPaged_Select", connection, transaction, new Dictionary<string, object>
                {
                    { "PageNumber", p.PageNumber },
                    { "RowsPerPage", p.RowsPerPage },
                    { "intClientID", Settings.ClientID },
                    { "intPropertyStatusItemID", propertyStatusID == 0 ? (object)DBNull.Value : propertyStatusID },
                    { "SortBy", String.IsNullOrEmpty(p.SortBy) ? (object)DBNull.Value : p.SortBy.ToLower() },
                    { "Direction", String.IsNullOrEmpty(p.SortDirection) ? (object)DBNull.Value : p.SortDirection.ToLower()}
                });

                result = new ResultSet<Property>(p.PageNumber, p.RowsPerPage, r, ReadProperty);

            }
            finally
            {
                if (r != null) r.Close();
            }

            return result;
        }


        public static Property Insert(Property p)
        {
            return DbFactory.Connect<Property, Property>(Insert, p);
        }

        public static Property Insert(DbConnection connection, DbTransaction transaction, Property p)
        {
            DbReader r = null;

            try
            {
                r = new DbReader("spProperty_Insert", connection, transaction, new Dictionary<string, object>
                {
                    {"intClientID", Settings.ClientID},
                    {"chrAddress1",p.Address1},
                    {"chrAddress2",p.Address2},
                    {"chrAddress3",p.Address3},
                    {"intCountyID",p.County != null ? p.County.ID : (object)DBNull.Value },
                    {"chrAddressPostcode",p.AddressPostcode},
                    {"intCountryID",p.Country != null ?p.Country.ID : (object)DBNull.Value},
                    {"intStatusID",p.StatusID},
                    {"chrGoogleCoordinates",p.GoogleCoordinates},
                    {"chrTitle",p.Title},
                    {"chrDescription",p.Description},
                    {"bitForSale",p.ForSale},
                    {"numPrice",p.Price > 0 ? p.Price : (object)DBNull.Value},
                    {"numPriceOld",p.PriceOld > 0 ? p.PriceOld : (object)DBNull.Value},
                    {"bitForRent",p.ForRent},
                    {"numRental",p.Rental > 0 ? p.Rental :(object)DBNull.Value},
                    {"numRentalOld",p.RentalOld > 0 ? p.RentalOld :(object)DBNull.Value},
                    {"bitPromotion1",p.Promotion1},
                    {"bitPromotion2",p.Promotion2},
                    {"chrAdditionalInformation",p.AdditionalInformation},
                    {"intPropertyTypeItemID",p.PropertyTypeItemID},
                    {"intBedroomCount",p.BedroomCount},
                    {"intBathroomCount",p.BathroomCount},
                    {"intAgencyTypeItemID",p.AgencyTypeItemID},
                    {"intTenureItemID",p.TenureItemID},
                    {"intCategoryItemID",p.CategoryItemID},
                    {"intForSaleStatusItemID",p.ForSaleStatusItemID},
                    {"intForRentStatusItemID",p.ForRentStatusItemID},
                    {"chrMapReference",p.MapReference},
                    {"chrDirections",p.Directions},
                    {"chrCommission",p.Commission},
                    {"bitForSaleRentBoard",p.ForSaleRentBoard},
                    {"bitAdvertisementAllowed",p.AdvertisementAllowed},
                    {"chrVendorsName",p.VendorsName },
                    {"chrVendorAddressLine1",p.VendorAddressLine1},
                    {"chrVendorAddressLine2",p.VendorAddressLine2},
                    {"chrVendorAddressLine3",p.VendorAddressLine3},
                    {"intVendorCountyID",p.VendorCounty != null ?p.VendorCounty.ID : (object)DBNull.Value},
                    {"intVendorCountryID",p.VendorCountry != null ? p.VendorCountry.ID :(object)DBNull.Value},
                    {"chrVendorPostcode",p.VendorPostcode},
                    {"chrVendorPhoneHome",p.VendorPhoneHome },
                    {"chrVendorPhoneWork",p.VendorPhoneWork},
                    {"chrVendorPhoneMobile",p.VendorPhoneMobile},
                    {"chrVendorEmail",p.VendorEmail},
                    {"chrVendorKeysHeldViewingInfo",p.VendorKeysHeldViewingInfo},
                    {"chrVendorAdditionalInfo",p.VendorAdditionalInfo},
                    {"chrVendorReasonForSellingRent",p.VendorReasonForSellingRent},
                    {"chrVendorReasonForRemoving",p.VendorReasonForRemoving},
                    {"chrVendorNextPurchase",p.VendorNextPurchase},
                    {"chrVendorHowTheyHeard",p.VendorHowTheyHeard},
                    {"chrVendorRestrictions",p.VendorRestrictions},
                    {"chrAttachmentFilename", p.AttachmentFileName},
                    {"chrAdditionalDescription", p.AdditionalDescription},
                    {"intForSaleCurrencyItemID", p.ForSaleCurrencyItemID },
                    {"intForRentCurrencyItemID", p.ForRentCurrencyItemID },
                    {"intRentalTermsItemID", p.RentalTermsItemID == 0 ? (object)DBNull.Value : p.RentalTermsItemID },
                    {"chrPropertyCode", String.IsNullOrEmpty(p.PropertyCode) ? (object)DBNull.Value :p.PropertyCode }
                });

                if (r.Read())
                {
                    p = new Property(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return p;
        }


        public static Property Update(Property p)
        {
            return DbFactory.Connect<Property, Property>(Update, p);
        }

        public static Property Update(DbConnection connection, DbTransaction transaction, Property p)
        {
            DbReader r = null;

            try
            {
                r = new DbReader("spProperty_Update", connection, transaction, new Dictionary<string, object>
                {
                    { "intPropertyID", p.ID },
                    {"intClientID", Settings.ClientID},
                    {"chrAddress1",p.Address1},
                    {"chrAddress2",p.Address2},
                    {"chrAddress3",p.Address3},
                    {"intCountyID",p.County != null ? p.County.ID : (object)DBNull.Value },
                    {"chrAddressPostcode",p.AddressPostcode},
                    {"intCountryID",p.Country != null ?p.Country.ID : (object)DBNull.Value},
                    {"intStatusID",p.StatusID},
                    {"chrGoogleCoordinates",p.GoogleCoordinates},
                    {"chrTitle",p.Title},
                    {"chrDescription",p.Description},
                    {"bitForSale",p.ForSale},
                    {"numPrice",p.Price > 0 ? p.Price : (object)DBNull.Value},
                    {"numPriceOld",p.PriceOld > 0 ? p.PriceOld : (object)DBNull.Value},
                    {"bitForRent",p.ForRent},
                    {"numRental",p.Rental > 0 ? p.Rental :(object)DBNull.Value},
                    {"numRentalOld",p.RentalOld > 0 ? p.RentalOld :(object)DBNull.Value},
                    {"bitPromotion1",p.Promotion1},
                    {"bitPromotion2",p.Promotion2},
                    {"chrAdditionalInformation",p.AdditionalInformation},
                    {"intPropertyTypeItemID",p.PropertyTypeItemID},
                    {"intBedroomCount",p.BedroomCount},
                    {"intBathroomCount",p.BathroomCount},
                    {"intAgencyTypeItemID",p.AgencyTypeItemID},
                    {"intTenureItemID",p.TenureItemID},
                    {"intCategoryItemID",p.CategoryItemID},
                    {"intForSaleStatusItemID",p.ForSaleStatusItemID},
                    {"intForRentStatusItemID",p.ForRentStatusItemID},
                    {"chrMapReference",p.MapReference},
                    {"chrDirections",p.Directions},
                    {"chrCommission",p.Commission},
                    {"bitForSaleRentBoard",p.ForSaleRentBoard},
                    {"bitAdvertisementAllowed",p.AdvertisementAllowed},
                    {"chrVendorsName",p.VendorsName },
                    {"chrVendorAddressLine1",p.VendorAddressLine1},
                    {"chrVendorAddressLine2",p.VendorAddressLine2},
                    {"chrVendorAddressLine3",p.VendorAddressLine3},
                    {"intVendorCountyID",p.VendorCounty != null ?p.VendorCounty.ID : (object)DBNull.Value},
                    {"intVendorCountryID",p.VendorCountry != null ? p.VendorCountry.ID :(object)DBNull.Value},
                    {"chrVendorPostcode",p.VendorPostcode},
                    {"chrVendorPhoneHome",p.VendorPhoneHome },
                    {"chrVendorPhoneWork",p.VendorPhoneWork},
                    {"chrVendorPhoneMobile",p.VendorPhoneMobile},
                    {"chrVendorEmail",p.VendorEmail},
                    {"chrVendorKeysHeldViewingInfo",p.VendorKeysHeldViewingInfo},
                    {"chrVendorAdditionalInfo",p.VendorAdditionalInfo},
                    {"chrVendorReasonForSellingRent",p.VendorReasonForSellingRent},
                    {"chrVendorReasonForRemoving",p.VendorReasonForRemoving},
                    {"chrVendorNextPurchase",p.VendorNextPurchase},
                    {"chrVendorHowTheyHeard",p.VendorHowTheyHeard},
                    {"chrVendorRestrictions",p.VendorRestrictions},
                    {"chrAttachmentFilename", p.AttachmentFileName},
                    {"chrAdditionalDescription", p.AdditionalDescription},
                    {"intForSaleCurrencyItemID", p.ForSaleCurrencyItemID },
                    {"intForRentCurrencyItemID", p.ForRentCurrencyItemID },
                    {"intRentalTermsItemID", p.RentalTermsItemID == 0 ? (object)DBNull.Value : p.RentalTermsItemID },
                    {"chrPropertyCode", p.PropertyCode }
                });

                if (r.Read())
                {
                    p = new Property(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return p;
        }


        public static bool Delete(Property p)
        {
            return DbFactory.Connect<bool, Property>(Delete, p);
        }

        public static bool Delete(DbConnection connection, DbTransaction transaction, Property p)
        {
            return DbFactory.ExecuteNonQuery("spProperty_Delete", connection, transaction, new Dictionary<string, object> 
            {
                { "intPropertyID", p.ID }
            }) > 0;
        }

        public static List<Property> PropertyiesBySubscribedUserCriteria(PropertySubscribedUser u)
        {
            return DbFactory.Connect<List<Property>, PropertySubscribedUser>(PropertyiesBySubscribedUserCriteria, u);
        }

        public static List<Property> PropertyiesBySubscribedUserCriteria(DbConnection connection, DbTransaction transaction, PropertySubscribedUser u)
        {
            List<Property> items = new List<Property>();
            DbReader r = null;

            string countryIDs = u.InterestCountyIDs != null ? String.Join(",", u.InterestCountyIDs.Select(i => i.ToString()).ToArray()) : null;

            try
            {
                r = new DbReader("spPropertyiesBySubscribedUserCriteria_Select", connection, transaction, new Dictionary<string, object> 
                {
                    {"bitBuy", u.WantsToBuy ? u.WantsToBuy : (object)DBNull.Value },
                    {"bitRent", u.WantsToRent ? u.WantsToRent : (object)DBNull.Value },
                    {"intPropertyTypeItemID", u.PropertyTypeItemID == 0 ? (object)DBNull.Value : u.PropertyTypeItemID },
                    {"chrPropertyCountyIDS", (countryIDs == null || countryIDs.Length == 0) ? (object)DBNull.Value : countryIDs },
                    {"intMinBedrooms", u.MinBedrooms== 0 ? (object)DBNull.Value : u.MinBedrooms },
                    {"intMinBathrooms", u.MinBathrooms == 0 ? (object)DBNull.Value : u.MinBathrooms },
                    {"numMinPrice", u.MinPrice == 0 ? (object)DBNull.Value : u.MinPrice},
                    {"numMaxPrice", u.MaxPrice == 0 ? (object)DBNull.Value : u.MaxPrice},
                    {"intClientID", Settings.ClientID },
                    {"intQualificationID", u.QualificationID == 0 ?(object)DBNull.Value : u.QualificationID }

                });

                while (r.Read())
                {
                    items.Add(new Property(r));
                }
                if (r != null) r.Close();

                if (items.Count > 0)
                {
                    foreach (var item in items)
                    {
                        item.Images = item.Images = DAC_PropertyImage.GetByPropertyID(connection, transaction, item.ID);
                    }
                }
            }
            finally
            {
                if (r != null) r.Close();
            }
            return items;
        }


        // Helpers


        public static List<string> GetDistinctCounties()
        {
            return DbFactory.Connect<List<string>>(GetDistinctCounties);
        }

        public static List<string> GetDistinctCounties(DbConnection connection, DbTransaction transaction)
        {
            List<string> items = new List<string>();
            DbReader r = null;

            try
            {
                r = new DbReader("spPropertyCountiesDistinct_Select", connection, transaction, new Dictionary<string, object>
                {
                    { "intClientID", Settings.ClientID }
                });

                while (r.Read())
                {
                    items.Add(r.Get<string>("chrAddressCounty"));
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return items;
        }

        public static Property ReadProperty(DbReader r)
        {
            return new Property(r);
        }
    }
}
