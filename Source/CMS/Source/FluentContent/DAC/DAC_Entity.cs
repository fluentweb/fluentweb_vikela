﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentContent.Entities;
using System.Data.Common;
using DataAccess;
using DataAccess.Base;

namespace FluentContent.DAC
{
    public class DAC_Entity
    {
        // Select

        public static List<Entity> Get(DbConnection connection, DbTransaction transaction, int clientID)
        {
            List<Entity> items = new List<Entity>();
            DbReader r = null;
            try
            {
                r = new DbReader("spEntity_Select", connection, transaction, new Dictionary<string, object> { { "intClientID", clientID == 0 ? (object)DBNull.Value : clientID } });
                while (r.Read())
                {
                    items.Add(ReadEntity(r));
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return items;
        }

        public static List<Entity> Get(int clientID)
        {
            return DbFactory.Connect<List<Entity>, int>(Get, clientID);
        }

        public static Entity GetByID(DbConnection connection, DbTransaction transaction, int id, int clientID)
        {
            Entity item = new Entity();
            DbReader r = null;
            try
            {
                r = new DbReader("spEntityByID_Select", connection, transaction, new Dictionary<string, object> 
                {
                    { "intClientID", clientID == 0 ? (object)DBNull.Value : clientID  },
                    { "intEntityID", id }
                });

                if (r.Read())
                {
                    item = ReadEntity(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return item;
        }

        public static Entity GetByID(int id, int clientID)
        {
            return DbFactory.Connect<Entity, int, int>(GetByID, id, clientID);
        }


        public static List<Entity> GetByTypeID(DbConnection connection, DbTransaction transaction, int typeID)
        {
            List<Entity> items = new List<Entity>();
            DbReader r = null;
            try
            {
                r = new DbReader("spEntityByType_Select", connection, transaction, new Dictionary<string, object> 
                {
                    { "intClientID", Settings.ClientID },
                    { "intEntityTypeID", typeID }
                });
                while (r.Read())
                {
                    items.Add(ReadEntity(r));
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return items;
        }

        public static List<Entity> GetByTypeID(int typeID)
        {
            return DbFactory.Connect<List<Entity>, int>(GetByTypeID, typeID);
        }

        // Insert

        public static int Insert(DbConnection connection, DbTransaction transaction, Entity e)
        {
            return DbFactory.ExecuteScalar<int>("spEntity_Insert", connection, transaction, new Dictionary<string, object> 
            {
                { "intEntityTypeID", e.TypeID },
                { "chrEntityName", e.Name },
                { "chrLogoFilename", e.LogoFilename },
                { "datExpires", e.DateExpires },
                { "bitEnabled", e.Enabled }
            });
        }

        public static int Insert(Entity e)
        {
            return DbFactory.Connect<int, Entity>(Insert, e);
        }

        // Update

        public static bool Update(DbConnection connection, DbTransaction transaction, Entity e)
        {
            return DbFactory.ExecuteNonQuery("spEntity_Update", connection, transaction, new Dictionary<string, object> 
            {
                { "intEntityID", e.ID },
                { "intEntityTypeID", e.TypeID },
                { "chrEntityName", e.Name },
                { "chrLogoFilename", e.LogoFilename ?? (object)DBNull.Value},
                { "datExpires", e.DateExpires  == DateTime.MinValue ? (object)DBNull.Value : e.DateExpires},
                { "bitEnabled", e.Enabled }
            }) > 0;
        }

        public static bool Update(Entity e)
        {
            return DbFactory.Connect<bool, Entity>(Update, e);
        }

        // Delete

        public static bool Delete(DbConnection connection, DbTransaction transaction, int id)
        {
            try
            {
                DbFactory.ExecuteNonQuery("spEntity_Delete", connection, transaction, new Dictionary<string, object> 
                {
                    { "intEntityID", id }
                });
            }
            catch
            {
                return false;
            }
            return true;
        }

        public static bool Delete(int id)
        {
            return DbFactory.Connect<bool, int>(Delete, id);
        }

        // Helpers

        private static Entity ReadEntity(DbReader r)
        {
            return new Entity
            {
                ID = r.Get<int>("intEntityID"),
                Name = r.Get<string>("chrEntityName"),
                Type = r.Get<string>("chrEntityType"),
                TypeID = r.Get<int>("intEntityTypeID"),
                Enabled = r.Get<bool>("bitEnabled", false),
                LogoFilename = r.Get<string>("chrLogoFileName"),
                DateExpires = r.Get<DateTime>("datExpires")
            };
        }
    }
}
