﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentContent.Entities;
using System.Data.Common;
using DataAccess.Base;
using DataAccess;

namespace FluentContent.DAC
{
    public class DAC_FormObject
    {
        // BY FORM ID
        public static List<FormObject> GetFormObjectsByFormID(int intFormID)
        {
            return DbFactory.Connect<List<FormObject>, int>(GetFormObjectsByFormID, intFormID);
        }

        public static List<FormObject> GetFormObjectsByFormID(DbConnection connection, DbTransaction transaction, int intFormID)
        {
            List<FormObject> items = new List<FormObject>();
            DbReader r = null;
            try
            {
                r = new DbReader("spFormObjectsByFormID_Select", connection, transaction, new Dictionary<string, object> 
                {
                    { "intFormID", intFormID }
                });
                while (r.Read())
                {
                    items.Add(ReadFormObject(r));
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return items;
        }

        // BY ID

        public static FormObject GetFormObjectByID(int intObjectID)
        {
            return DbFactory.Connect<FormObject, int>(GetFormObjectByID, intObjectID);
        }

        public static FormObject GetFormObjectByID(DbConnection connection, DbTransaction transaction, int intObjectID)
        {
            FormObject item = new FormObject();
            DbReader r = null;
            try
            {
                r = new DbReader("spFormObjectByID_Select", connection, transaction, new Dictionary<string, object> 
                {
                    { "intFormObjectID", intObjectID }
                });
                if (r.Read())
                {
                    item = ReadFormObject(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return item;
        }

        // DELETE

        public static bool Delete(DbConnection connection, DbTransaction transaction, int formObjectID)
        {
            return DbFactory.ExecuteNonQuery("spFormObject_Delete", connection, transaction, new Dictionary<string, object> 
            {
                { "intFormObjectID", formObjectID }
            }) > 0;
        }

        // TEMPLATES
        public static string GetFullTemplateWithDataByFormID(int formID)
        {
            List<FormObject> items = GetFormObjectsByFormID(formID);
            items = SortFormObjectList(items, 0);

            string returnValue = null;
            foreach (FormObject fo in items)
            {
                returnValue += GetTemplateWithDataForObject(fo, fo.ParentObjectID);
            }

            return returnValue;
        }

        public static List<FormObject> SortFormObjectList(List<FormObject> unorderedFormObjects, int parentID)
        {
            var items = unorderedFormObjects.FindAll(o => o.ParentObjectID == parentID);

            foreach (var item in items)
            {
                item.Children = unorderedFormObjects.FindAll(o => o.ParentObjectID == item.ID);
                if (item.Children.Count > 0)
                {
                    foreach (var child in item.Children)
                    {
                        child.Children = SortFormObjectList(unorderedFormObjects, child.ID);
                    }
                }
                else
                {
                    continue;
                }
            }
            return items;
        }

        private static string GetTemplateWithDataForObject(FormObject formObject, int parentContainerID)
        {
            string listItemTemplate = @"<li id='{0}' type-id='{1}'>
                                            <i class='icon-move'></i>
                                            <span class='template-title'>{2}</span>
                                            <a class='template-delete' href='#'>Delete</a>
                                            <hr />
                                            <div class='formWrapper'>                                                    
                                                {3}
                                            <div class='clear'></div>
                                            </div>
                                            {4}
                                        </li>";

            string containerTempalte = @"<ol>{0}</ol>";

            string inputFieldTemplate = "<p><label>{0}</label><input type='text' data-selector='{1}' value=\"{2}\" /></p>";

            string text1 = null;
            if (formObject.Type.Text1Required)
                text1 = String.Format(inputFieldTemplate, formObject.Type.Text1Label, "text_1", formObject.Text1);

            string text2 = null;
            if (formObject.Type.Text2Required)
                text2 = String.Format(inputFieldTemplate, formObject.Type.Text2Label, "text_2", formObject.Text2);

            string text3 = null;
            if (formObject.Type.Text3Required)
                text3 = String.Format(inputFieldTemplate, formObject.Type.Text3Label, "text_3", formObject.Text3);

            string data = null;
            if (formObject.Type.DataRequired)
                data = String.Format(inputFieldTemplate, formObject.Type.DataLabel, "data_1", formObject.Data);

            string container = null;
            if (formObject.Type.IsContainer)
            {
                string childrenValue = null;
                foreach (var child in formObject.Children)
                {
                    childrenValue += GetTemplateWithDataForObject(child, formObject.ParentObjectID);
                }
                container = String.Format(containerTempalte, childrenValue);
            }

            StringBuilder sbTemplate = new StringBuilder();
            sbTemplate.AppendFormat(listItemTemplate, formObject.ID, formObject.Type.ID, formObject.Type.Name,
                String.Format("{0}{1}{2}{3}", text1, text2, text3, data), container);
            return sbTemplate.ToString();
        }

        // INSERT AND UPDATE 

        private static bool SaveForm(DbConnection connection, DbTransaction transaction, List<FormObject> formObjects, List<int> deleteIDs)
        {
            // delete all items in deleteIDs
            foreach (var id in deleteIDs)
            {
                Delete(connection, transaction, id);
            }

            // insert new or update existing items
            foreach (var formObject in formObjects)
            {
                InsertUpdateFormObject(connection, transaction, formObject);
            }

            return true;
        }

        private static void InsertUpdateFormObject(DbConnection connection, DbTransaction transaction, FormObject o)
        {
            int parentID = 0;
            if (o.ID > 0)
            {
                // existing parent id 
                parentID = o.ID;

                DbFactory.ExecuteNonQuery("spFormObject_Update", connection, transaction, new Dictionary<string, object>
                {
                    { "intFormObjectID", o.ID },
                    { "intSequenceID", o.Sequence },
                    { "chrFormObjectText1", String.IsNullOrEmpty(o.Text1) ? (object)DBNull.Value : o.Text1 },
                    { "chrFormObjectText2", String.IsNullOrEmpty(o.Text2) ? (object)DBNull.Value : o.Text2 },
                    { "chrFormObjectText3", String.IsNullOrEmpty(o.Text3) ? (object)DBNull.Value : o.Text3 },
                    { "chrFormObjectData", String.IsNullOrEmpty(o.Data) ? (object)DBNull.Value : o.Data  },
                    { "intParentFormObjectID", o.ParentObjectID },
                    { "intFormObjectTypeID", o.Type.ID },
                    { "intFormID", o.FormID }
                });
            }
            else
            {
                int objectID = DbFactory.ExecuteScalar<int>("spFormObject_Insert", connection, transaction, new Dictionary<string, object> 
                {
                    { "intSequenceID", o.Sequence },
                    { "chrFormObjectText1", String.IsNullOrEmpty(o.Text1) ? (object)DBNull.Value : o.Text1 },
                    { "chrFormObjectText2", String.IsNullOrEmpty(o.Text2) ? (object)DBNull.Value : o.Text2 },
                    { "chrFormObjectText3", String.IsNullOrEmpty(o.Text3) ? (object)DBNull.Value : o.Text3 },
                    { "chrFormObjectData", String.IsNullOrEmpty(o.Data) ? (object)DBNull.Value : o.Data  },
                    { "intParentFormObjectID", o.ParentObjectID },
                    { "intFormObjectTypeID", o.Type.ID },
                    { "intFormID", o.FormID }
                });

                // save new parent ID
                parentID = objectID;
            }

            // recursively traverse childred and set parent id (this is to cater for newly created items)
            if (o.Children != null)
            {
                foreach (FormObject child in o.Children)
                {
                    child.ParentObjectID = parentID;
                    InsertUpdateFormObject(connection, transaction, child);
                }
            }
        }

        public static bool SaveForm(List<FormObject> formObjects, List<int> deleteIDs, int formID)
        {
            formObjects = prepareSequence(formObjects, 0, formID);
            return DbFactory.Connect<bool, List<FormObject>, List<int>>(SaveForm, formObjects, deleteIDs);
        }



        // HELPERS
        private static FormObject ReadFormObject(DbReader r)
        {
            return new FormObject
            {
                Data = r.Get<string>("chrFormObjectData"),
                FormID = r.Get<int>("intFormID"),
                ID = r.Get<int>("intFormObjectID"),
                ParentObjectID = r.Get<int>("intParentFormObjectID"),
                Sequence = r.Get<int>("intSequenceID"),
                Text1 = r.Get<string>("chrFormObjectText1"),
                Text2 = r.Get<string>("chrFormObjectText2"),
                Text3 = r.Get<string>("chrFormObjectText3"),
                Type = new FormObjectType
                {
                    DataRequired = r.Get<bool>("bitDataRequired"),
                    Description = r.Get<string>("chrFormObjectTypeDesc"),
                    FormTypeID = r.Get<int>("intFormTypeID"),
                    ID = r.Get<int>("intFormObjectTypeID"),
                    IsContainer = r.Get<bool>("bitIsContainer"),
                    Name = r.Get<string>("chrFormObjectType"),
                    Text1Required = r.Get<bool>("bitText1Required"),
                    Text2Required = r.Get<bool>("bitText2Required"),
                    Text3Required = r.Get<bool>("bitText3Required"),
                    DataLabel = r.Get<string>("chrDataLabel"),
                    Text1Label = r.Get<string>("chrText1Label"),
                    Text2Label = r.Get<string>("chrText2Label"),
                    Text3Label = r.Get<string>("chrText3Label")
                }
            };
        }

        private static List<FormObject> prepareSequence(List<FormObject> formObjects, int parentID, int formID)
        {
            int sequence = 1;
            foreach (FormObject fo in formObjects)
            {
                fo.FormID = formID;
                fo.Sequence = sequence++;
                fo.ParentObjectID = parentID;
                if (fo.Children != null && fo.Children.Count > 0)
                {
                    fo.Children = prepareSequence(fo.Children, fo.ID, formID);
                }
            }
            return formObjects;
        }
    }
}
