﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using DataAccess;
using DataAccess.Base;
using FluentContent.Entities;

namespace FluentContent.DAC
{
    public class DAC_WebsiteSetting
    {

        public static string Get(DbConnection connection, DbTransaction transaction, string key)
        {
            string s = null;
            s = DbFactory.ExecuteScalar<string>("spGetSettingByKey", connection, transaction, new Dictionary<string, object> 
            {
                { "Key", key }
            });

            return s == null ? String.Empty : s;
        }

        public static string Get(string key)
        {
            return DbFactory.Connect<string, string>(Get, key);
        }

        public static bool InsertUpdate(DbConnection connection, DbTransaction transaction, string key, string value)
        {
            DbFactory.ExecuteNonQuery("spInsertUpdateSetting", connection, transaction, new Dictionary<string, object> 
            {
                { "Key", key },
                { "Value", value }
            });
            return true;
        }

        public static bool InsertUpdate(string key, string value)
        {
            return DbFactory.Connect<bool, string, string>(InsertUpdate, key, value);
        }

    }
}
