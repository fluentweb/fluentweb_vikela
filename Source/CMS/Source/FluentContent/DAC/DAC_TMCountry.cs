﻿using DataAccess;
using DataAccess.Base;
using FluentContent.Entities;
using FluentContent.Entities.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.DAC
{
    public class DAC_TMCountry
    {
        public static DataTable GetCountryByCountry()
        {
            return DbFactory.Connect<DataTable>(GetCountryByCountry);
        }

        public static DataTable GetCountryByCountry(DbConnection connection, DbTransaction transaction)
        {
            return DbFactory.GetDataTable("spTMtCountryByCountry_Select", connection, transaction, new Dictionary<string, object> 
            {
                { "CountryID", (object)DBNull.Value }
            });
        }

        public static TMCountry GetCountryByCountry(int countryId)
        {
            return DbFactory.Connect<TMCountry, int>(GetCountryByCountry, countryId);
        }

        public static TMCountry GetCountryByCountry(DbConnection connection, DbTransaction transaction, int countryId)
        {
            TMCountry item = null;
            DbReader r = null;

            try
            {
                r = new DbReader("spTMtCountryByCountry_Select", connection, transaction, new Dictionary<string, object>
                {
                    { "CountryID", countryId }
                });

                if (r.Read())
                {
                    item = new TMCountry(r);
                }

                if (r != null) r.Close();
            }
            finally
            {
                if (r != null) r.Close();
            }

            return item;
        }

        public static TMCountry Insert(TMCountry tmc)
        {
            return DbFactory.Connect<TMCountry, TMCountry>(Insert, tmc);
        }

        public static TMCountry Insert(DbConnection connection, DbTransaction transaction, TMCountry tmc)
        {
            DbReader r = null;

            try
            {
                r = new DbReader("spTMtCountry_Insert", connection, transaction, new Dictionary<string, object>
                {
                    {"CountryID",tmc.CountryID},
                    {"PowerOfAttorney",tmc.PowerOfAttorney},
                    {"ConventionPriority",tmc.ConventionPriority},
                    {"ColourRestrictionBW",tmc.ColourRestrictionBW},
                    {"ColourRestrictionCC",tmc.ColourRestrictionCC},
                    {"ColourRestrictionCSM",tmc.ColourRestrictionCSM},
                    {"ColourRestrictionCL",tmc.ColourRestrictionCL},
                    {"LanguageID",tmc.TranslationLanguageID},
                    {"Available",tmc.Available},
                    {"ClassificationID",tmc.TMClassificationID}

                });

                if (r.Read())
                {
                    tmc = new TMCountry(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return tmc;
        }

        public static TMCountry Update(TMCountry tmc)
        {
            return DbFactory.Connect<TMCountry, TMCountry>(Update, tmc);
        }

        public static TMCountry Update(DbConnection connection, DbTransaction transaction, TMCountry tmc)
        {
            DbReader r = null;

            try
            {
                r = new DbReader("spTMtCountry_Update", connection, transaction, new Dictionary<string, object>
                {
                    {"CountryID",tmc.CountryID},
                    {"PowerOfAttorney",tmc.PowerOfAttorney},
                    {"ConventionPriority",tmc.ConventionPriority},
                    {"ColourRestrictionBW",tmc.ColourRestrictionBW},
                    {"ColourRestrictionCC",tmc.ColourRestrictionCC},
                    {"ColourRestrictionCSM",tmc.ColourRestrictionCSM},
                    {"ColourRestrictionCL",tmc.ColourRestrictionCL},
                    {"LanguageID",tmc.TranslationLanguageID},
                    {"Available",tmc.Available},
                    {"ClassificationID",tmc.TMClassificationID}

                });

                if (r.Read())
                {
                    tmc = new TMCountry(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return tmc;
        }
        public static bool Delete(TMCountry tmc)
        {
            return DbFactory.Connect<bool, TMCountry>(Delete, tmc);
        }

        public static bool Delete(DbConnection connection, DbTransaction transaction, TMCountry tmc)
        {
            return DbFactory.ExecuteNonQuery("spTMtCountry_Delete", connection, transaction, new Dictionary<string, object> 
            {
                { "CountryID", tmc.CountryID }
            }) > 0;
        }
        
        public static TMCountry ReadProperty(DbReader r)
        {
            return new TMCountry(r);
        }

    }
}
