﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentContent.Entities;
using System.Data.Common;
using DataAccess.Base;
using DataAccess;

namespace FluentContent.DAC
{
    public class DAC_FormObjectType
    {
        public static List<FormObjectType> GetFormObjectTypesByFormTypeID(int formTypeID)
        {
            return DbFactory.Connect<List<FormObjectType>, int>(GetFormObjectTypesByFormTypeID, formTypeID);
        }

        public static List<FormObjectType> GetFormObjectTypesByFormTypeID(DbConnection connection, DbTransaction transaction, int formTypeID)
        {
            List<FormObjectType> items = new List<FormObjectType>();
            DbReader r = null;

            try
            {
                r = new DbReader("spFormObjectTypesByFormTypeID_Select", connection, transaction, new Dictionary<string, object>
                {
                    { "intFormTypeID", formTypeID }
                });
                while (r.Read())
                {
                    items.Add(ReadFormObjectType(r));
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return items;
        }


        public static FormObjectType GetFormObjectTypeByID(int formObjectTypeID)
        {
            return DbFactory.Connect<FormObjectType, int>(GetFormObjectTypeByID, formObjectTypeID);
        }

        public static FormObjectType GetFormObjectTypeByID(DbConnection connection, DbTransaction transaction, int formObjectTypeID)
        {
            FormObjectType item = null;
            DbReader r = null;

            try
            {
                r = new DbReader("spFormObjectTypeByID_Select", connection, transaction, new Dictionary<string, object> 
                {
                    { "intFormObjectTypeID", formObjectTypeID }
                });
                if (r.Read())
                {
                    item = ReadFormObjectType(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return item;
        }

        // HELPERS

        public static string GetTemplate(int formObjectTuypeID)
        {
            FormObjectType formObjectType = GetFormObjectTypeByID(formObjectTuypeID);


            string listItemTemplate = @"<li id='-1' type-id='{0}'>
                                            <i class='icon-move'></i>
                                            <span class='template-title'>{1}</span>
                                            <a class='template-delete' href='#'>Delete</a>
                                            <hr />
                                            <div class='formWrapper'>                                                    
                                                {2}
                                            <div class='clear'></div>
                                            </div>
                                            {3}
                                        </li>";
            string containerTempalte = @"<ol></ol>";

            string inputFieldTemplate = @"<p>
                                            <label>{0}</label>
                                            <input type='text' data-selector='{1}' />
                                          </p>";

            string text1 = null;
            if (formObjectType.Text1Required)
                text1 = String.Format(inputFieldTemplate, formObjectType.Text1Label, "text_1");

            string text2 = null;
            if (formObjectType.Text2Required)
                text2 = String.Format(inputFieldTemplate, formObjectType.Text2Label, "text_2");

            string text3 = null;
            if (formObjectType.Text3Required)
                text3 = String.Format(inputFieldTemplate, formObjectType.Text3Label, "text_3");

            string data = null;
            if (formObjectType.DataRequired)
                data = String.Format(inputFieldTemplate, formObjectType.DataLabel, "data_1");

            string container = null;
            if (formObjectType.IsContainer)
                container = containerTempalte;

            StringBuilder sbTemplate = new StringBuilder();
            sbTemplate.AppendFormat(listItemTemplate, formObjectType.ID, formObjectType.Name,
                String.Format("{0}{1}{2}{3}", text1, text2, text3, data), container);
            return sbTemplate.ToString();
        }

        private static FormObjectType ReadFormObjectType(DbReader r)
        {
            return new FormObjectType
            {
                DataLabel = r.Get<string>("chrDataLabel"),
                DataRequired = r.Get<bool>("bitDataRequired"),
                Description = r.Get<string>("chrFormObjectTypeDesc"),
                FormTypeID = r.Get<int>("intFormTypeID"),
                ID = r.Get<int>("intFormObjectTypeID"),
                IsContainer = r.Get<bool>("bitIsContainer"),
                Name = r.Get<string>("chrFormObjectType"),
                Text1Label = r.Get<string>("chrText1Label"),
                Text1Required = r.Get<bool>("bitText1Required"),
                Text2Label = r.Get<string>("chrText2Label"),
                Text2Required = r.Get<bool>("bitText2Required"),
                Text3Label = r.Get<string>("chrText3Label"),
                Text3Required = r.Get<bool>("bitText3Required")
            };
        }
    }
}
