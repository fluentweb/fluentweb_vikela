﻿using DataAccess;
using DataAccess.Base;
using FluentContent.Entities;
using FluentContent.Entities.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.DAC
{
    public class DAC_TMClassificationClass
    {
        public static DataTable GetClassificationClassByClassification(int ClassificationID)
        {
            return DbFactory.Connect<DataTable, int>(GetClassificationByClassification, ClassificationID);
        }

        public static DataTable GetClassificationByClassification(DbConnection connection, DbTransaction transaction, int ClassificationID)
        {
            return DbFactory.GetDataTable("spTMtClassificationClass_Select", connection, transaction, new Dictionary<string, object> 
            {
                { "ClassificationID",  ClassificationID == 0 ? (object)DBNull.Value :  ClassificationID },
                    { "ClassID", (object)DBNull.Value }
            });
        }

        public static TMClassificationClass GetClassificationByClass(int classId)
        {
            return DbFactory.Connect<TMClassificationClass, int>(GetClassificationByClass, classId);
        }

        public static TMClassificationClass GetClassificationByClass(DbConnection connection, DbTransaction transaction, int classId)
        {
            DbReader r = null;
            TMClassificationClass result = null;
            try
            {
                r = new DbReader("spTMtClassificationClass_Select", connection, transaction, new Dictionary<string, object>
                {
                    { "ClassificationID", (object)DBNull.Value },
                    { "ClassID", classId }
                });

                if (r.Read())
                {
                    result = new TMClassificationClass(r);
                }

                if (r != null) r.Close();
            }
            finally
            {
                if (r != null) r.Close();
            }

            return result;
        }

        public static TMClassificationClass Insert(TMClassificationClass tmc)
        {
            return DbFactory.Connect<TMClassificationClass, TMClassificationClass>(Insert, tmc);
        }

        public static TMClassificationClass Insert(DbConnection connection, DbTransaction transaction, TMClassificationClass tmc)
        {
            DbReader r = null;

            try
            {
                r = new DbReader("spTMtClassificationClass_Insert", connection, transaction, new Dictionary<string, object>
                {                
                    {"ClassificationID",tmc.TMClassificationID},
                    {"ClassCode",tmc.ClassificationClassCode},
                    {"ClassNo",tmc.ClassificationClassNo  == 0 ? (object)DBNull.Value : tmc.ClassificationClassNo},
                    {"ClassName",tmc.ClassificationClass}
                });

                if (r.Read())
                {
                    tmc = new TMClassificationClass(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return tmc;
        }

        public static TMClassificationClass Update(TMClassificationClass tmc)
        {
            return DbFactory.Connect<TMClassificationClass, TMClassificationClass>(Update, tmc);
        }

        public static TMClassificationClass Update(DbConnection connection, DbTransaction transaction, TMClassificationClass tmc)
        {
            DbReader r = null;

            try
            {
                r = new DbReader("spTMtClassificationClass_Update", connection, transaction, new Dictionary<string, object>
                {
                    {"ClassificationID",tmc.TMClassificationID},
                    {"ClassCode",tmc.ClassificationClassCode},
                    {"ClassNo",tmc.ClassificationClassNo  == 0 ? (object)DBNull.Value : tmc.ClassificationClassNo},
                    {"ClassName",tmc.ClassificationClass},
                    {"ClassID",tmc.TMClassificationClassID}

                });

                if (r.Read())
                {
                    tmc = new TMClassificationClass(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return tmc;
        }

        public static bool Delete(TMClassificationClass tmc)
        {
            return DbFactory.Connect<bool, TMClassificationClass>(Delete, tmc);
        }

        public static bool Delete(DbConnection connection, DbTransaction transaction, TMClassificationClass tmc)
        {
            return DbFactory.ExecuteNonQuery("spTMtClassificationClass_Delete", connection, transaction, new Dictionary<string, object> 
            {
                { "ClassID", tmc.TMClassificationClassID }
            }) > 0;
        }

        public static TMClassification ReadProperty(DbReader r)
        {
            return new TMClassification(r);
        }
    }
}