﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentContent.Entities;
using System.Data.Common;
using DataAccess.Base;
using DataAccess;
using System.Data;
using Common.Extensions;

namespace FluentContent.DAC
{
    public class DAC_ClientReport
    {
        public static List<ClientReport> Get(int entityID, char cmsAccessLevel)
        {
            return DbFactory.Connect<List<ClientReport>, int, char>(Get, entityID, cmsAccessLevel);
        }

        public static List<ClientReport> Get(DbConnection conneciton, DbTransaction transaction, int entityID, char cmsAccessLevel)
        {
            List<ClientReport> items = new List<ClientReport>();
            DbReader r = null;
            try
            {
                r = new DbReader("spClientReports_Select", conneciton, transaction, new Dictionary<string, object> 
                {
                    { "ClientID", Settings.ClientID },
                    { "EntityID", entityID == 0 ? (object)DBNull.Value : entityID },
                    { "AccessLevel", cmsAccessLevel }
                });
                while (r.Read())
                {
                    items.Add(ReadClientReport(r));
                }
            }
            finally
            {
                if (r != null) r.Close();
            }
            return items;
        }

        public static ClientReport GetByID(int entityID, int reportID, char cmsAccessLevel)
        {
            Dictionary<string, object> p = new Dictionary<string, object>
            {                  
                { "EntityID", entityID == 0 ? (object)DBNull.Value : entityID }, 
                { "ReportID", reportID },
                { "AccessLevel", cmsAccessLevel }
            };
            return DbFactory.Connect<ClientReport, Dictionary<string, object>>(GetByID, p);
        }

        public static ClientReport GetByID(DbConnection conneciton, DbTransaction transaction, Dictionary<string, object> p)
        {
            ClientReport item = null;
            DbReader r = null;
            try
            {
                p.Add("ClientID", Settings.ClientID);
                r = new DbReader("spClientReportByID_Select", conneciton, transaction, p);
                if (r.Read())
                {
                    item = ReadClientReport(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }
            return item;
        }



        // Helpers

        public static DataTable ExecuteReport(ClientReport r, params object[] parameters)
        {
            return DbFactory.Connect<DataTable, ClientReport, object[]>(ExecuteReport, r, parameters);
        }

        public static DataTable ExecuteReport(DbConnection connection, ClientReport r, params object[] parameters)
        {
            Dictionary<string, object> inputParams = new Dictionary<string, object>();

            if (parameters != null)
            {
                for (int i = 0; i < parameters.Length; i++)
                {
                    if (i == 0)
                        inputParams.Add(r.Parameter1, (string)parameters[0] == "null" ? (object)DBNull.Value : parameters[0]);
                    if (i == 1)
                        inputParams.Add(r.Parameter2, (string)parameters[1] == "null" ? (object)DBNull.Value : parameters[1]);
                    if (i == 2)
                        inputParams.Add(r.Parameter3, (string)parameters[2] == "null" ? (object)DBNull.Value : parameters[2]);
                    if (i == 3)
                        inputParams.Add(r.Parameter4, (string)parameters[3] == "null" ? (object)DBNull.Value : parameters[3]);
                    if (i == 4)
                        inputParams.Add(r.Parameter5, (string)parameters[4] == "null" ? (object)DBNull.Value : parameters[4]);
                    if (i == 5)
                        inputParams.Add(r.Parameter6, (string)parameters[5] == "null" ? (object)DBNull.Value : parameters[5]);
                }
            }

            DataTable dtResults = DbFactory.GetDataTable(r.StoredProcedure, connection, inputParams);
            return dtResults;
        }

        private static ClientReport ReadClientReport(DbReader r)
        {
            return new ClientReport
            {
                ClientID = r.Get<int>("intClientID"),
                EntityID = r.Get<int>("intEntityID"),
                Description = r.Get<string>("chrReportDesc"),
                ID = r.Get<int>("intReportID"),
                Name = r.Get<string>("chrReportTitle"),
                StoredProcedure = r.Get<string>("chrStoredProcedure"),
                IsSubReport = r.Get<bool>("bitSubReport"),
                Parameter1 = r.Get<string>("chrParameter1"),
                Parameter2 = r.Get<string>("chrParameter2"),
                Parameter3 = r.Get<string>("chrParameter3"),
                Parameter4 = r.Get<string>("chrParameter4"),
                Parameter5 = r.Get<string>("chrParameter5"),
                Parameter6 = r.Get<string>("chrParameter6"),
                RequiresDateFields = r.Get<bool>("bitDatesRequired")
            };
        }
    }
}
