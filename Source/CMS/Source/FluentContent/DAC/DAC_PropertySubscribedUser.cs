﻿using DataAccess;
using DataAccess.Base;
using FluentContent.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Extensions;
using Common.Email;
using System.Net;
using FluentContent.Entities.Helpers;

namespace FluentContent.DAC
{
    public class DAC_PropertySubscribedUser
    {

        public static PropertySubscribedUser Insert(PropertySubscribedUser user)
        {
            return DbFactory.Connect<PropertySubscribedUser, PropertySubscribedUser>(Insert, user);
        }

        public static PropertySubscribedUser Insert(DbConnection connection, DbTransaction transaction, PropertySubscribedUser user)
        {
            DbReader r = null;
            PropertySubscribedUser newUser = null;
            try
            {
                r = new DbReader("spPropertySubscribedUser_Insert", connection, transaction, new Dictionary<string, object>
                {
                    {"intTitleID", user.Title.ID == 0 ? (object)DBNull.Value : user.Title.ID },	
                    {"chrFirstName", user.FirstName },
                    {"chrSurname", user.Surname },
                    {"chrAddress1", user.Address1 },
                    {"chrAddress2", user.Address2 },
                    {"chrAddress3", user.Address3 },
                    {"chrAddressPostcode", user.AddressPostcode },
                    {"intAddressCountryID", user.Country.ID == 0 ? (object)DBNull.Value : user.Country.ID },
                    {"intAddressCountyID", user.County.ID == 0 ? (object)DBNull.Value : user.County.ID },
                    {"chrPhoneHome", user.PhoneHome },
                    {"chrPhoneMobile", user.PhoneMobile },
                    {"chrPhoneWork", user.PhoneWork },
                    {"chrEmailAddress", user.EmailAddress },
                    {"chrSpecificRequirements", user.SpecificRequirements },
                    {"bitInterestedInValuation", user.InterestedInValuation },
                    {"bitWantsToBuy", user.WantsToBuy },
                    {"bitWantsToRent", user.WantsToRent },
                    {"intPropertyTypeItemID", user.PropertyTypeItemID == 0 ?(object)DBNull.Value : user.PropertyTypeItemID },
                    {"intMinBedrooms", user.MinBedrooms == 0 ? (object)DBNull.Value : user.MinBedrooms },
                    {"intMinBathrooms", user.MinBathrooms == 0 ? (object)DBNull.Value : user.MinBathrooms },
                    {"intQualification", user.QualificationID == 0 ? (object)DBNull.Value : user.QualificationID },
                    {"numMinPrice", user.MinPrice == 0 ? (object)DBNull.Value : user.MinPrice },
                    {"numMaxPrice", user.MaxPrice == 0 ? (object)DBNull.Value: user.MaxPrice },
                    {"bitSubscribed", user.Subscribed },
                    {"bitEmailAlerts", user.EmailAlerts },
                    {"intClientID", Settings.ClientID },
                    {"bitOutsideArea", user.OutsideArea ? user.OutsideArea : (object)DBNull.Value },
                    {"bitParking", user.Parking ? user.Parking : (object)DBNull.Value }
                });

                if (r.Read())
                {
                    newUser = ReadPropertySubscribedUser(r);
                    if (r != null) r.Close();
                    if (newUser != null)
                    {
                        newUser.InterestCountyIDs = user.InterestCountyIDs;
                        // insert interested counties;
                        foreach (var countyID in newUser.InterestCountyIDs)
                        {
                            InsertInterestedCounty(connection, transaction, countyID, newUser.ID);
                        }
                    }
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return newUser;
        }


        public static PropertySubscribedUser Update(PropertySubscribedUser user)
        {
            return DbFactory.Connect<PropertySubscribedUser, PropertySubscribedUser>(Update, user);
        }

        public static PropertySubscribedUser Update(DbConnection connection, DbTransaction transaction, PropertySubscribedUser user)
        {
            DbReader r = null;
            PropertySubscribedUser updatedUser = null;
            try
            {
                r = new DbReader("spPropertySubscribedUser_Update", connection, transaction, new Dictionary<string, object>
                {
                    {"intTitleID", user.Title.ID },	
                    {"chrFirstName", user.FirstName },
                    {"chrSurname", user.Surname },
                    {"chrAddress1", user.Address1 },
                    {"chrAddress2", user.Address2 },
                    {"chrAddress3", user.Address3 },
                    {"chrAddressPostcode", user.AddressPostcode },
                    {"intAddressCountryID", user.Country.ID },
                    {"intAddressCountyID", user.County.ID },
                    {"chrPhoneHome", user.PhoneHome },
                    {"chrPhoneMobile", user.PhoneMobile },
                    {"chrPhoneWork", user.PhoneWork },
                    {"chrEmailAddress", user.EmailAddress },
                    {"chrSpecificRequirements", user.SpecificRequirements },
                    {"bitInterestedInValuation", user.InterestedInValuation },
                    {"bitWantsToBuy", user.WantsToBuy },
                    {"bitWantsToRent", user.WantsToRent },
                    {"intPropertyTypeItemID", user.PropertyTypeItemID == 0 ?(object)DBNull.Value : user.PropertyTypeItemID },
                    {"intMinBedrooms", user.MinBedrooms == 0 ? (object)DBNull.Value : user.MinBedrooms },
                    {"intMinBathrooms", user.MinBathrooms == 0 ? (object)DBNull.Value : user.MinBathrooms },
                    {"intQualification", user.QualificationID == 0 ? (object)DBNull.Value : user.QualificationID },
                    {"numMinPrice", user.MinPrice == 0 ? (object)DBNull.Value : user.MinPrice },
                    {"numMaxPrice", user.MaxPrice == 0 ? (object)DBNull.Value: user.MaxPrice },
                    {"bitSubscribed", user.Subscribed },
                    {"bitEmailAlerts", user.EmailAlerts },
                    {"intSubscribedUserID", user.ID },
                    {"intClientID", Settings.ClientID },
                    {"bitOutsideArea", user.OutsideArea ? user.OutsideArea : (object)DBNull.Value },
                    {"bitParking", user.Parking ? user.Parking : (object)DBNull.Value }
                });

                if (r.Read())
                {
                    updatedUser = ReadPropertySubscribedUser(r);

                    if (r != null) r.Close();
                    if (updatedUser != null)
                    {
                        // delete old records
                        DeleteAllInterestedCounties(connection, transaction, updatedUser.ID);

                        // insert new records
                        updatedUser.InterestCountyIDs = user.InterestCountyIDs;

                        foreach (var countyID in updatedUser.InterestCountyIDs)
                        {
                            InsertInterestedCounty(connection, transaction, countyID, updatedUser.ID);
                        }
                    }
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return user;
        }


        public static PropertySubscribedUser Get(long userID)
        {
            return DbFactory.Connect<PropertySubscribedUser, long>(Get, userID);
        }

        public static PropertySubscribedUser Get(DbConnection connection, DbTransaction transaction, long userID)
        {
            PropertySubscribedUser item = null;
            DbReader r = null;

            try
            {
                r = new DbReader("spPropertySubscribedUser_Select", connection, transaction, new Dictionary<string, object>
                {
                    { "intSubscribedUserID", userID }
                });

                if (r.Read())
                {
                    item = ReadPropertySubscribedUser(r);
                }

                if (r != null) r.Close();
                if (item != null)
                    item.InterestCountyIDs = GetUserInterestedCounties(connection, transaction, item.ID);
            }
            finally
            {
                if (r != null) r.Close();
            }

            return item;
        }

        public static PropertySubscribedUser GetByEmail(string email)
        {
            return DbFactory.Connect<PropertySubscribedUser, string>(GetByEmail, email);
        }

        public static PropertySubscribedUser GetByEmail(DbConnection connection, DbTransaction transaction, string email)
        {
            PropertySubscribedUser item = null;
            DbReader r = null;

            try
            {
                r = new DbReader("spPropertySubscribedUserByEmail_Select", connection, transaction, new Dictionary<string, object>
                {
                    { "chrEmail", email }
                });

                if (r.Read())
                {
                    item = ReadPropertySubscribedUser(r);
                }

                if (r != null) r.Close();
                if (item != null)
                    item.InterestCountyIDs = GetUserInterestedCounties(connection, transaction, item.ID);
            }
            finally
            {
                if (r != null) r.Close();
            }

            return item;
        }


        public static List<PropertySubscribedUser> GetAll()
        {
            return DbFactory.Connect<List<PropertySubscribedUser>>(GetAll);
        }

        public static List<PropertySubscribedUser> GetAll(DbConnection connection, DbTransaction transaction)
        {
            List<PropertySubscribedUser> items = new List<PropertySubscribedUser>();
            DbReader r = null;

            try
            {
                r = new DbReader("spPropertySubscribedUser_Select", connection, transaction);

                while (r.Read())
                {
                    items.Add(ReadPropertySubscribedUser(r));
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return items;
        }


        public static ResultSet<PropertySubscribedUser> GetAllPaged(PagingParameter p)
        {
            return DbFactory.Connect<ResultSet<PropertySubscribedUser>, PagingParameter>(GetAllPaged, p);
        }

        public static ResultSet<PropertySubscribedUser> GetAllPaged(DbConnection connection, DbTransaction transaction, PagingParameter p)
        {
            DbReader r = null;
            ResultSet<PropertySubscribedUser> result = null;

            try
            {
                r = new DbReader("spPropertySubscribedUserListPaged_Select", connection, transaction, new Dictionary<string, object>
                {
                    { "PageNumber", p.PageNumber },
                    { "RowsPerPage", p.RowsPerPage },
                    { "SortBy", String.IsNullOrEmpty(p.SortBy) ? (object)DBNull.Value : p.SortBy.ToLower() },
                    { "Direction", String.IsNullOrEmpty(p.SortDirection) ? (object)DBNull.Value : p.SortDirection.ToLower()}
                });

                result = new ResultSet<PropertySubscribedUser>(p.PageNumber, p.RowsPerPage, r, ReadPropertySubscribedUser);

            }
            finally
            {
                if (r != null) r.Close();
            }

            return result;
        }



        public static bool Delete(long userID)
        {
            return DbFactory.Connect<bool, long>(Delete, userID);
        }

        public static bool Delete(DbConnection connection, DbTransaction transaction, long userID)
        {
            return DbFactory.ExecuteNonQuery("spPropertySubscribedUser_Delete", connection, transaction, new Dictionary<string, object> 
            {
                { "intSubscribedUserID", userID }
            }) > 0;
        }

        public static bool DeleteAllInterestedCounties(DbConnection connection, DbTransaction transaction, long userID)
        {
            return DbFactory.ExecuteNonQuery("spPropertySubscribedUserCounty_Delete", connection, transaction, new Dictionary<string, object> { { "intPropertySubscribedUserID", userID } }) > 0;
        }

        public static List<int> GetUserInterestedCounties(DbConnection connection, DbTransaction transaction, long userID)
        {
            DbReader r = null;
            List<int> items = new List<int>();
            try
            {
                r = new DbReader("spPropertySubscribedUserCounty_Select", connection, transaction, new Dictionary<string, object> 
                {
                    { "intPropertySubscribedUserID", userID }
                });
                while (r.Read())
                {
                    items.Add(r.Get<int>("intCountyID"));
                }
            }
            finally
            {
                if (r != null) r.Close();
            }
            return items;
        }


        public static int InsertInterestedCounty(int countyID, long userID)
        {
            return DbFactory.Connect<int, int, long>(InsertInterestedCounty, countyID, userID);
        }

        public static int InsertInterestedCounty(DbConnection connection, DbTransaction transaction, int countyID, long userID)
        {
            DbReader r = null;
            int countyIDResult = 0;
            try
            {
                r = new DbReader("spPropertySubscribedUserCounty_Insert", connection, transaction, new Dictionary<string, object>
                {
                   { "intCountyID", countyID },
                   { "intPropertySubscribedUserID", userID }
                });

                if (r.Read())
                {
                    countyIDResult = r.Get<int>("intCountyID");
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return countyIDResult;
        }


        public static int AlertUsersByPropertyCriteria(Property p, string alertTemplateKey, int? activatedByUserID = null)
        {
            return DbFactory.Connect<int, Property, string, int?>(AlertUsersByPropertyCriteria, p, alertTemplateKey, activatedByUserID);
        }

        public static int AlertUsersByPropertyCriteria(DbConnection connection, DbTransaction transaction, Property p, string alertTemplateKey, int? activatedByUserID = null)
        {
            List<PropertySubscribedUser> items = new List<PropertySubscribedUser>();
            DbReader r = null;
            int usersNotified = 0;
            // retrieve images
            p.Images = DAC_PropertyImage.GetByPropertyID(connection, transaction, p.ID);

            try
            {
                r = new DbReader("spPropertySubscribedUsersByCriteria_Select", connection, transaction, new Dictionary<string, object>
                {
                    { "bitBuy", p.ForSale ? p.ForSale : (object)DBNull.Value },
                    { "bitRent", p.ForRent ? p.ForRent : (object)DBNull.Value},
                    { "intPropertyTypeItemID", p.PropertyTypeItemID != 0 ? p.PropertyTypeItemID : (object)DBNull.Value},
                    { "intPropertyCountyID", p.County != null ? p.County.ID : (object)DBNull.Value},
                    { "intBedrooms", p.BedroomCount != 0 ? p.BedroomCount : (object)DBNull.Value},
                    { "intBathrooms", p.BathroomCount != 0 ? p.BathroomCount : (object)DBNull.Value},
                    { "numPropertyPrice", p.Price > 0 ? p.Price : (object)DBNull.Value},
                    { "numPropertyRental", p.Rental > 0 ? p.Rental : (object)DBNull.Value},
                    { "intClientID", Settings.ClientID },
                    { "intCategoryID", p.CategoryItemID == 0 ? (object)DBNull.Value : p.CategoryItemID }
                });

                while (r.Read())
                {
                    items.Add(ReadPropertySubscribedUser(r));
                }

                if (r != null) r.Close();
                if (items.Count > 0)
                {
                    DataTable dtRegTemplate = DAC_Item.GetItemByTitle("Email templates", alertTemplateKey);
                    if (dtRegTemplate != null && dtRegTemplate.Rows.Count > 0)
                    {
                        string from = dtRegTemplate.GetDataTableValue(0, "chrText2", String.Empty);
                        string displayName = dtRegTemplate.GetDataTableValue(0, "chrText3", String.Empty);
                        string cc = dtRegTemplate.GetDataTableValue(0, "chrText4", String.Empty);
                        string bcc = dtRegTemplate.GetDataTableValue(0, "chrText5", String.Empty);

                        string subject = dtRegTemplate.GetDataTableValue(0, "chrText6", String.Empty);
                        string password = dtRegTemplate.GetDataTableValue(0, "chrText7", String.Empty);

                        // set default sender in case it is empty
                        if (from == String.Empty) from = FluentContent.Settings.FailSafeEmail;


                        foreach (var item in items)
                        {

                            string body = dtRegTemplate.GetDataTableValue(0, "chrText11", String.Empty);
                            string recipients = String.Format("{0};{1}", item.EmailAddress, dtRegTemplate.GetDataTableValue(0, "chrText1", String.Empty));

                            string leadImageUrl = "<img src='{0}' width='200px' alt='property-lead-image' />";
                            if (p.LeadImage == null || String.IsNullOrEmpty(p.LeadImage.GUID)) leadImageUrl = String.Empty;
                            else
                                leadImageUrl = String.Format(leadImageUrl, String.Format("{0}/property-image/{1}", Settings.WebsiteRoot, p.LeadImage.GUID));

                            // replace placeholders
                            body = EmailManager.ReplaceKeys(body, new Dictionary<string, string> 
                            {
                                {"##FIRSTNAME##", item.FirstName},
                                {"##PROPERTY_LEAD_IMAGE##", leadImageUrl },
                                {"##PROPERTY_LINK##", p.WebsitePropertyURL },
                                {"##PROPERTY_TITLE##", p.Title }
                            });
                            body = String.Format("<html><body>{0}</body></html>", body);
                            NetworkCredential cred = new NetworkCredential(from, password);

                            try
                            {
                                bool sent = EmailManager.SendEmail(recipients, from, displayName, subject, body, true, cc, bcc, null, cred);
                                if (sent) usersNotified++;
                                DAC_PropertySubscriberAlert.Insert(connection, transaction, new PropertySubscriberAlert
                                {
                                    ActivatedByCMSUserID = activatedByUserID != null ? (int)activatedByUserID : 0,
                                    PropertyID = p.ID,
                                    PropertySubscriberUserID = item.ID,
                                    EmailAlertItemID = dtRegTemplate.GetDataTableValue<int>(0, "intItemID", 0)
                                });
                            }
                            catch
                            {
                                continue;
                            }
                        }
                    }
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return usersNotified;
        }

        private static PropertySubscribedUser ReadPropertySubscribedUser(DbReader r)
        {
            return new PropertySubscribedUser
            {
                ID = r.Get<long>("intSubscribedUserID"),
                Title = new Title { ID = r.Get<long>("intTitleID"), Description = r.Get<string>("chrTitle") },
                FirstName = r.Get<string>("chrFirstName"),
                Surname = r.Get<string>("chrSurname"),
                Address1 = r.Get<string>("chrAddress1"),
                Address2 = r.Get<string>("chrAddress2"),
                Address3 = r.Get<string>("chrAddress3"),
                AddressPostcode = r.Get<string>("chrAddressPostcode"),
                Country = new Country { ID = r.Get<int>("intAddressCountryID"), Name = r.Get<string>("chrCountry") },
                County = new County { ID = r.Get<int>("intAddressCountyID"), CountyName = r.Get<string>("chrCountyName") },
                PhoneHome = r.Get<string>("chrPhoneHome"),
                PhoneMobile = r.Get<string>("chrPhoneMobile"),
                PhoneWork = r.Get<string>("chrPhoneWork"),
                EmailAddress = r.Get<string>("chrEmailAddress"),
                SpecificRequirements = r.Get<string>("chrSpecificRequirements"),
                InterestedInValuation = r.Get<bool>("bitInterestedInValuation"),
                WantsToBuy = r.Get<bool>("bitWantsToBuy"),
                WantsToRent = r.Get<bool>("bitWantsToRent"),
                PropertyTypeItemID = r.Get<int>("intPropertyTypeItemID"),
                MinBedrooms = r.Get<int>("intMinBedrooms"),
                MinBathrooms = r.Get<int>("intMinBathrooms"),
                QualificationID = r.Get<int>("intQualification"),
                MinPrice = r.Get<decimal>("numMinPrice"),
                MaxPrice = r.Get<decimal>("numMaxPrice"),
                Subscribed = r.Get<bool>("bitSubscribed"),
                EmailAlerts = r.Get<bool>("bitEmailAlerts"),
                DateSubscribed = r.Get<DateTime>("datSubscribed"),
                PropertyType = r.Get<string>("chrPropertyType"),
                Qualification = r.Get<string>("chrQualification"),
                ClientID = r.Get<int>("intClientID"),
                OutsideArea = r.Get<bool>("bitOutsideArea"),
                Parking = r.Get<bool>("bitParking")
            };
        }
    }
}
