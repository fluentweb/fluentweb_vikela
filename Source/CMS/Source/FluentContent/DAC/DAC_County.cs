﻿using DataAccess;
using DataAccess.Base;
using FluentContent.Entities;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.DAC
{
    public class DAC_County
    {
        public static List<County> GetCountiesByCountryID(int countryID)
        {
            return DbFactory.Connect<List<County>, int>(GetCountiesByCountryID, countryID);
        }

        public static List<County> GetCountiesByCountryID(DbConnection connection, DbTransaction transaction, int countryID)
        {
            DbReader r = null;
            List<County> items = new List<County>();

            try
            {
                r = new DbReader("spCountyByCountry_Select", connection, transaction, new Dictionary<string, object> 
                {
                    {"intCountryID", countryID }
                });
                while (r.Read())
                {
                    items.Add(ReadCounty(r));
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return items;
        }

        private static County ReadCounty(DbReader r)
        {
            return new County 
            {
                 CountyName = r.Get<string>("chrCountyName"),
                 ID = r.Get<int>("intCountyID"),
                 CountryID = r.Get<int>("intCountryID")
            };
        }
    }
}
