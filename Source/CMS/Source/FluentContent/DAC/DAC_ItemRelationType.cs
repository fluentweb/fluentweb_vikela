﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;
using DataAccess;

namespace FluentContent.DAC
{
    public class DAC_ItemRelationType
    {
        public static DataTable GetByRelationName(string relationName)
        {
            return DbFactory.Connect<DataTable, string>(GetByRelationName, relationName);
        }

        public static DataTable GetByRelationName(DbConnection connection, DbTransaction transaction, string relationName)
        {
            return DbFactory.GetDataTable("spRelatedItemTypeByRelation_Select", connection, transaction, new Dictionary<string, object> 
            {
                { "RelationshipType", relationName }
            });
        }
    }
}
