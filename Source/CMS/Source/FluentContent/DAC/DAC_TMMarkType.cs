﻿using DataAccess;
using DataAccess.Base;
using FluentContent.Entities;
using FluentContent.Entities.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.DAC
{
    public class DAC_TMMarkType
    {
        public static DataTable GetMarkType()
        {
            return DbFactory.Connect<DataTable>(GetMarkType);
        }

        public static DataTable GetMarkType(DbConnection connection, DbTransaction transaction)
        {
            return DbFactory.GetDataTable("spTMtMarkType_Slect", connection, transaction, new Dictionary<string, object> 
            {
                { "MarkTypeID", (object)DBNull.Value }
            });
        }

        public static TMMarkType GetMarkType(int marktypeid)
        {
            return DbFactory.Connect<TMMarkType, int>(GetMarkType, marktypeid);
        }

        public static TMMarkType GetMarkType(DbConnection connection, DbTransaction transaction, int marktypeid)
        {
            TMMarkType item = null;
            DbReader r = null;

            try
            {
                r = new DbReader("spTMtMarkType_Slect", connection, transaction, new Dictionary<string, object>
                {
                    { "MarkTypeID", marktypeid }
                });

                if (r.Read())
                {
                    item = new TMMarkType(r);
                }

                if (r != null) r.Close();
            }
            finally
            {
                if (r != null) r.Close();
            }

            return item;
        }

        public static TMMarkType ReadProperty(DbReader r)
        {
            return new TMMarkType(r);
        }
    }
}
