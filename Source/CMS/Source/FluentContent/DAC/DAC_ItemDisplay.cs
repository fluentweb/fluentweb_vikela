﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentContent.Entities;
using DataAccess;
using DataAccess.Base;
using System.Data.Common;

namespace FluentContent.DAC
{
    public class DAC_DisplaySetting
    {
        /// <summary>
        /// Retrieves a list of all items ordered by 
        /// </summary>
        /// <returns></returns>
        public static List<DisplaySetting> Get()
        {
            return DbFactory.Connect<List<DisplaySetting>>(Get);
        }

        /// <summary>
        /// Retrieves a list of all pages ordered by PageName
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public static List<DisplaySetting> Get(DbConnection connection, DbTransaction transaction)
        {
            List<DisplaySetting> items = new List<DisplaySetting>();
            DbReader r = null;
            try
            {
                r = new DbReader("spItemDisplayList_Select", connection, transaction, new Dictionary<string, object> { { "ClientID", Settings.ClientID } });
                while (r.Read())
                {
                    if (r.Get<int>("intMaxNumberItems") != 1)
                        items.Add(ReadDisplaySetting(r));
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return items;
        }

        public static DisplaySetting GetByDisplayName(string displayName)
        {
            return DbFactory.Connect<DisplaySetting, string>(GetByDisplayName, displayName);
        }

        public static DisplaySetting GetByDisplayName(DbConnection connection, DbTransaction transaction, string displayName)
        {
            DisplaySetting setting = null;

            DbReader r = null;
            try
            {
                r = new DbReader("spItemDisplaySettingsByName_Select", connection, transaction, new Dictionary<string, object> 
                {
                    { "ClientID", Settings.ClientID },
                    { "DisplayName", displayName }
                });

                if(r.Read())
                {
                    setting = ReadDisplaySetting(r);
                    if (setting != null)
                        setting.PageSize = r.Get<int>("intPageSize");
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return setting;
        }


        private static DisplaySetting ReadDisplaySetting(DbReader r)
        {
            return new DisplaySetting
            {
                ID = r.Get<int>("intItemDisplayID"),
                ItemDisplayName = r.Get<string>("chrItemDisplay"),
                ItemImageTypeID = r.Get<int>("intItemTypeID"),
                ItemTextTypeID = r.Get<int>("intItemTextTypeID"),
                ItemTypeID = r.Get<int>("intItemTypeID"),
                MaxNumberItems = r.Get<int>("intMaxNumberItems"),
                OrderBy = r.Get<char>("chrOrderBy"),
                Randomise = r.Get<bool>("bitRandomise")                
            };
        }
    }
}
