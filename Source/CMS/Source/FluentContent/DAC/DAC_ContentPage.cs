﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentContent.Entities;
using System.Data.Common;
using DataAccess.Base;
using DataAccess;

namespace FluentContent.DAC
{
    public class DAC_ContentPage
    {
        /// <summary>
        /// Retrieves a list of all pages ordered by PageName
        /// </summary>
        /// <returns></returns>
        public static List<ContentPage> GetPages()
        {
            return DbFactory.Connect<List<ContentPage>>(GetPages);
        }

        /// <summary>
        /// Retrieves a list of all pages ordered by PageName
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public static List<ContentPage> GetPages(DbConnection connection, DbTransaction transaction)
        {
            List<ContentPage> items = new List<ContentPage>();
            DbReader r = null;
            try
            {
                r = new DbReader("spPages_Select", connection, transaction, new Dictionary<string, object> { { "ClientID", Settings.ClientID } });
                while (r.Read())
                {
                    ContentPage c = ReadContentPage(r);
                    c.UrlEditable = r.Get<bool>("bitPageUrlEditable");
                    items.Add(c);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return items;
        }

        public static bool PageUrlExists(string url, int pageID)
        {
            return DbFactory.Connect<bool, string, int>(PageUrlExists, url, pageID);
        }

        public static bool PageUrlExists(DbConnection connection, DbTransaction transaction, string url, int pageID)
        {
            return DbFactory.ExecuteScalar<int>("spPageURLExists", connection, transaction, new Dictionary<string, object> 
            {
                { "URL", url },
                { "PageID", pageID == -1 ? (object)DBNull.Value : pageID },
                { "ClientID", Settings.ClientID }
            }) > 0;
        }


        private static ContentPage ReadContentPage(DbReader r)
        {
            return new ContentPage
            {
                ClientID = r.Get<int>("intClientID"),
                Description = r.Get<string>("chrPageDescription"),
                ID = r.Get<int>("intPageID"),
                Keywords = r.Get<string>("chrPageKeywords"),
                PageName = r.Get<string>("chrPage"),
                Title = r.Get<string>("chrPageTitle"),
                Url = r.Get<string>("chrPageURL"),
                PageFullURL = r.Get<string>("chrPageFullURL"),
                TemplateName = r.Get<string>("chrPageTemplateName")
            };
        }
    }
}
