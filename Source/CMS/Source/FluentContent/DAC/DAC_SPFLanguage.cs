﻿using DataAccess;
using FluentContent.Entities.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.DAC
{
    public class DAC_SPFLanguage
    {
        public static DataTable GetLanguageByLanguage(int languageId = 0)
        {
            return DbFactory.Connect<DataTable, int>(GetLanguageByLanguage, languageId);
        }

        public static DataTable GetLanguageByLanguage(DbConnection connection, DbTransaction transaction, int languageId)
        {
            return DbFactory.GetDataTable("spSPFLanguageByLanguage_Select", connection, transaction, new Dictionary<string, object> 
            {
                { "LanguageID", languageId == 0 ? (object)DBNull.Value : languageId },
            });
        }

    }
}
