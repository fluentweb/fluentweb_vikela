﻿using FluentContent.Entities;
using DataAccess;
using DataAccess.Base;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.DAC
{
    public class DAC_PropertyAsset
    {
        public static List<PropertyAsset> Get(int propertyID)
        {
            return DbFactory.Connect<List<PropertyAsset>, int>(Get, propertyID);
        }

        public static List<PropertyAsset> Get(DbConnection connection, DbTransaction transaction, int propertyID)
        {
            List<PropertyAsset> items = new List<PropertyAsset>();
            DbReader r = null;
            try
            {
                r = new DbReader("spPropertyAssetByPropertyID_Select", connection, transaction, new Dictionary<string, object> 
                {
                    {"intPropertyID", propertyID }
                });
                while (r.Read())
                {
                    items.Add(ReadPropertyAsset(r));
                }
            }
            finally
            {
                if (r != null) r.Close();
            }
            return items;
        }

        public static PropertyAsset Insert(PropertyAsset asset)
        {
            return DbFactory.Connect<PropertyAsset, PropertyAsset>(Insert, asset);
        }

        public static PropertyAsset Insert(DbConnection connection, DbTransaction transaction, PropertyAsset asset)
        {
            PropertyAsset item = null;
            DbReader r = null;
            try
            {
                r = new DbReader("spPropertyAsset_Insert", connection, transaction, new Dictionary<string, object> 
                {
                    { "intAssetItemID", asset.AssetItemID },
                    { "intPropertyID", asset.PropertyID },
                    { "intLength", asset.Length },
                    { "intWidth", asset.Width },
                    { "intSize", asset.Size },
                    { "intFloorNumber", asset.FloorNumber },
                    { "chrDescription", asset.Description }
                });
                if (r.Read())
                {
                    item = ReadPropertyAsset(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }
            return item;
        }

        public static PropertyAsset Update(PropertyAsset asset)
        {
            return DbFactory.Connect<PropertyAsset, PropertyAsset>(Update, asset);
        }

        public static PropertyAsset Update(DbConnection connection, DbTransaction transaction, PropertyAsset asset)
        {
            PropertyAsset item = null;
            DbReader r = null;
            try
            {
                r = new DbReader("spPropertyAsset_Update", connection, transaction, new Dictionary<string, object> 
                {
                    { "intAssetID", asset.ID },
                    { "intAssetItemID", asset.AssetItemID },
                    { "intPropertyID", asset.PropertyID },
                    { "intLength", asset.Length },
                    { "intWidth", asset.Width },
                    { "intSize", asset.Size },
                    { "intFloorNumber", asset.FloorNumber },
                    { "chrDescription", asset.Description }
                });
                if (r.Read())
                {
                    item = ReadPropertyAsset(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }
            return item;
        }

        public static bool Delete(int assetID)
        {
            return DbFactory.Connect<bool, int>(Delete, assetID);
        }

        public static bool Delete(DbConnection connection, DbTransaction transaction, int assetID)
        {
            return DbFactory.ExecuteNonQuery("spPropertyAsset_Delete", connection, transaction, new Dictionary<string, object> 
            {
                 { "intAssetID", assetID }
            }) > 0;
        }


        // HELPERS
        private static PropertyAsset ReadPropertyAsset(DbReader r)
        {
            return new PropertyAsset
            {
                AssetItemID = r.Get<int>("intAssetItemID"),
                AssetTypeName = r.Get<string>("chrItemTitle"),
                Created = r.Get<DateTime>("datCreated"),
                Description = r.Get<string>("chrDescription"),
                FloorNumber = r.Get<int>("intFloorNumber"),
                ID = r.Get<int>("intAssetID"),
                Length = r.Get<double>("intLength"),
                PropertyID = r.Get<int>("intPropertyID"),
                Updated = r.Get<DateTime>("datUpdated"),
                Width = r.Get<double>("intWidth"),
                Size = r.Get<double>("intSize")
            };
        }
    }
}
