﻿using DataAccess;
using DataAccess.Base;
using FluentContent.Entities;
using FluentContent.Entities.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.DAC
{
    public class DAC_TMCountryState
    {
        public static DataTable GetDAC_CountryStateByCountry(int countryId)
        {
            return DbFactory.Connect<DataTable,int>(GetDAC_CountryStateByCountry,countryId);
        }

        public static DataTable GetDAC_CountryStateByCountry(DbConnection connection, DbTransaction transaction, int countryId)
        {
            return DbFactory.GetDataTable("spTMtCountryState_Select", connection, transaction, new Dictionary<string, object> 
            {
               { "ParentCountryID", countryId },
               { "MemberCountryID", (object)DBNull.Value }
            });
        }

        //public static TMCountryState GetDAC_CountryStateByCountry(int countryId)
        //{
        //    return DbFactory.Connect<TMCountryState, int>(GetDAC_CountryStateByCountry, countryId);
        //}

        //public static TMCountryState GetDAC_CountryStateByCountry(DbConnection connection, DbTransaction transaction, int countryId)
        //{
        //    TMCountryState item = null;
        //    DbReader r = null;

        //    try
        //    {
        //        r = new DbReader("spTMtCountryState_Select", connection, transaction, new Dictionary<string, object>
        //        {
        //            { "ParentCountryID", countryId },
        //            { "MemberCountryID", (object)DBNull.Value }
        //        });

        //        if (r.Read())
        //        {
        //            item = new TMCountryState(r);
        //        }

        //        if (r != null) r.Close();
        //    }
        //    finally
        //    {
        //        if (r != null) r.Close();
        //    }

        //    return item;
        //}

        public static TMCountryState Insert(TMCountryState tmcs)
        {
            return DbFactory.Connect<TMCountryState, TMCountryState>(Insert, tmcs);
        }

        public static TMCountryState Insert(DbConnection connection, DbTransaction transaction, TMCountryState tmcs)
        {
            DbReader r = null;

            try
            {
                r = new DbReader("spTMtCountryState_Insert", connection, transaction, new Dictionary<string, object>
                {
                    {"ParentCountryID",tmcs.CountryID},
                    {"MemberCountryID",tmcs.MemberCountryID}

                });

                if (r.Read())
                {
                    tmcs = new TMCountryState(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return tmcs;
        }

        public static bool Delete(TMCountryState tmcs)
        {
            return DbFactory.Connect<bool, TMCountryState>(Delete, tmcs);
        }

        public static bool Delete(DbConnection connection, DbTransaction transaction, TMCountryState tmcs)
        {
            return DbFactory.ExecuteNonQuery("spTMtCountryState_Delete", connection, transaction, new Dictionary<string, object> 
            {
                { "ParentCountryID", tmcs.CountryID },
                { "MemberCountryID", tmcs.MemberCountryID }
            }) > 0;
        }

        public static TMCountryState ReadProperty(DbReader r)
        {
            return new TMCountryState(r);
        }
    }
}
