﻿using DataAccess;
using DataAccess.Base;
using FluentContent.Entities;
using FluentContent.Entities.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.DAC
{
   public class DAC_TMCountryAdditionalRequirement
    {
       public static DataTable GetCountryAdditionalRequirement()
        {
            return DbFactory.Connect<DataTable>(GetCountryAdditionalRequirement);
        }

       public static DataTable GetCountryAdditionalRequirement(DbConnection connection, DbTransaction transaction)
        {
            return DbFactory.GetDataTable("spTMtCountryAdditionalRequirement_Select", connection, transaction, new Dictionary<string, object> 
            {
                { "CountryAdditionalRequirementID", (object)DBNull.Value }
            });
        }


       public static TMCountryAdditionalRequirement GetCountryAdditionalRequirement(int countryAdditionalRequirementID)
       {
           return DbFactory.Connect<TMCountryAdditionalRequirement, int>(GetCountryAdditionalRequirement, countryAdditionalRequirementID);
       }

       public static TMCountryAdditionalRequirement GetCountryAdditionalRequirement(DbConnection connection, DbTransaction transaction, int countryAdditionalRequirementID)
       {
           TMCountryAdditionalRequirement item = null;
           DbReader r = null;

           try
           {
               r = new DbReader("spTMtCountryAdditionalRequirement_Select", connection, transaction, new Dictionary<string, object>
                {
                    { "CountryAdditionalRequirementID", countryAdditionalRequirementID }
                });

               if (r.Read())
               {
                   item = new TMCountryAdditionalRequirement(r);
               }

               if (r != null) r.Close();
           }
           finally
           {
               if (r != null) r.Close();
           }

           return item;
       }

       public static TMCountryAdditionalRequirement Insert(TMCountryAdditionalRequirement tmcar)
       {
           return DbFactory.Connect<TMCountryAdditionalRequirement, TMCountryAdditionalRequirement>(Insert, tmcar);
       }

       public static TMCountryAdditionalRequirement Insert(DbConnection connection, DbTransaction transaction, TMCountryAdditionalRequirement tmcar)
       {
           DbReader r = null;

           try
           {
               r = new DbReader("spTMtCountryAdditionalRequirement_Insert", connection, transaction, new Dictionary<string, object>
                {
                    {"CountryID",tmcar.CountryID},
                    {"RequirementQuestion",tmcar.RequirementQuestion},
                    {"QuestionType",tmcar.QuestionType},

                });

               if (r.Read())
               {
                   tmcar = new TMCountryAdditionalRequirement(r);
               }
           }
           finally
           {
               if (r != null) r.Close();
           }

           return tmcar;
       }

       public static TMCountryAdditionalRequirement Update(TMCountryAdditionalRequirement tmcar)
       {
           return DbFactory.Connect<TMCountryAdditionalRequirement, TMCountryAdditionalRequirement>(Update, tmcar);
       }

       public static TMCountryAdditionalRequirement Update(DbConnection connection, DbTransaction transaction, TMCountryAdditionalRequirement tmcar)
       {
           DbReader r = null;

           try
           {
               r = new DbReader("spTMtCountryAdditionalRequirement_Update", connection, transaction, new Dictionary<string, object>
                {
                    {"CountryAdditionalRequirementID",tmcar.CountryAdditionalRequirementID},
                    {"CountryID",tmcar.CountryID},
                    {"RequirementQuestion",tmcar.RequirementQuestion},
                    {"QuestionType",tmcar.QuestionType},
                });

               if (r.Read())
               {
                   tmcar = new TMCountryAdditionalRequirement(r);
               }
           }
           finally
           {
               if (r != null) r.Close();
           }

           return tmcar;
       }
       public static bool Delete(TMCountryAdditionalRequirement tmcar)
       {
           return DbFactory.Connect<bool, TMCountryAdditionalRequirement>(Delete, tmcar);
       }

       public static bool Delete(DbConnection connection, DbTransaction transaction, TMCountryAdditionalRequirement tmcar)
       {
           return DbFactory.ExecuteNonQuery("spTMtCountryAdditionalRequirement_Delete", connection, transaction, new Dictionary<string, object> 
            {
                { "CountryAdditionalRequirementID", tmcar.CountryAdditionalRequirementID }
            }) > 0;
       }

       public static TMCountryAdditionalRequirement ReadProperty(DbReader r)
       {
           return new TMCountryAdditionalRequirement(r);
       }


    }
}
