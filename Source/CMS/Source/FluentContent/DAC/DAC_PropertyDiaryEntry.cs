﻿using DataAccess;
using DataAccess.Base;
using FluentContent.Entities;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.DAC
{
    public class DAC_PropertyDiaryEntry
    {
        public static int Insert(PropertyDiaryEntry f)
        {
            return DbFactory.Connect<int, PropertyDiaryEntry>(Insert, f);
        }

        public static int Insert(DbConnection connection, DbTransaction transaction, PropertyDiaryEntry a)
        {
            return DbFactory.ExecuteScalar<int>("spPropertyDiaryEntry_INSERT", connection, transaction, new Dictionary<string, object>
                {
                    { "datEntry", a.datEntry},
                    { "chrUser", a.chrUser },
                    { "chrNotes", a.chrNotes },
                    { "intPropertyID", a.intPropertyID}
                });
        }

        // BY ID
        public static PropertyDiaryEntry GetPropertyDiaryEntryByID(int intDiaryEntry)
        {
            return DbFactory.Connect<PropertyDiaryEntry, int>(GetPropertyDiaryEntryByID, intDiaryEntry);
        }

        public static PropertyDiaryEntry GetPropertyDiaryEntryByID(DbConnection connection, DbTransaction transaction, int intDiaryEntry)
        {
            PropertyDiaryEntry item = null;
            DbReader r = null;

            try
            {
                r = new DbReader("spPropertyDiaryEntry_SELECT", connection, transaction, new Dictionary<string, object> 
                {
                    { "intDiaryEntry", intDiaryEntry } 
                });

                if (r.Read())
                {
                    item = ReadPropertyDiaryEntry(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return item;
        }

        // DELETE
        public static bool Delete(int intDiaryEntry)
        {
            return DbFactory.Connect<bool, int>(Delete, intDiaryEntry);
        }

        public static bool Delete(DbConnection connection, DbTransaction transaction, int intDiaryEntry)
        {
            return DbFactory.ExecuteNonQuery("spPropertyDiaryEntry_DELETE", connection, transaction, new Dictionary<string, object> 
            {
                {"intDiaryEntry", intDiaryEntry }
            }) > 0;
        }


        //Update

        public static int Update(PropertyDiaryEntry f)
        {
            return DbFactory.Connect<int, PropertyDiaryEntry>(Update, f);
        }

        public static int Update(DbConnection connection, DbTransaction transaction, PropertyDiaryEntry a)
        {
            return DbFactory.ExecuteScalar<int>("spPropertyDiaryEntry_Update", connection, transaction, new Dictionary<string, object>
                {
                    { "intDiaryEntry", a.intDiaryEntry},
                    { "datEntry", a.datEntry},
                    { "chrUser", a.chrUser },
                    { "chrNotes", a.chrNotes },
                    { "intPropertyID", a.intPropertyID}
                });
        }


        // HELPERS
        private static PropertyDiaryEntry ReadPropertyDiaryEntry(DbReader r)
        {
            return new PropertyDiaryEntry
            {
                intDiaryEntry= r.Get<int>("intDiaryEntry"),
                datEntry = r.Get<DateTime>("datEntry"),
                chrUser = r.Get<string>("chrUser"),
                chrNotes = r.Get<string>("chrNotes"),
                intPropertyID = r.Get<int>("intPropertyID")
               
            };
        }
    }
}
