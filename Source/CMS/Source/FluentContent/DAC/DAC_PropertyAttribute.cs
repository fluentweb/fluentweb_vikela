﻿using DataAccess;
using DataAccess.Base;
using FluentContent.Entities;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.DAC
{
    public class DAC_PropertyAttribute
    {

        public static List<PropertyAttribute> GetAll(int propertyID)
        {
            return DbFactory.Connect<List<PropertyAttribute>, int>(GetAll, propertyID);
        }

        public static List<PropertyAttribute> GetAll(DbConnection connection, DbTransaction transaction, int propertyID)
        {
            List<PropertyAttribute> items = new List<PropertyAttribute>();
            DbReader r = null;

            try
            {
                r = new DbReader("spPropertyAttributes_Select", connection, transaction, new Dictionary<string, object>
                {
                    { "intPropertyID", propertyID }
                });

                while (r.Read())
                {
                    PropertyAttribute att = ReadPropertyAttribute(r);
                    if (att != null)
                    {
                        att.AttributeTypeItemDataType = r.Get<string>("chrDataType");
                        items.Add(att);
                    }
                }
                if (r != null) r.Close();

                foreach (var item in items)
                {
                    item.Item = DbFactory.GetDataTable("spItemByID_Select", connection, transaction, new Dictionary<string, object> 
                    {
                        {"ItemID", item.AttributeTypeItemID }
                    });
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return items;
        }

        public static PropertyAttribute Get(PropertyAttribute a)
        {
            return DbFactory.Connect<PropertyAttribute, PropertyAttribute>(Get, a);
        }

        public static PropertyAttribute Get(DbConnection connection, DbTransaction transaction, PropertyAttribute a)
        {
            PropertyAttribute item = null;
            DbReader r = null;

            try
            {
                r = new DbReader("spPropertyAttribute_Select", connection, transaction, new Dictionary<string, object>
                {
                    { "intPropertyAttributeID", a.ID },
                    { "intPropertyAttributeItemID", a.AttributeTypeItemID },
                    { "intPropertyID", a.PropertyID }
                });

                if (r.Read())
                {
                    item = ReadPropertyAttribute(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return item;
        }


        public static PropertyAttribute Insert(PropertyAttribute a)
        {
            return DbFactory.Connect<PropertyAttribute, PropertyAttribute>(Insert, a);
        }

        public static PropertyAttribute Insert(DbConnection connection, DbTransaction transaction, PropertyAttribute a)
        {
            DbReader r = null;

            try
            {
                r = new DbReader("spPropertyAttribute_Insert", connection, transaction, new Dictionary<string, object>
                {
                    { "intPropertyAttributeItemID", a.AttributeTypeItemID },
                    { "intPropertyID", a.PropertyID },
                    { "chrValue", a.Value }
                });

                if (r.Read())
                {
                    a = ReadPropertyAttribute(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return a;
        }


        public static List<PropertyAttribute> UpdateAll(List<PropertyAttribute> attributes)
        {
            return DbFactory.Connect<List<PropertyAttribute>, List<PropertyAttribute>>(UpdateAll, attributes);
        }

        public static List<PropertyAttribute> UpdateAll(DbConnection connection, DbTransaction transaction, List<PropertyAttribute> attributes)
        {
            if (attributes == null) return null;

            List<PropertyAttribute> updated = new List<PropertyAttribute>();

            foreach (var a in attributes)
            {
                updated.Add(Update(connection, transaction, a));
            }

            return updated;
        }

        public static PropertyAttribute Update(PropertyAttribute a)
        {
            return DbFactory.Connect<PropertyAttribute, PropertyAttribute>(Update, a);
        }

        public static PropertyAttribute Update(DbConnection connection, DbTransaction transaction, PropertyAttribute a)
        {
            DbReader r = null;

            try
            {
                r = new DbReader("spPropertyAttribute_Update", connection, transaction, new Dictionary<string, object>
                {
                    { "intPropertyAttributeID", a.ID },
                    { "intPropertyAttributeItemID", a.AttributeTypeItemID },
                    { "intPropertyID", a.PropertyID },
                    { "chrValue", a.Value }
                });

                if (r.Read())
                {
                    a = ReadPropertyAttribute(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return a;
        }


        public static bool Delete(PropertyAttribute a)
        {
            return DbFactory.Connect<bool, PropertyAttribute>(Delete, a);
        }

        public static bool Delete(DbConnection connection, DbTransaction transaction, PropertyAttribute a)
        {
            return DbFactory.ExecuteNonQuery("spPropertyAttribute_Delete", connection, transaction, new Dictionary<string, object> 
            {
                { "intPropertyAttributeID", a.ID },
                { "intPropertyAttributeItemID", a.AttributeTypeItemID },
                { "intPropertyID", a.PropertyID }
            }) > 0;
        }

        // Helpers


        public static List<string> GetDistinctAttributes(int attributeItemTypeID)
        {
            return DbFactory.Connect<List<string>, int>(GetDistinctAttributes, attributeItemTypeID);
        }

        public static List<string> GetDistinctAttributes(DbConnection connection, DbTransaction transaction, int attributeItemID)
        {
            List<string> items = new List<string>();
            DbReader r = null;

            try
            {
                r = new DbReader("spPropertyAttributeDistinct_Select", connection, transaction, new Dictionary<string, object>
                {
                    { "intClientID", Settings.ClientID },
                    { "intAttributeItemID", attributeItemID}
                });

                while (r.Read())
                {
                    items.Add(r.Get<string>("chrValue"));
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return items;
        }

        private static PropertyAttribute ReadPropertyAttribute(DbReader r)
        {
            return new PropertyAttribute
            {
                ID = r.Get<int>("intPropertyAttributeID"),
                AttributeTypeItemID = r.Get<int>("intPropertyAttributeItemID"),
                PropertyID = r.Get<int>("intPropertyID"),
                Value = r.Get<string>("chrValue"),
                AttributeTypeItemName = r.Get<string>("AttributeTypeName")
            };
        }

    }
}
