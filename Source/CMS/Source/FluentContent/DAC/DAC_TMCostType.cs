﻿using DataAccess;
using DataAccess.Base;
using FluentContent.Entities;
using FluentContent.Entities.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.DAC
{
    public class DAC_TMCostType
    {
        public static DataTable GetCostType()
        {
            return DbFactory.Connect<DataTable>(GetCostType);
        }

        public static DataTable GetCostType(DbConnection connection, DbTransaction transaction)
        {
            return DbFactory.GetDataTable("spTMtCostType_Select", connection, transaction, new Dictionary<string, object> 
            {
                { "CostTypeID", (object)DBNull.Value }
            });
        }

        public static TMCostType GetCostType(int costTypeId)
        {
            return DbFactory.Connect<TMCostType, int>(GetCostType, costTypeId);
        }

        public static TMCostType GetCostType(DbConnection connection, DbTransaction transaction, int costTypeId)
        {
            TMCostType item = null;
            DbReader r = null;

            try
            {
                r = new DbReader("spTMtCostType_Select", connection, transaction, new Dictionary<string, object>
                {
                    { "CostTypeID", costTypeId }
                });

                if (r.Read())
                {
                    item = new TMCostType(r);
                }

                if (r != null) r.Close();
            }
            finally
            {
                if (r != null) r.Close();
            }

            return item;
        }

        public static TMCostType Insert(TMCostType tmc)
        {
            return DbFactory.Connect<TMCostType, TMCostType>(Insert, tmc);
        }

        public static TMCostType Insert(DbConnection connection, DbTransaction transaction, TMCostType tmc)
        {
            DbReader r = null;

            try
            {
                r = new DbReader("spTMtCostType_Insert", connection, transaction, new Dictionary<string, object>
                {
                    {"TypeCode",tmc.CostTypeCode},
                    {"TypeDesc",tmc.CostType},
                    {"PerProduct",tmc.PerProduct},
                    {"PerMark",tmc.PerMark},
                    {"PerCountry",tmc.PerCountry},
                    {"PerClass",tmc.PerClass},
                    {"PerState",tmc.PerState}
                });

                if (r.Read())
                {
                    tmc = new TMCostType(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return tmc;
        }

        public static TMCostType Update(TMCostType tmc)
        {
            return DbFactory.Connect<TMCostType, TMCostType>(Update, tmc);
        }

        public static TMCostType Update(DbConnection connection, DbTransaction transaction, TMCostType tmc)
        {
            DbReader r = null;

            try
            {
                r = new DbReader("spTMtCostType_Update", connection, transaction, new Dictionary<string, object>
                {
                    {"CostTypeID",tmc.CostTypeID},
                    {"TypeCode",tmc.CostTypeCode},
                    {"TypeDesc",tmc.CostType},
                    {"PerProduct",tmc.PerProduct},
                    {"PerMark",tmc.PerMark},
                    {"PerCountry",tmc.PerCountry},
                    {"PerClass",tmc.PerClass},
                    {"PerState",tmc.PerState}
                });

                if (r.Read())
                {
                    tmc = new TMCostType(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return tmc;
        }

        public static bool Delete(TMCostType tmc)
        {
            return DbFactory.Connect<bool, TMCostType>(Delete, tmc);
        }

        public static bool Delete(DbConnection connection, DbTransaction transaction, TMCostType tmc)
        {
            return DbFactory.ExecuteNonQuery("spTMtCostType_Delete", connection, transaction, new Dictionary<string, object> 
            {
                { "CostTypeID", tmc.CostTypeID }
            }) > 0;
        }

        public static TMCostType ReadProperty(DbReader r)
        {
            return new TMCostType(r);
        }

    }
}
