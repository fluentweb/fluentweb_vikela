﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using DataAccess.Base;
using FluentContent.Entities;
using System.Data.Common;

namespace FluentContent.DAC
{
    public class DAC_ItemType
    {
        /// <summary>
        /// Retrieves a list of all items ordered by 
        /// </summary>
        /// <returns></returns>
        public static List<ItemType> Get(bool isAdmin)
        {
            return DbFactory.Connect<List<ItemType>, bool>(Get, isAdmin);
        }

        /// <summary>
        /// Retrieves a list of all itemTypes ordered by PageName
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public static List<ItemType> Get(DbConnection connection, DbTransaction transaction, bool isAdmin)
        {
            List<ItemType> items = new List<ItemType>();
            DbReader r = null;
            try
            {
                r = new DbReader("spItemTypeForMenu_Select", connection, transaction, new Dictionary<string, object> 
                {
                    { "ClientID", Settings.ClientID },
                    { "IsAdmin", isAdmin } 
                });

                while (r.Read())
                {
                    items.Add(ReadItemType(r));
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return items;
        }

        /// <summary>
        /// Retrieved ItemType by item ID
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="itemTypeID"></param>
        /// <returns></returns>
        public static ItemType Get(DbConnection connection, DbTransaction transaction, int itemTypeID)
        {
            DbReader r = null;
            ItemType item = null;

            try
            {
                r = new DbReader("spItemTypeByID_Select", connection, transaction, new Dictionary<string, object> 
                { 
                    { "ClientID", Settings.ClientID }, 
                    { "ID", itemTypeID } 
                });

                if (r.Read())
                {
                    item = ReadItemType(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return item;
        }

        /// <summary>
        /// Retrieves ItemType by item ID
        /// </summary>
        /// <param name="itemTypeID"></param>
        /// <returns></returns>
        public static ItemType Get(int itemTypeID)
        {
            return DbFactory.Connect<ItemType, int>(Get, itemTypeID);
        }

        private static ItemType ReadItemType(DbReader r)
        {
            return new ItemType
            {
                ClientID = r.Get<int>("intClientID"),
                ItemTypeName = r.Get<string>("chrItemTypeChr"),
                ID = r.Get<int>("intItemTypeID"),
                Height = r.Get<int>("intWidth", 0),
                Width = r.Get<int>("intHeight", 0)
            };
        }
    }
}
