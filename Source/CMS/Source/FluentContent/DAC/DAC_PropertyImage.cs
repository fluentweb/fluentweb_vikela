﻿
using DataAccess;
using DataAccess.Base;
using FluentContent.Entities;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.DAC
{
    public class DAC_PropertyImage
    {
        public static int MinPortraitImageHeight { get; set; }
        public static int MinLandscapeImageWidth { get; set; }

        public static int MaxImageHeight { get; set; }
        public static int MaxImageWidth { get; set; }

        public static PropertyImage Get(int imageID)
        {
            return DbFactory.Connect<PropertyImage, int>(Get, imageID);
        }

        public static PropertyImage Get(DbConnection connection, DbTransaction transaction, int imageID)
        {
            PropertyImage item = null;
            DbReader r = null;

            try
            {
                r = new DbReader("spPropertyImage_Select", connection, transaction, new Dictionary<string, object> 
                {
                    { "intImageID", imageID }
                });

                if (r.Read())
                {
                    item = ReadPropertyImage(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return item;
        }


        public static PropertyImage GetByGUID(string guid)
        {
            return DbFactory.Connect<PropertyImage, string>(GetByGUID, guid);
        }

        public static PropertyImage GetByGUID(DbConnection connection, DbTransaction transaction, string guid)
        {
            PropertyImage item = null;
            DbReader r = null;

            try
            {
                r = new DbReader("spPropertyImageByGUID_Select", connection, transaction, new Dictionary<string, object> 
                {
                    { "chrImageGUID", guid }
                });

                if (r.Read())
                {
                    item = ReadPropertyImage(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return item;
        }


        public static List<PropertyImage> GetByPropertyID(int propertyID)
        {
            return DbFactory.Connect<List<PropertyImage>, int>(GetByPropertyID, propertyID);
        }

        public static List<PropertyImage> GetByPropertyID(DbConnection connection, DbTransaction transaction, int propertyID)
        {
            List<PropertyImage> items = new List<PropertyImage>();
            DbReader r = null;

            try
            {
                r = new DbReader("spPropertyImageByPropertyID_Select", connection, transaction, new Dictionary<string, object> 
                {
                    { "intPropertyID", propertyID }
                });

                while (r.Read())
                {
                    items.Add(ReadPropertyImage(r));
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return items;
        }


        public static PropertyImage Insert(PropertyImage image)
        {
            return DbFactory.Connect<PropertyImage, PropertyImage>(Insert, image);
        }

        public static PropertyImage Insert(DbConnection connection, DbTransaction transaction, PropertyImage image)
        {
            PropertyImage item = null;
            DbReader r = null;

            try
            {
                r = new DbReader("spPropertyImage_Insert", connection, transaction, new Dictionary<string, object> 
                {
                    { "intPropertyID", image.PropertyID },
                    { "chrImageFilename", image.Filename },
                    { "chrImageGUID", image.GUID },
                    { "chrImageDescription", image.Description },
                    { "bitIsLeadImage", image.IsLeadImage },
                    { "intWidth", image.Width },
                    { "intHeight", image.Height }
                });

                if (r.Read())
                {
                    item = ReadPropertyImage(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return item;
        }

        public static PropertyImage Update(PropertyImage image)
        {
            return DbFactory.Connect<PropertyImage, PropertyImage>(Update, image);
        }

        public static PropertyImage Update(DbConnection connection, DbTransaction transaction, PropertyImage image)
        {
            PropertyImage item = null;
            DbReader r = null;

            try
            {
                r = new DbReader("spPropertyImage_Update", connection, transaction, new Dictionary<string, object> 
                {
                    { "intImageID", image.ID },
                    { "intPropertyID", image.PropertyID },
                    { "chrImageFilename", image.Filename },
                    { "chrImageGUID", image.GUID },
                    { "chrImageDescription", image.Description },
                    { "bitIsLeadImage", image.IsLeadImage },
                    { "intWidth", image.Width },
                    { "intHeight", image.Height }
                });

                if (r.Read())
                {
                    item = ReadPropertyImage(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return item;
        }


        public static bool Delete(int imageID)
        {
            return DbFactory.Connect<bool, int>(Delete, imageID);
        }

        public static bool Delete(DbConnection connection, DbTransaction transaction, int imageID)
        {
            return DbFactory.ExecuteNonQuery("spPropertyImage_Delete", connection, transaction, new Dictionary<string, object> 
            {
                { "intImageID", imageID }
            }) > 0;
        }

        // HELPERS


        private static PropertyImage ReadPropertyImage(DbReader r)
        {
            return new PropertyImage
            {
                Created = r.Get<DateTime>("datCreated"),
                Description = r.Get<string>("chrImageDescription"),
                Filename = r.Get<string>("chrImageFilename"),
                GUID = r.Get<string>("chrImageGUID"),
                Height = r.Get<int>("intHeight"),
                ID = r.Get<int>("intImageID"),
                IsLeadImage = r.Get<bool>("bitIsLeadImage"),
                PropertyID = r.Get<int>("intPropertyID"),
                Updated = r.Get<DateTime>("datUpdated"),
                Width = r.Get<int>("intWidth")
            };
        }

    }
}
