﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using FluentContent.Entities;
using DataAccess;
using DataAccess.Base;

namespace FluentContent.DAC
{
    public class DAC_UserDepartment
    {
        // Insert

        public static int Insert(DbConnection connection, DbTransaction transaction, UserDepartment d)
        {
            return DbFactory.ExecuteNonQuery("spUserDepartment_Insert", connection, transaction, new Dictionary<string, object> 
            {
                { "intParentDepartmentID", d.ParentDepartmentID },
                { "chrDepartmentName", d.Name },
                { "intClientID", Settings.ClientID },
                { "intEntityID", d.EntityID }
            });
        }

        public static int Insert(UserDepartment d)
        {
            return DbFactory.Connect<int, UserDepartment>(Insert, d);
        }

        // Update

        public static bool Update(DbConnection connection, DbTransaction transaction, UserDepartment d)
        {
            return DbFactory.ExecuteNonQuery("spUserDepartment_Update", connection, transaction, new Dictionary<string, object> 
            {
                { "intDepartmentID", d.ID },
                { "intParentDepartmentID", d.ParentDepartmentID },
                { "chrDepartmentName", d.Name},
                { "intClientID", Settings.ClientID },
                { "intEntityID", d.EntityID }
            }) > 0;
        }

        public static bool Update(UserDepartment d)
        {
            return DbFactory.Connect<bool, UserDepartment>(Update, d);
        }


        // Select



        public static List<UserDepartment> GetByEntityID(DbConnection connection, DbTransaction transaction, int entityID)
        {
            List<UserDepartment> items = new List<UserDepartment>();
            DbReader r = null;
            try
            {
                r = new DbReader("spUserDepartmentsByEntityID_Select", connection, transaction, new Dictionary<string, object> 
                {
                    { "intEntityID", entityID == 0 ? (object)DBNull.Value : entityID },
                    { "intClientID", Settings.ClientID }
                });

                while (r.Read())
                {
                    items.Add(ReadDepartment(r));
                }
            }
            finally
            {
                if (r != null) r.Close();
            }
            return items;
        }

        public static List<UserDepartment> GetByEntityID(int entityID)
        {
            return DbFactory.Connect<List<UserDepartment>, int>(GetByEntityID, entityID);
        }

        public static UserDepartment Get(DbConnection connection, DbTransaction transaction, int id)
        {
            UserDepartment item = null;
            DbReader r = null;
            try
            {
                r = new DbReader("spUserDepartment_Select", connection, transaction, new Dictionary<string, object> 
                {
                    { "intDepartmentID", id },
                    { "intClientID", Settings.ClientID }
                });

                if (r.Read())
                {
                    item = ReadDepartment(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }
            return item;
        }

        public static UserDepartment Get(int id)
        {
            return DbFactory.Connect<UserDepartment, int>(Get, id);
        }

        public static List<UserDepartment> Get(DbConnection connection, DbTransaction transaction)
        {
            List<UserDepartment> items = new List<UserDepartment>();
            DbReader r = null;
            try
            {
                r = new DbReader("spUserDepartment_Select", connection, transaction, new Dictionary<string, object> 
                {
                    { "intDepartmentID", 0 },
                    { "intClientID", Settings.ClientID }
                });

                while (r.Read())
                {
                    items.Add(ReadDepartment(r));
                }
            }
            finally
            {
                if (r != null) r.Close();
            }
            return items;
        }

        public static List<UserDepartment> Get()
        {
            return DbFactory.Connect<List<UserDepartment>>(Get);
        }

        // Delete  

        /// <summary>
        /// Returns 0 if users are attached to the department or any of its child departments or if no deletions were made.
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool Delete(DbConnection connection, DbTransaction transaction, int id)
        {
            return DbFactory.ExecuteScalar<int>("spUserDepartment_Delete", connection, transaction, new Dictionary<string, object> 
            {
                { "intDepartmentID", id },
                { "intClientID", Settings.ClientID }
            }) > 0;
        }

        public static bool Delete(int id)
        {
            return DbFactory.Connect<bool, int>(Delete, id);
        }


        // Helpers

        private static UserDepartment ReadDepartment(DbReader r)
        {
            return new UserDepartment
            {
                EntityID = r.Get<int>("intEntityID"),
                ID = r.Get<int>("intDepartmentID"),
                Name = r.Get<string>("chrDepartmentName"),
                ParentDepartmentID = r.Get<int>("intParentDepartmentID"),
                ParentName = r.Get<string>("chrParentName"),
                EntityName = r.Get<string>("chrEntityName"),
                ClientID = r.Get<int>("intClientID")
            };
        }
    }
}
