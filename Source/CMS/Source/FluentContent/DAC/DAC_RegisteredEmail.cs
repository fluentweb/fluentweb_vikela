﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using FluentContent.Entities;
using DataAccess;
using DataAccess.Base;
using FluentContent.Entities.Enums;

namespace FluentContent.DAC
{
    public class DAC_RegisteredEmail
    {
        // INSERT, UPDATE

        public static string InsertUpdate(RegisteredEmail re)
        {
            return DbFactory.Connect<string, RegisteredEmail>(InsertUpdate, re);
        }

        public static string InsertUpdate(DbConnection connection, DbTransaction transaction, RegisteredEmail re)
        {
            return DbFactory.ExecuteScalar<string>("spRegisteredEmails_InsertUpdate", connection, transaction, new Dictionary<string, object> 
            {
                { "chrEmail", re.Email },
                { "chrFirstName", re.FirstName },
                { "chrLastName", re.LastName },
                { "datDateOfBirth", re.DateOfBirth },
                { "chrGender", re.Gender },
                { "chrContactNumber", re.ContactNumber },
                { "chrCompany", re.CompanyName },
                { "bitClientEmailsSubscription", re.ClientEmailSubscribtion },
                { "bitThirdPartyEmailsSubscription", re.ThirdPartyEmailSubscription },
            });
        }

        // DELETE

        public static bool Delete(string email)
        {
            return DbFactory.Connect<bool, string>(Delete, email);
        }

        public static bool Delete(DbConnection connection, DbTransaction transaction, string email)
        {
            return DbFactory.ExecuteNonQuery("spRegisteredEmails_Delete", connection, transaction, new Dictionary<string, object> 
            {
                { "chrEmail", email }               
            }) > 0;
        }


        // SELECT
        public static List<RegisteredEmail> Get(string email)
        {
            return DbFactory.Connect<List<RegisteredEmail>, string>(Get, email);
        }

        public static List<RegisteredEmail> Get(DbConnection connection, DbTransaction transaction, string email)
        {
            List<RegisteredEmail> items = new List<RegisteredEmail>();
            DbReader r = null;
            try
            {
                r = new DbReader("spRegisteredEmails_Select", connection, transaction, new Dictionary<string, object> 
                {
                    { "chrEmail", String.IsNullOrEmpty(email) ? (object)DBNull.Value : email }
                });
                while (r.Read())
                {
                    items.Add(ReadRegisteredEmail(r));
                }
            }
            finally
            {
                if (r != null)
                    r.Close();
            }

            return items;
        }


        // HELPERS
        private static RegisteredEmail ReadRegisteredEmail(DbReader r)
        {
            return new RegisteredEmail
            {
                ClientEmailSubscribtion = r.Get<bool>("bitClientEmailsSubscription"),
                CompanyName = r.Get<string>("chrCompany"),
                ContactNumber = r.Get<string>("chrContactNumber"),
                DateCreated = r.Get<DateTime>("datCreated"),
                DateOfBirth = r.Get<DateTime>("datDateOfBirth"),
                DateUpdated = r.Get<DateTime>("datUpdated"),
                Email = r.Get<string>("chrEmail"),
                FirstName = r.Get<string>("chrFirstName"),
                Gender = r.Get<char>("chrGender"),
                LastName = r.Get<string>("chrLastName"),
                ThirdPartyEmailSubscription = r.Get<bool>("bitThirdPartyEmailsSubscription")
            };
        }
    }
}
