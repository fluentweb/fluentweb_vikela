﻿using DataAccess;
using DataAccess.Base;
using FluentContent.Entities;
using FluentContent.Entities.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.DAC
{
    public class DAC_TMCost
    {
        public static DataTable GetCost()
        {
            return DbFactory.Connect<DataTable>(GetCost);
        }

        public static DataTable GetCost(DbConnection connection, DbTransaction transaction)
        {
            return DbFactory.GetDataTable("spTMtCost_Select", connection, transaction, new Dictionary<string, object> 
            {
                { "CostID", (object)DBNull.Value }
            });
        }

        public static TMCost GetCost(int costId)
        {
            return DbFactory.Connect<TMCost, int>(GetCost, costId);
        }

        public static TMCost GetCost(DbConnection connection, DbTransaction transaction, int costId)
        {
            TMCost item = null;
            DbReader r = null;

            try
            {
                r = new DbReader("spTMtCost_Select", connection, transaction, new Dictionary<string, object>
                {
                    { "CostID", costId }
                });

                if (r.Read())
                {
                    item = new TMCost(r);
                }

                if (r != null) r.Close();
            }
            finally
            {
                if (r != null) r.Close();
            }

            return item;
        }

        public static TMCost Insert(TMCost tmc)
        {
            return DbFactory.Connect<TMCost, TMCost>(Insert, tmc);
        }

        public static TMCost Insert(DbConnection connection, DbTransaction transaction, TMCost tmc)
        {
            DbReader r = null;

            try
            {
                r = new DbReader("spTMtCost_Insert", connection, transaction, new Dictionary<string, object>
                {
                       {"bitBW",tmc.BW},
	                   {"BitColour",tmc.Colour},
	                   {"BitColourSeries",tmc.ColourSeries},
	                   {"CostType",tmc.CostTypeID},
	                   {"CountryID",tmc.CountryID},
	                   {"MaxClasses",tmc.MaxClasses == 0 ? (object)DBNull.Value : tmc.MaxClasses},
	                   {"MaxMarks",tmc.MaxMarks == 0 ? (object)DBNull.Value : tmc.MaxMarks},
	                   {"MaxProducts",tmc.MaxProducts == 0 ? (object)DBNull.Value : tmc.MaxProducts},
	                   {"MaxStates",tmc.MaxStates == 0 ? (object)DBNull.Value : tmc.MaxStates},
	                   {"MinClasses",tmc.MinClasses == 0 ? (object)DBNull.Value : tmc.MinClasses},
	                   {"MinMarks",tmc.MinMarks == 0 ? (object)DBNull.Value : tmc.MinMarks},
	                   {"MinProducts",tmc.MinProducts == 0 ? (object)DBNull.Value : tmc.MinProducts},
	                   {"MinStates",tmc.MinStates == 0 ? (object)DBNull.Value : tmc.MinStates},
	                   {"TMTypeID",tmc.TMTypeID},
	                   {"Price",tmc.Price == 0 ? (object)DBNull.Value : tmc.Price}
                });

                if (r.Read())
                {
                    tmc = new TMCost(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return tmc;
        }

        public static TMCost Update(TMCost tmc)
        {
            return DbFactory.Connect<TMCost, TMCost>(Update, tmc);
        }

        public static TMCost Update(DbConnection connection, DbTransaction transaction, TMCost tmc)
        {
            DbReader r = null;

            try
            {
                r = new DbReader("spTMtCost_Update", connection, transaction, new Dictionary<string, object>
                {
                       {"CostID",tmc.CostID},
                       {"bitBW",tmc.BW},
	                   {"BitColour",tmc.Colour},
	                   {"BitColourSeries",tmc.ColourSeries},
	                   {"CostType",tmc.CostTypeID},
	                   {"CountryID",tmc.CountryID},
	                   {"MaxClasses",tmc.MaxClasses == 0 ? (object)DBNull.Value : tmc.MaxClasses},
	                   {"MaxMarks",tmc.MaxMarks == 0 ? (object)DBNull.Value : tmc.MaxMarks},
	                   {"MaxProducts",tmc.MaxProducts == 0 ? (object)DBNull.Value : tmc.MaxProducts},
	                   {"MaxStates",tmc.MaxStates == 0 ? (object)DBNull.Value : tmc.MaxStates},
	                   {"MinClasses",tmc.MinClasses == 0 ? (object)DBNull.Value : tmc.MinClasses},
	                   {"MinMarks",tmc.MinMarks == 0 ? (object)DBNull.Value : tmc.MinMarks},
	                   {"MinProducts",tmc.MinProducts == 0 ? (object)DBNull.Value : tmc.MinProducts},
	                   {"MinStates",tmc.MinStates == 0 ? (object)DBNull.Value : tmc.MinStates},
	                   {"TMTypeID",tmc.TMTypeID},
	                   {"Price",tmc.Price == 0 ? (object)DBNull.Value : tmc.Price}
                });

                if (r.Read())
                {
                    tmc = new TMCost(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return tmc;
        }

        public static bool Delete(TMCost tmc)
        {
            return DbFactory.Connect<bool, TMCost>(Delete, tmc);
        }

        public static bool Delete(DbConnection connection, DbTransaction transaction, TMCost tmc)
        {
            return DbFactory.ExecuteNonQuery("spTMtCost_Delete", connection, transaction, new Dictionary<string, object> 
            {
                { "CostID", tmc.CostID }
            }) > 0;
        }

        public static TMCostType ReadProperty(DbReader r)
        {
            return new TMCostType(r);
        }

    }
}