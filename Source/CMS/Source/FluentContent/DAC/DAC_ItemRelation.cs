﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using FluentContent.Entities;
using DataAccess;

namespace FluentContent.DAC
{
    public class DAC_ItemRelation
    {
        public static bool UpdateItemRelation(DbConnection connection, DbTransaction transaction, ItemRelation itemRelation)
        {
            return DbFactory.ExecuteNonQuery("spRelatedItemBasic_Update", connection, transaction, new Dictionary<string, object> 
            {
                { "ItemID", itemRelation.ItemID },
                { "Sequence", itemRelation.ItemRelationSequence },
                { "Enabled", itemRelation.ItemEnabled },
                { "Title", itemRelation.ItemTitle },
                { "ItemRelateID", itemRelation.ID },
            }) > 0;
        }

        public static bool UpdateItemRelation(ItemRelation itemRelation)
        {
            return DbFactory.Connect<bool, ItemRelation>(UpdateItemRelation, itemRelation);
        }
    }
}
