﻿using DataAccess;
using DataAccess.Base;
using FluentContent.Entities;
using FluentContent.Entities.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.DAC
{
    public class DAC_TMCountryAdditionalRequirementChoice
    {
        public static DataTable GetCountryAdditionalRequirementChoice(int CountryAdditionalRequirementID)
        {
            return DbFactory.Connect<DataTable, int>(GetCountryAdditionalRequirementChoice, CountryAdditionalRequirementID);
        }

        public static DataTable GetCountryAdditionalRequirementChoice(DbConnection connection, DbTransaction transaction, int CountryAdditionalRequirementID)
        {
            return DbFactory.GetDataTable("spTMtCountryAdditionalRequirementChoice_Select", connection, transaction, new Dictionary<string, object> 
            {
                { "CountryAdditionalRequirementID", CountryAdditionalRequirementID },
                { "CountryAdditionalRequirementChoiceID", (object)DBNull.Value }
            });
        }


        public static TMCountryAdditionalRequirementChoice GetCountryAdditionalRequirementChoice(int countryAdditionalRequirementChoiceID, int CountryAdditionalRequirementID)
        {
            return DbFactory.Connect<TMCountryAdditionalRequirementChoice, int, int>(GetCountryAdditionalRequirementChoice, countryAdditionalRequirementChoiceID, CountryAdditionalRequirementID);
        }

        public static TMCountryAdditionalRequirementChoice GetCountryAdditionalRequirementChoice(DbConnection connection, DbTransaction transaction, int countryAdditionalRequirementChoiceID, int CountryAdditionalRequirementID)
        {
            TMCountryAdditionalRequirementChoice item = null;
            DbReader r = null;

            try
            {
                r = new DbReader("spTMtCountryAdditionalRequirementChoice_Select", connection, transaction, new Dictionary<string, object>
                {
                    { "CountryAdditionalRequirementID", CountryAdditionalRequirementID },
                    { "CountryAdditionalRequirementChoiceID", countryAdditionalRequirementChoiceID }
                });

                if (r.Read())
                {
                    item = new TMCountryAdditionalRequirementChoice(r);
                }

                if (r != null) r.Close();
            }
            finally
            {
                if (r != null) r.Close();
            }

            return item;
        }

        public static TMCountryAdditionalRequirementChoice Insert(TMCountryAdditionalRequirementChoice tmcarc)
        {
            return DbFactory.Connect<TMCountryAdditionalRequirementChoice, TMCountryAdditionalRequirementChoice>(Insert, tmcarc);
        }

        public static TMCountryAdditionalRequirementChoice Insert(DbConnection connection, DbTransaction transaction, TMCountryAdditionalRequirementChoice tmcarc)
        {
            DbReader r = null;

            try
            {
                r = new DbReader("spTMtCountryAdditionalRequirementChoice_Insert", connection, transaction, new Dictionary<string, object>
                {
                    {"CountryAdditionalRequirementID",tmcarc.CountryAdditionalRequirementID},
                    {"CostTypeID",tmcarc.CostTypeID == 0 ? (object)DBNull.Value : tmcarc.CostTypeID},
                    {"ChoiceText",tmcarc.ChoiceText},

                });

                if (r.Read())
                {
                    tmcarc = new TMCountryAdditionalRequirementChoice(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return tmcarc;
        }

        public static TMCountryAdditionalRequirementChoice Update(TMCountryAdditionalRequirementChoice tmcarc)
        {
            return DbFactory.Connect<TMCountryAdditionalRequirementChoice, TMCountryAdditionalRequirementChoice>(Update, tmcarc);
        }

        public static TMCountryAdditionalRequirementChoice Update(DbConnection connection, DbTransaction transaction, TMCountryAdditionalRequirementChoice tmcarc)
        {
            DbReader r = null;

            try
            {
                r = new DbReader("spTMtCountryAdditionalRequirementChoice_Update", connection, transaction, new Dictionary<string, object>
                {
                    {"CountryAdditionalRequirementChoiceID",tmcarc.CountryAdditionalRequirementChoiceID},
                    {"CostTypeID",tmcarc.CostTypeID == 0 ? (object)DBNull.Value : tmcarc.CostTypeID},
                    {"ChoiceText",tmcarc.ChoiceText},
                });

                if (r.Read())
                {
                    tmcarc = new TMCountryAdditionalRequirementChoice(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return tmcarc;
        }
        public static bool Delete(TMCountryAdditionalRequirementChoice tmcarc)
        {
            return DbFactory.Connect<bool, TMCountryAdditionalRequirementChoice>(Delete, tmcarc);
        }

        public static bool Delete(DbConnection connection, DbTransaction transaction, TMCountryAdditionalRequirementChoice tmcarc)
        {
            return DbFactory.ExecuteNonQuery("spTMtCountryAdditionalRequirementChoice_Delete", connection, transaction, new Dictionary<string, object> 
            {
                { "CountryAdditionalRequirementChoiceID", tmcarc.CountryAdditionalRequirementChoiceID }
            }) > 0;
        }

        public static TMCountryAdditionalRequirementChoice ReadProperty(DbReader r)
        {
            return new TMCountryAdditionalRequirementChoice(r);
        }
    }
}
