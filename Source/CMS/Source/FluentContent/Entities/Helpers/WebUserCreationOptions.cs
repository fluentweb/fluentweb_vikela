﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FluentContent.Entities.Helpers
{
    public class WebUserCreationOptions
    {
        public bool RequiresActivation { get; set; }
        public bool GeneratePassword { get; set; }
    }
}
