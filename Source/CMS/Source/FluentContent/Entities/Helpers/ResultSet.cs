﻿using DataAccess.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.Entities.Helpers
{
    public class ResultSet<T> where T : class
    {
        public int PageSize { get; private set; }
        public int TotalRows { get; set; }
        public int TotalPages { get; private set; }
        public int CurrentPage { get; private set; }

        public List<T> Items { get; private set; }

        public ResultSet(int page, int pageSize, DbReader r, Func<DbReader, T> readObjectFunction)
        {
            PageSize = pageSize;
            CurrentPage = page;

            Items = new List<T>();
            bool firstIteration = true;

            while (r.Read())
            {
                if (firstIteration)
                {
                    TotalRows = r.Get<int>("TotalRows");
                    decimal mod = TotalRows % PageSize;
                    TotalPages = mod != 0 ? ((int)(TotalRows / PageSize)) + 1 : TotalPages = (int)(TotalRows / PageSize);
                    firstIteration = false;
                }
                Items.Add(readObjectFunction(r));
            }
        }
    }
}
