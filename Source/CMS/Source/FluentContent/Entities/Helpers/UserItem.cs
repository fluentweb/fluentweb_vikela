﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FluentContent.Entities.Helpers
{
    public class UserItem
    {
        public int ItemID { get; set; }
        public int UserID { get; set; }
        public bool IsAttached { get; set; }
        public string ItemType { get; set; }
        public string ItemName { get; set; }

    }
}
