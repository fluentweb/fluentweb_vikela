﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FluentContent.Entities
{
    public class WebsiteSetting
    {  

        public string Key { get; set; }
        public string Value { get; set; }
    }
}
