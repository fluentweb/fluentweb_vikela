﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.Entities
{
    public class WebsiteOrderItem
    {
        public long ID { get; set; }
        public long WebsiteOrderID { get; set; }
        public int Quantity { get; set; }
        public long ExternalID { get; set; }
        public decimal PriceExcludingVAT { get; set; }
        public string Description { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }

        public WebsiteOrderItem()
        {
            Quantity = 1;
        }
    }
}
