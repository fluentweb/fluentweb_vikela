﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FluentContent.Entities
{
    public class FormType
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int ClientID { get; set; }
    }
}
