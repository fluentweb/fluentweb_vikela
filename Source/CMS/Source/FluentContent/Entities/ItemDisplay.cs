﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FluentContent.Entities
{
    public class DisplaySetting
    {
        public int ID { get; set; }
        public int ItemTypeID { get; set; }
        public int MaxNumberItems { get; set; }
        public bool Randomise { get; set; }
        public string ItemDisplayName { get; set; }     
        public int ItemTextTypeID { get; set; }
        public int ItemImageTypeID { get; set; }
        public char OrderBy { get; set; }
        public int PageSize { get; set; }
        

        public DisplaySetting()
        {
            ID = -1;
        }
    }
}
