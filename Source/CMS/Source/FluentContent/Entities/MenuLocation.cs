﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FluentContent.Entities
{
    public class MenuLocation
    {
        public int ID { get; set; }
        public int ClientID { get; set; }
        public string Location { get; set; }

        public MenuLocation()
        {
            ID = -1;
        }
    }
}
