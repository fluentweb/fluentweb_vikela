﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FluentContent.Entities
{
    public class ClientSetting
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public int ClientID { get; set; }
        public string Label { get; set; }

    }
}
