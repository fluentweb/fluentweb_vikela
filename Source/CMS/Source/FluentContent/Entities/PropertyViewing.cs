﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.Entities
{


    public class PropertyViewing
    {

        #region Declare variables

        private int _intPropertyViewingID;
        private int _intPropertyID;
        private DateTime _datViewing;
        private string _timTimeFrom;
        private string _timTimeTo;


        #endregion

        #region Declare Properties

        public int intPropertyViewingID
        {
            get { return _intPropertyViewingID; }
            set { _intPropertyViewingID = value; }
        }

        public int intPropertyID
        {
            get { return _intPropertyID; }
            set { _intPropertyID = value; }
        }

        public DateTime datViewing
        {
            get { return _datViewing; }
            set { _datViewing = value; }
        }

        public string timTimeFrom
        {
            get { return _timTimeFrom; }
            set { _timTimeFrom = value; }
        }

        public string timTimeTo
        {
            get { return _timTimeTo; }
            set { _timTimeTo = value; }
        }



        #endregion

    }




}
