﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentContent.Entities.Enums;

namespace FluentContent.Entities
{
    public class UserRole
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int ClientID { get; set; }
        public char CMSAccessLevel { get; set; }
        public int RoleHierarchy { get; set; }
        public bool EntityRequired { get; set; }
    }
}
