﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FluentContent.Entities.Base
{
    public abstract class EntityBase
    {
        public long ID { get; set; }

        public EntityBase()
        {
            ID = -1;
        }
    }
}
