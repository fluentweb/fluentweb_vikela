﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FluentContent.Entities.Enums
{
    public class Gender
    {
        public const char Male = 'M';
        public const char Female = 'F';
        public const char Undisclosed = 'U';
    }
}
