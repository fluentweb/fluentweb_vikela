﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.Entities.Enums
{
    public class TMCountryAdditionalRequirementQuestionType
    {
        public const char choice = 'C';
        public const char single_confirmation = 'S';
        public const char mandatory_confirmation = 'M';
        public static Dictionary<string, char> questionType = new Dictionary<string, char>()
        {
         {"choice", choice},
	    {"single confirmation", single_confirmation},
	    {"mandatory confirmation", mandatory_confirmation}
        };

    }
}
