﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.Entities.Enums
{
    public enum WebsiteOrderStatus
    {
        FAILED = -1,
        PENDING = 1, // unpaid and waiting to be accepted (default status of newly created order)      
        PAID = 2,
        VOID = 3,
        REFUNDED = 4,
        RESERVED = 5 // Authenticated user for later payment but not authorised
    }
}
