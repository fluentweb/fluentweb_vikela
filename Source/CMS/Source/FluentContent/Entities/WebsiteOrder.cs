﻿using Common.Payment.SagePay.Interfaces;
using FluentContent.Entities.Enums;
using FluentContent.Entities.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.Entities
{
    public class WebsiteOrder : EntityBase, ISagePayRequestObject
    {
        /// <summary>
        /// Maps to VendorTxCode (Session ID)
        /// </summary>
        public string TransactionGUID { get; set; }

        /// <summary>
        /// Reference returned from SagePay
        /// </summary>
        public string PaymentReference { get; set; }

        /// <summary>
        /// The SecurityKey of the authenticated transaction sent back by the Sage Pay  System when the transaction was registered.
        /// </summary>
        public string PaymentSecurityKey { get; set; }

        /// <summary>
        /// This field details the results of the 3D-Secure checks; Required to store by Sagepay
        /// </summary>
        public string Payment3DSecureStatus { get; set; }

        /// <summary>
        /// The encoded result code from the 3D-Secure checks; Required to store by SagePay only present if Payment3DSecureStatus is 'OK'
        /// </summary>
        public string PaymentCAVV { get; set; }

        ///// <summary>
        ///// The SecurityKey of the authenticate transaction sent back by the Sage Pay  System when the transaction was registered.
        ///// </summary>
        //public string SecurityKey { get; set; }

        public WebUser WebUser { get; set; }
        public int WebUserAddressID { get; set; }        
        public int ClientID { get; set; }

        public WebsiteOrderStatus Status { get; set; }

        public decimal VATRate { get; set; }
        public decimal TotalExcludingVAT { get; set; }
        public decimal TotalIncludingVAT { get { return TotalExcludingVAT + ((TotalExcludingVAT * VATRate) / 100); } }
        public decimal RefundAmount { get; set; }

        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
        public DateTime RefundedOn { get; set; }

        public string OrderTypeKey { get; set; }

        public List<WebsiteOrderItem> OrderItems { get; set; }

        #region ISagePayRequestObject Members

        public string VendorTxCode { get { return TransactionGUID; } }

        public string Amount { get { return TotalIncludingVAT.ToString("F2"); } }

        public string Surname { get { return WebUser.LastName; } }

        public string FirstName { get { return WebUser.FirstName; } }

        public string AddressLine1 { get { return WebUser.Addresses.First().AddressLine1; } }

        public string AddressLine2 { get { return WebUser.Addresses.First().AddressLine2; } }

        public string City { get { return WebUser.Addresses.First().TownCity; } }

        public string PostalCode { get { return WebUser.Addresses.First().PostCode; } }

        public string Country { get { return WebUser.Addresses.First().Country.ShortCode; } }

        public string State { get { return WebUser.Addresses.First().County; } }

        public string Phone { get { return WebUser.ContactNo; } }

        #endregion

        public WebsiteOrder()
        {
            OrderItems = new List<WebsiteOrderItem>();
        }
    }
}
