﻿using Common.Payment.SagePay.Enums;
using FluentContent.Entities.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.Entities
{
    public class WebsiteOrderTransaction : EntityBase
    {
        public SagePayTxType TransactionType { get; set; }
        public string VendorTxCode { get; set; }
        public string VPSTxId { get; set; }
        public string TxAuthNo { get; set; }
        public string SecurityKey { get; set; }
        public long WebsiteOrderID { get; set; }
        public long ParentTransactionID { get; set; }
        public DateTime CreatedOn { get; set; }

        public WebsiteOrderTransaction()
        {
        }
    }
}
