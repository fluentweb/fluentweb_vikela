﻿using DataAccess.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.Entities
{
    public class TMCountry
    {
        public int CountryID { get; set; }
        public bool Available { get; set; }
        public bool PowerOfAttorney { get; set; }
        public bool ConventionPriority { get; set; }
        public bool ColourRestrictionBW { get; set; }
        public bool ColourRestrictionCC { get; set; }
        public bool ColourRestrictionCSM { get; set; }
        public bool ColourRestrictionCL { get; set; }
        public int TMClassificationID { get; set; }
        public int TranslationLanguageID { get; set; }

        public TMCountry()
        {
        }

        public TMCountry(DbReader r)
        {
            this.CountryID = r.Get<int>("intCountryID");
            this.Available = r.Get<bool>("bitAvailable");
            this.PowerOfAttorney = r.Get<bool>("bitPowerOfAttorney");
            this.ConventionPriority = r.Get<bool>("bitConventionPriority");
            this.ColourRestrictionBW = r.Get<bool>("bitColourRestrictionBW");
            this.ColourRestrictionCC = r.Get<bool>("bitColourRestrictionCC");
            this.ColourRestrictionCSM = r.Get<bool>("bitColourRestrictionCSM");
            this.ColourRestrictionCL = r.Get<bool>("bitColourRestrictionCL");
            this.TMClassificationID = r.Get<int>("intTMClassificationID");
            this.TranslationLanguageID = r.Get<int>("intTranslationLanguageID");
        }
    }
}
