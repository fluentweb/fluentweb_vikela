﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.Entities
{
    public class PropertyValuation
    {
        public int PropertyValuationID { get; set; }

        public Title Title { get; set; }

        public string FirstName { get; set; }

        public string Surname { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Address3 { get; set; }

        public string AddressPostcode { get; set; }

        public Country Country { get; set; }

        public County County { get; set; }

        public string PhoneHome { get; set; }

        public string PhoneMobile { get; set; }

        public string PhoneWork { get; set; }

        public string EmailAddress { get; set; }

        public bool WantsToSell { get; set; }

        public bool WantsToRent { get; set; }

        public int PropertyTypeItemID { get; set; }

        public string PropertyType { get; set; }

        public int Bedrooms { get; set; }

        public int Bathrooms { get; set; }

        public string AdditionalInformation { get; set; }

        public decimal PropertyValue { get; set; }

        public string Notes { get; set; }

        public DateTime Created { get; set; }

        public long PropertySubscribedUserID { get; set; }

        public int ClientID { get; set; }

    }

}
