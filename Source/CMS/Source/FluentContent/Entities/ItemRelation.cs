﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FluentContent.Entities
{
    public class ItemRelation
    {
        public int ID { get; set; }
        public int ItemID { get; set; }
        public int ItemRelationSequence { get; set; }
        public bool ItemEnabled { get; set; }
        public string ItemTitle { get; set; }

        public ItemRelation()
        {
            ID = -1;
        }
    }
}
