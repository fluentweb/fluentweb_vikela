﻿using DataAccess.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.Entities
{
    public class TMClassificationClass
    {
        public int TMClassificationClassID { get; set; }

        public int TMClassificationID { get; set; }
        public string ClassificationClassCode { get; set; }
        public int ClassificationClassNo { get; set; }
        public string ClassificationClass { get; set; }

        public TMClassificationClass()
        {
        }

        public TMClassificationClass(DbReader r)
        {
            this.TMClassificationClassID = r.Get<int>("intTMClassificationClassID");
            this.TMClassificationID = r.Get<int>("intTMClassificationID");
            this.ClassificationClassCode = r.Get<string>("chrClassificationClassCode");
            this.ClassificationClassNo = r.Get<int>("intClassificationClassNo");
            this.ClassificationClass = r.Get<string>("chrClassificationClass");
        }
    }
}
