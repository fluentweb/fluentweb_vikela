﻿using DataAccess.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.Entities
{
    public class TMCountryAdditionalRequirementChoice
    {
        public int CountryAdditionalRequirementChoiceID { get; set; }
        public int CountryAdditionalRequirementID { get; set; }
        public string ChoiceText { get; set; }
        public int CostTypeID { get; set; }

        public TMCountryAdditionalRequirementChoice()
        {
        }
        public TMCountryAdditionalRequirementChoice(DbReader r)
        {
            this.CountryAdditionalRequirementChoiceID = r.Get<int>("intCountryAdditionalRequirementChoiceID");
            this.CountryAdditionalRequirementID = r.Get<int>("intCountryAdditionalRequirementID");
            this.ChoiceText = r.Get<string>("chrChoiceText");
            this.CostTypeID = r.Get<int>("intCostTypeID");
        }

    }
}
