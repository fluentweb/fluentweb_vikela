﻿using DataAccess.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.Entities
{
    public class TMClassification
    {
        public int TMClassificationID { get; set; }
        public string ClassificationTitle { get; set; }
        public string Classification { get; set; }
        public bool OldBritish { get; set; }

        public TMClassification()
        {
        }

         public TMClassification(DbReader r)
         {
            this.TMClassificationID = r.Get<int>("intTMClassificationID");
            this.ClassificationTitle = r.Get<string>("chrTMClassificationTitle");
            this.Classification = r.Get<string>("chrTMClassification");
            this.OldBritish = r.Get<bool>("bitOldBritish");
        }
    }
}
