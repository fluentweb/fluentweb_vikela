﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.Entities
{
    public class PropertySubscribedUser
    {
        public long ID { get; set; }

        public Title Title { get; set; }

        public string FirstName { get; set; }

        public string Surname { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Address3 { get; set; }

        public string AddressPostcode { get; set; }

        public Country Country { get; set; }

        public County County { get; set; }

        public string PhoneHome { get; set; }

        public string PhoneMobile { get; set; }

        public string PhoneWork { get; set; }

        public string EmailAddress { get; set; }

        public string SpecificRequirements { get; set; }

        public bool InterestedInValuation { get; set; }

        public bool WantsToBuy { get; set; }

        public bool WantsToRent { get; set; }

        public int PropertyTypeItemID { get; set; }

        public string PropertyType { get; set; }

        public int MinBedrooms { get; set; }

        public int MinBathrooms { get; set; }

        public int QualificationID { get; set; }

        public string Qualification { get; set; }

        public decimal MinPrice { get; set; }

        public decimal MaxPrice { get; set; }

        public bool Subscribed { get; set; }

        public bool EmailAlerts { get; set; }

        public DateTime DateSubscribed { get; set; }

        public int ClientID { get; set; }

        public List<int> InterestCountyIDs { get; set; }

        public bool OutsideArea { get; set; }

        public bool Parking { get; set; }

        public PropertySubscribedUser()
        {
            InterestCountyIDs = new List<int>();
        }
    }

}
