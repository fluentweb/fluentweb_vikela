﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentContent.Entities.Base;

namespace FluentContent.Entities
{
    public class Country: EntityBase
    {
        public string Name { get; set; }
        public string ShortCode { get; set; }
        public string Code { get; set; }
        public int ISO { get; set; }
    }
}
