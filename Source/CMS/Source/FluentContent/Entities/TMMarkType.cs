﻿using DataAccess.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.Entities
{
    public class TMMarkType
    {
        public int TMTypeID { get; set; }
        public string TMTypeCode { get; set; }
        public string TMType { get; set; }

        public TMMarkType()
        {
        }

        public TMMarkType(DbReader r)
        {
            this.TMTypeID = r.Get<int>("intTMTypeID");
            this.TMTypeCode = r.Get<string>("chrTMTypeCode");
            this.TMType = r.Get<string>("chrTMType");
        }

    }
}
