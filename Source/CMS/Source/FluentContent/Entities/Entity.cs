﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FluentContent.Entities
{
    public class Entity
    {
        public int ID { get; set; }
        public int TypeID { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public bool IsAttachedToCurrentItem { get; set; }
        public DateTime DateAttachedToCurrentItem { get; set; }
        public DateTime DateExpires { get; set; }
        public bool Enabled { get; set; }
        public string LogoFilename { get; set; }
    }
}
