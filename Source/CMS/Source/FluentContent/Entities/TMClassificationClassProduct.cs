﻿using DataAccess.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.Entities
{
    public class TMClassificationClassProduct
    {
        public int TMClassificationClassProductID { get; set; }

        public int TMClassificationClassID { get; set; }
        public string ClassificationClassProductCode { get; set; }
        public int ClassificationClassProductNo { get; set; }
        public string ClassificationClassProduct { get; set; }

        public TMClassificationClassProduct()
        {
        }

        public TMClassificationClassProduct(DbReader r)
        {
            this.TMClassificationClassProductID = r.Get<int>("intTMClassificationClassProductID");
            this.TMClassificationClassID = r.Get<int>("intTMClassificationClassID");
            this.ClassificationClassProductCode = r.Get<string>("chrTMClassificationClassProductCode");
            this.ClassificationClassProductNo = r.Get<int>("intTMClassificationClassProductNo");
            this.ClassificationClassProduct = r.Get<string>("chrTMClassificationClassProduct");
        }
    }
}
