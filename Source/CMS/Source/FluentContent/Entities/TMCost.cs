﻿using DataAccess.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.Entities
{
    public class TMCost
    {
        public int CostID { get; set; }
        public int CountryID { get; set; }
        public int TMTypeID { get; set; }
        public decimal Price { get; set; }
        public int CostTypeID { get; set; }
        public int MinMarks { get; set; }
        public int MaxMarks { get; set; }
        public int MinClasses { get; set; }
        public int MaxClasses { get; set; }
        public int MinStates { get; set; }
        public int MaxStates { get; set; }
        public int MinProducts { get; set; }
        public int MaxProducts { get; set; }
        public bool BW { get; set; }
        public bool Colour { get; set; }
        public bool ColourSeries { get; set; }

        public TMCost()
        {
        }

        public TMCost(DbReader r)
        {
            this.CostID = r.Get<int>("intCostID");
            this.CountryID = r.Get<int>("intCountryID");
            this.TMTypeID = r.Get<int>("intTMTypeID");
            this.Price = r.Get<decimal>("numPrice");
            this.CostTypeID = r.Get<int>("intCostTypeID");
            this.MinMarks = r.Get<int>("intMinMarks");
            this.MaxMarks = r.Get<int>("intMaxMarks");
            this.MinClasses = r.Get<int>("intMinClasses");
            this.MaxMarks = r.Get<int>("intMaxClasses");
            this.MinStates = r.Get<int>("intMinStates");
            this.MaxStates = r.Get<int>("intMaxStates");
            this.MinProducts = r.Get<int>("intMinProducts");
            this.MaxProducts = r.Get<int>("intMaxProducts");
            this.BW = r.Get<bool>("bitBW");
            this.Colour = r.Get<bool>("bitColour");
            this.ColourSeries = r.Get<bool>("bitColourSeries");
        }

    }
}
