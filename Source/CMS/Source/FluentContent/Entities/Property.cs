﻿using DataAccess.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentContent.Entities
{
    public class Property
    {
        public int ID { get; set; }
        public int ClientID { get; private set; }
        public string PropertyCode { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string AddressPostcode { get; set; }

        public County County { get; set; }
        public Country Country { get; set; }

        public int StatusID { get; set; }
        public string StatusName { get; set; }

        public string GoogleCoordinates { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string AdditionalDescription { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public bool Deleted { get; set; }
        public bool ForSale { get; set; }
        public decimal Price { get; set; }
        public decimal PriceOld { get; set; }
        public bool ForRent { get; set; }
        public decimal Rental { get; set; }
        public decimal RentalOld { get; set; }

        public bool Promotion1 { get; set; }
        public bool Promotion2 { get; set; }

        public string WebsitePropertyURL { get; set; }
        public string AdditionalInformation { get; set; }

        public int PropertyTypeItemID { get; set; }
        public string PropertyType { get; set; }

        public int BedroomCount { get; set; }
        public int BathroomCount { get; set; }

        public int AgencyTypeItemID { get; set; }
        public string AgencyType { get; set; }

        public int TenureItemID { get; set; }
        public string Tenure { get; set; }

        public int CategoryItemID { get; set; }
        public string Category { get; set; }

        public int ForSaleStatusItemID { get; set; }
        public string ForSaleStatus { get; set; }

        public int ForRentStatusItemID { get; set; }
        public string ForRentStatus { get; set; }

        public string MapReference { get; set; }
        public string Directions { get; set; }
        public string Commission { get; set; }

        public bool ForSaleRentBoard { get; set; }
        public bool AdvertisementAllowed { get; set; }

        public string VendorsName { get; set; }
        public string VendorAddressLine1 { get; set; }
        public string VendorAddressLine2 { get; set; }
        public string VendorAddressLine3 { get; set; }

        public Country VendorCountry { get; set; }
        public County VendorCounty { get; set; }

        public string VendorPostcode { get; set; }
        public string VendorPhoneHome { get; set; }
        public string VendorPhoneWork { get; set; }
        public string VendorPhoneMobile { get; set; }
        public string VendorEmail { get; set; }
        public string VendorKeysHeldViewingInfo { get; set; }
        public string VendorAdditionalInfo { get; set; }
        public string VendorReasonForSellingRent { get; set; }
        public string VendorReasonForRemoving { get; set; }
        public string VendorNextPurchase { get; set; }
        public string VendorHowTheyHeard { get; set; }
        public string VendorRestrictions { get; set; }

        public string AttachmentFileName { get; set; }
        public int ForRentCurrencyItemID { get; set; }
        public string ForRentCurrencySymbol { get; set; }
        public int ForSaleCurrencyItemID { get; set; }
        public string ForSaleCurrencySymbol { get; set; }
        public int RentalTermsItemID { get; set; }
        public string RentalTerms { get; set; }

        public string DisplayPrice
        {
            get
            {
                if (ForSale)
                {
                    if (Price > 0)
                        return String.Format("{0}{1}", ForSaleCurrencySymbol, Price.ToString("0,00"));
                    return "N/A";
                }
                if (ForRent)
                {
                    if (Rental > 0)
                    {
                        if (RentalTermsItemID > 0)
                            return String.Format("{0}{1} ({2})", ForRentCurrencySymbol, Rental.ToString("0,00"), RentalTerms);
                        return String.Format("{0}{1}", ForRentCurrencySymbol, Rental.ToString("0,00"));
                    }
                    return "N/A";
                }
                return "N/A";
            }
        }
        // helpers

        public PropertyImage LeadImage
        {
            get
            {
                if (Images == null || Images.Count == 0) return null;
                return Images.Find(i => i.IsLeadImage);
            }
        }

        // lists
        public List<PropertyImage> Images { get; set; }


        public List<PropertyAsset> Assets { get; set; }


        public Property()
        {
            Images = new List<PropertyImage>();
            ClientID = Settings.ClientID;
        }

        public Property(DbReader r)
        {
            this.Address1 = r.Get<string>("chrAddress1");
            this.Address2 = r.Get<string>("chrAddress2");
            this.Address3 = r.Get<string>("chrAddress3");
            this.County = new County { ID = r.Get<int>("intCountyID"), CountyName = r.Get<string>("chrCountyName") };
            this.AddressPostcode = r.Get<string>("chrAddressPostcode");
            this.ClientID = r.Get<int>("intClientID");
            this.Country = new Country { ID = r.Get<int>("intCountryID"), Name = r.Get<string>("chrCountry") };
            this.Created = r.Get<DateTime>("datCreated");
            this.Deleted = r.Get<bool>("bitDeleted");
            this.Description = r.Get<string>("chrDescription");
            this.ForRent = r.Get<bool>("bitForRent");
            this.ForSale = r.Get<bool>("bitForSale");
            this.GoogleCoordinates = r.Get<string>("chrGoogleCoordinates");
            this.PriceOld = r.Get<decimal>("numPriceOld");
            this.Price = r.Get<decimal>("numPrice");
            this.Rental = r.Get<decimal>("numRental");

            this.ID = r.Get<int>("intPropertyID");
            this.Promotion1 = r.Get<bool>("bitPromotion1");
            this.Promotion2 = r.Get<bool>("bitPromotion2");
            this.PropertyCode = r.Get<string>("chrPropertyCode");
            this.RentalOld = r.Get<decimal>("numRentalOld");
            this.StatusID = r.Get<int>("intStatusID");
            this.StatusName = r.Get<string>("chrStatus");
            this.Title = r.Get<string>("chrTitle");
            this.Updated = r.Get<DateTime>("datUpdated");
            this.WebsitePropertyURL = r.Get<string>("chrWebsiteURL");

            this.AdditionalInformation = r.Get<string>("chrAdditionalInformation");
            this.PropertyTypeItemID = r.Get<int>("intPropertyTypeItemID");
            this.PropertyType = r.Get<string>("chrPropertyType");
            this.BedroomCount = r.Get<int>("intBedroomCount");
            this.BathroomCount = r.Get<int>("intBathroomCount");
            this.AgencyTypeItemID = r.Get<int>("intAgencyTypeItemID");
            this.AgencyType = r.Get<string>("chrAgencyType");
            this.TenureItemID = r.Get<int>("intTenureItemID");
            this.Tenure = r.Get<string>("chrTenure");
            this.CategoryItemID = r.Get<int>("intCategoryItemID");
            this.Category = r.Get<string>("chrCategory");
            this.ForSaleStatusItemID = r.Get<int>("intForSaleStatusItemID");
            this.ForSaleStatus = r.Get<string>("chrForSaleStatus");
            this.ForRentStatusItemID = r.Get<int>("intForRentStatusItemID");
            this.ForRentStatus = r.Get<string>("chrForRentStatus");
            this.MapReference = r.Get<string>("chrMapReference");
            this.Directions = r.Get<string>("chrDirections");
            this.Commission = r.Get<string>("chrCommission");
            this.ForSaleRentBoard = r.Get<bool>("bitForSaleRentBoard");
            this.AdvertisementAllowed = r.Get<bool>("bitAdvertisementAllowed");
            this.VendorsName = r.Get<string>("chrVendorsName");
            this.VendorAddressLine1 = r.Get<string>("chrVendorAddressLine1");
            this.VendorAddressLine2 = r.Get<string>("chrVendorAddressLine2");
            this.VendorAddressLine3 = r.Get<string>("chrVendorAddressLine3");
            this.VendorCountry = new Country { ID = r.Get<int>("intVendorCountryID"), Name = r.Get<string>("chrCountry") };
            this.VendorCounty = new County { ID = r.Get<int>("intVendorCountyID"), CountyName = r.Get<string>("chrVendorCountyName") };
            this.VendorPostcode = r.Get<string>("chrVendorPostcode");
            this.VendorPhoneHome = r.Get<string>("chrVendorPhoneHome");
            this.VendorPhoneWork = r.Get<string>("chrVendorPhoneWork");
            this.VendorPhoneMobile = r.Get<string>("chrVendorPhoneMobile");
            this.VendorEmail = r.Get<string>("chrVendorEmail");
            this.VendorKeysHeldViewingInfo = r.Get<string>("chrVendorKeysHeldViewingInfo");
            this.VendorAdditionalInfo = r.Get<string>("chrVendorAdditionalInfo");
            this.VendorReasonForSellingRent = r.Get<string>("chrVendorReasonForSellingRent");
            this.VendorReasonForRemoving = r.Get<string>("chrVendorReasonForRemoving");
            this.VendorNextPurchase = r.Get<string>("chrVendorNextPurchase");
            this.VendorHowTheyHeard = r.Get<string>("chrVendorHowTheyHeard");
            this.VendorRestrictions = r.Get<string>("chrVendorRestrictions");
            this.AttachmentFileName = r.Get<string>("chrAttachmentFilename");
            this.AdditionalDescription = r.Get<string>("chrAdditionalDescription");
            this.ForRentCurrencyItemID = r.Get<int>("intForRentCurrencyItemID");
            this.ForRentCurrencySymbol = r.Get<string>("chrForRentCurrencySymbol");
            this.ForSaleCurrencyItemID = r.Get<int>("intForSaleCurrencyItemID");
            this.ForSaleCurrencySymbol = r.Get<string>("chrForSaleCurrencySymbol");
            this.RentalTermsItemID = r.Get<int>("intRentalTermsItemID");
            this.RentalTerms = r.Get<string>("chrRentalTerms");
        }

    }

}
