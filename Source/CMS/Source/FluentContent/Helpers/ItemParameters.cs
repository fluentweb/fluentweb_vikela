﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FluentContent.Helpers
{
    public class ItemParameters
    {
        public int ItemID { get; set; }
        public int ItemTypeID { get; set; }        
    }
}
