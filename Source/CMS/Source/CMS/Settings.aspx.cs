﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Extensions;
using CMS.Helpers.Bases;
using CMS.Helpers.Entities;
using CMS.Helpers.Interfaces;
using FluentContent.Entities;
using FluentContent.DAC;
using FluentContent.Entities.Enums;

namespace CMS
{
    public partial class Settings : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // check user rights
            if (UserSession.Role.CMSAccessLevel != CMSAccessLevel.Full) Response.Redirect("~/Home.aspx");

            if (!IsPostBack)
            {
                LoadSettings();
                LoadSubMenu();
            }
        }

        private void LoadSettings()
        {
            //txtSettingsBookingFee.Text = DAC_WebsiteSetting.Get(WebsiteSetting.BOOKING_FEE_PERCENT);
            List<ClientSetting> settings = DAC_ClientSettings.GetAll();
            lvItems.DataSource = settings;
            lvItems.DataBind();
        }

        protected void btnSaveSettings_Click(object sender, EventArgs e)
        {
            List<ClientSetting> settingsToUpdate = new List<ClientSetting>();
            foreach (var item in lvItems.Items)
            {
                if (item is ListViewDataItem)
                {
                    TextBox txtSettingValue = item.FindControl("txtSettingValue") as TextBox;
                    string value = txtSettingValue.Text.Trim();
                    string key = lvItems.DataKeys[item.DisplayIndex]["Key"].ToString();
                    settingsToUpdate.Add(new ClientSetting
                    {
                        Key = key,
                        Value = value,
                        ClientID = FluentContent.Settings.ClientID
                    });
                }
            }

            if (settingsToUpdate.Count > 0)
            {
                DAC_ClientSettings.UpdateAllSettings(settingsToUpdate);
            }

            Response.Redirect("~/Settings.aspx?info=saveok");

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            if (!Page.IsValid) return;

            //string bookingFee = txtSettingsBookingFee.Text.Trim().Length == 0 ? "0" : txtSettingsBookingFee.Text.Trim();


            //if (DAC_WebsiteSetting.InsertUpdate(WebsiteSetting.BOOKING_FEE_PERCENT, bookingFee))
            //{
            //    Response.Redirect("~/Settings.aspx?info=saveok");
            //}
            //else
            //{
            //    this.InsertError("Failed to save settings", "SettingsServer");
            //}
        }

        private void LoadSubMenu()
        {
            ICMSMaster master = this.Master as ICMSMaster;
            if (master == null) return;

            List<SubMenuItem> subMenu = new List<SubMenuItem>();

            subMenu.Add(new SubMenuItem(ResolveUrl("~/Settings.aspx"), "Setting Values"));

            master.SetSubMenuDataSource(subMenu, "Settings");
        }
    }
}
