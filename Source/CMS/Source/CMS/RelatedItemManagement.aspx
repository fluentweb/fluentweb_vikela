﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CmsMaster.Master" AutoEventWireup="true"
    CodeBehind="RelatedItemManagement.aspx.cs" Inherits="CMS.RelatedItemManagement" %>

<asp:Content ID="Content4" ContentPlaceHolderID="placeholderHeadCSS" runat="server">
    <link href="FluentControls/css/flcontent.css" runat="server" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="placeholderHeadJS" runat="server">
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="placeholderHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="placeholderBody" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Related Item Management</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="~/Home.aspx" runat="server">Home</a>
                </li>
                <!-- breadcrumbs -->
                <asp:ListView ID="lvBreadcrumbs" runat="server">
                    <LayoutTemplate>
                        <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <li>
                            <%#Eval("Value") == "#" ? String.Format("<span class='inactive'>{0}</span>",Eval("key")): String.Format("<a href='{0}'>{1}</a>",Eval("Value"), Eval("Key"))%>
                        </li>
                    </ItemTemplate>
                </asp:ListView>
                <!-- /breadcrumbs -->
            </ol>
        </div>
        <div class="col-lg-2">
        </div>
    </div>
    <asp:MultiView ID="mvRelatedItems" runat="server">
        <asp:View ID="viewRelate" runat="server">
            <div class="wrapper wrapper-content animated fadeInRight">
                <FC:RelatedItemRelate ID="UCRelatedItemRelate" runat="server" />
            </div>
        </asp:View>
        <asp:View ID="viewCreate" runat="server">
            <div class="wrapper wrapper-content animated fadeInRight">
                <FC:RelatedItemList ID="UCRelatedItemList" runat="server" />
            </div>
        </asp:View>
    </asp:MultiView>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="placeholderFooter" runat="server">
</asp:Content>
