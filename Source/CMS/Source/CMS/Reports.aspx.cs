﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FluentContent.DAC;
using Common.Extensions;
using FluentContent.Entities;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;

using Common.Session;
using CMS.Helpers;

namespace CMS
{
    public partial class Reports : System.Web.UI.Page
    {

        public int ReportID { get { return Request.GetQueryString<int>("id", -1); } }
        public int EntityID { get { return ((CmsMaster)Master).UserSession.EntityID; } }
        public DataTable Report { get; set; }
        private ClientReport _clientReport;

        public CMSUser UserSession { get { return SessionObjects.GetSession<CMSUser>(SessionKeys.CMS_USER_SESSION); } }

        public object[] Parameters
        {
            get
            {
                List<object> p = new List<object>();

                //for (int i = 0; i < 6; i++)
                //{
                //    string val = Request.GetQueryString<string>(String.Format("Param{0}", i + 1), null);

                //    if (val != null)
                //    {
                //        p.Add(val);
                //    }
                //}
                string[] keys = Request.QueryString.AllKeys;
                foreach (string key in keys)
                {
                    if (key == "id") continue;
                 
                    string val = Request.GetQueryString<string>(key, null);

                    // skip entity if null
                    if (key == "entity" && (val == null || val == "null"))
                        continue;

                    if (val != null)
                    {
                        p.Add(val);
                    }
                }
                return p.ToArray();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (ReportID == -1)
                    LoadReportList();
                else
                    LoadReportDetail();
            }
        }

        private void LoadReportDetail()
        {
            mvReports.SetActiveView(viewReportDetail);
            ClientReport r = DAC_ClientReport.GetByID(EntityID, ReportID, UserSession.Role.CMSAccessLevel);
            if (r == null) return;

            _clientReport = r;

            litReportName.Text = r.Name;
            mvReports.SetActiveView(viewReportDetail);

            pnlFilters.Visible = r.RequiresDateFields || r.RequiresUserFilter;
            pnlDateFilter.Visible = r.RequiresDateFields;
            pnlUserFilter.Visible = r.RequiresUserFilter;

            PrepareReport(r);
        }

        private void LoadReportList()
        {
            mvReports.SetActiveView(viewReportList);
            var result = DAC_ClientReport.Get(EntityID, UserSession.Role.CMSAccessLevel);
            if (result.Any())
            {
                result = result.FindAll(i => !i.IsSubReport);
            }
            lvReports.DataSource = result;
            lvReports.DataBind();
        }

        private void PrepareReport(ClientReport r)
        {
            Report = DAC_ClientReport.ExecuteReport(r, Parameters);

            // get breadcrumb
            if (Report != null)
            {
                if (Report.Columns.Contains("Breadcrumb"))
                {
                    if (Report.Rows.Count > 0)
                        litReportBreadcrumb.Text = Report.Rows[0]["Breadcrumb"].ToString();
                }
            }

        }

        public string GetDataHeaders()
        {
            if (Report == null) return String.Empty;
            string template = "<th>{0}</th>";
            StringBuilder sbStringToReturn = new StringBuilder();
            foreach (DataColumn c in Report.Columns)
            {
                if (c.ToString().Contains("Link") || c.ToString().ToLower() == "breadcrumb")
                    continue;
                else
                    sbStringToReturn.AppendFormat(template, c.ToString());

            }
            return sbStringToReturn.ToString();
        }

        public string GenerateRows()
        {
            if (Report == null) return String.Empty;

            string rowTemplate = "<tr>{0}</tr>";
            string cellTemplate = "<td>{0}</td>";


            string linkRegex = "^Link\\d*$";
            string paramRegex = "^Link\\d*Param\\d*$";

            string prevColumnName = String.Empty;
            string currColumnName = String.Empty;

            StringBuilder sbReturnString = new StringBuilder(String.Empty);

            int paramIndex = 0;

            foreach (DataRow r in Report.Rows)
            {
                // reset parameters for each new row
                paramIndex = 0;
                string cells = String.Empty;

                string url = String.Empty;
                foreach (DataColumn c in Report.Columns)
                {
                    string cellValue = String.Empty;
                    currColumnName = c.ToString();

                    // ignore breadcrumb
                    if (currColumnName.ToLower() == "breadcrumb")
                        continue;

                    bool linkComplete = false;

                    // if report link field generate link
                    if (Regex.IsMatch(currColumnName, linkRegex))
                    {
                        url = String.Format("{0}?id={1}", ResolveUrl("~/Reports.aspx"), r[c].ToString());
                    }
                    // if param field after report link or after another parameter add parameter to report link
                    else if ((Regex.IsMatch(prevColumnName, linkRegex) && Regex.IsMatch(currColumnName, paramRegex)) ||
                        (Regex.IsMatch(prevColumnName, paramRegex) && Regex.IsMatch(currColumnName, paramRegex)))
                    {
                        paramIndex++;
                        url += String.Format("&Param{0}={1}", paramIndex, r[c].ToString());
                    }
                    // if previous coloumn is parameter or link and current column is not param or link use as link title
                    else if (((Regex.IsMatch(prevColumnName, linkRegex) || Regex.IsMatch(prevColumnName, paramRegex))
                        && (!Regex.IsMatch(currColumnName, linkRegex) || !Regex.IsMatch(currColumnName, paramRegex))))
                    {
                        linkComplete = true;
                        url = String.Format("<a href='{0}' filter='{2}'>{1}</a>", url, r[c].ToString(), _clientReport.RequiresDateFields ? 1 : 0);
                    }
                    else
                    {
                        cellValue = r[c].ToString();
                        cellValue = String.IsNullOrEmpty(cellValue) ? "n/a" : cellValue;
                        if (r[c] is DateTime)
                        {
                            cellValue = Convert.ToDateTime(cellValue).ToString("dd, MMM yyyy HH:mm");
                        }
                    }

                    if (url.Length == 0)
                        cells += String.Format(cellTemplate, cellValue);
                    if (linkComplete && url.Length > 0)
                    {
                        cells += String.Format(cellTemplate, url);
                        url = String.Empty;
                    }
                    // curent column becomes previous column
                    prevColumnName = currColumnName;
                }

                // add cells
                string rowValue = String.Format(rowTemplate, cells);

                // append to complete string
                sbReturnString.Append(rowValue);


            }
            // return string
            return sbReturnString.ToString() == String.Empty ? "<p>No data to report</p>" : sbReturnString.ToString();
        }

        public string GetViewLink(object reportID)
        {
            if (EntityID > 0)
                return ResolveUrl(String.Format("~/Reports.aspx?id={0}&entity={1}", reportID, EntityID));
            return ResolveUrl(String.Format("~/Reports.aspx?id={0}&entity=null", reportID));
        }
    }
}
