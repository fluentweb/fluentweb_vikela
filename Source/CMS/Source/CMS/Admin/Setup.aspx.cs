﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using Common.Security;
using Common.Extensions;
using System.Data.Common;
using DataAccess;
using FluentContent.Entities;
using FluentContent.DAC;

namespace CMS.Admin
{
    public partial class Setup : System.Web.UI.Page
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (DAC_CMSUser.CMSAdminExists())
            {
                Response.Redirect("~/Login.aspx");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;
            SaveAdminUser();
        }

        private void SaveAdminUser()
        {
            CMSUser user = new CMSUser();
            user.CreatedBy = "SYSTEM";
            user.Email = txtEmail.Text.Trim();
            user.Role = new UserRole { ID = 1 };
            user.Name = txtFirstname.Text.Trim();

            // hash password
            string salt = String.Empty;
            user.Password = Password.CreatePasswordHash(txtPassword.Text.Trim(), out salt);
            user.Salt = salt;
            user.Surname = txtSurname.Text.Trim();
            user.Username = txtUsername.Text.Trim();

            if (DAC_CMSUser.Insert(user) > 0)
            {
                Response.Redirect("~/Login.aspx?info=admin");
            }
            else
            {
                this.Page.InsertError("Failed to insert admin user please try again", "CMSUserSave");
            }
        }
    }
}
