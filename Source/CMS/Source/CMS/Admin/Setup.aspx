﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UnAuthMaster.Master" AutoEventWireup="true"
    CodeBehind="Setup.aspx.cs" Inherits="CMS.Admin.Setup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="placeholderHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="placeholderBody" runat="server">
    <p>
        Use the following form to set up an admin account for the CMS
    </p>
    <div class="errorSummary hidden">
        <p>
            Please correct the highlighted fields & verify both passwords match
        </p>
    </div>
    <asp:ValidationSummary ID="vsErrorSummary" runat="server" DisplayMode="List" ValidationGroup="CMSUserSave"
        CssClass="errorSummary" />
    <div class="formWrapper">
        <p>
            <label>
                Username*
            </label>
            <asp:TextBox ID="txtUsername" runat="server" MaxLength="20"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvUserName" runat="server" Display="None" ValidationGroup="CMSUser"
                ControlToValidate="txtUsername"></asp:RequiredFieldValidator>
        </p>
        <p>
            <label>
                Password*
            </label>
            <asp:TextBox ID="txtPassword" TextMode="Password" runat="server" MaxLength="20"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvPassword" runat="server" Display="None" ValidationGroup="CMSUser"
                ControlToValidate="txtPassword"></asp:RequiredFieldValidator>
        </p>
        <p>
            <label>
                Confirm Password*
            </label>
            <asp:TextBox ID="txtConfirmPassword" TextMode="Password" runat="server" MaxLength="20"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvConfirmPassword" runat="server" Display="None"
                ValidationGroup="CMSUser" ControlToValidate="txtConfirmPassword"></asp:RequiredFieldValidator>
            <asp:CompareValidator ID="cmpPassword" runat="server" ControlToCompare="txtPassword"
                ControlToValidate="txtConfirmPassword" Display="None" ValidationGroup="CMSUser"
                Operator="Equal"></asp:CompareValidator>
        </p>
        <p>
            <label>
                First Name*
            </label>
            <asp:TextBox ID="txtFirstname" runat="server" MaxLength="255"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" Display="None" ValidationGroup="CMSUser"
                ControlToValidate="txtFirstname"></asp:RequiredFieldValidator>
        </p>
        <p>
            <label>
                Surname*
            </label>
            <asp:TextBox ID="txtSurname" runat="server" MaxLength="255"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvSurname" runat="server" Display="None" ValidationGroup="CMSUser"
                ControlToValidate="txtSurname"></asp:RequiredFieldValidator>
        </p>
        <p>
            <label>
                Email*
            </label>
            <asp:TextBox ID="txtEmail" runat="server" MaxLength="255"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvEmail" runat="server" Display="None" ValidationGroup="CMSUser"
                ControlToValidate="txtEmail"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="revEmail" runat="server" Display="None" ValidationGroup="CMSUser"
                ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
        </p>
        <p>
            <label class="invisible">
                &nbsp;
            </label>
            <asp:Button ID="btnSave" runat="server" Text="Save" ValidationGroup="CMSUser" OnClick="btnSave_Click" />
        </p>
        <p>
            * are required fields
        </p>
    </div>
</asp:Content>
