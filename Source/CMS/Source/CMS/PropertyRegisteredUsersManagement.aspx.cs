﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Extensions;
using FluentContent.DAC;
using CMS.Helpers;
using FluentContent.Entities;
using System.Web.UI.HtmlControls;
using System.Collections.Specialized;
using FluentContent.Entities.Helpers;
using System.Data;
using Common.Email;
using System.Net;
using Common.Session;

namespace CMS
{
    public partial class PropertyRegisteredUsersManagement : System.Web.UI.Page
    {
        private int _defaultPageSize = 50;

        public bool NewUser { get { return Request.GetQueryString<int>("new-user", 0) > 0; } }
        public long UserID { get { return Request.GetQueryString<long>("user-id", 0); } }
        public string Message { get { return Request.GetQueryString<string>("info"); } }
        public bool Alerted { get { return Request.GetQueryString<int>("alerted", 0) == 1; } }
        public long DeleteUserID { get { return Request.GetQueryString<long>("delete", 0); } }

        public string SortBy { get { return Request.GetQueryString<string>("sort"); } }
        public string SortDirection { get { return Request.GetQueryString<string>("dir"); } }

        public int PageNumber { get { return Request.GetQueryString<int>("page", 1); } }

        public int PageSize
        {
            get
            {
                int pageSize = Request.GetQueryString<int>("pagesize", 0);
                if (pageSize > 0)
                    return pageSize;
                return _defaultPageSize;
            }
        }

        public CMSUser UserSession { get { return SessionObjects.GetSession<CMSUser>(SessionKeys.CMS_USER_SESSION); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (DeleteUserID > 0)
                {
                    if (DAC_PropertySubscribedUser.Delete(DeleteUserID))
                    {
                        Response.Redirect("~/property-registered-users?info=deleted");
                    }
                }

                if (UserID > 0)
                    LoadUserEditView();
                else
                {
                    if (!NewUser && UserID == 0)
                        LoadUserList();
                    if (NewUser)
                        LoadNewUserView();
                }
            }
        }

        private void LoadUserEditView()
        {
            InitControls();

            PropertySubscribedUser user = DAC_PropertySubscribedUser.Get(UserID);
            if (user == null) Response.Redirect("~/property-registered-users?info=nouser");


            ddlTitle.SelectedValue = user.Title.ID == 0 ? String.Empty : user.Title.ID.ToString();
            ddlCountry.SelectedValue = user.Country.ID == 0 ? String.Empty : user.Country.ID.ToString();
            ddlCountry_SelectedIndexChanged(this, null);
            ddlCounty.SelectedValue = user.County.ID == 0 ? String.Empty : user.County.ID.ToString();
            txtMaxPrice.Text = user.MaxPrice.ToString();
            txtMinPrice.Text = user.MinPrice.ToString();
            ddlMinBathrooms.SelectedValue = user.MinBathrooms == 0 ? String.Empty : user.MinBathrooms.ToString();
            ddlMinBedrooms.SelectedValue = user.MinBedrooms == 0 ? String.Empty : user.MinBedrooms.ToString();
            ddlPropertyType.SelectedValue = user.PropertyTypeItemID == 0 ? String.Empty : user.PropertyTypeItemID.ToString();
            ddlQualification.SelectedValue = user.QualificationID == 0 ? String.Empty : user.QualificationID.ToString();

            txtAdditionalInformation.Value = user.SpecificRequirements;
            txtAddress1.Value = user.Address1;
            txtAddress2.Value = user.Address2;
            txtAddress3.Value = user.Address3;
            txtEmail.Value = user.EmailAddress;
            txtFirstName.Value = user.FirstName;
            txtPhoneHome.Value = user.PhoneHome;
            txtPhoneMobile.Value = user.PhoneMobile;
            txtPhoneWork.Value = user.PhoneWork;
            txtPostcode.Value = user.AddressPostcode;
            txtSurname.Value = user.Surname;

            chkFreeValuation.Checked = user.InterestedInValuation;
            chkInterestedInBuying.Checked = user.WantsToBuy;
            chkInterestedInRenting.Checked = user.WantsToRent;
            chkReceiveEmails.Checked = user.EmailAlerts;
            chkOutsideArea.Checked = user.OutsideArea;
            chkParkingSpace.Checked = user.Parking;


            if (user.InterestCountyIDs != null && user.InterestCountyIDs.Count > 0)
            {
                foreach (var item in lvCounties.Items)
                {
                    HtmlInputCheckBox chkParish = item.FindControl("chkParish") as HtmlInputCheckBox;

                    foreach (var id in user.InterestCountyIDs)
                    {
                        if (chkParish.Value == id.ToString())
                        {
                            chkParish.Checked = true;
                            break;
                        }
                    }
                }
            }

        }

        private void LoadNewUserView()
        {
            InitControls();
        }
        
        private void InitControls()
        {
            mvRegisteredUsers.SetActiveView(viewAddEditUser);

            ddlTitle.DataSource = DAC_Title.Get();
            ddlTitle.DataTextField = "Description";
            ddlTitle.DataValueField = "ID";
            ddlTitle.DataBind();

            ddlTitle.Items.Insert(0, new ListItem("Select Title", String.Empty));

            ddlCountry.DataSource = DAC_Country.Get();
            ddlCountry.DataTextField = "Name";
            ddlCountry.DataValueField = "ID";
            ddlCountry.DataBind();

            ddlCountry.SelectedValue = ddlCountry.Items.FindByText("Jersey").Value;

            ddlCounty.DataSource = DAC_County.GetCountiesByCountryID(Convert.ToInt32(ddlCountry.SelectedValue));
            ddlCounty.DataTextField = "CountyName";
            ddlCounty.DataValueField = "ID";
            ddlCounty.DataBind();

            ddlCounty.Items.Insert(0, new ListItem("Select County", String.Empty));

            ddlPropertyType.DataSource = ItemHelper.LoadItemDisplay("Property Type");
            ddlPropertyType.DataTextField = "chrItemTitle";
            ddlPropertyType.DataValueField = "intItemID";
            ddlPropertyType.DataBind();

            ddlPropertyType.Items.Insert(0, new ListItem("House or Flat", String.Empty));

            lvCounties.DataSource = DAC_County.GetCountiesByCountryID(Convert.ToInt32(ddlCountry.SelectedValue));
            lvCounties.DataBind();

            ddlMinBedrooms.DataSource = ItemHelper.LoadItemDisplay("Bedrooms");
            ddlMinBedrooms.DataTextField = "chrItemTitle";
            ddlMinBedrooms.DataValueField = "intValue1";
            ddlMinBedrooms.DataBind();
            ddlMinBedrooms.Items.Insert(0, new ListItem("Minimum Bedrooms", String.Empty));

            ddlMinBathrooms.DataSource = ItemHelper.LoadItemDisplay("Bathrooms");
            ddlMinBathrooms.DataTextField = "chrItemTitle";
            ddlMinBathrooms.DataValueField = "intValue1";
            ddlMinBathrooms.DataBind();
            ddlMinBathrooms.Items.Insert(0, new ListItem("Minimum Bathrooms", String.Empty));


            ddlQualification.DataSource = ItemHelper.LoadItemDisplay("Qualification");
            ddlQualification.DataTextField = "chrItemTitle";
            ddlQualification.DataValueField = "intItemID";
            ddlQualification.DataBind();
            ddlQualification.Items.Insert(0, new ListItem("How do you qualify?", String.Empty));

        }

        private void LoadUserList()
        {
            string ascImg = "~/images/sort-asc.png";
            string descImg = "~/images/sort-desc.png";

            imgBedrooms.ImageUrl = SortBy == "beds" ? (SortDirection == "asc" ? ascImg : descImg) : String.Empty;
            imgDate.ImageUrl = SortBy == "date" ? (SortDirection == "asc" ? ascImg : descImg) : String.Empty;
            imgEmail.ImageUrl = SortBy == "email" ? (SortDirection == "asc" ? ascImg : descImg) : String.Empty;
            imgEmailAlerts.ImageUrl = SortBy == "alerts" ? (SortDirection == "asc" ? ascImg : descImg) : String.Empty;
            imgMaxPrice.ImageUrl = SortBy == "max" ? (SortDirection == "asc" ? ascImg : descImg) : String.Empty;
            imgMinPrice.ImageUrl = SortBy == "min" ? (SortDirection == "asc" ? ascImg : descImg) : String.Empty;
            imgName.ImageUrl = SortBy == "name" ? (SortDirection == "asc" ? ascImg : descImg) : String.Empty;
            imgSearchType.ImageUrl = SortBy == "search" ? (SortDirection == "asc" ? ascImg : descImg) : String.Empty;
            imgCreated.ImageUrl = SortBy == "created" ? (SortDirection == "asc" ? ascImg : descImg) : String.Empty;

            imgBedrooms.Visible = !String.IsNullOrEmpty(imgBedrooms.ImageUrl);
            imgDate.Visible = !String.IsNullOrEmpty(imgDate.ImageUrl);
            imgEmail.Visible = !String.IsNullOrEmpty(imgEmail.ImageUrl);
            imgEmailAlerts.Visible = !String.IsNullOrEmpty(imgEmailAlerts.ImageUrl);
            imgMaxPrice.Visible = !String.IsNullOrEmpty(imgMaxPrice.ImageUrl);
            imgMinPrice.Visible = !String.IsNullOrEmpty(imgMinPrice.ImageUrl);
            imgName.Visible = !String.IsNullOrEmpty(imgName.ImageUrl);
            imgSearchType.Visible = !String.IsNullOrEmpty(imgSearchType.ImageUrl);
            imgCreated.Visible = !String.IsNullOrEmpty(imgCreated.ImageUrl);

            NameValueCollection collection = HttpUtility.ParseQueryString(Request.Url.Query);
            collection.Remove("pagesize");
            collection.Remove("page");
            collection.Remove("sort");
            collection.Remove("dir");

            string urlTemplate = "~/property-registered-users?sort={0}{1}&page={2}&pagesize={3}{4}";

            hlBedrooms.NavigateUrl = String.Format(urlTemplate, "beds", GetDirectionForColumn("beds"), PageNumber, PageSize, collection.Count > 0 ? ("&" + collection.ToQueryString(false)) : String.Empty);
            hlDate.NavigateUrl = String.Format(urlTemplate, "date", GetDirectionForColumn("date"), PageNumber, PageSize, collection.Count > 0 ? ("&" + collection.ToQueryString(false)) : String.Empty);
            hlEmail.NavigateUrl = String.Format(urlTemplate, "email", GetDirectionForColumn("email"), PageNumber, PageSize, collection.Count > 0 ? ("&" + collection.ToQueryString(false)) : String.Empty);
            hlEmailAlerts.NavigateUrl = String.Format(urlTemplate, "alerts", GetDirectionForColumn("alerts"), PageNumber, PageSize, collection.Count > 0 ? ("&" + collection.ToQueryString(false)) : String.Empty);
            hlMaxPrice.NavigateUrl = String.Format(urlTemplate, "max", GetDirectionForColumn("max"), PageNumber, PageSize, collection.Count > 0 ? ("&" + collection.ToQueryString(false)) : String.Empty);
            hlMinPrice.NavigateUrl = String.Format(urlTemplate, "min", GetDirectionForColumn("min"), PageNumber, PageSize, collection.Count > 0 ? ("&" + collection.ToQueryString(false)) : String.Empty);
            hlName.NavigateUrl = String.Format(urlTemplate, "name", GetDirectionForColumn("name"), PageNumber, PageSize, collection.Count > 0 ? ("&" + collection.ToQueryString(false)) : String.Empty);
            hlSearchType.NavigateUrl = String.Format(urlTemplate, "search", GetDirectionForColumn("search"), PageNumber, PageSize, collection.Count > 0 ? ("&" + collection.ToQueryString(false)) : String.Empty);
            hlCreated.NavigateUrl = String.Format(urlTemplate, "created", GetDirectionForColumn("created"), PageNumber, PageSize, collection.Count > 0 ? ("&" + collection.ToQueryString(false)) : String.Empty);


            ResultSet<PropertySubscribedUser> result = DAC_PropertySubscribedUser.GetAllPaged(new PagingParameter
            {
                PageNumber = PageNumber,
                RowsPerPage = PageSize,
                SortBy = SortBy,
                SortDirection = SortDirection
            });

            lvUsers.DataSource = result.Items;
            lvUsers.DataBind();


            litItemCount.Text = String.Format("({0} users)", result.TotalRows);

            if (result.TotalPages == 0 || result.TotalPages == 1)
            {
                divTopPager.Visible = false;
                divBottomPager.Visible = false;
            }
            else
            {

                List<int> pages = new List<int>();
                for (int i = 0; i < result.TotalPages; i++)
                {
                    pages.Add(i + 1);
                }
                if (pages == null || pages.Count == 1)
                {
                    lvPagerTop.DataSource = null;
                    lvPagerBottom.DataSource = null;
                }
                else
                {
                    lvPagerTop.DataSource = pages;
                    lvPagerTop.DataBind();
                    lvPagerBottom.DataSource = pages;
                    lvPagerBottom.DataBind();
                }
                // buttons
                PrepareNextPreviousButtons(result);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveUser(false);
        }

        protected void btnSaveAndAlert_Click(object sender, EventArgs e)
        {
            SaveUser(true);
        }

        private void SaveUser(bool sendAlert)
        {
            string email = txtEmail.Value.Trim();
            var existingUser = String.IsNullOrEmpty(email) ? null : DAC_PropertySubscribedUser.GetByEmail(email);

            if (existingUser != null && UserID == 0)
            {
                this.InsertError("Failed to submit, user is already registered!", "PropertyRegisteredUserGroupServer");
                return;
            }

            string additionalInformation = txtAdditionalInformation.Value.Trim();
            string address1 = txtAddress1.Value.Trim();
            string address2 = txtAddress2.Value.Trim();
            string address3 = txtAddress3.Value.Trim();
            string postcode = txtPostcode.Value.Trim();

            int titleID = String.IsNullOrEmpty(ddlTitle.SelectedValue) ? 0 : ddlTitle.SelectedValue.ToInt32();
            string firstName = txtFirstName.Value.Trim();
            string surname = txtSurname.Value.Trim();
            int countryID = String.IsNullOrEmpty(ddlCountry.SelectedValue) ? 0 : ddlCountry.SelectedValue.ToInt32();
            int countyID = String.IsNullOrEmpty(ddlCounty.SelectedValue) ? 0 : ddlCounty.SelectedValue.ToInt32();
            string mobile = txtPhoneMobile.Value.Trim();
            string homePhone = txtPhoneHome.Value.Trim();
            string workPhone = txtPhoneWork.Value.Trim();
            bool wantsFreeValuation = chkFreeValuation.Checked;
            bool interestedInRenting = chkInterestedInRenting.Checked;
            bool interestedInBuying = chkInterestedInBuying.Checked;

            int propertyTypeID = String.IsNullOrEmpty(ddlPropertyType.SelectedValue) ? 0 : ddlPropertyType.SelectedValue.ToInt32();
            int minBedrooms = ddlMinBedrooms.SelectedValue == String.Empty ? 0 : ddlMinBedrooms.SelectedValue.ToInt32();
            int minBathrooms = ddlMinBathrooms.SelectedValue == String.Empty ? 0 : ddlMinBathrooms.SelectedValue.ToInt32();
            int qualificationID = String.IsNullOrEmpty(ddlQualification.SelectedValue) ? 0 : ddlQualification.SelectedValue.ToInt32();
            decimal minPrice = Convert.ToDecimal(txtMinPrice.Text.Trim() == String.Empty ? "0" : txtMinPrice.Text.Trim());
            decimal maxPrice = Convert.ToDecimal(txtMaxPrice.Text.Trim() == String.Empty ? "0" : txtMaxPrice.Text.Trim());

            bool wantsToReceiveEmails = chkReceiveEmails.Checked;
            bool outsideArea = chkOutsideArea.Checked;
            bool parkingSpace = chkParkingSpace.Checked;

            List<int> parishes = GetSelectedParishes();

            PropertySubscribedUser user = new PropertySubscribedUser
            {
                Address1 = address1,
                Address2 = address2,
                Address3 = address3,
                AddressPostcode = postcode,
                Country = new Country { ID = countryID },
                County = new County { ID = countyID },
                EmailAddress = email,
                EmailAlerts = wantsToReceiveEmails,
                FirstName = firstName,
                InterestCountyIDs = parishes,
                InterestedInValuation = wantsFreeValuation,
                MaxPrice = maxPrice,
                MinPrice = minPrice,
                MinBathrooms = minBathrooms,
                MinBedrooms = minBedrooms,
                PhoneHome = homePhone,
                PhoneMobile = mobile,
                PhoneWork = workPhone,
                PropertyTypeItemID = propertyTypeID,
                QualificationID = qualificationID,
                SpecificRequirements = additionalInformation,
                Subscribed = true,
                Surname = surname,
                Title = new Title { ID = titleID },
                WantsToBuy = interestedInBuying,
                WantsToRent = interestedInRenting,
                OutsideArea = outsideArea,
                Parking = parkingSpace
            };

            bool sent = false;

            try
            {
                if (UserID == 0)
                {
                    user = DAC_PropertySubscribedUser.Insert(user);
                    // check if any alerts should be sent
                    if (user.EmailAlerts && sendAlert)
                    {
                        List<Property> matchingProperties = DAC_Property.PropertyiesBySubscribedUserCriteria(user);
                        sent = EmailForCriteriaMatchingProperties(matchingProperties, user);
                    }
                }
                else
                {

                    user.ID = UserID;
                    user = DAC_PropertySubscribedUser.Update(user);
                    if (user.EmailAlerts && sendAlert)
                    {
                        List<Property> matchingProperties = DAC_Property.PropertyiesBySubscribedUserCriteria(user);
                        sent = EmailForCriteriaMatchingProperties(matchingProperties, user);
                    }
                }
            }
            catch (Exception ex)
            {
                this.InsertError(ex.Message, "PropertyRegisteredUserGroupServer");
            }

            if (user == null) { this.InsertError("Failed to submit form", "PropertyRegisteredUserGroupServer"); return; }

            if (!sent)
                Response.Redirect("~/property-registered-users?info=" + (UserID > 0 ? "updated" : "added"));
            else
                Response.Redirect("~/property-registered-users?alerted=1&info=" + (UserID > 0 ? "updated" : "added"));
        }

        private List<int> GetSelectedParishes()
        {
            List<int> selectedParishes = new List<int>();
            foreach (var item in lvCounties.Items)
            {
                HtmlInputCheckBox chkParish = item.FindControl("chkParish") as HtmlInputCheckBox;
                if (chkParish.Checked)
                {
                    selectedParishes.Add(chkParish.Value.ToInt32());
                }
            }
            return selectedParishes;
        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            int countryID = String.IsNullOrEmpty(ddlCountry.SelectedValue) ? 0 : ddlCountry.SelectedValue.ToInt32();

            if (countryID > 0)
            {
                List<County> items = DAC_County.GetCountiesByCountryID(countryID);
                ddlCounty.DataSource = items;
                ddlCounty.DataTextField = "CountyName";
                ddlCounty.DataValueField = "ID";
                ddlCounty.DataBind();

                ddlCounty.Items.Insert(0, new ListItem("Select County", String.Empty));

                lvCounties.DataSource = items;
                lvCounties.DataBind();
            }
        }

        private string GetDirectionForColumn(string column)
        {
            string dirTemplate = "&dir={0}";
            string val;

            val = SortBy == column ? (SortDirection == "asc" ? "desc" : "asc") : "desc";

            return String.Format(dirTemplate, val);
        }

        #region Paging

        private void PrepareNextPreviousButtons(ResultSet<PropertySubscribedUser> result)
        {
            string classForPrev = String.Empty;
            string classForNext = String.Empty;

            NameValueCollection collection = HttpUtility.ParseQueryString(Request.Url.Query);
            collection.Remove("pagesize");
            collection.Remove("page");
            string previousUrl = ResolveUrl(String.Format("~/property-registered-users?page={0}&pagesize={1}{2}", PageNumber - 1, PageSize, collection.Count > 0 ? ("&" + collection.ToQueryString(false)) : String.Empty));
            string nextUrl = ResolveUrl(String.Format("~/property-registered-users?page={0}&pagesize={1}{2}", PageNumber + 1, PageSize, collection.Count > 0 ? ("&" + collection.ToQueryString(false)) : String.Empty));

            if ((result.TotalPages <= 1) ||
                (result.TotalPages == 1 && PageNumber == 1))
            {
                // disable next and prev
                classForPrev = "deactive";
                classForNext = "deactive";
                previousUrl = String.Empty;
                nextUrl = String.Empty;
            }

            if (result.TotalPages > 1)
            {
                if (PageNumber == 1)
                {
                    // disable prev
                    classForPrev = "deactive";
                    previousUrl = String.Empty;

                    // enable next
                    classForNext = String.Empty;
                }
                else
                {
                    if (PageNumber == result.TotalPages)
                    {
                        // enable prev
                        classForPrev = String.Empty;

                        // disable next
                        classForNext = "deactive";
                        nextUrl = String.Empty;
                    }

                }

            }

            //if (previousUrl.Length == 0)
            //{
            //    litTopPrev.Text = "<span class='prev'>Prev</span>";
            //    litBottomPrev.Text = litTopPrev.Text;
            //}
            //else
            //{
            //    litTopPrev.Text = String.Format("<a href='{0}' class='prev {1}'><strong>Prev</strong></a>", previousUrl, classForPrev);
            //    litBottomPrev.Text = litTopPrev.Text;
            //}


            //if (nextUrl.Length == 0)
            //{
            //    litTopNext.Text = "<span class='next'>Next</span>";
            //    litBottomNext.Text = litTopNext.Text;
            //}
            //else
            //{
            //    litTopNext.Text = String.Format("<a href='{0}' class='next {1}'><strong>Next</strong></a>", nextUrl, classForNext);
            //    litBottomNext.Text = litTopNext.Text;
            //}

            if (previousUrl.Length == 0)
            {
                litTopPrev.Text = "<li id=\"editable_previous\" class=\"paginate_button previous disabled\"> <a href=\"#\" aria-controls=\"editable\" data-dt-idx=\"0\" tabindex=\"0\">Previous</a></li>";
                litBottomPrev.Text = litTopPrev.Text;
            }
            else
            {
                litTopPrev.Text = "<li id=\"editable_previous\" class=\"paginate_button previous\">";
                litTopPrev.Text += String.Format("<a href='{0}' aria-controls=\"editable\" data-dt-idx=\"\0\" tabindex=\"0\">Previous</a>", previousUrl);
                litTopPrev.Text += "</li>";
                litBottomPrev.Text = litTopPrev.Text;
            }


            if (nextUrl.Length == 0)
            {
                litTopNext.Text = "<li id=\"editable_next\" class=\"paginate_button next disabled\"> <a href=\"#\" aria-controls=\"editable\" data-dt-idx=\"0\" tabindex=\"0\">Next</a></li>";
                litBottomNext.Text = litTopNext.Text;
            }
            else
            {
                litTopNext.Text += "<li id=\"editable_previous\" class=\"paginate_button next\">";
                litTopNext.Text += String.Format("<a href='{0}' aria-controls=\"editable\" data-dt-idx=\"\0\" tabindex=\"0\">Next</a>", nextUrl);
                litTopNext.Text += "</li>";
                litBottomNext.Text = litTopNext.Text;
            }
        }

        public string GetPageLink(object pageNumber)
        {
            NameValueCollection collection = HttpUtility.ParseQueryString(Request.Url.Query);
            collection.Remove("pagesize");
            collection.Remove("page");
            return ResolveUrl(String.Format("~/property-registered-users?page={0}&pagesize={1}{2}", pageNumber, PageSize, collection.Count > 0 ? ("&" + collection.ToQueryString(false)) : String.Empty));
        }

        public string GetPagerActiveClass(object pageNumber, string cssClass)
        {
            int page = Convert.ToInt32(pageNumber);
            if (page == PageNumber)
                return String.Format("class='{0}'", cssClass);
            return String.Empty;
        }

        #endregion

        #region
        private bool EmailForCriteriaMatchingProperties(List<Property> matchingProperties, PropertySubscribedUser u)
        {
            bool result = false;

            if (matchingProperties.Count == 0) return result;

            DataTable dtValuationEmail = DAC_Item.GetItemByTitle("Email templates", "Registration Alert");

            string propertyTable = "<table>{0}</table>";
            string rowTemplate = "<tr><td><img src='{0}' width='100' height='75' alt='property-lead-image' /></td><td style='vertical-align:top;'><a href='{1}'>{2}</a></td></tr>";

            string rows = String.Empty;
            foreach (var item in matchingProperties)
            {
                rows += String.Format(rowTemplate, item.LeadImage == null ? String.Format("{0}/img/PlaceHolder-Unq-small.png", FluentContentSettings.WebsiteRoot) :
                    String.Format("{0}/property-image/{1}?w=100&h=75", FluentContentSettings.WebsiteRoot, item.LeadImage.GUID),
                    item.WebsitePropertyURL, item.Title);

                DAC_PropertySubscriberAlert.Insert(new PropertySubscriberAlert
                {
                    ActivatedByCMSUserID = UserSession.ID,
                    PropertyID = item.ID,
                    PropertySubscriberUserID = u.ID,
                    EmailAlertItemID = dtValuationEmail.GetDataTableValue<int>(0, "intItemID", 0)
                });
            }

            propertyTable = String.Format(propertyTable, rows);


            if (dtValuationEmail != null && dtValuationEmail.Rows.Count > 0)
            {
                string recipients = String.Format("{0};{1}", u.EmailAddress, dtValuationEmail.GetDataTableValue(0, "chrText1", String.Empty));
                string from = dtValuationEmail.GetDataTableValue(0, "chrText2", String.Empty);
                string displayName = dtValuationEmail.GetDataTableValue(0, "chrText3", String.Empty);
                string cc = dtValuationEmail.GetDataTableValue(0, "chrText4", String.Empty);
                string bcc = dtValuationEmail.GetDataTableValue(0, "chrText5", String.Empty);
                string body = dtValuationEmail.GetDataTableValue(0, "chrText11", String.Empty);
                string subject = dtValuationEmail.GetDataTableValue(0, "chrText6", String.Empty);
                string password = dtValuationEmail.GetDataTableValue(0, "chrText7", String.Empty);

                // set default sender in case it is empty
                if (from == String.Empty) from = FluentContent.Settings.FailSafeEmail;

                // replace placeholders
                body = EmailManager.ReplaceKeys(body, new Dictionary<string, string> 
                {                            
                    {"##FIRSTNAME##", u.FirstName },                            
                    {"##PROPERTY_TABLE##", propertyTable },
                         
                });
                body = String.Format("<html><body>{0}</body></html>", body);
                NetworkCredential cred = new NetworkCredential(from, password);

                try
                {
                    result = EmailManager.SendEmail(recipients, from, displayName, subject, body, true, cc, bcc, null, cred);

                }
                catch { }


            }
            return result;
        }
        #endregion
    }
}