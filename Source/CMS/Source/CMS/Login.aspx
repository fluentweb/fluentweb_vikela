﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UnAuthMaster.Master" AutoEventWireup="true"
    CodeBehind="Login.aspx.cs" Inherits="CMS.Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="placeholderHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="placeholderBody" runat="server">
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>
                <img src="images/fluentLogo.jpg" width="100%" alt="" height="100px" />
            </div>
            <br />
            <div class="centered">
                <% 
                    switch (Info)
                    {
                        case "admin":
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Admin account successfully created please login below','success');", true);
                            break;
                        case "expired":
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Your session has expired please login below','warning');", true);
                            break;
                        case "loggedout":
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('You have successfully logged out','info');", true);
                            break;
                        default:
                            break;
                    }
                %>
                <BP:UCLogin ID="UCLogin" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>

