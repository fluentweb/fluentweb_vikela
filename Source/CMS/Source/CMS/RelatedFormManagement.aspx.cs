﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Extensions;
using CMS.Helpers;
using CMS.Helpers.Bases;
using FluentContent.Entities.Enums;


namespace CMS
{
    public partial class RelatedFormManagement : BasePage
    {
        

        protected void Page_Load(object sender, EventArgs e)
        {
            // check user rights
            if (UserSession.Role.CMSAccessLevel != CMSAccessLevel.Full) Response.Redirect("~/Home.aspx");

           
        }
    }
}
