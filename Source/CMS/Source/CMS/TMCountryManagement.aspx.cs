﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Extensions;
using FluentContent.DAC;
using FLContentSiteAssist.BusinessLogic;
using CMS.Helpers.Bases;
using FluentContent.Entities.Enums;
using System.Data;
using FluentContent.Entities;

namespace CMS
{
    public partial class TMCountryManagement : BasePage
    {
        public int CountryID { get { return Request.GetQueryString<int>("countryid", -1); } }
        public string Action { get { return Request.GetQueryString<string>("action"); } }
        protected void Page_Load(object sender, EventArgs e)
        {
            // check user rights
            if (UserSession.Role.CMSAccessLevel != CMSAccessLevel.Full) Response.Redirect("~/Home.aspx");

            if (!IsPostBack)
            {
                if (CountryID != -1 || Action == "create")
                {
                    LoadCountryDetail();
                    if (Action == "create")
                        litBreadCromp.Text = "<b>Create</b>";
                    else
                        litBreadCromp.Text = "<b>Edit</b>";
                }
                else
                    LoadCountryListing();
            }
        }
        private void LoadCountryListing()
        {
            count.Text = "0";
            DataTable dtTMCountry = DAC_TMCountry.GetCountryByCountry();
            if (dtTMCountry.Rows.Count > 0)
                count.Text = dtTMCountry.Rows.Count.ToString();
            lvTmCountry.DataSource = dtTMCountry;
            lvTmCountry.DataBind();
        }
        private void LoadCountryDetail()
        {
            mvPages.SetActiveView(viewDetail);
        }

        protected void btnCreateTop_Click(object sender, EventArgs e)
        {
            Response.Redirect(ResolveUrl(String.Format("~/TMCountryManagement.aspx?countryid={0}&action=create", CountryID)));
        }

        protected void btndelete_Click(object sender, EventArgs e)
        {
            int countryId = ((LinkButton)sender).CommandArgument.ToInt32();
            bool result = false;
            if (countryId > 0)
                result = DAC_TMCountry.Delete(new TMCountry { CountryID = countryId });

            if (result)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Delete success','success');", true);
                Response.Redirect(Request.RawUrl);
            }
            else
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Delete failed!','warning');", true);
        }
    }
}