﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCLogin.ascx.cs" Inherits="CMS.UserControls.UCLogin" %>

<div class="errorSummary hidden" validationgroup="LoginGroup">
    <p>
        Please correct the highlighted fields
    </p>
</div>
<asp:ValidationSummary ID="vsErrorSummary" runat="server" ValidationGroup="LoginGroupServer"
    CssClass="errorSummary" />

<div class="form-group">
    <asp:TextBox runat="server" ID="txtUsername" type="text" class="form-control" placeholder="Username" required=""> </asp:TextBox>
    <asp:RequiredFieldValidator ID="rfvUserName" runat="server" Display="None" ValidationGroup="LoginGroup"
        ControlToValidate="txtUsername"></asp:RequiredFieldValidator>

</div>
<div class="form-group">
    <asp:TextBox ID="txtPassword" runat="server" type="password" class="form-control" placeholder="Password" required=""></asp:TextBox>
    <asp:RequiredFieldValidator ID="rfvPassword" runat="server" Display="None" ValidationGroup="LoginGroup"
        ControlToValidate="txtPassword"></asp:RequiredFieldValidator>

</div>
<asp:Button ID="btnLogin" runat="server" type="submit" class="btn btn-primary block full-width m-b" OnClick="btnLogin_Click" Text="Login" />

<a href="~/ForgotPassword.aspx" runat="server"><small>Forgot password?</small></a>
<p class="m-t">
    <small>Vikela Content Managment System</small>
</p>
