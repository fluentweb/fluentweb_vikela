﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Extensions;


using Common.Security;
using CMS.Helpers;
using Common.Session;

using FluentContent.Entities;
using FluentContent.DAC;
using FluentContent.Entities.Enums;

namespace CMS.UserControls
{
    public partial class UCLogin : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                txtUsername.Focus();
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;
            string error = LoginUser();
            if (String.IsNullOrEmpty(error))
            {
                Response.Redirect("~/Home.aspx");
            }
            else
            {
               ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('"+ error + "','error');", true);
               //this.Page.InsertError(error, "LoginGroupServer");
            }

        }

        private string LoginUser()
        {
            CMSUser user = DAC_CMSUser.Get(txtUsername.Text.Trim());
            if (user == null) return "Username does not exist in our database";
           // if (!Password.PasswordMatchesHash(txtPassword.Text.Trim(), user.Salt, user.Password)) return "Password incorrect";

            // check if user has access rights to cms
            if (user.Role.CMSAccessLevel == CMSAccessLevel.None)
            {
                return "Username does not exist in our database";
            }

            if(user.Locked)
            {
                return "Your account has been locked";
            }

            if (user.Role.ID != 1 && user.ClientID != FluentContent.Settings.ClientID)
            {
                return "Invalid client";
            }

            
            // check for expired entity
            if (user.EntityID > 0)
            {
                Entity userEntity = DAC_Entity.GetByID(user.EntityID, user.ClientID);
                if (userEntity != null)
                {
                    if (userEntity.DateExpires != DateTime.MinValue && userEntity.DateExpires <= DateTime.Today)
                        return "Your account has expired. Please contact the webmaster";                    
                }
            }

            user.ClientID = FLContentSiteAssist.DataAccess.Client.ClientID;

            SessionObjects.SetSession<CMSUser>(SessionKeys.CMS_USER_SESSION, user);
            return null;
        }
    }
}