﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCAddEditCMSUser.ascx.cs"
    Inherits="CMS.UserControls.UCAddEditCMSUser" %>

<asp:Panel ID="pnlAddEditCMSUser" runat="server" DefaultButton="btnSave">
    <div class="form-horizontal">
        <div class="errorSummary hidden" validationgroup="CMSUser">
            <center>
                         <p>
                            Please correct the highlighted fields
                        </p>
                        </center>
        </div>
        <asp:ValidationSummary ID="vsErrorSummary" runat="server" DisplayMode="List" ValidationGroup="CMSUserSave"
            CssClass="errorSummary" />

        <div class="form-group">
            <label class="col-sm-2 control-label">
                Username*
            </label>
            <div class="col-sm-10">
                <asp:TextBox ID="txtUsername" CssClass="form-control" runat="server" required=""></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvUserName" runat="server" Display="None" ValidationGroup="CMSUser"
                    ControlToValidate="txtUsername"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="hr-line-dashed"></div>

        <div class="form-group">
            <label class="col-sm-2 control-label">
                First Name*
            </label>
            <div class="col-sm-10">
                <asp:TextBox ID="txtFirstname" CssClass="form-control" runat="server" required=""></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" Display="None" ValidationGroup="CMSUser"
                    ControlToValidate="txtFirstname"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="hr-line-dashed"></div>

        <div class="form-group">
            <label class="col-sm-2 control-label">
                Surname*
            </label>
            <div class="col-sm-10">
                <asp:TextBox ID="txtSurname" CssClass="form-control" runat="server" required=""></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvSurname" runat="server" Display="None" ValidationGroup="CMSUser"
                    ControlToValidate="txtSurname"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="hr-line-dashed"></div>

        <div class="form-group">
            <label class="col-sm-2 control-label">
                Email*
            </label>
            <div class="col-sm-10">
                <asp:TextBox ID="txtEmail" type="email" CssClass="form-control" runat="server" required=""></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvEmail" runat="server" Display="None" ValidationGroup="CMSUser"
                    ControlToValidate="txtEmail"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revEmail" runat="server" Display="None" ValidationGroup="CMSUser"
                    ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="hr-line-dashed"></div>

        <asp:Panel ID="pnlClient" runat="server">
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    Client
                </label>
                <div class="col-sm-10">
                    <asp:DropDownList ID="ddlClient" CssClass="form-control m-b" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlClient_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="hr-line-dashed"></div>

        </asp:Panel>
        <div class="form-group">
            <label class="col-sm-2 control-label">
                User Role
            </label>
            <div class="col-sm-10">
                <asp:DropDownList ID="ddlRole" runat="server" CssClass="form-control m-b" OnSelectedIndexChanged="ddlRole_SelectedIndexChanged"
                    CausesValidation="false" AutoPostBack="true">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvRole" runat="server" ErrorMessage="Role is required"
                    ValidationGroup="CMSUser" Display="None" ControlToValidate="ddlRole" InitialValue="0"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="hr-line-dashed"></div>

        <asp:Panel ID="pnlEntityArea" runat="server">
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    Entity
                </label>
                <div class="col-sm-10">
                    <asp:DropDownList ID="ddlEntity" runat="server" CssClass="form-control m-b" AutoPostBack="true" OnSelectedIndexChanged="ddlEntity_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvEntity" runat="server" ErrorMessage="This role requires an entity"
                        ValidationGroup="CMSUser" Display="None" ControlToValidate="ddlEntity" InitialValue="0"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="hr-line-dashed"></div>

        </asp:Panel>
        <div class="form-group">
            <label class="col-sm-2 control-label">
                Department
            </label>
            <div class="col-sm-10">
                <asp:DropDownList ID="ddlDepartment" CssClass="form-control m-b" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="hr-line-dashed"></div>

        <div class="form-group">
            <label class="col-sm-2 control-label">
                Locked
            </label>
            <div class="col-sm-10">
                <asp:CheckBox ID="chkLocked" runat="server" />
            </div>
        </div>
        <div class="hr-line-dashed"></div>
        <div class="form-group">
            <div class="col-sm-4 col-sm-offset-2">
                <asp:Button runat="server" ID="btnSave" ValidationGroup="CMSUser" CssClass="btn btn-primary" type="submit"
                    OnClick="btnSave_Click" Text="Submit" />
            </div>
        </div>
        <%--                        <div style="float: left;">
                            <%--                            <span id="spnInfoUpdated" runat="server" class="infoMessage" visible="false">Details
                updated successfully!</span> <span id="spnInfoCreated" runat="server" class="infoMessage"
                    visible="false">User created successfully!</span>
                        </div>--%>
    </div>
</asp:Panel>
