﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Extensions;


using Common.Security;
using CMS.Helpers;
using System.Data.Common;
using DataAccess;
using Common.Email.Entities;
using Common.Email;
using Common.Email.DAC;
using Common.Session;
using FluentContent.DAC;
using FluentContent.Entities;
using System.Data;
using Common.Extensions;
using System.Net;
using FluentContent.Entities.Enums;

namespace CMS.UserControls
{
    public partial class UCAddEditCMSUser : System.Web.UI.UserControl
    {
        public delegate void OnUserSavedHandler(CMSUser user, EventArgs e);
        public event OnUserSavedHandler OnUserSaved;

        public CMSUser LoggedInUser { get { return SessionObjects.GetSession<CMSUser>(SessionKeys.CMS_USER_SESSION); } }

        public int UserID { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (UserID > 0)
                {
                    // edit mode                    
                    LoadUserDetails();
                }
                else
                {
                    // insert mode
                    ClearForm();
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;
            DbFactory.Connect(SaveUser);
        }

        protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            UserRole selectedRole = DAC_UserRole.GetByID(ddlRole.SelectedValue.ToInt32(), LoggedInUser.ClientID);
            if (selectedRole == null) return;
            pnlEntityArea.Visible = selectedRole.EntityRequired;
            ddlEntity.ClearSelection();
            if (LoggedInUser.EntityID != 0)
            {
                ddlEntity.SelectedValue = LoggedInUser.EntityID.ToString();
                pnlEntityArea.Visible = false;
            }
            ddlEntity_SelectedIndexChanged(sender, e);
        }

        protected void ddlEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<UserDepartment> departments = DAC_UserDepartment.GetByEntityID(ddlEntity.SelectedValue.ToInt32());
            if(LoggedInUser.ClientID != 0)
                departments = departments.FindAll(i => i.ClientID == FluentContent.Settings.ClientID);

            List<UserDepartment> entityDepartments = departments;
            ddlDepartment.DataBindWithDataSource(entityDepartments, "Name", "ID");
            ddlDepartment.SetDefaultValue("Select Department", "0", true);
        }

        protected void ddlClient_SelectedIndexChanged(object sender, EventArgs e)
        {
            // load user roles available to current user
            ddlRole.DataBindWithDataSource(DAC_UserRole.Get(LoggedInUser.Role.RoleHierarchy, ddlClient.SelectedValue.ToInt32()), "Name", "ID");
            if (ddlRole.Items.Count == 0)
                ddlRole.Items.Insert(0, new ListItem("No role available", "0"));
            ddlRole_SelectedIndexChanged(sender, e);
        }

        private void InitControlsEdit()
        {

            // if logged in user has entity or no entity and editing own details 
            if (LoggedInUser.ID == UserID && LoggedInUser.EntityID == 0
                || LoggedInUser.ID == UserID && LoggedInUser.EntityID != 0)
            {
                // load departments for logged in user

                List<UserDepartment> departments = DAC_UserDepartment.GetByEntityID(LoggedInUser.EntityID);
                if (LoggedInUser.ClientID != 0 && LoggedInUser.EntityID != 0)
                    departments = departments.FindAll(i => i.ClientID == FluentContent.Settings.ClientID);

                ddlDepartment.DataBindWithDataSource(departments, "Name", "ID");
                ddlDepartment.SetDefaultValue("Select Department", "0", true);
            }
            // if has entity or no entity and editing other user details
            else if (LoggedInUser.ID != UserID && LoggedInUser.EntityID == 0
                || LoggedInUser.ID != UserID && LoggedInUser.EntityID != 0)
            {
                // load departments for user being edited.
                CMSUser editedUser = DAC_CMSUser.Get(UserID);

                List<UserDepartment> departments = DAC_UserDepartment.GetByEntityID(editedUser.EntityID);
                if (LoggedInUser.ClientID != 0 && LoggedInUser.EntityID != 0)
                    departments = departments.FindAll(i => i.ClientID == FluentContent.Settings.ClientID);

               
                ddlDepartment.DataBindWithDataSource(departments, "Name", "ID");
                ddlDepartment.SetDefaultValue("Select Department", "0", true);
            }

            // if no entity and editing own details hide entities
            if (LoggedInUser.ID == UserID && LoggedInUser.EntityID == 0)
                pnlEntityArea.Visible = false;
            else
            {
                // load entities   
                ddlEntity.DataBindWithDataSource(DAC_Entity.Get(LoggedInUser.ClientID), "Name", "ID");
                ddlEntity.SetDefaultValue("Select Entity", "0", true);
            }

            // load user roles available to current user   
            if (LoggedInUser.ClientID != 0 && LoggedInUser.EntityID == 0)
                ddlRole.DataBindWithDataSource(DAC_UserRole.Get(LoggedInUser.Role.RoleHierarchy, 0), "Name", "ID");
            else
                ddlRole.DataBindWithDataSource(DAC_UserRole.Get(LoggedInUser.Role.RoleHierarchy, LoggedInUser.ClientID), "Name", "ID");

            if (ddlRole.Items.Count == 0)
                ddlRole.Items.Insert(0, new ListItem("No role available", "0"));

            if (LoggedInUser.Role.ID == 1)
            {
                ddlClient.DataSource = DAC_Client.Get();
                ddlClient.DataTextField = "Name";
                ddlClient.DataValueField = "ID";
                ddlClient.DataBind();
                pnlClient.Visible = true;
                ddlClient.Items.Insert(0, new ListItem("All Clients", "0"));
            }
            else 
            {
                pnlClient.Visible = false;
            }
        }

        private void InitControlsAdd()
        {
            // load user roles available to current user
            if (LoggedInUser.Role.CMSAccessLevel == CMSAccessLevel.Full)
            {
                ddlRole.DataBindWithDataSource(DAC_UserRole.Get(LoggedInUser.Role.RoleHierarchy, 0), "Name", "ID");
            }
            else
            {
                ddlRole.DataBindWithDataSource(DAC_UserRole.Get(LoggedInUser.Role.RoleHierarchy, LoggedInUser.ClientID), "Name", "ID");
            }
            if (ddlRole.Items.Count == 0)
                ddlRole.Items.Insert(0, new ListItem("No role available", "0"));

            // load entities
            ddlEntity.DataBindWithDataSource(DAC_Entity.Get(LoggedInUser.ClientID), "Name", "ID");
            ddlEntity.SetDefaultValue("Select Entity", "0", true);

            // load departments available for logged in user entity

            List<UserDepartment> departments = DAC_UserDepartment.GetByEntityID(LoggedInUser.EntityID);
            if (LoggedInUser.ClientID != 0)
                departments = departments.FindAll(i => i.ClientID == FluentContent.Settings.ClientID);

            ddlDepartment.DataBindWithDataSource(departments, "Name", "ID");
            ddlDepartment.SetDefaultValue("Select Department", "0", true);

            // if logged in user has entity set same entity for new user and disable change of entity
            if (LoggedInUser.EntityID > 0)
            {
                ddlEntity.SelectedValue = LoggedInUser.EntityID.ToString();
                pnlEntityArea.Visible = false;
            }

            if (LoggedInUser.Role.ID == 1)
            {
                ddlClient.DataSource = DAC_Client.Get();
                ddlClient.DataTextField = "Name";
                ddlClient.DataValueField = "ID";
                ddlClient.DataBind();
                pnlClient.Visible = true;
                ddlClient.Items.Insert(0, new ListItem("All Clients", "0"));          
            }
            else
            {
                pnlClient.Visible = false;
            }

        }

        public bool LoadUserDetails()
        {
            InitControlsEdit();
            if (UserID < 1) return false;

            CMSUser user = DAC_CMSUser.Get(UserID);
            if (user == null) return false;

            // set values
            txtEmail.Text = user.Email;
            txtFirstname.Text = user.Name;
            txtSurname.Text = user.Surname;
            txtUsername.Text = user.Username;
            btnSave.CommandArgument = user.ID.ToString();
            chkLocked.Checked = user.Locked;

            // uneditable data            
            //txtEmail.Enabled = false;
            txtUsername.Enabled = false;

            ddlRole.SelectedValue = user.Role.ID.ToString();

            // disable role if editing own details
            ddlRole.Enabled = (user.ID != LoggedInUser.ID);

            // hide entity if logged in user belongs to entity or role doesn't require entity
            UserRole r = DAC_UserRole.GetByID(user.Role.ID, user.ClientID);
            pnlEntityArea.Visible = !(LoggedInUser.EntityID > 0 || (r != null && !r.EntityRequired));


            if(user.DepartmentID > 0)
                ddlDepartment.SelectedValue = user.DepartmentID.ToString();
            ddlEntity.SelectedValue = user.EntityID.ToString();

            if (LoggedInUser.Role.ID == 1 && UserID != LoggedInUser.ID ) // super admin
            {
                ddlClient.DataSource = DAC_Client.Get();
                ddlClient.DataTextField = "Name";
                ddlClient.DataValueField = "ID";
                ddlClient.DataBind();
                pnlClient.Visible = true;
                ddlClient.Items.Insert(0, new ListItem("All Clients", "0"));        

                ddlClient.SelectedValue = user.ClientID.ToString();
            }
            else
                pnlClient.Visible = false;

            return true;
        }

        private void ClearForm()
        {
            InitControlsAdd();

            // hide entity if logged in user belongs to entity or role doesn't require entity
            UserRole r = DAC_UserRole.GetByID(ddlRole.SelectedValue.ToInt32(), LoggedInUser.ClientID);
            pnlEntityArea.Visible = !(LoggedInUser.EntityID > 0 || (r != null && !r.EntityRequired));

            txtEmail.ClearText();
            txtFirstname.ClearText();
            txtSurname.ClearText();
            txtUsername.ClearText();
            btnSave.CommandArgument = "-1";
        }

        private void SaveUser(DbConnection connection, DbTransaction transaction)
        {
            int ID = btnSave.CommandArgument.ToInt32();

            #region capture required data

            CMSUser user = new CMSUser();

            user.ID = ID;
            user.Name = txtFirstname.Text.Trim();
            user.Surname = txtSurname.Text.Trim();
            user.Email = txtEmail.Text.Trim();
            user.Username = txtUsername.Text.Trim();
            user.Role = new UserRole { ID = ddlRole.SelectedValue.ToInt32() };
            user.EntityID = ddlEntity.SelectedValue.ToInt32();
            user.DepartmentID = ddlDepartment.SelectedValue.ToInt32();
            user.Locked = chkLocked.Checked;

            // validate email
            if (DAC_CMSUser.UserEmailExists(user.Email, LoggedInUser.ID == user.ID ? LoggedInUser.ID : user.ID))
            {
                this.Page.InsertError("Email is already taken", "CMSUserSave");
                return;
            }

            string randomPass = String.Empty;
            if (ID == -1)
            {
                // if new user use client id from Web.Config
                if (LoggedInUser.Role.ID == 1)
                    user.ClientID = ddlClient.SelectedValue.ToInt32();
                else
                    user.ClientID = FluentContent.Settings.ClientID;

                // validate new user data
                if (DAC_CMSUser.UserUsernameExists(connection, transaction, user.Username))
                {
                    this.Page.InsertError("Username already exists", "CMSUserSave");
                    return;
                }

                if (DAC_CMSUser.UserEmailExists(connection, transaction, user.Email, 0))
                {
                    this.Page.InsertError("Email already exists", "CMSUserSave");
                    return;
                }

                // new user
                string salt = String.Empty;
                randomPass = Password.CreateRandomPassword(8);
                user.Password = Password.CreatePasswordHash(randomPass, out salt);
                user.Salt = salt;
                user.CreatedBy = LoggedInUser != null ? LoggedInUser.Username : "SYSTEM";
            }
            else
            {
                user.ModifiedBy = LoggedInUser != null ? LoggedInUser.Username : "SYSTEM";
                if (LoggedInUser.Role.ID == 1)
                    user.ClientID = ddlClient.SelectedValue.ToInt32();
                else
                    user.ClientID = LoggedInUser.ClientID;                
            }

            #endregion

            #region save data

            int result = user.Save(connection, transaction);
            if (result > 0)
            {
                if (user.ID == -1)
                {
                    user.ID = result; // set user id from result

                    // if custom registration template exists send it
                    DataTable dtRegTemplate = DAC_Item.GetItemByTitle("Email templates", "Registration");
                    if (dtRegTemplate != null && dtRegTemplate.Rows.Count > 0)
                    {                        
                        string recipients = String.Format("{0};{1}", user.Email, dtRegTemplate.GetDataTableValue(0, "chrText1", String.Empty));
                        string from = dtRegTemplate.GetDataTableValue(0, "chrText2", String.Empty);                        
                        string displayName = dtRegTemplate.GetDataTableValue(0, "chrText3", String.Empty);
                        string cc = dtRegTemplate.GetDataTableValue(0, "chrText4", String.Empty);
                        string bcc = dtRegTemplate.GetDataTableValue(0, "chrText5", String.Empty);
                        string body = dtRegTemplate.GetDataTableValue(0, "chrText11", String.Empty);
                        string subject = dtRegTemplate.GetDataTableValue(0, "chrText6", String.Empty);
                        string password = dtRegTemplate.GetDataTableValue(0, "chrText7", String.Empty);

                        // set default sender in case it is empty
                        if (from == String.Empty) from = FluentContent.Settings.FailSafeEmail;
                            
                        // replace placeholders
                        body = EmailManager.ReplaceKeys(body, new Dictionary<string, string> 
                        {
                            {"##USERNAME##", user.Username},
                            {"##PASSWORD##", randomPass },
                            {"##FIRSTNAME##", user.Name },
                            {"##SURNAME##", user.Surname }
                        });
                        body = String.Format("<html><body>{0}</body></html>", body);
                        NetworkCredential cred = new NetworkCredential(from, password);
                        EmailManager.SendEmail(recipients, from, displayName, subject, body, true, cc, bcc, null, cred);                        
                    }
                    else
                    {
                        // else send generated password to user using db template
                        EmailTemplate t = DAC_EmailTemplate.Get(connection, transaction, "cms_new_user");

                        string recipients = String.Format("{0};{1}", user.Email, t.To);
                        t.Body = EmailManager.ReplaceKeys(t.Body, new Dictionary<string, string>
                        {
                            {"##USERNAME##", user.Username},
                            {"##PASSWORD##",randomPass }
                        });
                        EmailManager.SendEmail(recipients, t.From, t.DisplayName, t.Subject, t.Body, true, t.Cc, t.Bcc, null, t.Credentials);
                    }
                    ClearForm();
                    // show inserted message
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('User created successfully!','success');", true);

                }
                else
                {
                    if (LoggedInUser.ID == user.ID)
                    {
                        LoggedInUser.Name = user.Name;
                        LoggedInUser.Surname = user.Surname;
                        LoggedInUser.Email = user.Email;
                        LoggedInUser.Username = user.Username;
                        LoggedInUser.Role.ID = user.Role.ID;
                        LoggedInUser.EntityID = user.EntityID;
                        LoggedInUser.DepartmentID = user.DepartmentID;
                    }

                    // show update success message
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Details updated successfully!','success');", true);
                }
                if (OnUserSaved != null) OnUserSaved(user, null);
            }
            else
            {
                this.Page.InsertError("Failed to save user details!", "CMSUserSave");
            }

            #endregion
        }


    }
}