﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCLoginStatus.ascx.cs"
    Inherits="CMS.UserControls.UCLoginStatus" %>

<div class="dropdown profile-element">
    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
        <span class="clear"><span class="block m-t-xs"><strong class="font-bold"><%=UserSession.Name %></strong>
        </span><span class="text-muted text-xs block">Profile<b class="caret"></b></span></span></a>
    <ul class="dropdown-menu animated fadeInRight m-t-xs">
        <li><a href="<%=ResolveUrl("~/MyAccount.aspx") %>">My Account</a></li>
        <li class="divider"></li>
        <li><a href="<%=ResolveUrl("~/Logout.aspx") %>">Logout</a></li>
    </ul>
</div>
<div class="logo-element">
    <%=UserSession.Name %>
</div>
