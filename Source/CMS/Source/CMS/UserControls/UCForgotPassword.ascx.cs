﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using System.Data.Common;
using DataAccess;
using Common.Extensions;
using Common.Security;
using Common.Email.Entities;
using Common.Email.DAC;
using Common.Email;
using System.Data;
using FluentContent.DAC;
using System.Net;
using FluentContent.Entities;

namespace CMS.UserControls
{
    public partial class UCForgotPassword : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnRetrievePassword_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;
            string error = DbFactory.Connect<string>(RetrievePassword);
            if (String.IsNullOrEmpty(error))
            {
                txtUsername.ClearText();
                txtUserEmail.ClearText();
                spnInfo.Visible = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('A new passwordhas been sent to your email address! Proceed to','success');", true);

            }
            else
            {
                spnInfo.Visible = false;
                this.Page.InsertError(error, "ForgotPasswordGroupServer");
            }
        }

        private string RetrievePassword(DbConnection connection, DbTransaction transaction)
        {
            CMSUser u = DAC_CMSUser.Get(txtUsername.Text.Trim());
            if (u == null) return "Username does not exist!";
            if (u.Email.ToLower() != txtUserEmail.Text.Trim().ToLower()) return "Username and email do not match!";

            string salt = String.Empty;
            string randomPass = Password.CreateRandomPassword(8);
            string hash = Password.CreatePasswordHash(randomPass, out salt);

            // save password
            if (!DAC_CMSUser.ChangePassword(connection, transaction, hash, u.ID, salt, "SYSTEM")) return "Failed to generate new password, please try again later!";
           
            //////
            // if custom registration template exists send it
            DataTable dtRegTemplate = DAC_Item.GetItemByTitle("Email templates", "Generate New Password");
            if (dtRegTemplate != null && dtRegTemplate.Rows.Count > 0)
            {
                string recipients = String.Format("{0};{1}", u.Email, dtRegTemplate.GetDataTableValue(0, "chrText1", String.Empty));
                string from = dtRegTemplate.GetDataTableValue(0, "chrText2", String.Empty);
                string displayName = dtRegTemplate.GetDataTableValue(0, "chrText3", String.Empty);
                string cc = dtRegTemplate.GetDataTableValue(0, "chrText4", String.Empty);
                string bcc = dtRegTemplate.GetDataTableValue(0, "chrText5", String.Empty);
                string body = dtRegTemplate.GetDataTableValue(0, "chrText11", String.Empty);
                string subject = dtRegTemplate.GetDataTableValue(0, "chrText6", String.Empty);
                string password = dtRegTemplate.GetDataTableValue(0, "chrText7", String.Empty);

                // set default sender in case it is empty
                if (from == String.Empty) from = FluentContent.Settings.FailSafeEmail;

                // replace placeholders
                body = EmailManager.ReplaceKeys(body, new Dictionary<string, string> 
                        {
                            {"##USERNAME##", u.Username},
                            {"##PASSWORD##", randomPass },
                            {"##FIRSTNAME##", u.Name },
                            {"##SURNAME##", u.Surname }
                        });
                body = String.Format("<html><body>{0}</body></html>", body);
                NetworkCredential cred = new NetworkCredential(from, password);
                EmailManager.SendEmail(recipients, from, displayName, subject, body, true, cc, bcc, null, cred);
            }
            else
            {
                // else send generated password to user using db template
                EmailTemplate t = DAC_EmailTemplate.Get(connection, transaction, "cms_generate_new_password");

                // send email
                string recipients = String.Format("{0};{1}", u.Email, t.To);
                t.Body = EmailManager.ReplaceKeys(t.Body, new Dictionary<string, string>
                {                        
                    {"##PASSWORD##",randomPass }
                });
                if(!EmailManager.SendEmail(recipients, t.From, t.DisplayName, t.Subject, t.Body, true, t.Cc, t.Bcc, null, t.Credentials))
                {
                    return "Password changed but failed to send email!";                    
                }
            }
            ///////
            
            return null;
        }
    }
}