﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.Helpers;

using System.Data.Common;
using Common.Security;
using DataAccess;
using Common.Extensions;
using Common.Session;
using FluentContent.Entities;
using FluentContent.DAC;

namespace CMS.UserControls
{
    public partial class UCChangePassword : System.Web.UI.UserControl
    {
        public CMSUser UserSession { get { return SessionObjects.GetSession<CMSUser>(SessionKeys.CMS_USER_SESSION); } }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnChangePassword_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;
            
            // update password
            if (DbFactory.Connect<bool>(ChangePassword))
            {
                txtConfirmPassword.ClearText();
                txtPassword.ClearText();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Password updatedsuccessfully!','success');", true);

            }
            else
            {
                //this.Page.InsertError("Failed to update your password!", "ChangePasswordServer");
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Failed to update your password!','error');", true);
            }
        }

        private bool ChangePassword(DbConnection connection, DbTransaction transaction)
        {
            if (UserSession == null) return false;

            string salt = String.Empty;
            string hash = Password.CreatePasswordHash(txtPassword.Text.Trim(), out salt);

            return DAC_CMSUser.ChangePassword(connection, transaction, hash, UserSession.ID, salt, UserSession.Username);
        }
    }
}