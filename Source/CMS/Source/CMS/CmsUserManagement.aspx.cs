﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.Helpers.Entities;
using CMS.Helpers.Interfaces;
using Common.Extensions;
using CMS.Helpers.Bases;
using Common.Security;
using Common.Email.Entities;
using System.Data.Common;
using Common.Email.DAC;
using DataAccess;
using Common.Email;
using FluentContent.DAC;
using FluentContent.Entities.Helpers;
using CMS.Helpers;
using FluentContent.Entities;
using System.Data;
using System.Net;
using FluentContent.Entities.Enums;

namespace CMS
{
    public partial class CmsUserManagement : BasePage
    {
        public string Action { get { return Request.GetQueryString<string>("action", null); } }
        public int UserID { get { return Request.GetQueryString<int>("id", -1); } }

        public int DeptFilter { get { return Request.GetQueryString<int>("dept", 0); } }
        public int EntityFilter { get { return Request.GetQueryString<int>("entity", 0); } }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            UCAddEditUser.OnUserSaved += new CMS.UserControls.UCAddEditCMSUser.OnUserSavedHandler(UCAddEditUser_OnUserSaved);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // check user rights
            if (UserSession.Role.CMSAccessLevel != CMSAccessLevel.Full) Response.Redirect("~/Home.aspx");

            if (!IsPostBack)
            {
                LoadSubMenu();
                LoadViews();
            }
        }

        protected void lvUsers_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            int userID = e.CommandArgument.ToString().ToInt32();

            switch (e.CommandName)
            {
                case "EditUser":
                    LoadEditView(userID);
                    break;
                case "GeneratePass":
                    DbFactory.Connect<bool, int>(GenerateNewPassword, userID);
                    break;
                case "RemoveUser":
                    DeleteUser(userID);
                    break;
                default:
                    break;
            }
        }

        protected void btnConfirmFilter_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/CmsUserManagement.aspx?entity=" + ddlFilterEntities.SelectedValue + "&dept=" + ddlFilterDepts.SelectedValue);
        }

        protected void ddlFilterEntitites_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<UserDepartment> departments = DAC_UserDepartment.GetByEntityID(ddlFilterEntities.SelectedValue.ToInt32());
            // load only departments linked to client if not super admin
            if (UserSession.ClientID != 0)
                departments = departments.FindAll(i => i.ClientID == UserSession.ClientID);

            ddlFilterDepts.DataBindWithDataSource(departments, "Name", "ID");
            ddlFilterDepts.SetDefaultValue("All departments", "0", true);
            ddlFilterDepts.ClearSelection();
        }

        void UCAddEditUser_OnUserSaved(CMSUser user, EventArgs e)
        {
            List<UserItem> userItems = new List<UserItem>();
            foreach (ListViewDataItem item in lvUserItems.Items)
            {

                CheckBox chkUserItemChecked = item.FindControl("chkUserItemChecked") as CheckBox;

                if (chkUserItemChecked.Checked)
                {
                    HiddenField hdnItemID = item.FindControl("hdnItemID") as HiddenField;
                    userItems.Add(new UserItem { ItemID = hdnItemID.Value.ToInt32(), UserID = user.ID });
                }
            }

            DAC_CMSUser.InsertUserItems(userItems, user.ID);

        }

        /* Custom logic*/

        private void DeleteUser(int userID)
        {
            if (DAC_CMSUser.Delete(userID))
            {
                //    spnInfoUserDeleted.Visible = true;
                LoadUserList();
            }
            else
            {
                this.InsertError("Failed to delete user!", "UserManagementServer");
            }
        }

        private bool GenerateNewPassword(DbConnection connection, DbTransaction transaction, int userID)
        {
            string salt = String.Empty;
            string randomPass = Password.CreateRandomPassword(8);
            string hash = Password.CreatePasswordHash(randomPass, out salt);

            CMSUser u = DAC_CMSUser.Get(connection, transaction, userID);

            // save password
            if (!DAC_CMSUser.ChangePassword(connection, transaction, hash, u.ID, salt, "SYSTEM"))
            {
                this.InsertError("Failed to change password!", "UserManagementServer");
                return false;
            }
            //////
            // if custom registration template exists send it
            DataTable dtRegTemplate = DAC_Item.GetItemByTitle("Email templates", "Generate New Password");
            if (dtRegTemplate != null && dtRegTemplate.Rows.Count > 0)
            {
                string recipients = String.Format("{0};{1}", u.Email, dtRegTemplate.GetDataTableValue(0, "chrText1", String.Empty));
                string from = dtRegTemplate.GetDataTableValue(0, "chrText2", String.Empty);
                string displayName = dtRegTemplate.GetDataTableValue(0, "chrText3", String.Empty);
                string cc = dtRegTemplate.GetDataTableValue(0, "chrText4", String.Empty);
                string bcc = dtRegTemplate.GetDataTableValue(0, "chrText5", String.Empty);
                string body = dtRegTemplate.GetDataTableValue(0, "chrText11", String.Empty);
                string subject = dtRegTemplate.GetDataTableValue(0, "chrText6", String.Empty);
                string password = dtRegTemplate.GetDataTableValue(0, "chrText7", String.Empty);

                // set default sender in case it is empty
                if (from == String.Empty) from = FluentContent.Settings.FailSafeEmail;

                // replace placeholders
                body = EmailManager.ReplaceKeys(body, new Dictionary<string, string> 
                        {
                            {"##USERNAME##", u.Username},
                            {"##PASSWORD##", randomPass },
                            {"##FIRSTNAME##", u.Name },
                            {"##SURNAME##", u.Surname }
                        });
                body = String.Format("<html><body>{0}</body></html>", body);
                NetworkCredential cred = new NetworkCredential(from, password);
                EmailManager.SendEmail(recipients, from, displayName, subject, body, true, cc, bcc, null, cred);
            }
            else
            {
                // else send generated password to user using db template
                EmailTemplate t = DAC_EmailTemplate.Get(connection, transaction, "cms_generate_new_password");

                // send email
                string recipients = String.Format("{0};{1}", u.Email, t.To);
                t.Body = EmailManager.ReplaceKeys(t.Body, new Dictionary<string, string>
                {                        
                    {"##PASSWORD##",randomPass }
                });
                if (!EmailManager.SendEmail(recipients, t.From, t.DisplayName, t.Subject, t.Body, true, t.Cc, t.Bcc, null, t.Credentials))
                {
                    this.InsertError("Password changed but failed to send email!", "UserManagementServer");
                    return false;
                }
            }
            ///////

            // show success
            //  spnInfoPasswordGenerated.Visible = true;
            return true;
        }

        private void LoadViews()
        {
            switch (Action)
            {
                case "add":
                    LoadAddView();
                    break;
                default:
                    LoadUserList();
                    break;
            }
        }

        private void LoadAddView()
        {
            UCAddEditUser.UserID = -1;
            mvUsers.SetActiveView(viewAddEditUser);
        }

        private void LoadEditView(int userID)
        {
            if (userID < 1)
            {
                LoadUserList();
                return;
            }

            UCAddEditUser.UserID = userID;
            UCAddEditUser.LoadUserDetails();
            mvUsers.SetActiveView(viewAddEditUser);

            // load user items
            List<UserItem> items = DAC_CMSUser.GetUserItemAttachment(userID);
            lvUserItems.DataSource = items;
            lvUserItems.DataBind();

        }

        private void LoadUserList()
        {
            // filters

            ddlFilterEntities.DataBindWithDataSource(DAC_Entity.Get(UserSession.ClientID), "Name", "ID");
            ddlFilterEntities.SetDefaultValue("All entities", "0", true);

            // if user has entity then select user entity and load departments for entity
            if (UserSession.EntityID != 0)
            {
                ddlFilterEntities.SelectedValue = UserSession.EntityID.ToString();

                List<UserDepartment> departments = DAC_UserDepartment.GetByEntityID(UserSession.EntityID);

                // load only departments linked to client if not super admin
                if (UserSession.ClientID != 0)
                    departments = departments.FindAll(i => i.ClientID == UserSession.ClientID);

                ddlFilterDepts.DataBindWithDataSource(departments, "Name", "ID");
                ddlFilterEntities.Enabled = false;
            }
            // if user has no entity then set entity from filter and load departments for entity filter
            else
            {
                ddlFilterEntities.SelectedValue = EntityFilter.ToString();
                List<UserDepartment> departments = DAC_UserDepartment.GetByEntityID(EntityFilter);

                // load only departments linked to client if not super admin
                if (UserSession.ClientID != 0 && UserSession.EntityID != 0)
                    departments = departments.FindAll(i => i.ClientID == UserSession.ClientID);

                ddlFilterDepts.DataBindWithDataSource(departments, "Name", "ID");
                ddlFilterEntities.Enabled = true;
            }

            ddlFilterDepts.SetDefaultValue("All departments", "0", true);
            ddlFilterDepts.SelectedValue = DeptFilter.ToString();


            CMSUser u = new CMSUser
            {
                DepartmentID = UserSession.DepartmentID,
                EntityID = UserSession.EntityID,
                Role = new UserRole { RoleHierarchy = UserSession.Role.RoleHierarchy }
            };


            if (UserSession.Role.CMSAccessLevel == CMSAccessLevel.Full)
                u.EntityID = EntityFilter;

            u.DepartmentID = DeptFilter;

            List<CMSUser> users = DAC_CMSUser.GetAll(u);
            if (UserSession.ClientID != 0 && UserSession.EntityID != 0)
                users = users.FindAll(i => i.ClientID == FluentContent.Settings.ClientID);
            else
                users = users.FindAll(i => i.ClientID == 0 || i.ClientID == FluentContent.Settings.ClientID);

            lvUsers.DataSource = users;
            lvUsers.DataBind();
                count.Text = users.Count.ToString();
            mvUsers.SetActiveView(viewUserList);
        }

        private void LoadSubMenu()
        {
            ICMSMaster master = this.Master as ICMSMaster;
            if (master == null) return;

            List<SubMenuItem> subMenu = new List<SubMenuItem>();

            subMenu.Add(new SubMenuItem(ResolveUrl("~/CmsUserManagement.aspx?action=add"), "Add User"));
            subMenu.Add(new SubMenuItem(ResolveUrl("~/CmsUserManagement.aspx"), "List Users"));

            master.SetSubMenuDataSource(subMenu, "CMS Users");
        }
    }
}
