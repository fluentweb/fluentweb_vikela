﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Extensions;
using FluentContent.DAC;
using FLContentSiteAssist.BusinessLogic;
using CMS.Helpers.Bases;
using FluentContent.Entities.Enums;
using System.Data;
using FluentContent.Entities;
using System.Collections.Specialized;

namespace CMS
{
    public partial class TMClassificationClassProductManagement : BasePage
    {
        public NameValueCollection collection;
        public NameValueCollection collectionbreadCrompClass;

        public int ClassificationClassProductID { get { return Request.GetQueryString<int>("classificationclassproductid", -1); } }
        public int ClassificationClassID { get { return Request.GetQueryString<int>("classificationclassid", -1); } }
        public string ClassificationClassName { get { return Request.GetQueryString<string>("classificationclassname", string.Empty); } }
        public int ClassificationID { get { return Request.GetQueryString<int>("classificationid", -1); } }
        public string ClassificationName { get { return Request.GetQueryString<string>("classificationname", string.Empty); } }
        public string Action { get { return Request.GetQueryString<string>("action"); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            collection = HttpUtility.ParseQueryString(Request.Url.Query);
            collection.Remove("classificationclassproductid");
            collection.Remove("action");

            collectionbreadCrompClass = HttpUtility.ParseQueryString(Request.Url.Query);
            collectionbreadCrompClass.Remove("classificationclassproductid");
            collectionbreadCrompClass.Remove("classificationclassid");
            collectionbreadCrompClass.Remove("classificationclassname");
            collectionbreadCrompClass.Remove("action");

            // check user rights
            if (UserSession.Role.CMSAccessLevel != CMSAccessLevel.Full) Response.Redirect("~/Home.aspx");

            if (!IsPostBack)
            {
                if (ClassificationClassProductID != -1 || Action == "create")
                {
                    LoadClassificationClassProductDetail();
                    if (Action == "create")
                        litBreadCromp.Text = "<b>Create</b>";
                    else
                        litBreadCromp.Text = "<b>Edit</b>";
                }
                else
                    LoadClassificationClassProductListing();
            }
        }
        private void LoadClassificationClassProductListing()
        {
            count.Text = "0";
            DataTable dtTMClassificationClassProduct = DAC_TMClassificationClassProduct.GetClassificationClassProductByClass(ClassificationClassID);
            if (dtTMClassificationClassProduct.Rows.Count > 0)
                count.Text = dtTMClassificationClassProduct.Rows.Count.ToString();
            lvTMClassificationClassProduct.DataSource = dtTMClassificationClassProduct;
            lvTMClassificationClassProduct.DataBind();
        }
        private void LoadClassificationClassProductDetail()
        {
            mvPages.SetActiveView(viewDetail);
        }

        protected void btnCreateTop_Click(object sender, EventArgs e)
        {
            Response.Redirect(ResolveUrl("~/TMClassificationClassProductManagement.aspx?action=create&"+ collection.ToQueryString(false)));
        }

        protected void btndelete_Click(object sender, EventArgs e)
        {
            int productId = ((LinkButton)sender).CommandArgument.ToInt32();
            bool result = false;
            if (productId > 0)
                result = DAC_TMClassificationClassProduct.Delete(new TMClassificationClassProduct { TMClassificationClassProductID = productId });

            if (result)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Delete success','success');", true);
                Response.Redirect(Request.RawUrl);
            }
            else
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Delete failed!','warning');", true);
        }
    }
}