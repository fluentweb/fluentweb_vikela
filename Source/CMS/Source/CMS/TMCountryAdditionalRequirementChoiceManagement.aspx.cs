﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Extensions;
using FluentContent.DAC;
using FLContentSiteAssist.BusinessLogic;
using CMS.Helpers.Bases;
using FluentContent.Entities.Enums;
using System.Data;
using FluentContent.Entities;

namespace CMS
{
    public partial class TMCountryAdditionalRequirementChoiceManagement : BasePage
    {
        public int CountryAdditionalReqId { get { return Request.GetQueryString<int>("countryadditionalrequirementid", -1); } }
        public int CountryAdditionalReqCId { get { return Request.GetQueryString<int>("countryadditionalrequirementchoiceid", -1); } }
        public string Action { get { return Request.GetQueryString<string>("action"); } }
      
        protected void Page_Load(object sender, EventArgs e)
        {
            // check user rights
            if (UserSession.Role.CMSAccessLevel != CMSAccessLevel.Full) Response.Redirect("~/Home.aspx");

            if (!IsPostBack)
            {
                if (CountryAdditionalReqCId != -1 || Action == "create")
                {
                    LoadCountryDetail();
                    if (Action == "create")
                        litBreadCromp.Text = "<b>Create</b>";
                    else
                        litBreadCromp.Text = "<b>Edit</b>";
                }
                else
                    LoadCountryListing();
            }
        }
        private void LoadCountryListing()
        {
            count.Text = "0";
            if (CountryAdditionalReqId > 0)
            {
                DataTable dtTMCountryAdditionalRequirementChoice = DAC_TMCountryAdditionalRequirementChoice.GetCountryAdditionalRequirementChoice(CountryAdditionalReqId);
                if (dtTMCountryAdditionalRequirementChoice.Rows.Count > 0)
                    count.Text = dtTMCountryAdditionalRequirementChoice.Rows.Count.ToString();
                lvTMCountryAdditionalRequirementChoice.DataSource = dtTMCountryAdditionalRequirementChoice;
                lvTMCountryAdditionalRequirementChoice.DataBind();
            }
            else
                Response.Redirect(Request.UrlReferrer.ToString());
        }
        private void LoadCountryDetail()
        {
            mvPages.SetActiveView(viewDetail);
        }

        protected void btnCreateTop_Click(object sender, EventArgs e)
        {
            if (CountryAdditionalReqId > 0)
                Response.Redirect(ResolveUrl(String.Format("~/TMCountryAdditionalRequirementChoiceManagement.aspx?countryadditionalrequirementchoiceid={0}&countryadditionalrequirementid={1}&action=create", CountryAdditionalReqCId, CountryAdditionalReqId)));
            else
                return;
        }

        protected void btndelete_Click(object sender, EventArgs e)
        {
            int countryAdditionalReqCId = ((LinkButton)sender).CommandArgument.ToInt32();
            bool result = false;
            if (CountryAdditionalReqId > 0)
                result = DAC_TMCountryAdditionalRequirementChoice.Delete(new TMCountryAdditionalRequirementChoice { CountryAdditionalRequirementChoiceID = countryAdditionalReqCId });

            if (result)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Delete success','success');", true);
                Response.Redirect(Request.RawUrl);
            }
            else
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Delete failed!','warning');", true);
        }
    }
}