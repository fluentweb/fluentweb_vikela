﻿using FluentContent.DAC;
using FluentContent.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Extensions;
using CMS.Helpers;

namespace CMS
{
    public partial class PropertyValuationManagemet : System.Web.UI.Page
    {
        public bool NewValuation { get { return Request.GetQueryString<int>("new-valuation", 0) > 0; } }
        public int ValuationID { get { return Request.GetQueryString<int>("valuation-id", 0); } }
        public string Message { get { return Request.GetQueryString<string>("info"); } }
        public int DeleteValuationID { get { return Request.GetQueryString<int>("delete", 0); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (DeleteValuationID > 0)
                {
                    if(DAC_PropertyValuation.Delete(DeleteValuationID))
                    {
                        Response.Redirect("~/property-valuations?info=deleted");
                    }
                }

                if (ValuationID > 0)
                    LoadValuationEditView();
                else
                {
                    if (!NewValuation && ValuationID == 0)
                        LoadValuationList();
                    if (NewValuation)
                        LoadNewValuationView();
                }
            }
        }

        private void LoadValuationEditView()
        {
            InitControls();
            PropertyValuation valuation = DAC_PropertyValuation.Get(ValuationID);

            if (valuation == null) Response.Redirect("~/property-valuations?info=novaluation");


            ddlTitle.SelectedValue = valuation.Title.ID == 0 ? String.Empty : valuation.Title.ID.ToString();
            ddlCountry.SelectedValue = valuation.Country.ID == 0 ? String.Empty : valuation.Country.ID.ToString();
            ddlCountry_SelectedIndexChanged(this, null);
            ddlCounty.SelectedValue = valuation.County.ID == 0 ? String.Empty : valuation.County.ID.ToString();
            ddlBathrooms.SelectedValue = valuation.Bathrooms == 0 ? String.Empty : valuation.Bathrooms.ToString();
            ddlBedrooms.SelectedValue = valuation.Bedrooms == 0 ? String.Empty : valuation.Bedrooms.ToString();
            ddlPropertyValuatonType.SelectedValue = valuation.PropertyTypeItemID == 0 ? String.Empty : valuation.PropertyTypeItemID.ToString();
            

            txtAdditionalInformation.Value = valuation.AdditionalInformation;
            txtAddress1.Value = valuation.Address1;
            txtAddress2.Value = valuation.Address2;
            txtAddress3.Value = valuation.Address3;
            txtEmail.Value = valuation.EmailAddress;
            txtFirstName.Value = valuation.FirstName;
            txtPhoneHome.Value = valuation.PhoneHome;
            txtPhoneMobile.Value = valuation.PhoneMobile;
            txtPhoneWork.Value = valuation.PhoneWork;
            txtPostcode.Value = valuation.AddressPostcode;
            txtSurname.Value = valuation.Surname;
            txtNotes.Value = valuation.Notes;
            txtValue.Value = valuation.PropertyValue.ToString();
            
            chkInterestedInSelling.Checked = valuation.WantsToSell;
            chkInterestedInRenting.Checked = valuation.WantsToRent;
        }

        private void LoadNewValuationView()
        {
            InitControls();
        }

        private void InitControls()
        {
            mvValuations.SetActiveView(viewAddEditValuation);

            ddlTitle.DataSource = DAC_Title.Get();
            ddlTitle.DataTextField = "Description";
            ddlTitle.DataValueField = "ID";
            ddlTitle.DataBind();

            ddlTitle.Items.Insert(0, new ListItem("Select Title", String.Empty));

            ddlCountry.DataSource = DAC_Country.Get();
            ddlCountry.DataTextField = "Name";
            ddlCountry.DataValueField = "ID";
            ddlCountry.DataBind();

            ddlCountry.SelectedValue = ddlCountry.Items.FindByText("Jersey").Value;

            ddlCounty.DataSource = DAC_County.GetCountiesByCountryID(Convert.ToInt32(ddlCountry.SelectedValue));
            ddlCounty.DataTextField = "CountyName";
            ddlCounty.DataValueField = "ID";
            ddlCounty.DataBind();

            ddlCounty.Items.Insert(0, new ListItem("Select County", String.Empty));

            ddlPropertyValuatonType.DataSource = ItemHelper.LoadItemDisplay("Property Valuation Type");
            ddlPropertyValuatonType.DataTextField = "chrItemTitle";
            ddlPropertyValuatonType.DataValueField = "intItemID";
            ddlPropertyValuatonType.DataBind();

            ddlPropertyValuatonType.Items.Insert(0, new ListItem("Valuation Type", String.Empty));

            ddlBedrooms.DataSource = ItemHelper.LoadItemDisplay("Bedrooms");
            ddlBedrooms.DataTextField = "chrItemTitle";
            ddlBedrooms.DataValueField = "intValue1";
            ddlBedrooms.DataBind();
            ddlBedrooms.Items.Insert(0, new ListItem("Bedrooms", String.Empty));

            ddlBathrooms.DataSource = ItemHelper.LoadItemDisplay("Bathrooms");
            ddlBathrooms.DataTextField = "chrItemTitle";
            ddlBathrooms.DataValueField = "intValue1";
            ddlBathrooms.DataBind();
            ddlBathrooms.Items.Insert(0, new ListItem("Bathrooms", String.Empty));

        }

        private void LoadValuationList()
        {
            lvValuations.DataSource = DAC_PropertyValuation.GetAll();
            lvValuations.DataBind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string email = txtEmail.Value.Trim();
           

           
            string additionalInformation = txtAdditionalInformation.Value.Trim();
            string address1 = txtAddress1.Value.Trim();
            string address2 = txtAddress2.Value.Trim();
            string address3 = txtAddress3.Value.Trim();
            string postcode = txtPostcode.Value.Trim();

            int titleID = String.IsNullOrEmpty(ddlTitle.SelectedValue) ? 0 : ddlTitle.SelectedValue.ToInt32();
            string firstName = txtFirstName.Value.Trim();
            string surname = txtSurname.Value.Trim();
            int countryID = String.IsNullOrEmpty(ddlCountry.SelectedValue) ? 0 : ddlCountry.SelectedValue.ToInt32();
            int countyID = String.IsNullOrEmpty(ddlCounty.SelectedValue) ? 0 : ddlCounty.SelectedValue.ToInt32();
            string mobile = txtPhoneMobile.Value.Trim();
            string homePhone = txtPhoneHome.Value.Trim();
            string workPhone = txtPhoneWork.Value.Trim();

            bool interestedInRenting = chkInterestedInRenting.Checked;
            bool interestedInSelling = chkInterestedInSelling.Checked;

            int propertyValuationTypeID = String.IsNullOrEmpty(ddlPropertyValuatonType.SelectedValue) ? 0 : ddlPropertyValuatonType.SelectedValue.ToInt32();
            int bedrooms = ddlBedrooms.SelectedValue == String.Empty ? 0 : ddlBedrooms.SelectedValue.ToInt32();
            int bathrooms = ddlBathrooms.SelectedValue == String.Empty ? 0 : ddlBathrooms.SelectedValue.ToInt32();
            string notes = txtNotes.Value.Trim();
            decimal propertyValue = txtValue.Value.Trim() == String.Empty ? 0 : Convert.ToDecimal(txtValue.Value.Trim());

            PropertyValuation valuation = new PropertyValuation
            {
                Address1 = address1,
                Address2 = address2,
                Address3 = address3,
                AddressPostcode = postcode,
                Country = new Country { ID = countryID },
                County = new County { ID = countyID },
                EmailAddress = email,
                FirstName = firstName,
                PhoneHome = homePhone,
                PhoneMobile = mobile,
                PhoneWork = workPhone,
                PropertyTypeItemID = propertyValuationTypeID,
                Surname = surname,
                Title = new Title { ID = titleID },
                WantsToRent = interestedInRenting,
                AdditionalInformation = additionalInformation,
                Bathrooms = bathrooms,
                Bedrooms = bedrooms,
                Notes = notes,
                PropertyValue = propertyValue,
                WantsToSell = interestedInSelling
            };

            try
            {
                if (ValuationID == 0)
                {                   
                    valuation = DAC_PropertyValuation.Insert(valuation);
                }
                else
                {
                    var existingValuation = DAC_PropertyValuation.Get(ValuationID);

                    valuation.PropertyValuationID = ValuationID;
                    valuation.PropertySubscribedUserID = existingValuation.PropertySubscribedUserID;
                    valuation = DAC_PropertyValuation.Update(valuation);
                }

                if (valuation == null) { this.InsertError("Failed to submit form", "PropertyValuationGroupServer"); return; }

                Response.Redirect("~/property-valuations?info=" + (ValuationID > 0 ? "updated" : "added"));

            }
            catch (Exception ex)
            {
                this.InsertError(ex.Message, "PropertyRegisteredUserGroupServer");
            }

        }



        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            int countryID = String.IsNullOrEmpty(ddlCountry.SelectedValue) ? 0 : ddlCountry.SelectedValue.ToInt32();

            if (countryID > 0)
            {
                List<County> items = DAC_County.GetCountiesByCountryID(countryID);
                ddlCounty.DataSource = items;
                ddlCounty.DataTextField = "CountyName";
                ddlCounty.DataValueField = "ID";
                ddlCounty.DataBind();

                ddlCounty.Items.Insert(0, new ListItem("Select County", String.Empty));
            }
        }
    }
}