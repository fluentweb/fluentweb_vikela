﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Extensions;
using CMS.Helpers;
using CMS.Helpers.Bases;
using FluentContent.Entities.Enums;


namespace CMS
{
    public partial class RelatedItemManagement : BasePage
    {        
        public int ParentItemTypeID { get { return Request.GetQueryString<int>("itemtype", 0); } }
        public int ParentID { get { return Request.GetQueryString<int>("itemid"); } }
        public string RelationshipType { get { return Request.GetQueryString<string>("relationtype", String.Empty); } }
        
        public int ChildItemTypeID { get { return Request.GetQueryString<int>("childitemtype"); } }
        public bool IsChildCreateType { get { return Request.GetQueryString<bool>("childcreate", false); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            // check user rights
            if (UserSession.Role.CMSAccessLevel != CMSAccessLevel.Full) Response.Redirect("~/Home.aspx");

            if (!IsPostBack)
            {
                LoadViews();
                LoadBreadCrumbs();
            }
        }

        private void LoadBreadCrumbs()
        {
            Dictionary<string, string> b = new Dictionary<string, string>();

            if (RelationshipType != String.Empty)
            {
                b.Add(ItemHelper.GetItemTypeName(ParentItemTypeID) + " list", ResolveUrl("~/ItemManagement.aspx?itemtype=" + ParentItemTypeID));
                b.Add(ItemHelper.GetItemNameByID(ParentID), ResolveUrl("~/ItemManagement.aspx?itemtype=" + ParentItemTypeID + "&itemid=" + ParentID));
                b.Add(ItemHelper.GetItemTypeName(ChildItemTypeID) + " list", "#");
            }
            
            lvBreadcrumbs.DataSource = b;
            lvBreadcrumbs.DataBind();
        }
        private void LoadViews()
        {
            if (IsChildCreateType)
                mvRelatedItems.SetActiveView(viewCreate);
            else
                mvRelatedItems.SetActiveView(viewRelate);
        }
    }
}
