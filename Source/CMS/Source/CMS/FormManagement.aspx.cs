﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Extensions;
using FluentContent.DAC;
using FluentContent.Entities;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;

namespace CMS
{
    public partial class FormManagement : System.Web.UI.Page
    {
        public int TypeID { get { return Request.GetQueryString<int>("typeid", 0); } }
        public int FormID { get { return Request.GetQueryString<int>("formid", 0); } }
        public string Action { get { return Request.GetQueryString<string>("action", null); } }

        public string FormString { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (FormID == 0)
                {
                    if (Action == "create")
                    {
                        mvItems.SetActiveView(viewDetail);
                        List<FormObjectType> formObjectTypes = DAC_FormObjectType.GetFormObjectTypesByFormTypeID(TypeID);
                        lvFormControls.DataSource = formObjectTypes;
                        lvFormControls.DataBind();
                    }
                    else
                    {
                        mvItems.SetActiveView(viewListing);
                        LoadItems(TypeID);
                    }
                }
                else
                {
                    if (FormID != 0)
                    {
                        if (Action == "duplicate")
                        {
                            // duplicate current form
                            DAC_Form.CloneForm(FormID);
                            Response.Redirect(String.Format("~/FormManagement.aspx?typeid={0}", TypeID));
                        }
                    }
                    mvItems.SetActiveView(viewDetail);
                    LoadDetail(FormID);
                }
            }
        }

        public void LoadItems(int typeID)
        {
            List<Form> items = DAC_Form.GetFormsByTypeID(typeID);
            lvItems.DataSource = items;
            lvItems.DataBind();
        }

        public void LoadDetail(int formID)
        {
            Form item = DAC_Form.GetFormByID(formID);
            txtFormTitle.Text = item.Name;
            txtFormDescription.Text = item.Description;

            List<FormObjectType> formObjectTypes = DAC_FormObjectType.GetFormObjectTypesByFormTypeID(item.Type.ID);
            lvFormControls.DataSource = formObjectTypes;
            lvFormControls.DataBind();

            FormString = DAC_FormObject.GetFullTemplateWithDataByFormID(formID);
        }

        public void btnSave_Click(object sender, EventArgs e)
        {
            if (Action == "create")
            {
                int formID = DAC_Form.Insert(new Form
                {
                    Description = txtFormDescription.Text.Trim(),
                    Name = txtFormTitle.Text.Trim(),
                    Type = new FormType { ID = TypeID }
                });

                if (formID > 0)
                {
                    SaveFormData(formID);
                    Response.Redirect(ResolveUrl(String.Format("~/FormManagement.aspx?typeid={0}", TypeID)));
                }
                else
                {
                    this.InsertError("Failed to update form", "FormGroupServer");
                }
            }
            else
            {
                // save form object
                if (DAC_Form.Update(new Form
                {
                    Description = txtFormDescription.Text.Trim(),
                    ID = FormID,
                    Name = txtFormTitle.Text.Trim(),
                    Type = new FormType { ID = TypeID }
                }))
                {
                    SaveFormData(FormID);
                    Response.Redirect(ResolveUrl(String.Format("~/FormManagement.aspx?typeid={0}", TypeID)));
                }
                else
                {
                    this.InsertError("Failed to update form", "FormGroupServer");
                }
            }
        }

        private void SaveFormData(int formID)
        {
            string json = hdnFormData.Value;
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(List<FormObject>));
            MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(json));
            List<FormObject> result = serializer.ReadObject(ms) as List<FormObject>;

            var itemsToDelete = hdnDeleteItems.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            int[] idsToDelete = Array.ConvertAll(itemsToDelete, i => int.Parse(i));
            DAC_FormObject.SaveForm(result, idsToDelete.ToList(), formID);

            // reload form data
            FormString = DAC_FormObject.GetFullTemplateWithDataByFormID(formID);
        }

        public void btnDelete_Click(object sender, EventArgs e)
        {
            if (DAC_Form.Delete(FormID))
            {
                Response.Redirect(ResolveUrl(String.Format("~/FormManagement.aspx?typeid={0}", TypeID)));
            }
            else
            {
                this.InsertError("Failed to delete form", "FormGroupServer");
            }
        }

        public void btnCreate_Click(object sender, EventArgs e)
        {
            Response.Redirect(ResolveUrl(String.Format("~/FormManagement.aspx?typeid={0}&action=create", TypeID)));
        }
    }
}
