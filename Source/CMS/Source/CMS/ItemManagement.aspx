﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CmsMaster.Master" AutoEventWireup="true"
    CodeBehind="ItemManagement.aspx.cs" Inherits="CMS.ItemManagement"
    ValidateRequest="false" %>

<asp:Content ID="Content4" ContentPlaceHolderID="placeholderHeadCSS" runat="server">
    <link href="css_inispinia/plugins/iCheck/custom.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="css_inispinia/plugins/chosen/chosen.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="css_inispinia/plugins/colorpicker/bootstrap-colorpicker.min.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="css_inispinia/plugins/cropper/cropper.min.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="css_inispinia/plugins/switchery/switchery.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="css_inispinia/plugins/jasny/jasny-bootstrap.min.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="css_inispinia/plugins/nouslider/jquery.nouislider.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="css_inispinia/plugins/datapicker/datepicker3.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="css_inispinia/plugins/ionRangeSlider/ion.rangeSlider.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="css_inispinia/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="css_inispinia/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="css_inispinia/plugins/clockpicker/clockpicker.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="css_inispinia/plugins/daterangepicker/daterangepicker-bs3.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="css_inispinia/plugins/select2/select2.min.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="FluentControls/css/flcontent.css" runat="server" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="placeholderHeadJS" runat="server">
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="placeholderHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="placeholderBody" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Item Mangement</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="~/Home.aspx" runat="server">Home</a>
                </li>
                <li class="active">
                    <strong>
                        <asp:Literal ID="litItemTypeDescription" runat="server">Item Management</asp:Literal>
                    </strong>
                </li>
                <!-- breadcrumbs -->
                <asp:ListView ID="lvBreadcrumbs" runat="server">
                    <LayoutTemplate>
                        <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                    </LayoutTemplate>
                    <ItemSeparatorTemplate>
                    </ItemSeparatorTemplate>
                    <ItemTemplate>
                        <li>
                            <%#Eval("Value") == "#" ? String.Format("<span class='inactive'>{0}</span>",Eval("key")): String.Format("<a href='{0}'>{1}</a>",Eval("Value"), Eval("Key"))%>
                        </li>
                    </ItemTemplate>
                </asp:ListView>
                <!-- /breadcrumbs -->
            </ol>
        </div>
        <div class="col-lg-2">
        </div>
    </div>
    <asp:Literal ID="litBreadCrumb" runat="server"></asp:Literal>
    <asp:MultiView ID="mvItems" runat="server" ActiveViewIndex="0">
        <asp:View ID="viewListing" runat="server">
            <div class="wrapper wrapper-content animated fadeInRight">
                <FC:ItemsModify ID="UCItemList" runat="server" />
            </div>
        </asp:View>
        <asp:View ID="viewDetail" runat="server">
            <asp:HiddenField ID="hdnSelectedTab" runat="server" />
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="tabs-container">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#tab-1">Item</a></li>
                                <li class=""><a data-toggle="tab" href="#tab-2">Related Item</a></li>
                                <li class=""><a data-toggle="tab" href="#tab-3">Related Form</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="tab-1" class="tab-pane active">
                                    <div class="panel-body">

                                        <br />
                                        <%--<FC:ItemControl ID="UCItemControl" runat="server" />--%>
                                        <FC:ItemCustomControl ID="UCItemCustomControl" runat="server" />
                                    </div>
                                </div>
                                <div id="tab-2" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlRelatedItemType" CssClass="form-control m-b" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:Button ID="btnManage" runat="server" CssClass="btn btn-primary" Text="Manage" OnClick="btnManage_Click" />
                                                </div>
                                                <div class="col-sm-7">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="tab-3" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlRelatedFormType" CssClass="form-control m-b" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:Button ID="btnManageForms" runat="server" CssClass="btn btn-primary" Text="Manage" OnClick="btnManage_Click" />
                                                </div>
                                                <div class="col-sm-7">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:View>
    </asp:MultiView>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="placeholderFooter" runat="server">

    <script src="<%=ResolveUrl("~/js_inispinia/plugins/jquery-ui/jquery-ui.min.js") %>" type="text/javascript"></script>
    <!-- Chosen -->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/chosen/chosen.jquery.js")%>" type="text/javascript"></script>
    <!-- JSKnob -->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/jsKnob/jquery.knob.js")%>" type="text/javascript"></script>
    <!-- Input Mask-->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/jasny/jasny-bootstrap.min.js")%>" type="text/javascript"></script>
    <!-- Data picker -->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/datapicker/bootstrap-datepicker.js")%>" type="text/javascript"></script>
    <!-- Clock picker -->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/clockpicker/clockpicker.js")%>" type="text/javascript"></script>
    <!-- Date range use moment.js same as full calendar plugin -->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/fullcalendar/moment.min.js")%>" type="text/javascript"></script>
    <!-- Date range picker -->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/daterangepicker/daterangepicker.js")%>" type="text/javascript"></script>
    <!-- Select2 -->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/select2/select2.full.min.js")%>" type="text/javascript"></script>
    <!-- custom-plugin -->
     <script src="<%=ResolveUrl("~/js_inispinia/custom-plugin.js")%>" type="text/javascript"></script>

</asp:Content>
