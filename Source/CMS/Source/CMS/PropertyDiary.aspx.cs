﻿using CMS.Helpers;
using Common.Session;
using FluentContent.DAC;
using FluentContent.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMS
{
    public partial class PropertyDiary : System.Web.UI.Page
    {

        public CMSUser UserSession { get { return SessionObjects.GetSession<CMSUser>(SessionKeys.CMS_USER_SESSION); } }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["dId"] != null)
                {
                    PropertyDiaryEntry propertyDiaryEntry = new PropertyDiaryEntry();
                    propertyDiaryEntry = DAC_PropertyDiaryEntry.GetPropertyDiaryEntryByID(int.Parse(Request.QueryString["dId"].ToString()));
                    TextBox_chrNotes.Text = propertyDiaryEntry.chrNotes;
                    button_Send.Text = "Update";
                }
            }
        }

   
        protected void Button_Send_Click(object sender, EventArgs e)
        {
            try
            {
                Label_errMsg.Text = "";

                #region Get Data From DB

                string actionType = "";
                PropertyDiaryEntry propertyDiaryEntry = new PropertyDiaryEntry();
                if (Request.QueryString["dId"] != null)//Update
                {
                    propertyDiaryEntry = DAC_PropertyDiaryEntry.GetPropertyDiaryEntryByID(int.Parse(Request.QueryString["dId"].ToString()));
                    propertyDiaryEntry.chrNotes = TextBox_chrNotes.Text;
                    DAC_PropertyDiaryEntry.Update(propertyDiaryEntry);
                    actionType = "DiaryEntryupdated";
                }
                else//Insert
                {
                    propertyDiaryEntry.datEntry = DateTime.Now;
                    propertyDiaryEntry.chrUser = UserSession.Username;
                    propertyDiaryEntry.intPropertyID = int.Parse(Request.QueryString["pId"].ToString());// GetPropertyIDQueryString(),
                    propertyDiaryEntry.chrNotes = TextBox_chrNotes.Text;
                    DAC_PropertyDiaryEntry.Insert(propertyDiaryEntry);
                    actionType = "DiaryEntrycreated";
                }


                #endregion

                
                string PropertyID = Request.QueryString["pId"].ToString();
                Response.Redirect("~/property-management/" + PropertyID + "?action="+actionType+"#tabs-5");
            }
            catch (Exception er)
            {
                Label_errMsg.Text = "A Error Occured:" + er.Message;
            }
        }

       


        protected void Button_Cancel_Click(object sender, EventArgs e)
        {
            string PropertyID = Request.QueryString["pId"].ToString();
            Response.Redirect("~/property-management/" + PropertyID + "#tabs-5");
        }
    }

}