﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Extensions;
using FluentContent.DAC;
using CMS.Helpers.Bases;
using FluentContent.Entities;
using FluentContent.Entities.Enums;


namespace CMS
{
    public partial class DepartmentManagement : BasePage
    {
        public int DeptID { get { return Request.GetQueryString<int>("deptid", 0); } }
        public string Action { get { return Request.GetQueryString<string>("action", null); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            // check user rights
            if (UserSession.Role.CMSAccessLevel != CMSAccessLevel.Full && UserSession.Role.CMSAccessLevel != CMSAccessLevel.Entity) Response.Redirect("~/Home.aspx");

            if (!IsPostBack)
            {
                if (DeptID != 0)
                    LoadDetail();
                else
                {
                    if (Action != "add")
                    {
                        LoadListing(UserSession.EntityID);
                    }
                    else
                    {
                        LoadAddView();
                    }
                    btnDelete.Visible = Action != "add";
                }
            }
        }

        private void LoadAddView()
        {
            InitControls(0, 0);
            ddlEntities.SelectedValue = UserSession.EntityID != 0 ? UserSession.EntityID.ToString() : "0";
            ddlEntities.Enabled = UserSession.EntityID == 0;
            ddlEntities_SelectedIndexChanged(ddlEntities, null);
            mvItems.SetActiveView(viewDetail);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;
            UserDepartment d = new UserDepartment();
            d.ID = DeptID;
            d.Name = txtDepartmentName.Text.Trim();
            d.ParentDepartmentID = ddlParentDepartments.SelectedValue.ToInt32();
            d.EntityID = ddlEntities.SelectedValue.ToInt32();
            if (Action != "add")
            {
                if (DAC_UserDepartment.Update(d))
                {
                    Response.Redirect(ResolveUrl("~/DepartmentManagement.aspx?info=updated"));
                }
                else
                {
                    this.InsertError("Failed to update department", "DeptGroupServer");
                }
            }
            else
            {
                if (DAC_UserDepartment.Insert(d) > 0)
                {
                    Response.Redirect(ResolveUrl("~/DepartmentManagement.aspx?info=added"));
                }
                else
                {
                    this.InsertError("Failed to insert department", "DeptGroupServer");
                }
            }

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (DAC_UserDepartment.Delete(DeptID))
            {
                Response.Redirect(ResolveUrl("~/DepartmentManagement.aspx?info=deleted"));
            }
            else
            {
                this.InsertError("Cannot delete department. One or more users belong to this department or any of its child departments", "DeptGroupServer");
            }
        }

        protected void ddlEntities_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<UserDepartment> entityDepartments = DAC_UserDepartment.GetByEntityID(ddlEntities.SelectedValue.ToInt32());
            ddlParentDepartments.DataBindWithDataSource(entityDepartments, "Name", "ID");
            ddlParentDepartments.SetDefaultValue("Select Department", "0", true);
        }

        private void LoadDetail()
        {
            UserDepartment d = DAC_UserDepartment.Get(DeptID);
            if (d == null) return;

            InitControls(d.EntityID, DeptID);

            txtDepartmentName.Text = d.Name;
            ddlEntities.SelectedValue = d.EntityID.ToString();
            ddlParentDepartments.SelectedValue = d.ParentDepartmentID.ToString();


            mvItems.SetActiveView(viewDetail);
        }

        private void InitControls(int entityID, int currentDeptID)
        {
            ddlEntities.DataSource = DAC_Entity.Get(UserSession.ClientID);
            ddlEntities.DataTextField = "Name";
            ddlEntities.DataValueField = "ID";
            ddlEntities.DataBind();
            ddlEntities.SetDefaultValue("No Entity", "0", true);

            ddlEntities.Enabled = UserSession.EntityID == 0;

            LoadDepartments(entityID, currentDeptID);

        }

        private void LoadDepartments(int entityID, int currentDeptID)
        {
            List<UserDepartment> items = DAC_UserDepartment.GetByEntityID(entityID);
            items = items.FindAll(i => i.ID != currentDeptID);
            ddlParentDepartments.DataSource = items;
            ddlParentDepartments.DataTextField = "Name";
            ddlParentDepartments.DataValueField = "ID";
            ddlParentDepartments.DataBind();
            ddlParentDepartments.SetDefaultValue("No Parent", "0", true);
        }

        private void LoadListing(int entityID)
        {
            List<UserDepartment> items = null;
            if (entityID > 0)
                items = DAC_UserDepartment.GetByEntityID(entityID);
            else
                items = DAC_UserDepartment.Get();

            lvItems.DataSource = items;
            lvItems.DataBind();
        }
    }
}
