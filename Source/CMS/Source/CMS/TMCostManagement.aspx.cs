﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Extensions;
using FluentContent.DAC;
using FLContentSiteAssist.BusinessLogic;
using CMS.Helpers.Bases;
using FluentContent.Entities.Enums;
using System.Data;
using FluentContent.Entities;

namespace CMS
{
    public partial class TMCostManagement : BasePage
    {
        public int CostID { get { return Request.GetQueryString<int>("costid", -1); } }
        public string Action { get { return Request.GetQueryString<string>("action"); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            // check user rights
            if (UserSession.Role.CMSAccessLevel != CMSAccessLevel.Full) Response.Redirect("~/Home.aspx");

            if (!IsPostBack)
            {
                if (CostID != -1 || Action == "create")
                {
                    LoadCostDetail();
                    if (Action == "create")
                        litBreadCromp.Text = "<b>Create</b>";
                    else
                        litBreadCromp.Text = "<b>Edit</b>";
                }
                else
                    LoadCostListing();
            }
        }
        private void LoadCostListing()
        {
            count.Text = "0";
            DataTable dtTMCost = DAC_TMCost.GetCost();
            if (dtTMCost.Rows.Count > 0)
                count.Text = dtTMCost.Rows.Count.ToString();
            lvTMCost.DataSource = dtTMCost;
            lvTMCost.DataBind();
        }
        private void LoadCostDetail()
        {
            mvPages.SetActiveView(viewDetail);
        }

        protected void btnCreateTop_Click(object sender, EventArgs e)
        {
            Response.Redirect(ResolveUrl(String.Format("~/TMCostManagement.aspx?costid={0}&action=create", CostID)));
        }
        protected void btndelete_Click(object sender, EventArgs e)
        {
            int costid = ((LinkButton)sender).CommandArgument.ToInt32();
            bool result = false;
            if (costid > 0)
                result = DAC_TMCost.Delete(new TMCost { CostID = costid });

            if (result)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Delete success','success');", true);
                Response.Redirect(Request.RawUrl);
            }
            else
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Delete failed!','warning');", true);
        }
    }
}