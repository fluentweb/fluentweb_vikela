﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CmsMaster.Master" AutoEventWireup="true" CodeBehind="PropertyValuationManagemet.aspx.cs" Inherits="CMS.PropertyValuationManagemet" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content4" ContentPlaceHolderID="placeholderHeadCSS" runat="server">
    <!-- Data Tables -->
    <link id="Link1" href="/css_inispinia/plugins/dataTables/dataTables.bootstrap.css" runat="server" rel="stylesheet" type="text/css" />
    <link id="Link2" href="/css_inispinia/plugins/dataTables/dataTables.responsive.css" runat="server" rel="stylesheet" type="text/css" />
    <link id="Link3" href="/css_inispinia/plugins/dataTables/dataTables.tableTools.min.css" runat="server" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="placeholderHeadJS" runat="server">
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/jeditable/jquery.jeditable.js")%>" type="text/javascript"></script>
    <!-- Data Tables -->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/dataTables/jquery.dataTables.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/dataTables/dataTables.bootstrap.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/dataTables/dataTables.responsive.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/dataTables/dataTables.tableTools.min.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="placeholderHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="placeholderBody" runat="server">
    <asp:MultiView ActiveViewIndex="0" runat="server" ID="mvValuations">
        <asp:View runat="server" ID="viewPropertyValuation">
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Property Valuations</h2>
                    <ol class="breadcrumb">
                        <li><a href="~/property-valuations/new" runat="server">New valuation</a></li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5></h5>
                                <div class="ibox-tools">
                                </div>
                            </div>
                            <div class="ibox-content padding" style="display: block;">
                                <%
                                    switch (Message)
                                    {
                                        case "created":
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Valuation successfully created!','success');", true);
                                            break;
                                        case "updated":
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Valuation successfully updated!','success');", true);
                                            break;
                                        case "novaluation":
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Valuation not found!','success');", true);
                                            break;
                                        case "deleted":
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Valuation successfully deleted!','success');", true);
                                            break;
                                        default:
                                            break;
                                    } 
                                %>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTables-example">
                                        <thead>
                                            <tr>
                                                <th>Date
                                                </th>
                                                <th>Name
                                                </th>
                                                <th>Email
                                                </th>
                                                <th>Value Type
                                                </th>
                                                <th></th>

                                            </tr>
                                        </thead>
                                        <asp:ListView runat="server" ID="lvValuations">
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%#Eval("Created", "{0:dd/MM/yyyy}") %>
                                                    </td>
                                                    <td><%#String.Format("{0}, {1}", Eval("Surname"), Eval("FirstName")) %>
                                                    </td>
                                                    <td><%#Eval("EmailAddress") %>
                                                    </td>
                                                    <td>
                                                        <%#(bool)Eval("WantsToRent") ? "Rent" : String.Empty %>
                                                        <%# (bool)Eval("WantsToRent") ? ((bool)Eval("WantsToSell") ? "/Sell" : String.Empty) : ((bool)Eval("WantsToSell") ? "Sell" :String.Empty) %>
                                                    </td>

                                                    <td>
                                                        <a href='<%#Eval("PropertyValuationID","~/property-valuations/{0}") %>' class="btn btn-info" runat="server"><i class="fa fa-paste"></i>&nbsp;Edit</a>
                                                        <a runat="server" onclick="return confirm('Are you sure?');" class="btn btn-danger" href='<%#Eval("PropertyValuationID","~/property-valuations/delete/{0}") %>'><i class="fa fa-trash"></i>&nbsp;Delete</a></td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:ListView>
                                        <tfoot>
                                            <tr>
                                                <th>Date
                                                </th>
                                                <th>Name
                                                </th>
                                                <th>Email
                                                </th>
                                                <th>Value Type
                                                </th>
                                                <th></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:View>
        <asp:View runat="server" ID="viewAddEditValuation">
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2><%=ValuationID > 0 ? "Edit Valuation"  : "New Valuation" %></h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="~/Home.aspx" runat="server">Home</a>
                        </li>
                        <li>
                            <a href="~/property-valuations" runat="server">Valuation List</a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h3>Personal Details</h3>
                                <div class="ibox-tools">
                                </div>
                            </div>
                            <div class="ibox-content" style="display: block;">
                                <div class="form-horizontal">
                                    <asp:ValidationSummary runat="server" ValidationGroup="PropertyValuationGroupServer" DisplayMode="List" />
                                    <div class="errorSummary hidden" validationgroup="PropertyValuationGroup">
                                        <center>
                                            Fields with red border are required!
                                        </center>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Title
                                        </label>
                                        <div class="col-sm-10">
                                            <asp:DropDownList runat="server" ID="ddlTitle" CssClass="form-control ue-ddl">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            First name
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="txtFirstName" runat="server" />
                                            <asp:RequiredFieldValidator ErrorMessage="First name is required" ControlToValidate="txtFirstName" runat="server" Display="None" ValidationGroup="PropertyValuationGroup" />
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Surname
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="txtSurname" runat="server" />
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Address Line 1
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="txtAddress1" runat="server" />
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Address Line 2
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="txtAddress2" runat="server" />
                                        </div>

                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Address Line 3
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="txtAddress3" runat="server" />
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Postcode
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="txtPostcode" runat="server" />
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Country
                                        </label>
                                        <div class="col-sm-10">
                                            <asp:DropDownList runat="server" ID="ddlCountry" CssClass="form-control ue-ddl" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" AutoPostBack="true" CausesValidation="false">
                                            </asp:DropDownList>
                                        </div>

                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            County
                                        </label>
                                        <div class="col-sm-10">
                                            <asp:DropDownList runat="server" ID="ddlCounty" CssClass="form-control ue-ddl">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Mobile
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="txtPhoneMobile" runat="server" />
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Home Telephone
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="txtPhoneHome" runat="server" />
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Work Telephone
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="txtPhoneWork" runat="server" />
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Email
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="txtEmail" runat="server" />
                                            <asp:RequiredFieldValidator ErrorMessage="Email is required" ControlToValidate="txtEmail" runat="server" Display="None" ValidationGroup="PropertyValuationGroup" />
                                            <asp:RegularExpressionValidator ErrorMessage="Invalid Email" ControlToValidate="txtEmail" runat="server" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="None" ValidationGroup="PropertyValuationGroup" />
                                        </div>
                                    </div>
                                    <br />
                                    <div class="ibox-title">
                                        <h3>Valuation</h3>
                                        <div class="ibox-tools">
                                        </div>
                                    </div>
                                    <br />

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            User would like to:
                                        </label>
                                        <div class="col-sm-2">
                                            <input type="checkbox" id="chkInterestedInRenting" runat="server" class="chkInterest" />
                                            Rent
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="checkbox" id="chkInterestedInSelling" runat="server" class="chkInterest" />
                                            Sell
                                        </div>
                                        <div class="col-sm-6">
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Valuation Property Type
                                        </label>
                                        <div class="col-sm-10">
                                            <asp:DropDownList runat="server" ID="ddlPropertyValuatonType" CssClass="form-control ue-ddl">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Bedrooms
                                        </label>
                                        <div class="col-sm-10">
                                            <asp:DropDownList ID="ddlBedrooms" runat="server" CssClass="form-control ue-ddl">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Bathrooms
                                        </label>
                                        <div class="col-sm-10">
                                            <asp:DropDownList ID="ddlBathrooms" runat="server" CssClass="form-control ue-ddl">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Additional Information
                                        </label>
                                        <div class="col-sm-10">
                                            <textarea id="txtAdditionalInformation" runat="server" class="form-control" placeholder="Please add any specific requirements here"></textarea>
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Value
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="txtValue" runat="server" />
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Notes
                                        </label>
                                        <div class="col-sm-10">
                                            <textarea id="txtNotes" runat="server" class="form-control" placeholder="Please add any additiona notes here"></textarea>
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-2">
                                            <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" type="submit" OnClick="btnSave_Click" Text="Submit" ValidationGroup="PropertyValuationGroup" />
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:View>
    </asp:MultiView>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="placeholderFooter" runat="server">
    <script src="<%=ResolveUrl("~/js_inispinia/dataTableCustom.js")%>" type="text/javascript"></script>
</asp:Content>
