﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CmsMaster.Master" AutoEventWireup="true" CodeBehind="PropertiesManagement.aspx.cs" Inherits="CMS.PropertiesManagement" EnableEventValidation="false" %>


<asp:Content ID="Content4" ContentPlaceHolderID="placeholderHeadCSS" runat="server">
    <!-- Data Tables -->
    <link href="/css_inispinia/plugins/dataTables/dataTables.bootstrap.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="/css_inispinia/plugins/dataTables/dataTables.responsive.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="/css_inispinia/plugins/dataTables/dataTables.tableTools.min.css" runat="server" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="placeholderHeadJS" runat="server">
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/jeditable/jquery.jeditable.js")%>" type="text/javascript"></script>
    <!-- Data Tables -->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/dataTables/jquery.dataTables.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/dataTables/dataTables.bootstrap.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/dataTables/dataTables.responsive.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/dataTables/dataTables.tableTools.min.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="placeholderHead" runat="server">
             <script type="text/javascript">
                 
                 $(function () {
                     // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
                     $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                         // save the latest tab; use cookies if you like 'em better:
                         localStorage.setItem('lastTab', $(this).attr('href'));
                     });
                     // go to the latest tab, if it exists:
                     var lastTab = localStorage.getItem('lastTab');
                     if (lastTab) {
                         $('[href="' + lastTab + '"]').tab('show');
                     }
                 });
    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="placeholderBody" runat="server">

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Property Management</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="~/Home.aspx" runat="server">Home</a>
                </li>
                <li>
                    <a href="~/property-management" runat="server">Property Management</a>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <asp:MultiView ActiveViewIndex="0" runat="server" ID="mvProperties">
            <asp:View runat="server" ID="viewPropertyList">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Add/Edit Properties</h5>
                                <div class="ibox-tools">
                                </div>
                            </div>
                            <div class="ibox-content padding" style="display: block;">
                                <asp:ValidationSummary ID="vsSummaryListing" runat="server" CssClass="errorSummary"
                                    ValidationGroup="PropertyListing" DisplayMode="List" />
                                <div class="updateMessage">
                                    <%
                                        switch (Action)
                                        {
                                            case "deleted":
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Property deleted successfully!','success');", true);
                                                break;
                                            default:
                                                break;
                                        } 
                                    %>
                                </div>
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Title</label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList runat="server" ID="ddlPropertyStatusFilter" OnSelectedIndexChanged="ddlPropertyStatusFilter_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control m-b" />
                                            <span class="label label-success  pull-right">
                                                <asp:Literal ID="litItemCount" runat="server" /></span>
                                        </div>
                                        <div class="col-sm-9">
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-4">
                                            <asp:Button Text="Create" runat="server" OnClick="btnCreateProperty_Click" ID="btnCreateTop" CssClass="btn btn-outline btn-success" type="submit" />
                                        </div>
                                    </div>
                                </div>
                                <div class="pageNumber" id="divTopPager" runat="server">
                                    <div class="numberWrapper">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div id="editable_paginate" class="dataTables_paginate paging_simple_numbers">
                                                    <ul class="pagination">
                                                        <asp:Literal ID="litTopPrev" runat="server"></asp:Literal>
                                                        <asp:ListView runat="server" ID="lvPagerTop">
                                                            <LayoutTemplate>
                                                                <asp:PlaceHolder runat="server" ID="itemPlaceholder" />
                                                            </LayoutTemplate>
                                                            <ItemTemplate>
                                                                <li class="<%#GetPagerActiveClass(Container.DataItem, "paginate_button active")%>"><a href="<%#GetPageLink(Container.DataItem) %>" aria-controls="editable" data-dt-idx="<%#Container.DataItem %>" tabindex="0"><%#Container.DataItem %></a></li>
                                                            </ItemTemplate>
                                                        </asp:ListView>
                                                        <asp:Literal ID="litTopNext" runat="server"></asp:Literal>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTables-propertySortnoclint">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <asp:HyperLink ID="hlRefNo" runat="server">Ref.No.</asp:HyperLink><asp:Image ID="imgRefNo" runat="server" />
                                                </th>
                                                <th>
                                                    <asp:HyperLink ID="hlTitle" runat="server">Title</asp:HyperLink><asp:Image ID="imgTitle" runat="server" />

                                                </th>
                                                <th>
                                                    <asp:HyperLink ID="hlType" runat="server">Type</asp:HyperLink><asp:Image ID="imgType" runat="server" />

                                                </th>
                                                <th>
                                                    <asp:HyperLink ID="hlAddress" runat="server">Address</asp:HyperLink><asp:Image ID="imgAddress" runat="server" />
                                                </th>
                                                <th>
                                                    <asp:HyperLink ID="hlCounty" runat="server">County</asp:HyperLink><asp:Image ID="imgCounty" runat="server" />
                                                </th>
                                                <th class="center">
                                                    <asp:HyperLink ID="hlForSale" runat="server">Sale</asp:HyperLink><asp:Image ID="imgForSale" runat="server" />

                                                </th>
                                                <th class="center">
                                                    <asp:HyperLink ID="hlForRent" runat="server">Rent</asp:HyperLink><asp:Image ID="imgForRent" runat="server" />

                                                </th>
                                                <th>
                                                    <asp:HyperLink ID="hlPrice" runat="server">Price</asp:HyperLink><asp:Image ID="imgPrice" runat="server" />
                                                </th>
                                                <th>
                                                    <asp:HyperLink ID="hlRental" runat="server">Rental</asp:HyperLink><asp:Image ID="imgRental" runat="server" />
                                                </th>

                                                <th>
                                                    <asp:HyperLink ID="hlStatus" runat="server">Status</asp:HyperLink><asp:Image ID="imgStatus" runat="server" /></th>
                                                <th>
                                                    <asp:HyperLink ID="hlCreated" runat="server">Added</asp:HyperLink><asp:Image ID="imgCreated" runat="server" /></th>
                                                <%--<th>Delete</th>--%>
                                                <th>Action</th>
                                                <%--                                                <th>View</th>--%>
                                            </tr>
                                        </thead>
                                        <asp:ListView runat="server" ID="lvProperties">
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%#Eval("PropertyCode") %></td>
                                                    <td><%#Eval("Title") %></td>
                                                    <td><%#Eval("PropertyType") %></td>
                                                    <td><%#Eval("Address1") %></td>
                                                    <td><%#Eval("County.CountyName") %></td>
                                                    <td class="center"><%#(bool)Eval("ForSale") ? String.Format("<img src='{0}' alt='yes'/>", ResolveUrl("~/images/yes.png")): String.Empty %></td>
                                                    <td class="center"><%#(bool)Eval("ForRent") ? String.Format("<img src='{0}' alt='yes'/>", ResolveUrl("~/images/yes.png")): String.Empty %></td>
                                                    <td><%#(decimal)Eval("Price") != 0 ? Eval("Price", "{0:C}") : String.Empty %></td>
                                                    <td><%#(decimal)Eval("Rental") != 0 ? Eval("Rental", "{0:C}"): String.Empty %></td>

                                                    <td><%#Eval("StatusName") %></td>
                                                    <td><%#Eval("Created", "{0:dd/MM/yyyy}") %></td>
                                                    <%--   <td>
                                <asp:Button Text="Delete" runat="server" OnClientClick="return confirm('Are you sure?');" CommandArgument='<%#Eval("ID") %>' CssClass="linkStyle" ID="btnDelete" OnClick="btnDelete_Click" />
                            </td>--%>
                                                    <td><a id="A1" class="btn btn-info" style="font-size: 12px !important;" href='<%#Eval("ID", "/property-management/{0}") %>' runat="server"><i class="fa fa-paste"></i>&nbsp;Edit</a>&nbsp;&nbsp;<a id="A2" class="btn btn-primary" style="font-size: 12px !important;" href='<%#Eval("WebsitePropertyURL") %>' runat="server" target="_blank"><i class="fa fa-external-link"></i>&nbsp;View</a></td>
                                                </tr>
                                            </ItemTemplate>
                                            <EmptyDataTemplate>
                                                <td colspan="11">No properties yet...</td>
                                            </EmptyDataTemplate>
                                        </asp:ListView>
                                        <tfoot>
                                            <tr>
                                                <th>Ref.No.
                                                </th>
                                                <th>Title

                                                </th>
                                                <th>Type

                                                </th>
                                                <th>Address
                                                </th>
                                                <th>County
                                                </th>
                                                <th class="center">Sale

                                                </th>
                                                <th class="center">Rent

                                                </th>
                                                <th>Price
                                                </th>
                                                <th>Rental
                                                </th>

                                                <th>Status
                                                </th>
                                                <th>Added
                                                </th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <div class="pageNumber" id="divBottomPager" runat="server">
                                    <div class="numberWrapper" style="margin-bottom: 6px;">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div id="editable_paginate" class="dataTables_paginate paging_simple_numbers">
                                                    <ul class="pagination">
                                                        <asp:Literal ID="litBottomPrev" runat="server"></asp:Literal>
                                                        <asp:ListView runat="server" ID="lvPagerBottom">
                                                            <LayoutTemplate>
                                                                <asp:PlaceHolder runat="server" ID="itemPlaceholder" />
                                                            </LayoutTemplate>
                                                            <ItemTemplate>
                                                                <li class="<%#GetPagerActiveClass(Container.DataItem, "paginate_button active")%>"><a href="<%#GetPageLink(Container.DataItem) %>" aria-controls="editable" data-dt-idx="<%#Container.DataItem %>" tabindex="0"><%#Container.DataItem %></a></li>
                                                            </ItemTemplate>
                                                        </asp:ListView>
                                                        <asp:Literal ID="litBottomNext" runat="server"></asp:Literal>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <div class="col-sm-4">
                                                    <asp:Button Text="Create" runat="server" OnClick="btnCreateProperty_Click" ID="btnCreateProperty" CssClass="btn btn-outline btn-success" type="submit" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:View>
            <asp:View runat="server" ID="viewPropertyCreateEdit">
                <div class="row wrapper border-bottom white-bg">
                    <div class="panel-body">
                        <div class="form-horizontal">
                            <div class="col-sm-4 col-sm-offset-2">
                                <asp:Button runat="server" ID="btnSaveTop" OnClick="btnSave_Click" Text="Save" CssClass="btn btn-primary" type="submit" ValidationGroup="PropertyGroup" />
                            </div>
                        </div>
                    </div>
                </div>
                <asp:HiddenField ID="hdnSelectedTab" runat="server" />
                <div class="row">
                    <div class="col-lg-12">
                        <div class="tabs-container">
                            <ul class="nav nav-tabs">
                                <li id="htab1" class="active"><a id="atab1" data-toggle="tab" href="#tabs-1">Property Details</a></li>
                                <li id="htab2" class=""><a id="atab2" data-toggle="tab" href="#tabs-2">Assets</a></li>
                                <li id="htab3" class=""><a id="atab3" data-toggle="tab" href="#tabs-3">Attributes</a></li>
                                <li id="htab4" class=""><a id="atab4" data-toggle="tab" href="#tabs-4">Images</a></li>
                                <li id="htab5" class=""><a id="atab5" data-toggle="tab" href="#tabs-5">Diary</a></li>
                                <li id="htab6" class=""><a id="atab6" data-toggle="tab" href="#tabs-6">Open Viewings</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="tabs-1" class="tab-pane active">
                                    <div class="panel-body">
                                        <br />
                                        <div class="updateMessage">
                                            <%switch (Action)
                                              {
                                                  case "updated":
                                                      ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Property updated successfully!','success');", true);
                                                      break;
                                                  case "updated-alerted":
                                                      ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Property created successfully & users with matching criteria alerted " + Request.GetQueryString<int>("alerted", 0) + "user(s) alerted)!','success');", true);
                                                      break;
                                                  case "created":
                                                      ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Property created successfully!','success');", true);
                                                      break;
                                                  case "created-alerted":
                                                      ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Property created successfully & users with matching criteria alerted " + Request.GetQueryString<int>("alerted", 0) + "user(s) alerted)!','success');", true);
                                                      break;
                                                  default:
                                                      break;
                                              } %>
                                        </div>
                                        <div class="form-horizontal">
                                            <asp:ValidationSummary ID="vsErrorSummary" runat="server" CssClass="errorSummary"
                                                ValidationGroup="PropertyGroupServer" DisplayMode="List" />
                                            <div class="errorSummary hidden" validationgroup="PropertyGroup">
                                                <center>
                                                    All highlighted fields are required, also make sure to enter the appropriate format.
                                                </center>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Property Reference
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:TextBox runat="server" ID="txtPropertyReference" MaxLength="20" CssClass="form-control" />
                                                    <asp:HyperLink ID="hlPreview" Target="_blank" runat="server" Text="Preview property" Style="margin-left: 10px;" CssClass="previewLink" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Property Title*
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:TextBox runat="server" ID="txtPropertyTitle" MaxLength="250" CssClass="form-control" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="None" ErrorMessage="Property title is required" ControlToValidate="txtPropertyTitle" runat="server" ValidationGroup="PropertyGroup" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Description
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:TextBox runat="server" ID="txtDescription" TextMode="MultiLine" Style="resize: none;" CssClass="form-control" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Additional Description
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:TextBox runat="server" ID="txtAdditionalDescription" TextMode="MultiLine" Style="resize: none;" CssClass="form-control" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Address Line 1
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:TextBox runat="server" ID="txtAddress1" MaxLength="50" CssClass="form-control" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Address Line 2
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:TextBox runat="server" ID="txtAddress2" MaxLength="50" CssClass="form-control" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Address Line 3
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:TextBox runat="server" ID="txtAddress3" MaxLength="50" CssClass="form-control" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Postcode
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:TextBox runat="server" ID="txtAddressPostcode" MaxLength="20" CssClass="form-control" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Country
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:DropDownList CssClass="form-control m-b" runat="server" ID="ddlCountry" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    County
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:DropDownList CssClass="form-control m-b" runat="server" ID="ddlCounty" />
                                                    <asp:HiddenField ID="hdnCounty" runat="server" ClientIDMode="Static" />

                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Status
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:DropDownList CssClass="form-control m-b" runat="server" ID="ddlStatus" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Google Coordinates
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:TextBox runat="server" ID="txtGoogleCoordinates" MaxLength="400" CssClass="form-control" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    For Sale
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:CheckBox ID="chkForSale" runat="server" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Price Currency</label><div class="col-sm-10">
                                                    <asp:DropDownList CssClass="form-control m-b" runat="server" ID="ddlPriceCurrency" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Price
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:TextBox runat="server" ID="txtPrice" CssClass="form-control" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ErrorMessage="Invalid value" Display="None" ValidationExpression="^(?=.*\d)\d*(?:\.\d\d)?$" ControlToValidate="txtPrice" runat="server" ValidationGroup="PropertyGroup" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Old Price
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:TextBox runat="server" ID="txtPriceOld" CssClass="form-control" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ErrorMessage="Invalid value" Display="None" ValidationExpression="^(?=.*\d)\d*(?:\.\d\d)?$" ControlToValidate="txtPriceOld" runat="server" ValidationGroup="PropertyGroup" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    To Let
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:CheckBox ID="chkForRent" runat="server" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Rental Currency</label><div class="col-sm-10">
                                                    <asp:DropDownList CssClass="form-control m-b" runat="server" ID="ddlRentalCurrency" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Rental
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:TextBox runat="server" ID="txtRental" CssClass="form-control" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ErrorMessage="Invalid value" Display="None" ValidationExpression="^(?=.*\d)\d*(?:\.\d\d)?$" ControlToValidate="txtRental" runat="server" ValidationGroup="PropertyGroup" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Rental Terms</label><div class="col-sm-10">
                                                    <asp:DropDownList CssClass="form-control m-b" runat="server" ID="ddlRentalTerms" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Rental Old
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:TextBox runat="server" ID="txtRentalOld" CssClass="form-control" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ErrorMessage="Invalid value" Display="None" ValidationExpression="^(?=.*\d)\d*(?:\.\d\d)?$" ControlToValidate="txtRentalOld" runat="server" ValidationGroup="PropertyGroup" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Promotion 1
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:CheckBox ID="chkPromotion1" runat="server" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Promotion 2
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:CheckBox ID="chkPromotion2" runat="server" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Additional Information
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:TextBox runat="server" ID="txtAdditionalInformation" TextMode="MultiLine" Style="resize: none;" CssClass="form-control" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Property Type
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:DropDownList CssClass="form-control m-b" runat="server" ID="ddlPropertyType" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Bedroom Count
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:TextBox runat="server" ID="txtBedroomCount" CssClass="form-control" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Bathroom Count
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:TextBox runat="server" ID="txtBathroomCount" CssClass="form-control" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Agency Type
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:DropDownList CssClass="form-control m-b" runat="server" ID="ddlAgencyType" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Tenure
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:DropDownList CssClass="form-control m-b" runat="server" ID="ddlTenure" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Category
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:DropDownList CssClass="form-control m-b" runat="server" ID="ddlCategory" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    For Sale Status
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:DropDownList CssClass="form-control m-b" runat="server" ID="ddlForSaleStatus" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    To Let Status
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:DropDownList CssClass="form-control m-b" runat="server" ID="ddlForRentStatus" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Map Reference
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:TextBox runat="server" ID="txtMapReference" CssClass="form-control" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Directions
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:TextBox runat="server" ID="txtDirections" TextMode="MultiLine" Style="resize: none;" CssClass="form-control" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Commission
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:TextBox runat="server" ID="txtCommission" CssClass="form-control" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    For Sale/To Let Board
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:CheckBox runat="server" ID="chkForSaleRentBoard" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Advertisement Allowed
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:CheckBox runat="server" ID="chkAdvertisementAllowed" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <h3>Vendor Details</h3>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Name of Vendor(s)
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:TextBox CssClass="form-control" runat="server" ID="txtVendorsName" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Address Line 1
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:TextBox CssClass="form-control" runat="server" ID="txtVendorAddressLine1" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Address Line 2
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:TextBox CssClass="form-control" runat="server" ID="txtVendorAddressLine2" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Address Line 3
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:TextBox CssClass="form-control" runat="server" ID="txtVendorAddressLine3" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Postcode
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:TextBox CssClass="form-control" runat="server" ID="txtVendorPostcode" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Country
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:DropDownList runat="server" ID="ddlVendorCountry" CssClass="form-control m-b" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    County
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:DropDownList runat="server" ID="ddlVendorCounty" CssClass="form-control m-b" />
                                                    <asp:HiddenField ID="hdnVendorCounty" runat="server" ClientIDMode="Static" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Home Phone
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:TextBox CssClass="form-control" runat="server" ID="txtVendorHomePhone" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Mobile
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:TextBox CssClass="form-control" runat="server" ID="txtVendorMobilePhone" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Work Phone
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:TextBox CssClass="form-control" runat="server" ID="txtVendorWorkPhone" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Email
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:TextBox CssClass="form-control" runat="server" ID="txtVendorEmail" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Keys Held / Viewing Info
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:TextBox CssClass="form-control" runat="server" ID="txtVendorKeysHeldViewingInfo" TextMode="MultiLine" Style="resize: none;" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Additional Information
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:TextBox CssClass="form-control" runat="server" ID="txtVendorAdditionalInfo" TextMode="MultiLine" Style="max-height: 80px; height: 80px; resize: none;" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Reason for Selling/Letting
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:TextBox CssClass="form-control" runat="server" ID="txtReasonForSellingRenting" TextMode="MultiLine" Style="resize: none;" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Reason for Removing
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:TextBox CssClass="form-control" runat="server" ID="txtReasonForRemoving" TextMode="MultiLine" Style="resize: none;" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Next Purchase
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:TextBox CssClass="form-control" runat="server" ID="txtVendorNextPurchase" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    How they heard
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:TextBox CssClass="form-control" runat="server" ID="txtVendorHowTheyHeard" TextMode="MultiLine" Style="resize: none;" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Restrictions
                                                </label>
                                                <div class="col-sm-10">
                                                    <asp:TextBox CssClass="form-control" runat="server" ID="txtVendorRestrictions" TextMode="MultiLine" Style="resize: none;" />
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <h3>Attachments</h3>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    Attachment 
                                                </label>
                                                <div class="col-sm-4">
                                                    <asp:FileUpload ID="fuAttachment1" CssClass="btn btn-outline btn-success" type="file" Width="100%" runat="server" />
                                                </div>
                                                <div class="col-sm-6">
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <asp:Panel runat="server" ID="pnlExistingAttachment" Visible="false">
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">
                                                        Current Attachment
                                                    </label>
                                                    <div class="col-sm-10">
                                                        <asp:TextBox ID="txtExistingAttachment" CssClass="form-control" runat="server" ReadOnly="true" />
                                                    </div>
                                                </div>
                                                <div class="hr-line-dashed"></div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Overwrite existing file</label><div class="col-sm-10">
                                                        <asp:CheckBox runat="server" ID="chkOverwriteFile" CssClass="input_enabled" Checked="false" />
                                                    </div>
                                                </div>
                                                <div class="hr-line-dashed"></div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Delete existing file</label><div class="col-sm-10">
                                                        <asp:CheckBox runat="server" ID="chkDeleteAttachment" CssClass="input_enabled" Checked="false" />
                                                    </div>
                                                </div>
                                                <div class="hr-line-dashed"></div>
                                            </asp:Panel>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Select alert type</label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlEmailAlert" CssClass="form-control m-b" runat="server"></asp:DropDownList>

                                                </div>
                                                <div class="col-sm-4">
                                                    <asp:Button runat="server" ID="btnSaveAndAlert" CssClass="btn btn-primary" type="submit" OnClick="btnSaveAndAlert_Click" Text="Save & Send Alert Email" ValidationGroup="PropertyGroup" OnClientClick="return confirm('This will email users with matching criteria. Are you sure?')" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label"></label>
                                                <div class="col-sm-10">
                                                    &nbsp;
                                                </div>
                                            </div>
                                            <div class="col-sm-4 col-sm-offset-2">
                                                <asp:Button runat="server" ID="btnSave" OnClick="btnSave_Click" Text="Save" CssClass="btn btn-primary" type="submit" ValidationGroup="PropertyGroup" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="tabs-2" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="ibox float-e-margins">
                                                    <div class="ibox-title">
                                                        <h5>New Asset</h5>
                                                        <div class="ibox-tools">
                                                            <asp:HyperLink ID="hlAssetsPreviewProperty" Target="_blank" runat="server" Text="Preview property" />
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content padding" style="display: block;">
                                                        <asp:ValidationSummary ID="vsAssets" runat="server" CssClass="errorSummary"
                                                            ValidationGroup="PropertyGroupAssetsServer" DisplayMode="List" />
                                                        <div class="errorSummary hidden" validationgroup="PropertyGroupAssets">
                                                            <center>
                                                                All highlighted fields are required, also make sure to enter the appropriate format.
                                                            </center>
                                                        </div>
                                                        <div class="updateMessage">
                                                            <%
                                                                switch (Action)
                                                                {
                                                                    case "assetupdated":
                                                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Asset updated successfully!','success');", true);
                                                                        break;
                                                                    case "assetcreated":
                                                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Asset created successfully!','success');", true);
                                                                        break;
                                                                    case "assetdeleted":
                                                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage(' Asset deleted successfully!','success');", true);
                                                                        break;
                                                                    default:
                                                                        break;
                                                                } 
                                                            %>
                                                        </div>
                                                        <div class="form-horizontal">
                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">
                                                                    Asset Type
                                                                </label>
                                                                <div class="col-sm-10">
                                                                    <asp:DropDownList runat="server" ID="ddlAssetType" CssClass="form-control m-b" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">
                                                                    Length (m)
                                                                </label>
                                                                <div class="col-sm-10">
                                                                    <asp:TextBox runat="server" CssClass="form-control" ID="txtAssetLengthInMetres" />
                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" ErrorMessage="Enter a valid value in metres" Display="None" ControlToValidate="txtAssetLengthInMetres" runat="server" ValidationGroup="PropertyGroupAssets" ValidationExpression="^(?=.*\d)\d*(?:\.\d\d)?$" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">
                                                                    Width (m)
                                                                </label>
                                                                <div class="col-sm-10">
                                                                    <asp:TextBox runat="server" CssClass="form-control" ID="txtAssetWidthInMetres" />
                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator7" ErrorMessage="Enter a valid value in metres" Display="None" ControlToValidate="txtAssetWidthInMetres" runat="server" ValidationGroup="PropertyGroupAssets" ValidationExpression="^(?=.*\d)\d*(?:\.\d\d)?$" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">
                                                                    Size(m<sup>2</sup>)
                                                                </label>
                                                                <div class="col-sm-10">
                                                                    <asp:TextBox runat="server" CssClass="form-control" ID="txtAssetSizeInMetresSquared" />
                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator8" ErrorMessage="Enter a valid value in metres" Display="None" ControlToValidate="txtAssetSizeInMetresSquared" runat="server" ValidationGroup="PropertyGroupAssets" ValidationExpression="^(?=.*\d)\d*(?:\.\d\d)?$" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">
                                                                    Floor
                                                                </label>
                                                                <div class="col-sm-10">
                                                                    <asp:TextBox runat="server" CssClass="form-control" ID="txtAssetOnFloorNumber" />
                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator9" ErrorMessage="Enter a valid floor number" Display="None" ControlToValidate="txtAssetOnFloorNumber" runat="server" ValidationGroup="PropertyGroupAssets" ValidationExpression="^\d?\d$" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">
                                                                    Description
                                                                </label>
                                                                <div class="col-sm-10">
                                                                    <asp:TextBox runat="server" TextMode="MultiLine" ID="txtAssetDescription" CssClass="form-control" Style="resize: none;" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-sm-4 col-sm-offset-2">
                                                                    <asp:Button Text="Add This Asset" OnClick="btnSaveAsset_Click" ID="btnSaveAsset" runat="server" CssClass="btn btn-primary" type="submit" ValidationGroup="PropertyGroupAssets" OnClientClick="formChangesMade = false;" />
                                                                </div>
                                                            </div>
                                                            <div class="hr-line-dashed"></div>

                                                        </div>
                                                        <asp:ListView runat="server" ID="lvAssets" OnItemDataBound="lvAssets_ItemDataBound" OnItemCommand="lvAssets_ItemCommand" DataKeyNames="AssetItemID, ID, PropertyID">
                                                            <LayoutTemplate>
                                                                <h3>Existing Assets</h3>
                                                                <asp:PlaceHolder runat="server" ID="itemPlaceholder" />
                                                            </LayoutTemplate>
                                                            <ItemTemplate>
                                                                <div class="form-horizontal">
                                                                    <div class="form-group">
                                                                        <label class="col-sm-2 control-label">
                                                                            Asset Type
                                                                        </label>
                                                                        <div class="col-sm-10">
                                                                            <asp:DropDownList runat="server" ID="ddlAssetType" CssClass="form-control m-b" />
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="col-sm-2 control-label">
                                                                            Length (m)
                                                                        </label>
                                                                        <div class="col-sm-10">
                                                                            <asp:TextBox runat="server" CssClass="form-control" ID="txtAssetLengthInMetres" Text='<%#(double)Eval("Length")== 0 ? String.Empty : Eval("Length", "{0:F2}") %>' />
                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator10" ErrorMessage="Enter a valid value in metres" Display="None" ControlToValidate="txtAssetLengthInMetres" runat="server" ValidationGroup="PropertyGroupAssets" ValidationExpression="^(?=.*\d)\d*(?:\.\d\d)?$" />
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="col-sm-2 control-label">
                                                                            Width (m)
                                                                        </label>
                                                                        <div class="col-sm-10">
                                                                            <asp:TextBox runat="server" CssClass="form-control" ID="txtAssetWidthInMetres" Text='<%#(double)Eval("Width")== 0 ? String.Empty : Eval("Width", "{0:F2}") %>' />
                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator11" ErrorMessage="Enter a valid value in metres" Display="None" ControlToValidate="txtAssetWidthInMetres" runat="server" ValidationGroup="PropertyGroupAssets" ValidationExpression="^(?=.*\d)\d*(?:\.\d\d)?$" />
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="col-sm-2 control-label">
                                                                            Size(m<sup>2</sup>)
                                                                        </label>
                                                                        <div class="col-sm-10">
                                                                            <asp:TextBox runat="server" CssClass="form-control" ID="txtAssetSizeInMetresSquared" Text='<%#(double)Eval("Size") == 0 ? String.Empty :Eval("Size", "{0:F2}") %>' />
                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator12" ErrorMessage="Enter a valid value in metres" Display="None" ControlToValidate="txtAssetSizeInMetresSquared" runat="server" ValidationGroup="PropertyGroupAssets" ValidationExpression="^(?=.*\d)\d*(?:\.\d\d)?$" />
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="col-sm-2 control-label">
                                                                            Floor
                                                                        </label>
                                                                        <div class="col-sm-10">
                                                                            <asp:TextBox runat="server" CssClass="form-control" ID="txtAssetOnFloorNumber" Text='<%#(int)Eval("FloorNumber") == 0 ? String.Empty : Eval("FloorNumber") %>' />
                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator13" ErrorMessage="Enter a valid floor number" Display="None" ControlToValidate="txtAssetOnFloorNumber" runat="server" ValidationGroup="PropertyGroupAssets" ValidationExpression="^\d?\d$" />
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="col-sm-2 control-label">
                                                                            Description
                                                                        </label>
                                                                        <div class="col-sm-10">
                                                                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" ID="txtAssetDescription" Text='<%#Eval("Description") %>' />
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="col-sm-4 col-sm-offset-2">
                                                                            <asp:Button Text="Delete" ID="btnDeleteAsset" runat="server" CssClass="btn btn-danger" type="submit" CausesValidation="false" CommandName="DeleteAsset" CommandArgument='<%#Eval("ID") %>' OnClientClick="formChangesMade = false; confirm('Are you sure?');" />
                                                                            <asp:Button Text="Save" ID="btnSaveAsset" runat="server" CssClass="btn btn-primary" type="submit" ValidationGroup="PropertyGroupAssets" CommandName="UpdateAsset" CommandArgument='<%#Eval("ID") %>' OnClientClick="formChangesMade = false;" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="hr-line-dashed"></div>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:ListView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="tabs-3" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="ibox float-e-margins">
                                                    <div class="ibox-title">
                                                        <h5>New Attribute</h5>
                                                        <div class="ibox-tools">
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content padding" style="display: block;">
                                                        <asp:ValidationSummary ID="vsSummaryAttributes" runat="server" CssClass="errorSummary"
                                                            ValidationGroup="PropertyGroupAssetsServer" DisplayMode="List" />
                                                        <div class="errorSummary hidden attributeSummary" validationgroup="PropertyGroupAttributes">
                                                            <center>
                                                                All highlighted fields are required, also make sure to enter the appropriate format.
                                                            </center>
                                                        </div>
                                                        <div class="updateMessage">
                                                            <%switch (Action)
                                                              {
                                                                  case "attributesupdated":
                                                                      ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Attributes updated successfully!','success');", true);
                                                                      break;
                                                                  case "attributecreated":
                                                                      ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Attribute created successfully!','success');", true);
                                                                      break;
                                                                  case "attributedeleted":
                                                                      ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Attribute deleted successfully!','success');", true);
                                                                      break;
                                                                  default:
                                                                      break;
                                                              } %>
                                                        </div>
                                                        <div class="form-horizontal">
                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">Attribute Type:</label>
                                                                <div class="col-sm-10">
                                                                    <asp:DropDownList runat="server" ID="ddlAttributeType" CssClass="ddlAttributeType" />
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Select and Attribute Type" ControlToValidate="ddlAttributeType" Display="None" runat="server" ValidationGroup="PropertyGroupAttributes" />
                                                                </div>
                                                            </div>
                                                            <div class="hr-line-dashed"></div>
                                                            <div class="form-group">
                                                                <label id="valueLabel" runat="server" class="col-sm-2 control-label">
                                                                    Value:
                                                                </label>
                                                                <div class="col-sm-10">
                                                                    <div class="attributeValueString hidden">
                                                                        <%--<asp:TextBox runat="server" MaxLength="400" ID="txtAttributeStringValue" />--%>
                                                                        <asp:DropDownList ID="ddlTextAttributeStringValue" runat="server" CssClass="autocomplete" ClientIDMode="Static"></asp:DropDownList>
                                                                        <asp:HiddenField ID="hdnTextAttributeStringValue" runat="server" />
                                                                    </div>
                                                                    <div class="attributeValueDate hidden">
                                                                        <asp:TextBox runat="server" MaxLength="400" ID="txtAttributeDateValue" CssClass="date" />
                                                                        <asp:RequiredFieldValidator ErrorMessage="Value is required" ControlToValidate="txtAttributeDateValue" Display="None" ValidationGroup="PropertyGroupAttributes" runat="server" ID="rfvAttributeDateValue" ClientIDMode="Static" />
                                                                        <asp:CompareValidator ErrorMessage="Invalid date" ControlToValidate="txtAttributeDateValue" runat="server" Display="None"
                                                                            ValidationGroup="PropertyGroupAttributes" Type="Date" Operator="DataTypeCheck" ID="cvAttributeDateValue" ClientIDMode="Static" />
                                                                    </div>
                                                                    <div class="attributeValueNumeric hidden">
                                                                        <asp:TextBox runat="server" MaxLength="400" ID="txtAttributeNumericValue" />
                                                                        <asp:RequiredFieldValidator ErrorMessage="Value is required" ControlToValidate="txtAttributeNumericValue" Display="None" ValidationGroup="PropertyGroupAttributes" runat="server" ID="rfvAttributeNumericValue" ClientIDMode="Static" />
                                                                        <asp:RegularExpressionValidator ID="revAttributeNumericValue" runat="server" ControlToValidate="txtAttributeNumericValue" Display="None" ClientIDMode="Static"
                                                                            ValidationGroup="PropertyGroupAttributes" ErrorMessage="Invalid number" ValidationExpression="^(?=.*\d)\d*(?:\.\d+)?$" />
                                                                    </div>
                                                                    <div class="attributeValueBool hidden">
                                                                        <asp:DropDownList runat="server" ID="ddlAttributeBoolValue">
                                                                            <asp:ListItem Text="YES" Value="1" />
                                                                            <asp:ListItem Text="NO" Value="0" />
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="hr-line-dashed"></div>
                                                            <div class="form-group">
                                                                <div class="col-sm-4 col-sm-offset-2">
                                                                    <asp:Button Text="Save" runat="server" CssClass="btn btn-primary" type="submit" ValidationGroup="PropertyGroupAttributes" OnClick="btnSaveAttribute_Click" ID="btnSaveAttribute" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <asp:ListView runat="server" ID="lvAttributes" DataKeyNames="ID, AttributeTypeItemID, AttributeTypeItemDataType">
                                            <LayoutTemplate>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="ibox float-e-margins">
                                                            <div class="ibox-title">
                                                                <h5>Existing Attributes</h5>
                                                                <div class="ibox-tools">
                                                                </div>
                                                            </div>
                                                            <div class="ibox-content">
                                                                <div class="table-responsive">
                                                                    <table class="table table-striped table-bordered table-hover dataTables-example">
                                                                        <asp:PlaceHolder runat="server" ID="itemPlaceholder" />
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </LayoutTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%#Eval("AttributeTypeItemName") %> </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="txtAttributeValueList" Text='<%#Eval("Value") %>' CssClass='<%#Eval("AttributeTypeItemDataType").ToString() == "Boolean"  ? "hidden" : Eval("AttributeTypeItemDataType").ToString() == "Date" ? "date" :String.Empty %>' />
                                                        <asp:DropDownList runat="server" SelectedValue='<%#Eval("Value").ToString() != "1" && Eval("Value").ToString() != "0" ? "0" : Eval("Value") %>' ID="ddlAttribtueValueList" Visible='<%#Eval("AttributeTypeItemDataType").ToString() == "Boolean" %>'>
                                                            <asp:ListItem Text="YES" Value="1" />
                                                            <asp:ListItem Text="NO" Value="0" />
                                                        </asp:DropDownList>
                                                        <%#Eval("AttributeTypeItemDataType").ToString() != "Boolean" ? Eval("AttributeTypeItemDataType", "({0})") : String.Empty %>
                                                        <asp:RequiredFieldValidator ErrorMessage="Value is required" ControlToValidate="txtAttributeValueList" Display="None" Enabled='<%#Eval("AttributeTypeItemDataType").ToString() == "String" %>' ValidationGroup="PropertyGroupAttributeList" runat="server" ID="rfvAttributeStringValue" />

                                                        <asp:RequiredFieldValidator ErrorMessage="Value is required" ControlToValidate="txtAttributeValueList" Display="None" ValidationGroup="PropertyGroupAttributeList" runat="server" ID="rfvAttributeDateValue" Enabled='<%#Eval("AttributeTypeItemDataType").ToString() == "Date" %>' />

                                                        <asp:CompareValidator ErrorMessage="Invalid date" ControlToValidate="txtAttributeValueList" runat="server" Display="None"
                                                            ValidationGroup="PropertyGroupAttributeList" Type="Date" Operator="DataTypeCheck" ID="cvAttributeDateValue" Enabled='<%#Eval("AttributeTypeItemDataType").ToString() == "Date" %>' />

                                                        <asp:RequiredFieldValidator ErrorMessage="Value is required" ControlToValidate="txtAttributeValueList" Display="None" ValidationGroup="PropertyGroupAttributeList" runat="server" ID="rfvAttributeNumericValue" Enabled='<%#Eval("AttributeTypeItemDataType").ToString() == "Numeric" %>' />

                                                        <asp:RegularExpressionValidator ID="revAttributeNumericValue" runat="server" ControlToValidate="txtAttributeValueList" Display="None"
                                                            ValidationGroup="PropertyGroupAttributeList" ErrorMessage="Invalid number" ValidationExpression="^(?=.*\d)\d*(?:\.\d+)?$" Enabled='<%#Eval("AttributeTypeItemDataType").ToString() == "Numeric" %>' />
                                                    </td>
                                                    <td>
                                                        <asp:Button Text="Delete" runat="server" CommandArgument='<%#Container.DataItemIndex %>' CssClass="btn btn-w-m btn-danger" OnClick="btnDeleteAttribute_Click" OnClientClick="return confirm('Are you sure?');" ID="btnAttributeDelete" />
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:ListView>
                                    </div>
                                </div>
                                <div id="tabs-4" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="ibox float-e-margins">
                                                    <div class="ibox-title">
                                                        <h5><%=String.IsNullOrEmpty(ImageGUID) ? "New Image" : "Edit Image" %></h5>
                                                        <div class="ibox-tools">
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content padding" style="display: block;">
                                                        <asp:ValidationSummary ID="vsSummaryImages" runat="server" CssClass="errorSummary"
                                                            ValidationGroup="PropertyGroupImagesServer" DisplayMode="List" />
                                                        <div class="errorSummary hidden" validationgroup="PropertyGroupImages">
                                                            <center>
                                                                Please upload a valid image file (only *.jpg and *.jpeg files are allowed).
                                                            </center>
                                                        </div>
                                                        <div class="updateMessage">
                                                            <%switch (Action)
                                                              {
                                                                  case "imageupdated":
                                                                      ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Image updated successfully!','success');", true);
                                                                      break;
                                                                  case "imagecreated":
                                                                      ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Image uploaded successfully!','success');", true);
                                                                      break;
                                                                  case "imagedeleted":
                                                                      ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Image deleted successfully!','success');", true);
                                                                      break;
                                                                  default:
                                                                      break;
                                                              } %>
                                                        </div>
                                                        <div class="form-horizontal">
                                                            <asp:MultiView ActiveViewIndex="0" runat="server" ID="mvEditImage">
                                                                <asp:View runat="server" ID="viewNewImage">
                                                                    <div class="form-group">
                                                                        <strong>Please note:</strong> Your photos will be displayed on this website in a <strong>landscape</strong> format, so for best results try to use landscape photos.  For best quality make sure you photos are large and high definition. The perfect image size and proportion is approximately 1000 pixels wide by 660 pixels tall.
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="col-sm-2 control-label">Select a photo:</label>
                                                                        <div class="col-sm-4">
                                                                            <asp:FileUpload ID="fuImageUpload" CssClass="btn btn-outline btn-success" type="file" Width="100%" runat="server" />
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Image is required" ControlToValidate="fuImageUpload" runat="server"
                                                                                Display="None" ValidationGroup="PropertyGroupImages" />
                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator14" ErrorMessage="Invalid photo type" ControlToValidate="fuImageUpload" Display="None" runat="server" ValidationGroup="PropertyGroupImages" ValidationExpression="^.*\.(jpg|JPG|jpeg|JPEG|)$" />
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                        </div>
                                                                        <div class="hr-line-dashed"></div>
                                                                    </div>
                                                                </asp:View>
                                                                <asp:View runat="server" ID="viewEditingImage">
                                                                    <div class="form-group">
                                                                        <label class="col-sm-2 control-label">Image:</label><div class="col-sm-5">
                                                                            <asp:Image ID="imgImageToEdit" runat="server" Style="width: 100%; margin: 7px 0 10px 6px;" />
                                                                        </div>
                                                                        <div class="col-sm-5">
                                                                        </div>
                                                                    </div>
                                                                    <div class="hr-line-dashed"></div>
                                                                </asp:View>
                                                            </asp:MultiView>
                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">Description:</label><div class="col-sm-10">
                                                                    <asp:TextBox runat="server" ID="txtImageDescription" TextMode="MultiLine" CssClass="form-control" MaxLength="255" Style="resize: none;" />
                                                                </div>
                                                            </div>
                                                            <div class="hr-line-dashed"></div>
                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">Lead photo:</label><div class="col-sm-10">
                                                                    <asp:CheckBox ID="chkImageAsLead" runat="server" />
                                                                </div>
                                                            </div>
                                                            <div class="hr-line-dashed"></div>
                                                            <asp:MultiView ActiveViewIndex="0" runat="server" ID="mvImageUpload">
                                                                <asp:View runat="server" ID="viewUpload">
                                                                    <div class="form-group">
                                                                        <div class="col-sm-4 col-sm-offset-2">
                                                                            <asp:Button Text="Upload this photo" runat="server" CssClass="btn btn-primary" type="submit" ValidationGroup="PropertyGroupImages" OnClientClick="formChangesMade = false;" ID="btnImageUpload" OnClick="btnImageUpload_Click" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="hr-line-dashed"></div>
                                                                </asp:View>
                                                                <asp:View runat="server" ID="viewEdit">
                                                                    <div class="form-group">
                                                                        <div class="col-sm-4 col-sm-offset-2">
                                                                            <asp:Button Text="Save" runat="server" CssClass="btn btn-primary" type="submit" ValidationGroup="PropertyGroupImages" OnClientClick="formChangesMade = false;" ID="btnImageSave" OnClick="btnImageSave_Click" />
                                                                            <a class="btn btn-w-m btn-default" type="submit" href="<%=ResolveUrl(String.Format("~/property-management/{0}#tabs-4", PropertyID)) %>" style="margin-left: 5px;">Cancel</a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="hr-line-dashed"></div>
                                                                </asp:View>
                                                            </asp:MultiView>
                                                            <div class="galleryWrapper">
                                                                <asp:ListView runat="server" ID="lvImages">
                                                                    <ItemTemplate>
                                                                        <div class="thumbnail">
                                                                            <img src='<%# ResolveUrl(Eval("GUID", "~/property-image/{0}?w=150&h=100")) %>' alt="thumbnail" />
                                                                            <asp:Button Text="Delete" runat="server" OnClientClick="return confirm('Are you sure?');" CssClass="btn btn-w-m btn-danger" type="submit" CommandArgument='<%#Eval("ID") %>' OnClick="btnDeleteImage_Click" ID="btnDeleteImage" ToolTip="Delete" />
                                                                            <a id="A4" class="btn btn-info" type="submit" href='<%#String.Format("~/property-management/{0}?guid={1}#tabs-4", PropertyID, Eval("GUID")) %>' runat="server"><i class="fa fa-paste"></i>Edit</a>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:ListView>
                                                                <div class="imagePopup" style="display: none;" id="imagePopup">
                                                                    <img id="Img1" src="#" alt="image detail" class="imageDetail" runat="server" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="tabs-5" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="ibox float-e-margins">
                                                    <div class="ibox-title">
                                                        <h5>Diary</h5>
                                                        <div class="ibox-tools">
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content padding" style="display: block;">
                                                        <asp:ScriptManager ID="ScriptManager_Grid" runat="server"></asp:ScriptManager>
                                                        <div class="updateMessage">
                                                            <%switch (Action)
                                                              {
                                                                  case "DiaryEntryupdated":
                                                                      ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Diary Entry updated successfully!','success');", true);
                                                                      break;
                                                                  case "DiaryEntrycreated":
                                                                      ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Diary Entry created successfully!','success');", true);
                                                                      break;
                                                                  case "DiaryEntrydeleted":
                                                                      ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Diary Entry deleted successfull!','success');", true);
                                                                      break;
                                                                  default:
                                                                      break;
                                                              } %>
                                                        </div>
                                                        <div class="table-responsive">
                                                            <asp:GridView runat="server" ID="GridView_tPropertyDiaryEntry" Width="100%" DataKeyNames="intDiaryEntry" DataSourceID="SqlDataSource_tPropertyDiaryEntry" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" PageSize="10">
                                                                <EmptyDataTemplate>
                                                                    <center>No diary entries yet...</center>
                                                                </EmptyDataTemplate>

                                                                <Columns>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>
                                                                            <table width="100%" style="margin: 0 !important;">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th style="width: 20%">Date Entry</th>
                                                                                        <th style="width: 15%">User</th>
                                                                                        <th style="width: 40%">Notes</th>
                                                                                        <th style="width: 13%">Delete</th>
                                                                                        <th style="width: 12%">Edit</th>
                                                                                    </tr>
                                                                                </thead>
                                                                            </table>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <table width="100%" class="itemTable" style="margin: 0 !important;">
                                                                                <tr>
                                                                                    <td style="width: 20%">
                                                                                        <asp:Label CssClass="" runat="server" ID="Label_datEntry" Text='<%# String.Format("{0:ddd, dd-MMM-yyyy HH:mm}",Eval("datEntry")) %>'></asp:Label></td>
                                                                                    <td style="width: 15%">
                                                                                        <asp:Label CssClass="" runat="server" ID="Label_chrUser" Text='<%# Eval("chrUser") %>'></asp:Label></td>
                                                                                    <td style="width: 40%">
                                                                                        <asp:Label CssClass="" runat="server" ID="Label_chrNotes" Text='<%# Eval("chrNotes").ToString().Length>=50?Eval("chrNotes").ToString().Substring(0,50)+ "...":Eval("chrNotes").ToString() %>'></asp:Label></td>
                                                                                    <td style="width: 13%">
                                                                                        <asp:LinkButton ID="Button_Delete" runat="server" Text="Delete" OnCommand="Button_Delete_Command" CommandArgument='<%# Eval("intDiaryEntry") %>' OnClientClick="return confirm('Are you sure?');" Font-Names="Tahoma" Font-Size="Small" />
                                                                                    </td>
                                                                                    <td style="width: 12%">
                                                                                        <asp:LinkButton ID="Button_Edit" runat="server" PostBackUrl='<%#"PropertyDiary.aspx?dID="+Eval("intDiaryEntry")+"&pID="+Eval("intPropertyID")%>' Text="Edit" Font-Names="Tahoma" Font-Size="Small" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                        <asp:SqlDataSource ID="SqlDataSource_tPropertyDiaryEntry" runat="server" SelectCommand=" SELECT [intDiaryEntry]
,[datEntry]
,[chrUser]
,[chrNotes]
,[intPropertyID]
FROM [tPropertyDiaryEntry] WHERE intPropertyID = @propertyID order by datEntry desc"
                                                            ConnectionString="<%$ ConnectionStrings:SQLCONN %>">
                                                            <SelectParameters>
                                                                <asp:QueryStringParameter QueryStringField="propertyID" Name="propertyID" Type="Int32" />
                                                            </SelectParameters>
                                                        </asp:SqlDataSource>
                                                    </div>
                                                    <br />
                                                    <div class="form-horizontal">
                                                        <div class="hr-line-dashed"></div>
                                                        <div class="form-group">
                                                            <div class="col-sm-4 col-sm-offset-2">
                                                                <asp:Button Text="Create" runat="server" ID="LinkButton_CreateDiary" CssClass="btn btn-primary" type="submit" OnClick="LinkButton_CreateDiary_Click" />
                                                                <asp:LinkButton ID="Button_Print" runat="server" CssClass="btn btn-outline btn-success" OnClick="Button_Print_Click" Text="Print Preview" Font-Names="Tahoma" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="tabs-6" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="ibox float-e-margins">
                                                    <div class="ibox-title">
                                                        <h5>Open Viewings</h5>
                                                        <div class="ibox-tools">
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content padding" style="display: block;">
                                                        <div class="updateMessage">
                                                            <%switch (Action)
                                                              {
                                                                  case "OpenViewingIncorrectDateSelected":
                                                                      ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Selected Date is invalid!','error');", true);
                                                                      break;
                                                                  case "OpenViewingEqualFromTo":
                                                                      ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage(' Start time and end time is invalid!','error');", true);
                                                                      break;
                                                                  case "OpenViewingExist":
                                                                      ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage(' An open viewing during this time already exists.  Please Select another date and time.','warning');", true);
                                                                      break;
                                                                  case "OpenViewingcreated":
                                                                      ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Open Viewing created successfully!','success');", true);

                                                                      break;
                                                                  case "OpenViewingdeleted":
                                                                      ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Open Viewing deleted successfully!','success');", true);
                                                                      break;
                                                                  default:
                                                                      break;
                                                              } %>
                                                        </div>
                                                        <div class="form-horizontal">
                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">Date</label>
                                                                <div class="col-sm-4">
                                                                    <div id="data_1" class="form-group">
                                                                        <div class="input-group date">
                                                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                                            <asp:TextBox runat="server" MaxLength="400" ID="TextBox_datViewing" CssClass="form-control" ValidationGroup="Validation_OpenViewings" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <%--                                                                        <asp:RequiredFieldValidator ErrorMessage="Value is required" ControlToValidate="TextBox_datViewing" Display="None" ValidationGroup="Validation_OpenViewings" runat="server" ID="RequiredFieldValidator_datViewing" ClientIDMode="Static" />
                                                                        <asp:CompareValidator ErrorMessage="Invalid date" ControlToValidate="TextBox_datViewing" runat="server" Display="None"
                                                                            ValidationGroup="Validation_OpenViewings" Type="Date" Operator="DataTypeCheck" ID="CompareValidator_datViewing" ClientIDMode="Static" />--%>
                                                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator_datViewing" runat="server" ControlToValidate="TextBox_datViewing" Display="Dynamic" ErrorMessage="لطفا تاریخ خود را انتخاب کنید" SetFocusOnError="True" ValidationGroup="Validation_OpenViewings" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    &nbsp;<asp:RegularExpressionValidator ID="RegularExpressionValidator_datViewing" runat="server" ControlToValidate="TextBox_datViewing" Display="Dynamic" ErrorMessage="تاریخ را مانند 1356/02/09 وارد نمایید" SetFocusOnError="True" ValidationExpression="\d{4}(/)\d{2}(/)\d{2}" ValidationGroup="Validation_OpenViewings" ForeColor="Red"></asp:RegularExpressionValidator>--%></td>
                                                                </div>
                                                            </div>
                                                            <div class="hr-line-dashed"></div>
                                                            <div class="form-group">

                                                                <label class="col-sm-2 control-label">Time from</label>
                                                                <div class="col-sm-4">
                                                                    <asp:UpdatePanel ID="UpdatePanel_OpenViewings_TimeFrom" runat="server">
                                                                        <ContentTemplate>
                                                                            <asp:DropDownList ID="DropDownList_timTimeFrom" runat="server" CssClass="form-control m-b"
                                                                                OnSelectedIndexChanged="DropDownList_timTimeFrom_SelectedIndexChanged" AutoPostBack="true">
                                                                                <asp:ListItem Value="06:00">06:00</asp:ListItem>
                                                                                <asp:ListItem Value="06:15">06:15</asp:ListItem>
                                                                                <asp:ListItem Value="06:30">06:30</asp:ListItem>
                                                                                <asp:ListItem Value="06:45">06:45</asp:ListItem>
                                                                                <asp:ListItem Value="07:00">07:00</asp:ListItem>
                                                                                <asp:ListItem Value="07:15">07:15</asp:ListItem>
                                                                                <asp:ListItem Value="07:30">07:30</asp:ListItem>
                                                                                <asp:ListItem Value="07:45">07:45</asp:ListItem>
                                                                                <asp:ListItem Value="08:00">08:00</asp:ListItem>
                                                                                <asp:ListItem Value="08:15">08:15</asp:ListItem>
                                                                                <asp:ListItem Value="08:30">08:30</asp:ListItem>
                                                                                <asp:ListItem Value="08:45">08:45</asp:ListItem>
                                                                                <asp:ListItem Value="09:00">09:00</asp:ListItem>
                                                                                <asp:ListItem Value="09:15">09:15</asp:ListItem>
                                                                                <asp:ListItem Value="09:30">09:30</asp:ListItem>
                                                                                <asp:ListItem Value="09:45">09:45</asp:ListItem>
                                                                                <asp:ListItem Value="10:00">10:00</asp:ListItem>
                                                                                <asp:ListItem Value="10:15">10:15</asp:ListItem>
                                                                                <asp:ListItem Value="10:30">10:30</asp:ListItem>
                                                                                <asp:ListItem Value="10:45">10:45</asp:ListItem>
                                                                                <asp:ListItem Value="11:00">11:00</asp:ListItem>
                                                                                <asp:ListItem Value="11:15">11:15</asp:ListItem>
                                                                                <asp:ListItem Value="11:30">11:30</asp:ListItem>
                                                                                <asp:ListItem Value="11:45">11:45</asp:ListItem>
                                                                                <asp:ListItem Value="12:00">12:00</asp:ListItem>
                                                                                <asp:ListItem Value="12:15">12:15</asp:ListItem>
                                                                                <asp:ListItem Value="12:30">12:30</asp:ListItem>
                                                                                <asp:ListItem Value="12:45">12:45</asp:ListItem>
                                                                                <asp:ListItem Value="13:00">13:00</asp:ListItem>
                                                                                <asp:ListItem Value="13:15">13:15</asp:ListItem>
                                                                                <asp:ListItem Value="13:30">13:30</asp:ListItem>
                                                                                <asp:ListItem Value="13:45">13:45</asp:ListItem>
                                                                                <asp:ListItem Value="14:00">14:00</asp:ListItem>
                                                                                <asp:ListItem Value="14:15">14:15</asp:ListItem>
                                                                                <asp:ListItem Value="14:30">14:30</asp:ListItem>
                                                                                <asp:ListItem Value="14:45">14:45</asp:ListItem>
                                                                                <asp:ListItem Value="15:00">15:00</asp:ListItem>
                                                                                <asp:ListItem Value="15:15">15:15</asp:ListItem>
                                                                                <asp:ListItem Value="15:30">15:30</asp:ListItem>
                                                                                <asp:ListItem Value="15:45">15:45</asp:ListItem>
                                                                                <asp:ListItem Value="16:00">16:00</asp:ListItem>
                                                                                <asp:ListItem Value="16:15">16:15</asp:ListItem>
                                                                                <asp:ListItem Value="16:30">16:30</asp:ListItem>
                                                                                <asp:ListItem Value="16:45">16:45</asp:ListItem>
                                                                                <asp:ListItem Value="17:00">17:00</asp:ListItem>
                                                                                <asp:ListItem Value="17:15">17:15</asp:ListItem>
                                                                                <asp:ListItem Value="17:30">17:30</asp:ListItem>
                                                                                <asp:ListItem Value="17:45">17:45</asp:ListItem>
                                                                                <asp:ListItem Value="18:00">18:00</asp:ListItem>
                                                                                <asp:ListItem Value="18:15">18:15</asp:ListItem>
                                                                                <asp:ListItem Value="18:30">18:30</asp:ListItem>
                                                                                <asp:ListItem Value="18:45">18:45</asp:ListItem>
                                                                                <asp:ListItem Value="19:00">19:00</asp:ListItem>
                                                                                <asp:ListItem Value="19:15">19:15</asp:ListItem>
                                                                                <asp:ListItem Value="19:30">19:30</asp:ListItem>
                                                                                <asp:ListItem Value="19:45">19:45</asp:ListItem>
                                                                                <asp:ListItem Value="20:00">20:00</asp:ListItem>
                                                                                <asp:ListItem Value="20:15">20:15</asp:ListItem>
                                                                                <asp:ListItem Value="20:30">20:30</asp:ListItem>
                                                                                <asp:ListItem Value="20:45">20:45</asp:ListItem>
                                                                                <asp:ListItem Value="21:00">21:00</asp:ListItem>
                                                                                <asp:ListItem Value="21:15">21:15</asp:ListItem>
                                                                                <asp:ListItem Value="21:30">21:30</asp:ListItem>
                                                                                <asp:ListItem Value="21:45">21:45</asp:ListItem>
                                                                                <asp:ListItem Value="22:00">22:00</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                </div>
                                                            </div>
                                                            <div class="hr-line-dashed"></div>
                                                            <div class="form-group">

                                                                <label class="col-sm-2 control-label">Time to</label>
                                                                <div class="col-sm-4">
                                                                    <asp:UpdatePanel ID="UpdatePanel_OpenViewings_DropDownTo" runat="server">
                                                                        <ContentTemplate>

                                                                            <asp:DropDownList ID="DropDownList_timTimeTo" runat="server" CssClass="form-control m-b">
                                                                                <asp:ListItem Value="06:00">06:00</asp:ListItem>
                                                                                <asp:ListItem Value="06:15">06:15</asp:ListItem>
                                                                                <asp:ListItem Value="06:30">06:30</asp:ListItem>
                                                                                <asp:ListItem Value="06:45">06:45</asp:ListItem>
                                                                                <asp:ListItem Value="07:00" Selected="True">07:00</asp:ListItem>
                                                                                <asp:ListItem Value="07:15">07:15</asp:ListItem>
                                                                                <asp:ListItem Value="07:30">07:30</asp:ListItem>
                                                                                <asp:ListItem Value="07:45">07:45</asp:ListItem>
                                                                                <asp:ListItem Value="08:00">08:00</asp:ListItem>
                                                                                <asp:ListItem Value="08:15">08:15</asp:ListItem>
                                                                                <asp:ListItem Value="08:30">08:30</asp:ListItem>
                                                                                <asp:ListItem Value="08:45">08:45</asp:ListItem>
                                                                                <asp:ListItem Value="09:00">09:00</asp:ListItem>
                                                                                <asp:ListItem Value="09:15">09:15</asp:ListItem>
                                                                                <asp:ListItem Value="09:30">09:30</asp:ListItem>
                                                                                <asp:ListItem Value="09:45">09:45</asp:ListItem>
                                                                                <asp:ListItem Value="10:00">10:00</asp:ListItem>
                                                                                <asp:ListItem Value="10:15">10:15</asp:ListItem>
                                                                                <asp:ListItem Value="10:30">10:30</asp:ListItem>
                                                                                <asp:ListItem Value="10:45">10:45</asp:ListItem>
                                                                                <asp:ListItem Value="11:00">11:00</asp:ListItem>
                                                                                <asp:ListItem Value="11:15">11:15</asp:ListItem>
                                                                                <asp:ListItem Value="11:30">11:30</asp:ListItem>
                                                                                <asp:ListItem Value="11:45">11:45</asp:ListItem>
                                                                                <asp:ListItem Value="12:00">12:00</asp:ListItem>
                                                                                <asp:ListItem Value="12:15">12:15</asp:ListItem>
                                                                                <asp:ListItem Value="12:30">12:30</asp:ListItem>
                                                                                <asp:ListItem Value="12:45">12:45</asp:ListItem>
                                                                                <asp:ListItem Value="13:00">13:00</asp:ListItem>
                                                                                <asp:ListItem Value="13:15">13:15</asp:ListItem>
                                                                                <asp:ListItem Value="13:30">13:30</asp:ListItem>
                                                                                <asp:ListItem Value="13:45">13:45</asp:ListItem>
                                                                                <asp:ListItem Value="14:00">14:00</asp:ListItem>
                                                                                <asp:ListItem Value="14:15">14:15</asp:ListItem>
                                                                                <asp:ListItem Value="14:30">14:30</asp:ListItem>
                                                                                <asp:ListItem Value="14:45">14:45</asp:ListItem>
                                                                                <asp:ListItem Value="15:00">15:00</asp:ListItem>
                                                                                <asp:ListItem Value="15:15">15:15</asp:ListItem>
                                                                                <asp:ListItem Value="15:30">15:30</asp:ListItem>
                                                                                <asp:ListItem Value="15:45">15:45</asp:ListItem>
                                                                                <asp:ListItem Value="16:00">16:00</asp:ListItem>
                                                                                <asp:ListItem Value="16:15">16:15</asp:ListItem>
                                                                                <asp:ListItem Value="16:30">16:30</asp:ListItem>
                                                                                <asp:ListItem Value="16:45">16:45</asp:ListItem>
                                                                                <asp:ListItem Value="17:00">17:00</asp:ListItem>
                                                                                <asp:ListItem Value="17:15">17:15</asp:ListItem>
                                                                                <asp:ListItem Value="17:30">17:30</asp:ListItem>
                                                                                <asp:ListItem Value="17:45">17:45</asp:ListItem>
                                                                                <asp:ListItem Value="18:00">18:00</asp:ListItem>
                                                                                <asp:ListItem Value="18:15">18:15</asp:ListItem>
                                                                                <asp:ListItem Value="18:30">18:30</asp:ListItem>
                                                                                <asp:ListItem Value="18:45">18:45</asp:ListItem>
                                                                                <asp:ListItem Value="19:00">19:00</asp:ListItem>
                                                                                <asp:ListItem Value="19:15">19:15</asp:ListItem>
                                                                                <asp:ListItem Value="19:30">19:30</asp:ListItem>
                                                                                <asp:ListItem Value="19:45">19:45</asp:ListItem>
                                                                                <asp:ListItem Value="20:00">20:00</asp:ListItem>
                                                                                <asp:ListItem Value="20:15">20:15</asp:ListItem>
                                                                                <asp:ListItem Value="20:30">20:30</asp:ListItem>
                                                                                <asp:ListItem Value="20:45">20:45</asp:ListItem>
                                                                                <asp:ListItem Value="21:00">21:00</asp:ListItem>
                                                                                <asp:ListItem Value="21:15">21:15</asp:ListItem>
                                                                                <asp:ListItem Value="21:30">21:30</asp:ListItem>
                                                                                <asp:ListItem Value="21:45">21:45</asp:ListItem>
                                                                                <asp:ListItem Value="22:00">22:00</asp:ListItem>
                                                                                <asp:ListItem Value="22:15">22:15</asp:ListItem>
                                                                                <asp:ListItem Value="22:30">22:30</asp:ListItem>
                                                                                <asp:ListItem Value="22:45">22:45</asp:ListItem>
                                                                                <asp:ListItem Value="23:00">23:00</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                </div>
                                                            </div>
                                                            <div class="hr-line-dashed"></div>
                                                            <div class="form-group">
                                                                <div class="col-sm-4 col-sm-offset-2">
                                                                    <asp:Button ID="Button_AddPropertyViewing" runat="server" OnClick="Button_AddPropertyViewing_Click" Text="Add" ValidationGroup="Validation_OpenViewings" CssClass="btn btn-primary" type="submit" />
                                                                </div>
                                                            </div>
                                                            <div class="hr-line-dashed"></div>
                                                            <h3>Existing Viewings</h3>
                                                            <div class="table-responsive">
                                                                <asp:GridView runat="server" ID="GridView_PropertyViewing" Width="100%" DataKeyNames="intPropertyViewingID" DataSourceID="SqlDataSource_PropertyViewing" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True">
                                                                    <EmptyDataTemplate>
                                                                        <center>No open viewings yet...</center>
                                                                    </EmptyDataTemplate>
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>
                                                                                <table width="100%" class="itemTable" style="margin: 0 !important;">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th style="width: 40%;">Date</th>
                                                                                            <th style="width: 20%;">Time From</th>
                                                                                            <th style="width: 20%;">Time To</th>
                                                                                            <th style="width: 20%;">Delete</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                </table>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <table width="100%" style="margin: 0 !important;">
                                                                                    <tr>
                                                                                        <td style="width: 40%;">
                                                                                            <asp:HiddenField ID="HiddenField_datViewing" runat="server" Value='<%# Eval("datViewing") %>' />
                                                                                            <asp:Label CssClass="" runat="server" ID="Label_datViewing" Text='<%# String.Format("{0:ddd, dd-MMM-yyyy}",Eval("datViewing")) %>'></asp:Label></td>
                                                                                        <td style="width: 20%;">
                                                                                            <asp:Label CssClass="" runat="server" ID="Label_timTimeFrom" Text='<%# Eval("timTimeFrom") %>'></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 20%;">
                                                                                            <asp:Label CssClass="" runat="server" ID="Label_timTimeTo" Text='<%# Eval("timTimeTo") %>'></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 20%;">
                                                                                            <asp:LinkButton ID="Button_DeletePropertyViewing" runat="server" CssClass="ignoreChanges" Text="Delete" OnCommand="Button_DeletePropertyViewing_Command" CommandArgument='<%# Eval("intPropertyViewingID") %>' OnClientClick="return confirm('Are you sure?');" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>

                                                            </div>

                                                            <asp:SqlDataSource ID="SqlDataSource_PropertyViewing" runat="server" SelectCommand="SELECT
 [intPropertyViewingID] ,
 [intPropertyID] ,
 [datViewing] ,
 [timTimeFrom] ,
 [timTimeTo] 
 FROM [dbo].[tPropertyViewing] where intPropertyID = @propertyID order by datViewing desc"
                                                                ConnectionString="<%$ ConnectionStrings:SQLCONN %>">
                                                                <SelectParameters>
                                                                    <asp:QueryStringParameter QueryStringField="propertyID" Name="propertyID" Type="Int32" />
                                                                </SelectParameters>
                                                            </asp:SqlDataSource>


                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:View>
        </asp:MultiView>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="placeholderFooter" runat="server">
    <script src="<%=ResolveUrl("~/js_inispinia/dataTableCustom.js")%>" type="text/javascript"></script>
    <!-- Chosen -->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/chosen/chosen.jquery.js")%>" type="text/javascript"></script>
    <!-- JSKnob -->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/jsKnob/jquery.knob.js")%>" type="text/javascript"></script>
    <!-- Input Mask-->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/jasny/jasny-bootstrap.min.js")%>" type="text/javascript"></script>
    <!-- Data picker -->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/datapicker/bootstrap-datepicker.js")%>" type="text/javascript"></script>
    <!-- Clock picker -->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/clockpicker/clockpicker.js")%>" type="text/javascript"></script>
    <!-- Date range use moment.js same as full calendar plugin -->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/fullcalendar/moment.min.js")%>" type="text/javascript"></script>
    <!-- Date range picker -->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/daterangepicker/daterangepicker.js")%>" type="text/javascript"></script>
    <!-- Select2 -->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/select2/select2.full.min.js")%>" type="text/javascript"></script>
    <!-- custom-plugin -->
    <script src="<%=ResolveUrl("~/js_inispinia/custom-plugin.js")%>" type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {

            // -- get hash
            var hash = window.location.hash;

            // -- check for changes
            //window.onbeforeunload = confirmBeforeUnload;
            checkForUnsavedData();

            $('.ignoreChanges').click(function () {
                formChangesMade = false;
            })
            $('.ignoreChanges').change(function () {
                formChangesMade = false;
            })

            $('.btnSaveAttribute').click(function () {
                var initialDataType = $('.ddlAttributeType option:selected').attr('data-type');
                if (initialDataType == 'String') {
                    var value = $('[id$="_hdnTextAttributeStringValue"]').val();
                    if (value.length == 0) {
                        $('.attributeSummary').removeClass('hidden');
                        $('.ui-autocomplete-input').addClass('error');
                        return false;
                    } else {
                        $('.attributeSummary').addClass('hidden');
                    }
                }

            });

            // -- load attribute types if String type is default
            var attributeType = $('.ddlAttributeType option:selected').attr('data-type');
            loadStringAttributes(attributeType);

            $('.ddlAttributeType').change(function () {
                var attributeType = $('.ddlAttributeType option:selected').attr('data-type');
                loadStringAttributes(attributeType);
            });

            // -- load tabs
            //-- hide extra tabs if creating new item
            var propertyRefFieldVisible = $('.previewLink').length;
            if (!propertyRefFieldVisible) {
                document.getElementById("htab2").className = "disabled";
                document.getElementById("htab3").className = "disabled";
                document.getElementById("htab4").className = "disabled";
                document.getElementById("htab5").className = "disabled";
                document.getElementById("htab6").className = "disabled";
                $("#atab2").attr("href", "#");
                $("#atab3").attr("href", "#");
                $("#atab4").attr("href", "#");
                $("#atab5").attr("href", "#");
                $("#atab6").attr("href", "#");
            }

            //if (hash) {
            //    var tabIndex = hash.substr(hash.length - 1, 1);
            //    tabIndex = tabIndex - 1;
            //    $('#tabs').tabs('option', 'active', tabIndex);
            //}
            $('#imagePopup').dialog({
                resizable: false,
                modal: true,
                width: 'auto',
                autoOpen: false,
                title: 'Image Detail',
                draggable: false
            });
            $('.thumbnail img').click(function () {
                var url = $(this).attr('src');
                url = url.substring(0, url.lastIndexOf('?'));
                $('.imageDetail').attr('src', url);
                $('#imagePopup').dialog('open');
            });

            $('body').on('click', '.ui-widget-overlay', function () {
                $('#imagePopup').dialog('close');
            });

            //-- prepare attribute plugins
            $('.date').datepicker({ dateFormat: 'dd/mm/yy' });

            var initialDataType = $('.ddlAttributeType option:selected').attr('data-type');
            changeControlType(initialDataType);

            $('.autocomplete').combobox();


            $('.ddlAttributeType').change(function () {
                var dataType = $('option:selected', this).attr('data-type');
                changeControlType(dataType);
            });


            // load counties
            $('.ddlCountry').change(function () {
                var val = $(this).val();
                loadCountyList('.ddlCounty', val, '-- Select county --');
            });

            $('.ddlVendorCountry').change(function () {
                var val = $(this).val();
                loadCountyList('.ddlVendorCounty', val, '-- Select county --');
            });

            $('#hdnCounty').val($('.ddlCounty').val());
            $('#hdnVendorCounty').val($('.ddlVendorCounty').val());

            $('.ddlCounty').change(function () {
                $('#hdnCounty').val($(this).val());
            });

            $('.ddlVendorCounty').change(function () {
                $('#hdnVendorCounty').val($(this).val());
            });

            //$('.itemTable tr').hover(function () {
            //    $(this).addClass('activeRow');
            //}, function () {
            //    $(this).removeClass('activeRow');
            //});

        });

        var formChangesMade = false;

        function checkForUnsavedData() {
            $('select, input, textarea').change(function () {
                formChangesMade = true;
            });
        }

        function confirmBeforeUnload() {
            if (formChangesMade) {
                return 'All unsaved data will be lost!';
            }
        }

        function loadStringAttributes(attributeType) {
            if (attributeType == 'String') {
                var id = $('.ddlAttributeType option:selected').val();
                console.log(id);
                javaLib.ajaxCall(jLib.resolveUrl('~/Handlers/CommonHandler.ashx?action=load_ditinct_attributes'), 'GET', { 'attributeTypeID': id },
                   function (data) {
                       $.each(data, function (val, text) {
                           $('#ddlTextAttributeStringValue').append($('<option/>').val(text).text(text));
                       });
                   }, function (error) {
                       console.log(error);
                   });
            }
        }

        function changeControlType(dataType) {
            switch (dataType) {
                case 'Boolean':
                    console.log('bool');
                    $('.attributeValueBool').removeClass('hidden');
                    $('.attributeValueNumeric').addClass('hidden');
                    $('.attributeValueDate').addClass('hidden');
                    $('.attributeValueString').addClass('hidden');
                    //ValidatorEnable(document.getElementById("rfvAttributeStringValue"), false);
                    ValidatorEnable(document.getElementById("rfvAttributeDateValue"), false);
                    ValidatorEnable(document.getElementById("cvAttributeDateValue"), false);
                    ValidatorEnable(document.getElementById("rfvAttributeNumericValue"), false);
                    ValidatorEnable(document.getElementById("revAttributeNumericValue"), false);
                    break;
                case 'Numeric':
                    console.log('number');
                    $('.attributeValueNumeric').removeClass('hidden');
                    $('.attributeValueBool').addClass('hidden');
                    $('.attributeValueDate').addClass('hidden');
                    $('.attributeValueString').addClass('hidden');
                    //ValidatorEnable(document.getElementById("rfvAttributeStringValue"), false);
                    ValidatorEnable(document.getElementById("rfvAttributeDateValue"), false);
                    ValidatorEnable(document.getElementById("cvAttributeDateValue"), false);
                    ValidatorEnable(document.getElementById("rfvAttributeNumericValue"), true);
                    ValidatorEnable(document.getElementById("revAttributeNumericValue"), true);
                    break;
                case 'Date':
                    console.log('date');
                    $('.attributeValueDate').removeClass('hidden');
                    $('.attributeValueBool').addClass('hidden');
                    $('.attributeValueNumeric').addClass('hidden');
                    $('.attributeValueString').addClass('hidden');
                    //ValidatorEnable(document.getElementById("rfvAttributeStringValue"), false);
                    ValidatorEnable(document.getElementById("rfvAttributeDateValue"), true);
                    ValidatorEnable(document.getElementById("cvAttributeDateValue"), true);
                    ValidatorEnable(document.getElementById("rfvAttributeNumericValue"), false);
                    ValidatorEnable(document.getElementById("revAttributeNumericValue"), false);
                    break;
                case 'String':
                    console.log('string');
                    $('.attributeValueString').removeClass('hidden');
                    $('.attributeValueBool').addClass('hidden');
                    $('.attributeValueNumeric').addClass('hidden');
                    $('.attributeValueDate').addClass('hidden');
                    //ValidatorEnable(document.getElementById("rfvAttributeStringValue"), true);
                    ValidatorEnable(document.getElementById("rfvAttributeDateValue"), false);
                    ValidatorEnable(document.getElementById("cvAttributeDateValue"), false);
                    ValidatorEnable(document.getElementById("rfvAttributeNumericValue"), false);
                    ValidatorEnable(document.getElementById("revAttributeNumericValue"), false);
                    break;
                default:
            }
        }

        function loadCountyList(element, countryID, defaultText) {
            javaLib.ajaxCall(
                jLib.resolveUrl('~/Handlers/CommonHandler.ashx?action=get_county_list'),
                'GET',
                { 'country_id': countryID },
                function (data) {
                    $(element).empty();
                    $(element).append($('<option />').val('0').text(defaultText));
                    $.each(data, function (index, item) {
                        $(element).append($('<option />').val(item.ID).text(item.CountyName));
                    });
                }, function (error) {
                    consol.log(error);
                });
        }

        // -- autocomplete combobox
        (function ($) {
            $.widget("custom.combobox", {
                _create: function () {
                    this.wrapper = $("<span>")
                      .addClass("custom-combobox")
                      .insertAfter(this.element);

                    this.element.hide();
                    this._createAutocomplete();
                    this._createShowAllButton();
                },

                _createAutocomplete: function () {
                    var selected = this.element.children(":selected"),
                      value = selected.val() ? selected.text() : "";

                    this.input = $("<input>")
                      .appendTo(this.wrapper)
                      .val(value)
                      .attr("title", "")
                      .addClass("custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left")
                      .autocomplete({
                          delay: 0,
                          minLength: 0,
                          source: $.proxy(this, "_source")
                      })
                      .tooltip({
                          tooltipClass: "ui-state-highlight"
                      });

                    this._on(this.input, {
                        autocompleteselect: function (event, ui) {
                            console.log(ui.item.value);
                            $(this.input).parent().siblings('input').val(ui.item.value);
                            ui.item.option.selected = true;
                            this._trigger("select", event, {
                                item: ui.item.option
                            });
                        },

                        autocompletechange: "_removeIfInvalid"
                    });
                },

                _createShowAllButton: function () {
                    var input = this.input,
                      wasOpen = false;

                    $("<a>")
                      .attr("tabIndex", -1)
                      .attr("title", "Show All Items")
                      .tooltip()
                      .appendTo(this.wrapper)
                      .button({
                          icons: {
                              primary: "ui-icon-triangle-1-s"
                          },
                          text: false
                      })
                      .removeClass("ui-corner-all")
                      .addClass("custom-combobox-toggle ui-corner-right")
                      .mousedown(function () {
                          wasOpen = input.autocomplete("widget").is(":visible");
                      })
                      .click(function () {
                          input.focus();

                          // Close if already visible
                          if (wasOpen) {
                              return;
                          }

                          // Pass empty string as value to search for, displaying all results
                          input.autocomplete("search", "");
                      });
                },

                _source: function (request, response) {
                    var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                    response(this.element.children("option").map(function () {
                        var text = $(this).text();
                        if (this.value && (!request.term || matcher.test(text)))
                            return {
                                label: text,
                                value: text,
                                option: this
                            };
                    }));
                },

                _removeIfInvalid: function (event, ui) {

                    // Selected an item, nothing to do
                    if (ui.item) {
                        return;
                    }



                    // Search for a match (case-insensitive)
                    var value = this.input.val(),
                      valueLowerCase = value.toLowerCase(),
                      valid = false;

                    $(this.input).parent().siblings('input').val(value);

                    this.element.children("option").each(function () {
                        if ($(this).text().toLowerCase() === valueLowerCase) {
                            this.selected = valid = true;
                            return false;
                        }
                    });

                    // Found a match, nothing to do
                    if (valid) {
                        return;
                    }

                    //// Remove invalid value
                    //this.input
                    //  .val("")
                    //  .attr("title", value + " didn't match any item")
                    //  .tooltip("open");
                    //this.element.val("");
                    //this._delay(function () {
                    //    this.input.tooltip("close").attr("title", "");
                    //}, 2500);
                    //this.input.autocomplete("instance").term = "";
                },

                _destroy: function () {
                    this.wrapper.remove();
                    this.element.show();
                }
            });
        })(jQuery);

    </script>


</asp:Content>
