﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FLContentSiteAssist.Common;
using System.Data;
using System.IO;
using CMS.Helpers;
using FLContentSiteAssist.BusinessLogic.DataProcessing;
using FLContentSiteAssist.BusinessLogic;
using Common.Extensions;

namespace CMS.FluentControls
{
    public partial class PageImageEdit : System.Web.UI.UserControl
    {
        public int ImageID { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                Refresh_Screen();
            }

        }

        private void Refresh_Screen()
        {          
            string _rootpath = FluentContentSettings.WebsiteRoot;
            if (_rootpath.Substring(_rootpath.Length - 1, 1) != "/") { _rootpath += "/"; }
            PageImage _pageimage = new PageImage();

            ProcessGetPageImageByID _getimage = new ProcessGetPageImageByID();
            _pageimage.PageImageID = ImageID;
            _getimage.PageImage = _pageimage;
            _getimage.Invoke();

            btnSave.CommandArgument = ImageID.ToString();

            if (_getimage.ResultSet.Tables[0].Rows.Count > 0)
            {
                DataRow dr = _getimage.ResultSet.Tables[0].Rows[0];

                imageTitle.Text = dr["chrPageImageTitle"].ToString();
                imageCaption.Text = dr["chrPageImageCaption"].ToString();
                imageAlt.Text = dr["chrPageImageAlt"].ToString();
                imageText.Text = dr["chrPageImageText"].ToString();
                imageLinkURL.Text = dr["chrPageImageLinkURL"].ToString();
                image_instr.Text = ".jpg .png .gif (w:" + dr["intWidth"].ToString() + "px h:" + dr["intHeight"].ToString() + "px)";
                image_instr_thumb.Text = ".jpg .png .gif (w:" + dr["intThumbWidth"].ToString() + "px h:" + dr["intThumbHeight"].ToString() + "px)";
                if (dr["chrPageImageURL"].ToString() != "")
                {

                    image.ImageUrl = _rootpath + FluentContentSettings.ImageUploadFolder + "/" + dr["chrPageImageURL"].ToString();
                    deleteImage.Checked = false;
                    deleteImage.Visible = true;
                    deleteLabel.Visible = true;
                    image.Visible = true;
                }
                else
                {
                    deleteImage.Checked = false;
                    deleteImage.Visible = false;
                    deleteLabel.Visible = false;
                    image.Visible = false;
                }
                if (dr["chrPageImageThumbURL"].ToString() != "")
                {
                    thumbnailImage.ImageUrl = _rootpath + FluentContentSettings.ImageUploadFolder + "/" + dr["chrPageImageThumbURL"].ToString(); 
                    deleteThumbnailImage.Checked = false;
                    deleteThumbnailImage.Visible = true;
                    deleteThumbnailLabel.Visible = true;
                    thumbnailImage.Visible = true;
                }
                else
                {
                    deleteThumbnailImage.Checked = false;
                    deleteThumbnailImage.Visible = false;
                    deleteThumbnailLabel.Visible = false;
                    thumbnailImage.Visible = false;
                }


            }

        }

        protected void Update_Click(object sender, EventArgs e)
        {

            //string _rootpath = Request.Url.GetLeftPart(UriPartial.Authority) + HttpRuntime.AppDomainAppVirtualPath;
            string _rootpath = FluentContentSettings.WebsiteRoot;
            if (_rootpath.Substring(_rootpath.Length - 1, 1) != "/") { _rootpath += "/"; }
            string _imgurl = "";
            string _thumbimgurl = "";
          //  updateMessage.Visible = false;

            //First upload image
            if (ImageUpload.PostedFile.FileName != null && ImageUpload.PostedFile.FileName != "")
            {
                string pathfilename = ImageUpload.PostedFile.FileName;
                string filename = Path.GetFileName(ImageUpload.PostedFile.FileName);
                string localpath = FluentContentSettings.ImageUploadLocation + "\\";
                string localfilename = FluentContentSettings.ImageUploadLocation + "\\" + filename;
                //string localpath = AHCUtilities.GetImageUploadLocation + "\\";
                //string localfilename = AHCUtilities.GetImageUploadLocation + "\\" + filename;
                string filenameleft = Path.GetFileNameWithoutExtension(filename);
                string ext = Path.GetExtension(filename);
                if (overwriteImg.Checked)
                {
                    ImageUpload.PostedFile.SaveAs(localfilename);
                }
                else
                {
                    if (File.Exists(@localfilename))
                    {
                        //Find next available filename
                        for (int i = 1; i <= 1000; i++)
                        {
                            if (!File.Exists(@filenameleft + i.ToString() + ext))
                            {
                                localfilename = localpath + filenameleft + i.ToString() + ext;
                                break;
                            }
                        }
                    }
                    ImageUpload.PostedFile.SaveAs(localfilename);
                }                             

                image.ImageUrl = _rootpath + FluentContentSettings.ImageUploadFolder + "/" + Path.GetFileName(localfilename);
                image.Visible = true;
                _imgurl = Path.GetFileName(localfilename);
            }

            //errorMessage.Visible = false;
            //and the thumbnail
            if (ThumbnailUpload.PostedFile.FileName != null && ThumbnailUpload.PostedFile.FileName != "")
            {
                string pathfilename = ThumbnailUpload.PostedFile.FileName;
                string filename = Path.GetFileName(ThumbnailUpload.PostedFile.FileName);
                string localpath = FluentContentSettings.ImageUploadLocation + "\\";
                string localfilename = FluentContentSettings.ImageUploadLocation + "\\" + filename;
                string filenameleft = Path.GetFileNameWithoutExtension(filename);
                string ext = Path.GetExtension(filename);
                if (overwriteThumbnailImg.Checked)
                {
                    ThumbnailUpload.PostedFile.SaveAs(localfilename);
                }
                else
                {
                    if (File.Exists(@localfilename))
                    {
                        //Find next available filename
                        for (int i = 1; i <= 1000; i++)
                        {
                            if (!File.Exists(@filenameleft + i.ToString() + ext))
                            {
                                localfilename = localpath + filenameleft + i.ToString() + ext;
                                break;
                            }
                        }
                    }
                    ThumbnailUpload.PostedFile.SaveAs(localfilename);
                }

                thumbnailImage.ImageUrl = _rootpath + FluentContentSettings.ImageUploadFolder + "/" + Path.GetFileName(localfilename);
                thumbnailImage.Visible = true;
                _thumbimgurl = Path.GetFileName(localfilename);
            }

            //Now insert the rest of the data into the database
            ProcessUpdatePageImage updateimage = new ProcessUpdatePageImage();
            PageImage item = new PageImage();
            item.PageImageID = btnSave.CommandArgument.ToInt32();
            item.ImageURL = _imgurl;
            item.ImageThumbURL = _thumbimgurl;
            item.ImageTitle = imageTitle.Text;
            item.ImageText = imageText.Text;
            item.ImageAlt = imageAlt.Text;
            item.ImageCaption = imageCaption.Text;
            item.ImageLinkURL = imageLinkURL.Text;
            item.DeleteImage = deleteImage.Checked;
            item.DeleteThumbnailImage = deleteThumbnailImage.Checked;

            updateimage.PageImage = item;
            updateimage.Invoke();

            Refresh_Screen();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Successfully updated','success');", true);
        }
    }
}