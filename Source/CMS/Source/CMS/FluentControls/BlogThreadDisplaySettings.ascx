﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlogThreadDisplaySettings.ascx.cs"
    Inherits="CMS.FluentControls.BlogThreadDisplaySettings" %>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Edit Display Item</h5>
            </div>
            <div class="ibox-content padding" style="display: block;">
                <div class="form-horizontal">
                    <asp:HiddenField ID="itemDisplay" runat="server" />
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            Max number of items
                        </label>
                        <div class="col-sm-10">
                            <asp:TextBox ID="maxItems" CssClass="input_tiny" runat="server" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            Max number of items per page
                        </label>
                        <div class="col-sm-10">
                            <asp:TextBox ID="maxItemsPerPage" CssClass="input_tiny" runat="server" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            Order threads
                        </label>
                        <div class="col-sm-5">
                            <asp:RadioButton ID="date" Text="by date descending" GroupName="orderby" runat="server" />
                        </div>
                        <div class="col-sm-5">
                            <asp:RadioButton ID="datea" Text="by date ascending" GroupName="orderby" runat="server" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <asp:ImageButton runat="server" ID="update" OnClick="Update_Click"  CssClass="btn btn-info" type="submit" 
                                value="" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
