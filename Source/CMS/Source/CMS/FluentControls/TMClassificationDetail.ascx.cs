﻿using FluentContent.DAC;
using FluentContent.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Extensions;
using CMS.Helpers;
using FLContentSiteAssist.BusinessLogic;
using FluentContent.Entities.Helpers;
using System.Collections.Specialized;
using System.IO;
using System.Drawing.Imaging;
using System.Data;
using Common.Url;
using Common.Session;

namespace CMS.FluentControls
{
    public partial class TMClassificationDetail : System.Web.UI.UserControl
    {
        public int ClassificationID { get { return Request.GetQueryString<int>("classificationid", -1); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (ClassificationID != -1)
                {
                    FillScreen(ClassificationID);
                }
            }
        }

        private void FillScreen(int ClassificationID)
        {
            TMClassification tmc = DAC_TMClassification.GetClassificationByClassification(ClassificationID);
            if (tmc == null) return;
            txtclassificationname.Text = tmc.ClassificationTitle.ToString();
            txtClassificationDescription.Text = tmc.Classification.ToString();
            chk_oldbritish.Checked = tmc.OldBritish;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void Save()
        {
            if (!Page.IsValid) return;

            TMClassification tmc = new TMClassification();
            tmc.ClassificationTitle = txtclassificationname.Text.Trim();
            tmc.Classification = txtClassificationDescription.Text.Trim();
            tmc.OldBritish = chk_oldbritish.Checked;
            tmc.TMClassificationID = ClassificationID;

            if (ClassificationID > 0)
            {
                tmc = DAC_TMClassification.Update(tmc);
                if (tmc == null)
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Update failed!','warning');", true);
                else
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Classification successfully updated.','success');", true);
            }
            else
            {
                tmc = DAC_TMClassification.Insert(tmc);
                if (tmc == null)
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Create failed!','warning');", true);
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('New Classification successfully created.  You may now create another...','success');", true);
                    ClearPage();
                }
            }
        }

        private void ClearPage()
        {
            txtclassificationname.Text = string.Empty;
            txtClassificationDescription.Text = string.Empty;
            chk_oldbritish.Checked = false;
        }
    }
}