﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TMCountryAdditionalRequirementChoiceDetail.ascx.cs" Inherits="CMS.FluentControls.TMCountryAdditionalRequirementChoiceDetail" %>
<div class="form-horizontal">
    <asp:HiddenField ID="intCountryID" runat="server" />
    <div class="form-group">
        <label class="col-sm-2 control-label">Cost Type</label>
        <div class="col-sm-10">
            <asp:DropDownList ID="ddchoiceType" CssClass="form-control m-b" runat="server" AutoPostBack="false">
            </asp:DropDownList>
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Choice Text</label>
        <div class="col-sm-10">
             <asp:TextBox ID="txtChoiceText" CssClass="form-control" runat="server" TextMode="MultiLine"
                Rows="3" />
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <div class="col-sm-4 col-sm-offset-2">
            <asp:Button ID="btnSave" CssClass="btn btn-primary" type="submit" runat="server" OnClick="btnSave_Click" Text="Save" />
        </div>
    </div>
</div>
