﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TMClassificationClassProductDetail.ascx.cs" Inherits="CMS.FluentControls.TMClassificationClassProductDetail" %>
<div class="form-horizontal">
    <div class="errorSummary hidden" validationgroup="classificationclassproduct">
        <center>
                         <p>
                            Please correct the highlighted fields
                        </p>
                        </center>
    </div>
    <asp:ValidationSummary ID="vsErrorSummary" runat="server" DisplayMode="List" ValidationGroup="classificationclassproduct"
        CssClass="errorSummary" />
    <div class="form-group">
        <label class="col-sm-2 control-label">Product Name</label>
        <div class="col-sm-10">
            <asp:TextBox ID="txtproductname" ValidationGroup="classificationclassproduct" CssClass="form-control" runat="server" MaxLength="255" />
            <asp:RequiredFieldValidator SetFocusOnError="true" ID="rntxtproductname" Display="None" runat="server" ErrorMessage="Please fill out this field" ValidationGroup="classificationclassproduct"
                ControlToValidate="txtproductname"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-lg-2 control-label">Product Code</label>
        <div class="col-lg-10">
            <asp:TextBox ID="txtproductCode" CssClass="form-control" runat="server" MaxLength="255" />
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-lg-2 control-label">Product Number</label>
        <div class="col-lg-10">
            <asp:TextBox ID="txtproductnumber" ValidationGroup="classificationclassproduct" CssClass="form-control" runat="server" MaxLength="255" />
            <asp:CompareValidator runat="server" Operator="DataTypeCheck" Type="Integer"
                ControlToValidate="txtproductnumber" ErrorMessage="Value must be a whole number" ValidationGroup="classificationclassproduct" />
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <div class="col-sm-4 col-sm-offset-2">
            <asp:Button ID="btnSave" CssClass="btn btn-primary" ValidationGroup="classificationclassproduct" type="submit" runat="server" OnClick="btnSave_Click" Text="Save" />
        </div>
    </div>
</div>
