﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FLContentSiteAssist.Common;
using FLContentSiteAssist.BusinessLogic;
using System.Data;
using Common.Extensions;
using CMS.Helpers;

namespace CMS.FluentControls
{
    public partial class ItemControlText : System.Web.UI.UserControl
    {
        public int ItemID { get { return Request.GetQueryString<int>("itemid", 0); } }

        protected override void OnInit(EventArgs e)
        {
            //string _rootpath = Request.Url.GetLeftPart(UriPartial.Authority) + HttpRuntime.AppDomainAppVirtualPath;
            string _rootpath = FluentContentSettings.WebsiteRoot;
            if (_rootpath.Substring(_rootpath.Length - 1, 1) != "/") { _rootpath += "/"; }

            base.OnInit(e);
            Refresh_Screen(true);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Refresh_Screen(Boolean _adddata)
        {
            int _counttext = 0;
            Item item = new Item();
            item.ItemID = ItemID;
            intItemID.Value = ItemID.ToString();
            ProcessGetItemTextByItem _textdata = new ProcessGetItemTextByItem();
            _textdata.Item = item;
            _textdata.Invoke();

            if (_textdata.ResultSet.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in _textdata.ResultSet.Tables[0].Rows)
                {
                    _counttext += 1;

                    //Text type title
                    Panel Section = new Panel();
                    Section.CssClass = "section";
                    Panel titlerow = new Panel();
                    titlerow.CssClass = "headerrow";
                    titlerow.Controls.Add(new LiteralControl(dr["chrItemTextType"].ToString()));
                    Section.Controls.Add(titlerow);

                    //Text Title
                    Panel InputRow1 = new Panel();
                    InputRow1.CssClass = "input_row";
                    Panel fieldName1 = new Panel();
                    fieldName1.CssClass = "field_name";
                    Panel fieldControl1 = new Panel();
                    fieldControl1.CssClass = "field_control";
                    TextBox itemtext1 = new TextBox();
                    itemtext1.CssClass = "input_normal";
                    itemtext1.ID = "title_" + _counttext;
                    if (_adddata) { itemtext1.Text = dr["chrItemTextTitle"].ToString(); }
                    HiddenField texttypeid1 = new HiddenField();
                    texttypeid1.ID = "texttype_" + _counttext;
                    texttypeid1.Value = dr["intItemTextTypeID"].ToString();
                    fieldControl1.Controls.Add(itemtext1);
                    fieldControl1.Controls.Add(texttypeid1);
                    fieldName1.Controls.Add(new LiteralControl("Title"));
                    InputRow1.Controls.Add(fieldName1);
                    InputRow1.Controls.Add(fieldControl1);
                    Section.Controls.Add(InputRow1);
                    //Extract
                    Panel InputRow2 = new Panel();
                    InputRow2.CssClass = "input_row";
                    Panel fieldName2 = new Panel();
                    fieldName2.CssClass = "field_name";
                    Panel fieldControl2 = new Panel();
                    fieldControl2.CssClass = "field_control";
                    TextBox itemtext2 = new TextBox();
                    itemtext2.CssClass = "input_large";
                    itemtext2.TextMode = TextBoxMode.MultiLine;
                    itemtext2.ID = "extract_" + _counttext;
                    if (_adddata) { itemtext2.Text = dr["chrItemTextExtract"].ToString(); }
                    fieldControl2.Controls.Add(itemtext2);
                    fieldName2.Controls.Add(new LiteralControl("Extract"));
                    InputRow2.Controls.Add(fieldName2);
                    InputRow2.Controls.Add(fieldControl2);
                    Section.Controls.Add(InputRow2);
                    //Text
                    Panel InputRow3 = new Panel();
                    InputRow3.CssClass = "input_row";
                    Panel fieldName3 = new Panel();
                    fieldName3.CssClass = "field_name";
                    Panel fieldControl3 = new Panel();
                    fieldControl3.CssClass = "field_control";
                    TextBox itemtext3 = new TextBox();
                    itemtext3.CssClass = "input_large";
                    if (_adddata) { itemtext3.Text = dr["chrItemText"].ToString(); }
                    itemtext3.TextMode = TextBoxMode.MultiLine;
                    itemtext3.ID = "text_" + _counttext;
                    fieldControl3.Controls.Add(itemtext3);
                    fieldName3.Controls.Add(new LiteralControl("Text"));
                    InputRow3.Controls.Add(fieldName3);
                    InputRow3.Controls.Add(fieldControl3);
                    Section.Controls.Add(InputRow3);

                    contentPanel.Controls.Add(Section);
                }
                numberOfText.Value = _counttext.ToString();
            }

        }

        protected void Update_Click(object sender, EventArgs e)
        {

            //Loop through every text field and merge into database   
            for (int i = 1; i <= Convert.ToInt16(numberOfText.Value); i++)
            {
                ItemText text = new ItemText();
                text.ItemID = Convert.ToInt16(intItemID.Value);
                TextBox titletext = this.contentPanel.FindControl("title_" + i) as TextBox;
                text.TextTitle = titletext.Text;
                TextBox extracttext = this.contentPanel.FindControl("extract_" + i) as TextBox;
                text.TextExtract = extracttext.Text;
                TextBox texttext = this.contentPanel.FindControl("text_" + i) as TextBox;
                text.Text = texttext.Text;
                HiddenField texttypeid = this.contentPanel.FindControl("texttype_" + i) as HiddenField;
                text.TextTypeID = Convert.ToInt16(texttypeid.Value);

                ProcessUpdateItemText updatetext = new ProcessUpdateItemText();
                updatetext.ItemText = text;
                updatetext.Invoke();

                updateMessage.Visible = true;
            }

        }
    }
}