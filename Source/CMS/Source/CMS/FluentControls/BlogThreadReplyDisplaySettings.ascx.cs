﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using FLContentSiteAssist.BusinessLogic;
using FLContentSiteAssist.Common;

namespace CMS.FluentControls
{
    public partial class BlogThreadReplyDisplaySettings : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (IsPostBack)
            {

                /*            string CtrlID = string.Empty;
                            if (Request.Form[hidSourceID.UniqueID] != null &&
                                Request.Form[hidSourceID.UniqueID] != string.Empty)
                            {
                                CtrlID = Request.Form[hidSourceID.UniqueID];
                            }

                            if (CtrlID == "update" | CtrlID == "add")
                            {
                                Closing = false;
                            }
                            else
                            {
                                Closing = true;
                            }
                */
            }
            else
            {
                Refresh_Screen();
            }

        }

        protected void Refresh_Screen()
        {
            ProcessGetBlogReplyDisplay _display = new ProcessGetBlogReplyDisplay();
            Blog blog = new Blog();

            blog.DisplayID = Convert.ToInt16(Request.QueryString["replydisplay"]);
            _display.Blog = blog;
            _display.Invoke();

            DataSet displaydata = _display.ResultSet;
            StringBuilder _html = new StringBuilder();

            if (displaydata.Tables[0].Rows.Count > 0)
            {
                maxItems.Text = displaydata.Tables[0].Rows[0]["intMaxNumberItems"].ToString();
                maxItemsPerPage.Text = displaydata.Tables[0].Rows[0]["intMaxNumberItemsPerPage"].ToString();
                if (displaydata.Tables[0].Rows[0]["chrOrderBy"].ToString() == "d")
                {
                    date.Checked = true;
                }
                else if (displaydata.Tables[0].Rows[0]["chrOrderBy"].ToString() == "a")
                {
                    datea.Checked = true;
                }
            }
        }

        protected void Update_Click(object sender, EventArgs e)
        {
            Blog _blog = new Blog();
            _blog.DisplayID = Convert.ToInt16(Request.QueryString["replydisplay"]);
            if (date.Checked)
            {
                _blog.OrderBy = "d";
            }
            else if (datea.Checked)
            {
                _blog.OrderBy = "a";
            }
            if (maxItems.Text == "") { maxItems.Text = "0"; }
            if (maxItemsPerPage.Text == "") { maxItemsPerPage.Text = "0"; }
            _blog.MaxItems = Convert.ToInt16(maxItems.Text);
            _blog.MaxItemsPerPage = Convert.ToInt16(maxItemsPerPage.Text);

            ProcessUpdateBlogReplyDisplay updatedata = new ProcessUpdateBlogReplyDisplay();
            updatedata.Blog = _blog;
            updatedata.Invoke();

            updateMessage.Visible = true;
        }

        public bool Closing { get; set; }
    }
}