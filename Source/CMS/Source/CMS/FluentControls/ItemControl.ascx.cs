﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FLContentSiteAssist.BusinessLogic;
using FLContentSiteAssist.Common;
using System.IO;
using System.Data;
using CMS.Helpers;
using Common.Extensions;

namespace CMS.FluentControls
{
    public partial class ItemControl : System.Web.UI.UserControl
    {
        private DataRow dr;

        public int ItemID { get { return Request.GetQueryString<int>("itemid", 0); } }
        public int ItemTypeID { get { return Request.GetQueryString<int>("itemtype", -1); } }
        public string RelateType { get { return Request.GetQueryString<string>("relatetype", String.Empty); } }
        public int ParentID { get { return Request.GetQueryString<int>("parentid", 0); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            //string _rootpath = Request.Url.GetLeftPart(UriPartial.Authority) + HttpRuntime.AppDomainAppVirtualPath;
            string _rootpath = FluentContentSettings.WebsiteRoot;
            if (_rootpath.Substring(_rootpath.Length - 1, 1) != "/") { _rootpath += "/"; }

            if (!IsPostBack)
            {
                Item _item = new Item();
                _item.ItemID = Convert.ToInt16(Request.QueryString["itemid"]);
                if (Request.QueryString["itemtype"] != null && Request.QueryString["itemtype"] != "")
                {
                    _item.ItemTypeID = Convert.ToInt16(Request.QueryString["itemtype"]);
                }
                
                if (RelateType != String.Empty)
                {
                    _item.ParentItemID = ParentID;
                }
                _item.ItemTypeID = ItemTypeID;
                _item.ItemID = ItemID;

                //Check if we are editing or creating new.  If editing need to load the item to get item type
                if (_item.ItemID != 0 && RelateType == String.Empty)
                {
                    //This is an edit so get the Item's details
                    intItemID.Value = _item.ItemID.ToString();
                    ProcessGetItemByID getItem = new ProcessGetItemByID();
                    getItem.Item = _item;
                    getItem.Invoke();
                    dr = getItem.ResultSet.Tables[0].Rows[0];
                    _item.ItemTypeID = Convert.ToInt16(dr["intItemTypeID"]);
                }

                //Need to get Sequence no's for item type
                ProcessGetItemTypeMaxSequence _maxseq = new ProcessGetItemTypeMaxSequence();
                _maxseq.Item = _item;
                _maxseq.Invoke();

                if (_maxseq.ResultSet.Tables[0].Rows.Count > 0)
                {
                    for (int i = 1; i <= Convert.ToInt16(_maxseq.ResultSet.Tables[0].Rows[0]["maxSequence"]) + 1; i++)
                    {
                        sequenceID.Items.Add(i.ToString());
                    }
                    image_instr.Text = ".jpg .png .gif (w:" + _maxseq.ResultSet.Tables[0].Rows[0]["intWidth"].ToString() + "px h:" + _maxseq.ResultSet.Tables[0].Rows[0]["intHeight"].ToString() + "px)";
                }

                //Now check if this is an edit (itemID is passed) and fill screen
                if (_item.ItemID != 0 && RelateType == String.Empty)
                {
                    btnCreate.Visible = false;

                    itemTitle.Text = dr["chrItemTitle"].ToString();
                    itemURL.Text = dr["chrItemURL"].ToString();
                    itemExtract.Text = dr["chrItemExtract"].ToString();
                    itemText.Text = dr["chrItemText"].ToString();
                    if (dr["datStart"].ToString() != "")
                    {
                        datepickerfr.Text = Convert.ToDateTime(dr["datStart"].ToString()).ToString("dd/MM/yyyy");
                    }
                    if (dr["datExpire"].ToString() != "")
                    {
                        datepickerto.Text = Convert.ToDateTime(dr["datExpire"].ToString()).ToString("dd/MM/yyyy");
                    }
                    if (dr["timStart"].ToString() != "")
                    {
                        timeFrom.Text = Convert.ToDateTime(dr["timStart"].ToString()).ToString("HH:mm");
                    }
                    if (dr["timExpire"].ToString() != "")
                    {
                        timeTo.Text = Convert.ToDateTime(dr["timExpire"].ToString()).ToString("HH:mm");
                    }
                    if (dr["chrItemImageURL"].ToString() != "")
                    {
                        image.ImageUrl = _rootpath + dr["chrItemImageURL"].ToString();
                        image.Visible = true;
                        deleteImage.Checked = false;
                        deleteImage.Visible = true;
                        deleteLabel.Visible = true;
                    }
                    else
                    {
                        image.Visible = false;
                        deleteImage.Checked = false;
                        deleteImage.Visible = false;
                        deleteLabel.Visible = false;
                    }
                    image.Visible = true;
                    imageTitle.Text = dr["chrItemImageTitle"].ToString();
                    imageText.Text = dr["chrItemImageText"].ToString();
                    imageAlt.Text = dr["chrItemImageAlt"].ToString();
                    imageCaption.Text = dr["chrItemImageCaption"].ToString();
                    sequenceID.SelectedValue = dr["intSequence"].ToString();

                    if (dr["bitEnabled"].ToString() == "True")
                    {
                        enabled.Checked = true;
                    }
                    else
                    {
                        enabled.Checked = false;
                    }
                    itemLink.Text = dr["chrLinkURL"].ToString();

                    intItemID.Value = _item.ItemID.ToString();

                    if (image.ImageUrl != "")
                    {
                        deleteImage.Checked = false;
                        deleteImage.Visible = true;
                        deleteLabel.Visible = true;
                    }
                    else
                    {
                        deleteImage.Checked = false;
                        deleteImage.Visible = false;
                        deleteLabel.Visible = false;
                    }
                }
                else
                {
                    btnUpdate.Visible = false;
                }

            }
        }

        protected void Create_Click(object sender, EventArgs e)
        {
            //string _rootpath = Request.Url.GetLeftPart(UriPartial.Authority) + HttpRuntime.AppDomainAppVirtualPath;
            string _rootpath = FluentContentSettings.WebsiteRoot;
            if (_rootpath.Substring(_rootpath.Length - 1, 1) != "/") { _rootpath += "/"; }

            string _imgurl = "";

            //First upload image
            if (ImageUpload.PostedFile.FileName != null && ImageUpload.PostedFile.FileName != "")
            {
                string pathfilename = ImageUpload.PostedFile.FileName;
                string filename = Path.GetFileName(ImageUpload.PostedFile.FileName);
                string localpath = FluentContentSettings.ImageUploadLocation + "\\";

                string localfilename = FluentContentSettings.ImageUploadLocation + "\\" + filename;
                string filenameleft = Path.GetFileNameWithoutExtension(filename);
                string ext = Path.GetExtension(filename);
                if (overwriteImg.Checked)
                {
                    ImageUpload.PostedFile.SaveAs(localfilename);
                }
                else
                {
                    if (File.Exists(@localfilename))
                    {
                        //Find next available filename
                        for (int i = 1; i <= 1000; i++)
                        {
                            if (!File.Exists(@filenameleft + i.ToString() + ext))
                            {
                                localfilename = localpath + filenameleft + i.ToString() + ext;
                                break;
                            }
                        }
                    }
                    ImageUpload.PostedFile.SaveAs(localfilename);
                }
                image.ImageUrl = _rootpath + "uploaded/images/" + Path.GetFileName(localfilename);
                image.Visible = true;
                _imgurl = "uploaded/images/" + Path.GetFileName(localfilename);
            }

            //Requires at least a title
            if (itemTitle.Text.Trim() != "")
            {
                //Now insert the rest of the data into the database
                ProcessInsertItem insertitem = new ProcessInsertItem();
                Item item = new Item();
                item.ItemTypeID = ItemTypeID;
                item.Title = itemTitle.Text;
                item.Extract = itemExtract.Text;
                item.Text = itemText.Text;
                item.ItemURL = itemURL.Text.Trim() == String.Empty ? itemTitle.Text.CleanForSEO() : itemURL.Text.CleanForSEO();
                itemURL.Text = item.ItemURL;
                //if (itemURL.Text.Trim() == "") { itemURL.Text = itemTitle.Text.Replace(" ", "_"); }
                //item.ItemURL = itemURL.Text;
                item.ImageURL = _imgurl;
                item.ImageTitle = imageTitle.Text;
                item.ImageText = imageText.Text;
                item.ImageAlt = imageAlt.Text;
                item.ImageCaption = imageCaption.Text;
                item.Sequence = Convert.ToInt16(sequenceID.SelectedValue);
                item.Enabled = enabled.Checked;
                item.Link = itemLink.Text;
                if (datepickerfr.Text != "")
                {
                    item.DateFrom = Convert.ToDateTime(datepickerfr.Text);
                }
                else
                {
                    item.DateFrom = Convert.ToDateTime("01/01/1900");
                }
                if (datepickerto.Text != "")
                {
                    item.DateTo = Convert.ToDateTime(datepickerto.Text);
                }
                else
                {
                    item.DateTo = Convert.ToDateTime("01/01/1900");
                }

                if (timeFrom.Text != "")
                {
                    item.TimeFrom = timeFrom.Text;
                }
                else
                {
                    item.TimeFrom = "00:00";
                }

                if (timeTo.Text != "")
                {
                    item.TimeTo = timeTo.Text;
                }
                else
                {
                    item.TimeTo = "00:00";
                }


                if (RelateType != String.Empty)
                {
                    item.ParentItemID = ParentID;
                }


                insertitem.Item = item;
                insertitem.Invoke();

                //Now clear the screem and show the success message
                itemTitle.Text = "";
                itemExtract.Text = "";
                itemText.Text = "";
                itemURL.Text = "";
                image.ImageUrl = "";
                image.Visible = false;
                imageTitle.Text = "";
                imageAlt.Text = "";
                imageCaption.Text = "";
                imageText.Text = "";
                itemLink.Text = "";
                datepickerfr.Text = "";
                datepickerto.Text = "";
                timeFrom.Text = "";
                timeTo.Text = "";
                sequenceID.SelectedIndex = -1;
                enabled.Checked = false;

                //If this is a related item then the RelatedItem record must be created              
                if (RelateType != String.Empty)
                {
                    RelatedItem relateditem = new RelatedItem();
                    //Get the created item id
                    relateditem.ChildItemID = insertitem.Item.ItemID;
                    relateditem.ParentItemID = ParentID;
                    relateditem.RelationshipType = RelateType;
                    relateditem.Sequence = Convert.ToInt16(sequenceID.SelectedValue);

                    ProcessInsertRelatedItem insertrelateditem = new ProcessInsertRelatedItem();
                    insertrelateditem.RelatedItem = relateditem;
                    insertrelateditem.Invoke();

                    Response.Redirect(String.Format("~/RelatedItemManagement.aspx?itemid={0}&relationtype={1}&childitemtype={2}&childcreate=True&message=created", ParentID,
                        RelateType, ItemTypeID)); 
                }
                updateMessage.Visible = true;
            }

        }

        protected void Update_Click(object sender, EventArgs e)
        {
            //string _rootpath = Request.Url.GetLeftPart(UriPartial.Authority) + HttpRuntime.AppDomainAppVirtualPath;
            string _rootpath = FluentContentSettings.WebsiteRoot;
            if (_rootpath.Substring(_rootpath.Length - 1, 1) != "/") { _rootpath += "/"; }

            string _imgurl = "";

            //First upload image
            if (ImageUpload.PostedFile.FileName != null && ImageUpload.PostedFile.FileName != "")
            {
                string pathfilename = ImageUpload.PostedFile.FileName;
                string filename = Path.GetFileName(ImageUpload.PostedFile.FileName);
                string localpath = FluentContentSettings.ImageUploadLocation + "\\";
                string localfilename = FluentContentSettings.ImageUploadLocation + "\\" + filename;
                string filenameleft = Path.GetFileNameWithoutExtension(filename);
                string ext = Path.GetExtension(filename);
                if (overwriteImg.Checked)
                {
                    ImageUpload.PostedFile.SaveAs(localfilename);
                }
                else
                {
                    if (File.Exists(@localfilename))
                    {
                        //Find next available filename
                        for (int i = 1; i <= 1000; i++)
                        {
                            if (!File.Exists(@filenameleft + i.ToString() + ext))
                            {
                                localfilename = localpath + filenameleft + i.ToString() + ext;
                                break;
                            }
                        }
                    }
                    ImageUpload.PostedFile.SaveAs(localfilename);
                }
                image.ImageUrl = _rootpath + "uploaded/images/" + Path.GetFileName(localfilename);
                image.Visible = true;
                _imgurl = "uploaded/images/" + Path.GetFileName(localfilename);
            }


            //Requires at least a title
            if (itemTitle.Text.Trim() != "")
            {
                //Now insert the rest of the data into the database
                ProcessUpdateItemComplete updateitem = new ProcessUpdateItemComplete();
                Item item = new Item();
                item.ItemTypeID = ItemTypeID;
                item.ItemID = Convert.ToInt16(intItemID.Value);
                item.Title = itemTitle.Text;
                item.Extract = itemExtract.Text;
                item.Text = itemText.Text;
                item.ItemURL = itemURL.Text.Trim() == String.Empty ? itemTitle.Text.CleanForSEO() : itemURL.Text.CleanForSEO();
                itemURL.Text = item.ItemURL;
                //if (itemURL.Text.Trim() == "") { itemURL.Text = itemTitle.Text.Replace(" ", "_"); }
                //item.ItemURL = itemURL.Text;
                item.ImageURL = _imgurl;
                item.ImageTitle = imageTitle.Text;
                item.ImageText = imageText.Text;
                item.ImageAlt = imageAlt.Text;
                item.ImageCaption = imageCaption.Text;
                item.Sequence = Convert.ToInt16(sequenceID.SelectedValue);
                item.Enabled = enabled.Checked;
                item.Link = itemLink.Text;
                item.DeleteImage = deleteImage.Checked;
                if (deleteImage.Checked && deleteImage.Visible)
                {
                    image.Visible = false;
                    deleteImage.Checked = false;
                    deleteImage.Visible = false;
                    deleteLabel.Visible = false;
                }

                if (datepickerfr.Text != "")
                {
                    item.DateFrom = Convert.ToDateTime(datepickerfr.Text);
                }
                else
                {
                    item.DateFrom = Convert.ToDateTime("01/01/1900");
                }
                if (datepickerto.Text != "")
                {
                    item.DateTo = Convert.ToDateTime(datepickerto.Text);
                }
                else
                {
                    item.DateTo = Convert.ToDateTime("01/01/1900");
                }

                if (RelateType != String.Empty)
                {
                    item.ParentItemID = ParentID;
                }

                if (timeFrom.Text != "")
                {
                    item.TimeFrom = timeFrom.Text;
                }
                else
                {
                    item.TimeFrom = null;//"00:00";
                }

                if (timeTo.Text != "")
                {
                    item.TimeTo = timeTo.Text;
                }
                else
                {
                    item.TimeTo = null; //"00:00";
                }
                updateitem.Item = item;
                updateitem.Invoke();

                updateMessage.Text = "Successfully updated";
                updateMessage.Visible = true;
            }
        }

        private void Refresh_Screen()
        {
            /*
            FLContentSiteAssist.Common.Page _page = new FLContentSiteAssist.Common.Page();

            ProcessGetPageByID _getpage = new ProcessGetPageByID();
            _page.PageID = Convert.ToInt16(Request.QueryString["pageid"]);
            _getpage.Page = _page;
            _getpage.Invoke();

            if (_getpage.ResultSet.Tables[0].Rows.Count > 0)
            {
                DataRow pagedata = _getpage.ResultSet.Tables[0].Rows[0];

                StringBuilder _html = new StringBuilder();

                pageName.Text = pagedata["chrPage"].ToString();
                pageDesc.Text = pagedata["chrPageDescription"].ToString();
                pageURL.Text = pagedata["chrPageURL"].ToString();
                pageKeywords.Text = pagedata["chrPageKeywords"].ToString();
                pageTitle.Text = pagedata["chrPageTitle"].ToString();
                intPageID.Value = pagedata["intPageID"].ToString();

            }
            */
        }
    }
}