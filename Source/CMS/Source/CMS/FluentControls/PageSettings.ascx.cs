﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FLContentSiteAssist.BusinessLogic;
using System.Text;
using System.Data;
using FluentContent.DAC;

namespace CMS.FluentControls
{
    public partial class PageSettings : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Refresh_Screen();
            }
        }

        protected void Update_Click(object sender, EventArgs e)
        {
            ProcessUpdatePage _updatepage = new ProcessUpdatePage();
            FLContentSiteAssist.Common.Page _page = new FLContentSiteAssist.Common.Page();

            _page.PageID = Convert.ToInt16(intPageID.Value);
            _page.PageName = pageName.Text;
            _page.PageKeywords = pageKeywords.Text;
            _page.PageTitle = pageTitle.Text.Trim();
            _page.PageDesc = pageDesc.Text;
            _page.PageURL = pageURL.Text;

            // validate page url
            if (DAC_ContentPage.PageUrlExists(pageURL.Text.Trim(), _page.PageID))
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Update failed! Page URL is already in use, please use a different URL!','warning');", true);

                return;
            }

            _updatepage.Page = _page;
            _updatepage.Invoke();

            Refresh_Screen();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Successfully updated','success');", true);
        }

        private void Refresh_Screen()
        {
            FLContentSiteAssist.Common.Page _page = new FLContentSiteAssist.Common.Page();

            ProcessGetPageByID _getpage = new ProcessGetPageByID();
            _page.PageID = Convert.ToInt16(Request.QueryString["pageid"]);
            _getpage.Page = _page;
            _getpage.Invoke();

            if (_getpage.ResultSet.Tables[0].Rows.Count > 0)
            {
                DataRow pagedata = _getpage.ResultSet.Tables[0].Rows[0];

                StringBuilder _html = new StringBuilder();

                pageName.Text = pagedata["chrPage"].ToString();
                pageDesc.Text = pagedata["chrPageDescription"].ToString();
                pageURL.Text = pagedata["chrPageURL"].ToString();
                pageKeywords.Text = pagedata["chrPageKeywords"].ToString();
                pageTitle.Text = pagedata["chrPageTitle"].ToString();
                intPageID.Value = pagedata["intPageID"].ToString();
                pageURL.Enabled = Convert.ToBoolean(pagedata["bitPageURLEditable"]);
               
            }
        }
    }
}