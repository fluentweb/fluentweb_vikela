﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FLContentSiteAssist.BusinessLogic;
using FLContentSiteAssist.Common;
using System.Data;
using System.Text;
using Common.Extensions;
using CMS.Helpers;

namespace CMS.FluentControls
{
    public partial class ItemsModify : System.Web.UI.UserControl
    {
        public int ItemTypeID { get { return Request.GetQueryString<int>("itemtype", -1); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            //string _rootpath = Request.Url.GetLeftPart(UriPartial.Authority) + HttpRuntime.AppDomainAppVirtualPath;
            string _rootpath = FluentContentSettings.WebsiteRoot;
            if (_rootpath.Substring(_rootpath.Length - 1, 1) != "/") { _rootpath += "/"; }

            if (IsPostBack)
            {

                string CtrlID = string.Empty;
                if (Request.Form[hidSourceID.UniqueID] != null &&
                    Request.Form[hidSourceID.UniqueID] != string.Empty)
                {
                    CtrlID = Request.Form[hidSourceID.UniqueID];
                }
            }
            else
            {
                Refresh_Screen(_rootpath);
            }

        }

        protected void Update_Click(object sender, EventArgs e)
        {
            //string _rootpath = Request.Url.GetLeftPart(UriPartial.Authority) + HttpRuntime.AppDomainAppVirtualPath;
            string _rootpath = FluentContentSettings.WebsiteRoot;
            if (_rootpath.Substring(_rootpath.Length - 1, 1) != "/") { _rootpath += "/"; }

            // Look through every row of links and update the database with their values
            string _itemsequence = itemSequence.Value;
            string[] _itemorder = itemSequence.Value.Split(";".ToCharArray());

            for (int i = 1; i <= Convert.ToInt16(hiddenRows.Value); i++)
            {
                Item item = new Item();

                item.ItemID = Convert.ToInt16(Request.Form["id" + i.ToString()]);
                item.Title = Request.Form["title" + i.ToString()];
                item.Sequence = listIndex(_itemorder, "li1_" + i);
                if (Request.Form["enabled" + i.ToString()] == "on")
                {
                    item.Enabled = true;
                }
                else
                {
                    item.Enabled = false;
                }
                bool _delete = false;
                if (Request.Form["delete" + i.ToString()] == "on")
                {
                    _delete = true;
                }

                if (!_delete)
                {
                    ProcessUpdateItemBasic updateitem = new ProcessUpdateItemBasic();
                    updateitem.Item = item;
                    updateitem.Invoke();
                }
                else
                {
                    ProcessDeleteItem deleteitem = new ProcessDeleteItem();
                    deleteitem.Item = item;
                    deleteitem.Invoke();
                }

            }

            Refresh_Screen(_rootpath);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Successfully updated','success');", true);
        }

        protected void Create_Click(object sender, EventArgs e)
        {
            Response.Redirect(ResolveUrl(String.Format("~/ItemManagement.aspx?itemtype={0}&action=create", ItemTypeID)));
        }

        protected void Refresh_Screen(string _rootpath)
        {

            ProcessGetItemByType items = new ProcessGetItemByType();
            Item item = new Item();
            item.ItemTypeID = ItemTypeID;
            item.ViewDisabled = true;
            items.Item = item;
            items.Invoke();

            DataSet itemdata = items.ResultSet;
            StringBuilder _html = new StringBuilder();
            int _itemcount = 1;
            count.Text = "0";
            if (itemdata.Tables[0].Rows.Count > 0)
            {
                count.Text = itemdata.Tables[0].Rows.Count.ToString();
                foreach (DataRow dr in itemdata.Tables[0].Rows)
                {
                    //Build the screen to show the links.
                    _html.Append("<li id=\"li1_" + _itemcount + "\" name=\"li1_" + _itemcount + "\" class=\"listrow\"><img id=\"mv1" + _itemcount + "\" src=\"FluentControls/images/reorder.jpg\" alt=\"click and hold to drag up or down\" title=\"click and hold to drag up or down\" />");
                    _html.Append("<input type=\"text\" id=\"title" + _itemcount + "\" name=\"title" + _itemcount + "\" class=\"input_itemtitle\" value=\"" + dr["chrItemTitle"] + "\" />");
                    if (String.IsNullOrEmpty(dr["chrItemImageURL"].ToString()))
                    {
                        string formatted = "<span class='itemimage'><img class='itemimage' src='{0}' /></span>";
                        _html.AppendFormat(formatted, ResolveUrl("~/images/no_image.png"));
                    }
                    else
                    {
                        string formatted = "<span class='itemimage'><img class='itemimage' src='{0}' /></span>";
                        _html.AppendFormat(formatted, _rootpath + FluentContentSettings.ImageUploadFolder + "/" + dr["chrItemImageURL"]);
                    }
                    if (dr["bitEnabled"].ToString() == "True")
                    {
                        _html.Append("<input type=\"checkbox\" id=\"enabled" + _itemcount + "\" name=\"enabled" + _itemcount + "\" class=\"input_enabled\" checked=\"true\" />");
                    }
                    else
                    {
                        _html.Append("<input type=\"checkbox\" id=\"enabled" + _itemcount + "\" name=\"enabled" + _itemcount + "\" class=\"input_enabled\" />");
                    }
                    _html.Append("<input type=\"checkbox\" id=\"delete" + _itemcount + "\" name=\"delete" + _itemcount + "\" class=\"input_delete\"/>");
                    _html.Append("<input type=\"hidden\" id=\"id" + _itemcount + "\" name=\"id" + _itemcount + "\" value=\"" + dr["intItemID"] + "\" />");
                    _html.AppendFormat("<a href='ItemManagement.aspx?itemtype={0}&itemid={1}'type=\"button\" class=\"btn btn-info\"><i class=\"fa fa-paste\"></i>&nbsp;Edit</a>", ItemTypeID, dr["intItemID"]);
                    _html.Append("</li>");
                    _itemcount += 1;


                }
                itemrows.Text = _html.ToString();
                hiddenRows.Value = Convert.ToString(_itemcount - 1);
            }
            else
            {
                itemrows.Text = "<li class='noItems'>No items available</li>";
                btnSave.Visible = false;
            }
        }

        private int listIndex(string[] inputArray, string searchTerm)
        {
            int firstIndex = 0;

            for (int i = 0; i < inputArray.Length; i++)
            {
                if (inputArray[i] == searchTerm)
                {
                    firstIndex = i;
                    break;
                }

            }
            return firstIndex + 1;
        }
    }
}