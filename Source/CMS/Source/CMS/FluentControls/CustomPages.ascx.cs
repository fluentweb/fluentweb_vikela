﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FLContentSiteAssist.BusinessLogic;
using System.Text;
using CMS.Helpers;
using System.Data;

namespace CMS.FluentControls
{
    public partial class CustomPages : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                refreshPage();
            }
        }

        private void refreshPage()
        {
            string _rootpath = FluentContentSettings.WebsiteRoot;
            if (_rootpath.Substring(_rootpath.Length - 1, 1) != "/") { _rootpath += "/"; }

            ProcessGetPageTemplates templates = new ProcessGetPageTemplates();
            templates.Invoke();

            if (templates.ResultSet.Tables[0].Rows.Count > 0)
            {
                PageTemplate.DataSource = templates.ResultSet;
                PageTemplate.DataTextField = "chrPageTemplateName";
                PageTemplate.DataValueField = "intPageTemplateID";
                PageTemplate.DataBind();
            }

            ProcessGetPages pages = new ProcessGetPages();
            pages.Invoke();
            count.Text = "0";
            if (pages.ResultSet.Tables[0].Rows.Count > 0)
            {
                int _itemcount = 1;
                StringBuilder _html = new StringBuilder();
                count.Text = pages.ResultSet.Tables[0].Rows.Count.ToString();
                foreach (DataRow dr in pages.ResultSet.Tables[0].Rows)
                {
                    _html.Append("<li id=\"li1_" + _itemcount + "\" name=\"li1_" + _itemcount + "\" class=\"listrow\">");
                    _html.Append("<input type=\"text\" id=\"PageTitle" + _itemcount + "\" name=\"PageTitle" + _itemcount + "\" class=\"inputcustompagetitle\" value=\"" + dr["chrPage"] + "\" />");
                    _html.Append("<input type=\"text\" id=\"PageURL" + _itemcount + "\" name=\"PageURL" + _itemcount + "\" class=\"inputcustompageurl\" value=\"" + dr["chrPageURL"] + "\" />");
                    _html.Append("<input type=\"text\" id=\"PageTemplate" + _itemcount + "\" name=\"PageTemplate" + _itemcount + "\" class=\"inputcustompagetemplate\" value=\"" + dr["chrPageTemplateName"] + "\" readonly=\"readonly\" />");
                    _html.Append("<input style=\"margin-right: 5%;\" type=\"checkbox\" id=\"delete" + _itemcount + "\" name=\"delete" + _itemcount + "\" class=\"inputchkbox\"/>");
                    _html.Append("<a style=\"margin-right: 1%;\" href='CustomPageManagement.aspx?pageid=" + dr["intPageID"] + "' type=\"button\" class=\"btn btn-info\"><i class=\"fa fa-paste\"></i>&nbsp;Edit</a>");
                    _html.Append("<a style=\"margin-right: 1%;\" href=\"" + _rootpath + dr["chrPageFullURL"] + "\" target='_blank' type=\"button\" class=\"btn btn-primary\"><i class=\"fa fa-external-link\"></i>&nbsp;View</a>");
                    _html.Append("  <input type=\"hidden\" id=\"id" + _itemcount + "\" name=\"id" + _itemcount + "\" value=\"" + dr["intPageID"] + "\" />");
                    _html.Append("</li>");
                    _itemcount += 1;
                }
                hiddenRows.Value = Convert.ToString(_itemcount - 1);
                pagerows.Text = _html.ToString();
            }
            else 
            {
                pagerows.Text = String.Empty;
            }
        }

        protected void Update_Click(object sender, EventArgs e)
        {

            //Odd behaviour in Chrome and Firefox causes Update_Click to fire event though add_click is triggered
            string check = Request.Form[hidSourceID.UniqueID];
            if (check == "add")
            {
                Add();
                return;
            }

            for (int i = 1; i <= Convert.ToInt16(hiddenRows.Value); i++)
            {
                //Needs at least a title
                string _pagename = Request.Form["PageTitle" + i.ToString()];
                string _pageurl = Request.Form["PageURL" + i.ToString()];

                if (_pagename != "")
                {
                    //If the URL has been blanked then replace it
                    if (_pageurl == "")
                    {
                        _pageurl = _pagename.Replace(" ", "-");
                    }
                    else
                    {
                        _pageurl = _pageurl.Replace(" ", "-");
                    }
                    //Add the .aspx
                    //if (_pageurl.ToLower().IndexOf(".aspx") == -1)
                    //{
                    //    _pageurl = _pageurl + ".aspx";
                    //}

                    FLContentSiteAssist.Common.Page _page = new FLContentSiteAssist.Common.Page();
                    _page.PageID = Convert.ToInt16(Request.Form["id" + i.ToString()]);
                    _page.PageName = _pagename;
                    _page.PageURL = _pageurl;
                    bool _delete = false;
                    if (Request.Form["delete" + i.ToString()] == "on")
                    {
                        _delete = true;
                    }

                    if (!_delete)
                    {
                        ProcessUpdatePageBasic updatepage = new ProcessUpdatePageBasic();
                        updatepage.Page = _page;
                        updatepage.Invoke();
                    }
                    else
                    {
                        ProcessDeletePage deletepage = new ProcessDeletePage();
                        deletepage.Page = _page;
                        deletepage.Invoke();
                    }
                }
            }

            refreshPage();
        }

        protected void Add_Click(object sender, EventArgs e)
        {
            Add();
        }

        protected void Add()
        {

            //Need a title and a template
            if (PageTitle.Text != "" && PageTemplate.SelectedIndex > -1)
            {
                //Create a URL if needed
                if (PageURL.Text == "")
                {
                    PageURL.Text = PageTitle.Text.Replace(" ", "_");
                }
                else
                {
                    PageURL.Text = PageURL.Text.Replace(" ", "_");
                }

                //Add the .aspx
                if (PageURL.Text.ToLower().IndexOf(".aspx") == -1)
                {
                    PageURL.Text = PageURL.Text + ".aspx";
                }

                FLContentSiteAssist.Common.Page _page = new FLContentSiteAssist.Common.Page();
                _page.PageName = PageTitle.Text;
                _page.PageURL = PageURL.Text;
                _page.TemplateID = Convert.ToInt16(PageTemplate.SelectedValue);
                ProcessInsertPage insertPage = new ProcessInsertPage();
                insertPage.Page = _page;
                insertPage.Invoke();

                PageTitle.Text = "";
                PageURL.Text = "";
                PageTemplate.ClearSelection();
                refreshPage();

            }

        }
    }
}