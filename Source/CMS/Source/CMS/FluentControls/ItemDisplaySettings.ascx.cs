﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FLContentSiteAssist.Common;
using FLContentSiteAssist.BusinessLogic;
using System.Data;
using System.Text;

namespace CMS.FluentControls
{
    public partial class ItemDisplaySettings : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (IsPostBack)
            {

                string CtrlID = string.Empty;
                if (Request.Form[hidSourceID.UniqueID] != null &&
                    Request.Form[hidSourceID.UniqueID] != string.Empty)
                {
                    CtrlID = Request.Form[hidSourceID.UniqueID];
                }

                //if (CtrlID == "update" | CtrlID == "add")
                //{
                //    Closing = false;
                //}
                //else
                //{
                //    Closing = true;
                //}

            }
            else
            {
                Refresh_Screen();
            }

        }

        protected void Refresh_Screen()
        {
            ProcessGetItemDisplaySettings _itemdisplay = new ProcessGetItemDisplaySettings();
            Item item = new Item();

            item.ItemDisplayID = Convert.ToInt16(Request.QueryString["itemdisplay"]);
            _itemdisplay.Item = item;
            _itemdisplay.Invoke();

            DataSet displaydata = _itemdisplay.ResultSet;
            StringBuilder _html = new StringBuilder();

            if (displaydata.Tables[0].Rows.Count > 0)
            {
                maxItems.Text = displaydata.Tables[0].Rows[0]["intMaxNumberItems"].ToString();
                if (displaydata.Tables[0].Rows[0]["chrOrderBy"].ToString() == "r")
                {
                    random.Checked = true;
                }
                else if (displaydata.Tables[0].Rows[0]["chrOrderBy"].ToString() == "s")
                {
                    sequence.Checked = true;
                }
                else if (displaydata.Tables[0].Rows[0]["chrOrderBy"].ToString() == "d")
                {
                    date.Checked = true;
                }
                else if (displaydata.Tables[0].Rows[0]["chrOrderBy"].ToString() == "a")
                {
                    datea.Checked = true;
                }

                // item display paging

                object paged = displaydata.Tables[0].Rows[0]["bitPaged"];
                bool isPaged = paged == DBNull.Value ? false : Convert.ToBoolean(paged);
                object size = displaydata.Tables[0].Rows[0]["intPageSize"];
                int pageSize = size == DBNull.Value ? 0 : Convert.ToInt32(size);
                txtPageSize.Text = pageSize.ToString();
            }
        }

        protected void Update_Click(object sender, EventArgs e)
        {
            Item _item = new Item();
            _item.ItemDisplayID = Convert.ToInt16(Request.QueryString["itemdisplay"]);
            if (random.Checked)
            {
                _item.OrderBy = "r";
            }
            else if (date.Checked)
            {
                _item.OrderBy = "d";
            }
            else if (sequence.Checked)
            {
                _item.OrderBy = "s";
            }
            else if (datea.Checked)
            {
                _item.OrderBy = "a";
            }
            _item.MaxItems = Convert.ToInt16(maxItems.Text);
            int pageSize = txtPageSize.Text.Trim().Length > 0 ? Convert.ToInt32(txtPageSize.Text.Trim()) : 0;
            _item.IsPaged = pageSize > 0;
            _item.PageSize = pageSize;

            ProcessUpdateItemDisplay updateitem = new ProcessUpdateItemDisplay();
            updateitem.Item = _item;
            updateitem.Invoke();

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Successfully updated','success');", true);
        }
    }
}