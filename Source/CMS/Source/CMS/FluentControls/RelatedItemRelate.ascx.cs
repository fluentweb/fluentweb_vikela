﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FLContentSiteAssist.Common;
using FLContentSiteAssist.BusinessLogic;
using System.Data;
using System.Text;
using Common.Extensions;

namespace CMS.FluentControls
{
    public partial class RelatedItemRelate : System.Web.UI.UserControl
    {
        public int ItemID { get { return Request.GetQueryString<int>("itemid", 0); } }
        public string RelationshipType { get { return Request.GetQueryString<string>("relationtype"); } }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (IsPostBack)
            {

                string CtrlID = string.Empty;
                if (Request.Form[hidSourceID.UniqueID] != null &&
                    Request.Form[hidSourceID.UniqueID] != string.Empty)
                {
                    CtrlID = Request.Form[hidSourceID.UniqueID];
                }
            }
            else
            {
                if (ItemID != 0)
                {
                    Refresh_Screen();
                }
            }

        }

        protected void Update_Click(object sender, EventArgs e)
        {
            //Odd behaviour in Chrome and Firefox causes Update_Click to fire event though add_click is triggered
            string check = Request.Form[hidSourceID.UniqueID];
            if (check == "add")
            {
                Add();
                return;
            }

            // Look through every row of items and update the database with their values
            string _listsequence = listSequence.Value;
            string[] _listorder = listSequence.Value.Split(";".ToCharArray());

            for (int i = 1; i <= Convert.ToInt16(hiddenRows.Value); i++)
            {
                RelatedItem item = new RelatedItem();

                item.RelationshipID = Convert.ToInt16(Request.Form["id" + i.ToString()]);
                item.Sequence = listIndex(_listorder, "li1_" + i);
                bool _delete = false;
                if (Request.Form["delete" + i.ToString()] == "on")
                {
                    _delete = true;
                }

                if (!_delete)
                {
                    ProcessUpdateRelatedItem updateitem = new ProcessUpdateRelatedItem();
                    updateitem.RelatedItem = item;
                    updateitem.Invoke();
                }
                else
                {
                    ProcessDeleteRelatedItem deleterelateditem = new ProcessDeleteRelatedItem();
                    deleterelateditem.RelatedItem = item;
                    deleterelateditem.Invoke();
                }

            }

            Refresh_Screen();

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Successfully updated','success');", true);
        }

        protected void Add_Click(object sender, EventArgs e)
        {

            Add();

        }

        protected void Add()
        {

            RelatedItem item = new RelatedItem();
            item.RelationshipType = add.CommandArgument;
            item.ParentItemID = Convert.ToInt16(Request.QueryString["itemid"]);
            item.ChildItemID = Convert.ToInt16(addTitle.SelectedValue);
            if (hiddenRows.Value == "")
            {
                item.Sequence = 1;
            }
            else
            {
                item.Sequence = Convert.ToInt16(hiddenRows.Value) + 1;
            };

            ProcessInsertRelatedItem insertitem = new ProcessInsertRelatedItem();
            insertitem.RelatedItem = item;
            insertitem.Invoke();

            Refresh_Screen();

        }

       
        protected void Refresh_Screen()
        {
            ProcessGetRelatedItemAvailable availableitems = new ProcessGetRelatedItemAvailable();
            RelatedItem relateditem = new RelatedItem();
            relateditem.RelationshipType = RelationshipType;
            relateditem.ItemID = ItemID;
            availableitems.RelatedItem = relateditem;
            availableitems.Invoke();

            add.CommandArgument = RelationshipType;

            if (availableitems.ResultSet.Tables[0].Rows.Count > 0)
            {
                addTitle.DataSource = availableitems.ResultSet;
                addTitle.DataTextField = "chrItemTitle";
                addTitle.DataValueField = "intItemID";
                addTitle.DataBind();
            }
            else
            {
                addTitle.Items.Clear();
                addTitle.Items.Insert(0, String.Format("No items of type '{0}' available", RelationshipType));
                addTitle.Enabled = false;
                add.Visible = false;
            }

            ProcessGetRelatedItem items = new ProcessGetRelatedItem();
            items.RelatedItem = relateditem;
            items.Invoke();

            DataSet itemdata = items.ResultSet;
            StringBuilder _linkhtml = new StringBuilder();
            int _linkcount = 1;
            count.Text = "0";
            if (itemdata.Tables[0].Rows.Count > 0)
            {
                count.Text = itemdata.Tables[0].Rows.Count.ToString();
                foreach (DataRow dr in itemdata.Tables[0].Rows)
                {
                    //Build the screen to show the links.
                    _linkhtml.Append("<li id=\"li1_" + _linkcount + "\" name=\"li1_" + _linkcount + "\" class=\"listrow\"><img id=\"mv1" + _linkcount + "\" src=\"FluentControls/images/reorder.jpg\" alt=\"click and hold to drag up or down\" title=\"click and hold to drag up or down\" />");
                    _linkhtml.Append("<input type=\"text\" id=\"title" + _linkcount + "\" name=\"title" + _linkcount + "\" class=\"input_title\" value=\"" + dr["chrItemTitle"] + "\" readonly=\"readonly\" />");
                    _linkhtml.Append("<input type=\"checkbox\" id=\"delete" + _linkcount + "\" name=\"delete" + _linkcount + "\" class=\"input_delete\"/>");
                    _linkhtml.Append("<input type=\"hidden\" id=\"id" + _linkcount + "\" name=\"id" + _linkcount + "\" value=\"" + dr["intItemRelateID"] + "\" />");
                    _linkhtml.Append("</li>");
                    _linkcount += 1;
                }
                linkrows.Text = _linkhtml.ToString();
                hiddenRows.Value = Convert.ToString(_linkcount - 1);
                divDelete.Visible = true;
            }
            else
            {
                linkrows.Text = "";
                divDelete.Visible = false;
            }
        }

        private int listIndex(string[] inputArray, string searchTerm)
        {
            int firstIndex = 0;

            for (int i = 0; i < inputArray.Length; i++)
            {
                if (inputArray[i] == searchTerm)
                {
                    firstIndex = i;
                    break;
                }

            }
            return firstIndex + 1;
        }

    }
}