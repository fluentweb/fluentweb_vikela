﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FLContentSiteAssist.BusinessLogic;
using FLContentSiteAssist.Common;
using System.Data;

namespace CMS.FluentControls
{
    public partial class BlogThreadEdit : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Refresh_Screen();
            }
        }

        protected void Update_Click(object sender, EventArgs e)
        {
            //Requires at least a title, comment and an author
            if (title.Text != "" && editedHTML.Value.Trim() != "" && author.SelectedIndex > -1)
            {
                if (threadURL.Text.Trim() == "") { threadURL.Text = threadURL.Text.Replace(" ", "_"); }
                Blog blog = new Blog();
                blog.ThreadID = Convert.ToInt16(Request.QueryString["blogthread"]);
                blog.UserID = Convert.ToInt16(author.SelectedValue);
                blog.Title = title.Text.Trim();
                blog.Comment = editedHTML.Value.Trim();
                blog.Status = "Approved";
                blog.URL = threadURL.Text;

                ProcessUpdateBlogThread _updatethread = new ProcessUpdateBlogThread();
                _updatethread.Blog = blog;
                _updatethread.Invoke();

                //Now put the html editor back to its edited text as the refresh clears it!!
                textToEdit.Text = editedHTML.Value.Trim();

                //Now show the success message...
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Blog thread successfully updated.','success');", true);

            }

        }

        private void Refresh_Screen()
        {
            ProcessGetUser _userlist = new ProcessGetUser();
            _userlist.SiteName = "Arts In Health Care";
            _userlist.Invoke();
            if (_userlist.ResultSet.Tables[0].Rows.Count > 0)
            {
                author.DataSource = _userlist.ResultSet;
                author.DataValueField = "intUserID";
                author.DataTextField = "chrFullname";
                author.DataBind();
            }

            ProcessGetBlogThread _threaddata = new ProcessGetBlogThread();
            Blog blog = new Blog();
            blog.ThreadID = Convert.ToInt16(Request.QueryString["blogthread"]);
            _threaddata.Blog = blog;
            _threaddata.Invoke();

            if (_threaddata.ResultSet.Tables[0].Rows.Count > 0)
            {
                DataRow dr = _threaddata.ResultSet.Tables[0].Rows[0];
                title.Text = dr["chrTitle"].ToString();
                threadURL.Text = dr["chrThreadURL"].ToString();
                textToEdit.Text = dr["chrComment"].ToString();
                author.SelectedValue = dr["intUserID"].ToString();
            }

        }
    }
}