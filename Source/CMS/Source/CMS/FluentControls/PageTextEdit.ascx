﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="PageTextEdit.ascx.cs"
    Inherits="CMS.FluentControls.PageTextEdit" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<div>
    <asp:HiddenField ID="hidSourceID" runat="server" />
    <asp:HiddenField ID="editedHTML" runat="server" />

    <div class="ibox-content no-padding" style="display: block;">
        <CKEditor:CKEditorControl ID="textEditor" ResizeEnabled="false"  BasePath="~/ckeditor" runat="server"></CKEditor:CKEditorControl>
        <br />
        <asp:Button ID="btnSave" CssClass="btn btn-primary" type="submit" runat="server" OnClick="Update_Click" Text="Save" />
    </div>
</div>
