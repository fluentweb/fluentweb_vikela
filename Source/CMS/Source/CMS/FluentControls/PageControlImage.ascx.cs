﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CMS.Helpers;
using System.IO;
using FLContentSiteAssist.BusinessLogic;
using FLContentSiteAssist.Common;
using Common.Extensions;
using FLContentSiteAssist.BusinessLogic.DataProcessing;

namespace CMS.FluentControls
{
    public partial class PageControlImage : System.Web.UI.UserControl
    {
        //public int PageID { get { return Request.GetQueryString<int>("pageid", -1); } }

        protected override void OnInit(EventArgs e)
        {
            //string _rootpath = Request.Url.GetLeftPart(UriPartial.Authority) + HttpRuntime.AppDomainAppVirtualPath;
            string _rootpath = FluentContentSettings.WebsiteRoot;
            if (_rootpath.Substring(_rootpath.Length - 1, 1) != "/") { _rootpath += "/"; }

            base.OnInit(e);
           // if (PageID == -1) return;
            Refresh_Screen(true, _rootpath);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Refresh_Screen(Boolean _adddata, string _rootpath)
        {
            int _countimage = 0;
            PageImage pageImage = new PageImage();
            SitePage page = new SitePage();
           // pageImage.PageID = PageID;
            intPageID.Value = PageID.ToString(); //Request.QueryString["itemid"];

            ProcessGetPageByID pageProcess = new ProcessGetPageByID();
            pageProcess.Page = new FLContentSiteAssist.Common.Page { PageID = PageID };
            pageProcess.Invoke();

            btnSave.CommandArgument = PageID.ToString();

            if (pageProcess.ResultSet.HasRows(0))
            {
                ProcessGetPageImage _imagedata = new ProcessGetPageImage();
                _imagedata.SitePage = new SitePage { Name = pageProcess.ResultSet.Tables[0].GetDataTableValue<string>(0, "chrPage") };
                _imagedata.Invoke();

                if (_imagedata.ResultSet.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in _imagedata.ResultSet.Tables[0].Rows)
                    {
                        _countimage += 1;

                        #region Load Image Fields
                        //Image type title
                        Panel Section = new Panel();
                        Section.CssClass = "section";
                        Panel titlerow = new Panel();
                        titlerow.CssClass = "headerrow";
                        titlerow.Controls.Add(new LiteralControl(dr["chrPageTemplateImageType"].ToString()));
                        Section.Controls.Add(titlerow);

                        //Image upload
                        Panel InputRow1 = new Panel();
                        InputRow1.CssClass = "input_row";
                        Panel fieldName1 = new Panel();
                        fieldName1.CssClass = "field_name";
                        Panel fieldControl1 = new Panel();
                        fieldControl1.CssClass = "field_control";
                        FileUpload fileupload = new FileUpload();
                        fileupload.ID = "imageupload_" + _countimage;
                        CheckBox overwrite = new CheckBox();
                        overwrite.ID = "overwrite_" + _countimage;
                        overwrite.Checked = false;
                        overwrite.CssClass = "input_enabled";
                        Panel imagebox = new Panel();
                        imagebox.CssClass = "imagebox";
                        Image image = new Image();
                        image.CssClass = "image";
                        image.Visible = false;
                        image.ID = "image_" + _countimage;

                        CheckBox deleteimage = new CheckBox();
                        deleteimage.ID = "deleteimage_" + _countimage;
                        deleteimage.Visible = false;
                        deleteimage.CssClass = "input_enabled";
                        deleteimage.Checked = false;
                        Label deleteimagelbl = new Label();
                        deleteimagelbl.ID = "deleteLabel_" + _countimage;
                        deleteimagelbl.Text = "Remove this image";
                        deleteimagelbl.Visible = false;
                        if (_adddata)
                        {


                            image.ImageUrl = _rootpath + FluentContentSettings.ImageUploadFolder + "/" + dr["chrPageImageURL"].ToString();
                            if (dr["chrPageImageURL"].ToString() != "")
                            {
                                image.Visible = true;
                                deleteimage.Visible = true;
                                deleteimagelbl.Visible = true;
                            }
                            else
                            {
                                image.Visible = false;
                                deleteimage.Visible = false;
                                deleteimagelbl.Visible = false;
                            }
                        }
                        fieldName1.Controls.Add(new LiteralControl("Image"));
                        imagebox.Controls.Add(image);
                        fieldControl1.Controls.Add(fileupload);

                        string _imageinstr = String.Format(".jpg .png .gif (w:{0}px {1}h:{2}px {3})", dr["intWidth"], RestrictionString(dr["chrImageWidthRestrict"].ToString()),
                            dr["intHeight"], RestrictionString(dr["chrImageHeightRestrict"].ToString()));

                        fieldControl1.Controls.Add(new LiteralControl("&nbsp;" + _imageinstr + "<br />"));
                        fieldControl1.Controls.Add(overwrite);
                        fieldControl1.Controls.Add(new LiteralControl(" Overwrite existing image"));
                        fieldControl1.Controls.Add(imagebox);
                        fieldControl1.Controls.Add(deleteimage);
                        fieldControl1.Controls.Add(deleteimagelbl);

                        // add dimensions for validation data
                        HiddenField hdnDimensions = new HiddenField();
                        hdnDimensions.ID = "hdnDimensions_" + _countimage;
                        hdnDimensions.Value = String.Format("{0},{1}|{2},{3}", dr["intWidth"], dr["intHeight"], dr["chrImageWidthRestrict"], dr["chrImageHeightRestrict"]);
                        fieldControl1.Controls.Add(hdnDimensions);

                        HiddenField hdnPageImageID = new HiddenField();
                        hdnPageImageID.ID = "hdnPageImageID_" + _countimage;
                        hdnPageImageID.Value = dr["intPageImageID"].ToString();
                        fieldControl1.Controls.Add(hdnPageImageID);

                        HiddenField imagetypeid1 = new HiddenField();
                        imagetypeid1.ID = "imagetype_" + _countimage;
                        imagetypeid1.Value = dr["intPageTemplateImageTypeID"].ToString();
                        fieldControl1.Controls.Add(imagetypeid1);
                        InputRow1.Controls.Add(fieldName1);
                        InputRow1.Controls.Add(fieldControl1);
                        Section.Controls.Add(InputRow1);

                        //Link URL
                        Panel InputRow2 = new Panel();
                        InputRow2.CssClass = "input_row";
                        Panel fieldName2 = new Panel();
                        fieldName2.CssClass = "field_name";
                        Panel fieldControl2 = new Panel();
                        fieldControl2.CssClass = "field_control";
                        TextBox linkurl = new TextBox();
                        linkurl.CssClass = "input_normal";
                        linkurl.ID = "linkurl_" + _countimage;
                        if (_adddata) { linkurl.Text = dr["chrPageImageLinkURL"].ToString(); }
                        fieldControl2.Controls.Add(linkurl);
                        fieldName2.Controls.Add(new LiteralControl("Image Click URL"));
                        InputRow2.Controls.Add(fieldName2);
                        InputRow2.Controls.Add(fieldControl2);
                        Section.Controls.Add(InputRow2);

                        //Image ALT
                        Panel InputRow3 = new Panel();
                        InputRow3.CssClass = "input_row";
                        Panel fieldName3 = new Panel();
                        fieldName3.CssClass = "field_name";
                        Panel fieldControl3 = new Panel();
                        fieldControl3.CssClass = "field_control";
                        TextBox imagealt = new TextBox();
                        imagealt.CssClass = "input_normal";
                        if (_adddata) { imagealt.Text = dr["chrPageImageAlt"].ToString(); }
                        imagealt.ID = "imagealt_" + _countimage;
                        fieldControl3.Controls.Add(imagealt);
                        fieldName3.Controls.Add(new LiteralControl("Image Alt"));
                        InputRow3.Controls.Add(fieldName3);
                        InputRow3.Controls.Add(fieldControl3);
                        Section.Controls.Add(InputRow3);

                        //Image Caption
                        Panel InputRow4 = new Panel();
                        InputRow4.CssClass = "input_row";
                        Panel fieldName4 = new Panel();
                        fieldName4.CssClass = "field_name";
                        Panel fieldControl4 = new Panel();
                        fieldControl4.CssClass = "field_control";
                        TextBox imagecaption = new TextBox();
                        imagecaption.CssClass = "input_normal";
                        if (_adddata) { imagecaption.Text = dr["chrPageImageCaption"].ToString(); }
                        imagecaption.ID = "imagecaption_" + _countimage;
                        fieldControl4.Controls.Add(imagecaption);
                        fieldName4.Controls.Add(new LiteralControl("Image Caption"));
                        InputRow4.Controls.Add(fieldName4);
                        InputRow4.Controls.Add(fieldControl4);
                        Section.Controls.Add(InputRow4);

                        //Image title
                        Panel InputRow5 = new Panel();
                        InputRow5.CssClass = "input_row";
                        Panel fieldName5 = new Panel();
                        fieldName5.CssClass = "field_name";
                        Panel fieldControl5 = new Panel();
                        fieldControl5.CssClass = "field_control";
                        TextBox imagetitle = new TextBox();
                        imagetitle.CssClass = "input_normal";
                        if (_adddata) { imagetitle.Text = dr["chrPageImageTitle"].ToString(); }
                        imagetitle.ID = "imagetitle_" + _countimage;
                        fieldControl5.Controls.Add(imagetitle);
                        fieldName5.Controls.Add(new LiteralControl("Image Title"));
                        InputRow5.Controls.Add(fieldName5);
                        InputRow5.Controls.Add(fieldControl5);
                        Section.Controls.Add(InputRow5);

                        //Image text
                        Panel InputRow6 = new Panel();
                        InputRow6.CssClass = "input_row";
                        Panel fieldName6 = new Panel();
                        fieldName6.CssClass = "field_name";
                        Panel fieldControl6 = new Panel();
                        fieldControl6.CssClass = "field_control";
                        TextBox imagetext = new TextBox();
                        imagetext.CssClass = "input_normal";
                        if (_adddata) { imagetext.Text = dr["chrPageImageText"].ToString(); }
                        imagetext.ID = "imagetext_" + _countimage;
                        fieldControl6.Controls.Add(imagetext);
                        fieldName6.Controls.Add(new LiteralControl("Image Text"));
                        InputRow6.Controls.Add(fieldName6);
                        InputRow6.Controls.Add(fieldControl6);
                        Section.Controls.Add(InputRow6);

                        contentPanel.Controls.Add(Section);
                        #endregion
                    }
                    numberOfImages.Value = _countimage.ToString();
                }
                else
                {
                    btnSave.Visible = false;
                }
            }

        }

        protected void Update_Click(object sender, EventArgs e)
        {

            //string _rootpath = Request.Url.GetLeftPart(UriPartial.Authority) + HttpRuntime.AppDomainAppVirtualPath;
            string _rootpath = FluentContentSettings.WebsiteRoot;
            if (_rootpath.Substring(_rootpath.Length - 1, 1) != "/") { _rootpath += "/"; }
            string _imgurl = "";
            updateMessage.Visible = false;

            //Loop through every text field and merge into database   
            for (int i = 1; i <= Convert.ToInt16(numberOfImages.Value); i++)
            {

                //First upload image  
                FileUpload filetoupload = this.contentPanel.FindControl("imageupload_" + i) as FileUpload;
                CheckBox overwriteimg = this.contentPanel.FindControl("overwrite_" + i) as CheckBox;
                Image imagecontrol = this.contentPanel.FindControl("image_" + i) as Image;
                HiddenField hdnDimensions = this.contentPanel.FindControl("hdnDimensions_" + i) as HiddenField;
                HiddenField hdnPageImageID = this.contentPanel.FindControl("hdnPageImageID_" + i) as HiddenField;

                if (filetoupload.PostedFile.FileName != null && filetoupload.PostedFile.FileName != "")
                {
                    //var dims = hdnDimensions.Value.Split(new char[] { '|' });

                    //int requiredWidth = dims[0].ToInt32();
                    //int requiredHeight = dims[1].ToInt32();

                    if (!ValidDimensions(filetoupload, hdnDimensions.Value))
                    {
                        // show error
                        errorMessage.Visible = true;
                        return;
                    }

                    string pathfilename = filetoupload.PostedFile.FileName;
                    string filename = Path.GetFileName(filetoupload.PostedFile.FileName);
                    string localpath = FluentContentSettings.ImageUploadLocation + "\\";
                    string localfilename = FluentContentSettings.ImageUploadLocation + "\\" + filename;
                    string filenameleft = Path.GetFileNameWithoutExtension(filename);
                    string ext = Path.GetExtension(filename);
                    if (overwriteimg.Checked)
                    {
                        filetoupload.PostedFile.SaveAs(localfilename);
                    }
                    else
                    {
                        if (File.Exists(@localfilename))
                        {
                            //Find next available filename
                            for (int c = 1; i <= 1000; i++)
                            {
                                if (!File.Exists(@filenameleft + c.ToString() + ext))
                                {
                                    localfilename = localpath + filenameleft + c.ToString() + ext;
                                    break;
                                }
                            }
                        }
                        filetoupload.PostedFile.SaveAs(localfilename);
                    }
                    imagecontrol.ImageUrl = _rootpath + FluentContentSettings.ImageUploadFolder + "/" + Path.GetFileName(localfilename);
                    imagecontrol.Visible = true;
                    _imgurl = Path.GetFileName(localfilename);
                }


                errorMessage.Visible = false;

                //Requires at least an image to save

                if (imagecontrol.ImageUrl != "")
                {
                    //ItemImage image = new ItemImage();
                    PageImage image = new PageImage();
                    image.ID = hdnPageImageID.Value.ToInt32();

                    image.PageID = btnSave.CommandArgument.ToInt32();// Convert.ToInt16(intItemID.Value);

                    image.ImageURL = _imgurl;

                    CheckBox deleteimg = this.contentPanel.FindControl("deleteimage_" + i) as CheckBox;
                    image.DeleteImage = deleteimg.Checked;
                    TextBox imagealt = this.contentPanel.FindControl("imagealt_" + i) as TextBox;
                    image.ImageAlt = imagealt.Text;
                    TextBox imagecaption = this.contentPanel.FindControl("imagecaption_" + i) as TextBox;
                    image.ImageCaption = imagecaption.Text;
                    TextBox imagetitle = this.contentPanel.FindControl("imagetitle_" + i) as TextBox;
                    image.ImageTitle = imagetitle.Text;
                    TextBox linkurl = this.contentPanel.FindControl("linkurl_" + i) as TextBox;
                    image.ImageLinkUrl = linkurl.Text;
                    TextBox imagetext = this.contentPanel.FindControl("imagetext_" + i) as TextBox;
                    image.ImageText = imagetext.Text;
                    HiddenField imagetypeid = this.contentPanel.FindControl("imagetype_" + i) as HiddenField;
                    image.ImageTypeID = Convert.ToInt16(imagetypeid.Value);


                    ProcessUpdatePageImage updateImage = new ProcessUpdatePageImage();
                    updateImage.PageImage = image;
                    updateImage.Invoke();

                    if (deleteimg.Checked) { imagecontrol.Visible = false; }

                    updateMessage.Visible = true;
                }
            }
        }

        private bool ValidDimensions(FileUpload fu, string dimensionData)
        {
            int width = 0, height = 0;
            char widthRestrict, heightRestrict;

            string[] dimData = dimensionData.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
            string[] dims = dimData[0].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            string[] restrictions = dimData[1].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            width = dims[0].ToInt32();
            height = dims[1].ToInt32();

            widthRestrict = restrictions[0][0];
            heightRestrict = restrictions[1][0];

            int invalidCount = 0;
            using (System.Drawing.Image image = System.Drawing.Image.FromStream(fu.PostedFile.InputStream))
            {
                switch (widthRestrict)
                {
                    // maximum
                    case 'm':
                        if (image.Width > width) invalidCount++;
                        break;
                    // minimum
                    case 'n':
                        if (image.Width < width) invalidCount++;
                        break;
                    // exact
                    case 'x':
                    default:
                        if (image.Width != width) invalidCount++;
                        break;
                }

                switch (heightRestrict)
                {
                    // maximum
                    case 'm':
                        if (image.Height > height) invalidCount++;
                        break;
                    // minimum
                    case 'n':
                        if (image.Height < height) invalidCount++;
                        break;
                    // exact
                    case 'x':
                    default:
                        if (image.Height != height) invalidCount++;
                        break;
                }


                return (invalidCount == 0);
            }
        }

        private string RestrictionString(string restrictionData)
        {
            switch (restrictionData)
            {
                case "m":
                    return "max ";
                case "n":
                    return "min ";
                default:
                    return String.Empty;
            }
        }
    }
}