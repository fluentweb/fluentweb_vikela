﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.Helpers;
using System.IO;
using System.Text.RegularExpressions;
using Common.Extensions;

namespace CMS.FluentControls
{
    public partial class ExistingImagesDropdown : System.Web.UI.UserControl
    {
        public string JSONData { get; set; }


        public string SelectedImage
        {
            get
            {
                return this.hdnSelectedExistingImage.Value;
            }
        }

        public void Initialise(string dimensionData)
        {
            string url = FluentContentSettings.ImageUploadLocation;
            Regex pattern = new Regex(@".{1,}\.(jpg|png|gif)", RegexOptions.IgnoreCase);
            var files = Directory.GetFiles(url, "*.*").Where(file => pattern.IsMatch(file));

            List<string> imageFiles = new List<string>();

            if (!files.Any()) JSONData = String.Empty;


            foreach (var file in files)
            {
                using (System.Drawing.Image i = System.Drawing.Image.FromFile(file))
                {

                    if (i == null) continue;
                    if (ValidImage(dimensionData, i))
                        imageFiles.Add(file);
                }
            }

            string itemFormat = "{{ text: \"{0}\", value: {1}, selected: false, description: \"{3}\", imageSrc: \"{2}\"}}";
            List<string> toReturn = new List<string>();

            if (imageFiles.Count == 0) JSONData = String.Empty;

            int count = 0;
            foreach (string file in imageFiles)
            {
                toReturn.Add(String.Format(itemFormat, Path.GetFileName(file).TrimDownTo(23, "..."),
                    count++,
                    ResolveUrl(String.Format("{0}/{1}/{2}", FluentContentSettings.WebsiteRoot, FluentContentSettings.ImageUploadFolder, Path.GetFileName(file))),
                    Path.GetFileName(file)));
            }

            JSONData = String.Format("[{0}]", String.Join(",", toReturn.ToArray()));
        }

        private bool ValidImage(string dimensionData, System.Drawing.Image image)
        {
            int width = 0, height = 0;
            char widthRestrict, heightRestrict;

            string[] dimData = dimensionData.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
            string[] dims = dimData[0].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            string[] restrictions = dimData[1].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            width = dims[0].ToInt32();
            height = dims[1].ToInt32();

            widthRestrict = restrictions[0][0];
            heightRestrict = restrictions[1][0];

            int invalidCount = 0;

            switch (widthRestrict)
            {
                // maximum
                case 'm':
                    if (image.Width > width) invalidCount++;
                    break;
                // minimum
                case 'n':
                    if (image.Width < width) invalidCount++;
                    break;
                // exact
                case 'x':
                default:
                    if (image.Width != width) invalidCount++;
                    break;
            }

            switch (heightRestrict)
            {
                // maximum
                case 'm':
                    if (image.Height > height) invalidCount++;
                    break;
                // minimum
                case 'n':
                    if (image.Height < height) invalidCount++;
                    break;
                // exact
                case 'x':
                default:
                    if (image.Height != height) invalidCount++;
                    break;
            }


            return (invalidCount == 0);
        }
    }
}