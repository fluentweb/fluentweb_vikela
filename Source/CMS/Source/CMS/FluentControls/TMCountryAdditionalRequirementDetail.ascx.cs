﻿using FluentContent.DAC;
using FluentContent.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Extensions;
using CMS.Helpers;
using FLContentSiteAssist.BusinessLogic;
using FluentContent.Entities.Helpers;
using System.Collections.Specialized;
using System.IO;
using System.Drawing.Imaging;
using System.Data;
using Common.Url;
using Common.Session;
using FluentContent.Entities.Enums;

namespace CMS.FluentControls
{
    public partial class TMCountryAdditionalRequirementDetail : System.Web.UI.UserControl
    {
        public int CountryAdditionalReqId { get { return Request.GetQueryString<int>("countryadditionalrequirementid", -1); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillDrowpDown();
                if (CountryAdditionalReqId != -1)
                {
                    FillScreen(CountryAdditionalReqId);
                }
            }
        }

        private void FillScreen(int countryID)
        {
            TMCountryAdditionalRequirement tmcar = DAC_TMCountryAdditionalRequirement.GetCountryAdditionalRequirement(CountryAdditionalReqId);
            if (tmcar == null) return;
            ddcountryName.SelectedValue = tmcar.CountryID.ToString();
            ddQuestionType.SelectedValue = tmcar.QuestionType.ToString();
            txtRequirementQuestion.Text = tmcar.RequirementQuestion.ToString();
        }

        private void FillDrowpDown()
        {
            //Country dd
            ddcountryName.DataSource = DAC_TMCountry.GetCountryByCountry();
            ddcountryName.DataTextField = "chrCountry";
            ddcountryName.DataValueField = "intCountryID";
            ddcountryName.DataBind();

            //Translat dd
            ddQuestionType.DataSource = DAC_TMCountryAdditionalRequirementChoiceType.GetCountryAdditionalRequirementChoiceType();
            ddQuestionType.DataTextField = "chrQuestionTypeDesc";
            ddQuestionType.DataValueField = "chrQuestionType";
            ddQuestionType.DataBind();

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void Save()
        {
            TMCountryAdditionalRequirement tmcar = new TMCountryAdditionalRequirement();
            tmcar.CountryID = ddcountryName.SelectedValue.ToInt32();
            tmcar.RequirementQuestion = txtRequirementQuestion.Text.Trim();
            tmcar.QuestionType = ddQuestionType.SelectedValue.ToString();
            tmcar.CountryAdditionalRequirementID = CountryAdditionalReqId;

            if (CountryAdditionalReqId > 0)
            {
                tmcar = DAC_TMCountryAdditionalRequirement.Update(tmcar);
                if (tmcar == null)
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Update failed!','warning');", true);
                else
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Country successfully updated.','success');", true);
            }
            else
            {
                tmcar = DAC_TMCountryAdditionalRequirement.Insert(tmcar);
                if (tmcar == null)
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Create failed!','warning');", true);
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('New country successfully created.  You may now create another...','success');", true);
                    ClearPage();
                }
            }
        }

        private void ClearPage()
        {
            ddcountryName.SelectedIndex = 0;
            ddQuestionType.SelectedIndex = 0;
            txtRequirementQuestion.Text = string.Empty;
          
        }
    }
}