﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Extensions;
using FLContentSiteAssist.BusinessLogic;
using FLContentSiteAssist.Common;
using System.Data;
using CMS.Helpers;
using System.IO;
using FluentContent.DAC;
using FluentContent.Entities;
using FluentContent.Entities.Enums;

namespace CMS.FluentControls
{
    public partial class ItemCustomControl : System.Web.UI.UserControl
    {
        private DataRow dr;

        public int ItemID { get { return Request.GetQueryString<int>("itemid", 0); } }
        public int ItemTypeID { get { return Request.GetQueryString<int>("itemtype", -1); } }
        public string RelateType { get { return Request.GetQueryString<string>("relatetype", String.Empty); } }
        public int ParentID { get { return Request.GetQueryString<int>("parentid", 0); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //set screen for item type
                if (ItemTypeID != -1)
                    SetScreen(ItemTypeID);

                //If we are editing then fill screen for this item
                if (RelateType == String.Empty && ItemID > 0)
                {
                    FillScreen(ItemID);
                    btnCreate.Visible = false;
                }
                else
                {
                    btnUpdate.Visible = false;
                }

            }
        }

        protected void Create_Click(object sender, EventArgs e)
        {
            string _rootpath = Request.Url.GetLeftPart(UriPartial.Authority) + HttpRuntime.AppDomainAppVirtualPath;
            if (_rootpath.Substring(_rootpath.Length - 1, 1) != "/") { _rootpath += "/"; }

            string _imgurl = "";
            string _img2url = "";
            string _fileurl = "";
            string _file2url = "";
            //updateMessage.Visible = false;
            // validate item url
            string sItemURL = itemURL.Text.Trim() == String.Empty ? itemTitle.Text.CleanForSEO() : itemURL.Text.CleanForSEO();

            if (DAC_Item.ItemURLExists(sItemURL, new FluentContent.Helpers.ItemParameters { ItemID = -1, ItemTypeID = ItemTypeID }))
            {
                itemURL.Text = sItemURL;

                if (itemURL.Visible) // show error if itemURL is editable
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Url already in use, please select a different URL','error');", true);
                    return;
                }
                else // generate new url
                {
                    int counter = 0;

                    while (DAC_Item.ItemURLExists(sItemURL, new FluentContent.Helpers.ItemParameters { ItemID = -1, ItemTypeID = ItemTypeID }))
                    {
                        sItemURL = String.Format("{0}{1}", sItemURL, counter++);
                    }
                }
            }


            //First upload image 1
            if (divImage1.Visible && ImageUpload.PostedFile != null && ImageUpload.PostedFile.FileName != "")
            {
                // validate image
                if (!ValidDimensions(ImageUpload, hdnImage1Dims.Value.Trim()))
                {
                    // show error
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Invalid image dimensions, please check requirements next to each image','error');", true);

                    return;
                }

                string pathfilename = ImageUpload.PostedFile.FileName;
                string filename = Path.GetFileName(ImageUpload.PostedFile.FileName);
                string localpath = FluentContentSettings.ImageUploadLocation + "\\";

                string localfilename = FluentContentSettings.ImageUploadLocation + "\\" + filename;
                string filenameleft = Path.GetFileNameWithoutExtension(filename);
                string ext = Path.GetExtension(filename);
                if (overwriteImg.Checked)
                {
                    ImageUpload.PostedFile.SaveAs(localfilename);
                }
                else
                {
                    if (File.Exists(@localfilename))
                    {
                        //Find next available filename
                        for (int i = 1; i <= 1000; i++)
                        {
                            if (!File.Exists(@filenameleft + i.ToString() + ext))
                            {
                                localfilename = localpath + filenameleft + i.ToString() + ext;
                                break;
                            }
                        }
                    }
                    ImageUpload.PostedFile.SaveAs(localfilename);
                }
                image.ImageUrl = _rootpath + FluentContentSettings.ImageUploadFolder + "/" + Path.GetFileName(localfilename);
                image.Visible = true;

                _imgurl = Path.GetFileName(localfilename);
            }

            //check if user is selecting existing image
            if (UCExistingImageDropdown1.SelectedImage != String.Empty)
            {
                _imgurl = UCExistingImageDropdown1.SelectedImage;
            }


            //Upload image 2
            if (divImage2.Visible && Image2Upload.PostedFile != null && Image2Upload.PostedFile.FileName != "")
            {
                // validate image
                if (!ValidDimensions(Image2Upload, hdnImage2Dims.Value.Trim()))
                {
                    // show error
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Invalid image dimensions, please check requirements next to each image','error');", true);

                    return;
                }

                string pathfilename = Image2Upload.PostedFile.FileName;
                string filename = Path.GetFileName(Image2Upload.PostedFile.FileName);
                string localpath = FluentContentSettings.ImageUploadLocation + "\\";
                string localfilename = FluentContentSettings.ImageUploadLocation + "\\" + filename;
                string filenameleft = Path.GetFileNameWithoutExtension(filename);
                string ext = Path.GetExtension(filename);
                if (overwriteImg2.Checked)
                {
                    Image2Upload.PostedFile.SaveAs(localfilename);
                }
                else
                {
                    if (File.Exists(@localfilename))
                    {
                        //Find next available filename
                        for (int i = 1; i <= 1000; i++)
                        {
                            if (!File.Exists(@filenameleft + i.ToString() + ext))
                            {
                                localfilename = localpath + filenameleft + i.ToString() + ext;
                                break;
                            }
                        }
                    }
                    Image2Upload.PostedFile.SaveAs(localfilename);
                }
                image2.ImageUrl = _rootpath + FluentContentSettings.ImageUploadFolder + "/" + Path.GetFileName(localfilename);
                image2.Visible = true;

                _img2url = Path.GetFileName(localfilename);
            }

            //check if user is selecting existing image
            if (UCExistingImageDropdown2.SelectedImage != String.Empty)
            {
                _img2url = UCExistingImageDropdown2.SelectedImage;
            }


            //Upload file 1
            if (divFileUpload.Visible && FileUpload.PostedFile != null && FileUpload.PostedFile.FileName != "")
            {
                string pathfilename = FileUpload.PostedFile.FileName;
                string filename = Path.GetFileName(FileUpload.PostedFile.FileName);
                string localpath = FluentContentSettings.DocumentUploadLocation + "\\";
                string localfilename = FluentContentSettings.DocumentUploadLocation + "\\" + filename;
                string filenameleft = Path.GetFileNameWithoutExtension(filename);
                string ext = Path.GetExtension(filename);
                if (overwriteFile.Checked)
                {
                    FileUpload.PostedFile.SaveAs(localfilename);
                }
                else
                {
                    if (File.Exists(@localfilename))
                    {
                        //Find next available filename
                        for (int i = 1; i <= 1000; i++)
                        {
                            if (!File.Exists(@filenameleft + i.ToString() + ext))
                            {
                                localfilename = localpath + filenameleft + i.ToString() + ext;
                                break;
                            }
                        }
                    }
                    FileUpload.PostedFile.SaveAs(localfilename);
                }

                _fileurl = Path.GetFileName(localfilename);

            }


            //Upload file 2
            if (divFile2Upload.Visible && File2Upload.PostedFile != null && File2Upload.PostedFile.FileName != "")
            {
                string pathfilename = File2Upload.PostedFile.FileName;
                string filename = Path.GetFileName(File2Upload.PostedFile.FileName);
                string localpath = FluentContentSettings.DocumentUploadLocation + "\\";
                string localfilename = FluentContentSettings.DocumentUploadLocation + "\\" + filename;
                string filenameleft = Path.GetFileNameWithoutExtension(filename);
                string ext = Path.GetExtension(filename);
                if (overwriteFile2.Checked)
                {
                    File2Upload.PostedFile.SaveAs(localfilename);
                }
                else
                {
                    if (File.Exists(@localfilename))
                    {
                        //Find next available filename
                        for (int i = 1; i <= 1000; i++)
                        {
                            if (!File.Exists(@filenameleft + i.ToString() + ext))
                            {
                                localfilename = localpath + filenameleft + i.ToString() + ext;
                                break;
                            }
                        }
                    }
                    File2Upload.PostedFile.SaveAs(localfilename);
                }

                _file2url = Path.GetFileName(localfilename);

            }


            //Requires at least a title
            if (itemTitle.Text.Trim() != "")
            {
                //Now insert the rest of the data into the database
                ProcessInsertItem insertitem = new ProcessInsertItem();
                Item item = new Item();
                item.ItemTypeID = ItemTypeID;
                item.Title = itemTitle.Text;
                item.Text1 = text1.Text;
                item.Text2 = text2.Text;
                item.Text3 = text3.Text;
                item.Text4 = text4.Text;
                item.Text5 = text5.Text;
                item.Text6 = text6.Text;
                item.Text7 = text7.Text;
                item.Text8 = text8.Text;
                item.Text9 = text9.Text;
                item.Text10 = text10.Text;
                item.Text11 = text11.Text;
                item.Text12 = text12.Text;
                item.DataType = ddlDataType.SelectedValue;
                item.Checkbox1 = chkCheckbox1.Checked;
                item.Checkbox2 = chkCheckbox2.Checked;
                item.Checkbox3 = chkCheckbox3.Checked;

                decimal resultPrice;
                if (decimal.TryParse(price1.Text, out resultPrice))
                { item.price1 = resultPrice; }
                else
                { item.price1 = 0; }
                if (decimal.TryParse(price2.Text, out resultPrice))
                { item.price2 = resultPrice; }
                else
                { item.price2 = 0; }
                if (decimal.TryParse(price3.Text, out resultPrice))
                { item.price3 = resultPrice; }
                else
                { item.price3 = 0; }

                int resultvalue;
                if (int.TryParse(value1.Text, out resultvalue))
                { item.value1 = resultvalue; }
                else
                { item.value1 = 0; }

                item.ItemURL = sItemURL; //itemURL.Text.Trim() == String.Empty ? itemTitle.Text.CleanForSEO() : itemURL.Text.CleanForSEO();
                itemURL.Text = item.ItemURL;

                item.ImageURL = _imgurl;
                item.ImageTitle = imageTitle.Text.Trim();
                item.ImageAlt = imageAlt.Text;
                item.ImageCaption = imageCaption.Text;
                item.Image2URL = _img2url;
                item.Image2Title = image2Title.Text;
                item.Image2Alt = image2Alt.Text;
                item.Image2Caption = image2Caption.Text;
                item.FileURL = _fileurl;
                item.FileTypeID = Convert.ToInt16(fileType.SelectedValue);
                item.File2URL = _file2url;
                item.File2TypeID = Convert.ToInt16(file2Type.SelectedValue);
                item.Sequence = Convert.ToInt16(sequenceID.SelectedValue);
                item.Enabled = enabled.Checked;
                item.Link = itemLink.Text;

                if (datepickerfr.Text != "")
                {
                    item.DateFrom = Convert.ToDateTime(datepickerfr.Text);
                }
                else
                {
                    item.DateFrom = Convert.ToDateTime("01/01/1900");
                }
                if (datepickerto.Text != "")
                {
                    item.DateTo = Convert.ToDateTime(datepickerto.Text);
                }
                else
                {
                    item.DateTo = Convert.ToDateTime("01/01/1900");
                }

                if (timeFrom.Text != "")
                {
                    item.TimeFrom = timeFrom.Text;
                }
                else
                {
                    item.TimeFrom = "00:00";
                }

                if (timeTo.Text != "")
                {
                    item.TimeTo = timeTo.Text;
                }
                else
                {
                    item.TimeTo = "00:00";
                }


                if (RelateType != String.Empty)
                {
                    item.ParentItemID = ParentID;
                }

                insertitem.Item = item;
                insertitem.Invoke();

                // item entities logic
                if (divLinkedEntity.Visible)
                {
                    // handle item entity inserts after item was inserted
                    List<Entity> selectedEntities = new List<Entity>();
                    foreach (ListViewDataItem i in lvItemLinkedEntities.Items)
                    {
                        CheckBox chkEntityRelated = i.FindControl("chkEntityRelated") as CheckBox;

                        if (chkEntityRelated.Checked)
                        {
                            HiddenField hdnEntityID = i.FindControl("hdnEntityID") as HiddenField;
                            TextBox txtDateLinked = i.FindControl("txtDateLinked") as TextBox;
                            TextBox txtDateExpires = i.FindControl("txtDateExpires") as TextBox;
                            DateTime dt = txtDateLinked.Text.GetDateTime("dd/MM/yyyy");
                            DateTime dtExpires = txtDateExpires.Text.GetDateTime("dd/MM/yyyy");
                            selectedEntities.Add(new Entity { ID = hdnEntityID.Value.ToInt32(), DateAttachedToCurrentItem = dt, DateExpires = dtExpires });
                        }
                    }

                    // insert item entities
                    DAC_Item.InsertItemEntities(selectedEntities, insertitem.Item.ItemID);
                }

                //Now clear the screen and show the success message
                ClearScreen();

                //If this is a related item then the RelatedItem record must be created                
                if (RelateType != String.Empty)
                {
                    RelatedItem relateditem = new RelatedItem();
                    //Get the created item id
                    relateditem.ChildItemID = insertitem.Item.ItemID;
                    relateditem.ParentItemID = ParentID;
                    relateditem.RelationshipType = RelateType;
                    relateditem.Sequence = Convert.ToInt16(sequenceID.SelectedValue);

                    ProcessInsertRelatedItem insertrelateditem = new ProcessInsertRelatedItem();
                    insertrelateditem.RelatedItem = relateditem;
                    insertrelateditem.Invoke();

                    Response.Redirect(String.Format("~/RelatedItemManagement.aspx?itemid={0}&relationtype={1}&childitemtype={2}&childcreate=True&message=created", ParentID,
                        RelateType, ItemTypeID));
                }

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('New item successfully created.  You may now create another...','success');", true);
            }

        }

        protected void Update_Click(object sender, EventArgs e)
        {
            //string _rootpath = Request.Url.GetLeftPart(UriPartial.Authority) + HttpRuntime.AppDomainAppVirtualPath;
            string _rootpath = FluentContentSettings.WebsiteRoot;
            if (_rootpath.Substring(_rootpath.Length - 1, 1) != "/") { _rootpath += "/"; }

            string _imgurl = "";
            string _img2url = "";
            string _fileurl = "";
            string _file2url = "";

            // validate item url
            string sItemURL = itemURL.Text.Trim() == String.Empty ? itemTitle.Text.CleanForSEO() : itemURL.Text.CleanForSEO();

            if (DAC_Item.ItemURLExists(sItemURL, new FluentContent.Helpers.ItemParameters { ItemID = Convert.ToInt32(intItemID.Value), ItemTypeID = ItemTypeID }))
            {
                if (itemURL.Visible) // show error if itemURL is editable
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Url already in use, please select a different URL','error');", true);
                    return;
                }
                else // generate new url
                {
                    int counter = 0;

                    while (DAC_Item.ItemURLExists(sItemURL, new FluentContent.Helpers.ItemParameters { ItemID = Convert.ToInt32(intItemID.Value), ItemTypeID = ItemTypeID }))
                    {
                        sItemURL = String.Format("{0}{1}", sItemURL, counter++);
                    }
                }
            }



            //First upload image
            if (divImage1.Visible = true && ImageUpload.PostedFile != null && ImageUpload.PostedFile.FileName != "")
            {
                // validate image
                if (!ValidDimensions(ImageUpload, hdnImage1Dims.Value.Trim()))
                {
                    // show error
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Invalid image dimensions, please check requirements next to each image','error');", true);
                    return;
                }

                string pathfilename = ImageUpload.PostedFile.FileName;
                string filename = Path.GetFileName(ImageUpload.PostedFile.FileName);
                string localpath = FluentContentSettings.ImageUploadLocation + "\\";
                string localfilename = FluentContentSettings.ImageUploadLocation + "\\" + filename;
                string filenameleft = Path.GetFileNameWithoutExtension(filename);
                string ext = Path.GetExtension(filename);
                if (overwriteImg.Checked)
                {
                    ImageUpload.PostedFile.SaveAs(localfilename);
                }
                else
                {
                    if (File.Exists(@localfilename))
                    {
                        //Find next available filename
                        for (int i = 1; i <= 1000; i++)
                        {
                            if (!File.Exists(@filenameleft + i.ToString() + ext))
                            {
                                localfilename = localpath + filenameleft + i.ToString() + ext;
                                break;
                            }
                        }
                    }
                    ImageUpload.PostedFile.SaveAs(localfilename);
                }
                _imgurl = Path.GetFileName(localfilename);
            }

            //check if user is selecting existing image
            if (UCExistingImageDropdown1.SelectedImage != String.Empty)
            {
                _imgurl = UCExistingImageDropdown1.SelectedImage;
            }

            //Image 2
            if (divImage2.Visible = true && Image2Upload.PostedFile != null && Image2Upload.PostedFile.FileName != "")
            {
                // validate image
                if (!ValidDimensions(Image2Upload, hdnImage2Dims.Value.Trim()))
                {
                    // show error
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Invalid image dimensions, please check requirements next to each image','error');", true);
                    return;
                }

                string pathfilename = Image2Upload.PostedFile.FileName;
                string filename = Path.GetFileName(Image2Upload.PostedFile.FileName);
                string localpath = FluentContentSettings.ImageUploadLocation + "\\";
                string localfilename = FluentContentSettings.ImageUploadLocation + "\\" + filename;
                string filenameleft = Path.GetFileNameWithoutExtension(filename);
                string ext = Path.GetExtension(filename);
                if (overwriteImg2.Checked)
                {
                    Image2Upload.PostedFile.SaveAs(localfilename);
                }
                else
                {
                    if (File.Exists(@localfilename))
                    {
                        //Find next available filename
                        for (int i = 1; i <= 1000; i++)
                        {
                            if (!File.Exists(@filenameleft + i.ToString() + ext))
                            {
                                localfilename = localpath + filenameleft + i.ToString() + ext;
                                break;
                            }
                        }
                    }
                    Image2Upload.PostedFile.SaveAs(localfilename);
                }
                _img2url = Path.GetFileName(localfilename);
            }


            //check if user is selecting existing image
            if (UCExistingImageDropdown2.SelectedImage != String.Empty)
            {
                _img2url = UCExistingImageDropdown2.SelectedImage;
            }

            //Upload file 
            if (divFileUpload.Visible && FileUpload.PostedFile != null && FileUpload.PostedFile.FileName != "")
            {
                string pathfilename = FileUpload.PostedFile.FileName;
                string filename = Path.GetFileName(FileUpload.PostedFile.FileName);
                string localpath = FluentContentSettings.DocumentUploadLocation + "\\";
                string localfilename = FluentContentSettings.DocumentUploadLocation + "\\" + filename;
                string filenameleft = Path.GetFileNameWithoutExtension(filename);
                string ext = Path.GetExtension(filename);
                if (overwriteFile.Checked)
                {
                    FileUpload.PostedFile.SaveAs(localfilename);
                }
                else
                {
                    if (File.Exists(@localfilename))
                    {
                        //Find next available filename
                        for (int i = 1; i <= 1000; i++)
                        {
                            if (!File.Exists(@filenameleft + i.ToString() + ext))
                            {
                                localfilename = localpath + filenameleft + i.ToString() + ext;
                                break;
                            }
                        }
                    }
                    FileUpload.PostedFile.SaveAs(localfilename);
                }
                _fileurl = Path.GetFileName(localfilename);

            }

            //Upload file 2
            if (divFile2Upload.Visible && File2Upload.PostedFile != null && File2Upload.PostedFile.FileName != "")
            {
                string pathfilename = File2Upload.PostedFile.FileName;
                string filename = Path.GetFileName(File2Upload.PostedFile.FileName);
                string localpath = FluentContentSettings.DocumentUploadLocation + "\\";
                string localfilename = FluentContentSettings.DocumentUploadLocation + "\\" + filename;
                string filenameleft = Path.GetFileNameWithoutExtension(filename);
                string ext = Path.GetExtension(filename);
                if (overwriteFile2.Checked)
                {
                    File2Upload.PostedFile.SaveAs(localfilename);
                }
                else
                {
                    if (File.Exists(@localfilename))
                    {
                        //Find next available filename
                        for (int i = 1; i <= 1000; i++)
                        {
                            if (!File.Exists(@filenameleft + i.ToString() + ext))
                            {
                                localfilename = localpath + filenameleft + i.ToString() + ext;
                                break;
                            }
                        }
                    }
                    File2Upload.PostedFile.SaveAs(localfilename);
                }
                _file2url = Path.GetFileName(localfilename);

            }

            //Requires at least a title
            if (itemTitle.Text.Trim() != "")
            {
                //Now insert the rest of the data into the database
                ProcessUpdateItemComplete updateitem = new ProcessUpdateItemComplete();
                Item item = new Item();
                item.ItemTypeID = ItemTypeID;
                item.ItemID = Convert.ToInt16(intItemID.Value);
                item.Title = itemTitle.Text;
                item.Text1 = text1.Text;
                item.Text2 = text2.Text;
                item.Text3 = text3.Text;
                item.Text4 = text4.Text;
                item.Text5 = text5.Text;
                item.Text6 = text6.Text;
                item.Text7 = text7.Text;
                item.Text8 = text8.Text;
                item.Text9 = text9.Text;
                item.Text10 = text10.Text;
                item.Text11 = text11.Text;
                item.Text12 = text12.Text;
                item.DataType = ddlDataType.SelectedValue;
                item.Checkbox1 = chkCheckbox1.Checked;
                item.Checkbox2 = chkCheckbox2.Checked;
                item.Checkbox3 = chkCheckbox3.Checked;

                decimal resultPrice;
                if (decimal.TryParse(price1.Text, out resultPrice))
                { item.price1 = resultPrice; }
                else
                { item.price1 = 0; }
                if (decimal.TryParse(price2.Text, out resultPrice))
                { item.price2 = resultPrice; }
                else
                { item.price2 = 0; }
                if (decimal.TryParse(price3.Text, out resultPrice))
                { item.price3 = resultPrice; }
                else
                { item.price3 = 0; }

                int resultvalue;
                if (int.TryParse(value1.Text, out resultvalue))
                { item.value1 = resultvalue; }
                else
                { item.value1 = 0; }

                item.ItemURL = sItemURL; //itemURL.Text.Trim() == String.Empty ? itemTitle.Text.CleanForSEO() : itemURL.Text.CleanForSEO();
                itemURL.Text = item.ItemURL;
                item.ImageURL = _imgurl;
                item.ImageTitle = imageTitle.Text.Trim();
                item.ImageAlt = imageAlt.Text;
                item.ImageCaption = imageCaption.Text;
                item.Image2URL = _img2url;
                item.Image2Title = image2Title.Text;
                item.Image2Alt = image2Alt.Text;
                item.Image2Caption = image2Caption.Text;
                item.FileURL = _fileurl;
                item.FileTypeID = Convert.ToInt16(fileType.SelectedValue);
                item.File2URL = _file2url;
                item.File2TypeID = Convert.ToInt16(file2Type.SelectedValue);
                item.Sequence = Convert.ToInt16(sequenceID.SelectedValue);
                item.Enabled = enabled.Checked;
                item.Link = itemLink.Text;
                item.DeleteImage = deleteImage.Checked;
                item.DeleteImage2 = deleteImage2.Checked;
                item.DeleteFile = deleteFile.Checked;
                item.DeleteFile2 = deleteFile2.Checked;
                if (datepickerfr.Text != "")
                {
                    item.DateFrom = Convert.ToDateTime(datepickerfr.Text);
                }
                else
                {
                    item.DateFrom = Convert.ToDateTime("01/01/1900");
                }
                if (datepickerto.Text != "")
                {
                    item.DateTo = Convert.ToDateTime(datepickerto.Text);
                }
                else
                {
                    item.DateTo = Convert.ToDateTime("01/01/1900");
                }
                if (RelateType != String.Empty)
                {
                    item.ParentItemID = ParentID;
                }

                if (timeFrom.Text != "")
                {
                    item.TimeFrom = timeFrom.Text;
                }
                else
                {
                    item.TimeFrom = null;//"00:00";
                }

                if (timeTo.Text != "")
                {
                    item.TimeTo = timeTo.Text;
                }
                else
                {
                    item.TimeTo = null; //"00:00";
                }

                updateitem.Item = item;
                updateitem.Invoke();

                // item entities logic
                if (divLinkedEntity.Visible)
                {
                    // handle item entity inserts after item was inserted
                    List<Entity> selectedEntities = new List<Entity>();
                    foreach (ListViewDataItem i in lvItemLinkedEntities.Items)
                    {
                        CheckBox chkEntityRelated = i.FindControl("chkEntityRelated") as CheckBox;

                        if (chkEntityRelated.Checked)
                        {
                            HiddenField hdnEntityID = i.FindControl("hdnEntityID") as HiddenField;
                            TextBox txtDateLinked = i.FindControl("txtDateLinked") as TextBox;
                            TextBox txtDateExpires = i.FindControl("txtDateExpires") as TextBox;
                            DateTime dt = txtDateLinked.Text.GetDateTime("dd/MM/yyyy");
                            DateTime dtExpires = txtDateExpires.Text.GetDateTime("dd/MM/yyyy");
                            selectedEntities.Add(new Entity { ID = hdnEntityID.Value.ToInt32(), DateAttachedToCurrentItem = dt, DateExpires = dtExpires });
                        }
                    }

                    // insert item entities
                    DAC_Item.InsertItemEntities(selectedEntities, item.ItemID);
                }

                SetScreen(ItemTypeID);
                FillScreen(item.ItemID);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Successfully updated','success');", true);

            }
        }

        private void SetScreen(int itemtypeid)
        {

            //Need to customise the screen for item type
            ProcessGetItemType itemType = new ProcessGetItemType();
            Item item = new Item();
            item.IsAdmin = (this.Page.Master as CmsMaster).UserSession.Role.CMSAccessLevel == CMSAccessLevel.Full;
            item.ItemTypeID = itemtypeid;
            itemType.Item = item;
            itemType.Invoke();

            if (itemType.ResultSet.Tables[0].Rows.Count > 0)
            {
                DataRow itemData = itemType.ResultSet.Tables[0].Rows[0];

                labelTitle.Text = itemData["chrTitleName"].ToString();

                if (itemData["bitImageRequired"].ToString() == "True")
                {
                    divImage1.Visible = true;
                    line_divImage1.Visible = true;
                    labelImage1Name.Text = itemData["chrItemImageName"].ToString();
                    image_instr.Text = String.Format(".jpg .png .gif (w:{0}px {1}h:{2}px {3})", itemData["intWidth"], RestrictionString(itemData["chrImage1WidthRestrict"].ToString()),
                        itemData["intHeight"], RestrictionString(itemData["chrImage1HeightRestrict"].ToString()));

                    hdnImage1Dims.Value = String.Format("{0},{1}|{2},{3}", itemData["intWidth"], itemData["intHeight"], itemData["chrImage1WidthRestrict"], itemData["chrImage1HeightRestrict"]);

                    UCExistingImageDropdown1.Initialise(hdnImage1Dims.Value);
                }

                if (itemData["bitImage2Required"].ToString() == "True")
                {
                    divImage2.Visible = true;
                    line_divImage2.Visible = true;
                    labelImage2Name.Text = itemData["chrItemImage2Name"].ToString();
                    image2_instr.Text = String.Format(".jpg .png .gif (w:{0}px {1}h:{2}px {3})", itemData["intWidth2"], RestrictionString(itemData["chrImage2WidthRestrict"].ToString()),
                         itemData["intHeight2"], RestrictionString(itemData["chrImage2HeightRestrict"].ToString()));

                    hdnImage2Dims.Value = String.Format("{0},{1}|{2},{3}", itemData["intWidth2"], itemData["intHeight2"], itemData["chrImage2WidthRestrict"], itemData["chrImage2HeightRestrict"]);

                    UCExistingImageDropdown2.Initialise(hdnImage2Dims.Value);
                }

                if (itemData["bitFileUploadRequired"].ToString() == "True")
                {
                    divFileUpload.Visible = true;
                    labelFileUpload.Text = itemData["chrFileName"].ToString();
                    link_divFileUpload.Visible = true;
                }

                if (itemData["bitFile2UploadRequired"].ToString() == "True")
                {
                    divFile2Upload.Visible = true;
                    labelFile2Upload.Text = itemData["chrFile2Name"].ToString();
                    link_divFile2Upload.Visible = true;
                }

                if (itemData["bitDatesRequired"].ToString() == "True")
                {
                    divDates.Visible = true;
                    labelDateFrom.Text = itemData["chrStartDateName"].ToString();
                    labelTimeFrom.Text = itemData["chrStartTimeName"].ToString();
                    labelDateTo.Text = itemData["chrExpireDateName"].ToString();
                    labelTimeTo.Text = itemData["chrExpireTimeName"].ToString();
                    line_divDates.Visible = true;
                }

                if (itemData["bitItemURLRequired"].ToString() == "True")
                {
                    divURL.Visible = true;
                    labelURL.Text = itemData["chrItemURLName"].ToString();
                    line_divURL.Visible = true;
                }

                if (itemData["bitItemLinkRequired"].ToString() == "True")
                {
                    divLink.Visible = true;
                    labelLink.Text = itemData["chrItemLinkName"].ToString();
                    line_divLink.Visible = true;
                }

                if (itemData["bitText1Required"].ToString() == "True")
                {
                    divText1.Visible = true;
                    labelText1.Text = itemData["chrText1Name"].ToString();
                    line_divText1.Visible = true;
                }

                if (itemData["bitText2Required"].ToString() == "True")
                {
                    divText2.Visible = true;
                    labelText2.Text = itemData["chrText2Name"].ToString();
                    line_divText2.Visible = true;
                }

                if (itemData["bitText3Required"].ToString() == "True")
                {
                    divText3.Visible = true;
                    labelText3.Text = itemData["chrText3Name"].ToString();
                    line_divText3.Visible = true;
                }

                if (itemData["bitText4Required"].ToString() == "True")
                {
                    divText4.Visible = true;
                    labelText4.Text = itemData["chrText4Name"].ToString();
                    line_divText4.Visible = true;
                }

                if (itemData["bitText5Required"].ToString() == "True")
                {
                    divText5.Visible = true;
                    labelText5.Text = itemData["chrText5Name"].ToString();
                    line_divText4.Visible = true;
                }

                if (itemData["bitText6Required"].ToString() == "True")
                {
                    divText6.Visible = true;
                    labelText6.Text = itemData["chrText6Name"].ToString();
                    line_divText6.Visible = true;
                }

                if (itemData["bitText7Required"].ToString() == "True")
                {
                    divText7.Visible = true;
                    labelText7.Text = itemData["chrText7Name"].ToString();
                    line_divText7.Visible = true;
                }

                if (itemData["bitText8Required"].ToString() == "True")
                {
                    divText8.Visible = true;
                    labelText8.Text = itemData["chrText8Name"].ToString();
                    line_divText8.Visible = true;
                }

                if (itemData["bitText9Required"].ToString() == "True")
                {
                    divText9.Visible = true;
                    labelText9.Text = itemData["chrText9Name"].ToString();
                    line_divText9.Visible = true;
                }

                if (itemData["bitText10Required"].ToString() == "True")
                {
                    divText10.Visible = true;
                    labelText10.Text = itemData["chrText10Name"].ToString();
                    line_divText10.Visible = true;
                }

                if (itemData["bitText11Required"].ToString() == "True")
                {
                    divText11.Visible = true;
                    labelText11.Text = itemData["chrText11Name"].ToString();
                    line_divText11.Visible = true;
                }

                if (itemData["bitText12Required"].ToString() == "True")
                {
                    divText12.Visible = true;
                    labelText12.Text = itemData["chrText12Name"].ToString();
                    line_divText12.Visible = true;
                }

                if (itemData["intLinkedEntityTypeID"].ToInt32() != 0)
                {
                    divLinkedEntity.Visible = true;
                    labelLinkedEntity.Text = itemData["chrLinkedEntityTypeName"].ToString();

                    lvItemLinkedEntities.DataSource = DAC_Entity.GetByTypeID(itemData["intLinkedEntityTypeID"].ToInt32());
                    lvItemLinkedEntities.DataBind();
                }

                if (itemData["bitPrice1Required"].ToString() == "True")
                {
                    divPrice1.Visible = line_divPrice1.Visible = true;
                    labelPrice1.Text = itemData["chrPrice1Name"].ToString();
                }

                if (itemData["bitPrice2Required"].ToString() == "True")
                {
                    divPrice2.Visible = true;
                    labelPrice2.Text = itemData["chrPrice2Name"].ToString();
                    line_divPrice2.Visible = true;

                }

                if (itemData["bitPrice3Required"].ToString() == "True")
                {
                    divPrice3.Visible = true;
                    labelPrice3.Text = itemData["chrPrice3Name"].ToString();
                    line_divPrice3.Visible = true;
                }

                if (itemData["bitValue1Required"].ToString() == "True")
                {
                    divValue1.Visible = true;

                    labelValue1.Text = itemData["chrValue1Name"].ToString();
                }

                if (itemData["bitCheckbox1Required"].ToString() == "True")
                {
                    divCheckbox1.Visible = true;
                    lblCheckbox1.Text = itemData["chrCheckbox1Name"].ToString();
                    line_divCheckbox1.Visible = true;
                }
                if (itemData["bitCheckbox2Required"].ToString() == "True")
                {
                    divCheckbox2.Visible = true;
                    lblCheckbox2.Text = itemData["chrCheckbox2Name"].ToString();
                    line_divCheckbox2.Visible = true;
                }

                if (itemData["bitCheckbox3Required"].ToString() == "True")
                {
                    divCheckbox3.Visible = true;
                    lblCheckbox3.Text = itemData["chrCheckbox3Name"].ToString();
                    line_divCheckbox3.Visible = true;
                }

                if (itemData["bitDataTypeRequired"].ToString() == "True")
                {
                    ddlDataType.DataSource = new List<string> { "String", "Numeric", "Boolean", "Date" };
                    ddlDataType.DataBind();
                    divDataType.Visible = true;
                    lblDataType.Text = itemData["chrDataTypeName"].ToString();
                    line_divDataType.Visible = true;
                }
            }
            else
            {
                // no item type
                Response.Redirect("~/Home.aspx");
            }


            //Need to get Sequence no's for item type
            ProcessGetItemTypeMaxSequence _maxseq = new ProcessGetItemTypeMaxSequence();
            _maxseq.Item = item;
            _maxseq.Invoke();

            if (_maxseq.ResultSet.Tables[0].Rows.Count > 0)
            {
                for (int i = 1; i <= Convert.ToInt16(_maxseq.ResultSet.Tables[0].Rows[0]["maxSequence"]) + 1; i++)
                {
                    sequenceID.Items.Add(i.ToString());
                }
            }


            //Need to load client's available file types
            ProcessGetFileTypes getFileTypes = new ProcessGetFileTypes();
            getFileTypes.Invoke();
            if (getFileTypes.ResultSet.Tables[0].Rows.Count > 0)
            {
                fileType.DataSource = getFileTypes.ResultSet;
                fileType.DataTextField = "chrFileType";
                fileType.DataValueField = "intFileTypeID";
                fileType.DataBind();

                file2Type.DataSource = getFileTypes.ResultSet;
                file2Type.DataTextField = "chrFileType";
                file2Type.DataValueField = "intFileTypeID";
                file2Type.DataBind();
            }
            else
            {
                fileType.Items.Add("-1");
                file2Type.Items.Add("-1");
            }

        }

        private void FillScreen(int itemid)
        {

            //string _rootpath = Request.Url.GetLeftPart(UriPartial.Authority) + HttpRuntime.AppDomainAppVirtualPath;
            string _rootpath = FluentContentSettings.WebsiteRoot;
            if (_rootpath.Substring(_rootpath.Length - 1, 1) != "/") { _rootpath += "/"; }

            ProcessGetItemByID getItem = new ProcessGetItemByID();
            Item item = new Item();
            item.ItemID = itemid;
            getItem.Item = item;
            getItem.Invoke();
            dr = getItem.ResultSet.Tables[0].Rows[0];

            itemTitle.Text = dr["chrItemTitle"].ToString();
            itemURL.Text = dr["chrItemURL"].ToString();
            if (dr["datStart"].ToString() != "")
            {
                datepickerfr.Text = Convert.ToDateTime(dr["datStart"].ToString()).ToString("dd/MM/yyyy");
            }
            if (dr["datExpire"].ToString() != "")
            {
                datepickerto.Text = Convert.ToDateTime(dr["datExpire"].ToString()).ToString("dd/MM/yyyy");
            }
            if (dr["timStart"].ToString() != "")
            {
                timeFrom.Text = Convert.ToDateTime(dr["timStart"].ToString()).ToString("HH:mm");
            }
            if (dr["timExpire"].ToString() != "")
            {
                timeTo.Text = Convert.ToDateTime(dr["timExpire"].ToString()).ToString("HH:mm");
            }

            //Image 1
            if (dr["chrItemImageURL"].ToString() != "")
            {
                image.ImageUrl = _rootpath + FluentContentSettings.ImageUploadFolder + "/" + dr["chrItemImageURL"].ToString();
                image.Visible = true;
                deleteImage.Checked = false;
                deleteImage.Visible = true;
                deleteLabel.Visible = true;
            }
            else
            {
                image.Visible = false;
                deleteImage.Checked = false;
                deleteImage.Visible = false;
                deleteLabel.Visible = false;
            }
            imageTitle.Text = dr["chrItemImageTitle"].ToString();
            imageAlt.Text = dr["chrItemImageAlt"].ToString();
            imageCaption.Text = dr["chrItemImageCaption"].ToString();

            //Image 2
            if (dr["chrItemImage2URL"].ToString() != "")
            {
                image2.ImageUrl = _rootpath + FluentContentSettings.ImageUploadFolder + "/" + dr["chrItemImage2URL"].ToString();
                image2.Visible = true;
                deleteImage2.Checked = false;
                deleteImage2.Visible = true;
                deleteLabel2.Visible = true;
            }
            else
            {
                image2.Visible = false;
                deleteImage2.Checked = false;
                deleteImage2.Visible = false;
                deleteLabel2.Visible = false;
            }
            image2Title.Text = dr["chrItemImage2Title"].ToString();
            image2Alt.Text = dr["chrItemImage2Alt"].ToString();
            image2Caption.Text = dr["chrItemImage2Caption"].ToString();

            sequenceID.SelectedValue = dr["intSequence"].ToString();

            if (dr["bitEnabled"].ToString() == "True")
            {
                enabled.Checked = true;
            }
            else
            {
                enabled.Checked = false;
            }
            itemLink.Text = dr["chrLinkURL"].ToString();

            text1.Text = dr["chrText1"].ToString();
            text2.Text = dr["chrText2"].ToString();
            text3.Text = dr["chrText3"].ToString();
            text4.Text = dr["chrText4"].ToString();
            text5.Text = dr["chrText5"].ToString();
            text6.Text = dr["chrText6"].ToString();
            text7.Text = dr["chrText7"].ToString();
            text8.Text = dr["chrText8"].ToString();
            text9.Text = dr["chrText9"].ToString();
            text10.Text = dr["chrText10"].ToString();
            text11.Text = dr["chrText11"].ToString();
            text12.Text = dr["chrText12"].ToString();

            price1.Text = dr["numPrice1"].ToString();
            price2.Text = dr["numPrice2"].ToString();
            price3.Text = dr["numPrice3"].ToString();

            value1.Text = dr["intValue1"].ToString();

            chkCheckbox1.Checked = Convert.ToBoolean(dr["bitCheckbox1"] == DBNull.Value ? false : dr["bitCheckbox1"]);
            chkCheckbox2.Checked = Convert.ToBoolean(dr["bitCheckbox2"] == DBNull.Value ? false : dr["bitCheckbox2"]);
            chkCheckbox3.Checked = Convert.ToBoolean(dr["bitCheckbox3"] == DBNull.Value ? false : dr["bitCheckbox3"]);

            ddlDataType.SelectedValue = String.IsNullOrEmpty(dr["chrDataType"].ToString()) ? "String" : dr["chrDataType"].ToString();

            if (dr["chrItemFileURL"].ToString() != "")
            {
                ExistingFile.Text = dr["chrItemFileURL"].ToString();
                deleteFile.Checked = false;
                divRemoveExistingFile.Visible = true;
            }
            else
            {
                ExistingFile.Text = "";
                divRemoveExistingFile.Visible = false;
                deleteFile.Checked = false;
            }

            if (dr["intFileTypeID"] != DBNull.Value && Convert.ToInt16(dr["intFileTypeID"]) == 0)
            {
                fileType.ClearSelection();
            }
            else
            {
                fileType.SelectedValue = dr["intFileTypeID"].ToString();
            }

            if (dr["chrItemFile2URL"].ToString() != "")
            {
                ExistingFile2.Text = dr["chrItemFile2URL"].ToString();
                deleteFile2.Checked = false;
                divRemoveExistingFile2.Visible = true;
            }
            else
            {
                ExistingFile2.Text = "";
                divRemoveExistingFile2.Visible = false;
                deleteFile2.Checked = false;
            }

            if (dr["intFile2TypeID"] != DBNull.Value && Convert.ToInt16(dr["intFile2TypeID"]) == 0)
            {
                file2Type.ClearSelection();
            }
            else
            {
                file2Type.SelectedValue = dr["intFile2TypeID"].ToString();
            }


            if (dr["intLinkedEntityTypeID"].ToInt32() != 0)
            {
                lvItemLinkedEntities.DataSource = DAC_Item.GetItemEntities(dr["intItemID"].ToInt32(), dr["intLinkedEntityTypeID"].ToInt32());
                lvItemLinkedEntities.DataBind();
            }

            intItemID.Value = itemid.ToString();

        }

        private void ClearScreen()
        {
            itemTitle.Text = "";
            text1.Text = "";
            text2.Text = "";
            text3.Text = "";
            text4.Text = "";
            text5.Text = "";
            text6.Text = "";
            text7.Text = "";
            text8.Text = "";
            text9.Text = "";
            text10.Text = "";
            text11.Text = "";
            text12.Text = "";
            itemURL.Text = "";
            image.ImageUrl = "";
            image.Visible = false;
            imageTitle.Text = "";
            imageAlt.Text = "";
            imageCaption.Text = "";
            image2.ImageUrl = "";
            image2.Visible = false;
            image2Title.Text = "";
            image2Alt.Text = "";
            image2Caption.Text = "";
            fileType.ClearSelection();
            file2Type.ClearSelection();
            itemLink.Text = "";
            datepickerfr.Text = "";
            datepickerto.Text = "";
            timeFrom.Text = "";
            timeTo.Text = "";
            sequenceID.SelectedIndex = -1;
            enabled.Checked = false;
            overwriteFile.Checked = false;
            overwriteFile2.Checked = false;
            overwriteImg.Checked = false;
            overwriteImg2.Checked = false;
            price1.Text = "";
            price2.Text = "";
            price3.Text = "";

            //Need to customise the screen for item type
            ProcessGetItemType itemType = new ProcessGetItemType();
            Item item = new Item();
            item.IsAdmin = (this.Page.Master as CmsMaster).UserSession.Role.CMSAccessLevel == CMSAccessLevel.Full;
            item.ItemTypeID = ItemTypeID;
            itemType.Item = item;
            itemType.Invoke();

            if (itemType.ResultSet.Tables[0].Rows.Count > 0)
            {
                DataRow itemData = itemType.ResultSet.Tables[0].Rows[0];
                if (itemData["intLinkedEntityTypeID"].ToInt32() != 0)
                {
                    divLinkedEntity.Visible = true;
                    labelLinkedEntity.Text = itemData["chrLinkedEntityTypeName"].ToString();

                    lvItemLinkedEntities.DataSource = DAC_Entity.GetByTypeID(itemData["intLinkedEntityTypeID"].ToInt32());
                    lvItemLinkedEntities.DataBind();
                }
            }
        }

        // helpers

        private bool ValidDimensions(FileUpload fu, string dimensionData)
        {
            int width = 0, height = 0;
            char widthRestrict, heightRestrict;

            string[] dimData = dimensionData.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
            string[] dims = dimData[0].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            string[] restrictions = dimData[1].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            width = dims[0].ToInt32();
            height = dims[1].ToInt32();

            widthRestrict = restrictions[0][0];
            heightRestrict = restrictions[1][0];

            int invalidCount = 0;
            using (System.Drawing.Image image = System.Drawing.Image.FromStream(fu.PostedFile.InputStream))
            {
                switch (widthRestrict)
                {
                    // maximum
                    case 'm':
                        if (image.Width > width) invalidCount++;
                        break;
                    // minimum
                    case 'n':
                        if (image.Width < width) invalidCount++;
                        break;
                    // exact
                    case 'x':
                    default:
                        if (image.Width != width) invalidCount++;
                        break;
                }

                switch (heightRestrict)
                {
                    // maximum
                    case 'm':
                        if (image.Height > height) invalidCount++;
                        break;
                    // minimum
                    case 'n':
                        if (image.Height < height) invalidCount++;
                        break;
                    // exact
                    case 'x':
                    default:
                        if (image.Height != height) invalidCount++;
                        break;
                }


                return (invalidCount == 0);
            }
        }

        private string RestrictionString(string restrictionData)
        {
            switch (restrictionData)
            {
                case "m":
                    return "max ";
                case "n":
                    return "min ";
                default:
                    return String.Empty;
            }
        }
    }
}