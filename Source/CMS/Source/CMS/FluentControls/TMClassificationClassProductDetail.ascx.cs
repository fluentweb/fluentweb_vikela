﻿using FluentContent.DAC;
using FluentContent.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Extensions;
using CMS.Helpers;
using FLContentSiteAssist.BusinessLogic;
using FluentContent.Entities.Helpers;
using System.Collections.Specialized;
using System.IO;
using System.Drawing.Imaging;
using System.Data;
using Common.Url;
using Common.Session;

namespace CMS.FluentControls
{
    public partial class TMClassificationClassProductDetail : System.Web.UI.UserControl
    {
        public int ClassificationClassProductID { get { return Request.GetQueryString<int>("classificationclassproductid", -1); } }
        public int ClassificationClassID { get { return Request.GetQueryString<int>("classificationclassid", -1); } }
        public string ClassificationClassName { get { return Request.GetQueryString<string>("classificationclassname", string.Empty); } }
        public int ClassificationID { get { return Request.GetQueryString<int>("classificationid", -1); } }
        public string ClassificationName { get { return Request.GetQueryString<string>("classificationname", string.Empty); } }
      
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (ClassificationClassID != -1)
                {
                    FillScreen(ClassificationClassProductID);
                }
            }
        }

        private void FillScreen(int ProductID)
        {
            TMClassificationClassProduct tmc = DAC_TMClassificationClassProduct.GetClassificationByProduct(ProductID);
            if (tmc == null) return;
            txtproductname.Text = tmc.ClassificationClassProduct.ToString();
            txtproductCode.Text = tmc.ClassificationClassProductCode.ToString();
            txtproductnumber.Text = tmc.ClassificationClassProductNo == 0 ? string.Empty : tmc.ClassificationClassProductNo.ToString(); ;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void Save()
        {
            if (!Page.IsValid) return;

            TMClassificationClassProduct tmc = new TMClassificationClassProduct();
            tmc.ClassificationClassProduct = txtproductname.Text.Trim();
            tmc.ClassificationClassProductCode = txtproductCode.Text.Trim();
            tmc.ClassificationClassProductNo = string.IsNullOrEmpty(txtproductnumber.Text.Trim()) ? 0 : txtproductnumber.Text.Trim().ToInt32();
            tmc.TMClassificationClassID = ClassificationClassID;
            tmc.TMClassificationClassProductID = ClassificationClassProductID;

            if (ClassificationClassProductID > 0)
            {
                tmc = DAC_TMClassificationClassProduct.Update(tmc);
                if (tmc == null)
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Update failed!','warning');", true);
                else
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Class successfully updated.','success');", true);
            }
            else
            {
                tmc = DAC_TMClassificationClassProduct.Insert(tmc);
                if (tmc == null)
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Create failed!','warning');", true);
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('New Class successfully created.  You may now create another...','success');", true);
                    ClearPage();
                }
            }
        }

        private void ClearPage()
        {
            txtproductname.Text = string.Empty;
            txtproductCode.Text = string.Empty;
            txtproductnumber.Text = string.Empty;
        }
    }
}