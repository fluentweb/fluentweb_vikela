﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TMCostDetail.ascx.cs" Inherits="CMS.FluentControls.TMCostDetail" %>
<div class="form-horizontal">
    <div class="errorSummary hidden" validationgroup="cost">
        <center>
                         <p>
                            Please correct the highlighted fields
                        </p>
                        </center>
    </div>
    <asp:ValidationSummary ID="vsErrorSummary" runat="server" DisplayMode="List" ValidationGroup="cost"
        CssClass="errorSummary" />
    <div class="form-group">
        <label class="col-sm-2 control-label">Cost Type Code</label>
        <div class="col-sm-10">
            <asp:DropDownList ID="ddcostTypeCode" CssClass="form-control m-b" runat="server" AutoPostBack="false">
            </asp:DropDownList>
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Country</label>
        <div class="col-sm-10">
            <asp:DropDownList ID="ddcountryName" CssClass="form-control m-b" runat="server" AutoPostBack="false">
            </asp:DropDownList>
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-sm-2 control-label">TM Type</label>
        <div class="col-sm-10">
            <asp:DropDownList ID="ddtmType" CssClass="form-control m-b" runat="server" AutoPostBack="false">
            </asp:DropDownList>
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Item Cost</label>
        <div class="col-sm-10">
            <asp:TextBox ID="txtItemCost" ValidationGroup="cost" CssClass="form-control" runat="server" MaxLength="255" />
            <asp:RequiredFieldValidator SetFocusOnError="true" ID="rntxtitemcost" Display="None" runat="server" ErrorMessage="Please fill out this field" ValidationGroup="cost"
                ControlToValidate="txtItemCost"></asp:RequiredFieldValidator>
            <asp:CompareValidator ID="cntxtitemcost" runat="server" Operator="DataTypeCheck" Type="Double"
                ControlToValidate="txtItemCost" ErrorMessage="Value must be a whole number " ValidationGroup="cost" />
            <asp:RegularExpressionValidator ID="rentxtitemcost" runat="server" ControlToValidate="txtitemcost" ValidationGroup="cost" ValidationExpression="^(\d{1,18})(.\d{2})?$" ErrorMessage="Invalid format must be a money format (0.00)."></asp:RegularExpressionValidator>
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-lg-2 control-label">Min No Marks</label>
        <div class="col-lg-10">
            <asp:TextBox ID="txtMinNoMarks" ValidationGroup="cost" CssClass="form-control" runat="server" MaxLength="255" />
            <asp:CompareValidator runat="server" Operator="DataTypeCheck" Type="Integer"
                ControlToValidate="txtMinNoMarks" ErrorMessage="Value must be a whole number" ValidationGroup="cost" />
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-lg-2 control-label">Max No Marks</label>
        <div class="col-lg-10">
            <asp:TextBox ID="txtMaxNoMarks" ValidationGroup="cost" CssClass="form-control" runat="server" MaxLength="255" />
            <asp:CompareValidator runat="server" Operator="DataTypeCheck" Type="Integer"
                ControlToValidate="txtMaxNoMarks" ErrorMessage="Value must be a whole number" ValidationGroup="cost" />
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-lg-2 control-label">Min No Classes</label>
        <div class="col-lg-10">
            <asp:TextBox ID="txtMinNoClasses" ValidationGroup="cost" CssClass="form-control" runat="server" MaxLength="255" />
            <asp:CompareValidator runat="server" Operator="DataTypeCheck" Type="Integer"
                ControlToValidate="txtMinNoClasses" ErrorMessage="Value must be a whole number" ValidationGroup="cost" />
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-lg-2 control-label">Max No Classes</label>
        <div class="col-lg-10">
            <asp:TextBox ID="txtMaxNoClasses" ValidationGroup="cost" CssClass="form-control" runat="server" MaxLength="255" />
            <asp:CompareValidator runat="server" Operator="DataTypeCheck" Type="Integer"
                ControlToValidate="txtMaxNoClasses" ErrorMessage="Value must be a whole number" ValidationGroup="cost" />
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-lg-2 control-label">Colour</label>
        <div class="col-lg-10">
            <asp:CheckBox runat="server" ID="chk_colour" CssClass="input_enabled" />
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-lg-2 control-label">B&W</label>

        <div class="col-lg-10">
            <asp:CheckBox runat="server" ID="chk_bw" CssClass="input_enabled" />
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-lg-2 control-label">Colour Series</label>
        <div class="col-lg-10">
            <asp:CheckBox runat="server" ID="chk_colourseries" CssClass="input_enabled" />
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-lg-2 control-label">Min No States</label>
        <div class="col-lg-10">
            <asp:TextBox ID="txtMinNoStates" ValidationGroup="cost" CssClass="form-control" runat="server" MaxLength="255" />
            <asp:CompareValidator runat="server" Operator="DataTypeCheck" Type="Integer"
                ControlToValidate="txtMinNoStates" ErrorMessage="Value must be a whole number" ValidationGroup="cost" />
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-lg-2 control-label">Max No States</label>
        <div class="col-lg-10">
            <asp:TextBox ID="txtMaxNoStates" ValidationGroup="cost" CssClass="form-control" runat="server" MaxLength="255" />
            <asp:CompareValidator runat="server" Operator="DataTypeCheck" Type="Integer"
                ControlToValidate="txtMaxNoStates" ErrorMessage="Value must be a whole number" ValidationGroup="cost" />
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-lg-2 control-label">Min No Products</label>
        <div class="col-lg-10">
            <asp:TextBox ID="txtMinNoProducts" ValidationGroup="cost" CssClass="form-control" runat="server" MaxLength="255" />
            <asp:CompareValidator runat="server" Operator="DataTypeCheck" Type="Integer"
                ControlToValidate="txtMinNoProducts" ErrorMessage="Value must be a whole number" ValidationGroup="cost" />
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-lg-2 control-label">Max No Products</label>
        <div class="col-lg-10">
            <asp:TextBox ID="txtMaxNoProducts" ValidationGroup="cost" CssClass="form-control" runat="server" MaxLength="255" />
            <asp:CompareValidator runat="server" Operator="DataTypeCheck" Type="Integer"
                ControlToValidate="txtMaxNoProducts" ErrorMessage="Value must be a whole number" ValidationGroup="cost" />
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <div class="col-sm-4 col-sm-offset-2">
            <asp:Button ID="btnSave" CssClass="btn btn-primary" ValidationGroup="cost" type="submit" runat="server" OnClick="btnSave_Click" Text="Save" />
        </div>
    </div>
</div>
