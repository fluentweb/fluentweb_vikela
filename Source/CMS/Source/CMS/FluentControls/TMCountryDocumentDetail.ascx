﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TMCountryDocumentDetail.ascx.cs" Inherits="CMS.FluentControls.TMCountryDocumentDetail" %>
<div class="form-horizontal">
    <asp:HiddenField ID="intCountryID" runat="server" />
    <div class="form-group">
        <label class="col-sm-2 control-label">Country</label>

        <div class="col-sm-10">
            <asp:DropDownList ID="ddcountryName" CssClass="form-control m-b" runat="server" AutoPostBack="false">
            </asp:DropDownList>
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Convention Priority Claimed</label>
        <div class="col-sm-10">
            <asp:CheckBox runat="server" ID="chk_ConventionPriorityClaimed" CssClass="input_enabled" />
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Document Desc</label>
        <div class="col-sm-10">
            <asp:TextBox runat="server" ID="txt_DocumentDesc" CssClass="form-control" TextMode="MultiLine" MaxLength="400" Rows="3"/>
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <div class="col-sm-4 col-sm-offset-2">
            <asp:Button ID="btnSave" CssClass="btn btn-primary" type="submit" runat="server" OnClick="btnSave_Click" Text="Save" />
        </div>
    </div>
</div>
