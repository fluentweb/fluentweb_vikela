﻿using FluentContent.DAC;
using FluentContent.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Extensions;
using CMS.Helpers;
using FLContentSiteAssist.BusinessLogic;
using FluentContent.Entities.Helpers;
using System.Collections.Specialized;
using System.IO;
using System.Drawing.Imaging;
using System.Data;
using Common.Url;
using Common.Session;

namespace CMS.FluentControls
{
    public partial class TMCostDetail : System.Web.UI.UserControl
    {
        public int CostID { get { return Request.GetQueryString<int>("costid", -1); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillDrowpDown();
                if (CostID != -1)
                {
                    FillScreen(CostID);
                }
            }
        }

        private void FillScreen(int CostID)
        {
            TMCost tmc = DAC_TMCost.GetCost(CostID);
            if (tmc == null) return;
            ddcountryName.SelectedValue = tmc.CountryID.ToString();
            ddcostTypeCode.SelectedValue = tmc.CostTypeID.ToString();
            ddtmType.SelectedValue = tmc.TMTypeID.ToString();
            txtItemCost.Text = tmc.Price.ToString();
            txtMinNoMarks.Text = tmc.MinMarks == 0 ? string.Empty : tmc.MinMarks.ToString();
            txtMaxNoMarks.Text = tmc.MaxMarks == 0 ? string.Empty : tmc.MaxMarks.ToString();
            txtMinNoClasses.Text = tmc.MinClasses == 0 ? string.Empty : tmc.MinClasses.ToString();
            txtMaxNoClasses.Text = tmc.MaxClasses == 0 ? string.Empty : tmc.MaxClasses.ToString();
            txtMinNoProducts.Text = tmc.MinProducts == 0 ? string.Empty : tmc.MinProducts.ToString();
            txtMaxNoProducts.Text = tmc.MaxProducts == 0 ? string.Empty : tmc.MaxProducts.ToString();
            txtMinNoStates.Text = tmc.MinStates == 0 ? string.Empty : tmc.MinStates.ToString();
            txtMaxNoStates.Text = tmc.MaxStates == 0 ? string.Empty : tmc.MaxStates.ToString();
            chk_bw.Checked = tmc.Colour;
            chk_bw.Checked = tmc.BW;
            chk_colourseries.Checked = tmc.ColourSeries;
        }

        private void FillDrowpDown()
        {
            //Country dd
                DataTable dtCountry = DAC_TMCountry.GetCountryByCountry();
                DataRow drEmpty = dtCountry.NewRow();
                drEmpty["chrCountry"] = "--Select Country--";
                drEmpty["intCountryID"] = 0;
                dtCountry.Rows.InsertAt(drEmpty, 0);

                ddcountryName.DataSource = dtCountry;
                ddcountryName.DataTextField = "chrCountry";
                ddcountryName.DataValueField = "intCountryID";
                ddcountryName.DataBind();

                //CostType dd
                ddcostTypeCode.DataSource = DAC_TMCostType.GetCostType();
                ddcostTypeCode.DataTextField = "chrCostTypeCode";
                ddcostTypeCode.DataValueField = "intCostTypeID";
                ddcostTypeCode.DataBind();

                //Classification dd
                ddtmType.DataSource = DAC_TMMarkType.GetMarkType();
                ddtmType.DataTextField = "chrTMType";
                ddtmType.DataValueField = "intTMTypeID";
                ddtmType.DataBind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void Save()
        {
            if (!Page.IsValid) return;

            TMCost tmc = new TMCost();
            tmc.CountryID = ddcountryName.SelectedValue.ToInt32();
            tmc.CostTypeID = ddcostTypeCode.SelectedValue.ToInt32();
            tmc.TMTypeID = ddtmType.SelectedValue.ToInt32();
            tmc.Price = Convert.ToDecimal(txtItemCost.Text.Trim());
            tmc.MinMarks = string.IsNullOrEmpty(txtMinNoMarks.Text.Trim()) ? 0 : txtMinNoMarks.Text.Trim().ToInt32();
            tmc.MaxMarks = string.IsNullOrEmpty(txtMaxNoMarks.Text.Trim()) ? 0 : txtMaxNoMarks.Text.Trim().ToInt32();
            tmc.MinClasses = string.IsNullOrEmpty(txtMinNoClasses.Text.Trim()) ? 0 : txtMinNoClasses.Text.Trim().ToInt32(); 
            tmc.MaxClasses = string.IsNullOrEmpty(txtMaxNoClasses.Text.Trim()) ? 0 : txtMaxNoClasses.Text.Trim().ToInt32(); 
            tmc.MinProducts = string.IsNullOrEmpty(txtMinNoProducts.Text.Trim()) ? 0 : txtMinNoProducts.Text.Trim().ToInt32();
            tmc.MaxProducts = string.IsNullOrEmpty(txtMaxNoProducts.Text.Trim()) ? 0 : txtMaxNoProducts.Text.Trim().ToInt32(); 
            tmc.MinStates = string.IsNullOrEmpty(txtMinNoStates.Text.Trim()) ? 0 : txtMinNoStates.Text.Trim().ToInt32(); 
            tmc.MaxStates = string.IsNullOrEmpty(txtMaxNoStates.Text.Trim()) ? 0 : txtMaxNoStates.Text.Trim().ToInt32();
            tmc.Colour = chk_bw.Checked;
            tmc.BW = chk_bw.Checked;
            tmc.ColourSeries = chk_colourseries.Checked;
            tmc.CostID = CostID;

            if (CostID > 0)
            {
                tmc = DAC_TMCost.Update(tmc);
                if (tmc == null)
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Update failed!','warning');", true);
                else
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Cost successfully updated.','success');", true);
            }
            else
            {
                tmc = DAC_TMCost.Insert(tmc);
                if (tmc == null)
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Create failed!','warning');", true);
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('New Cost successfully created.  You may now create another...','success');", true);
                    ClearPage();
                }
            }
        }

        private void ClearPage()
        {
            ddcountryName.SelectedIndex = 0;
            ddcostTypeCode.SelectedIndex = 0;
            ddtmType.SelectedIndex = 0;
            txtItemCost.Text = string.Empty;
            txtMinNoMarks.Text = string.Empty;
            txtMaxNoMarks.Text = string.Empty;
            txtMinNoClasses.Text = string.Empty;
            txtMaxNoClasses.Text = string.Empty;
            txtMinNoProducts.Text = string.Empty;
            txtMaxNoProducts.Text = string.Empty;
            txtMinNoStates.Text = string.Empty;
            txtMaxNoStates.Text = string.Empty;
            chk_bw.Checked = false;
            chk_bw.Checked = false;
            chk_colourseries.Checked = false;
        }
    }
}