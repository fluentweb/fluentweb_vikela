﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExistingImagesDropdown.ascx.cs"
    Inherits="CMS.FluentControls.ExistingImagesDropdown" %>

<script src="<%=ResolveUrl("~/scripts/jQuery/jquery-ui-1.11.2/jquery-ui.min.js") %>" type="text/javascript"></script>

<script type="text/javascript">

    $(function () {

        // initialise
        initExistingImageSelect();

        $("#<%=clearExistingImageSelection.ClientID %>").click(function () {
            $('#<%=divExistingImages.ClientID %>').ddslick('destroy');
            initExistingImageSelect();
            $('#<%=hdnSelectedExistingImage.ClientID %>').val('');
            return false;
        });

        function initExistingImageSelect() {
            $('#<%=divExistingImages.ClientID %>').ddslick({
                data: <%=JSONData %>,
                width: '100%',
                imagePosition: "left",
                truncateDescription: false,
                selectText: "Select an existing image",
                onSelected: function (data) {
                    //console.log(data);
                    $('#<%=hdnSelectedExistingImage.ClientID %>').val(data.selectedData.description);
                }
            });
            }
    });
</script>

<div class="exisitngImageWrapper">
    <div class="col-sm-2">
    </div>
    <div class="col-sm-4">
        <div class="existingImageSelect" id="divExistingImages" runat="server">
        </div>
    </div>
    <div class="col-sm-2">
        <a href="#" runat="server" id="clearExistingImageSelection">reset</a>
    </div>
    <div class="col-sm-4"></div>
</div>
<asp:HiddenField ID="hdnSelectedExistingImage" runat="server" />
