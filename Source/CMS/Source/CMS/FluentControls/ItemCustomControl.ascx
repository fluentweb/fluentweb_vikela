﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ItemCustomControl.ascx.cs"
    Inherits="CMS.FluentControls.ItemCustomControl" %>
<%@ Register Src="ExistingImagesDropdown.ascx" TagName="ExistingImagesDropdown" TagPrefix="FC" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:HiddenField ID="hidSourceID" runat="server" />
<div class="form-horizontal">
    <asp:HiddenField ID="intItemID" runat="server" />
    <div id="divTitle" runat="server" class="form-group" onmousedown="var msg = document.getElementById('<%=updateMessage.ClientID%>');if (msg!=null){msg.style.visibility = 'hidden';};">
        <label class="col-sm-2 control-label">
            <asp:Literal runat="server" ID="labelTitle" />
        </label>
        <div class="col-sm-10">
            <asp:TextBox ID="itemTitle" CssClass="form-control" runat="server" MaxLength="255" />
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group" runat="server" id="divSequence">
        <label class="col-sm-2 control-label">
            Sequence
        </label>
        <div class="col-sm-10">
            <asp:DropDownList ID="sequenceID" CssClass="form-control m-b" runat="server" />
        </div>
    </div>
    <div class="hr-line-dashed"></div>

    <div class="form-group" runat="server" id="divEnabled">
        <label class="col-sm-2 control-label">
            Enabled
        </label>
        <div class="col-sm-10">
            <asp:CheckBox runat="server" ID="enabled" CssClass="input_enabled" />
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div id="divValue1" runat="server" visible="false" class="form-group" onmousedown="var msg = document.getElementById('<%=updateMessage.ClientID%>');if (msg!=null){msg.style.visibility = 'hidden';};">
        <label class="col-sm-2 control-label">
            <asp:Literal runat="server" ID="labelValue1" />
        </label>
        <div class="col-sm-10">
            <asp:TextBox ID="value1" CssClass="form-control" runat="server" />
        </div>
    </div>
    <div id="line_divValue1" runat="server" visible="false" class="hr-line-dashed"></div>
    <div id="divPrice1" runat="server" visible="false" class="form-group" onmousedown="var msg = document.getElementById('<%=updateMessage.ClientID%>');if (msg!=null){msg.style.visibility = 'hidden';};">
        <label class="col-sm-2 control-label">
            <asp:Literal runat="server" ID="labelPrice1" />
        </label>
        <div class="col-sm-10">
            <asp:TextBox ID="price1" CssClass="form-control" runat="server" />
        </div>
    </div>
    <div id="line_divPrice1" runat="server" visible="false" class="hr-line-dashed"></div>
    <div id="divPrice2" runat="server" visible="false" class="form-group" onmousedown="var msg = document.getElementById('<%=updateMessage.ClientID%>');if (msg!=null){msg.style.visibility = 'hidden';};">
        <label class="col-sm-2 control-label">
            <asp:Literal runat="server" ID="labelPrice2" />
        </label>
        <div class="col-sm-10">
            <asp:TextBox ID="price2" CssClass="form-control" runat="server" />
        </div>
    </div>
    <div id="line_divPrice2" runat="server" visible="false" class="hr-line-dashed"></div>
    <div id="divPrice3" runat="server" visible="false" class="form-group" onmousedown="var msg = document.getElementById('<%=updateMessage.ClientID%>');if (msg!=null){msg.style.visibility = 'hidden';};">
        <label class="col-sm-2 control-label">
            <asp:Literal runat="server" ID="labelPrice3" />
        </label>
        <div class="col-sm-10">
            <asp:TextBox ID="price3" CssClass="form-control" runat="server" />
        </div>
    </div>
    <div id="line_divPrice3" runat="server" visible="false" class="hr-line-dashed"></div>

    <div id="divText1" runat="server" visible="false" class="form-group" onmousedown="var msg = document.getElementById('<%=updateMessage.ClientID%>');if (msg!=null){msg.style.visibility = 'hidden';};">
        <label class="col-sm-2 control-label">
            <asp:Literal runat="server" ID="labelText1" />
        </label>
        <div class="col-sm-10">
            <asp:TextBox ID="text1" CssClass="form-control" runat="server" TextMode="MultiLine"
                Rows="3" />
        </div>
    </div>
    <div id="line_divText1" runat="server" visible="false" class="hr-line-dashed"></div>
    <div id="divText2" runat="server" visible="false" class="form-group" onmousedown="var msg = document.getElementById('<%=updateMessage.ClientID%>');if (msg!=null){msg.style.visibility = 'hidden';};">
        <label class="col-sm-2 control-label">
            <asp:Literal runat="server" ID="labelText2" />
        </label>
        <div class="col-sm-10">
            <asp:TextBox ID="text2" CssClass="form-control" runat="server" TextMode="MultiLine"
                Rows="3" />
        </div>
    </div>
    <div id="line_divText2" runat="server" visible="false" class="hr-line-dashed"></div>
    <div id="divText3" runat="server" visible="false" class="form-group" onmousedown="var msg = document.getElementById('<%=updateMessage.ClientID%>');if (msg!=null){msg.style.visibility = 'hidden';};">
        <label class="col-sm-2 control-label">
            <asp:Literal runat="server" ID="labelText3" />
        </label>
        <div class="col-sm-10">
            <asp:TextBox ID="text3" CssClass="form-control" runat="server" TextMode="MultiLine"
                Rows="3" />
        </div>
    </div>
    <div id="line_divText3" runat="server" visible="false" class="hr-line-dashed"></div>

    <div id="divText4" runat="server" visible="false" class="form-group" onmousedown="var msg = document.getElementById('<%=updateMessage.ClientID%>');if (msg!=null){msg.style.visibility = 'hidden';};">
        <label class="col-sm-2 control-label">
            <asp:Literal runat="server" ID="labelText4" />
        </label>
        <div class="col-sm-10">
            <asp:TextBox ID="text4" CssClass="form-control" runat="server" TextMode="MultiLine"
                Rows="3" />
        </div>
    </div>
    <div id="line_divText4" runat="server" visible="false" class="hr-line-dashed"></div>

    <div id="divText5" runat="server" visible="false" class="form-group" onmousedown="var msg = document.getElementById('<%=updateMessage.ClientID%>');if (msg!=null){msg.style.visibility = 'hidden';};">
        <label class="col-sm-2 control-label">
            <asp:Literal runat="server" ID="labelText5" />
        </label>
        <div class="col-sm-10">
            <asp:TextBox ID="text5" CssClass="form-control" runat="server" TextMode="MultiLine"
                Rows="3" />
        </div>
    </div>
    <div id="line_divText5" runat="server" visible="false" class="hr-line-dashed"></div>

    <div id="divText6" runat="server" visible="false" class="form-group" onmousedown="var msg = document.getElementById('<%=updateMessage.ClientID%>');if (msg!=null){msg.style.visibility = 'hidden';};">
        <label class="col-sm-2 control-label">
            <asp:Literal runat="server" ID="labelText6" />
        </label>
        <div class="col-sm-10">
            <asp:TextBox ID="text6" CssClass="form-control" runat="server" TextMode="MultiLine"
                Rows="3" />
        </div>
    </div>
    <div id="line_divText6" runat="server" visible="false" class="hr-line-dashed"></div>

    <div id="divText7" runat="server" visible="false" class="form-group" onmousedown="var msg = document.getElementById('<%=updateMessage.ClientID%>');if (msg!=null){msg.style.visibility = 'hidden';};">
        <label class="col-sm-2 control-label">
            <asp:Literal runat="server" ID="labelText7" />
        </label>
        <div class="col-sm-10">
            <asp:TextBox ID="text7" CssClass="form-control" runat="server" TextMode="MultiLine"
                Rows="3" />
        </div>
    </div>
    <div id="line_divText7" runat="server" visible="false" class="hr-line-dashed"></div>

    <div id="divText8" runat="server" visible="false" class="form-group" onmousedown="var msg = document.getElementById('<%=updateMessage.ClientID%>');if (msg!=null){msg.style.visibility = 'hidden';};">
        <label class="col-sm-2 control-label">
            <asp:Literal runat="server" ID="labelText8" />
        </label>
        <div class="col-sm-10">
            <asp:TextBox ID="text8" CssClass="form-control" runat="server" TextMode="MultiLine"
                Rows="3" />
        </div>
    </div>
    <div id="line_divText8" runat="server" visible="false" class="hr-line-dashed"></div>

    <div id="divText9" runat="server" visible="false" class="form-group" onmousedown="var msg = document.getElementById('<%=updateMessage.ClientID%>');if (msg!=null){msg.style.visibility = 'hidden';};">
        <label class="col-sm-2 control-label">
            <asp:Literal runat="server" ID="labelText9" />
        </label>
        <div class="col-sm-10">
            <asp:TextBox ID="text9" CssClass="form-control" runat="server" TextMode="MultiLine"
                Rows="3" />
        </div>
    </div>
    <div id="line_divText9" runat="server" visible="false" class="hr-line-dashed"></div>

    <div id="divText10" runat="server" visible="false" class="form-group" onmousedown="var msg = document.getElementById('<%=updateMessage.ClientID%>');if (msg!=null){msg.style.visibility = 'hidden';};">
        <label class="col-sm-2 control-label">
            <asp:Literal runat="server" ID="labelText10" />
        </label>
        <div class="col-sm-10">
            <CKEditor:CKEditorControl ID="text10" ResizeEnabled="false" BasePath="~/ckeditor" runat="server" Rows="3"></CKEditor:CKEditorControl>
        </div>
    </div>
    <div id="line_divText10" runat="server" visible="false" class="hr-line-dashed"></div>

    <div id="divText11" runat="server" visible="false" class="form-group" onmousedown="var msg = document.getElementById('<%=updateMessage.ClientID%>');if (msg!=null){msg.style.visibility = 'hidden';};">
        <label class="col-sm-2 control-label">
            <asp:Literal runat="server" ID="labelText11" />
        </label>
        <div class="col-sm-10">
            <CKEditor:CKEditorControl ID="text11" ResizeEnabled="false" BasePath="~/ckeditor" runat="server" Rows="5"></CKEditor:CKEditorControl>

        </div>
    </div>
    <div id="line_divText11" runat="server" visible="false" class="hr-line-dashed"></div>

    <div id="divText12" runat="server" visible="false" class="form-group" onmousedown="var msg = document.getElementById('<%=updateMessage.ClientID%>');if (msg!=null){msg.style.visibility = 'hidden';};">
        <label class="col-sm-2 control-label">
            <asp:Literal runat="server" ID="labelText12" />
        </label>
        <div class="col-sm-10">
            <CKEditor:CKEditorControl ID="text12" ResizeEnabled="false" BasePath="~/ckeditor" runat="server" Rows="5"></CKEditor:CKEditorControl>
        </div>
    </div>
    <div id="line_divText12" runat="server" visible="false" class="hr-line-dashed"></div>

    <div class="form-group" runat="server" id="divURL" visible="false">
        <label class="col-sm-2 control-label">
            <asp:Literal runat="server" ID="labelURL" />
        </label>
        <div class="col-sm-10">
            <asp:TextBox ID="itemURL" CssClass="form-control" runat="server" MaxLength="255" />
        </div>
    </div>
    <div id="line_divURL" runat="server" visible="false" class="hr-line-dashed"></div>

    <div class="form-group" runat="server" id="divLink" visible="false">
        <label class="col-sm-2 control-label">
            <asp:Literal runat="server" ID="labelLink" />
        </label>
        <div class="col-sm-10">
            <asp:TextBox ID="itemLink" CssClass="input_normal form-control" runat="server" MaxLength="255" />
        </div>
    </div>
    <div id="line_divLink" runat="server" visible="false" class="hr-line-dashed"></div>

    <div class="form-group" runat="server" id="divCheckbox1" visible="false">
        <label class="col-sm-2 control-label">
            <asp:Literal ID="lblCheckbox1" runat="server" />
        </label>
        <div class="col-sm-10">
            <asp:CheckBox runat="server" ID="chkCheckbox1" CssClass="input_enabled" />
        </div>
    </div>
    <div id="line_divCheckbox1" runat="server" visible="false" class="hr-line-dashed"></div>

    <div class="form-group" runat="server" id="divCheckbox2" visible="false">
        <label class="col-sm-2 control-label">
            <asp:Literal ID="lblCheckbox2" runat="server" />
        </label>
        <div class="col-sm-10">
            <asp:CheckBox runat="server" ID="chkCheckbox2" CssClass="input_enabled" />
        </div>
    </div>
    <div id="line_divCheckbox2" runat="server" visible="false" class="hr-line-dashed"></div>

    <div class="form-group" runat="server" id="divCheckbox3" visible="false">
        <label class="col-sm-2 control-label">
            <asp:Literal ID="lblCheckbox3" runat="server" />
        </label>
        <div class="col-sm-10">
            <asp:CheckBox runat="server" ID="chkCheckbox3" CssClass="input_enabled" />
        </div>
    </div>
    <div id="line_divCheckbox3" runat="server" visible="false" class="hr-line-dashed"></div>

    <div class="form-group" runat="server" id="divDataType" visible="false">
        <label class="col-sm-2 control-label">
            <asp:Literal ID="lblDataType" runat="server" />
        </label>
        <div class="col-sm-10">
            <asp:DropDownList ID="ddlDataType" runat="server" />
        </div>
    </div>
    <div id="line_divDataType" runat="server" visible="false" class="hr-line-dashed"></div>
    <div class="form-group" runat="server" id="divLinkedEntity" visible="false">
        <label class="col-sm-2 control-label">
            <asp:Literal runat="server" ID="labelLinkedEntity" />
        </label>
        <div class="col-sm-10">
            <asp:ListView ID="lvItemLinkedEntities" runat="server">
                <LayoutTemplate>
                    <table>
                        <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <%#Eval("Name") %>
                            <asp:HiddenField ID="hdnEntityID" runat="server" Value='<%#Eval("ID") %>' />
                        </td>
                        <td>
                            <asp:CheckBox ID="chkEntityRelated" runat="server" Checked='<%#Eval("IsAttachedToCurrentItem") %>' />
                        </td>
                        <td>Date linked
                                <asp:TextBox ID="txtDateLinked" runat="server" CssClass="datePicker" Text='<%# Convert.ToDateTime(Eval("DateAttachedToCurrentItem")) == DateTime.MinValue ? 
                                String.Empty : Convert.ToDateTime(Eval("DateAttachedToCurrentItem")).ToString("dd/MM/yyyy") %>'></asp:TextBox>
                        </td>
                        <td>Date expires
                                <asp:TextBox ID="txtDateExpires" runat="server" CssClass="datePicker" Text='<%# Convert.ToDateTime(Eval("DateExpires")) == DateTime.MinValue ? 
                                String.Empty : Convert.ToDateTime(Eval("DateExpires")).ToString("dd/MM/yyyy") %>'></asp:TextBox>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </div>
    </div>
    <div id="line_divLinkedEntity" runat="server" visible="false" class="hr-line-dashed"></div>
    <div id="divDates" runat="server" visible="false">
        <div class="form-group">
            <label class="col-sm-2 control-label">
                <asp:Literal runat="server" ID="labelDateFrom" />
            </label>
            <div class="col-sm-4">
                <div id="data_1" class="form-group">
                    <div class="input-group date">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <asp:TextBox ID="datepickerfr" CssClass="form-control" runat="server" />
                    </div>
                </div>
            </div>
            <label class="col-sm-2 control-label">
                <asp:Literal
                    runat="server" ID="labelTimeFrom" />
            </label>
            <div class="col-sm-4">
                <div data-autoclose="true" class="input-group clockpicker">
                    <asp:TextBox ID="timeFrom" CssClass="form-control" runat="server" />
                    <span class="input-group-addon">
                        <span class="fa fa-clock-o"></span>
                    </span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">
                <asp:Literal runat="server" ID="labelDateTo" />
            </label>
            <div class="col-sm-4">
                <div id="data_1" class="form-group">
                    <div class="input-group date">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <asp:TextBox ID="datepickerto" CssClass="form-control" runat="server" />
                    </div>
                </div>
            </div>
            <label class="col-sm-2 control-label">
                <asp:Literal runat="server" ID="labelTimeTo" />
            </label>
            <div class="col-sm-4">
                <div data-autoclose="true" class="input-group clockpicker">
                    <asp:TextBox ID="timeTo" CssClass="form-control" runat="server" />
                    <span class="input-group-addon">
                        <span class="fa fa-clock-o"></span>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div id="line_divDates" runat="server" visible="false" class="hr-line-dashed"></div>

    <div id="divImage1" runat="server" visible="false">
        <div class="form-group">
            <label class="col-sm-2 control-label">
                <asp:Literal runat="server" ID="labelImage1Name" /></label>
            <div class="col-sm-4">
                <asp:FileUpload ID="ImageUpload" Width="100%" CssClass="btn btn-outline btn-success" type="file" runat="server" />
            </div>
            <div class="col-sm-6">
                <asp:HiddenField ID="hdnImage1Dims" runat="server" />
                <asp:Label ID="image_instr" runat="server" />
            </div>
        </div>
        <div class="form-group">
            <FC:ExistingImagesDropdown ID="UCExistingImageDropdown1" runat="server" />
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">
            </label>
            <div class="col-sm-10">
                <asp:CheckBox runat="server" ID="overwriteImg" CssClass="input_enabled" Checked="false" />
                &nbsp;<small class="text-navy">Overwrite existing image</small>
                <br />
                <br />
                <asp:Image runat="server" ID="image" CssClass="image" style="outline: medium none;" BorderStyle="None" Height="100px" Visible="false" />
                <br />
                <asp:CheckBox runat="server" ID="deleteImage" CssClass="input_enabled" Checked="false"
                    Visible="false" />&nbsp;<small class="text-danger"><asp:Label ID="deleteLabel" runat="server" Text="Remove this image" Visible="false" /></small>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Image Title</label>
            <div class="col-sm-10">
                <asp:TextBox ID="imageTitle" CssClass="form-control" runat="server" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Image Alt Text</label>
            <div class="col-sm-10">
                <asp:TextBox ID="imageAlt" CssClass="form-control" runat="server" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Image Caption</label>
            <div class="col-sm-10">
                <asp:TextBox ID="imageCaption" CssClass="form-control" runat="server" MaxLength="255" />
            </div>
        </div>
    </div>
    <div id="line_divImage1" runat="server" visible="false" class="hr-line-dashed"></div>

    <div id="divImage2" runat="server" visible="false">
        <div class="form-group">
            <label class="col-sm-2 control-label">
                <asp:Literal runat="server" ID="labelImage2Name" /></label>
            <div class="col-sm-4">
                <asp:FileUpload ID="Image2Upload" Width="100%" CssClass="btn btn-outline btn-success" type="file" runat="server" />
            </div>
            <div class="col-sm-6">
                <asp:HiddenField ID="hdnImage2Dims" runat="server" />
                <asp:Label ID="image2_instr" runat="server" />
            </div>
        </div>
        <div class="form-group">
            <FC:ExistingImagesDropdown ID="UCExistingImageDropdown2" runat="server" />
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">
            </label>
            <div class="col-sm-10">
                <asp:CheckBox runat="server" ID="overwriteImg2" CssClass="input_enabled" Checked="false" />
                &nbsp;<small class="text-navy">Overwrite existing image</small>
                <br />
                <br />
                <asp:Image runat="server" ID="image2" CssClass="image" style="outline: medium none;" BorderStyle="None" Height="100px" Visible="false" />
                <br />
                <asp:CheckBox runat="server" ID="deleteImage2" CssClass="input_enabled" Checked="false"
                    Visible="false" />&nbsp;<small class="text-danger"><asp:Label ID="deleteLabel2" runat="server" Text="Remove this image" Visible="false" /></small>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Image Title</label>
            <div class="col-sm-10">
                <asp:TextBox ID="image2Title" CssClass="form-control" runat="server" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Image Alt Text</label>
            <div class="col-sm-10">
                <asp:TextBox ID="image2Alt" CssClass="form-control" runat="server" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Image Caption</label>
            <div class="col-sm-10">
                <asp:TextBox ID="image2Caption" CssClass="form-control" runat="server" TextMode="MultiLine" />
            </div>
        </div>
    </div>
    <div id="line_divImage2" runat="server" visible="false" class="hr-line-dashed"></div>

    <div id="divFileUpload" runat="server" visible="false">
        <div class="form-group">
            <label class="col-sm-2 control-label">
                <asp:Literal runat="server" ID="labelFileUpload" />
            </label>
            <div class="col-sm-4">
                <asp:FileUpload ID="FileUpload" Width="100%" CssClass="btn btn-outline btn-success" type="file" runat="server" />
            </div>
            <div class="col-sm-4">
                <div id="divRemoveExistingFile" runat="server" visible="true" style="padding-bottom: 5px;">
                    <asp:TextBox ID="ExistingFile" CssClass="form-control" runat="server" ReadOnly="true" />
                    <asp:CheckBox runat="server" ID="deleteFile" CssClass="input_enabled" Checked="false" /><asp:Label
                        ID="labelRemoveFile" runat="server" Text="Remove this file" />
                </div>
            </div>
            <div class="col-sm-2">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">
                File Type
            </label>
            <div class="col-sm-4">
                &nbsp;<asp:DropDownList ID="fileType" CssClass="form-control m-b"
                    runat="server" />
            </div>
            <div class="col-sm-6">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">
            </label>
            <div class="col-sm-10">
                <asp:CheckBox runat="server" ID="overwriteFile" CssClass="input_enabled" Checked="false" />
                &nbsp;<small class="text-navy">Overwrite existing file</small>
            </div>
        </div>
    </div>
    <div id="link_divFileUpload" runat="server" visible="false" class="hr-line-dashed"></div>
    <div id="divFile2Upload" runat="server" visible="false">
        <div class="form-group">
            <label class="col-sm-2 control-label">
                <asp:Literal runat="server" ID="labelFile2Upload" />
            </label>
            <div class="col-sm-4">
                <asp:FileUpload ID="File2Upload" Font-Size="X-Small" CssClass="btn btn-outline btn-success" type="file" runat="server" />

            </div>
            <div class="col-sm-4">
                <div id="divRemoveExistingFile2" runat="server" visible="false" style="padding-bottom: 5px;">
                    <asp:TextBox ID="ExistingFile2" CssClass="form-control" runat="server" ReadOnly="true" />
                    <asp:CheckBox runat="server" ID="deleteFile2" CssClass="input_enabled" Checked="false" /><asp:Label
                        ID="labelRemoveFile2" runat="server" Text="Remove this file" />
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">
                File Type
            </label>
            <div class="col-sm-4">
                <asp:DropDownList ID="file2Type" CssClass="form-control m-b"
                    runat="server" />
            </div>
            <div class="col-sm-6">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">
            </label>
            <div class="col-sm-10">
                <asp:CheckBox runat="server" ID="overwriteFile2" CssClass="input_enabled" Checked="false" />
                &nbsp;<small class="text-navy">Overwrite existing file</small>
            </div>
        </div>
    </div>
    <div id="link_divFile2Upload" runat="server" visible="false" class="hr-line-dashed"></div>
    <div class="form-group">
        <div class="col-sm-4 col-sm-offset-2">
            <asp:Button ID="btnCreate" CssClass="btn btn-info" type="submit" runat="server" OnClick="Create_Click" Text="Create" />
            <asp:Button ID="btnUpdate" CssClass="btn btn-primary" type="submit" runat="server" OnClick="Update_Click" Text="Update" />
        </div>
    </div>
</div>
