﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using FLContentSiteAssist.BusinessLogic;
using FLContentSiteAssist.Common;


namespace CMS.FluentControls
{
    public partial class LinkLocationSettings : System.Web.UI.UserControl
    {

        protected void Page_Load(object sender, EventArgs e)
        {

            if (IsPostBack)
            {

                string CtrlID = string.Empty;
                if (Request.Form[hidSourceID.UniqueID] != null &&
                    Request.Form[hidSourceID.UniqueID] != string.Empty)
                {
                    CtrlID = Request.Form[hidSourceID.UniqueID];
                }
            }
            else
            {
                Refresh_Screen();
            }

        }

        protected void Update_Click(object sender, EventArgs e)
        {
            // Look through every row of links and update the database with their values
            string _listsequence = listSequence.Value;
            string[] _listorder = listSequence.Value.Split(";".ToCharArray());

            for (int i = 1; i <= Convert.ToInt16(hiddenRows.Value); i++)
            {
                Link link = new Link();

                link.LinkID = Convert.ToInt16(Request.Form["id" + i.ToString()]);
                link.Title = Request.Form["title" + i.ToString()];
                link.Url = Request.Form["link" + i.ToString()];
                link.Sequence = listIndex(_listorder, "li1_" + i);
                if (Request.Form["enabled" + i.ToString()] == "on")
                {
                    link.Enabled = true;
                }
                else
                {
                    link.Enabled = false;
                }
                bool _delete = false;
                if (Request.Form["delete" + i.ToString()] == "on")
                {
                    _delete = true;
                }

                if (!_delete)
                {
                    ProcessUpdateLink updatelink = new ProcessUpdateLink();
                    updatelink.Link = link;
                    updatelink.Invoke();
                }
                else
                {
                    ProcessDeleteLink deletelink = new ProcessDeleteLink();
                    deletelink.Link = link;
                    deletelink.Invoke();
                }

            }

            Refresh_Screen();

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Successfully updated','success');", true);
        }

        protected void Add_Click(object sender, EventArgs e)
        {
            Link link = new Link();
            link.LocationID = Convert.ToInt16(Request.QueryString["location"]);
            link.Title = addTitle.Text;
            link.Url = addLink.Text;
            link.Sequence = Convert.ToInt16(hiddenRows.Value) + 1;
            link.Enabled = addEnabled.Checked;

            ProcessInsertLink insertlink = new ProcessInsertLink();
            insertlink.Link = link;
            insertlink.Invoke();

            Refresh_Screen();
        }

        protected void Refresh_Screen()
        {
            ProcessGetLinkByLocationID links = new ProcessGetLinkByLocationID();
            Link linkclass = new Link();
            linkclass.LocationID = Convert.ToInt16(Request.QueryString["location"]);
            linkclass.ViewDisabled = true;
            links.Link = linkclass;
            links.Invoke();

            DataSet linkdata = links.ResultSet;
            StringBuilder _linkhtml = new StringBuilder();
            int _linkcount = 1;
            count.Text = "0";
            if (linkdata.Tables[0].Rows.Count > 0)
            {
                count.Text = linkdata.Tables[0].Rows.Count.ToString();
                foreach (DataRow dr in linkdata.Tables[0].Rows)
                {                    
                    //Build the screen to show the links.
                    _linkhtml.Append("<li id=\"li1_" + _linkcount + "\" name=\"li1_" + _linkcount + "\" class=\"listrow\"><img id=\"mv1" + _linkcount + "\" src=\"FluentControls/images/reorder.jpg\" alt=\"click and hold to drag up or down\" title=\"click and hold to drag up or down\" />");
                    _linkhtml.Append("<input type=\"text\" id=\"title" + _linkcount + "\" name=\"title" + _linkcount + "\" class=\"input_title\" value=\"" + dr["chrLinkTitle"] + "\" />");
                    _linkhtml.Append("<input type=\"text\" id=\"link" + _linkcount + "\" name=\"link" + _linkcount + "\" class=\"input_link\" value=\"" + dr["chrLinkURL"] + "\" />");
                    if (dr["bitEnabled"].ToString() == "True")
                    {
                        _linkhtml.Append("<input type=\"checkbox\" id=\"enabled" + _linkcount + "\" name=\"enabled" + _linkcount + "\" class=\"input_enabled\" checked=\"true\" />");
                    }
                    else
                    {
                        _linkhtml.Append("<input type=\"checkbox\" id=\"enabled" + _linkcount + "\" name=\"enabled" + _linkcount + "\" class=\"input_enabled\" />");
                    }
                    _linkhtml.Append("<input type=\"checkbox\" id=\"delete" + _linkcount + "\" name=\"delete" + _linkcount + "\" class=\"input_delete\"/>");
                    _linkhtml.Append("<input type=\"hidden\" id=\"id" + _linkcount + "\" name=\"id" + _linkcount + "\" value=\"" + dr["intLinkID"] + "\" />");
                    _linkhtml.Append("</li>");
                    _linkcount += 1;

                    _linkcount += 1;
                }
                linkrows.Text = _linkhtml.ToString();

            }
            hiddenRows.Value = Convert.ToString(_linkcount - 1);
        }

        private int listIndex(string[] inputArray, string searchTerm)
        {
            int firstIndex = 0;

            for (int i = 0; i < inputArray.Length; i++)
            {
                if (inputArray[i] == searchTerm)
                {
                    firstIndex = i;
                    break;
                }

            }
            return firstIndex + 1;
        }
    }
}