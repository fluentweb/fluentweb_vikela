﻿using FluentContent.DAC;
using FluentContent.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Extensions;
using CMS.Helpers;
using FLContentSiteAssist.BusinessLogic;
using FluentContent.Entities.Helpers;
using System.Collections.Specialized;
using System.IO;
using System.Drawing.Imaging;
using System.Data;
using Common.Url;
using Common.Session;

namespace CMS.FluentControls
{
    public partial class TMCountryDocumentDetail : System.Web.UI.UserControl
    {
        public int CountryDocumentID { get { return Request.GetQueryString<int>("countrydocumentid", -1); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillDrowpDown();
                if (CountryDocumentID != -1)
                {
                    FillScreen(CountryDocumentID);
                }
            }
        }

        private void FillScreen(int CountryDocumentID)
        {
            TMCountryDocument tmcd = DAC_TMCountryDocument.GetTMCountryDocument(CountryDocumentID);
            if (tmcd == null) return;
            ddcountryName.SelectedValue = tmcd.CountryID.ToString();
            chk_ConventionPriorityClaimed.Checked = tmcd.ConventionPriorityClaimed;
            txt_DocumentDesc.Text = tmcd.DocumentDesc;
        }

        private void FillDrowpDown()
        {
            //Country dd
            ddcountryName.DataSource = DAC_TMCountry.GetCountryByCountry();
            ddcountryName.DataTextField = "chrCountry";
            ddcountryName.DataValueField = "intCountryID";
            ddcountryName.DataBind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void Save()
        {
            TMCountryDocument tmcd = new TMCountryDocument();
            tmcd.CountryID = ddcountryName.SelectedValue.ToInt32();
            tmcd.ConventionPriorityClaimed = chk_ConventionPriorityClaimed.Checked;
            tmcd.DocumentDesc = txt_DocumentDesc.Text.Trim();
            tmcd.CountryDocumentID = CountryDocumentID;

            if (CountryDocumentID > 0)
            {
                tmcd = DAC_TMCountryDocument.Update(tmcd);
                if (tmcd == null)
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Update failed!','warning');", true);
                else
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Country successfully updated.','success');", true);
            }
            else
            {
                tmcd = DAC_TMCountryDocument.Insert(tmcd);
                if (tmcd == null)
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Create failed!','warning');", true);
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('New country successfully created.  You may now create another...','success');", true);
                    ClearPage();
                }
            }
        }

        private void ClearPage()
        {
            ddcountryName.SelectedIndex = 0;
            chk_ConventionPriorityClaimed.Checked = false;
            txt_DocumentDesc.Text = string.Empty;
        }
    }
}