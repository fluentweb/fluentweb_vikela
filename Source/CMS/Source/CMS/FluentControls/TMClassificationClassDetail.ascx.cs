﻿using FluentContent.DAC;
using FluentContent.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Extensions;
using CMS.Helpers;
using FLContentSiteAssist.BusinessLogic;
using FluentContent.Entities.Helpers;
using System.Collections.Specialized;
using System.IO;
using System.Drawing.Imaging;
using System.Data;
using Common.Url;
using Common.Session;

namespace CMS.FluentControls
{
    public partial class TMClassificationClassDetail : System.Web.UI.UserControl
    {
        public int ClassificationID { get { return Request.GetQueryString<int>("classificationid", -1); } }
        public int ClassificationClassID { get { return Request.GetQueryString<int>("classificationclassid", -1); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (ClassificationClassID != -1)
                {
                    FillScreen(ClassificationClassID);
                }
            }
        }

        private void FillScreen(int ClassificationClassID)
        {
            TMClassificationClass tmc = DAC_TMClassificationClass.GetClassificationByClass(ClassificationClassID);
            if (tmc == null) return;
            txtclassname.Text = tmc.ClassificationClass.ToString();
            txtClassCode.Text = tmc.ClassificationClassCode.ToString();
            txtclassnumber.Text = tmc.ClassificationClassNo == 0 ? string.Empty : tmc.ClassificationClassNo.ToString();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void Save()
        {
            if (!Page.IsValid) return;

            TMClassificationClass tmc = new TMClassificationClass();
            tmc.ClassificationClass = txtclassname.Text.Trim();
            tmc.ClassificationClassCode = txtClassCode.Text.Trim();
            tmc.ClassificationClassNo = string.IsNullOrEmpty(txtclassnumber.Text.Trim()) ? 0 : txtclassnumber.Text.Trim().ToInt32();
            tmc.TMClassificationID = ClassificationID;
            tmc.TMClassificationClassID = ClassificationClassID;

            if (ClassificationClassID > 0)
            {
                tmc = DAC_TMClassificationClass.Update(tmc);
                if (tmc == null)
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Update failed!','warning');", true);
                else
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Class successfully updated.','success');", true);
            }
            else
            {
                tmc = DAC_TMClassificationClass.Insert(tmc);
                if (tmc == null)
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Create failed!','warning');", true);
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('New Class successfully created.  You may now create another...','success');", true);
                    ClearPage();
                }
            }
        }

        private void ClearPage()
        {
            txtclassname.Text = string.Empty;
            txtClassCode.Text = string.Empty;
            txtclassnumber.Text = string.Empty;
        }
    }
}