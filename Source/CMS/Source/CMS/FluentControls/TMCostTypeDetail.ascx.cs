﻿using FluentContent.DAC;
using FluentContent.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Extensions;
using CMS.Helpers;
using FLContentSiteAssist.BusinessLogic;
using FluentContent.Entities.Helpers;
using System.Collections.Specialized;
using System.IO;
using System.Drawing.Imaging;
using System.Data;
using Common.Url;
using Common.Session;

namespace CMS.FluentControls
{
    public partial class TMCostTypeDetail : System.Web.UI.UserControl
    {
        public int CostTypeID { get { return Request.GetQueryString<int>("costtypeid", -1); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (CostTypeID != -1)
                {
                    FillScreen(CostTypeID);
                }
            }
        }

        private void FillScreen(int costtypeid)
        {
            TMCostType tmc = DAC_TMCostType.GetCostType(costtypeid);
            if (tmc == null) return;
            txtCostTypecode.Text = tmc.CostTypeCode;
            txtCostType.Text = tmc.CostType;
            chk_permark.Checked = tmc.PerMark;
            chk_percountry.Checked = tmc.PerCountry;
            chk_perclass.Checked = tmc.PerClass;
            chk_perproduct.Checked = tmc.PerProduct;
            chk_perstate.Checked = tmc.PerState;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void Save()
        {
            if (!Page.IsValid) return;

            TMCostType tmc = new TMCostType();
            tmc.CostTypeCode = txtCostTypecode.Text.Trim();
            tmc.CostType = txtCostType.Text.Trim();
            tmc.PerMark = chk_permark.Checked;
            tmc.PerCountry = chk_percountry.Checked;
            tmc.PerClass = chk_perclass.Checked;
            tmc.PerProduct = chk_perproduct.Checked;
            tmc.PerState = chk_perstate.Checked;
            tmc.CostTypeID = CostTypeID;

            if (CostTypeID > 0)
            {
                tmc = DAC_TMCostType.Update(tmc);
                if (tmc == null)
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Update failed!','warning');", true);
                else
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Cost Type successfully updated.','success');", true);
            }
            else
            {
                tmc = DAC_TMCostType.Insert(tmc);
                if (tmc == null)
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Create failed!','warning');", true);
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('New Cost Type successfully created.  You may now create another...','success');", true);
                    ClearPage();
                }
            }
        }

        private void ClearPage()
        {
            txtCostType.ClearText();
            txtCostTypecode.ClearText();
            chk_permark.Checked = false;
            chk_percountry.Checked = false;
            chk_perclass.Checked = false;
            chk_perproduct.Checked = false;
            chk_perstate.Checked = false;
        }
    }
}