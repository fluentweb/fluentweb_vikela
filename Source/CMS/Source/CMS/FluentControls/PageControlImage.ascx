﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="PageControlImage.ascx.cs"
    Inherits="CMS.FluentControls.PageControlImage" %>
<asp:HiddenField ID="hidSourceID" runat="server" />
<div class="main">
    <div class="innerwindow">
        <asp:HiddenField ID="intPageID" runat="server" />
        <asp:HiddenField ID="numberOfImages" runat="server" />
        <div id="fillerLine" runat="server" class="updateMessage" style="height: 20px; width: 100%;">
            <asp:Label ID="updateMessage" runat="server" Visible="false" Text="Successfully updated" />
            <asp:Label ID="errorMessage" runat="server" Visible="false" Text="Invalid image dimensions, please check requirements next to each image" />
        </div>
        <asp:Panel ID="contentPanel" runat="server">
        </asp:Panel>
        <asp:Button ID="btnSave" runat="server" OnClick="Update_Click" Text="Save" />
    </div>
</div>
