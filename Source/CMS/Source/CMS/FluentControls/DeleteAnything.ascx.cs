﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FLContentSiteAssist.BusinessLogic;
using FLContentSiteAssist.Common;
using System.Data;
using System.Text;

namespace CMS.FluentControls
{
    public partial class DeleteAnything : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                string objectType = Request.QueryString["type"];
                int objectID = Convert.ToInt16(Request.QueryString["id"]);

                if (objectType == "thr")
                {
                    StringBuilder _warning = new StringBuilder();
                    _warning.Append("Are you sure you want to delete this thread?");

                    Blog blog = new Blog();
                    blog.ThreadID = objectID;
                    ProcessGetBlogThread threaddata = new ProcessGetBlogThread();
                    threaddata.Blog = blog;
                    threaddata.Invoke();

                    if (threaddata.ResultSet.Tables[0].Rows.Count > 0)
                    {
                        DataRow dr = threaddata.ResultSet.Tables[0].Rows[0];
                        _warning.Append("<br/><br/>");
                        _warning.Append("Created " + Convert.ToDateTime(dr["datCreated"].ToString()).ToString("d/mm/yyyy") + ". Titled \"" + dr["chrTitle"] + "\"");
                    }

                    warningText.Text = _warning.ToString();

                }

                if (objectType == "thrrep")
                {
                    StringBuilder _warning = new StringBuilder();
                    _warning.Append("Are you sure you want to delete this thread reply?");

                    Blog blog = new Blog();
                    blog.ReplyID = objectID;
                    ProcessGetBlogReply threaddata = new ProcessGetBlogReply();
                    threaddata.Blog = blog;
                    threaddata.Invoke();

                    if (threaddata.ResultSet.Tables[0].Rows.Count > 0)
                    {
                        DataRow dr = threaddata.ResultSet.Tables[0].Rows[0];
                        _warning.Append("<br/><br/>");
                        _warning.Append("Submitted by " + dr["chFirstName"] + " " + dr["chSecondName"] + "," + Convert.ToDateTime(dr["datCreated"].ToString()).ToString("d/mm/yyyy"));
                    }

                    warningText.Text = _warning.ToString();

                }

            }
        }

        protected void Delete_Click(object sender, EventArgs e)
        {
            int objectID = Convert.ToInt16(Request.QueryString["id"]);
            string objectType = Request.QueryString["type"];

            // Delete a blog thread
            if (objectType == "thr")
            {
                Blog blog = new Blog();
                blog.ThreadID = objectID;
                ProcessGetBlogThread threaddata = new ProcessGetBlogThread();
                threaddata.Blog = blog;
                threaddata.Invoke();

                if (threaddata.ResultSet.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = threaddata.ResultSet.Tables[0].Rows[0];
                    blog.Title = dr["chrTitle"].ToString(); ;
                    blog.Comment = dr["chrComment"].ToString();
                    blog.UserID = System.Convert.ToInt16(dr["intUserID"].ToString());
                    blog.URL = dr["chrThreadURL"].ToString();
                    blog.Status = "Deleted";
                    ProcessUpdateBlogThread threadupdate = new ProcessUpdateBlogThread();
                    threadupdate.Blog = blog;
                    threadupdate.Invoke();
                }

            }

            // Delete a blog thread
            if (objectType == "thrrep")
            {
                Blog blog = new Blog();
                blog.ReplyID = objectID;
                ProcessGetBlogReply threaddata = new ProcessGetBlogReply();
                threaddata.Blog = blog;
                threaddata.Invoke();

                if (threaddata.ResultSet.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = threaddata.ResultSet.Tables[0].Rows[0];
                    blog.Comment = dr["chrComment"].ToString();
                    blog.Status = "Deleted";
                    ProcessUpdateBlogReply threadupdate = new ProcessUpdateBlogReply();
                    threadupdate.Blog = blog;
                    threadupdate.Invoke();
                }
            }

            ScriptManager.RegisterStartupScript(this, typeof(string), "script", "<script type=text/javascript>parent.location.href = parent.location.href;</script>", false);
        }
    }

}