﻿using FluentContent.DAC;
using FluentContent.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Extensions;
using CMS.Helpers;
using FLContentSiteAssist.BusinessLogic;
using FluentContent.Entities.Helpers;
using System.Collections.Specialized;
using System.IO;
using System.Drawing.Imaging;
using System.Data;
using Common.Url;
using Common.Session;
using FluentContent.Entities.Enums;

namespace CMS.FluentControls
{
    public partial class TMCountryAdditionalRequirementChoiceDetail : System.Web.UI.UserControl
    {
        public int CountryAdditionalReqId { get { return Request.GetQueryString<int>("countryadditionalrequirementid", -1); } }
        public int CountryAdditionalReqCId { get { return Request.GetQueryString<int>("countryadditionalrequirementchoiceid", -1); } }
      
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillDrowpDown();
                if (CountryAdditionalReqCId != -1)
                {
                    FillScreen(CountryAdditionalReqCId);
                }
            }
        }

        private void FillScreen(int countryID)
        {
            TMCountryAdditionalRequirementChoice tmcarc = DAC_TMCountryAdditionalRequirementChoice.GetCountryAdditionalRequirementChoice(CountryAdditionalReqCId, CountryAdditionalReqId);
            if (tmcarc == null) return;
            ddchoiceType.SelectedValue = tmcarc.CostTypeID.ToString();
            txtChoiceText.Text = tmcarc.ChoiceText.ToString();
        }

        private void FillDrowpDown()
        {
            //Country dd
            DataTable dt = DAC_TMCostType.GetCostType();
            DataRow drEmpty;
            drEmpty = dt.NewRow();
            drEmpty["chrCostType"] = "";
            drEmpty["intCostTypeID"] = 0;
            dt.Rows.InsertAt(drEmpty, 0);
            ddchoiceType.DataSource = dt;
            ddchoiceType.DataTextField = "chrCostType";
            ddchoiceType.DataValueField = "intCostTypeID";

            ddchoiceType.DataBind();

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void Save()
        {
            TMCountryAdditionalRequirementChoice tmcarc = new TMCountryAdditionalRequirementChoice();
            tmcarc.CountryAdditionalRequirementChoiceID = CountryAdditionalReqCId;
            tmcarc.CountryAdditionalRequirementID = CountryAdditionalReqId;
            tmcarc.CostTypeID = ddchoiceType.SelectedValue.ToInt32();
            tmcarc.ChoiceText = txtChoiceText.Text.Trim();

            if (CountryAdditionalReqCId > 0)
            {
                tmcarc = DAC_TMCountryAdditionalRequirementChoice.Update(tmcarc);
                if (tmcarc == null)
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Update failed!','warning');", true);
                else
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Country successfully updated.','success');", true);
            }
            else
            {
                tmcarc = DAC_TMCountryAdditionalRequirementChoice.Insert(tmcarc);
                if (tmcarc == null)
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Create failed!','warning');", true);
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('New country successfully created.  You may now create another...','success');", true);
                    ClearPage();
                }
            }
        }

        private void ClearPage()
        {
            ddchoiceType.SelectedIndex = 0;
            txtChoiceText.Text = string.Empty;
        }
    }
}