﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FLContentSiteAssist.Common;
using FLContentSiteAssist.BusinessLogic;
using System.Data;
using System.Text;
using Common.Extensions;
using FluentContent.DAC;


namespace CMS.FluentControls
{
    public partial class RelatedFormRelate : System.Web.UI.UserControl
    {
        public int ItemTypeID { get { return Request.GetQueryString<int>("itemtype", 0); } }
        public int ItemID { get { return Request.GetQueryString<int>("itemid", 0); } }
        public int FormTypeID { get { return Request.GetQueryString<int>("formtype", 0); } }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (IsPostBack)
            {

                string CtrlID = string.Empty;
                if (Request.Form[hidSourceID.UniqueID] != null &&
                    Request.Form[hidSourceID.UniqueID] != string.Empty)
                {
                    CtrlID = Request.Form[hidSourceID.UniqueID];
                }
            }
            else
            {
                if (ItemID != 0)
                {
                    Refresh_Screen();
                }
            }

        }

        protected void Update_Click(object sender, EventArgs e)
        {
            //Odd behaviour in Chrome and Firefox causes Update_Click to fire event though add_click is triggered
            string check = Request.Form[hidSourceID.UniqueID];
            if (check == "add")
            {
                Add();
                return;
            }

            // Look through every row of items and update the database with their values
        
            for (int i = 1; i <= Convert.ToInt16(hiddenRows.Value); i++)
            {
                int relationID = Convert.ToInt32(Request.Form["id" + i.ToString()]);
               
                bool _delete = false;
                if (Request.Form["delete" + i.ToString()] == "on")
                {
                    _delete = true;
                }

                if (_delete)
                {
                    DAC_ItemFormRelation.Delete(relationID);
                }
            }

            Refresh_Screen();

            updateMessage.Visible = true;
        }

        protected void Add_Click(object sender, EventArgs e)
        {
            Add();
        }

        protected void Add()
        {
            int id = DAC_ItemFormRelation.Insert(ItemID, addTitle.SelectedValue.ToInt32());

            Refresh_Screen();
        }


        protected void Refresh_Screen()
        {
            DataTable availableForms = DAC_ItemFormRelation.GetAvailableRelatedForms(ItemID, FormTypeID);

            if (availableForms.Rows.Count > 0)
            {
                addTitle.DataSource = availableForms;
                addTitle.DataTextField = "chrForm";
                addTitle.DataValueField = "intFormID";
                addTitle.DataBind();
                add.Visible = true;
            }
            else
            {
                addTitle.Items.Clear();
                addTitle.Items.Insert(0, "No forms of this type available");
                addTitle.Enabled = false;
                add.Visible = false;
            }

            DataTable relatedForms = DAC_ItemFormRelation.GetItemRelatedForms(ItemID, FormTypeID);

            if (relatedForms.Rows.Count > 0)
            {
                StringBuilder _linkhtml = new StringBuilder();
                int _linkcount = 1;

                foreach (DataRow dr in relatedForms.Rows)
                {
                    _linkhtml.Append("<li id=\"li1_" + _linkcount + "\" name=\"li1_" + _linkcount + "\" class=\"listrow\">");
                    _linkhtml.Append("<input type=\"text\" id=\"title" + _linkcount + "\" name=\"title" + _linkcount + "\" class=\"input_title\" value=\"" + dr["chrForm"] + "\" readonly=\"readonly\" />");
                    _linkhtml.Append("<input type=\"checkbox\" id=\"delete" + _linkcount + "\" name=\"delete" + _linkcount + "\" class=\"input_delete\"/>");
                    _linkhtml.Append("<input type=\"hidden\" id=\"id" + _linkcount + "\" name=\"id" + _linkcount + "\" value=\"" + dr["intItemFormRelateID"] + "\" />");
                    _linkhtml.Append("</li>");
                    _linkcount += 1;
                }

                linkrows.Text = _linkhtml.ToString();
                hiddenRows.Value = Convert.ToString(_linkcount - 1);
                divDelete.Visible = true;
            }
            else
            {
                linkrows.Text = String.Empty;
                divDelete.Visible = false;
            }
        }

        private int listIndex(string[] inputArray, string searchTerm)
        {
            int firstIndex = 0;

            for (int i = 0; i < inputArray.Length; i++)
            {
                if (inputArray[i] == searchTerm)
                {
                    firstIndex = i;
                    break;
                }

            }
            return firstIndex + 1;
        }
    }
}