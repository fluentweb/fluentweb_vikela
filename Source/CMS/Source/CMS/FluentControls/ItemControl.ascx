﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ItemControl.ascx.cs"
    Inherits="CMS.FluentControls.ItemControl" %>

<script type="text/javascript">

    $(function() {
        $('[id$="datepickerfr"]').datepicker({ dateFormat: 'dd/mm/yy' });
        $('[id$="datepickerto"]').datepicker({ dateFormat: 'dd/mm/yy' });
    });
     
</script>

<asp:HiddenField ID="hidSourceID" runat="server" />
<div class="main">
    <div class="innerwindow">
        <asp:HiddenField ID="intItemID" runat="server" />
        <div id="fillerLine" runat="server" class="updateMessage" style="height: 20px; width: 100%;">
            <asp:Label ID="updateMessage" runat="server" Visible="false" Text="New item successfully created.  You may now create another..." />
        </div>
        <div class="input_row" onmousedown="var msg = document.getElementById('<%=updateMessage.ClientID%>');if (msg!=null){msg.style.visibility = 'hidden';};">
            <div class="field_name">
                Item Title</div>
            <div class="field_control">
                <asp:TextBox ID="itemTitle" CssClass="input_normal" runat="server" /></div>
        </div>
        <div class="input_row">
            <div class="field_name">
                Item Extract</div>
            <div class="field_control">
                <asp:TextBox ID="itemExtract" CssClass="input_large" runat="server" TextMode="MultiLine"
                    Rows="3" /></div>
        </div>
        <div class="input_row">
            <div class="field_name">
                Item Text</div>
            <div class="field_control">
                <asp:TextBox ID="itemText" CssClass="input_large" runat="server" TextMode="MultiLine"
                    Rows="3" /></div>
        </div>
        <div class="input_row">
            <div class="field_name">
                Date from :</div>
            <div class="field_control">
                <asp:TextBox ID="datepickerfr" CssClass="input_date" runat="server" />&nbsp;Time
                (hh:mm):
                <asp:TextBox ID="timeFrom" CssClass="input_small" runat="server" />
                &nbsp;&nbsp;Date To:
                <asp:TextBox ID="datepickerto" CssClass="input_date" runat="server" />&nbsp;Time:
                <asp:TextBox ID="timeTo" CssClass="input_small" runat="server" />
            </div>
        </div>
        <div class="input_row">
            <div class="field_name">
                Item URL</div>
            <div class="field_control">
                <asp:TextBox ID="itemURL" CssClass="input_normal" runat="server" /></div>
        </div>
        <div class="input_row">
            <div class="field_name">
                Item Image</div>
            <div class="field_control">
                <asp:FileUpload ID="ImageUpload" runat="server" />
                &nbsp;<asp:Label ID="image_instr" runat="server" />
                <br />
                <asp:CheckBox runat="server" ID="overwriteImg" CssClass="input_enabled" Checked="false" />
                Overwrite existing image
                <div class="imagebox">
                    <asp:Image runat="server" ID="image" CssClass="image" Visible="false" /></div>
                <asp:CheckBox runat="server" ID="deleteImage" CssClass="input_enabled" Checked="false"
                    Visible="false" /><asp:Label ID="deleteLabel" runat="server" Text="Remove this image"
                        Visible="false" />
            </div>
        </div>
        <div class="input_row">
            <div class="field_name">
                Image Title</div>
            <div class="field_control">
                <asp:TextBox ID="imageTitle" CssClass="input_normal" runat="server" /></div>
        </div>
        <div class="input_row">
            <div class="field_name">
                Image Alt Text</div>
            <div class="field_control">
                <asp:TextBox ID="imageAlt" CssClass="input_normal" runat="server" /></div>
        </div>
        <div class="input_row">
            <div class="field_name">
                Image Caption</div>
            <div class="field_control">
                <asp:TextBox ID="imageCaption" CssClass="input_normal" runat="server" /></div>
        </div>
        <div class="input_row">
            <div class="field_name">
                Image Text</div>
            <div class="field_control">
                <asp:TextBox ID="imageText" CssClass="input_large" runat="server" TextMode="MultiLine"
                    Rows="3" /></div>
        </div>
        <div class="input_row">
            <div class="field_name">
                Item Link</div>
            <div class="field_control">
                <asp:TextBox ID="itemLink" CssClass="input_large" runat="server" TextMode="MultiLine"
                    Rows="3" /></div>
        </div>
        <div class="input_row">
            <div class="field_name">
                Sequence</div>
            <div class="field_control">
                <asp:DropDownList ID="sequenceID" CssClass="dropdown_small" runat="server" />
            </div>
        </div>
        <div class="input_row">
            <div class="field_name">
                Enabled</div>
            <div class="field_control">
                <asp:CheckBox runat="server" ID="enabled" CssClass="input_enabled" /></div>
        </div>
    </div>
    <asp:Button ID="btnCreate" runat="server" OnClick="Create_Click" Text="Create" />
    <asp:Button ID="btnUpdate" runat="server" OnClick="Update_Click" Text="Save" />
</div>
