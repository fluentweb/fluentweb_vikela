﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FLContentSiteAssist.BusinessLogic;
using FLContentSiteAssist.Common;
using System.Data;
using Common.Extensions;

namespace CMS.FluentControls
{
    public partial class PageTextEdit : System.Web.UI.UserControl
    {
        public int TextID { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Refresh_Screen();
            }
            //btnSave.OnClientClick = String.Format("GetEditedHTML('{0}', '{1}');", editedHTML.ClientID, textEditor.ClientID);
        }

        protected void Update_Click(object sender, EventArgs e)
        {
            PageText _pagetext = new PageText();

            _pagetext.Text = textEditor.Text;// editedHTML.Value.Trim();
            _pagetext.TextID = btnSave.CommandArgument.ToInt32(); // Convert.ToInt16(Request.QueryString["textid"]);

            ProcessUpdatePageText updatetext = new ProcessUpdatePageText();
            updatetext.PageText = _pagetext;
            updatetext.Invoke();

            // The following line sets the HTML editor back to its edited content 
            // as otherwise the postback clears it back to original state
            // textToEdit.Text = _pagetext.Text.Trim();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Successfully updated','success');", true);

        }

        private void Refresh_Screen()
        {
            PageText _pagetext = new PageText();

            ProcessGetPageTextByID _gettext = new ProcessGetPageTextByID();
            _pagetext.TextID = TextID;//Convert.ToInt16(Request.QueryString["textid"]);            
            btnSave.CommandArgument = TextID.ToString();
            _gettext.PageText = _pagetext;
            _gettext.Invoke();
            if (_gettext.ResultSet.Tables[0].Rows.Count > 0)
            {
                DataRow textdata = _gettext.ResultSet.Tables[0].Rows[0];                                
                textEditor.Text = textdata["chrPageText"].ToString().Trim();
            }

        }

    }
}