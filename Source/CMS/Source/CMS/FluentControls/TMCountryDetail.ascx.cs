﻿using FluentContent.DAC;
using FluentContent.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Extensions;
using CMS.Helpers;
using FLContentSiteAssist.BusinessLogic;
using FluentContent.Entities.Helpers;
using System.Collections.Specialized;
using System.IO;
using System.Drawing.Imaging;
using System.Data;
using Common.Url;
using Common.Session;

namespace CMS.FluentControls
{
    public partial class TMCountryDetail : System.Web.UI.UserControl
    {
        public int CountryID { get { return Request.GetQueryString<int>("countryid", -1); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillDrowpDown();
                if (CountryID != -1)
                {
                    FillScreen(CountryID);
                }
            }
        }

        private void FillScreen(int countryID)
        {
            TMCountry tmc = DAC_TMCountry.GetCountryByCountry(countryID);
            if (tmc == null) return;
            ddcountryName.SelectedValue = tmc.CountryID.ToString();
            chk_available.Checked = tmc.Available;
            chk_powerofattorney.Checked = tmc.PowerOfAttorney;
            chk_conventionpriority.Checked = tmc.ConventionPriority;
            chk_colourrestrictionbw.Checked = tmc.ColourRestrictionBW;
            chk_colourrestrictioncc.Checked = tmc.ColourRestrictionCC;
            chk_colourrestrictioncsm.Checked = tmc.ColourRestrictionCSM;
            chk_colourrestrictioncl.Checked = tmc.ColourRestrictionCL;
            ddtranslationlanguage.SelectedValue = tmc.TranslationLanguageID.ToString();
            ddclassificationid.SelectedValue = tmc.TMClassificationID.ToString();
        }

        private void FillDrowpDown()
        {
            //Country dd
            if (CountryID > 0)
            {
                ddcountryName.DataSource = DAC_SPFCountry.GetCountryByCountry(CountryID);
                ddcountryName.DataTextField = "chrCountry";
                ddcountryName.DataValueField = "intCountryID";
                ddcountryName.DataBind();
            }
            else
            {
                ddcountryName.DataSource = DAC_SPFCountry.GetCountryByCountry();
                ddcountryName.DataTextField = "chrCountry";
                ddcountryName.DataValueField = "intCountryID";
                ddcountryName.DataBind();
            }



            //Translat dd
            ddtranslationlanguage.DataSource = DAC_SPFLanguage.GetLanguageByLanguage();
            ddtranslationlanguage.DataTextField = "chrLanguage";
            ddtranslationlanguage.DataValueField = "intLanguageID";
            ddtranslationlanguage.DataBind();

            //Classification dd
            ddclassificationid.DataSource = DAC_TMClassification.GetClassificationByClassification();
            ddclassificationid.DataTextField = "chrTMClassificationTitle";
            ddclassificationid.DataValueField = "intTMClassificationID";
            ddclassificationid.DataBind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void Save()
        {
            TMCountry tmc = new TMCountry();
            tmc.CountryID = ddcountryName.SelectedValue.ToInt32();
            tmc.Available = chk_available.Checked;
            tmc.PowerOfAttorney = chk_powerofattorney.Checked;
            tmc.ConventionPriority = chk_conventionpriority.Checked;
            tmc.ColourRestrictionBW = chk_colourrestrictionbw.Checked;
            tmc.ColourRestrictionCC = chk_colourrestrictioncc.Checked;
            tmc.ColourRestrictionCSM = chk_colourrestrictioncsm.Checked;
            tmc.ColourRestrictionCL = chk_colourrestrictioncl.Checked;
            tmc.ConventionPriority = chk_conventionpriority.Checked;
            tmc.TranslationLanguageID = ddtranslationlanguage.SelectedValue.ToInt32();
            tmc.TMClassificationID = ddclassificationid.SelectedValue.ToInt32();

            if (CountryID > 0)
            {
                tmc = DAC_TMCountry.Update(tmc);
                if (tmc == null)
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Update failed!','warning');", true);
                else
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Country successfully updated.','success');", true);
            }
            else
            {
                tmc = DAC_TMCountry.Insert(tmc);
                if (tmc == null)
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Create failed!','warning');", true);
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('New country successfully created.  You may now create another...','success');", true);
                    ClearPage();
                }
                FillDrowpDown();
            }
        }

        private void ClearPage()
        {
            ddcountryName.SelectedIndex = 0;
            ddclassificationid.SelectedIndex = 0;
            ddtranslationlanguage.SelectedIndex = 0;
            chk_available.Checked = false;
            chk_powerofattorney.Checked = false;
            chk_conventionpriority.Checked = false;
            chk_colourrestrictionbw.Checked = false;
            chk_colourrestrictioncc.Checked = false;
            chk_colourrestrictioncsm.Checked = false;
            chk_colourrestrictioncl.Checked = false;
        }
    }
}