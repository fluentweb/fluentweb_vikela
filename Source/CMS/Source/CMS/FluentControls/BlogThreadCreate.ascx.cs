﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FLContentSiteAssist.BusinessLogic;
using FLContentSiteAssist.Common;

namespace CMS.FluentControls
{
    public partial class BlogThreadCreate : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Refresh_Screen();
            }
        }

        protected void Create_Click(object sender, EventArgs e)
        {
            //Requires at a title, comment and an author
            if (title.Text != "" && editedHTML.Value.Trim() != "" && author.SelectedIndex > -1)
            {
                if (threadURL.Text.Trim() == "") { threadURL.Text = threadURL.Text.Replace(" ", "_"); }
                Blog blog = new Blog();
                blog.BlogTypeID = Convert.ToInt16(Request.QueryString["blogtype"]);
                blog.UserID = Convert.ToInt16(author.SelectedValue);
                blog.Title = title.Text.Trim();
                blog.Comment = editedHTML.Value.Trim();
                blog.Status = "Approved";
                blog.URL = threadURL.Text;


                ProcessInsertBlogThread _insertthread = new ProcessInsertBlogThread();
                _insertthread.Blog = blog;
                _insertthread.Invoke();

                //Now clear the screen and show the success message...
                title.Text = "";
                threadURL.Text = "";
                editedHTML.Value = "";

               // updateMessage.Visible = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('New thread successfully created. You may now create another...','success');", true);

            }

            /*  PageText _pagetext = new PageText();

            _pagetext.Text = editedHTML.Value.Trim();
            _pagetext.TextID = Convert.ToInt16(Request.QueryString["textid"]);
            */
        }

        private void Refresh_Screen()
        {
            ProcessGetUser _userlist = new ProcessGetUser();
            _userlist.SiteName = "Arts In Health Care";
            _userlist.Invoke();
            if (_userlist.ResultSet.Tables[0].Rows.Count > 0)
            {
                author.DataSource = _userlist.ResultSet;
                author.DataValueField = "intUserID";
                author.DataTextField = "chrFullname";
                author.DataBind();
            }

        }
    }
}