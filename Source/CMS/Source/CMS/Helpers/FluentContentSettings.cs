﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS.Helpers
{
    public class FluentContentSettings
    {
        /// <summary>
        /// Physica path
        /// </summary>
        public static string ImageUploadLocation { get; set; }
        /// <summary>
        /// Physical path
        /// </summary>
        public static string DocumentUploadLocation { get; set; }

        /// <summary>
        /// Virtual path
        /// </summary>
        public static string ImageUploadFolder { get; set; }

        /// <summary>
        /// Virtual path
        /// </summary>
        public static string DocumentUploadFolder { get; set; }
        public static string WebsiteRoot { get; set; }
    }
}
