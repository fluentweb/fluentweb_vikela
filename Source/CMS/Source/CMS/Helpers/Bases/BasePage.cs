﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Common.Session;
using FluentContent.Entities;

namespace CMS.Helpers.Bases
{
    public class BasePage : Page
    {
        public CMSUser UserSession { get { return SessionObjects.GetSession<CMSUser>(SessionKeys.CMS_USER_SESSION); } }
    }
}
