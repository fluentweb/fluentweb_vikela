﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentContent.Entities;
using FluentContent.DAC;
using FLContentSiteAssist.BusinessLogic;
using Common.Extensions;
using System.Data;
using System.Web.UI.WebControls;

namespace CMS.Helpers
{
    public class ItemHelper
    {
        public static string GetItemTypeName(int itemTypeID)
        {
            ItemType type = DAC_ItemType.Get(itemTypeID);
            if (type == null) return String.Empty;
            return type.ItemTypeName;
        }

        public static string GetItemNameByID(int itemID)
        {
            ProcessGetItemByID getItem = new ProcessGetItemByID();

            getItem.Item = new FLContentSiteAssist.Common.Item { ItemID = itemID };
            getItem.Invoke();

            if (getItem.ResultSet.HasRows(0))
                return getItem.ResultSet.Tables[0].GetDataTableValue(0, "chrItemTitle", String.Empty).Replace("<br />", String.Empty);
            return String.Empty;

        }

        public static void LoadItem(string itemType, ListView lv)
        {
            ProcessGetItem getItem = new ProcessGetItem();
            getItem.Item = new FLContentSiteAssist.Common.Item { ItemType = itemType, ViewDisabled = false };
            getItem.Invoke();
            lv.DataSource = ReplaceDomainNames(getItem.ResultSet);
            lv.DataBind();
        }

        public static DataTable LoadItem(string itemType)
        {
            ProcessGetItem getItem = new ProcessGetItem();
            getItem.Item = new FLContentSiteAssist.Common.Item { ItemType = itemType, ViewDisabled = false };
            getItem.Invoke();
            if (getItem.ResultSet.HasRows(0))
                return ReplaceDomainNames(getItem.ResultSet).Tables[0];
            return null;
        }

        public static DataTable LoadItemDisplay(string itemDisplay)
        {
            ProcessGetItemDisplay getDisplay = new ProcessGetItemDisplay();
            getDisplay.Item = new FLContentSiteAssist.Common.Item { Display = itemDisplay };
            getDisplay.Invoke();

            if (getDisplay.ResultSet.HasRows(0))
                return ReplaceDomainNames(getDisplay.ResultSet).Tables[0];
            return null;
        }

        public static DataTable LoadItemByUrl(string itemType, string url)
        {
            ProcessGetItemByURL getItemByUrl = new ProcessGetItemByURL();
            getItemByUrl.Item = new FLContentSiteAssist.Common.Item { ItemType = itemType, ItemURL = url };
            getItemByUrl.Invoke();

            if (getItemByUrl.ResultSet.HasRows(0))
                return ReplaceDomainNames(getItemByUrl.ResultSet).Tables[0];
            return null;
        }

        public static DataTable LoadItemDisplay(string itemDisplay, int parentID)
        {
            ProcessGetItemDisplay getItemDisplay = new ProcessGetItemDisplay();
            getItemDisplay.Item = new FLContentSiteAssist.Common.Item { Display = itemDisplay, ParentItemID = parentID };
            getItemDisplay.Invoke();

            if (getItemDisplay.ResultSet.HasRows(0))
                return ReplaceDomainNames(getItemDisplay.ResultSet).Tables[0];
            return null;
        }

        public static DataTable LoadItemDisplay(string itemDisplay, string parentType, string parentUrl, out DataTable parentItem)
        {
            ProcessGetItemByURL getItemByUrl = new ProcessGetItemByURL();
            getItemByUrl.Item = new FLContentSiteAssist.Common.Item { ItemType = parentType, ItemURL = parentUrl };
            getItemByUrl.Invoke();

            parentItem = null;

            if (!getItemByUrl.ResultSet.HasRows(0)) return null;
            parentItem = getItemByUrl.ResultSet.Tables[0];

            int parentID = getItemByUrl.ResultSet.Tables[0].GetDataTableValue<int>(0, "intItemID", 0);

            ProcessGetItemDisplay getItemDisplay = new ProcessGetItemDisplay();
            getItemDisplay.Item = new FLContentSiteAssist.Common.Item { Display = itemDisplay, ParentItemID = parentID };
            getItemDisplay.Invoke();

            if (getItemDisplay.ResultSet.HasRows(0))
                return ReplaceDomainNames(getItemDisplay.ResultSet).Tables[0];
            return null;
        }

        private static DataSet ReplaceDomainNames(DataSet ds)
        {
            DataTable dt = ds.Tables[0];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i].BeginEdit();
                dt.Rows[i]["chrText11"] = ReplaceDomainName(dt.Rows[i]["chrText11"].ToString());
                dt.Rows[i]["chrText12"] = ReplaceDomainName(dt.Rows[i]["chrText12"].ToString());
                dt.Rows[i].EndEdit();
            }
            ds.AcceptChanges();
            return ds;

        }

        private static string ReplaceDomainName(string input)
        {
            return input.Replace("##DOMAIN##/", FluentContent.Settings.WebsiteRoot);
        }
    }
}
