﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CMS.Helpers.Entities;

namespace CMS.Helpers.Interfaces
{
    public interface ICMSMaster
    {
        void SetSubMenuDataSource(IList<SubMenuItem> dataSource, string header);
    }
}
