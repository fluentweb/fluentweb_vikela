﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS.Helpers.Entities
{
    public class SubMenuItem
    {
        public string Link { get; set; }
        public string Description { get; set; }
        public string CssClass { get; set; }

        public SubMenuItem(string link, string description, string cssClass)
        {
            Link = link;
            Description = description;
            CssClass = cssClass;
        }

        public SubMenuItem(string link, string description) : this(link, description, String.Empty) { }
    }
}
