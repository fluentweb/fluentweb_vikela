﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FLContentSiteAssist.BusinessLogic;
using Common.Extensions;
using FluentContent.DAC;
using FluentContent.Entities;

namespace CMS.Helpers
{
    public class MenuHelper
    {
        public static string GetMenuName(int locationID)
        {
            List<MenuLocation> items = DAC_MenuLocation.Get();

            MenuLocation name = items.Find(i => i.ID == locationID);

            if (name == null) return String.Empty;
            return name.Location;
        }
    }
}
