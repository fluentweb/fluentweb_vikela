﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FLContentSiteAssist.BusinessLogic;
using Common.Extensions;

namespace CMS.Helpers
{
    public class DisplayHelper
    {
        public static string GetDisplayName(int displayID)
        {
            ProcessGetItemDisplaySettings settings = new ProcessGetItemDisplaySettings();
            settings.Item = new FLContentSiteAssist.Common.Item { ItemDisplayID = displayID };
            settings.Invoke();

            if (settings.ResultSet.HasRows(0))
                return settings.ResultSet.Tables[0].GetDataTableValue(0, "chrItemDisplay", String.Empty);
            return String.Empty;
        }
    }
}
