﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common.Extensions;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using Common.FileUtils.Images;

namespace CMS
{
    public class LoadImage : IHttpHandler
    {
        public int Width { get { return HttpContext.Current.Request.GetQueryString<int>("w"); } }
        public int Height { get { return HttpContext.Current.Request.GetQueryString<int>("h"); } }
        public string ImageFileName { get { return HttpContext.Current.Request.GetQueryString<string>("img", String.Empty); } }

        private string _imageSavePath = ConfigurationManager.AppSettings["ParkingImagesFolder"];

        public void ProcessRequest(HttpContext context)
        {
            if (Width > 1000 || Height > 1000) throw new HttpException("Width or height are out of bounds");
            if (ImageFileName == String.Empty) NotFound(context.Response);

            string imagePath = Path.Combine(_imageSavePath, ImageFileName);

            // check if file exists
            if (File.Exists(imagePath))
            {
                // check if browser is caching image
                DateTime dtImageLastModified = File.GetLastWriteTime(imagePath);
                if (IsCached(context.Request, dtImageLastModified))
                {
                    context.Response.StatusCode = 304;
                    context.Response.StatusDescription = "Not Modified";
                    context.Response.End();
                }

                // create Image from file
                using (Image i = Image.FromFile(imagePath))
                {
                    int resizeWidth = Width == 0 ? i.Width : Width;
                    int resizeHeight = Height == 0 ? i.Height : Height;

                    // resize original image
                    using (Image temp = ImageManager.ResizeFixed(i, resizeWidth, resizeHeight))
                    {
                        // push resized image to response with original image RawFromat
                        context.Response.ContentType = ImageManager.GetImageContentType(i);

                        // set cache 
                        context.Response.Cache.SetCacheability(HttpCacheability.Public);
                        context.Response.Cache.SetLastModified(dtImageLastModified);

                        // set image name and push to OutputStream
                        context.Response.AddHeader("Content-Disposition", String.Format("filename=\"{0}\"", ImageFileName));

                        // for png format image needs to be saved to a seekable stream. Using an intermediate MemoryStream does the trick
                        using (MemoryStream ms = new MemoryStream())
                        {
                            temp.Save(ms, i.RawFormat);
                            ms.WriteTo(context.Response.OutputStream);
                        }               
                    }
                }
            }
            else
            {
                // return not found
                NotFound(context.Response);
            }
        }

        private bool IsCached(HttpRequest request, DateTime imageLastModifiedDate)
        {
            string ifModifiedSince = request.Headers["If-Modified-Since"];

            if (!String.IsNullOrEmpty(ifModifiedSince))
            {
                DateTime dtIfModifiedSince;
                if (DateTime.TryParse(ifModifiedSince, out dtIfModifiedSince))
                {
                    return imageLastModifiedDate.ToString("dd MMM yyyy HH:mm:ss") == dtIfModifiedSince.ToString("dd MMM yyyy HH:mm:ss");
                }
            }

            return false;
        }

        private void NotFound(HttpResponse response)
        {
            response.Clear();
            response.StatusCode = 404;
            response.StatusDescription = "Not Found";
            response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
