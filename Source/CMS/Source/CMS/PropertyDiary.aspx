﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CmsMaster.Master" AutoEventWireup="true" CodeBehind="PropertyDiary.aspx.cs" Inherits="CMS.PropertyDiary" %>



<asp:Content ID="Content4" ContentPlaceHolderID="placeholderHeadCSS" runat="server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="placeholderHeadJS" runat="server">
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="placeholderHead" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="placeholderBody" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Create Diary</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="~/Home.aspx" runat="server">Home</a>
                </li>
                <li>
                    <strong>Create Diary</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content padding" style="display: block;">

                        <div class="form-horizontal">
                            <div class="form-group">
                                <asp:Label ID="Label_errMsg" runat="server"></asp:Label>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Notes</label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="TextBox_chrNotes" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator_chrNotes" runat="server" ControlToValidate="TextBox_chrNotes" Display="Dynamic" ErrorMessage="Please enter your notes" SetFocusOnError="True" ValidationGroup="Validation" ForeColor="Red"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <br />
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <asp:Button ID="button_Send" runat="server" OnClick="Button_Send_Click" Text="Create" CssClass="btn btn-primary" type="submit" ValidationGroup="Validation" />
                                    &nbsp;
                                   <asp:Button ID="button_Cancel" runat="server" OnClick="Button_Cancel_Click" Text="Cancel" CssClass="btn btn-default" type="submit" />
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="placeholderFooter" runat="server">
</asp:Content>
