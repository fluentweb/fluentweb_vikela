﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CmsMaster.Master" AutoEventWireup="true"
    CodeBehind="FormManagement.aspx.cs" Inherits="CMS.FormManagement" %>

<asp:Content ID="Content4" ContentPlaceHolderID="placeholderHeadCSS" runat="server">
    <!-- Data Tables -->
    <link href="css_inispinia/custom.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="css_inispinia/plugins/dataTables/dataTables.bootstrap.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="css_inispinia/plugins/dataTables/dataTables.responsive.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="css_inispinia/plugins/dataTables/dataTables.tableTools.min.css" runat="server" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="placeholderHeadJS" runat="server">
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/jeditable/jquery.jeditable.js")%>" type="text/javascript"></script>
    <!-- Data Tables -->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/dataTables/jquery.dataTables.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/dataTables/dataTables.bootstrap.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/dataTables/dataTables.responsive.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/dataTables/dataTables.tableTools.min.js")%>" type="text/javascript"></script>
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="placeholderHead" runat="server">

    <script src="<%=ResolveUrl("~/scripts/sortable/jquery-sortable.js")%>" type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {

            // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // save the latest tab; use cookies if you like 'em better:
                localStorage.setItem('lastTab', $(this).attr('href'));
            });

            // go to the latest tab, if it exists:
            var lastTab = localStorage.getItem('lastTab');
            if (lastTab) {
                $('[href="' + lastTab + '"]').tab('show');
            }

            // sortable
            var group = $('ol.form-layout-list').sortable({
                group: 'vertical',
                pullPlaceholder: false,
                handle: 'i.icon-move',
                onDragStart: function (item, container, _super) {
                    console.log('dragStart');
                    _super(item, container);
                },
                onDrop: function (item, container, _super) {
                    serializeForm();
                    _super(item, container);
                },
                serialize: function (parent, children, isContainer) {

                    var result = $.extend({}, {
                        id: parent.attr('id'),
                        type: { id: parent.attr('type-id') },
                        text_1: parent.children('.formWrapper').children('p').children('input[data-selector="text_1"]').val(),
                        text_2: parent.children('.formWrapper').children('p').children('input[data-selector="text_2"]').val(),
                        text_3: parent.children('.formWrapper').children('p').children('input[data-selector="text_3"]').val(),
                        data_1: parent.children('.formWrapper').children('p').children('input[data-selector="data_1"]').val()
                    });

                    if (isContainer) {
                        return children;
                    }
                    else if (children[0]) {
                        result.children = children;
                    }

                    return result;
                }
                ,
                tolerance: 5,
                distance: 10
            });

            serializeForm();

            function serializeForm() {
                var val = group.sortable("serialize").get();
                var json = JSON.stringify(val, null, '   ');
                // $('.output').text(json);
                $('.hdnFormData').val(json);
            }

            $('a.add-link').click(function () {
                var templateTypeID = $(this).attr('type-id');
                var templateTypeName = $(this).attr('type-name');
                var listItemToAdd;

                $.ajax({
                    url: "Handlers/CommonHandler.ashx?action=form_object_type_template",
                    data: 'type-id=' + templateTypeID,
                    complete: function (xhr, status) {
                        console.log(status);
                        listItemToAdd = xhr.responseText;
                        $('ol.form-layout-list').append(listItemToAdd);
                        serializeForm();

                        $('html, body').animate({
                            scrollTop: $('ol.form-layout-list li:last').offset().top - 100
                        }, 500);
                    }
                });

                return false;
            });

            $('body').on('click', '.template-delete', function () {
                var result = confirm('Are you sure?');
                if (result) {
                    var idToDelete = $(this).parent().attr('id');
                    if (idToDelete > 0) {
                        var currentDeleteItems = $('.hdnDeleteItems').val();
                        if (currentDeleteItems.length > 0)
                            currentDeleteItems = currentDeleteItems + ',' + idToDelete;
                        else
                            currentDeleteItems = idToDelete;
                        $('.hdnDeleteItems').val(currentDeleteItems);
                    }
                    console.log($('.hdnDeleteItems').val());
                    $(this).parent().remove();

                    // reserialize form
                    serializeForm();
                    return false;
                } else {
                    return false;
                }
            });

            $('.save-button').on('click', function () {
                serializeForm();
            });
        });
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="placeholderBody" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Forms</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="~/Home.aspx" runat="server">Home</a>
                </li>
                <li>
                    <strong>Form</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <asp:MultiView ID="mvItems" runat="server" ActiveViewIndex="0">
            <asp:View ID="viewListing" runat="server">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5></h5>
                                <div class="ibox-tools">
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="table-responsive">
                                    <asp:ListView ID="lvItems" runat="server">
                                        <LayoutTemplate>
                                            <table class="table table-striped table-bordered table-hover dataTables-example">
                                                <thead>
                                                    <tr>
                                                        <th>Name
                                                        </th>
                                                        <th>Description
                                                        </th>
                                                        <th>Type
                                                        </th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                                <tfoot>
                                                    <tr>
                                                        <th>Name
                                                        </th>
                                                        <th>Description
                                                        </th>
                                                        <th>Type
                                                        </th>
                                                        <th></th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <%#Eval("Name") %>
                                                </td>
                                                <td>
                                                    <%#Eval("Description") %>
                                                </td>
                                                <td>
                                                    <%#Eval("Type.Name") %>
                                                </td>
                                                <td>
                                                    <a runat="server" class="btn btn-info" href='<%#String.Format("~/FormManagement.aspx?formid={0}&typeid={1}", Eval("ID"), TypeID) %>'><i class="fa fa-paste"></i>&nbsp;Edit</a> <a runat="server" class="btn btn-warning" onclick="return confirm('Are you sure you want to duplicate?');" href='<%#String.Format("~/FormManagement.aspx?formid={0}&action=duplicate", Eval("ID")) %>'><i class="fa fa-warning"></i>&nbsp;Duplicate </a>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <EmptyDataTemplate>
                                            <p>
                                                There are currently no forms of this type
                                            </p>
                                        </EmptyDataTemplate>
                                    </asp:ListView>
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <div class="col-sm-4">
                                                <asp:Button runat="server" ID="btnCreate" CssClass="btn btn-primary" type="submit" OnClick="btnCreate_Click" Text="Create"
                                                    ValidationGroup="FormGroup" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:View>
            <asp:View ID="viewDetail" runat="server">
                <div class="errorSummary hidden" validationgroup="FormGroup">
                    <p>
                        Please correct the highlighted fields
                    </p>
                </div>
                <asp:HiddenField ID="hdnSelectedTab" runat="server" />
                <div class="row">
                    <div class="col-lg-12">
                        <div class="tabs-container">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#tabs-1">Form</a></li>
                                <li class=""><a data-toggle="tab" href="#tabs-2">Design</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="tabs-1" class="tab-pane active">
                                    <div class="panel-body">
                                        <div class="form-horizontal">
                                            <asp:ValidationSummary ID="vsErrorSummary" runat="server" ValidationGroup="FormGroupServer"
                                                CssClass="errorSummary" />
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Title</label>
                                                <div class="col-sm-10">
                                                    <asp:TextBox ID="txtFormTitle" runat="server" CssClass="form-control" MaxLength="255"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfvFormTitle" runat="server" Display="None" ValidationGroup="FormGroup"
                                                        ControlToValidate="txtFormTitle"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Description</label>

                                                <div class="col-sm-10">
                                                    <asp:TextBox ID="txtFormDescription" CssClass="form-control" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfvFormDescription" runat="server" Display="None"
                                                        ValidationGroup="FormGroup" ControlToValidate="txtFormDescription"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <div class="col-sm-4 col-sm-offset-2">
                                                    <asp:Button runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary" type="submit"
                                                        Text="Save" ValidationGroup="FormGroup" />
                                                    <asp:Button runat="server" ID="btnDelete" OnClick="btnDelete_Click" Text="Delete" CssClass="btn btn-danger" type="submit"
                                                        OnClientClick="return confirm('Are you sure? This will delete all form related templates and data');"
                                                        ValidationGroup="FormGroup" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="tabs-2" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <div class="col-sm-4">
                                                    <asp:Button runat="server" ID="btnSaveSecondaryTop" CssClass="save-button btn btn-primary" type="submit" OnClick="btnSave_Click"
                                                        Text="Save" ValidationGroup="FormGroup" />
                                                </div>
                                            </div>
                                        </div>
                                        <div id="form-builder">
                                            <div class="row animated fadeInDown">
                                                <div class="col-lg-3">
                                                    <div class="ibox float-e-margins">
                                                        <div class="ibox-title">
                                                            <h5>Form Controls</h5>
                                                            <div class="ibox-tools">
                                                                <a class="collapse-link">
                                                                    <i class="fa fa-chevron-up"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="ibox-content" style="display: block;">
                                                            <div class="form-controls" id="form-controls-div">
                                                                <ol class="form-controls-list vertical">
                                                                    <asp:ListView ID="lvFormControls" runat="server">
                                                                        <LayoutTemplate>
                                                                            <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                                                        </LayoutTemplate>
                                                                        <ItemTemplate>
                                                                            <li>
                                                                                <%#Eval("Name") %>
                                                                                <a href="#" class="add-link" type-name='<%#Eval("Name")%>' type-id='<%#Eval("ID")%>'>add &raquo;</a>
                                                                            </li>
                                                                        </ItemTemplate>
                                                                    </asp:ListView>
                                                                </ol>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-9">
                                                    <div class="ibox float-e-margins">
                                                        <div class="ibox-title">
                                                            <h5>Form Layout</h5>
                                                            <div class="ibox-tools">
                                                                <a class="collapse-link">
                                                                    <i class="fa fa-chevron-up"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="ibox-content" style="display: block;">
                                                            <div class="form-layout">

                                                                <ol class="form-layout-list vertical">
                                                                    <%=FormString %>
                                                                </ol>
                                                            </div>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <div class="col-sm-4">
                                                    <asp:Button runat="server" ID="btnSaveSecondaryBottom" CssClass="save-button btn btn-primary" type="submit" OnClick="btnSave_Click"
                                                        Text="Save" ValidationGroup="FormGroup" />
                                                    <a href="#form-controls-div" class="btn btn-info" type="submit">To Form Objects</a>
                                                    <input type="hidden" runat="server" class="hdnFormData" id="hdnFormData" />
                                                    <input type="hidden" runat="server" class="hdnDeleteItems" id="hdnDeleteItems" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </asp:View>
        </asp:MultiView>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="placeholderFooter" runat="server">
    <script src="<%=ResolveUrl("~/js_inispinia/dataTableCustom.js")%>" type="text/javascript"></script>
</asp:Content>
