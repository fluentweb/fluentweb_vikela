﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CmsMaster.Master" AutoEventWireup="True"
    CodeBehind="EntityManagement.aspx.cs" Inherits="CMS.EntityManagement" %>

<asp:Content ID="Content3" ContentPlaceHolderID="placeholderHeadCSS" runat="server">
    <!-- Data Tables -->
    <link href="css_inispinia/plugins/dataTables/dataTables.bootstrap.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="css_inispinia/plugins/dataTables/dataTables.responsive.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="css_inispinia/plugins/dataTables/dataTables.tableTools.min.css" runat="server" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="placeholderHeadJS" runat="server">
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/jeditable/jquery.jeditable.js")%>" type="text/javascript"></script>
    <!-- Data Tables -->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/dataTables/jquery.dataTables.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/dataTables/dataTables.bootstrap.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/dataTables/dataTables.responsive.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/dataTables/dataTables.tableTools.min.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="placeholderHead" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="placeholderBody" runat="server">
    <div id="fillerLine" runat="server" class="updateMessage" style="height: 20px; width: 100%;">
        <% 
            switch (Request.GetQueryString<string>("info", String.Empty))
            {
                case "deleted":
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Item successfully deleted','success');", true);

                    break;
                case "updated":
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Item successfully updated','success');", true);

                    break;
                case "added":
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Item successfully added','success');", true);

                    break;
                default:
                    break;
            }
        %>
        <asp:Label ID="lblErrorDimensions" runat="server" Visible="false" Text="Invalid image dimensions, please check requirements next to image" />
    </div>
    <asp:MultiView ID="mvItems" runat="server" ActiveViewIndex="0">
        <asp:View ID="viewListing" runat="server">
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Entity Management</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="~/Home.aspx" runat="server">Home</a>
                        </li>
                        <li class="active">
                            <strong>
                                <asp:Literal ID="litHeader" runat="server">Entity</asp:Literal>
                                Management
                            </strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Entity List</h5>
                                <div class="ibox-tools">
                                </div>
                            </div>
                            <div class="ibox-content padding" style="display: block;">
                                <div class="errorSummary hidden" validationgroup="EntityGroupCreate">
                                    <center>
                                        <p>
                                            Please correct the highlighted fields
                                        </p>
                                    </center>
                                </div>
                                <div class="form-horizontal">
                                    <asp:ValidationSummary ID="vsErrorSummaryCreate" runat="server" ValidationGroup="EntityGroupCreateServer"
                                        CssClass="errorSummary" />
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Entity Name
                                        </label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtEntityNameCreate" CssClass="form-control" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvEntityNameCreate" runat="server" ControlToValidate="txtEntityNameCreate"
                                                ValidationGroup="EntityGroupCreate" Display="None"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-2">
                                            <asp:Button ID="btnSaveCreate" runat="server" CssClass="btn btn-primary" type="submit" Text="Save" ValidationGroup="EntityGroupCreate"
                                                OnClick="btnSaveCreate_Click" />
                                        </div>
                                    </div>
                                </div>
                                <asp:ListView ID="lvItems" runat="server">
                                    <LayoutTemplate>
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover dataTables-example">
                                                <thead>
                                                    <tr>
                                                        <th>Description
                                                        </th>
                                                        <th>Type
                                                        </th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                                <tfoot>
                                                    <tr>
                                                        <th>Description
                                                        </th>
                                                        <th>Type
                                                        </th>
                                                        <th></th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <%#Eval("Name") %>
                                            </td>
                                            <td>
                                                <%#Eval("Type") %>
                                            </td>
                                            <td>
                                                <a runat="server" class="btn btn-info" type="submit" href='<%#String.Format("~/EntityManagement.aspx?enitityid={0}&typeid={1}", Eval("ID"), Eval("TypeID")) %>'><i class="fa fa-paste"></i>&nbsp;Edit</a>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:View>
        <asp:View ID="viewDetail" runat="server">
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Entity Management</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="~/Home.aspx" runat="server">Home</a>
                        </li>
                        <li><a href='<%=ResolveUrl(String.Format("~/EntityManagement.aspx?typeid={0}", EntityTypeID)) %>'>
                            <asp:Literal ID="litEntityName" runat="server">Entity</asp:Literal>
                            list</a></li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Edit Entity</h5>
                                <div class="ibox-tools">
                                </div>
                            </div>
                            <div class="ibox-content padding" style="display: block;">
                                <div class="form-horizontal">
                                    <div class="errorSummary hidden" validationgroup="EntityGroup">
                                        <center>
                                            <p>
                                                Please correct the highlighted fields
                                            </p>
                                        </center>
                                    </div>
                                    <asp:ValidationSummary ID="vsErrorSummary" runat="server" ValidationGroup="EntityGroupServer"
                                        CssClass="errorSummary" />

                                    <div id="divTitle" runat="server" class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Entity Name
                                        </label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtEntityName" CssClass="form-control" runat="server" MaxLength="255" />
                                            <asp:RequiredFieldValidator ID="rfvEntityName" runat="server" Display="None" ValidationGroup="EntityGroup"
                                                ControlToValidate="txtEntityName"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group" runat="server" id="divEnabled">
                                        <label class="col-sm-2 control-label">
                                            Enabled
                                        </label>
                                        <div class="col-sm-10">
                                            <asp:CheckBox runat="server" ID="chkEntityEnabled" CssClass="input_enabled" />
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group" id="divDates" runat="server">
                                        <label class="col-sm-2 control-label">
                                            Expiry Date
                                        </label>

                                        <div class="col-sm-4">
                                            <div id="data_1" class="form-group">
                                                <div class="input-group date">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    <asp:TextBox ID="txtExpiryDate" CssClass="form-control" runat="server" />

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <asp:Literal
                                                runat="server" ID="labelTimeFrom" />
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div id="divImage1" runat="server">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Entity Logo Image</label>
                                            <div class="col-sm-4">
                                                <asp:FileUpload ID="ImageUpload" Width="100%" CssClass="btn btn-outline btn-success" type="file" runat="server" />
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:HiddenField ID="hdnImage1Dims" runat="server" />
                                                <asp:Label ID="image_instr" CssClass="help-block m-b-none" runat="server" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">
                                            </label>
                                            <div class="col-sm-10">
                                                <asp:CheckBox runat="server" ID="overwriteImg" typ="checkbox" Checked="false" />
                                                &nbsp;<small class="text-navy">Overwrite existing image</small>
                                                <br />
                                                <br />
                                                <asp:Image runat="server" ID="image" CssClass="image" Width="100%" BorderStyle="None" Height="100px" Visible="false" />
                                                <br />
                                                <asp:CheckBox runat="server" ID="chkDeleteImage" CssClass="input_enabled" Checked="false"
                                                    Visible="false" />&nbsp;<small class="text-danger"><asp:Label ID="deleteLabel" runat="server" Text="Remove this image" Visible="false" /></small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-2">

                                            <asp:Button ID="btnSave" runat="server" Text="Save" ValidationGroup="EntityGroup" CssClass="btn btn-primary" type="submit"
                                                OnClick="btnSave_Click" />
                                            <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-danger" type="submit" OnClientClick="return confirm('Are you sure? This will also delete departments and users attached to it');"
                                                OnClick="btnDelete_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:View>
    </asp:MultiView>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="placeholderFooter" runat="server">
    <script src="<%=ResolveUrl("~/js_inispinia/dataTableCustom.js")%>" type="text/javascript"></script>
    <!-- Chosen -->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/chosen/chosen.jquery.js")%>" type="text/javascript"></script>
    <!-- JSKnob -->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/jsKnob/jquery.knob.js")%>" type="text/javascript"></script>
    <!-- Input Mask-->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/jasny/jasny-bootstrap.min.js")%>" type="text/javascript"></script>
    <!-- Data picker -->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/datapicker/bootstrap-datepicker.js")%>" type="text/javascript"></script>
    <!-- Clock picker -->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/clockpicker/clockpicker.js")%>" type="text/javascript"></script>
    <!-- Date range use moment.js same as full calendar plugin -->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/fullcalendar/moment.min.js")%>" type="text/javascript"></script>
    <!-- Date range picker -->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/daterangepicker/daterangepicker.js")%>" type="text/javascript"></script>
    <!-- Select2 -->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/select2/select2.full.min.js")%>" type="text/javascript"></script>
    <!-- custom-plugin -->
    <script src="<%=ResolveUrl("~/js_inispinia/custom-plugin.js")%>" type="text/javascript"></script>

</asp:Content>
