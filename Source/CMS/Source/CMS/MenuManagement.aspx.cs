﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.Helpers;
using Common.Extensions;
using CMS.Helpers.Bases;
using FluentContent.Entities.Enums;


namespace CMS
{
    public partial class MenuManagement : BasePage
    {
        public int MenuID { get { return Request.GetQueryString("location", 0); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            // check user rights
            if (UserSession.Role.CMSAccessLevel != CMSAccessLevel.Full) Response.Redirect("~/Home.aspx");

            if (!IsPostBack)
                LoadHeader();
        }

        private void LoadHeader()
        {
            litMenuManagementDescription.Text = MenuHelper.GetMenuName(MenuID);
        }
    }
}
