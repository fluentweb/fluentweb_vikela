﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CmsMaster.Master" AutoEventWireup="true" CodeBehind="PropertyRegisteredUsersManagement.aspx.cs" Inherits="CMS.PropertyRegisteredUsersManagement" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content4" ContentPlaceHolderID="placeholderHeadCSS" runat="server">
    <!-- Data Tables -->
    <link id="Link1" href="/css_inispinia/plugins/dataTables/dataTables.bootstrap.css" runat="server" rel="stylesheet" type="text/css" />
    <link id="Link2" href="/css_inispinia/plugins/dataTables/dataTables.responsive.css" runat="server" rel="stylesheet" type="text/css" />
    <link id="Link3" href="/css_inispinia/plugins/dataTables/dataTables.tableTools.min.css" runat="server" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="placeholderHeadJS" runat="server">
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/jeditable/jquery.jeditable.js")%>" type="text/javascript"></script>
    <!-- Data Tables -->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/dataTables/jquery.dataTables.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/dataTables/dataTables.bootstrap.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/dataTables/dataTables.responsive.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/dataTables/dataTables.tableTools.min.js")%>" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="placeholderHead" runat="server">
    <%--    <script>
        $(function () {
            //$('.itemTable tr').hover(function () {
            //    $(this).addClass('activeRow');
            //}, function () {
            //    $(this).removeClass('activeRow');
            //});


            //var email = document.getElementById('ctl00_placeholderBody_rfvEmail');
            //var emailReg = document.getElementById('ctl00_placeholderBody_revEmail');

            //var requireEmails = $('.chkReceiveEmails').is(':checked');
            //if (requireEmails) {
            //    ValidatorEnable(email, true);
            //    ValidatorEnable(emailReg, true);
            //} else {
            //    ValidatorEnable(email, false);
            //    ValidatorEnable(emailReg, false);
            //}

            //$('.chkReceiveEmails').click(function () {

            //    if ($(this).is(':checked')) {
            //        ValidatorEnable(email, true);
            //        ValidatorEnable(emailReg, true);
            //    } else {
            //        ValidatorEnable(email, false);
            //        ValidatorEnable(emailReg, false);
            //    }

            //});

        });
    </script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="placeholderBody" runat="server">
    <asp:MultiView ActiveViewIndex="0" runat="server" ID="mvRegisteredUsers">
        <asp:View runat="server" ID="viewUserList">
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Subscribers</h2>
                    <ol class="breadcrumb">
                        <li><a id="A1" href="~/property-registered-users/new" runat="server">New user</a></li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5></h5>
                                <div class="ibox-tools">

                                    <span class="label label-success  pull-right">
                                        <asp:Literal ID="litItemCount" runat="server" /></span>
                                </div>
                            </div>

                            <div class="ibox-content padding" style="display: block;">
                                <%
                                    switch (Message)
                                    {
                                        case "created":
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('User successfully added!','success');", true);
                                            break;
                                        case "updated":
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('User successfully updated!','success');", true);

                                            break;
                                        case "nouser":
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('User not found!','warning');", true);

                                            break;
                                        case "deleted":
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('User successfully deleted!','success');", true);

                                            break;
                                        default:
                                            break;
                                    }

                                    if (Alerted)
                                    {
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Property email alert sent!','success');", true);
                                    }
                                %>
                                <div class="pageNumber" id="divTopPager" runat="server">
                                    <div class="numberWrapper">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div id="editable_paginate" class="dataTables_paginate paging_simple_numbers">
                                                    <ul class="pagination">
                                                        <asp:Literal ID="litTopPrev" runat="server"></asp:Literal>
                                                        <asp:ListView runat="server" ID="lvPagerTop">
                                                            <LayoutTemplate>
                                                                <asp:PlaceHolder runat="server" ID="itemPlaceholder" />
                                                            </LayoutTemplate>
                                                            <ItemTemplate>
                                                                <li class="<%#GetPagerActiveClass(Container.DataItem, "paginate_button active")%>"><a href="<%#GetPageLink(Container.DataItem) %>" aria-controls="editable" data-dt-idx="<%#Container.DataItem %>" tabindex="0"><%#Container.DataItem %></a></li>
                                                            </ItemTemplate>
                                                        </asp:ListView>
                                                        <asp:Literal ID="litTopNext" runat="server"></asp:Literal>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTables-propertySortnoclint">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <asp:HyperLink ID="hlDate" runat="server">Date</asp:HyperLink><asp:Image ID="imgDate" runat="server" />
                                                </th>
                                                <th>
                                                    <asp:HyperLink ID="hlName" runat="server">Name</asp:HyperLink><asp:Image ID="imgName" runat="server" />
                                                </th>
                                                <th>
                                                    <asp:HyperLink ID="hlBedrooms" runat="server">Bedrooms</asp:HyperLink><asp:Image ID="imgBedrooms" runat="server" />
                                                </th>
                                                <th>
                                                    <asp:HyperLink ID="hlMinPrice" runat="server">Min. Price</asp:HyperLink><asp:Image ID="imgMinPrice" runat="server" />
                                                </th>
                                                <th>
                                                    <asp:HyperLink ID="hlMaxPrice" runat="server">Max. Price</asp:HyperLink><asp:Image ID="imgMaxPrice" runat="server" />
                                                </th>
                                                <th>
                                                    <asp:HyperLink ID="hlEmail" runat="server">Email</asp:HyperLink><asp:Image ID="imgEmail" runat="server" />
                                                </th>
                                                <th>
                                                    <asp:HyperLink ID="hlSearchType" runat="server">Search Type</asp:HyperLink><asp:Image ID="imgSearchType" runat="server" />
                                                </th>
                                                <th>
                                                    <asp:HyperLink ID="hlEmailAlerts" runat="server">Email Alerts</asp:HyperLink><asp:Image ID="imgEmailAlerts" runat="server" />
                                                </th>
                                                <th>
                                                    <asp:HyperLink ID="hlCreated" runat="server">Added</asp:HyperLink><asp:Image ID="imgCreated" runat="server" />
                                                </th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <asp:ListView runat="server" ID="lvUsers">
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%#Eval("DateSubscribed", "{0:dd/MM/yyyy}") %>
                                                    </td>
                                                    <td><strong><%# String.Format("{0} {1}", Eval("Surname") != null  && !String.IsNullOrEmpty(Eval("Surname").ToString()) ?  Eval("Surname", "{0},") : String.Empty, Eval("FirstName")) %></strong>
                                                    </td>
                                                    <td><%#Eval("MinBedrooms") %>
                                                    </td>
                                                    <td><%#Eval("MinPrice", "{0:C}") %>
                                                    </td>
                                                    <td><%#Eval("MaxPrice", "{0:C}") %>
                                                    </td>
                                                    <td>
                                                        <%#Eval("EmailAddress") %>
                                                    </td>
                                                    <td><%#(bool)Eval("WantsToRent") ? "Rent" : String.Empty %>
                                                        <%# (bool)Eval("WantsToRent") ? ((bool)Eval("WantsToBuy") ? "/Buy" : String.Empty) : ((bool)Eval("WantsToBuy") ? "Buy" :String.Empty) %>
                                                    </td>
                                                    <td><%#(bool)Eval("EmailAlerts") ? "Yes": "No" %>
                                                    </td>
                                                    <td><%#Eval("DateSubscribed", "{0:dd/MM/yyyy}") %></td>
                                                    <td>
                                                        <a id="A2" class="btn btn-info" href='<%#Eval("ID","~/property-registered-users/{0}") %>' runat="server"><i class="fa fa-paste"></i>&nbsp;Edit</a>
                                                        <a id="A3" runat="server" class="btn btn-danger" onclick="return confirm('Are you sure?');" href='<%#Eval("ID","~/property-registered-users/delete/{0}") %>'><i class="fa fa-trash"></i>&nbsp;Delete</a></td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:ListView>
                                        <tfoot>
                                            <tr>
                                                <th>
                                                    Date
                                                </th>
                                                <th>
                                                   Name
                                                </th>
                                                <th>
                                                  Bedrooms
                                                </th>
                                                <th>
                                                 Min. Price
                                                </th>
                                                <th>
                                                   Max. Price
                                                </th>
                                                <th>
                                                Email
                                                </th>
                                                <th>
                                                   Search Type
                                                </th>
                                                <th>
                                                  Email Alerts
                                                </th>
                                                <th>
                                                   Added
                                                </th>
                                                <th></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <div class="pageNumber" id="divBottomPager" runat="server">
                                    <div class="numberWrapper">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div id="editable_paginate" class="dataTables_paginate paging_simple_numbers">
                                                    <ul class="pagination">
                                                        <asp:Literal ID="litBottomPrev" runat="server"></asp:Literal>
                                                        <asp:ListView runat="server" ID="lvPagerBottom">
                                                            <LayoutTemplate>
                                                                <asp:PlaceHolder runat="server" ID="itemPlaceholder" />
                                                            </LayoutTemplate>
                                                            <ItemTemplate>
                                                                <li class="<%#GetPagerActiveClass(Container.DataItem, "paginate_button active")%>"><a href="<%#GetPageLink(Container.DataItem) %>" aria-controls="editable" data-dt-idx="<%#Container.DataItem %>" tabindex="0"><%#Container.DataItem %></a></li>
                                                            </ItemTemplate>
                                                        </asp:ListView>
                                                        <asp:Literal ID="litBottomNext" runat="server"></asp:Literal>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:View>
        <asp:View runat="server" ID="viewAddEditUser">
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2><%=UserID > 0 ? "Edit User"  : "New User" %></h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="~/Home.aspx" runat="server">Home</a>
                        </li>
                        <li>
                            <a href="~/property-registered-users" runat="server">User List</a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h3>Personal Details</h3>
                                <div class="ibox-tools">
                                </div>
                            </div>
                            <div class="ibox-content" style="display: block;">
                                <div class="form-horizontal">
                                    <asp:ValidationSummary runat="server" ValidationGroup="PropertyRegisteredUserGroupServer" DisplayMode="List" />
                                    <div class="errorSummary hidden" validationgroup="PropertyRegisteredUserGroup">
                                        <center>
                                            Fields with red border are required!
                                        </center>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Title
                                        </label>
                                        <div class="col-sm-10">
                                            <asp:DropDownList runat="server" ID="ddlTitle" CssClass="form-control ue-ddl">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            First name
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="txtFirstName" runat="server" />
                                            <asp:RequiredFieldValidator ErrorMessage="First name is required" ControlToValidate="txtFirstName" runat="server" Display="None" ValidationGroup="PropertyRegisteredUserGroup" />
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Surname
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="txtSurname" runat="server" />
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Address Line 1
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="txtAddress1" runat="server" />
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Address Line 2
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="txtAddress2" runat="server" />
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Address Line 3
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="txtAddress3" runat="server" />
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Postcode
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="txtPostcode" runat="server" />
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Country
                                        </label>
                                        <div class="col-sm-10">
                                            <asp:DropDownList runat="server" ID="ddlCountry" CssClass="form-control ue-ddl" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" AutoPostBack="true" CausesValidation="false">
                                            </asp:DropDownList>

                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            County
                                        </label>
                                        <div class="col-sm-10">
                                            <asp:DropDownList runat="server" ID="ddlCounty" CssClass="form-control ue-ddl">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Mobile
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="txtPhoneMobile" runat="server" />
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Home Telephone
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="txtPhoneHome" runat="server" />
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Work Telephone
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="txtPhoneWork" runat="server" />
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Email
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="txtEmail" runat="server" />
                                            <asp:RequiredFieldValidator ID="rfvEmail" ErrorMessage="Email is required" CssClass="rfvEmail" ControlToValidate="txtEmail" runat="server" Display="None" ValidationGroup="PropertyRegisteredUserGroup" />
                                            <asp:RegularExpressionValidator ID="revEmail" ErrorMessage="Invalid Email" CssClass="revEmail" ControlToValidate="txtEmail" runat="server" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="None" ValidationGroup="PropertyRegisteredUserGroup" />
                                        </div>
                                    </div>
                                    <br />
                                    <div class="ibox-title">
                                        <h3>Property Interest</h3>
                                        <div class="ibox-tools">
                                        </div>
                                    </div>
                                    <br />
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Interested in valuation
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="checkbox" id="chkFreeValuation" runat="server" />
                                        </div>

                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            User interested in
                                        </label>
                                        <div class="col-sm-2">
                                            <input type="checkbox" id="chkInterestedInRenting" runat="server" class="chkInterest" />
                                            Renting
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="checkbox" id="chkInterestedInBuying" runat="server" class="chkInterest" />
                                            Buying
                                        </div>
                                        <div class="col-sm-6">
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Property Type
                                        </label>
                                        <div class="col-sm-10">
                                            <asp:DropDownList runat="server" ID="ddlPropertyType" CssClass="form-control ue-ddl">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Counties:
                                        </label>
                                        <div class="col-sm-10">
                                            <asp:ListView runat="server" ID="lvCounties">
                                                <LayoutTemplate>
                                                    <ul class="list">
                                                        <asp:PlaceHolder runat="server" ID="itemPlaceholder" />
                                                    </ul>
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <li style="list-style: outside none none;">
                                                        <input type="checkbox" id="chkParish" runat="server" value='<%#Eval("ID") %>' class="chkParish" />
                                                        <%#Eval("CountyName") %>           
                                                    </li>
                                                </ItemTemplate>
                                                <EmptyDataTemplate>
                                                    NO COUNTIES FOR THIS COUNTRY
                                                </EmptyDataTemplate>
                                            </asp:ListView>
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Min Bedrooms
                                        </label>
                                        <div class="col-sm-10">
                                            <asp:DropDownList ID="ddlMinBedrooms" runat="server" CssClass="form-control ue-ddl">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Min Bathrooms
                                        </label>
                                        <div class="col-sm-10">
                                            <asp:DropDownList ID="ddlMinBathrooms" runat="server" CssClass="form-control ue-ddl">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Min Price
                                        </label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtMinPrice" runat="server" CssClass="form-control" />

                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Max Price
                                        </label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtMaxPrice" runat="server" CssClass="form-control" />
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Qualification
                                        </label>
                                        <div class="col-sm-10">
                                            <asp:DropDownList ID="ddlQualification" runat="server" CssClass="form-control ue-ddl">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Email Alerts
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="checkbox" runat="server" id="chkReceiveEmails" class="chkReceiveEmails" />
                                        </div>
                                    </div>

                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Outside Area
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="checkbox" runat="server" id="chkOutsideArea" />
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Parking Space
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="checkbox" runat="server" id="chkParkingSpace" />
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Additional Information
                                        </label>
                                        <div class="col-sm-10">
                                            <textarea id="txtAdditionalInformation" runat="server" class="form-control" placeholder="Please add any specific requirements here" rows="5"></textarea>
                                        </div>
                                    </div>

                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-2">
                                            <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" CssClass="btn btn-primary" type="submit" Text="Submit" ValidationGroup="PropertyRegisteredUserGroup" />
                                            <asp:Button runat="server" ID="btnSaveAndAlert" OnClick="btnSaveAndAlert_Click" CssClass="btn btn-info" type="submit" Text="Save & Send Alert Email" ValidationGroup="PropertyRegisteredUserGroup" OnClientClick="return confirm('An email alert including all properties matching the user criteria will be sent to the user. Are you sure?')" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:View>
    </asp:MultiView>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="placeholderFooter" runat="server">
    <script src="<%=ResolveUrl("~/js_inispinia/dataTableCustom.js")%>" type="text/javascript"></script>
</asp:Content>
