﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Extensions;
using FluentContent.Entities;
using FluentContent.DAC;
using CMS.Helpers.Bases;

using System.IO;
using CMS.Helpers;
using FluentContent.Entities.Enums;

namespace CMS
{
    public partial class EntityManagement : BasePage
    {
        public int EntityTypeID { get { return Request.GetQueryString<int>("typeid", 0); } }
        public int EntityID { get { return Request.GetQueryString<int>("enitityid", 0); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            // check user rights
            if (UserSession.Role.CMSAccessLevel != CMSAccessLevel.Full) Response.Redirect("~/Home.aspx");

            if (!IsPostBack)
            {
                if (EntityTypeID != 0)
                {
                    EntityType t = DAC_EntityType.Get(EntityTypeID);
                    if (t == null) return;
                    litHeader.Text = t.Name;
                    litEntityName.Text = t.Name;
                }

                if (EntityID != 0)
                    LoadDetail();
                else
                {
                    LoadListing();
                }
            }
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;

            string _imgurl = null;
            string _rootpath = Request.Url.GetLeftPart(UriPartial.Authority) + HttpRuntime.AppDomainAppVirtualPath;
            if (_rootpath.Substring(_rootpath.Length - 1, 1) != "/") { _rootpath += "/"; }

            //First upload image 1
            if (ImageUpload.PostedFile != null && ImageUpload.PostedFile.FileName != "")
            {
                // validate image
                if (!ValidDimensions(ImageUpload, hdnImage1Dims.Value.Trim()))
                {
                    // show error
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Invalid image dimensions, please check requirements next to image','success');", true);
                    return;
                }

                string pathfilename = ImageUpload.PostedFile.FileName;
                string filename = Path.GetFileName(ImageUpload.PostedFile.FileName);
                string localpath = FluentContentSettings.ImageUploadLocation + "\\";

                string localfilename = FluentContentSettings.ImageUploadLocation + "\\" + filename;
                string filenameleft = Path.GetFileNameWithoutExtension(filename);
                string ext = Path.GetExtension(filename);
                if (overwriteImg.Checked)
                {
                    ImageUpload.PostedFile.SaveAs(localfilename);
                }
                else
                {
                    if (File.Exists(@localfilename))
                    {
                        //Find next available filename
                        for (int i = 1; i <= 1000; i++)
                        {
                            if (!File.Exists(@filenameleft + i.ToString() + ext))
                            {
                                localfilename = localpath + filenameleft + i.ToString() + ext;
                                break;
                            }
                        }
                    }
                    ImageUpload.PostedFile.SaveAs(localfilename);
                }
                // not used as redirectin occurs (if no redirection use proper url
                image.ImageUrl = _rootpath + FluentContentSettings.ImageUploadFolder + "/" + Path.GetFileName(localfilename);
                image.Visible = true;

                _imgurl = Path.GetFileName(localfilename);
            }
            else 
            {
                if (chkDeleteImage.Checked)
                {
                    Entity existingEntity = DAC_Entity.GetByID(EntityID, UserSession.ClientID);
                    if (existingEntity != null)
                    {
                        
                        string localfilename = FluentContentSettings.ImageUploadLocation + "\\" + existingEntity.LogoFilename;
                        File.Delete(localfilename);
                    }
                    _imgurl = String.Empty;
                }
            }

            Entity entity = new Entity();
            entity.ID = EntityID;
            entity.Name = txtEntityName.Text.Trim();
            entity.TypeID = EntityTypeID;
            entity.LogoFilename = _imgurl;
            entity.Enabled = chkEntityEnabled.Checked;

            // get expiry date
            if (txtExpiryDate.Text != String.Empty)
            {
                entity.DateExpires = Convert.ToDateTime(txtExpiryDate.Text.Trim());
            }

            if (DAC_Entity.Update(entity))
            {
                Response.Redirect(ResolveUrl(String.Format("~/EntityManagement.aspx?typeid={0}&info=updated", EntityTypeID)));
            }
            else
            {
                this.InsertError("Failed to update entity", "EntityGroupServer");
            }

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {            
            if(DAC_Entity.Delete(EntityID))
            {
                Response.Redirect(ResolveUrl(String.Format("~/EntityManagement.aspx?typeid={0}&info=deleted", EntityTypeID)));
            }
            else
            {
                this.InsertError("Failed to delete entity", "EntityGroupServer");
            }          
        }

        protected void btnSaveCreate_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;

            Entity item = new Entity();

            item.Name = txtEntityNameCreate.Text.Trim();
            item.TypeID = EntityTypeID;

            if (DAC_Entity.Insert(item) > 0)
            {
                Response.Redirect(ResolveUrl(String.Format("~/EntityManagement.aspx?typeid={0}&info=added", EntityTypeID)));
            }
            else
            {
                this.InsertError("Failed to create entity", "EntityGroupCreateServer");
            }
        }

        private void LoadDetail()
        {
            Entity e = DAC_Entity.GetByID(EntityID, UserSession.ClientID);
            if (e == null) return;

            string _rootpath = FluentContentSettings.WebsiteRoot;
            if (_rootpath.Substring(_rootpath.Length - 1, 1) != "/") { _rootpath += "/"; }

            txtEntityName.Text = e.Name;
            chkEntityEnabled.Checked = e.Enabled;
            txtExpiryDate.Text = e.DateExpires == DateTime.MinValue ? String.Empty : e.DateExpires.ToString("dd/MM/yyyy");

            if (e.LogoFilename != null && e.LogoFilename != String.Empty)
            {
                image.ImageUrl = _rootpath + FluentContentSettings.ImageUploadFolder + "/" + e.LogoFilename;
                image.Visible = true;
                chkDeleteImage.Checked = false;
                chkDeleteImage.Visible = true;
                deleteLabel.Visible = true;
            }

            EntityType type = DAC_EntityType.Get(e.TypeID);
            if (type != null)
            {
                image_instr.Text = String.Format(".jpg .png .gif (w:{0}px {1}h:{2}px {3})", type.Width, RestrictionString(type.ImageWidthRestrict),
                     type.Height, RestrictionString(type.ImageHeightRestrict));
                hdnImage1Dims.Value = String.Format("{0},{1}|{2},{3}", type.Width, type.Height, type.ImageWidthRestrict, type.ImageHeightRestrict);
            }
            mvItems.SetActiveView(viewDetail);
        }

        private void LoadListing()
        {
            //lvItems.DataSource = DAC_Entity.Get(UserSession.ClientID);
            lvItems.DataSource = DAC_Entity.GetByTypeID(EntityTypeID);
            lvItems.DataBind();
        }

        private bool ValidDimensions(FileUpload fu, string dimensionData)
        {
            int width = 0, height = 0;
            char widthRestrict, heightRestrict;

            string[] dimData = dimensionData.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
            string[] dims = dimData[0].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            string[] restrictions = dimData[1].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            width = dims[0].ToInt32();
            height = dims[1].ToInt32();

            widthRestrict = restrictions[0][0];
            heightRestrict = restrictions[1][0];

            int invalidCount = 0;
            using (System.Drawing.Image image = System.Drawing.Image.FromStream(fu.PostedFile.InputStream))
            {
                switch (widthRestrict)
                {
                    // maximum
                    case 'm':
                        if (image.Width > width) invalidCount++;
                        break;
                    // minimum
                    case 'n':
                        if (image.Width < width) invalidCount++;
                        break;
                    // exact
                    case 'x':
                    default:
                        if (image.Width != width) invalidCount++;
                        break;
                }

                switch (heightRestrict)
                {
                    // maximum
                    case 'm':
                        if (image.Height > height) invalidCount++;
                        break;
                    // minimum
                    case 'n':
                        if (image.Height < height) invalidCount++;
                        break;
                    // exact
                    case 'x':
                    default:
                        if (image.Height != height) invalidCount++;
                        break;
                }


                return (invalidCount == 0);
            }
        }

        private string RestrictionString(string restrictionData)
        {
            switch (restrictionData)
            {
                case "m":
                    return "max ";
                case "n":
                    return "min ";
                default:
                    return String.Empty;
            }
        }
    }
}
