﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CmsMaster.Master" AutoEventWireup="true"
    CodeBehind="MyAccount.aspx.cs" Inherits="CMS.MyAccount" %>

<asp:Content ID="Content3" ContentPlaceHolderID="placeholderHeadCSS" runat="server">
    <link id="Link2" href="FluentControls/css/flcontent.css" runat="server" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="placeholderHeadJS" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="placeholderHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="placeholderBody" runat="server">

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Change Personal Details</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="~/Home.aspx" runat="server">Home</a>
                </li>
                <li>Change Personal Details & PassWord
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Edit Profile</h5>
                         <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
                    </div>
                    <div class="ibox-content" style="display: block;">
                        <BP:UCAddEditCMSUser ID="UCAddEditCMSUser" runat="server" />
                    </div>
                </div>
            </div>
        </div>
        <BP:UCChangePassword ID="UCChangePassword" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="placeholderFooter" runat="server">
</asp:Content>

