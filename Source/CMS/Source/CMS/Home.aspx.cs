﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.Helpers.Interfaces;
using CMS.Helpers.Entities;
using System.Configuration;
using System.Net.Configuration;
using System.Web.Configuration;

namespace CMS
{
    public partial class Home : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadSubMenu();
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Welcome To Fluent Web Soiution','success');", true);
            }
        }

        /* Custom logic*/

        private void LoadSubMenu()
        {
            ICMSMaster master = this.Master as ICMSMaster;
            if (master == null) return;

            List<SubMenuItem> subMenu = new List<SubMenuItem>();

            subMenu.Add(new SubMenuItem(ResolveUrl("~/Home.aspx"), "Home"));
            subMenu.Add(new SubMenuItem(ResolveUrl("~/Settings.aspx"), "Settings"));
            subMenu.Add(new SubMenuItem(ResolveUrl("~/CmsUserManagement.aspx"), "CMS Users"));                        

            master.SetSubMenuDataSource(subMenu, "Main Menu");
        }
    }
}
