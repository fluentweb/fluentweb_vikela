﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CmsMaster.Master" AutoEventWireup="true" CodeBehind="TMCountryAdditionalRequirementManagement.aspx.cs" Inherits="CMS.TMCountryAdditionalRequirementManagement" %>

<asp:Content ID="Content4" ContentPlaceHolderID="placeholderHeadCSS" runat="server">
    <!-- Data Tables -->
    <link href="css_inispinia/plugins/dataTables/dataTables.bootstrap.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="css_inispinia/plugins/dataTables/dataTables.responsive.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="css_inispinia/plugins/dataTables/dataTables.tableTools.min.css" runat="server" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="placeholderHeadJS" runat="server">
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/jeditable/jquery.jeditable.js")%>" type="text/javascript"></script>
    <!-- Data Tables -->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/dataTables/jquery.dataTables.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/dataTables/dataTables.bootstrap.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/dataTables/dataTables.responsive.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/dataTables/dataTables.tableTools.min.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="placeholderHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="placeholderBody" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Trade Mark Management</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="~/Home.aspx" runat="server">Home</a>
                </li>
                <li>
                    <a href="~/TMCountryAdditionalRequirementManagement.aspx" runat="server">Country Additional Requirement</a>
                </li>
                <li>
                    <asp:Literal ID="litBreadCromp" runat="server"></asp:Literal>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <asp:MultiView ID="mvPages" runat="server" ActiveViewIndex="0">
            <asp:View ID="viewListing" runat="server">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5></h5>
                                <div class="ibox-tools">
                                    <span class="label label-success  pull-right">
                                        <asp:Literal ID="count" runat="server"></asp:Literal>
                                        &nbsp;Country(s) Additional Requirement</span>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <div class="col-sm-4">
                                            <asp:Button ID="btnCreateTop" runat="server" CssClass="btn btn-outline btn-success" type="submit" OnClick="btnCreateTop_Click" Text="Create" />
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTables-example">
                                        <thead>
                                            <tr>
                                                <th>Country Name</th>
                                                <th>Requirement question</th>
                                                <th>Question Type</th>
                                                <th>Action</th>

                                            </tr>
                                        </thead>

                                        <asp:ListView ID="lvTMCountryAdditionalRequirement" runat="server">
                                            <LayoutTemplate>
                                                <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                            </LayoutTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%#Eval("chrCountry") %></td>
                                                    <td>
                                                        <%#Eval("chrRequirementQuestion") %>
                                                    </td>
                                                    <td><%#Eval("chrQuestionTypeDesc")%></td>
                                                    <td class="center"><a class="btn btn-info" style="font-size: 12px !important;" href="TMCountryAdditionalRequirementManagement.aspx?countryadditionalrequirementid=<%#Eval("intCountryAdditionalRequirementID") %>"><i class="fa fa-paste"></i>&nbsp;Edit</a>
                                                        &nbsp;<asp:LinkButton ID="btndelete" CssClass="btn btn-danger" type="button" runat="server" CommandName="DeleteTMCountryAdditionalRequirement" CommandArgument='<%#Eval("intCountryAdditionalRequirementID") %>' OnClientClick="return confirm('Are you sure, to Delete?');return false;" OnClick="btndelete_Click"><i class="fa fa-trash"></i>&nbsp;Delete</asp:LinkButton>
                                                        &nbsp;<asp:LinkButton ID="lnbChoice" CssClass="btn btn-warning" type="button" runat="server" CommandName="ChoiceTMCountryAdditionalRequirement" CommandArgument='<%#Eval("intCountryAdditionalRequirementID") %>' OnClick="lnbChoice_Click"><i class="fa fa-hand-o-up"></i>&nbsp;Choices</asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <EmptyDataTemplate>
                                                <p>
                                                    There are currently no Additional requarment
                                                </p>
                                            </EmptyDataTemplate>
                                        </asp:ListView>
                                        <tfoot>
                                            <tr>
                                                <th>Country Name</th>
                                                <th>Requirement question</th>
                                                <th>Question Type</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <div class="col-sm-4">
                                            <asp:Button ID="btnCreate" runat="server" CssClass="btn btn-outline btn-success" type="submit" OnClick="btnCreateTop_Click" Text="Create" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:View>
            <asp:View ID="viewDetail" runat="server">
                <%--   <div class="errorSummary hidden" validationgroup="TMCountryGroup">
                    <p>
                        Please correct the highlighted fields
                    </p>
                </div>
                <asp:ValidationSummary ID="vsErrorSummary" runat="server" ValidationGroup="PageServerGroup"
                    CssClass="errorSummary" />--%>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="tabs-container">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#tab-1">Additional Requirement Country Detail</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="tab-1" class="tab-pane active">
                                    <div class="panel-body">
                                        <br />
                                        <FC:TMCountryAdditionalRequirementDetail ID="UCTMCountryAdditionalRequirementDetail" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:View>
        </asp:MultiView>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="placeholderFooter" runat="server">
    <script src="<%=ResolveUrl("~/js_inispinia/dataTableCustom.js")%>" type="text/javascript"></script>
</asp:Content>
