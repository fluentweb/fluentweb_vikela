﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Extensions;
using FluentContent.DAC;
using FLContentSiteAssist.BusinessLogic;
using CMS.Helpers.Bases;
using FluentContent.Entities.Enums;
using System.Data;
using FluentContent.Entities;
using System.Collections.Specialized;

namespace CMS
{
    public partial class TMClassificationClassManagement : BasePage
    {
        public NameValueCollection collection;
        public int ClassificationID { get { return Request.GetQueryString<int>("classificationid", -1); } }
        public string ClassificationName { get { return Request.GetQueryString<string>("classificationname", string.Empty); } }
        public int ClassificationClassID { get { return Request.GetQueryString<int>("classificationclassid", -1); } }
        public string Action { get { return Request.GetQueryString<string>("action"); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            collection = HttpUtility.ParseQueryString(Request.Url.Query);
            collection.Remove("classificationclassid");
            collection.Remove("action");

            // check user rights
            if (UserSession.Role.CMSAccessLevel != CMSAccessLevel.Full) Response.Redirect("~/Home.aspx");

            if (!IsPostBack)
            {
                if (ClassificationClassID != -1 || Action == "create")
                {
                    LoadClassificationClassDetail();
                    if (Action == "create")
                        litBreadCromp.Text = "<b>Create</b>";
                    else
                        litBreadCromp.Text = "<b>Edit</b>";
                }
                else
                    LoadClassificationClasslisting();
            }
        }
        private void LoadClassificationClasslisting()
        {
            count.Text = "0";
            DataTable dtTMClassificationClass = DAC_TMClassificationClass.GetClassificationClassByClassification(ClassificationID);
            if (dtTMClassificationClass.Rows.Count > 0)
                count.Text = dtTMClassificationClass.Rows.Count.ToString();
            lvTMClassificationClass.DataSource = dtTMClassificationClass;
            lvTMClassificationClass.DataBind();
        }
        private void LoadClassificationClassDetail()
        {
            mvPages.SetActiveView(viewDetail);
        }

        protected void btnCreateTop_Click(object sender, EventArgs e)
        {
            Response.Redirect(ResolveUrl("~/TMClassificationClassManagement.aspx?action=create&" + collection.ToQueryString(false)));
        }

        protected void btndelete_Click(object sender, EventArgs e)
        {
            int classificationclassId = ((LinkButton)sender).CommandArgument.ToInt32();
            bool result = false;
            if (classificationclassId > 0)
                result = DAC_TMClassificationClass.Delete(new TMClassificationClass { TMClassificationClassID = classificationclassId });

            if (result)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Delete success','success');", true);
                Response.Redirect(Request.RawUrl);
            }
            else
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Delete failed!','warning');", true);
        }
    }
}