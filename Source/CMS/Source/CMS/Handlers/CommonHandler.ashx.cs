﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common.Extensions;
using System.Data;
using FluentContent.DAC;
using System.Text;
using System.Web.SessionState;
using FluentContent.Entities;



namespace CMS.Handlers
{
    public class CommonHandler : IHttpHandler, IRequiresSessionState
    {
        public string Action { get { return HttpContext.Current.Request.GetRequestValue<string>("action"); } }

        public void ProcessRequest(HttpContext context)
        {
            switch (Action)
            {
                case "link_list":
                    LoadLinkList(context);
                    break;
                case "form_object_type_template":
                    GetTemplateHTML(context);
                    break;
                case "get_entity_list":
                    GetEntityList(context);
                    break;
                case "get_user_list":
                    GetUserList(context);
                    break;
                case "load_ditinct_attributes":
                    LoadDistinctAttributes(context);
                    break;
                case "get_county_list":
                    LoadCountyList(context);                    
                    break;
            }

        }

        private void LoadCountyList(HttpContext context)
        {
            int countryID = context.Request.GetRequestValue<int>("country_id");
            List<County> items = DAC_County.GetCountiesByCountryID(countryID);
            object result = new { Code = 1, Result = items };
            context.Response.Write(result.ToJSON());
        }

        private void LoadDistinctAttributes(HttpContext context)
        {
            int attributeTypeID = context.Request.GetRequestValue<int>("attributeTypeID");

            List<string> types = DAC_PropertyAttribute.GetDistinctAttributes(attributeTypeID);
            
            object result = new { Code = 1, Result = types };
            context.Response.Write(result.ToJSON());

        }

        private void GetUserList(HttpContext context)
        {
            object data = null;

            int clientID = FluentContent.Settings.ClientID;
            List<string> entityIDs = context.Request.GetRequestValue<string>("entityIDs").JSONToObject<List<string>>();

            List<CMSUser> items = DAC_CMSUser.GetAllByEntityIDs(entityIDs);

            if (items != null && items.Count > 0)
            {
                var userList = items.Select(i => new { Name = String.Format("{0} {1}", i.Name, i.Surname), ID = i.ID, Entity = i.EntityName });

                object result = new { Code = 1, Result = userList };
                context.Response.Write(result.ToJSON());
            }
            else
            {
                object result = new { Code = 1, Result = data };
                context.Response.Write(result.ToJSON());
            }

        }

        private void GetEntityList(HttpContext context)
        {
            int clientID = FluentContent.Settings.ClientID;
            object data = null;

            List<Entity> items = DAC_Entity.Get(clientID);

            if (items != null && items.Count > 0)
            {
                var all = items.OrderBy(i => i.Name);
                var entityList = all.Select(i => new { Name = i.Name, ID = i.ID });

                object result = new { Code = 1, Result = entityList };
                context.Response.Write(result.ToJSON());
            }
            else
            {
                object result = new { Code = 1, Result = data };
                context.Response.Write(result.ToJSON());
            }
        }

        private void LoadLinkList(HttpContext context)
        {
            DataTable links = DAC_WebsiteLinks.GetWebsiteLinkList();

            StringBuilder sbReturnArray = new StringBuilder();
            sbReturnArray.Append("var tinyMCELinkList = new Array(");
            if (links != null && links.Rows.Count > 0)
            {
                foreach (DataRow row in links.Rows)
                {
                    sbReturnArray.AppendFormat("[\"{0}\", \"##DOMAIN##/{1}\"],", row["Title"], row["URL"]);
                }
            }

            string value = sbReturnArray.ToString();
            value = value.Remove(value.LastIndexOf(",", value.Length - 1));
            value = value += ");";

            //context.Response.ContentType = "text/javascript";
            //context.Response.Headers.Add("pragma", "no-cache");
            //context.Response.Headers.Add("expires", "0");
            context.Response.Write(value);
        }

        private void GetTemplateHTML(HttpContext context)
        {
            var typeID = context.Request.GetRequestValue<int>("type-id");
            string template = DAC_FormObjectType.GetTemplate(typeID);
            context.Response.Write(template);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
