﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common.Extensions;
using System.Drawing;
using FluentContent.Entities;
using System.IO;
using CMS.Helpers;
using FluentContent.DAC;
using System.Globalization;
using System.Drawing.Imaging;
using System.Reflection;

namespace CMS.Handlers
{
    /// <summary>
    /// Summary description for LoadImage
    /// </summary>
    public class LoadPropertyImage : IHttpHandler
    {
        
        public void ProcessRequest(HttpContext context)
        {
            string guid = context.Request.GetQueryString<string>("guid");
            Bitmap bmp = null;
            PropertyImage image = null;
            FileInfo fileInfo = null;
            Bitmap imageBitmap = null;
            try
            {
                if (!String.IsNullOrEmpty(guid))
                {
                    image = DAC_PropertyImage.GetByGUID(guid);
                    string filePath =  FluentContentSettings.ImageUploadLocation + "/properties/" + image.Filename;
                    fileInfo = new FileInfo(filePath);
                    if (fileInfo.Exists)
                    {
                        MemoryStream ms = null;

                        var bytes = File.ReadAllBytes(filePath);
                        ms = new MemoryStream(bytes);
                        imageBitmap = (Bitmap)System.Drawing.Image.FromStream(ms);

                        //imageBitmap = (Bitmap)Bitmap.FromFile(filePath);

                    }
                }

                if (image == null || imageBitmap == null) return;

                #region Check for caching

                try
                {
                    if (!String.IsNullOrEmpty(context.Request.Headers["If-Modified-Since"]))
                    {
                        CultureInfo provider = CultureInfo.InvariantCulture;
                        var lastMod = DateTime.ParseExact(context.Request.Headers["If-Modified-Since"], "r", provider).ToLocalTime();

                        if (lastMod.ToString("dd/MM/yyyy HH:mm:ss") == fileInfo.LastWriteTime.ToString("dd/MM/yyyy HH:mm:ss"))
                        {
                            context.Response.StatusCode = 304;
                            context.Response.StatusDescription = "Not Modified";
                            return;
                        }
                    }
                }
                catch
                {
                }

                #endregion

                bmp = Resize(context, imageBitmap);
            }
            catch
            {
                throw;
            }

            if (bmp != null)
            {
                MemoryStream ms = null;
                try
                {
                    ms = new MemoryStream();

                    // default mimetype
                    context.Response.ContentType = "image/jpeg";

                    foreach (ImageCodecInfo ci in ImageCodecInfo.GetImageDecoders())
                    {
                        if (ci.FormatID == bmp.RawFormat.Guid)
                        {
                            context.Response.ContentType = ci.MimeType;
                            break;
                        }
                    }

                    bmp.Save(ms, bmp.RawFormat);
                    context.Response.Cache.SetCacheability(HttpCacheability.Public);
                    context.Response.Cache.SetLastModified(fileInfo.LastWriteTime);

                    ms.WriteTo(context.Response.OutputStream);
                }
                finally
                {
                    bmp.Dispose();
                }
            }
        }

        private Bitmap Resize(HttpContext context, Bitmap bmp)
        {
            string imageWidth = context.Request.QueryString["w"];
            string imageHeight = context.Request.QueryString["h"];
            int destinationWidth = 0;
            int destinationHeight = 0;
            Image resizedImage = bmp;

            if (Int32.TryParse(imageWidth, out destinationWidth) && Int32.TryParse(imageHeight, out destinationHeight))
            {
                resizedImage = Common.Images.ImageManager.ResizeImageCrop(bmp, destinationWidth, destinationHeight, Common.Images.AnchorPosition.CENTER);
            }

            return (Bitmap)resizedImage;
        }

        private static DateTime ReadLastModifiedFromResponse(HttpCachePolicy cache)
        {
            BindingFlags flags = BindingFlags.NonPublic | BindingFlags.GetField | BindingFlags.Instance;
            return (DateTime)cache.GetType().GetField("_utcLastModified", flags).GetValue(cache);
        }

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }
    }
}