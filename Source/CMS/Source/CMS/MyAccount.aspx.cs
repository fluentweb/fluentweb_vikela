﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.Helpers;
using CMS.Helpers.Bases;
using CMS.Helpers.Interfaces;
using CMS.Helpers.Entities;

namespace CMS
{
    public partial class MyAccount : BasePage
    {        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadSubMenu();
                InitControls();
            }
        }

        private void InitControls()
        {
            UCAddEditCMSUser.UserID = UserSession.ID;
        }

        private void LoadSubMenu()
        {
            ICMSMaster master = this.Master as ICMSMaster;
            if (master == null) return;

            List<SubMenuItem> subMenu = new List<SubMenuItem>();

            subMenu.Add(new SubMenuItem(ResolveUrl("~/Logout.aspx?info=loggedout"), "Logout"));            

            master.SetSubMenuDataSource(subMenu, "CMS Users");
        }
    }
}
