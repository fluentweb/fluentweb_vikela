﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LinkLocationSettings.ascx.cs"
    Inherits="CMS.FluentControls.LinkLocationSettings" %>
<!-- Individual YUI JS files -->

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/yui/2.9.0/build/yahoo-dom-event/yahoo-dom-event.js"></script>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/yui/2.9.0/build/animation/animation-min.js"></script>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/yui/2.9.0/build/dragdrop/dragdrop-min.js"></script>
<script src="FluentControls/js/common.js" type="text/javascript"></script>

<script type="text/javascript">

    $(function () {
        setListOrder();
    });

    function setListOrder() {
        // used to insert list order into an ASP control for server side use //
        var Dom = YAHOO.util.Dom;
        var Event = YAHOO.util.Event;
        var DDM = YAHOO.util.DragDropMgr;

        var parseList = function (ul, title) {
            var items = ul.getElementsByTagName("li");
            var out = "";
            for (i = 0; i < items.length; i = i + 1) {
                out += items[i].id + ";";
            }
            return out;
        };

        var ul1 = Dom.get("ul1");

        $('[id$="_listSequence"]').val(parseList(ul1, "List 1"));
    }

</script>

<asp:HiddenField ID="hiddenRows" runat="server" />
<asp:HiddenField ID="hidSourceID" runat="server" />
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Add New Item</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content padding" style="display: block;">

                <asp:HiddenField ID="intAdvertID" runat="server" />
                <asp:HiddenField ID="chrAdvertLocation" runat="server" />
                <asp:HiddenField ID="listSequence" runat="server" />
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Title</label>

                        <div class="col-sm-10">
                            <asp:TextBox runat="server" ID="addTitle" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Link</label>
                        <div class="col-sm-10">
                            <asp:TextBox runat="server" ID="addLink" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Enabled</label>

                        <div class="col-sm-10">
                            <asp:CheckBox runat="server" ID="addEnabled" typ="checkbox" Checked="false" />
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <asp:Button runat="server" ID="add" CssClass="btn btn-primary" type="submit" OnClientClick="SetSource(this.id)"
                                OnClick="Add_Click" Text="Add" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Edit Exiting Item</h5>

                <div class="ibox-tools">
                    <span class="label label-success" style="float: none !important">
                        <asp:Literal ID="count" runat="server"></asp:Literal>&nbsp;menu(s)</span>
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content padding" style="display: block;">
                <div class="table-responsive">
                    <div class="linklist">
                        <ul id="ul1">
                            <li>
                                <label class="control-label">Sort</label>
                                <label style="width: 28%; margin-left: 3%; margin-right: 3%;" class="control-label">Title</label>
                                <label style="width: 28%; margin-right: 3%;" class="control-label">Link</label>
                                <label style="width: 10%; margin-right: 3%;" class="control-label">Enable</label>
                                <label style="width: 10%; margin-right: 3%;" class="control-label">Delete</label>
                            </li>
                            <asp:Literal ID="linkrows" runat="server" />
                        </ul>
                    </div>
                </div>
                <div class="form-horizontal">
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4">
                            <asp:Button runat="server" ID="btnSave" CssClass="btn btn-primary" type="submit"
                                OnClick="Update_Click" Text="Save" />
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>



