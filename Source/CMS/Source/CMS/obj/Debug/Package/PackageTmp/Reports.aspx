﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CmsMaster.Master" AutoEventWireup="true"
    CodeBehind="Reports.aspx.cs" Inherits="CMS.Reports" %>

<asp:Content ID="Content4" ContentPlaceHolderID="placeholderHeadCSS" runat="server">
     <link href="css_inispinia/plugins/iCheck/custom.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="css_inispinia/plugins/chosen/chosen.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="css_inispinia/plugins/colorpicker/bootstrap-colorpicker.min.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="css_inispinia/plugins/cropper/cropper.min.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="css_inispinia/plugins/switchery/switchery.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="css_inispinia/plugins/jasny/jasny-bootstrap.min.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="css_inispinia/plugins/nouslider/jquery.nouislider.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="css_inispinia/plugins/datapicker/datepicker3.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="css_inispinia/plugins/ionRangeSlider/ion.rangeSlider.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="css_inispinia/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="css_inispinia/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="css_inispinia/plugins/clockpicker/clockpicker.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="css_inispinia/plugins/daterangepicker/daterangepicker-bs3.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="css_inispinia/plugins/select2/select2.min.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="FluentControls/css/flcontent.css" runat="server" rel="stylesheet" type="text/css" />

      <!-- Data Tables -->
    <link href="css_inispinia/plugins/dataTables/dataTables.bootstrap.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="css_inispinia/plugins/dataTables/dataTables.responsive.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="css_inispinia/plugins/dataTables/dataTables.tableTools.min.css" runat="server" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="placeholderHeadJS" runat="server">
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/jeditable/jquery.jeditable.js")%>" type="text/javascript"></script>
    <!-- Data Tables -->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/dataTables/jquery.dataTables.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/dataTables/dataTables.bootstrap.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/dataTables/dataTables.responsive.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/dataTables/dataTables.tableTools.min.js")%>" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="placeholderHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="placeholderBody" runat="server">
    <asp:MultiView ID="mvReports" runat="server">
        <asp:View ID="viewReportList" runat="server">
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Reports</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="~/Home.aspx" runat="server">Home</a>
                        </li>
                        <li>Reports
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5></h5>
                                <div class="ibox-tools">
                                </div>
                            </div>
                            <div class="ibox-content" style="display: block;">
                                <div class="table-responsive">
                                    <asp:ListView ID="lvReports" runat="server">
                                        <LayoutTemplate>
                                            <table class="table table-striped table-bordered table-hover dataTables-example">
                                                <thead>
                                                    <tr>
                                                        <th>Report Name
                                                        </th>
                                                        <th>Description
                                                        </th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                                <tfoot>
                                                    <tr>
                                                        <th>Report Name
                                                        </th>
                                                        <th>Description
                                                        </th>
                                                        <th>Action</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <%#Eval("Name") %>
                                                </td>
                                                <td>
                                                    <%#Eval("Description") %>
                                                </td>
                                                <td>
                                                    <a class="btn btn-primary" style="font-size: 12px !important;" href="<%#GetViewLink(Eval("ID"))%>"><i class="fa fa-external-link"></i>&nbsp;View</a>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <EmptyDataTemplate>
                                            <p>
                                                There are currently no reports
                                            </p>
                                        </EmptyDataTemplate>
                                    </asp:ListView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:View>
        <asp:View ID="viewReportDetail" runat="server">
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>
                        <asp:Literal ID="litReportName" runat="server"></asp:Literal></h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="~/Home.aspx" runat="server">Home</a>
                        </li>
                        <li>
                            <a href="~/Reports.aspx" runat="server">Reports</a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <div class="form-horizontal">
                                    <strong>
                                        <asp:Literal ID="litReportBreadcrumb" runat="server" /></strong>
                                </div>
                                <br />
                                <a class="btn btn-outline btn-success" type="button" href="#" onclick="history.go(-1); return false;">Back</a>
                                <div class="ibox-tools">
                                </div>
                            </div>
                            <div class="ibox-content" style="display: block;">
                                <div class="form-horizontal">
                                    <asp:Panel ID="pnlFilters" runat="server" CssClass="dateFilterPanel">
                                        <asp:Panel ID="pnlDateFilter" runat="server">
                                            <div class="form-group" id="data_5">
                                                <div class="col-sm-6 input-daterange input-group" id="datepicker" style="margin-left:11px;">
                                                     <span class="input-group-addon">From Date:</span>
                                                    <asp:TextBox ID="txtFromDate" type="text" CssClass="input-sm form-control txtFromDate" name="start" runat="server" MaxLength="255" />
                                                    <span class="input-group-addon">To Date:</span>
                                                    <asp:TextBox ID="txtToDate" type="text" CssClass="input-sm form-control txtToDate" name="end"  runat="server" MaxLength="255" />
                                                </div>
<%--                                                <asp:CompareValidator ID="cvtxdtFrom" runat="server" Operator="DataTypeCheck" Type="Date"
                                                    ControlToValidate="txtFromDate" ErrorMessage="Value must be a whole date " ValidationGroup="date" />
                                                <asp:CompareValidator ID="cvtxdtTo" runat="server" Operator="DataTypeCheck" Type="Date"
                                                    ControlToValidate="txtToDate" ErrorMessage="Value must be a whole date " ValidationGroup="date" />--%>
                                            </div>
                                           <%-- <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    <asp:Label ID="lblFromDate" runat="server" AssociatedControlID="txtFromDate">From Date:</asp:Label>
                                                </label>
                                                <div class="col-sm-4">
                                                </div>
                                                <label class="col-sm-2 control-label">
                                                    <asp:Label ID="lblToDate" runat="server" AssociatedControlID="txtToDate">To Date:</asp:Label>
                                                </label>
                                                <div class="col-sm-4">

                                                </div>
                                            </div>--%>
                                        </asp:Panel>
                                        <asp:Panel runat="server" ID="pnlUserFilter">
                                            <ul id="ulEntityList">
                                                <li class="heading">Entities
                                                 <img src="images/ajax-loader.gif" alt="loading..." class="ajaxLoad hidden" />
                                                </li>
                                            </ul>
                                            <ul id="ulUserList">
                                                <li class="heading">Users</li>
                                                <li>Select at least one entity to list users</li>
                                            </ul>
                                            <div class="clear"></div>
                                        </asp:Panel>
                                        <div class="form-group">
                                            <div class="col-sm-4">
                                                <a href="#" class="btn btn-outline btn-success" id="btnRefreshReport">Apply</a>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTables-example">
                                        <thead>
                                            <tr>
                                                <%= GetDataHeaders() %>
                                            </tr>
                                        </thead>
                                        <%= GenerateRows() %>
                                        <tfoot>
                                            <tr>
                                                <%= GetDataHeaders() %>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:View>
    </asp:MultiView>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="placeholderFooter" runat="server">
    <script src="<%=ResolveUrl("~/js_inispinia/dataTableCustom.js")%>" type="text/javascript"></script>

    <script src="<%=ResolveUrl("~/js_inispinia/plugins/jquery-ui/jquery-ui.min.js") %>" type="text/javascript"></script>
    <!-- Chosen -->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/chosen/chosen.jquery.js")%>" type="text/javascript"></script>
    <!-- JSKnob -->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/jsKnob/jquery.knob.js")%>" type="text/javascript"></script>
    <!-- Input Mask-->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/jasny/jasny-bootstrap.min.js")%>" type="text/javascript"></script>
    <!-- Data picker -->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/datapicker/bootstrap-datepicker.js")%>" type="text/javascript"></script>
    <!-- Clock picker -->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/clockpicker/clockpicker.js")%>" type="text/javascript"></script>
    <!-- Date range use moment.js same as full calendar plugin -->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/fullcalendar/moment.min.js")%>" type="text/javascript"></script>
    <!-- Date range picker -->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/daterangepicker/daterangepicker.js")%>" type="text/javascript"></script>
    <!-- Select2 -->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/select2/select2.full.min.js")%>" type="text/javascript"></script>
    <!-- custom-plugin -->
    <script src="<%=ResolveUrl("~/js_inispinia/custom-plugin.js")%>" type="text/javascript"></script>
   
      <script type="text/javascript">

        var loading = false;

        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }

        function preloadSelectedCheckboxes() {
            var entityIDs = getParameterByName('EntityList');
            if (entityIDs) {
                var array = entityIDs.split(',');
                $('#ulEntityList input[type="checkbox"]').each(function () {
                    var val = $(this).val();
                    for (var i = 0; i < array.length; i++) {
                        if (array[i] == val) {
                            $(this).prop('checked', true);
                        }
                    }
                });
                getUserCheckboxList($('#ulUserList'), array);
            }
        }

        function loadEntityList() {
            if (!loading) {
                loading = true;
                $('.ajaxLoad').removeClass('hidden');
                javaLib.ajaxCall(jLib.resolveUrl('~/Handlers/CommonHandler.ashx'), 'GET', { 'action': 'get_entity_list' }, function (data) {
                    bindCheckboxList($('#ulEntityList'), data);
                    $('.ajaxLoad').addClass('hidden');
                    loading = false;
                    preloadSelectedCheckboxes();
                }, function (error) {
                    console.log(error);
                    $('.ajaxLoad').addClass('hidden');
                    loading = false;
                });
            }
        }

        // bind checkbox list
        function bindCheckboxList(ul, data) {

            $(data).each(function (index) {
                if (data[index].Entity)
                    $(ul).append('<li><label><input type="checkbox" value="' + data[index].ID + '" />' + data[index].Name + ' (' + data[index].Entity + ')</label></li>');
                else
                    $(ul).append('<li><label><input type="checkbox" value="' + data[index].ID + '" />' + data[index].Name + '</label></li>');
            });
        }

        // bind user checkbox list
        function getUserCheckboxList(ul, entityIDs) {
            if (!loading) {
                loading = true;
                $('.ajaxLoad').removeClass('hidden');
                javaLib.ajaxCall(jLib.resolveUrl('~/Handlers/CommonHandler.ashx'), 'GET', { 'action': 'get_user_list', 'entityIDs': JSON.stringify(entityIDs) }, function (data) {
                    console.log(data);
                    $('#ulUserList').empty();
                    $('#ulUserList').append('<li class="heading">Users</li>');
                    if (!data)
                        $('#ulUserList').append('<li>Select at least one entity to list users</li>');
                    bindCheckboxList($('#ulUserList'), data);
                    $('.ajaxLoad').addClass('hidden');

                    // preload any selected users
                    var userIDs = getParameterByName('UserList');
                    if (userIDs) {
                        var array = userIDs.split(',');
                        $('#ulUserList input[type="checkbox"]').each(function () {
                            var val = $(this).val();
                            for (var i = 0; i < array.length; i++) {
                                if (array[i] == val) {
                                    $(this).prop('checked', true);
                                }
                            }
                        });
                    }
                    loading = false;
                }, function (error) {
                    console.log(error);
                    $('.ajaxLoad').addClass('hidden');
                    loading = false;
                });
            }
        }

        // retrieve selected checkbox ids
        function getSelectedIDs(checkboxes) {
            var ids = [];
            $(checkboxes).each(function (index) {
                if ($(this).is(':checked')) {
                    ids.push($(this).val());
                }
            });
            return ids;
        }

        $(function () {
           // $('.datePicker').datepicker({ dateFormat: 'dd-M-yy' });

            var fromParam = getParameterByName('FromDate');
            var toParam = getParameterByName('ToDate');

            if (fromParam != 'null')
                $('.txtFromDate').val(fromParam);
            if (toParam != 'null')
                $('.txtToDate').val(toParam);

            $('#btnRefreshReport').on('click', function () {
                if (!loading) {
                    var from = $('.txtFromDate').val();
                    //from = from.replace(/\s+/g, '-');
                    var to = $('.txtToDate').val();
                    //to = to.replace(/\s+/g, '-');

                    var entityIDs = getSelectedIDs('#ulEntityList input[type="checkbox"]');
                    var userIDs = getSelectedIDs('#ulUserList input[type="checkbox"]');

                    var url = document.URL; //$(this).attr('href');
                    var params = $.parseParams(url.split('?')[1] || '');

                    if (from.length > 0)
                        params['FromDate'] = from;
                    else
                        params['FromDate'] = null;

                    if (to.length > 0)
                        params['ToDate'] = to;
                    else
                        params['ToDate'] = null;

                    if (entityIDs && entityIDs.length > 0)
                        params['EntityList'] = entityIDs.join();
                    else
                        params['EntityList'] = null;

                    if (userIDs && userIDs.length > 0)
                        params['UserList'] = userIDs.join();
                    else
                        params['UserList'] = null;

                    var result = url.split('?')[0] || '';
                    result = result + '?' + decodeURIComponent($.param(params));
                    //window.location.href = result;
                    window.location.replace(result);
                    return false;
                } else {
                    return false;
                }
            });

            // entity checkboxes init
            if ($('#ulEntityList').is(':visible')) {
                loadEntityList();
            }

            // entity checkboxes click event
            $('#ulEntityList input[type="checkbox"]').live('click', (function () {
                if (!loading) {
                    var entityIDs = [];
                    entityIDs = getSelectedIDs('#ulEntityList input[type="checkbox"]');
                    getUserCheckboxList($('#ulUserList'), entityIDs);
                } else {
                    return false;
                }
            }));
        });
    </script>

</asp:Content>

