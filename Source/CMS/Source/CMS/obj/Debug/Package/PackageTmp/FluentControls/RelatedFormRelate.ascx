﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RelatedFormRelate.ascx.cs" Inherits="CMS.FluentControls.RelatedFormRelate" %>
<!-- Individual YUI JS files -->

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/yui/2.9.0/build/yahoo-dom-event/yahoo-dom-event.js"></script>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/yui/2.9.0/build/animation/animation-min.js"></script>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/yui/2.9.0/build/dragdrop/dragdrop-min.js"></script>

<script src="FluentControls/js/common.js" type="text/javascript"></script>

<script type="text/javascript">

    function setListOrder() {
        // used to insert list order into an ASP control for server side use //
        var Dom = YAHOO.util.Dom;
        var Event = YAHOO.util.Event;
        var DDM = YAHOO.util.DragDropMgr;


        var parseList = function(ul, title) {
            var items = ul.getElementsByTagName("li");
            var out = "";
            for (i = 0; i < items.length; i = i + 1) {
                out += items[i].id + ";";
            }
            return out;
        };

        var ul1 = Dom.get("ul1");

        $('[id$="_listSequence"]').val(parseList(ul1, "List 1"));
    }
</script>

<asp:HiddenField ID="hiddenRows" runat="server" />
<asp:HiddenField ID="hidSourceID" runat="server" />
<div class="innerwindow">
    <asp:HiddenField ID="listSequence" runat="server" />
    <div id="fillerLine" runat="server" class="updateMessage" style="height: 20px; width: 100%;">
        <div id="updateMessage" runat="server" visible="false">
            Successfully updated</div>
    </div>
    <div class="relateditemlist">
        <div class="relateditemlistheader">
            <div class="relateditemtitle">
                Item Title</div>
            <br style="clear: left;" />
        </div>
        <div class="addline">
            <div class="filler">
                &nbsp</div>
            <div class="fields">
                <asp:DropDownList runat="server" ID="addTitle" CssClass="input_title" />
            </div>
            <div class="add">
                <asp:ImageButton ImageUrl="images/Add.jpg" runat="server" ID="add" OnClientClick="SetSource(this.id)"
                    OnClick="Add_Click" /></div>
        </div>
        <br />
        <br />
        <div class="relateditemlistheader">
            <div class="relateditemtitle">
            </div>
            <div class="delete" runat="server" id="divDelete">
                Delete</div>
            <br style="clear: left;" />
        </div>
        <ul id="ul1">
            <asp:Literal ID="linkrows" runat="server" />
        </ul>
    </div>
</div>
<div class="message">
    <asp:Button ID="btnSave" runat="server" OnClick="Update_Click" Text="Save" />
    <asp:Literal ID="updatemsg" runat="server" />
</div>
