﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DeleteAnything.ascx.cs"
    Inherits="CMS.FluentControls.DeleteAnything" %>
<asp:HiddenField ID="hidSourceID" runat="server" />
<asp:HiddenField ID="hidObjectType" runat="server" />
<div class="main">
    <div class="innerwindow">
        <div class="deletionText">
            <asp:Literal ID="warningText" runat="server" />
        </div>
    </div>
</div>
<asp:ImageButton runat="server" ID="delete" OnClick="Delete_Click" Width="0px" Height="0px"
    value="" />
