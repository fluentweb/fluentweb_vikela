﻿//Page-Level Scripts

$(document).ready(function () {

        $('.dataTables-example').dataTable({
            "paginate": true,
            "sort": true,
            "info": true,

            //"order": [[0, 'asc']],
            responsive: false,
            "dom": 'T<"clear">lfrtip',
            "tableTools": {
                "sSwfPath": "js_inispinia/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
            },
            "aoColumnDefs": [{
                "bSortable": false,
                "aTargets": ["no-sort"]
            }]
        });

        $('.dataTables-property').dataTable({
            "paginate": false,
            "sort": true,
            "info": false,

           // "order": [[0, 'asc']],
            responsive: false,
            "dom": 'T<"clear">lfrtip',
            "tableTools": {
                "sSwfPath": "js_inispinia/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
            },
            "aoColumnDefs": [{
                "bSortable": false,
                "aTargets": ["no-sort"]
            }]
        });

        $('.dataTables-propertySortnoclint').dataTable({
            "paginate": false,
            "sort": false,
            "info": false,

            // "order": [[0, 'asc']],
            responsive: false,
            "dom": 'T<"clear">lfrtip',
            "tableTools": {
                "sSwfPath": "js_inispinia/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
            },
            "aoColumnDefs": [{
                "bSortable": false,
                "aTargets": ["no-sort"]
            }]
        });
    //$('.dataTables-example').DataTable({
    //    "dom": 'lTfigt',
    //    "tableTools": {
    //        "sSwfPath": "js_inispinia/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
    //    }
    //});

    /* Init DataTables */
    //var oTable = $('#editable').DataTable();

    ///* Apply the jEditable handlers to the table */
    //oTable.$('td').editable('../example_ajax.php', {
    //    "callback": function (sValue, y) {
    //        var aPos = oTable.fnGetPosition(this);
    //        oTable.fnUpdate(sValue, aPos[0], aPos[1]);
    //    },
    //    "submitdata": function (value, settings) {
    //        return {
    //            "row_id": this.parentNode.getAttribute('id'),
    //            "column": oTable.fnGetPosition(this)[2]
    //        };
    //    },

    //    "width": "90%",
    //    "height": "100%"
    //});





    //$('.user-list-table, .client-list-table, .page-list-table').dataTable({
    //    responsive: true,
    //    "dom": 'T<"clear">lfrtip',
    //    "tableTools": {
    //        "sSwfPath": "../../Scripts/Admin/js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
    //    },
    //    "paginate": false,
    //    "searching": false,
    //    "sort": true,
    //    "info": false,
    //    "order": [[0, 'asc']],
    //    "aoColumnDefs": [{
    //        "bSortable": false,
    //        "aTargets": ["no-sort"]
    //    }]

    //});



    //$('.alert-list-table').dataTable({
    //    "order": [[1, 'asc'], [0, 'asc']],
    //    responsive: true,
    //    "dom": 'T<"clear">lfrtip',
    //    "tableTools": {
    //        "sSwfPath": "../../Scripts/Admin/js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
    //    },
    //    "paginate": false,
    //    "sort": true,
    //    "info": false,
    //    "order": [[0, 'asc']],
    //    "aoColumnDefs": [{
    //        "bSortable": false,
    //        "aTargets": ["no-sort"]
    //    }]
    //});
    });