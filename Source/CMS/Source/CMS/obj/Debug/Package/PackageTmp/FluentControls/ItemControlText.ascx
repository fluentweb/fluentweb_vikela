﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ItemControlText.ascx.cs"
    Inherits="CMS.FluentControls.ItemControlText" %>
<asp:HiddenField ID="hidSourceID" runat="server" />
<div class="main">
    <div class="innerwindow">
        <asp:HiddenField ID="intItemID" runat="server" />
        <asp:HiddenField ID="numberOfText" runat="server" />
        <div id="fillerLine" runat="server" class="updateMessage" style="height: 20px; width: 100%;">
            <asp:Label ID="updateMessage" runat="server" Visible="false" Text="Successfully updated" />
        </div>
        <asp:Panel ID="contentPanel" runat="server">
        </asp:Panel>
        <asp:Button ID="btnSave" runat="server" OnClick="Update_Click" Text="Save" />
    </div>
</div>
