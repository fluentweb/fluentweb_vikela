﻿/*
AUTHOR: Lukas Pechar
NOTE: Any html elements with the class attribute set to the same value as the id attribute should 
should not be changed as they are used as jQuery selectors
*/

/* on dom ready */
$(function () {
    $('#aspnetForm').checkFormValidity();
});

function initFromToDatePicker(fromDateInput, toDateInput, showYear, showMonth) {
    // datepicker on start date
    $(fromDateInput).datepicker({
        dateFormat: 'dd/mm/yy',
        changeYear: showYear,
        changeMonth: showMonth,
        onSelect: function (dateText, inst) {
            $(toDateInput).datepicker('option', 'minDate', dateText);
        }
    });

    // datepicker on end date
    $(toDateInput).datepicker({
        dateFormat: 'dd/mm/yy',
        changeYear: showYear,
        changeMonth: showMonth,
        onSelect: function () { }
    });
}

/* jQuery extensions */
(function ($) {
    $.fn.extend({
        validationError: function (options) {
            var defaults = {
                show: true,
                errorClass: ''
            };

            options = $.extend(defaults, options);

            this.each(function () {
                if (options.show)
                    $(this).addClass(options.errorClass);
                else
                    $(this).removeClass(options.errorClass);
            });

        },
        checkFormValidity: function (options) {
            $(this).submit(function (data) {

                var defaults = {
                    errorMessageDiv: $('.errorSummary')
                };

                options = $.extend(defaults, options);

                if (typeof (Page_Validators) != 'undefined') {
                    if (Page_IsValid)
                        $(options.errorMessageDiv).addClass('hidden');
                    else {
                        var invalidGroup = '';
                        $(Page_Validators).each(function () {
                            if (!this.isvalid) {
                                invalidGroup = this.validationGroup;
                                return;
                            }
                        });
                        $(options.errorMessageDiv).each(function () {
                            var currentSummaryGroup = $(this).attr('validationGroup');
                            if (currentSummaryGroup == invalidGroup) {
                                $(this).removeClass('hidden');
                                $('body, html').animate({ scrollTop: $(this).offset().top }, 50);
                            } else {
                                $(this).addClass('hidden');
                            }
                        });
                    }
                }
            });
        },
        /*
        http://www.blograndom.com/blog/2011/04/jquery-find-next-element-in-dom-by-selector/
        */
        getNextElementInDOM: function (selector, options) {
            var defaults = { stopAt: 'body' };
            options = $.extend(defaults, options);

            var parent = $(this).parent();
            var found = parent.find(selector);

            switch (true) {
                case (found.length > 0):
                    return found;
                case (parent.length === 0 || parent.is(options.stopAt)):
                    return $([]);
                default:
                    return parent.getNextElementInDOM(selector);
            }
        }
    });
})(jQuery);

/* Custom library */
var jLib = (function () {

    // gets all indices of stringToFind in str
    var getAllIndices = function (str, stringToFind) {
        var result = [];
        var currentPos = 0;
        while (str.indexOf(stringToFind, currentPos) > -1) {
            var index = str.indexOf(stringToFind, currentPos);
            result.push(index);
            currentPos = index + stringToFind.length;
        }
        return result;
    };

    var doAjaxPost = function (url, method, isAsync, data) {
        var result = false;
        var jsonData = JSON.stringify(data);
        $.ajax({
            url: url,
            type: "POST",
            async: isAsync,
            data: { action: method, parameter: jsonData },
            dataType: "json",
            success: function (msg) {
                if (msg != null) //used for calls without return type
                    result = eval(msg);
                else
                    result = false;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                result = false;
            }
        });
        return result;
    };

    var validDate = function (sender, args) {
        var date = Date.parseExact(args.Value, 'dd/MM/yyyy');

        if (date == null) {
            args.IsValid = false;
        } else {
            args.IsValid = true;
        }
    };

    var resolveUrl = function (url) {
        if (url.indexOf("~/") == 0) {
            url = baseUrl + url.substring(2);
        }
        return url;
    };


    return {
        getAllIndices: getAllIndices,
        doAjaxPost: doAjaxPost,
        validDate: validDate,
        resolveUrl: resolveUrl
    };
})();



function tinyMCEInit(textareaClass, mceWidth, mceHeight) {

    $(textareaClass).tinymce({
        // Location of TinyMCE script
        script_url: 'scripts/plugins/tinymce/jscripts/tiny_mce/tiny_mce.js',

        // website links
        //external_link_list_url: "scripts/plugins/tinymce/jscripts/tiny_mce/website_urls.js",
        external_link_list_url: "Handlers/CommonHandler.ashx?action=link_list",

        // General options
        theme: "advanced",
        plugins: "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
        force_p_newlines: true,
        force_br_newlines: false,
        forced_root_block: '',

        width: mceWidth,
        height: mceHeight,

        // Theme options
        theme_advanced_buttons1: "code,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist,|,link,unlink,|,forecolor,removeformatformatselect,fontselect,fontsizeselect",
        /*theme_advanced_buttons1: "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",        
        theme_advanced_buttons2: "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
        theme_advanced_buttons3: "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
        theme_advanced_buttons4: "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",*/
        theme_advanced_toolbar_location: "top",
        theme_advanced_toolbar_align: "left",
        theme_advanced_statusbar_location: "bottom",
        theme_advanced_resizing: true,
        theme_advanced_resize_horizontal: false,
        valid_elements: "*[*]",
        extended_valid_elements: "iframe[src|width|height|name|align], address"
    });
}

/**
 * $.parseParams - parse query string paramaters into an object.
 * https://gist.github.com/kares/956897
 */
(function ($) {
    var re = /([^&=]+)=?([^&]*)/g;
    var decodeRE = /\+/g;  // Regex for replacing addition symbol with a space
    var decode = function (str) { return decodeURIComponent(str.replace(decodeRE, " ")); };
    $.parseParams = function (query) {
        var params = {}, e;
        while (e = re.exec(query)) {
            var k = decode(e[1]), v = decode(e[2]);
            if (k.substring(k.length - 2) === '[]') {
                k = k.substring(0, k.length - 2);
                (params[k] || (params[k] = [])).push(v);
            }
            else params[k] = v;
        }
        return params;
    };
})(jQuery)


var javaLib = (function () {
    var ajaxCall = function (url, method, data, successCallback, errorCallback) {
        // check for optional params    
        if (typeof (data) == 'function') {
            if (typeof (successCallback) == 'function') {
                errorCallback = successCallback;
            }
            successCallback = data;
        }

        var error;
        var success;

        // set default error callback
        if (typeof (errorCallback) != 'function') {
            error = function (textStatus, errorThrown) {
                console.log('Status: ' + textStatus);
                console.log('Error: ' + errorThrown);
            }
        } else {
            error = errorCallback;
        }

        // set default success callback
        if (typeof (successCallback) != 'function') {
            success = function (data) {
                console.log('Success:' + data);
            }
        } else {
            success = successCallback;
        }

        $.ajax({
            type: method,
            url: url,
            cache: false,
            success: function (data) {
                var response = JSON.parse(data);
                if (response) {
                    if (response.Code == 403 && response.Message == 'Unauthorized') {
                        window.location.href = loginPage;
                    }
                    if (response.Code == 0) {
                        error('Failed', response.Message);
                    } else {
                        success(response.Result);
                    }
                }
            },
            error: error,
            data: data
        });
    };

    return {
        ajaxCall: ajaxCall
    }
})();