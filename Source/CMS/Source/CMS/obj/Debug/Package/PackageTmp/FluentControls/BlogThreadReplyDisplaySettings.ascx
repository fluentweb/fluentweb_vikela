﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlogThreadReplyDisplaySettings.ascx.cs"
    Inherits="CMS.FluentControls.BlogThreadReplyDisplaySettings" %>
<div class="main">
    <div class="innerwindow">
        <asp:HiddenField ID="itemDisplay" runat="server" />
        <div id="fillerLine" runat="server" class="updateMessage" style="height: 40px; width: 100%;">
        </div>
        <div class="input_row">
            <div class="field_name" style="width: 50%;">
                Max number of items</div>
            <div class="field_control" style="width: 45%;">
                <asp:TextBox ID="maxItems" CssClass="input_tiny" runat="server" /></div>
        </div>
        <div class="input_row">
            <div class="field_name" style="width: 50%;">
                Max number of items per page</div>
            <div class="field_control" style="width: 45%;">
                <asp:TextBox ID="maxItemsPerPage" CssClass="input_tiny" runat="server" /></div>
        </div>
        <div class="input_row">
            <div class="field_name" style="width: 50%;">
                Order replies</div>
            <div class="field_control" style="width: 45%;">
                <asp:RadioButton ID="date" Text="by date descending" GroupName="orderby" runat="server" />
                <br />
                <asp:RadioButton ID="datea" Text="by date ascending" GroupName="orderby" runat="server" />
            </div>
        </div>
    </div>
    <div id="updateMessage" runat="server" visible="false" class="updateMessage" style="height: 20px;">
        Successfully updated</div>
    <asp:ImageButton runat="server" ID="update" OnClick="Update_Click" Width="0px" Height="0px"
        value="" />
</div>
