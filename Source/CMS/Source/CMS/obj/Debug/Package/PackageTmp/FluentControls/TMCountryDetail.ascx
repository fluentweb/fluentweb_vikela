﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TMCountryDetail.ascx.cs" Inherits="CMS.FluentControls.TMCountryDetail" %>
<div class="form-horizontal">
    <asp:HiddenField ID="intCountryID" runat="server" />
    <div class="form-group">
        <label class="col-sm-2 control-label">Country</label>

        <div class="col-sm-10">
            <asp:DropDownList ID="ddcountryName" CssClass="form-control m-b" runat="server" AutoPostBack="false">
            </asp:DropDownList>
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Available</label>
        <div class="col-sm-10">
            <asp:CheckBox runat="server" ID="chk_available" CssClass="input_enabled" />
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Power Of Attorney</label>
        <div class="col-sm-10">
            <asp:CheckBox runat="server" ID="chk_powerofattorney" CssClass="input_enabled" />
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Convention Priority</label>
        <div class="col-sm-10">
            <asp:CheckBox runat="server" ID="chk_conventionpriority" CssClass="input_enabled" />
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-lg-2 control-label">Colour Restriction BW</label>
        <div class="col-lg-10">
            <asp:CheckBox runat="server" ID="chk_colourrestrictionbw" CssClass="input_enabled" />
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-lg-2 control-label">Colour Restriction CC</label>

        <div class="col-lg-10">
            <asp:CheckBox runat="server" ID="chk_colourrestrictioncc" CssClass="input_enabled" />
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-lg-2 control-label">Colour Restriction CSM</label>

        <div class="col-lg-10">
            <asp:CheckBox runat="server" ID="chk_colourrestrictioncsm" CssClass="input_enabled" />
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-lg-2 control-label">Colour Restriction CL</label>

        <div class="col-lg-10">
            <asp:CheckBox runat="server" ID="chk_colourrestrictioncl" CssClass="input_enabled" />
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-lg-2 control-label">Trade Mark</label>
        <div class="col-lg-10">
            <asp:DropDownList ID="ddclassificationid" CssClass="form-control m-b" runat="server" AutoPostBack="false">
            </asp:DropDownList>
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-lg-2 control-label">Translation Language</label>
        <div class="col-lg-10">
            <asp:DropDownList ID="ddtranslationlanguage" CssClass="form-control m-b" runat="server" AutoPostBack="false">
            </asp:DropDownList>
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <div class="col-sm-4 col-sm-offset-2">
            <asp:Button ID="btnSave" CssClass="btn btn-primary" type="submit" runat="server" OnClick="btnSave_Click" Text="Save" />
        </div>
    </div>
</div>
