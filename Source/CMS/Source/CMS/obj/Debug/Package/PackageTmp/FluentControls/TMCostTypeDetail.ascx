﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TMCostTypeDetail.ascx.cs" Inherits="CMS.FluentControls.TMCostTypeDetail" %>
<div class="form-horizontal">
    <div class="errorSummary hidden" validationgroup="costtype">
        <center>
                         <p>
                            Please correct the highlighted fields
                        </p>
                        </center>
    </div>
    <asp:ValidationSummary ID="vsErrorSummary" runat="server" DisplayMode="List" ValidationGroup="costtype"
        CssClass="errorSummary" />

    <div class="form-group">
        <label class="col-sm-2 control-label">Cost Type code</label>
        <div class="col-sm-10">
            <asp:TextBox ID="txtCostTypecode" ValidationGroup="costtype" CssClass="form-control" runat="server" MaxLength="3" />
            <asp:RequiredFieldValidator ID="rntxtCostTypecode" runat="server" Display="None" ErrorMessage="Please Fill Out this Field" ValidationGroup="costtype"
                ControlToValidate="txtCostTypecode"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator Display="Dynamic" ValidationGroup="costtype" ControlToValidate="txtCostTypecode" ID="RegularExpressionValidator3" ValidationExpression="^[\s\S]{3,3}$" runat="server" ErrorMessage="3 characters required.">
            </asp:RegularExpressionValidator>
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Cost Type</label>
        <div class="col-sm-10">
            <asp:TextBox ID="txtCostType" ValidationGroup="costtype" CssClass="form-control" runat="server" MaxLength="250" />
            <asp:RequiredFieldValidator ID="rntxtCostType" runat="server" ErrorMessage="Please Fill Out this Field" ValidationGroup="costtype"
                ControlToValidate="txtCostType"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Per Mark</label>
        <div class="col-sm-10">
            <asp:CheckBox runat="server" ID="chk_permark" CssClass="input_enabled" />
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Per Country</label>
        <div class="col-sm-10">
            <asp:CheckBox runat="server" ID="chk_percountry" CssClass="input_enabled" />
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-lg-2 control-label">Per Class</label>
        <div class="col-lg-10">
            <asp:CheckBox runat="server" ID="chk_perclass" CssClass="input_enabled" />
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-lg-2 control-label">Per Product</label>

        <div class="col-lg-10">
            <asp:CheckBox runat="server" ID="chk_perproduct" CssClass="input_enabled" />
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-lg-2 control-label">Per State</label>
        <div class="col-lg-10">
            <asp:CheckBox runat="server" ID="chk_perstate" CssClass="input_enabled" />
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <div class="col-sm-4 col-sm-offset-2">
            <asp:Button ID="btnSave" CssClass="btn btn-primary" ValidationGroup="costtype" type="submit" runat="server" OnClick="btnSave_Click" Text="Save" />
        </div>
    </div>
</div>
