﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ItemDisplaySettings.ascx.cs"
    Inherits="CMS.FluentControls.ItemDisplaySettings" %>
<asp:HiddenField ID="hidSourceID" runat="server" />
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Edit Display Item</h5>
                </div>
                <div class="ibox-content padding" style="display: block;">
                    <div class="form-horizontal">
                        <asp:HiddenField ID="itemDisplay" runat="server" />
                        <div id="fillerLine" runat="server" class="updateMessage" style="height: 40px; width: 100%;">
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Item Display</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="maxItems" CssClass="form-control" runat="server" MaxLength="255" />
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">
                                Order items by</label>

                            <div class="col-sm-2">
                                <asp:RadioButton ID="random" Text="&nbsp;Randomise" Checked="True" GroupName="orderby"
                                    runat="server" />
                            </div>
                            <div class="col-sm-2">
                                <asp:RadioButton ID="sequence" Text="&nbsp;By sequence" GroupName="orderby" runat="server" />
                            </div>
                            <div class="col-sm-3">
                                <asp:RadioButton ID="date" Text="&nbsp;By date descending" GroupName="orderby" runat="server" />
                            </div>
                            <div class="col-sm-3">
                                <asp:RadioButton ID="datea" Text="&nbsp;By date ascending" GroupName="orderby" runat="server" />
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Page size </label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtPageSize" CssClass="form-control" runat="server" MaxLength="255" />
                                <span class="help-block m-b-none">(0 for no paging)</span>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <asp:Button ID="btnSave" CssClass="btn btn-info" type="submit" runat="server" OnClick="Update_Click" Text="Save" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
