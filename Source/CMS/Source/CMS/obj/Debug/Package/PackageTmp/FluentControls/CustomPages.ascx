﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomPages.ascx.cs"
    Inherits="CMS.FluentControls.CustomPages" %>
<asp:HiddenField ID="hidSourceID" runat="server" />
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Add New Page</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content padding" style="display: block;">
                <asp:HiddenField ID="hiddenRows" runat="server" />
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Page Title</label>
                        <div class="col-sm-10">
                            <asp:TextBox runat="server" ID="PageTitle" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Page URL</label>
                        <div class="col-sm-10">
                            <asp:TextBox runat="server" ID="PageURL" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Page Template</label>
                        <div class="col-sm-10">
                            <asp:DropDownList runat="server" ID="PageTemplate" CssClass="form-control m-b" />
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <asp:Button runat="server" ID="add" CssClass="btn btn-primary" type="submit" OnClientClick="SetSource(this.id)"
                                OnClick="Add_Click" Text="Add" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Edit Exiting Page</h5>
                <div class="ibox-tools">
                    <span class="label label-success" style="float: none !important">
                        <asp:Literal ID="count" runat="server"></asp:Literal>&nbsp;page(s)</span>
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                   
                </div>
            </div>
            <div class="ibox-content" style="display: block">
                <div class="table-responsive">
                    <div class="custompagelist">
                        <ul>
                            <li>
                                <label style="width: 20%; margin-left: 3%; margin-right: 1%;" class="control-label">Title</label>
                                <label style="width: 20%; margin-right: 1%;" class="control-label">URL</label>
                                <label style="width: 15%;" class="control-label">Template</label>
                                <label style="width: 10%; margin-right: 1%;" class="control-label">Delete</label>
                                  <label style="width: 15%; margin-right: 1%;" class="control-label"></label>
                                  <label style="width: 13%; margin-right: 1%;" class="control-label"></label>
                            </li>
                            <asp:Literal ID="pagerows" runat="server" />
                        </ul>
                    </div>
                </div>
                <div class="form-horizontal">
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4">
                            <asp:Button runat="server" ID="btnSave" CssClass="btn btn-primary" type="submit" OnClick="Update_Click" Text="Save" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

