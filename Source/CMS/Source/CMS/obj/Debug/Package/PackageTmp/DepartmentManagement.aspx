﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CmsMaster.Master" AutoEventWireup="True"
    CodeBehind="DepartmentManagement.aspx.cs" Inherits="CMS.DepartmentManagement" %>

<asp:Content ID="Content3" ContentPlaceHolderID="placeholderHeadCSS" runat="server">
    <link id="Link1" href="FluentControls/css/flcontent.css" runat="server" rel="stylesheet" type="text/css" />
    <!-- Data Tables -->
    <link href="css_inispinia/plugins/dataTables/dataTables.bootstrap.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="css_inispinia/plugins/dataTables/dataTables.responsive.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="css_inispinia/plugins/dataTables/dataTables.tableTools.min.css" runat="server" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="placeholderHeadJS" runat="server">
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/jeditable/jquery.jeditable.js")%>" type="text/javascript"></script>
    <!-- Data Tables -->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/dataTables/jquery.dataTables.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/dataTables/dataTables.bootstrap.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/dataTables/dataTables.responsive.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/dataTables/dataTables.tableTools.min.js")%>" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="placeholderHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="placeholderBody" runat="server">
    <div id="fillerLine" runat="server" class="updateMessage" style="height: 20px; width: 100%;">
        <% 
            switch (Request.GetQueryString<string>("info", String.Empty))
            {
                case "deleted":
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('tem successfully deleted','success');", true);
                    break;
                case "updated":
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Item successfully updated','success');", true);
                    break;
                case "added":
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Item successfully added','success');", true);
                    break;
                default:
                    break;
            }
        %>
    </div>

    <asp:MultiView ID="mvItems" runat="server" ActiveViewIndex="0">
        <asp:View ID="viewListing" runat="server">
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Department Management</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="~/Home.aspx" runat="server">Home</a>
                        </li>
                        <li class="active">
                            <strong>Department Management
                            </strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Department List</h5>
                                <div class="ibox-tools">
                                </div>
                            </div>
                            <div class="ibox-content padding" style="display: block;">
                                <asp:ListView ID="lvItems" runat="server">
                                    <LayoutTemplate>
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover dataTables-example">
                                                <thead>
                                                    <tr>
                                                        <th>Department
                                                        </th>
                                                        <th>Parent Dept.
                                                        </th>
                                                        <th>Entity
                                                        </th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                                <tfoot>
                                                    <tr>
                                                        <th>Department
                                                        </th>
                                                        <th>Parent Dept.
                                                        </th>
                                                        <th>Entity
                                                        </th>
                                                        <th></th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <%#Eval("Name") %>
                                            </td>
                                            <td>
                                                <%#Eval("ParentName") %>
                                            </td>
                                            <td>
                                                <%#Eval("EntityName") %>
                                            </td>
                                            <td>
                                                <a runat="server" href='<%#String.Format("~/DepartmentManagement.aspx?deptid={0}", Eval("ID")) %>'><i class="fa fa-paste"></i>Edit</a>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:View>
        <asp:View ID="viewDetail" runat="server">
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Department Management</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="~/Home.aspx" runat="server">Home</a>
                        </li>
                        <li class="active"><strong>New Department Management</strong></li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Add Department</h5>
                                <div class="ibox-tools">
                                </div>
                            </div>
                            <div class="ibox-content padding" style="display: block;">

                                <div class="form-horizontal">
                                    <div class="errorSummary hidden" validationgroup="DeptGroup">
                                        <center>
                                            <p>
                                                Please correct the highlighted fields
                                            </p>
                                        </center>
                                    </div>
                                    <asp:ValidationSummary ID="vsErrorSummary" runat="server" ValidationGroup="DeptGroupServer"
                                        CssClass="errorSummary" />

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Department Name
                                        </label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtDepartmentName" CssClass="form-control" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvDepartmentName" runat="server" Display="None"
                                                ValidationGroup="DeptGroup" ControlToValidate="txtDepartmentName"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Entity
                                        </label>
                                        <div class="col-sm-10">
                                            <asp:DropDownList ID="ddlEntities" runat="server" CssClass="form-control m-b" AutoPostBack="true" OnSelectedIndexChanged="ddlEntities_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Parent Department
                                        </label>
                                        <div class="col-sm-10">
                                            <asp:DropDownList ID="ddlParentDepartments" CssClass="form-control m-b" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>

                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-2">
                                            <asp:Button ID="btnSave" runat="server" Text="Save" ValidationGroup="DeptGroup" CssClass="btn btn-primary" type="submit" OnClick="btnSave_Click" />
                                            <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-danger" type="submit" OnClientClick="return confirm('Are you sure? This will also delete any child departments');"
                                                OnClick="btnDelete_Click" />

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:View>
    </asp:MultiView>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="placeholderFooter" runat="server">
    <script src="<%=ResolveUrl("~/js_inispinia/dataTableCustom.js")%>" type="text/javascript"></script>
</asp:Content>
