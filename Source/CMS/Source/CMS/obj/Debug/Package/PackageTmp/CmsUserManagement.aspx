﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CmsMaster.Master" AutoEventWireup="true"
    CodeBehind="CmsUserManagement.aspx.cs" Inherits="CMS.CmsUserManagement" %>

<asp:Content ID="Content3" ContentPlaceHolderID="placeholderHeadCSS" runat="server">
    <link id="Link1" href="FluentControls/css/flcontent.css" runat="server" rel="stylesheet" type="text/css" />
    <!-- Data Tables -->
    <link href="css_inispinia/plugins/dataTables/dataTables.bootstrap.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="css_inispinia/plugins/dataTables/dataTables.responsive.css" runat="server" rel="stylesheet" type="text/css" />
    <link href="css_inispinia/plugins/dataTables/dataTables.tableTools.min.css" runat="server" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="placeholderHeadJS" runat="server">
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/jeditable/jquery.jeditable.js")%>" type="text/javascript"></script>
    <!-- Data Tables -->
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/dataTables/jquery.dataTables.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/dataTables/dataTables.bootstrap.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/dataTables/dataTables.responsive.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/js_inispinia/plugins/dataTables/dataTables.tableTools.min.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="placeholderHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="placeholderBody" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>CMS User Management</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="~/Home.aspx" runat="server">Home</a>
                </li>
                <li class="active">
                    <strong>CMS User Management
                    </strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <asp:MultiView ID="mvUsers" runat="server">
            <asp:View ID="viewUserList" runat="server">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Edit/Delet User(s)</h5>
                                <div class="ibox-tools">
                                    <span class="label label-success  pull-right">
                                        <asp:Literal ID="count" runat="server"></asp:Literal>
                                        &nbsp;user(s)</span>
                                </div>
                            </div>
                            <div class="ibox-content" style="display: block;">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <asp:ValidationSummary ID="errorSummary" runat="server" CssClass="errorSummary" ValidationGroup="UserManagementServer" />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlFilterEntities" CssClass="form-control m-b" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterEntitites_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlFilterDepts" CssClass="form-control m-b" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:Button ID="btnConfirmFilter" CssClass="btn btn-primary" type="submit" runat="server" Text="Filter" OnClick="btnConfirmFilter_Click" />
                                        </div>
                                    </div>
                                </div>
                                <asp:ListView ID="lvUsers" runat="server" OnItemCommand="lvUsers_ItemCommand">
                                    <LayoutTemplate>
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover dataTables-example">
                                                <thead>
                                                    <tr>
                                                        <th>Name
                                                        </th>
                                                        <th>Surname
                                                        </th>
                                                        <th>Username
                                                        </th>
                                                        <th>Email
                                                        </th>
                                                        <th>Role
                                                        </th>
                                                        <th>Dept.
                                                        </th>
                                                        <th>Options
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                                <tfoot>
                                                    <tr>
                                                        <th>Name
                                                        </th>
                                                        <th>Surname
                                                        </th>
                                                        <th>Username
                                                        </th>
                                                        <th>Email
                                                        </th>
                                                        <th>Role
                                                        </th>
                                                        <th>Dept.
                                                        </th>
                                                        <th>Options
                                                        </th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <%#Eval("Name") %>
                                            </td>
                                            <td>
                                                <%#Eval("Surname") %>
                                            </td>
                                            <td>
                                                <%#Eval("Username") %>
                                            </td>
                                            <td>
                                                <%#Eval("Email") %>
                                            </td>
                                            <td>
                                                <%#Eval("Role.Name") %>
                                            </td>
                                            <td>
                                                <%#Eval("DepartmentName") %>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnEdit" CssClass="btn btn-info" runat="server" Visible='<%# (int)Eval("ID") != UserSession.ID %>'
                                                    CommandName="EditUser" CommandArgument='<%#Eval("ID") %>'><i class="fa fa-paste"></i> Edit</asp:LinkButton>
                                                <asp:LinkButton ID="lbtnGenPass" CssClass="btn btn-w-m btn-warning" runat="server" CommandName="GeneratePass" CommandArgument='<%#Eval("ID") %>'
                                                    OnClientClick="return confirm('A new password will be sent to the user. Are you sure?');"><i class="fa fa-building-o"></i> Generate Password</asp:LinkButton>
                                                <asp:LinkButton ID="lbtnDelete" CssClass="btn btn-w-m btn-danger" runat="server" Visible='<%# (int)Eval("ID") != UserSession.ID %>'
                                                    CommandName="RemoveUser" CommandArgument='<%#Eval("ID") %>' OnClientClick="return confirm('Are you sure?');"><i class="fa fa-trash"></i> Delete</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:View>
            <asp:View ID="viewAddEditUser" runat="server">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Add/Edit User</h5>
                                <div class="ibox-tools">
                                </div>
                            </div>
                            <div class="ibox-content" style="display: block;">
                                <BP:UCAddEditCMSUser ID="UCAddEditUser" runat="server" />
                                <br />
                                <asp:ListView ID="lvUserItems" runat="server">
                                    <LayoutTemplate>
                                        <h3>Linked items</h3>
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover dataTables-example">
                                                <thead>
                                                    <tr>
                                                        <th>Type
                                                        </th>
                                                        <th>Title
                                                        </th>
                                                        <th>Linked
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                                <tfoot>
                                                    <tr>
                                                        <th>Type
                                                        </th>
                                                        <th>Title
                                                        </th>
                                                        <th>Linked
                                                        </th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <asp:HiddenField ID="hdnItemID" runat="server" Value='<%#Eval("ItemID") %>' />
                                                <%#Eval("ItemType")%>
                                            </td>
                                            <td>
                                                <%#Eval("ItemName") %>
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chkUserItemChecked" runat="server" Checked='<%#Eval("IsAttached")%>' />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:View>
        </asp:MultiView>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="placeholderFooter" runat="server">
    <script src="<%=ResolveUrl("~/js_inispinia/dataTableCustom.js")%>" type="text/javascript"></script>
</asp:Content>
