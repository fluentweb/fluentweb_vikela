﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ItemsModify.ascx.cs"
    Inherits="CMS.FluentControls.ItemsModify" %>
<!-- Individual YUI JS files -->

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/yui/2.9.0/build/yahoo-dom-event/yahoo-dom-event.js"></script>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/yui/2.9.0/build/animation/animation-min.js"></script>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/yui/2.9.0/build/dragdrop/dragdrop-min.js"></script>

<script src="FluentControls/js/common.js" type="text/javascript"></script>

<script type="text/javascript">

    $(function () {
        setListOrder();
        //check for deletion
        $('.btnSave').click(checkDeletion);
    });

    function checkDeletion() {
        var anyChecked = false;
        $('.input_delete').each(function () {
            if ($(this).is(':checked')) {
                anyChecked = true;
                return false;
            }
        });

        if (anyChecked) {
            return confirm('This will delete the selected items. Are you sure?');
        }
    }

    function setListOrder() {
        // used to insert list order into an ASP control for server side use //
        var Dom = YAHOO.util.Dom;
        var Event = YAHOO.util.Event;
        var DDM = YAHOO.util.DragDropMgr;


        var parseList = function (ul, title) {
            var items = ul.getElementsByTagName("li");
            var out = "";
            for (i = 0; i < items.length; i = i + 1) {
                out += items[i].id + ";";
            }
            return out;
        };

        var ul1 = Dom.get("ul1");

        $('[id$="_itemSequence"]').val(parseList(ul1, "List 1"));
    }
</script>
<style>
    .imgItem {
        border: medium none;
        margin-right: 27px;
        width: 35px;
        vertical-align: middle;
    }
</style>
<asp:HiddenField ID="hiddenRows" runat="server" />
<asp:HiddenField ID="hidSourceID" runat="server" />
<asp:HiddenField ID="intAdvertID" runat="server" />
<asp:HiddenField ID="chrAdvertLocation" runat="server" />
<asp:HiddenField ID="itemSequence" runat="server" />
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Add/Edit Item(s)</h5>
                <div class="ibox-tools">
                    <span class="label label-success" style="float: none !important">
                        <asp:Literal ID="count" runat="server"></asp:Literal>
                        &nbsp;item(s)</span>
                </div>
            </div>
            <div class="ibox-content padding" style="display: block;">
                <div class="form-horizontal">
                    <div class="form-group">
                        <div class="col-sm-4">
                            <asp:Button ID="btnCreateTop" runat="server" CssClass="btn btn-outline btn-success" type="submit" OnClick="Create_Click" Text="Create" />
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <div class="itemlist">
                        <ul id="ul1">
                            <li>
                                <label class="control-label">Sort</label>
                                <label style="width: 28%; margin-left: 3%; margin-right: 3%;" class="control-label">Title</label>
                                <label style="width: 5%; margin-right: 3%;" class="control-label">Image</label>
                                <label style="width: 10%; margin-right: 3%;" class="control-label">Enable</label>
                                <label style="width: 10%; margin-right: 3%;" class="control-label">Delete</label>
                            </li>
                            <asp:Literal ID="itemrows" runat="server" />
                        </ul>
                    </div>
                </div>
                <div class="form-horizontal">
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4">
                            <asp:Button ID="btnCreate" runat="server" CssClass="btn btn-outline btn-success" type="submit" OnClick="Create_Click" Text="Create" />
                            <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" type="submit" OnClick="Update_Click"
                                Text="Save" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
