﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PageImageEdit.ascx.cs"
    Inherits="CMS.FluentControls.PageImageEdit" %>
<asp:HiddenField ID="hidSourceID" runat="server" />
<div class="ibox-content no-padding" style="display: block;">
    <div class="form-horizontal">
        <br />
        <div class="form-group">
            <label class="col-sm-2 control-label">Image</label>
            <div class="col-sm-4">
                <asp:FileUpload ID="ImageUpload" CssClass="btn btn-outline btn-success" type="file" Width="100%" runat="server" />
            </div>
            <div class="col-sm-4">
                <asp:Label ID="image_instr" CssClass="help-block m-b-none" runat="server" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">
            </label>
            <div class="col-sm-10">

                <asp:CheckBox runat="server" ID="overwriteImg" typ="checkbox" Checked="false" />
                &nbsp;<small class="text-navy">Overwrite existing image</small>
                <br />
                <br />
                <asp:Image runat="server" ID="image" CssClass="image" style="outline: medium none;" BorderStyle="None" Height="100px" Visible="false" />
                <br />
                <asp:CheckBox runat="server" ID="deleteImage" CssClass="" Checked="false"
                    Visible="false" />&nbsp;<small class="text-danger"><asp:Label ID="deleteLabel" runat="server" Text="Remove this image" Visible="false" /></small>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Image Title</label>
            <div class="col-sm-10">
                <asp:TextBox ID="imageTitle" CssClass="form-control" runat="server" MaxLength="255" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Image Alt Text</label>
            <div class="col-sm-10">
                <asp:TextBox ID="imageAlt" CssClass="form-control" runat="server" MaxLength="255" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Image Caption</label>
            <div class="col-sm-10">
                <asp:TextBox ID="imageCaption" CssClass="form-control" runat="server" MaxLength="255" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Image Text</label>
            <div class="col-sm-10">
                <asp:TextBox ID="imageText" CssClass="form-control" runat="server" TextMode="MultiLine" Rows="3" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Image Link URL</label>
            <div class="col-sm-10">
                <asp:TextBox ID="imageLinkURL" CssClass="form-control" runat="server" TextMode="MultiLine" Rows="3" />
            </div>
        </div>
        <div class="hr-line-dashed"></div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Image Thumbnail</label>
            <div class="col-sm-4">
                <asp:FileUpload ID="ThumbnailUpload"  CssClass="btn btn-outline btn-success" type="button" Width="100%" runat="server" />
            </div>
            <div class="col-sm-4">
                <asp:Label ID="image_instr_thumb" CssClass="help-block m-b-none" runat="server" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-10">
                <asp:CheckBox runat="server" ID="overwriteThumbnailImg" typ="checkbox" Checked="false" />&nbsp;<small class="text-navy">Overwrite existing image</small>
                <br />
                <asp:Image runat="server" ID="thumbnailImage" CssClass="image" Width="100%" BorderStyle="None" Height="100px" Visible="false" />
                <br />
                <asp:CheckBox runat="server" ID="deleteThumbnailImage" CssClass="" Checked="false"
                    Visible="false" />&nbsp;<asp:Label ID="deleteThumbnailLabel" runat="server" CssClass="text-danger" Text="Remove this image" Visible="false" />
            </div>
        </div>
        <div class="hr-line-dashed"></div>
        <div class="form-group">
            <div class="col-sm-4 col-sm-offset-2">
                <asp:Button ID="btnSave" CssClass="btn btn-primary" type="submit" runat="server" OnClick="Update_Click" Text="Save" />
            </div>
        </div>
    </div>
</div>
