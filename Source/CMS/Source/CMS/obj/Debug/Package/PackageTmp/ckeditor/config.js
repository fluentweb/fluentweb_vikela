/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
    // config.uiColor = '#AADC6E';
    CKEDITOR.config.allowedContent = true; // don't filter my data
    config.extraAllowedContent = 'span;p;i;ul;li;table;tr;th;td;style;h1;h2;h3;h4;h5;h6;h7;h8;h9;h10;h11;h12;*[id];*(*);*{*};()';
    //CKEDITOR.config.format_tags = 'p;pre;address;div;span;i;ul;li;table;tr;th;td;style;h1;h2;h3;h4;h5;h6;h7;h8;h9;h10;h11;h12;em';
    //CKEDITOR.DefaultLanguage = "en";
    // Remove one plugin.
   // config.removePlugins = 'elementspath';

    // Remove multiple plugins.
    config.removePlugins = 'save,maximize,about';
};
