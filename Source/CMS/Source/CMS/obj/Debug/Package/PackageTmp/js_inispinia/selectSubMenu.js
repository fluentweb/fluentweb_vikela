﻿
function extractPageName(hrefString) {
    var arr;
    if (hrefString.indexOf('&') === -1) {
        arr = hrefString.split('/');
    }
    else {
        arr = hrefString.split('&');
        arr = arr[0].split('/');
    }
    return (arr.length < 2) ? hrefString : arr[arr.length - 2].toLowerCase() + arr[arr.length - 1].toLowerCase();
}

function setActiveMenu(arr, crtPage) {
    for (var i = 0; i < arr.length; i++) {
        if (extractPageName(arr[i].href) == crtPage) {
            if (arr[i].parentNode.tagName != "DIV") {
                // arr[i].className = "active";
                arr[i].parentNode.className = "active";
            }
        }
    }
}

function setPage() {
    hrefString = document.location.href ? document.location.href : document.location;

    if (document.getElementById("side-menu") != null) {

        for (var i = 1; i < 10; i++) {
            setActiveMenu(document.getElementById("sub" + i).getElementsByTagName("a"), extractPageName(hrefString));
        }
    }
}

