﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TMClassificationClassDetail.ascx.cs" Inherits="CMS.FluentControls.TMClassificationClassDetail" %>
<div class="form-horizontal">
    <div class="errorSummary hidden" validationgroup="classificationclass">
        <center>
                         <p>
                            Please correct the highlighted fields
                        </p>
                        </center>
    </div>
    <asp:ValidationSummary ID="vsErrorSummary" runat="server" DisplayMode="List" ValidationGroup="classificationclass"
        CssClass="errorSummary" />
    <div class="form-group">
        <label class="col-sm-2 control-label">Class Name</label>
        <div class="col-sm-10">
            <asp:TextBox ID="txtclassname" ValidationGroup="classificationclass" CssClass="form-control" runat="server" MaxLength="255" />
            <asp:RequiredFieldValidator SetFocusOnError="true" ID="rntxtclassname" Display="None" runat="server" ErrorMessage="Please fill out this field" ValidationGroup="classificationclass"
                ControlToValidate="txtclassname"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-lg-2 control-label">Class Code</label>
        <div class="col-lg-10">
            <asp:TextBox ID="txtClassCode" CssClass="form-control" runat="server" MaxLength="255" />
        </div>
    </div>
    <div class="hr-line-dashed"></div>
     <div class="form-group">
        <label class="col-lg-2 control-label">Class Number</label>
        <div class="col-lg-10">
            <asp:TextBox ID="txtclassnumber" ValidationGroup="classificationclass" CssClass="form-control" runat="server" MaxLength="255" />
            <asp:CompareValidator runat="server" Operator="DataTypeCheck" Type="Integer"
                ControlToValidate="txtclassnumber" ErrorMessage="Value must be a whole number" ValidationGroup="classificationclass" />
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <div class="col-sm-4 col-sm-offset-2">
            <asp:Button ID="btnSave" CssClass="btn btn-primary" ValidationGroup="classificationclass" type="submit" runat="server" OnClick="btnSave_Click" Text="Save" />
        </div>
    </div>
</div>
