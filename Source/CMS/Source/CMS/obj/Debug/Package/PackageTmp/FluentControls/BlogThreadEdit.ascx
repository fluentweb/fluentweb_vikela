﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlogThreadEdit.ascx.cs"
    Inherits="CMS.FluentControls.BlogThreadEdit" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Edit</h5>
            </div>
            <div class="ibox-content padding" style="display: block;">
                <div class="form-horizontal">

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Title   </label>
                        <div class="col-sm-10">
                            <asp:TextBox ID="title" CssClass="input_normal_plus" runat="server" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Thread URL   </label>
                        <div class="col-sm-10">
                            <asp:TextBox ID="threadURL" CssClass="input_normal" runat="server" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Author   </label>
                        <div class="col-sm-10">
                            <asp:DropDownList ID="author" CssClass="dropdown_normal" runat="server" />
                        </div>
                    </div>
                    <div class="form-group">
                        <CKEditor:CKEditorControl ID="textEditor" ResizeEnabled="false" BasePath="~/ckeditor" runat="server"></CKEditor:CKEditorControl>
                        <asp:Literal ID="textToEdit" runat="server" />
                    </div>
                    <asp:HiddenField ID="hidSourceID" runat="server" />
                    <asp:HiddenField ID="editedHTML" runat="server" />
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <asp:ImageButton runat="server" ID="create" CssClass="btn btn-primary" type="submit" OnClientClick="GetEditedHTML();" OnClick="Update_Click"
                                value="" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
