﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCForgotPassword.ascx.cs"
    Inherits="CMS.UserControls.UCForgotPassword" %>

<asp:Panel ID="pnlForgotPassword" runat="server" DefaultButton="btnRetrievePassword">

    <div class="passwordBox animated fadeInDown">
        <div class="row">

            <div class="col-md-12">
                <div class="ibox-content">

                    <h2 class="font-bold">Forgot password</h2>

                    <p>
                        Use the form below to retrieve your password
                    </p>
                    <div class="errorSummary hidden" validationgroup="ForgotPasswordGroup">
                        <center>
                        <p>
                            Please correct the highlighted fields
                        </p>
                        </center>
                    </div>
                    <asp:ValidationSummary ID="vsErrorSummary" runat="server" ValidationGroup="ForgotPasswordGroupServer"
                        CssClass="errorSummary" />
                    <div class="row">

                        <div class="col-lg-12">
                            <div class="form-group">
                                <asp:TextBox ID="txtUsername" runat="server" class="form-control" placeholder="User name" MaxLength="20" required="">
                                </asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvUsername" runat="server" Display="None" ValidationGroup="ForgotPasswordGroup"
                                    ControlToValidate="txtUsername"></asp:RequiredFieldValidator>
                            </div>
                            <div class="form-group">
                                <asp:TextBox ID="txtUserEmail" runat="server" type="email" class="form-control" placeholder="Email address" MaxLength="255" required="">
                                </asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvUserEmail" runat="server" Display="None" ValidationGroup="ForgotPasswordGroup"
                                    ControlToValidate="txtUserEmail"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revUserEmail" runat="server" Display="None" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                    ValidationGroup="ForgotPasswordGroup" ControlToValidate="txtUserEmail"></asp:RegularExpressionValidator>

                            </div>
                            <asp:Button ID="btnRetrievePassword" type="submit" class="btn btn-primary block full-width m-b" runat="server" Text="Send new password" OnClick="btnRetrievePassword_Click"
                                ValidationGroup="ForgotPasswordGroup" />
                            <span id="spnInfo" runat="server" class="infoMessage" visible="false"> <a href="~/Login.aspx" runat="server">login</a></span>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-md-12">

                <p class="m-t">
                    <small>© Copyright www.FluentWebSolutions.co.uk 2012. A Fluent Web Solution.</small>
                </p>
            </div>

        </div>
    </div>
</asp:Panel>



