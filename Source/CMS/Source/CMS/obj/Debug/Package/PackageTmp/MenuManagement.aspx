﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CmsMaster.Master" AutoEventWireup="true"
    CodeBehind="MenuManagement.aspx.cs" Inherits="CMS.MenuManagement" %>

<asp:Content ID="Content4" ContentPlaceHolderID="placeholderHeadCSS" runat="server">
     <link id="Link4" href="FluentControls/css/flcontent.css" runat="server" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="placeholderHeadJS" runat="server">
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="placeholderHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="placeholderBody" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Menu Mangement</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="~/Home.aspx" runat="server">Home</a>
                </li>
                <li class="active">
                    <strong><asp:Literal ID="litMenuManagementDescription" runat="server">Menu Mangement</asp:Literal></strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
        </div>
    </div>
        <div class="wrapper wrapper-content animated fadeInRight">
    <FC:LinkLocationSettings ID="UCLinkLocationSettings" runat="server" />
            </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="placeholderFooter" runat="server">
</asp:Content>
