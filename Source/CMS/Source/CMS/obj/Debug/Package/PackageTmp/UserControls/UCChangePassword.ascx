﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCChangePassword.ascx.cs"
    Inherits="CMS.UserControls.UCChangePassword" %>
<asp:Panel ID="pnlChangePassword" runat="server" DefaultButton="btnChangePassword">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Change Password</h5>
                     <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
                </div>
                <div class="ibox-content padding" style="display: block;">
                    <div class="form-horizontal">
                        <div class="errorSummary hidden" validationgroup="ChangePassword">
                            <center>
                            <p>
                                Please correct the highlighted fields
                            </p>
                            </center>
                            <div class="hr-line-dashed"></div>
                        </div>
                        <asp:ValidationSummary ID="vsErrorSummary" runat="server" ValidationGroup="ChangePasswordServer"
                            CssClass="errorSummary" />

                        <div class="form-group">
                            <label class="col-sm-2 control-label">
                                New Password*
                            </label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtPassword" CssClass="form-control" TextMode="Password" runat="server" ></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvPassword" runat="server" Display="None" ValidationGroup="ChangePassword"
                                    ControlToValidate="txtPassword"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">
                                Confirm New Password*
                            </label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtConfirmPassword" CssClass="form-control" TextMode="Password" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvConfirmPassword" runat="server" Display="None"
                                    ValidationGroup="ChangePassword" ControlToValidate="txtConfirmPassword"></asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="cmpPassword" runat="server" ControlToCompare="txtPassword"
                                    ControlToValidate="txtConfirmPassword" Display="None" ValidationGroup="ChangePassword"
                                    Operator="Equal"></asp:CompareValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <asp:Button ID="btnChangePassword" runat="server" CssClass="btn btn-primary" type="submit" Text="Submit" ValidationGroup="ChangePassword"
                                    OnClick="btnChangePassword_Click" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Panel>
