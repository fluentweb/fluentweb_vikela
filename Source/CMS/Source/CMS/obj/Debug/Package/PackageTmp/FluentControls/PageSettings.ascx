﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PageSettings.ascx.cs"
    Inherits="CMS.FluentControls.PageSettings" %>
<asp:HiddenField ID="hidSourceID" runat="server" />
<div class="form-horizontal">
    <asp:HiddenField ID="intPageID" runat="server" />
    <div class="form-group">
        <label class="col-sm-2 control-label">Page Name</label>

        <div class="col-sm-10">
            <asp:TextBox ID="pageName" CssClass="form-control" runat="server" MaxLength="255" />
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Page URL</label>
        <div class="col-sm-10">
            <asp:TextBox ID="pageURL" CssClass="form-control" runat="server" MaxLength="255" />
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Page Title</label>

        <div class="col-sm-10">
            <asp:TextBox ID="pageTitle" CssClass="form-control" runat="server" />
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Page Description</label>

        <div class="col-sm-10">
            <asp:TextBox ID="pageDesc" CssClass="form-control" runat="server" TextMode="MultiLine" Rows="3" />
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-lg-2 control-label">Page Keywords</label>

        <div class="col-lg-10">
            <asp:TextBox ID="pageKeywords" CssClass="form-control" runat="server" TextMode="MultiLine" Rows="3" />
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <div class="col-sm-4 col-sm-offset-2">
            <asp:Button ID="btnSave" CssClass="btn btn-primary" type="submit" runat="server" OnClick="Update_Click" Text="Save" />
        </div>
    </div>
</div>
