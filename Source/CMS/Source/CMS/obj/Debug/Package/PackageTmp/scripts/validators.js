﻿/*
    Overrides default validators, adds 'error' class to controls that failed validation.
    Requires to be registered in OnRender event on server side.
*/


//ValidatorCommonOnSubmit = function() {
//    ClearValidatorCallouts();
//    var result = SetValidatorCallouts();
//    return result;
//}

ValidatorUpdateIsValid = function() {
    Page_IsValid = AllValidatorsValid(Page_Validators);
    ClearValidatorCallouts();
    SetValidatorCallouts();
} 

ValidatorValidate = function(val, validationGroup, event) {
    val.isvalid = true;
    if ((typeof (val.enabled) == 'undefinded' || val.enabled != false) && IsValidationGroupMatch(val, validationGroup)) {
        if (typeof (val.evaluationfunction) == 'function') {
            val.isvalid = val.evaluationfunction(val);
            if (!val.isvalid && Page_InvalidControlToBeFocused == null && typeof (val.focusOnError) == 'string' && val.onFocusError == 't') {
                ValidatorSetFocus(val, event);
            }
        }
    }

    ClearValidatorCallouts();
    SetValidatorCallouts();
    ValidatorUpdateDisplay(val);
}


SetValidatorCallouts = function() {
    var i;
    var pageValid = true;
    for (i = 0; i < Page_Validators.length; i++) {
        var inputControl = document.getElementById(Page_Validators[i].controltovalidate);
        if (!Page_Validators[i].isvalid) {
            // if (pageValid) inputControl.focus();
            WebForm_AppendToClassName(inputControl, 'error');
            $(inputControl).attr('title', Page_Validators[i].errormessage);
            pageValid = false;
        }
    }

    return pageValid;
}


ClearValidatorCallouts = function() {
    var i;
    var invalidControls = [];
    for (i = 0; i < Page_Validators.length; i++) {
        var inputControl = document.getElementById(Page_Validators[i].controltovalidate);
        if(typeof(WebForm_RemoveClassName) == 'function')
			WebForm_RemoveClassName(inputControl, 'error');
        $(inputControl).removeAttr('title');
    }
}
