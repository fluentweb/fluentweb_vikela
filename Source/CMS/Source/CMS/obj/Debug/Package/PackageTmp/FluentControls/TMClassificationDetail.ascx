﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TMClassificationDetail.ascx.cs" Inherits="CMS.FluentControls.TMClassificationDetail" %>
<div class="form-horizontal">
    <div class="errorSummary hidden" validationgroup="classification">
        <center>
                         <p>
                            Please correct the highlighted fields
                        </p>
                        </center>
    </div>
    <asp:ValidationSummary ID="vsErrorSummary" runat="server" DisplayMode="List" ValidationGroup="classification"
        CssClass="errorSummary" />
    <div class="form-group">
        <label class="col-sm-2 control-label">Classification Name</label>
        <div class="col-sm-10">
            <asp:TextBox ID="txtclassificationname" ValidationGroup="classification" CssClass="form-control" runat="server" MaxLength="255" />
            <asp:RequiredFieldValidator SetFocusOnError="true" ID="rntxtclassificationname" Display="None" runat="server" ErrorMessage="Please fill out this field" ValidationGroup="classification"
                ControlToValidate="txtclassificationname"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label class="col-lg-2 control-label">Classification Description</label>
        <div class="col-lg-10">
            <asp:TextBox ID="txtClassificationDescription" ValidationGroup="cost" CssClass="form-control" runat="server" MaxLength="255" />
        </div>
    </div>
    <div class="hr-line-dashed hidden"></div>
    <div class="form-group hidden">
        <label class="col-lg-2 control-label">Old British</label>
        <div class="col-lg-10">
            <asp:CheckBox runat="server" ID="chk_oldbritish" CssClass="input_enabled" />
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <div class="col-sm-4 col-sm-offset-2">
            <asp:Button ID="btnSave" CssClass="btn btn-primary" ValidationGroup="classification" type="submit" runat="server" OnClick="btnSave_Click" Text="Save" />
        </div>
    </div>
</div>
