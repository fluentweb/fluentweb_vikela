﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.Helpers;
using Common.Session;
using FluentContent.Entities;

namespace CMS
{
    public partial class UnAuthMaster : System.Web.UI.MasterPage
    {
        /* Page events*/

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (SessionObjects.GetSession<CMSUser>(SessionKeys.CMS_USER_SESSION) != null)
                Response.Redirect("~/Home.aspx");
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void Render(HtmlTextWriter writer)
        {
            // overrised standard asp.net validation
            string scriptUrl = ResolveUrl("~/scripts/validators.js");
            string validatorOverride = String.Format("<script src='{0}' type='text/javascript'></script>", scriptUrl);
            Page.ClientScript.RegisterStartupScript(this.GetType(), "ValidatorOverrideScripts", validatorOverride, false);
            base.Render(writer);
        }
    }
}
