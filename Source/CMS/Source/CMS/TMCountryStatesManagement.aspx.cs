﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Extensions;
using FluentContent.DAC;
using FLContentSiteAssist.BusinessLogic;
using CMS.Helpers.Bases;
using FluentContent.Entities.Enums;
using System.Data;
using FluentContent.Entities;

namespace CMS
{
    public partial class TMCountryStatesManagement : BasePage
    {
        public int CountryId { get { return Request.GetQueryString<int>("countryid", -1); } }
        public string CountryName { get { return Request.GetQueryString<string>("countryname", string.Empty); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            // check user rights
            if (UserSession.Role.CMSAccessLevel != CMSAccessLevel.Full) Response.Redirect("~/Home.aspx");

            if (!IsPostBack)
            {
                if (CountryId != -1)
                {
                    litBreadCromp.Text = "<b>" + CountryName + "</b>";
                    FillContent();
                }
            }
        }

        private void FillContent()
        {
            count.Text = "0";
            if (CountryId > 0)
            {
                LoadCountries();

                DataTable dtCountryState = DAC_TMCountryState.GetDAC_CountryStateByCountry(CountryId);
                if (dtCountryState.Rows.Count > 0)
                    count.Text = dtCountryState.Rows.Count.ToString();
                lvTMCountryState.DataSource = dtCountryState;
                lvTMCountryState.DataBind();
            }
            else
                Response.Redirect(Request.UrlReferrer.ToString());
        }

        private void LoadCountries()
        {
            ddcountryName.DataSource = DAC_SPFCountry.GetCountryByCountryState(CountryId);
            ddcountryName.DataTextField = "chrCountry";
            ddcountryName.DataValueField = "intCountryID";
            ddcountryName.DataBind();
        }

        protected void btndelete_Click(object sender, EventArgs e)
        {
            string[] commandArgs = ((LinkButton)sender).CommandArgument.ToString().Split(new char[] { ',' });
            string _countryId = commandArgs[0];
            string _memberCountryID = commandArgs[1];

            int countryId = _countryId.ToInt32();
            int memberCountryID = _memberCountryID.ToInt32();

            bool result = false;
            if (CountryId > 0)
                result = DAC_TMCountryState.Delete(new TMCountryState { CountryID = countryId, MemberCountryID = memberCountryID });

            if (result)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Delete success','success');", true);
                FillContent();
            }
            else
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Delete failed!','warning');", true);
        }

        protected void add_Click(object sender, EventArgs e)
        {
            TMCountryState tmcs = new TMCountryState();
            tmcs.CountryID = CountryId;
            tmcs.MemberCountryID = ddcountryName.SelectedValue.ToInt32();
            DAC_TMCountryState.Insert(tmcs);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Country successfully added.','success');", true);
            FillContent();
        }
    }
}