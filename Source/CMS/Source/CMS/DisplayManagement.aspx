﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CmsMaster.Master" AutoEventWireup="true"
    CodeBehind="DisplayManagement.aspx.cs" Inherits="CMS.DisplayManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="placeholderHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="placeholderBody" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Display Mangement</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="~/Home.aspx" runat="server">Home</a>
                </li>
                <li class="active">
                    <strong>
                        <asp:Literal ID="litDisplayManagementDescription" runat="server">Item Display Management</asp:Literal>
                    </strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
        </div>
    </div>

    <h2></h2>

    <div class="wrapper wrapper-content animated fadeInRight">
        <FC:ItemDisplaySettings ID="UCItemDisplaySettings" runat="server" />
    </div>
</asp:Content>
