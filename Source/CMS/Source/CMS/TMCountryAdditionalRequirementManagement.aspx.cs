﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Extensions;
using FluentContent.DAC;
using FLContentSiteAssist.BusinessLogic;
using CMS.Helpers.Bases;
using FluentContent.Entities.Enums;
using System.Data;
using FluentContent.Entities;

namespace CMS
{
    public partial class TMCountryAdditionalRequirementManagement : BasePage
    {
        public int CountryAdditionalReqId { get { return Request.GetQueryString<int>("countryadditionalrequirementid", -1); } }
        public string Action { get { return Request.GetQueryString<string>("action"); } }
        protected void Page_Load(object sender, EventArgs e)
        {
            // check user rights
            if (UserSession.Role.CMSAccessLevel != CMSAccessLevel.Full) Response.Redirect("~/Home.aspx");

            if (!IsPostBack)
            {
                if (CountryAdditionalReqId != -1 || Action == "create")
                {
                    LoadCountryDetail();
                    if (Action == "create")
                        litBreadCromp.Text = "<b>Create</b>";
                    else
                        litBreadCromp.Text = "<b>Edit</b>";
                }
                else
                    LoadCountryListing();
            }
        }
        private void LoadCountryListing()
        {
            count.Text = "0";
            DataTable dtTMCountryAdditionalRequirement = DAC_TMCountryAdditionalRequirement.GetCountryAdditionalRequirement();
            if (dtTMCountryAdditionalRequirement.Rows.Count > 0)
                count.Text = dtTMCountryAdditionalRequirement.Rows.Count.ToString();
            lvTMCountryAdditionalRequirement.DataSource = dtTMCountryAdditionalRequirement;
            lvTMCountryAdditionalRequirement.DataBind();
        }
        private void LoadCountryDetail()
        {
            mvPages.SetActiveView(viewDetail);
        }

        protected void btnCreateTop_Click(object sender, EventArgs e)
        {
            Response.Redirect(ResolveUrl(String.Format("~/TMCountryAdditionalRequirementManagement.aspx?countryadditionalrequirementid={0}&action=create", CountryAdditionalReqId)));
        }

        protected void btndelete_Click(object sender, EventArgs e)
        {
            int countryAdditionalReqId = ((LinkButton)sender).CommandArgument.ToInt32();
            bool result = false;
            if (countryAdditionalReqId > 0)
                result = DAC_TMCountryAdditionalRequirement.Delete(new TMCountryAdditionalRequirement { CountryAdditionalRequirementID = countryAdditionalReqId });

            if (result)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Delete success','success');", true);
                Response.Redirect(Request.RawUrl);
            }
            else
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Delete failed!','warning');", true);
        }

        protected void lnbChoice_Click(object sender, EventArgs e)
        {
            int countryAdditionalReqId = ((LinkButton)sender).CommandArgument.ToInt32();
            if (countryAdditionalReqId > 0)
                Response.Redirect(ResolveUrl(String.Format("~/TMCountryAdditionalRequirementChoiceManagement.aspx?countryadditionalrequirementid={0}", countryAdditionalReqId)));

        }
    }
}