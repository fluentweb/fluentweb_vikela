﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using DataAccess;
using System.Configuration;
using FluentContent.DAC;

namespace CMS
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            // setup DataAccess
            DbFactory.ConnectionSettings = new DataAccess.Setup.DbConnectionSettings(
                ConfigurationManager.ConnectionStrings["SQLCONN"].ConnectionString,
                ConfigurationManager.ConnectionStrings["SQLCONN"].ProviderName);

            // setup Web.Common
            Common.Setup.CommonSettings.ErrorEmailApplicationName = ConfigurationManager.AppSettings["ErrorEmailApplicationName"];
            Common.Setup.CommonSettings.ErrorEmailDisplayName = ConfigurationManager.AppSettings["ErrorEmailDisplayName"];
            Common.Setup.CommonSettings.ErrorEmailRecipientAddress = ConfigurationManager.AppSettings["ErrorEmailRecipientAddress"];
            Common.Setup.CommonSettings.ErrorEmailSenderAddress = ConfigurationManager.AppSettings["ErrorEmailSenderAddress"];
            Common.Setup.CommonSettings.ErrorEmailPassword = ConfigurationManager.AppSettings["ErrorEmailPassword"];	

            // setup Fluent Content 
            Helpers.FluentContentSettings.ImageUploadLocation = ConfigurationManager.AppSettings["ImageUploadLocation"];
            Helpers.FluentContentSettings.DocumentUploadLocation = ConfigurationManager.AppSettings["DocumentUploadLocation"];

            Helpers.FluentContentSettings.ImageUploadFolder = ConfigurationManager.AppSettings["ImageUploadFolder"];
            Helpers.FluentContentSettings.DocumentUploadFolder = ConfigurationManager.AppSettings["DocumentUploadFolder"];

            FLContentSiteAssist.DataAccess.Client.ClientID = Convert.ToInt32(ConfigurationManager.AppSettings["FluentContentClientID"]);
            FLContentSiteAssist.Common.CMS.AdminOverride = ConfigurationManager.AppSettings["AdminOverride"] == "true";

            // setup Fluent Content new
            FluentContent.Settings.ClientID = FLContentSiteAssist.DataAccess.Client.ClientID;
            Helpers.FluentContentSettings.WebsiteRoot = ConfigurationManager.AppSettings["WebsiteRoot"];
            FluentContent.Settings.WebsiteRoot = Helpers.FluentContentSettings.WebsiteRoot;


            DAC_PropertyImage.MinLandscapeImageWidth = Convert.ToInt32(ConfigurationManager.AppSettings["ImageMinLandscapeWidth"]);
            DAC_PropertyImage.MinPortraitImageHeight = Convert.ToInt32(ConfigurationManager.AppSettings["ImageMinPortraitHeight"]);

            DAC_PropertyImage.MaxImageWidth = Convert.ToInt32(ConfigurationManager.AppSettings["ImageMaxWidth"]);
            DAC_PropertyImage.MaxImageHeight = Convert.ToInt32(ConfigurationManager.AppSettings["ImageMaxHeight"]);
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}