﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Extensions;
using FluentContent.DAC;
using FLContentSiteAssist.BusinessLogic;
using CMS.Helpers.Bases;
using FluentContent.Entities.Enums;
using System.Data;
using FluentContent.Entities;


namespace CMS
{
    public partial class TMCostTypeManagement : BasePage
    {
        public int CostTypeID { get { return Request.GetQueryString<int>("costtypeid", -1); } }
        public string Action { get { return Request.GetQueryString<string>("action"); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            // check user rights
            if (UserSession.Role.CMSAccessLevel != CMSAccessLevel.Full) Response.Redirect("~/Home.aspx");

            if (!IsPostBack)
            {
                if (CostTypeID != -1 || Action == "create")
                {
                    LoadCostTypeDetail();
                    if (Action == "create")
                        litBreadCromp.Text = "<b>Create</b>";
                    else
                        litBreadCromp.Text = "<b>Edit</b>";
                }
                else
                    LoadCostTypeListing();
            }
        }
        private void LoadCostTypeListing()
        {
            count.Text = "0";
            DataTable dtTMCostType = DAC_TMCostType.GetCostType();
            if (dtTMCostType.Rows.Count > 0)
                count.Text = dtTMCostType.Rows.Count.ToString();
            lvTMCostType.DataSource = dtTMCostType;
            lvTMCostType.DataBind();
        }
        private void LoadCostTypeDetail()
        {
            mvPages.SetActiveView(viewDetail);
        }

        protected void btnCreateTop_Click(object sender, EventArgs e)
        {
            Response.Redirect(ResolveUrl(String.Format("~/TMCostTypeManagement.aspx?costtypeid={0}&action=create",CostTypeID)));
        }

        protected void btndelete_Click(object sender, EventArgs e)
        {
            int costtypeid = ((LinkButton)sender).CommandArgument.ToInt32();
            bool result = false;
            if (costtypeid > 0)
                result = DAC_TMCostType.Delete(new TMCostType { CostTypeID = costtypeid });

            if (result)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Delete success','success');", true);
                Response.Redirect(Request.RawUrl);
            }
            else
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Delete failed!','warning');", true);
        }
    }
}