﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Extensions;
using FLContentSiteAssist.BusinessLogic;
using FLContentSiteAssist.BusinessLogic.DataProcessing;
using System.Data;
using FluentContent.DAC;
using FluentContent.Entities;
using CMS.Helpers;

using CMS.Helpers.Bases;
using FluentContent.Entities.Enums;

namespace CMS
{
    public partial class ItemManagement : BasePage
    {
        public string Action { get { return Request.GetQueryString<string>("action"); } }
        public int ItemID { get { return Request.GetQueryString<int>("itemid", 0); } }
        public int ItemTypeID { get { return Request.GetQueryString<int>("itemtype", 0); } }

        public int ParentID { get { return Request.GetQueryString<int>("parentid", 0); } }
        public int ParentTypeID { get { return Request.GetQueryString<int>("parenttypeid", 0); } }

        public string RelationshipType { get { return Request.GetQueryString<string>("relationtype"); } }
        public bool IsChildCreateType { get { return Request.GetQueryString<bool>("childcreate", false); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            // check user rights
            if (UserSession.Role.CMSAccessLevel != CMSAccessLevel.Full) Response.Redirect("~/Home.aspx");

            if (!IsPostBack)
            {
                if (Action == "create" || ItemID != 0)
                {
                    mvItems.SetActiveView(viewDetail);

                    LoadRelatedItemDropdownAndView();
                    LoadRelatedFormDropdownAndView();
                }
                LoadItemTypeDescription();

                LoadBreadCrumbs();
            }
        }

       

        private void LoadBreadCrumbs()
        {
            Dictionary<string, string> b = new Dictionary<string, string>();

            if (ParentID != 0 && ParentTypeID != 0)
            {
                // root item type list
                b.Add(ItemHelper.GetItemTypeName(ParentTypeID) + " list", ResolveUrl("~/ItemManagement.aspx?itemtype=" + ParentTypeID));

                // parent item detail
                b.Add(ItemHelper.GetItemNameByID(ParentID), ResolveUrl("~/ItemManagement.aspx" +
                    "?itemid=" + ParentID +
                    "&itemtype=" + ParentTypeID));

                b.Add(ItemHelper.GetItemTypeName(ItemTypeID) + " list", ResolveUrl("~/RelatedItemManagement.aspx" +
                    "?itemid=" + ParentID +
                    "&itemtype=" + ParentTypeID +
                    "&relationtype=" + RelationshipType +
                    "&childitemtype=" + ItemTypeID +
                    "&childcreate=" + IsChildCreateType));
            }
            else
            {
                if (ItemTypeID != 0 && mvItems.ActiveViewIndex == 1)
                    b.Add(litItemTypeDescription.Text + " list", ResolveUrl("~/ItemManagement.aspx?itemtype=" + ItemTypeID));
            }

            if (ItemID != 0)
                b.Add(ItemHelper.GetItemNameByID(ItemID), "#");

            lvBreadcrumbs.DataSource = b;
            lvBreadcrumbs.DataBind();
        }

        private void LoadItemTypeDescription()
        {
            litItemTypeDescription.Text = Helpers.ItemHelper.GetItemTypeName(ItemTypeID);
        }

        private void LoadRelatedItemDropdownAndView()
        {
            // else get related item types
            ProcessGetRelatedItemType relatedItem = new ProcessGetRelatedItemType();
            relatedItem.RelatedItem = new FLContentSiteAssist.Common.RelatedItem { ItemTypeID = ItemTypeID };
            relatedItem.Invoke();

            if (relatedItem.ResultSet.HasRows(0))
            {
                ddlRelatedItemType.DataSource = relatedItem.ResultSet;
                ddlRelatedItemType.DataTextField = "chrItemTypeRelateDesc";
                ddlRelatedItemType.DataValueField = "chrItemTypeRelateDesc";
                ddlRelatedItemType.DataBind();
            }
            else
            {
                ddlRelatedItemType.Items.Insert(0, "Unavailable");
                ddlRelatedItemType.Enabled = false;
                btnManage.Enabled = false;
            }
        }
        
        private void LoadRelatedFormDropdownAndView()
        {
            // else get related form types
            DataTable relatedFormTypes = DAC_ItemTypeFormTypeRelation.GetItemTypeRelatedFormTypes(ItemTypeID);

            if (relatedFormTypes.Rows.Count > 0)
            {
                ddlRelatedFormType.DataSource = relatedFormTypes;
                ddlRelatedFormType.DataTextField = "chrFormType";
                ddlRelatedFormType.DataValueField = "intFormTypeID";
                ddlRelatedFormType.DataBind();
            }
            else
            {
                ddlRelatedFormType.Items.Insert(0, "Unavailable");
                ddlRelatedFormType.Enabled = false;
                btnManageForms.Enabled = false;
            }
        }

        protected void btnManage_Click(object sender, EventArgs e)
        {
            DataTable relationType = DAC_ItemRelationType.GetByRelationName(ddlRelatedItemType.SelectedValue);
            if (relationType == null) return;

            Response.Redirect(String.Format("~/RelatedItemManagement.aspx?itemid={0}&itemtype={1}&relationtype={2}&childitemtype={3}&childcreate={4}",
                ItemID, ItemTypeID, relationType.GetDataTableValue<string>(0, "chrItemTypeRelateDesc"),
                relationType.GetDataTableValue<int>(0, "intChildItemTypeID"), relationType.GetDataTableValue<bool>(0, "bitCreateNewChild", false)));
        }

        protected void btnManageForms_Click(object sender, EventArgs e)
        {
            Response.Redirect(String.Format("~/RelatedFormManagement.aspx?itemid={0}&itemtype={1}&formtype={2}",
                ItemID, ItemTypeID, ddlRelatedFormType.SelectedValue));
        }
        
    }
}
