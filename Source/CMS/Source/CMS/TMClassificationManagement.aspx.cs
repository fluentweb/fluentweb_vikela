﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Extensions;
using FluentContent.DAC;
using FLContentSiteAssist.BusinessLogic;
using CMS.Helpers.Bases;
using FluentContent.Entities.Enums;
using System.Data;
using FluentContent.Entities;

namespace CMS
{
    public partial class TMClassificationManagement : BasePage
    {
        public int ClassificationID { get { return Request.GetQueryString<int>("classificationid", -1); } }
        public string Action { get { return Request.GetQueryString<string>("action"); } }
        protected void Page_Load(object sender, EventArgs e)
        {
            // check user rights
            if (UserSession.Role.CMSAccessLevel != CMSAccessLevel.Full) Response.Redirect("~/Home.aspx");

            if (!IsPostBack)
            {
                if (ClassificationID != -1 || Action == "create")
                {
                    LoadCountryDetail();
                    if (Action == "create")
                        litBreadCromp.Text = "<b>Create</b>";
                    else
                        litBreadCromp.Text = "<b>Edit</b>";
                }
                else
                    LoadClassificationListing();
            }
        }
        private void LoadClassificationListing()
        {
            count.Text = "0";
            DataTable dtTMClassification = DAC_TMClassification.GetClassificationByClassification();
            if (dtTMClassification.Rows.Count > 0)
                count.Text = dtTMClassification.Rows.Count.ToString();
            lvTMClassification.DataSource = dtTMClassification;
            lvTMClassification.DataBind();
        }
        private void LoadCountryDetail()
        {
            mvPages.SetActiveView(viewDetail);
        }

        protected void btnCreateTop_Click(object sender, EventArgs e)
        {
            Response.Redirect(ResolveUrl(String.Format("~/TMClassificationManagement.aspx?classificationid={0}&action=create", ClassificationID)));
        }

        protected void btndelete_Click(object sender, EventArgs e)
        {
            int classificationId = ((LinkButton)sender).CommandArgument.ToInt32();
            bool result = false;
            if (classificationId > 0)
                result = DAC_TMClassification.Delete(new TMClassification { TMClassificationID = classificationId });

            if (result)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Delete success','success');", true);
                Response.Redirect(Request.RawUrl);
            }
            else
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Delete failed!','warning');", true);
        }
    }
}