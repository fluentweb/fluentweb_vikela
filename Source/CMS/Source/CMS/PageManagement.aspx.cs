﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Extensions;
using FluentContent.DAC;
using FLContentSiteAssist.BusinessLogic;
using CMS.Helpers.Bases;
using FluentContent.Entities.Enums;
using System.Data;


namespace CMS
{
    public partial class PageManagement : BasePage
    {
        public int PageID { get { return Request.GetQueryString<int>("pageid", -1); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            // check user rights
            if (UserSession.Role.CMSAccessLevel != CMSAccessLevel.Full) Response.Redirect("~/Home.aspx");

            if (!IsPostBack)
            {
                if (PageID != -1)
                    LoadPageDetail();
                else
                    LoadPageListing();
            }
        }

        // Custom logic

        private void LoadPageListing()
        {
            count.Text = DAC_ContentPage.GetPages().Count.ToString();
            lvPages.DataSource = DAC_ContentPage.GetPages();
            lvPages.DataBind();
        }

        private void LoadPageDetail()
        {
            mvPages.SetActiveView(viewDetail);

            // load page by id
            ProcessGetPageByID pageProcess = new ProcessGetPageByID();
            pageProcess.Page = new FLContentSiteAssist.Common.Page { PageID = PageID };
            pageProcess.Invoke();

            if (pageProcess.ResultSet.HasRows(0))
            {
                // load page text areas
                ProcessGetPageText textProcess = new ProcessGetPageText();
                textProcess.SitePage = new FLContentSiteAssist.Common.SitePage { Name = pageProcess.ResultSet.Tables[0].GetDataTableValue<string>(0, "chrPage") };
                textProcess.Invoke();

                // bind text areas
                lvPageTextAreas.DataSource = textProcess.ResultSet.Tables[0];
                lvPageTextAreas.DataBind();

                ProcessGetPageImage imageProcess = new ProcessGetPageImage();
                imageProcess.SitePage = new FLContentSiteAssist.Common.SitePage { Name = pageProcess.ResultSet.Tables[0].GetDataTableValue<string>(0, "chrPage") };
                imageProcess.Invoke();
                
                lvPageImages.DataSource = imageProcess.ResultSet.Tables[0];
                lvPageImages.DataBind();
            }
        }
    }
}
