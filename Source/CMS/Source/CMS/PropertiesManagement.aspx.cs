﻿using FluentContent.DAC;
using FluentContent.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Extensions;
using CMS.Helpers;
using FLContentSiteAssist.BusinessLogic;
using FluentContent.Entities.Helpers;
using System.Collections.Specialized;
using System.IO;
using System.Drawing.Imaging;
using System.Data;
using Common.Url;
using Common.Session;

namespace CMS
{
    public partial class PropertiesManagement : System.Web.UI.Page
    {
        private int _defaultPageSize = 50;

        public int PageNumber { get { return Request.GetQueryString<int>("page", 1); } }

        public int PageSize
        {
            get
            {
                int pageSize = Request.GetQueryString<int>("pagesize", 0);
                if (pageSize > 0)
                    return pageSize;
                return _defaultPageSize;
            }
        }

        public int PropertyID { get { return Request.GetQueryString<int>("propertyID"); } }
        public bool CreateMode { get { return Request.GetQueryString<string>("mode") == "create"; } }
        public string Action { get { return Request.GetQueryString<string>("action"); } }
        public string ImageGUID { get { return Request.GetQueryString<string>("guid"); } }

        public string SortBy { get { return Request.GetQueryString<string>("sort"); } }
        public string SortDirection { get { return Request.GetQueryString<string>("dir"); } }

        public int Status { get { return Request.GetQueryString<int>("status"); } }

        public CMSUser UserSession { get { return SessionObjects.GetSession<CMSUser>(SessionKeys.CMS_USER_SESSION); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (PropertyID > 0 || CreateMode)
                    LoadControls();
                else
                    LoadPropertyList();
            }
        }

        private void LoadControls()
        {

            #region Property logic
            hlPreview.Visible = false;
            mvProperties.SetActiveView(viewPropertyCreateEdit);

            ddlCountry.DataSource = DAC_Country.Get();
            ddlCountry.DataTextField = "Name";
            ddlCountry.DataValueField = "ID";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("-- Select country --", "0"));

            ProcessGetItem process = new ProcessGetItem();
            process.Item = new FLContentSiteAssist.Common.Item { ItemType = "Property Status" };
            process.Invoke();
            ddlStatus.DataSource = process.ResultSet;
            ddlStatus.DataTextField = "chrItemTitle";
            ddlStatus.DataValueField = "intItemID";
            ddlStatus.DataBind();

            ddlCounty.DataSource = DAC_County.GetCountiesByCountryID(ddlCountry.SelectedValue.ToInt32());
            ddlCounty.DataTextField = "CountyName";
            ddlCounty.DataValueField = "ID";
            ddlCounty.DataBind();
            ddlCounty.Items.Insert(0, new ListItem("-- Select county --", "0"));

            ddlVendorCountry.DataSource = DAC_Country.Get();
            ddlVendorCountry.DataTextField = "Name";
            ddlVendorCountry.DataValueField = "ID";
            ddlVendorCountry.DataBind();
            ddlVendorCountry.Items.Insert(0, new ListItem("-- Select country --", "0"));

            ddlVendorCounty.DataSource = DAC_County.GetCountiesByCountryID(ddlVendorCountry.SelectedValue.ToInt32());
            ddlVendorCounty.DataTextField = "CountyName";
            ddlVendorCounty.DataValueField = "ID";
            ddlVendorCounty.DataBind();
            ddlVendorCounty.Items.Insert(0, new ListItem("-- Select county --", "0"));

            ddlForRentStatus.DataSource = ItemHelper.LoadItemDisplay("For Rent Status");
            ddlForRentStatus.DataTextField = "chrItemTitle";
            ddlForRentStatus.DataValueField = "intItemID";
            ddlForRentStatus.DataBind();
            ddlForRentStatus.Items.Insert(0, new ListItem("-- Select status --", "0"));

            ddlForSaleStatus.DataSource = ItemHelper.LoadItemDisplay("For Sale Status");
            ddlForSaleStatus.DataTextField = "chrItemTitle";
            ddlForSaleStatus.DataValueField = "intItemID";
            ddlForSaleStatus.DataBind();
            ddlForSaleStatus.Items.Insert(0, new ListItem("-- Select status --", "0"));

            ddlAgencyType.DataSource = ItemHelper.LoadItemDisplay("Agency Type");
            ddlAgencyType.DataTextField = "chrItemTitle";
            ddlAgencyType.DataValueField = "intItemID";
            ddlAgencyType.DataBind();
            ddlAgencyType.Items.Insert(0, new ListItem("-- Select agency type --", "0"));

            ddlTenure.DataSource = ItemHelper.LoadItemDisplay("Tenure");
            ddlTenure.DataTextField = "chrItemTitle";
            ddlTenure.DataValueField = "intItemID";
            ddlTenure.DataBind();
            ddlTenure.Items.Insert(0, new ListItem("-- Select tenure --", "0"));

            ddlCategory.DataSource = ItemHelper.LoadItemDisplay("Category");
            ddlCategory.DataTextField = "chrItemTitle";
            ddlCategory.DataValueField = "intItemID";
            ddlCategory.DataBind();
            ddlCategory.Items.Insert(0, new ListItem("-- Select category --", "0"));

            ddlPropertyType.DataSource = ItemHelper.LoadItemDisplay("Property Type");
            ddlPropertyType.DataTextField = "chrItemTitle";
            ddlPropertyType.DataValueField = "intItemID";
            ddlPropertyType.DataBind();
            ddlPropertyType.Items.Insert(0, new ListItem("-- Select property type --", "0"));

            ddlPriceCurrency.DataSource = ItemHelper.LoadItemDisplay("Currency");
            ddlPriceCurrency.DataTextField = "chrItemTitle";
            ddlPriceCurrency.DataValueField = "intItemID";
            ddlPriceCurrency.DataBind();

            ddlRentalCurrency.DataSource = ItemHelper.LoadItemDisplay("Currency");
            ddlRentalCurrency.DataTextField = "chrItemTitle";
            ddlRentalCurrency.DataValueField = "intItemID";
            ddlRentalCurrency.DataBind();

            ddlRentalTerms.DataSource = ItemHelper.LoadItemDisplay("Rental Terms");
            ddlRentalTerms.DataTextField = "chrItemTitle";
            ddlRentalTerms.DataValueField = "intItemID";
            ddlRentalTerms.DataBind();
            ddlRentalTerms.Items.Insert(0, new ListItem("-- Select rental terms --", "0"));

            DataTable dt = ItemHelper.LoadItemDisplay("Email Templates");
            if (dt != null)
            {
                dt.DefaultView.RowFilter = "bitCheckbox1 = 1";
                ddlEmailAlert.DataSource = dt;
                ddlEmailAlert.DataTextField = "chrItemTitle";
                ddlEmailAlert.DataValueField = "chrItemTitle";
                ddlEmailAlert.DataBind();
            }

            if (ddlStatus.Items.Count == 0)
            {
                ddlStatus.Enabled = false;
                ddlStatus.Items.Insert(0, new ListItem("Please set at least one status under ITEMS > PROPERTY STATUS", "0"));
            }
            else
                ddlStatus.Items.Insert(0, new ListItem("-- Select status --", "0"));
            #endregion

            #region Asset logic
            ddlAssetType.DataSource = ItemHelper.LoadItemDisplay("Assets");
            ddlAssetType.DataTextField = "chrItemTitle";
            ddlAssetType.DataValueField = "intItemID";
            ddlAssetType.DataBind();
            #endregion

            #region Attribute logic
            List<PropertyAttributeType> types = DAC_PropertyAttributeType.GetAll(PropertyID);

            for (int i = 0; i < types.Count; i++)
            {
                ddlAttributeType.Items.Insert(i, new ListItem(types[i].Title, types[i].ID.ToString()));
                ddlAttributeType.Items[i].Attributes.Add("data-type", types[i].DataType);
            }

            if (ddlAttributeType.Items.Count == 0)
            {
                ddlAttributeType.Items.Insert(0, new ListItem("No more attributes available", "0"));
                ddlAttributeType.Enabled = false;
                valueLabel.Visible = false;
                btnSaveAttribute.Visible = false;
            }

            #endregion

            // load property detail
            if (PropertyID > 0)
            {
                Property p = DAC_Property.Get(PropertyID);
                if (p == null) return;

                hlPreview.Visible = true;
                txtAddress1.Text = p.Address1;
                txtAddress2.Text = p.Address2;
                txtAddress3.Text = p.Address3;

                ddlCountry.SelectedValue = p.Country == null || p.Country.ID == 0 ? String.Empty : p.Country.ID.ToString();
                ddlCounty.DataSource = DAC_County.GetCountiesByCountryID(ddlCountry.SelectedValue.ToInt32());
                ddlCounty.DataValueField = "ID";
                ddlCounty.DataTextField = "CountyName";
                ddlCounty.DataBind();
                ddlCounty.Items.Insert(0, new ListItem("-- Select county --", "0"));

                ddlCounty.SelectedValue = p.County == null || p.County.ID == 0 ? String.Empty : p.County.ID.ToString();

                txtAddressPostcode.Text = p.AddressPostcode;
                txtDescription.Text = p.Description;
                txtGoogleCoordinates.Text = p.GoogleCoordinates;
                txtPrice.Text = p.Price != 0 ? p.Price.ToString() : String.Empty;
                txtPriceOld.Text = p.PriceOld != 0 ? p.PriceOld.ToString() : String.Empty;
                txtPropertyReference.Text = p.PropertyCode;
                txtPropertyTitle.Text = p.Title;
                txtRental.Text = p.Rental != 0 ? p.Rental.ToString() : String.Empty;
                txtRentalOld.Text = p.RentalOld != 0 ? p.RentalOld.ToString() : String.Empty;

                txtAdditionalInformation.Text = p.AdditionalInformation;
                txtBathroomCount.Text = p.BathroomCount.ToString();
                txtBedroomCount.Text = p.BedroomCount.ToString();
                txtCommission.Text = p.Commission;
                txtDirections.Text = p.Directions;
                txtMapReference.Text = p.MapReference;
                txtReasonForRemoving.Text = p.VendorReasonForRemoving;
                txtReasonForSellingRenting.Text = p.VendorReasonForSellingRent;
                txtVendorKeysHeldViewingInfo.Text = p.VendorKeysHeldViewingInfo;
                txtVendorAdditionalInfo.Text = p.VendorAdditionalInfo;
                txtVendorAddressLine1.Text = p.VendorAddressLine1;
                txtVendorAddressLine2.Text = p.VendorAddressLine2;
                txtVendorAddressLine3.Text = p.VendorAddressLine3;
                txtVendorEmail.Text = p.VendorEmail;
                txtVendorHomePhone.Text = p.VendorPhoneHome;
                txtVendorHowTheyHeard.Text = p.VendorHowTheyHeard;
                txtVendorMobilePhone.Text = p.VendorPhoneMobile;
                txtVendorNextPurchase.Text = p.VendorNextPurchase;
                txtVendorPostcode.Text = p.VendorPostcode;
                txtVendorRestrictions.Text = p.VendorRestrictions;
                txtVendorsName.Text = p.VendorsName;
                txtVendorWorkPhone.Text = p.VendorPhoneWork;
                txtExistingAttachment.Text = p.AttachmentFileName;
                txtAdditionalDescription.Text = p.AdditionalDescription;

                pnlExistingAttachment.Visible = !String.IsNullOrEmpty(p.AttachmentFileName);

                ddlAgencyType.SelectedValue = p.AgencyTypeItemID.ToString();
                ddlCategory.SelectedValue = p.CategoryItemID.ToString();
                ddlForRentStatus.SelectedValue = p.ForRentStatusItemID.ToString();
                ddlForSaleStatus.SelectedValue = p.ForSaleStatusItemID.ToString();
                ddlPropertyType.SelectedValue = p.PropertyTypeItemID.ToString();
                ddlTenure.SelectedValue = p.TenureItemID.ToString();
                ddlVendorCountry.SelectedValue = p.VendorCountry != null ? p.VendorCountry.ID.ToString() : "0";

                ddlVendorCounty.DataSource = DAC_County.GetCountiesByCountryID(ddlVendorCountry.SelectedValue.ToInt32());
                ddlVendorCounty.DataValueField = "ID";
                ddlVendorCounty.DataTextField = "CountyName";
                ddlVendorCounty.DataBind();
                ddlVendorCounty.Items.Insert(0, new ListItem("-- Select county --", "0"));

                ddlVendorCounty.SelectedValue = p.VendorCounty != null ? p.VendorCounty.ID.ToString() : "0";

                ddlRentalTerms.SelectedValue = p.RentalTermsItemID.ToString();

                ddlStatus.SelectedValue = p.StatusID.ToString();

                if (p.ForSaleCurrencyItemID > 0)
                    ddlPriceCurrency.SelectedValue = p.ForSaleCurrencyItemID.ToString();

                if (p.ForRentCurrencyItemID > 0)
                    ddlRentalCurrency.SelectedValue = p.ForRentCurrencyItemID.ToString();

                chkForRent.Checked = p.ForRent;
                chkForSale.Checked = p.ForSale;
                chkPromotion1.Checked = p.Promotion1;
                chkPromotion2.Checked = p.Promotion2;
                chkAdvertisementAllowed.Checked = p.AdvertisementAllowed;
                chkForSaleRentBoard.Checked = p.ForSaleRentBoard;
                                                
                hlPreview.NavigateUrl = p.WebsitePropertyURL;
                hlAssetsPreviewProperty.NavigateUrl = p.WebsitePropertyURL;
                // load assets
                lvAssets.DataSource = DAC_PropertyAsset.Get(PropertyID);
                lvAssets.DataBind();

                // load images
                lvImages.DataSource = DAC_PropertyImage.GetByPropertyID(PropertyID);
                lvImages.DataBind();

                if (!String.IsNullOrEmpty(ImageGUID))
                {
                    mvImageUpload.SetActiveView(viewEdit);
                    mvEditImage.SetActiveView(viewEditingImage);
                    PropertyImage existingImage = DAC_PropertyImage.GetByGUID(ImageGUID);
                    if (existingImage != null)
                    {
                        txtImageDescription.Text = existingImage.Description;
                        chkImageAsLead.Checked = existingImage.IsLeadImage;
                        chkImageAsLead.Enabled = !chkImageAsLead.Checked;
                        imgImageToEdit.ImageUrl = ResolveUrl("~/property-image/" + existingImage.GUID);
                    }
                }

                // load attributes
                lvAttributes.DataSource = DAC_PropertyAttribute.GetAll(PropertyID);
                lvAttributes.DataBind();
            }
        }

        #region Property Methods

        private string GetDirectionForColumn(string column)
        {
            string dirTemplate = "&dir={0}";
            string val;

            val = SortBy == column ? (SortDirection == "asc" ? "desc" : "asc") : "desc";

            return String.Format(dirTemplate, val);
        }

        private void LoadPropertyList()
        {

            string ascImg = "~/images/sort-asc.png";
            string descImg = "~/images/sort-desc.png";

            imgAddress.ImageUrl = SortBy == "address" ? (SortDirection == "asc" ? ascImg : descImg) : String.Empty;
            imgForRent.ImageUrl = SortBy == "rent" ? (SortDirection == "asc" ? ascImg : descImg) : String.Empty;
            imgForSale.ImageUrl = SortBy == "sale" ? (SortDirection == "asc" ? ascImg : descImg) : String.Empty;
            imgPrice.ImageUrl = SortBy == "price" ? (SortDirection == "asc" ? ascImg : descImg) : String.Empty;
            imgRefNo.ImageUrl = SortBy == "ref" ? (SortDirection == "asc" ? ascImg : descImg) : String.Empty;
            imgRental.ImageUrl = SortBy == "rental" ? (SortDirection == "asc" ? ascImg : descImg) : String.Empty;
            imgTitle.ImageUrl = SortBy == "title" ? (SortDirection == "asc" ? ascImg : descImg) : String.Empty;
            imgType.ImageUrl = SortBy == "type" ? (SortDirection == "asc" ? ascImg : descImg) : String.Empty;
            imgStatus.ImageUrl = SortBy == "status" ? (SortDirection == "asc" ? ascImg : descImg) : String.Empty;
            imgCounty.ImageUrl = SortBy == "county" ? (SortDirection == "asc" ? ascImg : descImg) : String.Empty;
            imgCreated.ImageUrl = SortBy == "created" ? (SortDirection == "asc" ? ascImg : descImg) : String.Empty;

            imgAddress.Visible = !String.IsNullOrEmpty(imgAddress.ImageUrl);
            imgForRent.Visible = !String.IsNullOrEmpty(imgForRent.ImageUrl);
            imgForSale.Visible = !String.IsNullOrEmpty(imgForSale.ImageUrl);
            imgPrice.Visible = !String.IsNullOrEmpty(imgPrice.ImageUrl);
            imgRefNo.Visible = !String.IsNullOrEmpty(imgRefNo.ImageUrl);
            imgRental.Visible = !String.IsNullOrEmpty(imgRental.ImageUrl);
            imgTitle.Visible = !String.IsNullOrEmpty(imgTitle.ImageUrl);
            imgType.Visible = !String.IsNullOrEmpty(imgType.ImageUrl);
            imgStatus.Visible = !String.IsNullOrEmpty(imgStatus.ImageUrl);
            imgCounty.Visible = !String.IsNullOrEmpty(imgCounty.ImageUrl);
            imgCreated.Visible = !String.IsNullOrEmpty(imgCreated.ImageUrl);
            
            
            NameValueCollection collection = HttpUtility.ParseQueryString(Request.Url.Query);
            collection.Remove("pagesize");
            collection.Remove("page");
            collection.Remove("sort");
            collection.Remove("dir");
            string urlTemplate = "~/property-management?sort={0}{1}&page={2}&pagesize={3}{4}";

            hlRefNo.NavigateUrl = String.Format(urlTemplate, "ref", GetDirectionForColumn("ref"), PageNumber, PageSize, collection.Count > 0 ? ("&" + collection.ToQueryString(false)) : String.Empty);
            hlAddress.NavigateUrl = String.Format(urlTemplate, "address", GetDirectionForColumn("address"), PageNumber, PageSize, collection.Count > 0 ? ("&" + collection.ToQueryString(false)) : String.Empty);
            hlForRent.NavigateUrl = String.Format(urlTemplate, "rent", GetDirectionForColumn("rent"), PageNumber, PageSize, collection.Count > 0 ? ("&" + collection.ToQueryString(false)) : String.Empty);
            hlForSale.NavigateUrl = String.Format(urlTemplate, "sale", GetDirectionForColumn("sale"), PageNumber, PageSize, collection.Count > 0 ? ("&" + collection.ToQueryString(false)) : String.Empty);
            hlPrice.NavigateUrl = String.Format(urlTemplate, "price", GetDirectionForColumn("price"), PageNumber, PageSize, collection.Count > 0 ? ("&" + collection.ToQueryString(false)) : String.Empty);
            hlRental.NavigateUrl = String.Format(urlTemplate, "rental", GetDirectionForColumn("rental"), PageNumber, PageSize, collection.Count > 0 ? ("&" + collection.ToQueryString(false)) : String.Empty);
            hlStatus.NavigateUrl = String.Format(urlTemplate, "status", GetDirectionForColumn("status"), PageNumber, PageSize, collection.Count > 0 ? ("&" + collection.ToQueryString(false)) : String.Empty);
            hlTitle.NavigateUrl = String.Format(urlTemplate, "title", GetDirectionForColumn("title"), PageNumber, PageSize, collection.Count > 0 ? ("&" + collection.ToQueryString(false)) : String.Empty);
            hlType.NavigateUrl = String.Format(urlTemplate, "type", GetDirectionForColumn("type"), PageNumber, PageSize, collection.Count > 0 ? ("&" + collection.ToQueryString(false)) : String.Empty);
            hlCounty.NavigateUrl = String.Format(urlTemplate, "county", GetDirectionForColumn("county"), PageNumber, PageSize, collection.Count > 0 ? ("&" + collection.ToQueryString(false)) : String.Empty);
            hlCreated.NavigateUrl = String.Format(urlTemplate, "created", GetDirectionForColumn("created"), PageNumber, PageSize, collection.Count > 0 ? ("&" + collection.ToQueryString(false)) : String.Empty);

            ddlPropertyStatusFilter.DataSource = ItemHelper.LoadItemDisplay("Property Status");
            ddlPropertyStatusFilter.DataTextField = "chrItemTitle";
            ddlPropertyStatusFilter.DataValueField = "intItemID";
            ddlPropertyStatusFilter.DataBind();

            ddlPropertyStatusFilter.SelectedValue = Request.GetQueryString<int>("status", ddlPropertyStatusFilter.Items[0].Value.ToInt32()).ToString();

            mvProperties.SetActiveView(viewPropertyList);
            ResultSet<Property> result = DAC_Property.GetAll(new PagingParameter
            {
                PageNumber = PageNumber,
                RowsPerPage = PageSize,
                SortBy = SortBy,
                SortDirection = SortDirection
            }, Status == 0 ? ddlPropertyStatusFilter.SelectedValue.ToInt32() : Status);
            lvProperties.DataSource = result.Items;
            lvProperties.DataBind();

            litItemCount.Text = String.Format("({0} properties)", result.TotalRows);

            if (result.TotalPages == 0 || result.TotalPages == 1)
            {
                divTopPager.Visible = false;
                divBottomPager.Visible = false;
            }
            else
            {

                List<int> pages = new List<int>();
                for (int i = 0; i < result.TotalPages; i++)
                {
                    pages.Add(i + 1);
                }
                if (pages == null || pages.Count == 1)
                {
                    lvPagerTop.DataSource = null;
                    lvPagerBottom.DataSource = null;
                }
                else
                {
                    lvPagerTop.DataSource = pages;
                    lvPagerTop.DataBind();

                    lvPagerBottom.DataSource = pages;
                    lvPagerBottom.DataBind();
                }
                // buttons
                PrepareNextPreviousButtons(result);
            }
        }

        private void SaveProperty(bool sendAlerts)
        {
            Property p = ReadPropertyValues();

            // create
            if (p.ID == 0)
            {
                p = DAC_Property.Insert(p);
                if (p == null) { this.InsertError("Failed to save", "PropertyGroupServer"); return; }
                if (sendAlerts)
                {
                    int totalAlerted = DAC_PropertySubscribedUser.AlertUsersByPropertyCriteria(p, ddlEmailAlert.SelectedValue, UserSession.ID);
                    Response.Redirect("~/property-management/" + p.ID + "?action=created-alerted&alerted=" + totalAlerted);
                }

                Response.Redirect("~/property-management/" + p.ID + "?action=created");
            }

            // edit
            p = DAC_Property.Update(p);
            if (p == null) { this.InsertError("Failed to save", "PropertyGroupServer"); return; }
            if (sendAlerts)
            {
                int totalAlerted = DAC_PropertySubscribedUser.AlertUsersByPropertyCriteria(p, ddlEmailAlert.SelectedValue, UserSession.ID);
                Response.Redirect("~/property-management/" + p.ID + "?action=updated-alerted&alerted=" + totalAlerted);
            }
            Response.Redirect("~/property-management/" + p.ID + "?action=updated");
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveProperty(false);
        }

        protected void btnSaveAndAlert_Click(object sender, EventArgs e)
        {
            SaveProperty(true);
        }

        protected void btnCreateProperty_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/property-management/create");
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            int propertyID = ((Button)sender).CommandArgument.ToInt32();
            bool result = false;
            if (propertyID > 0)
                result = DAC_Property.Delete(new Property { ID = propertyID });


            if (result)
            {
                if (!Request.RawUrl.Contains("action=deleted"))
                    Response.Redirect(Request.RawUrl.IndexOf('?') == -1 ? Request.RawUrl + "?action=deleted" : Request.RawUrl + "&action=deleted");
                else
                    Response.Redirect(Request.RawUrl);
            }
            else
            {
                this.InsertError("Failed to delete", "PropertyListing");
            }
        }

        protected void ddlPropertyStatusFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            Response.Redirect(String.Format("~/property-management?status={0}", ddlPropertyStatusFilter.SelectedValue));
        }

        private Property ReadPropertyValues()
        {
            Property p = new Property();
            p.ID = PropertyID;
            p.Address1 = txtAddress1.Text.Trim();
            p.Address2 = txtAddress2.Text.Trim();
            p.Address3 = txtAddress3.Text.Trim();
            p.AddressPostcode = txtAddressPostcode.Text.Trim();
            p.Country = new Country { ID = ddlCountry.SelectedValue.ToInt32() };
            p.County = new County { ID = String.IsNullOrEmpty(hdnCounty.Value) ? 0 : hdnCounty.Value.ToInt32() };
            p.Description = txtDescription.Text.Trim();
            p.ForRent = chkForRent.Checked;
            p.ForSale = chkForSale.Checked;
            p.GoogleCoordinates = txtGoogleCoordinates.Text.Trim();
            p.Price = Convert.ToDecimal(txtPrice.Text.Trim() == String.Empty ? "0" : txtPrice.Text.Trim());
            p.PriceOld = Convert.ToDecimal(txtPriceOld.Text.Trim() == String.Empty ? "0" : txtPriceOld.Text.Trim());
            p.Promotion1 = chkPromotion1.Checked;
            p.Promotion2 = chkPromotion2.Checked;
            p.PropertyCode = txtPropertyReference.Text.Trim();
            p.Rental = Convert.ToDecimal(txtRental.Text.Trim() == String.Empty ? "0" : txtRental.Text.Trim());
            p.RentalOld = Convert.ToDecimal(txtRentalOld.Text.Trim() == String.Empty ? "0" : txtRentalOld.Text.Trim());
            p.StatusID = ddlStatus.SelectedValue.ToInt32();
            p.Title = txtPropertyTitle.Text.Trim();
            p.AdditionalInformation = txtAdditionalInformation.Text.Trim();
            p.AdvertisementAllowed = chkAdvertisementAllowed.Checked;
            p.AgencyTypeItemID = ddlAgencyType.SelectedValue.ToInt32();
            p.BathroomCount = String.IsNullOrEmpty(txtBathroomCount.Text.Trim()) ? 0 : txtBathroomCount.Text.ToInt32();
            p.BedroomCount = String.IsNullOrEmpty(txtBedroomCount.Text.Trim()) ? 0 : txtBedroomCount.Text.ToInt32();
            p.CategoryItemID = ddlCategory.SelectedValue.ToInt32();
            p.Commission = txtCommission.Text.Trim();
            p.Directions = txtDirections.Text.Trim();
            p.ForRentStatusItemID = ddlForRentStatus.SelectedValue.ToInt32();
            p.ForSaleRentBoard = chkForSaleRentBoard.Checked;
            p.ForSaleStatusItemID = ddlForSaleStatus.SelectedValue.ToInt32();
            p.MapReference = txtMapReference.Text.Trim();
            p.PropertyTypeItemID = ddlPropertyType.SelectedValue.ToInt32();
            p.TenureItemID = ddlTenure.SelectedValue.ToInt32();
            p.VendorAdditionalInfo = txtVendorAdditionalInfo.Text.Trim();
            p.VendorAddressLine1 = txtVendorAddressLine1.Text.Trim();
            p.VendorAddressLine2 = txtVendorAddressLine2.Text.Trim();
            p.VendorAddressLine3 = txtVendorAddressLine3.Text.Trim();
            p.VendorCountry = new Country { ID = ddlVendorCountry.SelectedValue.ToInt32() };
            p.VendorCounty = new County { ID = String.IsNullOrEmpty(hdnVendorCounty.Value) ? 0 : hdnVendorCounty.Value.ToInt32() };
            p.VendorEmail = txtVendorEmail.Text.Trim();
            p.VendorHowTheyHeard = txtVendorHowTheyHeard.Text.Trim();
            p.VendorKeysHeldViewingInfo = txtVendorKeysHeldViewingInfo.Text.Trim();
            p.VendorNextPurchase = txtVendorNextPurchase.Text.Trim();
            p.VendorPhoneHome = txtVendorHomePhone.Text.Trim();
            p.VendorPhoneMobile = txtVendorMobilePhone.Text.Trim();
            p.VendorPhoneWork = txtVendorWorkPhone.Text.Trim();
            p.VendorPostcode = txtVendorPostcode.Text.Trim();
            p.VendorReasonForRemoving = txtReasonForRemoving.Text.Trim();
            p.VendorReasonForSellingRent = txtReasonForSellingRenting.Text.Trim();
            p.VendorRestrictions = txtVendorRestrictions.Text.Trim();
            p.VendorsName = txtVendorsName.Text.Trim();
            p.AttachmentFileName = txtExistingAttachment.Text.Trim();
            p.AdditionalDescription = txtAdditionalDescription.Text.Trim();
            p.ForRentCurrencyItemID = ddlRentalCurrency.SelectedValue.ToInt32();
            p.ForSaleCurrencyItemID = ddlPriceCurrency.SelectedValue.ToInt32();
            p.RentalTermsItemID = ddlRentalTerms.SelectedValue.ToInt32();

            //Upload attachment
            string attachmentFilename = null;

            if (fuAttachment1.PostedFile != null && fuAttachment1.PostedFile.FileName != String.Empty)
            {
                string pathfilename = fuAttachment1.PostedFile.FileName;
                string filename = Path.GetFileName(fuAttachment1.PostedFile.FileName);
                string localpath = FluentContentSettings.DocumentUploadLocation + "\\";
                string localfilename = FluentContentSettings.DocumentUploadLocation + "\\" + filename;
                string filenameleft = Path.GetFileNameWithoutExtension(filename);
                string ext = Path.GetExtension(filename);
                if (chkOverwriteFile.Checked)
                {
                    fuAttachment1.PostedFile.SaveAs(localfilename);
                }
                else
                {
                    if (File.Exists(@localfilename))
                    {
                        //Find next available filename
                        for (int i = 1; i <= 1000; i++)
                        {
                            if (!File.Exists(@filenameleft + i.ToString() + ext))
                            {
                                localfilename = localpath + filenameleft + i.ToString() + ext;
                                break;
                            }
                        }
                    }
                    fuAttachment1.PostedFile.SaveAs(localfilename);
                }

                attachmentFilename = Path.GetFileName(localfilename);
            }

            // delete file if necessary
            if (chkDeleteAttachment.Checked && !String.IsNullOrEmpty(p.AttachmentFileName))
            {
                string localfilename = FluentContentSettings.DocumentUploadLocation + "\\" + p.AttachmentFileName;
                try
                {
                    File.Delete(localfilename);
                }
                catch
                {
                    throw;
                }
            }

            // set attachment filename value
            p.AttachmentFileName = chkDeleteAttachment.Checked ? null : attachmentFilename;

            return p;
        }

        #endregion

        #region Paging

        private void PrepareNextPreviousButtons(ResultSet<Property> result)
        {
            string classForPrev = String.Empty;
            string classForNext = String.Empty;

            NameValueCollection collection = HttpUtility.ParseQueryString(Request.Url.Query);
            collection.Remove("pagesize");
            collection.Remove("page");
            string previousUrl = ResolveUrl(String.Format("~/property-management?page={0}&pagesize={1}{2}", PageNumber - 1, PageSize, collection.Count > 0 ? ("&" + collection.ToQueryString(false)) : String.Empty));
            string nextUrl = ResolveUrl(String.Format("~/property-management?page={0}&pagesize={1}{2}", PageNumber + 1, PageSize, collection.Count > 0 ? ("&" + collection.ToQueryString(false)) : String.Empty));

            if ((result.TotalPages <= 1) ||
                (result.TotalPages == 1 && PageNumber == 1))
            {
                // disable next and prev
                classForPrev = "deactive";
                classForNext = "deactive";
                previousUrl = String.Empty;
                nextUrl = String.Empty;
            }

            if (result.TotalPages > 1)
            {
                if (PageNumber == 1)
                {
                    // disable prev
                    classForPrev = "deactive";
                    previousUrl = String.Empty;

                    // enable next
                    classForNext = String.Empty;
                }
                else
                {
                    if (PageNumber == result.TotalPages)
                    {
                        // enable prev
                        classForPrev = String.Empty;

                        // disable next
                        classForNext = "deactive";
                        nextUrl = String.Empty;
                    }

                }

            }
            if (previousUrl.Length == 0)
            {
                litTopPrev.Text = "<li id=\"editable_previous\" class=\"paginate_button previous disabled\"> <a href=\"#\" aria-controls=\"editable\" data-dt-idx=\"0\" tabindex=\"0\">Previous</a></li>";
                litBottomPrev.Text = litTopPrev.Text;
            }
            else
            {
                litTopPrev.Text = "<li id=\"editable_previous\" class=\"paginate_button previous\">";
                litTopPrev.Text += String.Format("<a href='{0}' aria-controls=\"editable\" data-dt-idx=\"\0\" tabindex=\"0\">Previous</a>", previousUrl);
                litTopPrev.Text += "</li>";
                litBottomPrev.Text = litTopPrev.Text;
            }


            if (nextUrl.Length == 0)
            {
                litTopNext.Text = "<li id=\"editable_next\" class=\"paginate_button next disabled\"> <a href=\"#\" aria-controls=\"editable\" data-dt-idx=\"0\" tabindex=\"0\">Next</a></li>";
                litBottomNext.Text = litTopNext.Text;
            }
            else
            {
                litTopNext.Text += "<li id=\"editable_previous\" class=\"paginate_button next\">";
                litTopNext.Text += String.Format("<a href='{0}' aria-controls=\"editable\" data-dt-idx=\"\0\" tabindex=\"0\">Next</a>", nextUrl);
                litTopNext.Text += "</li>";
                litBottomNext.Text = litTopNext.Text;
            }
        }

        public string GetPageLink(object pageNumber)
        {
            NameValueCollection collection = HttpUtility.ParseQueryString(Request.Url.Query);
            collection.Remove("pagesize");
            collection.Remove("page");
            return ResolveUrl(String.Format("~/property-management?page={0}&pagesize={1}{2}",
                pageNumber, PageSize, collection.Count > 0 ? ("&" + collection.ToQueryString(false)) : String.Empty));
        }

        public string GetPagerActiveClass(object pageNumber, string cssClass)
        {
            int page = Convert.ToInt32(pageNumber);
            if (page == PageNumber)
                return String.Format("class={0}", cssClass);
            return "paginate_button";
        }

        #endregion

        #region Asset Methods

        protected void btnSaveAsset_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;
            PropertyAsset a = new PropertyAsset();
            a.AssetItemID = ddlAssetType.SelectedValue.ToInt32();
            a.Description = txtAssetDescription.Text.Trim();
            a.FloorNumber = txtAssetOnFloorNumber.Text.Trim() == String.Empty ? 0 : txtAssetOnFloorNumber.Text.Trim().ToInt32();
            a.Length = txtAssetLengthInMetres.Text.Trim() == String.Empty ? 0 : Convert.ToDouble(txtAssetLengthInMetres.Text.Trim());
            a.Width = txtAssetWidthInMetres.Text.Trim() == String.Empty ? 0 : Convert.ToDouble(txtAssetWidthInMetres.Text.Trim());
            a.Size = txtAssetSizeInMetresSquared.Text.Trim() == String.Empty ? 0 : Convert.ToDouble(txtAssetSizeInMetresSquared.Text.Trim());
            a.PropertyID = PropertyID;

            a = DAC_PropertyAsset.Insert(a);
            Response.Redirect(ResolveUrl(String.Format("~/property-management/{0}?action=assetcreated#tabs-2", PropertyID)));
        }

        protected void lvAssets_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item is ListViewDataItem)
            {
                DropDownList ddlAssetType = e.Item.FindControl("ddlAssetType") as DropDownList;
                ddlAssetType.DataSource = ItemHelper.LoadItemDisplay("Assets");
                ddlAssetType.DataTextField = "chrItemTitle";
                ddlAssetType.DataValueField = "intItemID";
                ddlAssetType.DataBind();

                // set dropdown value
                string selectedValue = lvAssets.DataKeys[e.Item.DisplayIndex]["AssetItemID"].ToString();
                ddlAssetType.SelectedValue = selectedValue;
            }
        }

        protected void lvAssets_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.CommandName == "UpdateAsset")
            {
                DropDownList ddlAssetType = e.Item.FindControl("ddlAssetType") as DropDownList;
                TextBox txtAssetLengthInMetres = e.Item.FindControl("txtAssetLengthInMetres") as TextBox;
                TextBox txtAssetWidthInMetres = e.Item.FindControl("txtAssetWidthInMetres") as TextBox;
                TextBox txtAssetSizeInMetresSquared = e.Item.FindControl("txtAssetSizeInMetresSquared") as TextBox;
                TextBox txtAssetOnFloorNumber = e.Item.FindControl("txtAssetOnFloorNumber") as TextBox;
                TextBox txtAssetDescription = e.Item.FindControl("txtAssetDescription") as TextBox;

                int assetID = Convert.ToInt32(e.CommandArgument);
                int propertyID = Convert.ToInt32(lvAssets.DataKeys[e.Item.DisplayIndex]["PropertyID"]);

                PropertyAsset a = new PropertyAsset();
                a.AssetItemID = ddlAssetType.SelectedValue.ToInt32();
                a.Description = txtAssetDescription.Text.Trim();
                a.FloorNumber = txtAssetOnFloorNumber.Text.Trim() == String.Empty ? 0 : txtAssetOnFloorNumber.Text.Trim().ToInt32();
                a.Length = txtAssetLengthInMetres.Text.Trim() == String.Empty ? 0 : Convert.ToDouble(txtAssetLengthInMetres.Text.Trim());
                a.Width = txtAssetWidthInMetres.Text.Trim() == String.Empty ? 0 : Convert.ToDouble(txtAssetWidthInMetres.Text.Trim());
                a.Size = txtAssetSizeInMetresSquared.Text.Trim() == String.Empty ? 0 : Convert.ToDouble(txtAssetSizeInMetresSquared.Text.Trim());
                a.PropertyID = propertyID;
                a.ID = assetID;

                a = DAC_PropertyAsset.Update(a);
                Response.Redirect(ResolveUrl(String.Format("~/property-management/{0}?action=assetupdated#tabs-2", propertyID)));

            }

            if (e.CommandName == "DeleteAsset")
            {
                int assetID = Convert.ToInt32(e.CommandArgument);
                DAC_PropertyAsset.Delete(assetID);
                Response.Redirect(ResolveUrl(String.Format("~/property-management/{0}?action=assetdeleted#tabs-2", PropertyID)));
            }
        }
        #endregion

        #region Images Methods

        protected void btnImageUpload_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;

            PropertyImage image = new PropertyImage();
            image.Description = txtImageDescription.Text.Trim();
            string imageGUID = Guid.NewGuid().ToString().Replace("-", String.Empty);
            image.Filename = String.Format("{0}.jpg", imageGUID);
            image.GUID = imageGUID;
            image.IsLeadImage = chkImageAsLead.Checked;
            image.PropertyID = PropertyID;

            byte[] fileBytes = fuImageUpload.FileBytes;
            using (System.Drawing.Image i = System.Drawing.Image.FromStream(new MemoryStream(fileBytes)))
            {
                int height = i.Height;
                int width = i.Width;
                bool imageIsSquare = height == width;
                bool imageIsLandscape = width > height;

                // validate dimensions
                if ((imageIsSquare && width < DAC_PropertyImage.MinLandscapeImageWidth) ||
                    (imageIsLandscape && width < DAC_PropertyImage.MinLandscapeImageWidth))
                {
                    this.Page.InsertError(String.Format("Image must be at least {0}px wide.", DAC_PropertyImage.MinLandscapeImageWidth), "PropertyGroupImagesServer");
                    return;
                }

                if (!imageIsLandscape && height < DAC_PropertyImage.MinPortraitImageHeight)
                {
                    this.Page.InsertError(String.Format("Image must be at least {0}px tall.", DAC_PropertyImage.MinPortraitImageHeight), "PropertyGroupImagesServer");
                    return;
                }


                System.Drawing.Image newImage = i;

                if (width > DAC_PropertyImage.MaxImageWidth || height > DAC_PropertyImage.MaxImageHeight)
                {
                    newImage = Common.FileUtils.Images.ImageManager.ResizeFixed(i, DAC_PropertyImage.MaxImageWidth, DAC_PropertyImage.MaxImageHeight);
                }

                //string imagePath = @"uploaded\images\properties\" + image.Filename;
                string imagePath = Path.Combine(FluentContentSettings.ImageUploadLocation, "properties", image.Filename);
                try
                {
                    //newImage.Save(Server.MapPath(imagePath), ImageFormat.Jpeg);
                    newImage.Save(imagePath, ImageFormat.Jpeg);

                }
                catch
                {
                    this.Page.InsertError("Failed to save image, please try again", "PropertyGroupImagesServer");
                    return;
                }

                image.Width = newImage.Width;
                image.Height = newImage.Height;

                image = DAC_PropertyImage.Insert(image);
                Response.Redirect(ResolveUrl(String.Format("~/property-management/{0}?action=imagecreated#tabs-4", PropertyID)));
            }
        }

        protected void btnImageSave_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;

            PropertyImage image = DAC_PropertyImage.GetByGUID(ImageGUID);
            if (image == null)
            {
                this.Page.InsertError("Error occured when saving image", "PropertyGroupImagesServer");
                return;
            }

            image.Description = txtImageDescription.Text.Trim();
            image.IsLeadImage = chkImageAsLead.Checked;

            DAC_PropertyImage.Update(image);
            Response.Redirect(ResolveUrl(String.Format("~/property-management/{0}?action=imageupdated#tabs-4", image.PropertyID)));
        }

        protected void btnDeleteImage_Click(object sender, EventArgs e)
        {
            Button deleteButton = sender as Button;
            int id = Convert.ToInt32(deleteButton.CommandArgument);
            if (id > 0)
            {
                PropertyImage image = DAC_PropertyImage.Get(id);
                if (image != null)
                {
                    string filePath = Path.Combine(FluentContentSettings.ImageUploadLocation, image.Filename);
                    File.Delete(filePath);
                    DAC_PropertyImage.Delete(id);
                    Response.Redirect(ResolveUrl(String.Format("~/property-management/{0}?action=imagedeleted#tab-4", image.PropertyID)));
                }
            }
        }

        #endregion

        #region Attribute Methods

        protected void btnSaveAttribute_Click(object sender, EventArgs e)
        {
            string value = null;

            PropertyAttributeType type = DAC_PropertyAttributeType.Get(ddlAttributeType.SelectedValue.ToInt32());

            if (type == null)
                Response.Redirect("~/property-management/" + PropertyID + "#tabs-3");

            switch (type.DataType)
            {
                case "Boolean":
                    value = ddlAttributeBoolValue.SelectedValue;
                    break;
                case "String":
                    value = hdnTextAttributeStringValue.Value.Trim(); //txtAttributeStringValue.Text.Trim();
                    break;
                case "Date":
                    value = txtAttributeDateValue.Text.Trim();
                    break;
                case "Numeric":
                    value = txtAttributeNumericValue.Text.Trim();
                    break;
                default:
                    break;
            }

            PropertyAttribute a = new PropertyAttribute
            {
                AttributeTypeItemID = ddlAttributeType.SelectedValue.ToInt32(),
                PropertyID = PropertyID,
                Value = value
            };

            DAC_PropertyAttribute.Insert(a);
            Response.Redirect("~/property-management/" + PropertyID + "?action=attributecreated#tabs-3");
        }

        protected void btnDeleteAttribute_Click(object sender, EventArgs e)
        {
            int index = ((Button)sender).CommandArgument.ToInt32();

            int attributeTypeItemID = lvAttributes.DataKeys[index]["AttributeTypeItemID"].ToInt32();
            int attributeItemID = lvAttributes.DataKeys[index]["ID"].ToInt32();

            PropertyAttribute att = new PropertyAttribute
            {
                ID = attributeItemID,
                AttributeTypeItemID = attributeTypeItemID,
                PropertyID = PropertyID
            };

            DAC_PropertyAttribute.Delete(att);
            Response.Redirect("~/property-management/" + PropertyID + "?action=attributedeleted#tabs-3");
        }

        protected void btnSaveAttributeList_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid)
                Response.Redirect("~/property-management/" + PropertyID + "#tabs-3");

            List<PropertyAttribute> items = new List<PropertyAttribute>();

            // read items
            foreach (ListViewDataItem item in lvAttributes.Items)
            {
                TextBox txtAttributeValueList = item.FindControl("txtAttributeValueList") as TextBox;
                DropDownList ddlAttribtueValueList = item.FindControl("ddlAttribtueValueList") as DropDownList;
                string dataType = lvAttributes.DataKeys[item.DataItemIndex]["AttributeTypeItemDataType"].ToString();
                int attributeTypeItemID = lvAttributes.DataKeys[item.DataItemIndex]["AttributeTypeItemID"].ToInt32();
                int attributeID = lvAttributes.DataKeys[item.DataItemIndex]["ID"].ToInt32();

                items.Add(new PropertyAttribute
                {
                    Value = dataType == "Boolean" ? ddlAttribtueValueList.SelectedValue : txtAttributeValueList.Text.Trim(),
                    PropertyID = PropertyID,
                    AttributeTypeItemID = attributeTypeItemID,
                    ID = attributeID
                });
            }

            DAC_PropertyAttribute.UpdateAll(items);
            Response.Redirect("~/property-management/" + PropertyID + "?action=attributesupdated#tabs-3");
        }

        #endregion

        #region PropertyDiaryEntry

        protected void LinkButton_CreateDiary_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/PropertyDiary.aspx?pID=" + Request.QueryString["propertyID"].ToString());
        }

        protected void Button_Delete_Command(object sender, CommandEventArgs e)
        {
            try
            {
                if (!Page.IsValid)
                    Response.Redirect("~/property-management/" + Request.QueryString["propertyID"].ToString() + "#tabs-5");
                DAC_PropertyDiaryEntry.Delete(e.CommandArgument.ToInt32());

                Response.Redirect("~/property-management/" + PropertyID + "?action=DiaryEntrydeleted#tabs-5");
                //GridView_tPropertyDiaryEntry.DataBind();
            }
            catch (Exception er)
            {
                throw new Exception(er.Message);
            }

        }

        protected void Button_Print_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/PropertyDiaryPrint.aspx?pID=" + Request.QueryString["propertyID"].ToString());
        }

        #endregion

        #region PropertyViewing

        protected void Button_AddPropertyViewing_Click(object sender, EventArgs e)
        {
            try
            {
                //                Label_errMsg.Text = "";
                if (!Page.IsValid)
                    Response.Redirect("~/property-management/" + Request.QueryString["propertyID"].ToString() + "#tabs-6");

                #region Check date
                DateTime DateTimeNow = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0, 0);
                string[] datViewing = TextBox_datViewing.Text.Split('/', '\\');
                DateTime SelectDateTextBox = new DateTime(int.Parse(datViewing[2]), int.Parse(datViewing[1]), int.Parse(datViewing[0]), 0, 0, 0, 0);
                if (DateTime.Compare(SelectDateTextBox, DateTimeNow) == -1)
                {
                    Response.Redirect("~/property-management/" + PropertyID + "?action=OpenViewingIncorrectDateSelected#tabs-6");
                }

                #endregion

                #region Check Drop downs value

                string[] DropDownTimeFrom = DropDownList_timTimeFrom.Text.Split(':');
                string[] DropDownTimeTo = DropDownList_timTimeTo.SelectedValue.Split(':');
                int DateTimeDropDownCompareResult = DateTime.Compare((new DateTime(2000, 1, 1, Int32.Parse(DropDownTimeFrom[0]), Int32.Parse(DropDownTimeFrom[1]), 0)),
     (new DateTime(2000, 1, 1, Int32.Parse(DropDownTimeTo[0]), Int32.Parse(DropDownTimeTo[1]), 0))
     );
                #endregion


                if (DateTimeDropDownCompareResult == 1 || DateTimeDropDownCompareResult == 0)
                {
                    Response.Redirect("~/property-management/" + PropertyID + "?action=OpenViewingEqualFromTo#tabs-6");
                }
                else
                {
                    #region tPropertyViewing

                    PropertyViewing PropertyViewing = new PropertyViewing();
                    PropertyViewing.intPropertyID = Request.QueryString["propertyID"].ToInt32();
                    PropertyViewing.datViewing = DateTime.Parse(TextBox_datViewing.Text);
                    PropertyViewing.timTimeFrom = DropDownList_timTimeFrom.SelectedValue;
                    PropertyViewing.timTimeTo = DropDownList_timTimeTo.SelectedValue;

                    #region Check for unique
                    bool isExist = false;
                    foreach (GridViewRow _row in GridView_PropertyViewing.Rows)
                    {
                        string[] Label_timTimeTo = ((Label)_row.FindControl("Label_timTimeTo")).Text.Split(':');


                        //new DateTime(2000,1,1,TimeFromDropDownList[0],TimeFromDropDownList[1],0)
                        //new DateTime(2000,1,1,TimeFromGrid[0],TimeFromGrid[1],0)
                        int DateTimeReusultCompare = DateTime.Compare((new DateTime(2000, 1, 1, Int32.Parse(DropDownTimeFrom[0]), Int32.Parse(DropDownTimeFrom[1]), 0)),
                             (new DateTime(2000, 1, 1, Int32.Parse(Label_timTimeTo[0]), Int32.Parse(Label_timTimeTo[1]), 0))
                             );
                        if (
                            (DateTimeReusultCompare == -1)
                            &&
                            (DateTime.Parse(((HiddenField)_row.FindControl("HiddenField_datViewing")).Value).ToShortDateString() == PropertyViewing.datViewing.ToShortDateString())
                           )
                        {
                            Response.Redirect("~/property-management/" + PropertyID + "?action=OpenViewingExist#tabs-6");
                            isExist = true;
                            break;
                        }
                    }

                    #endregion
                    if (!isExist)
                    {
                        DAC_PropertyViewing.Insert(PropertyViewing);
                        Response.Redirect("~/property-management/" + PropertyID + "?action=OpenViewingcreated#tabs-6");
                    }

                    #endregion

                }
            }
            catch (Exception er)
            {
                throw new Exception(er.Message);
            }
        }

        protected void Button_DeletePropertyViewing_Command(object sender, CommandEventArgs e)
        {
            try
            {
                if (!Page.IsValid)
                    Response.Redirect("~/property-management/" + Request.QueryString["propertyID"].ToString() + "#tabs-6");
                DAC_PropertyViewing.Delete(e.CommandArgument.ToInt32());
                Response.Redirect("~/property-management/" + PropertyID + "?action=OpenViewingdeleted#tabs-6");
                //GridView_PropertyViewing.DataBind();
            }
            catch (Exception er)
            {
                throw new Exception(er.Message);
            }

        }

        protected void DropDownList_timTimeFrom_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] TimeFrom = DropDownList_timTimeFrom.SelectedValue.Split(':');
                TimeFrom[0] = ((TimeFrom[0].ToInt32()) + 1).ToString();
                string TODefaultValue = (TimeFrom[0].Length < 2 ? "0" + TimeFrom[0] : TimeFrom[0]) + ":" + TimeFrom[1];
                DropDownList_timTimeTo.SelectedIndex = DropDownList_timTimeTo.Items.IndexOf(
                    DropDownList_timTimeTo.Items.FindByText(TODefaultValue));

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        #endregion
    }
}