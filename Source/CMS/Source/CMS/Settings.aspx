﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CmsMaster.Master" AutoEventWireup="true"
    CodeBehind="Settings.aspx.cs" Inherits="CMS.Settings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="placeholderHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="placeholderBody" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Website Settings</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="~/Home.aspx" runat="server">Home</a>
                </li>
                <li class="action">
                    <strong>Setting</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
        </div>
    </div>
    <div class="errorSummary hidden" validationgroup="Settings">
        <p>
            Please ensure all fields are in the correct format
        </p>
    </div>
    <asp:ValidationSummary ID="vsErrorSummary" runat="server" ValidationGroup="SettingsServer"
        CssClass="errorSummary" />
    <%
        if (Request.GetQueryString<string>("info", String.Empty) == "saveok")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "showMessage('Settings saved successfully!','success');", true);
        }
    %>
    <div class="wrapper wrapper-content animated fadeInRight">

        <%--    <p>
                <label>
                    VAT Rate: &#37;
                </label>
                <asp:TextBox ID="txtSettingsVatRate" runat="server" CssClass="digits"></asp:TextBox>
                <asp:RegularExpressionValidator ControlToValidate="txtSettingsVatRate" ValidationExpression="^\d*\.?\d*$"
                    Display="None" runat="server" ErrorMessage="Invalid number" ValidationGroup="Settings"></asp:RegularExpressionValidator>
                <span class="hint">leave blank for 0&#37;</span>
            </p>          
            <p>
                <asp:Button ID="btnSave" runat="server" Text="Update" ValidationGroup="Settings"
                    OnClick="btnSave_Click" />
            </p>
        --%>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Edit Setting</h5>
                        <div class="ibox-tools">
                           
                        </div>
                    </div>
                    <div class="ibox-content" style="display: block;">
                        <asp:ListView ID="lvItems" runat="server" DataKeyNames="Key">
                            <LayoutTemplate>
                                <div class="form-horizontal">
                                    <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-2">
                                            <asp:Button ID="btnSaveSettings" runat="server" CssClass="btn btn-primary" Text="Save Settings" OnClick="btnSaveSettings_Click" />
                                        </div>
                                    </div>
                                </div>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">
                                        <%#Eval("Label") %>
                                    </label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="txtSettingValue" runat="server" CssClass="form-control" Text='<%#Eval("Value") %>'></asp:TextBox>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                            </ItemTemplate>
                            <EmptyDataTemplate>
                                <p>
                                    No settings available
                                </p>
                            </EmptyDataTemplate>
                        </asp:ListView>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
