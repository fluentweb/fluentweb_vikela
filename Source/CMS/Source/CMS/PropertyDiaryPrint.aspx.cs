﻿using FluentContent.DAC;
using FluentContent.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMS
{
    public partial class PropertyDiaryPrint : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Property property = DAC_Property.Get(int.Parse(Request.QueryString["pId"].ToString()));
                Label_PropertyId.Text = "Ref.No. " + property.PropertyCode;
                Label_PropertyName.Text = property.Title;

            }
        }

        protected void Button_Cancel_Click(object sender, EventArgs e)
        {
            string PropertyID = Request.QueryString["pId"].ToString();
            Response.Redirect("~/property-management/" + PropertyID + "#tabs-5");
        }
    }
}