﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.Helpers.Interfaces;
using CMS.Helpers.Entities;
using CMS.Helpers;
using FluentContent.DAC;
using Common.Session;
using FluentContent.Entities;
using FluentContent.Entities.Enums;
using FluentContent.Helpers;

namespace CMS
{
    public partial class CmsMaster : System.Web.UI.MasterPage
    {
        // Properties

        public CMSUser UserSession { get { return SessionObjects.GetSession<CMSUser>(SessionKeys.CMS_USER_SESSION); } }

        private List<FluentContent.Entities.ItemType> ItemTypes { get; set; }

        public List<string> HiddenMenuKeys { get; set; }

        public string CurrentMenuURL { get; set; }

        // Page events

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (SessionObjects.GetSession<CMSUser>(SessionKeys.CMS_USER_SESSION) == null)
                Response.Redirect("~/Login.aspx?info=expired");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            form1.Action = Request.RawUrl;
            LoadHiddenMenuKeys();
            CurrentMenu();
            if (!IsPostBack)
            {
                LoadMenuLocations();
                LoadItemTypes();
                LoadDisplaySettings();
                LoadEntityTypes();
                LoadFormTypes();
            }
        }

        private void LoadHiddenMenuKeys()
        {
            HiddenMenuKeys = MenuVisibility.GetAll();
        }



        protected override void Render(HtmlTextWriter writer)
        {
            // overrides standard asp.net validation
            string scriptUrl = ResolveUrl("~/scripts/validators.js");
            string validatorOverride = String.Format("<script src='{0}' type='text/javascript'></script>", scriptUrl);
            Page.ClientScript.RegisterStartupScript(this.GetType(), "ValidatorOverrideScripts", validatorOverride, false);
            base.Render(writer);
        }

        private void LoadFormTypes()
        {
            lvForms.DataSource = DAC_FormType.GetFormsTypes();
            lvForms.DataBind();
        }
        private void LoadMenuLocations()
        {
            lvMenuLocations.DataSource = DAC_MenuLocation.Get();
            lvMenuLocations.DataBind();
        }

        private void LoadItemTypes()
        {
            ItemTypes = DAC_ItemType.Get(UserSession.Role.CMSAccessLevel == CMSAccessLevel.Full);
            lvItemTypes.DataSource = ItemTypes;
            lvItemTypes.DataBind();
        }

        private void LoadDisplaySettings()
        {
            lvDisplaySettings.DataSource = DAC_DisplaySetting.Get();
            lvDisplaySettings.DataBind();
        }


        private void LoadEntityTypes()
        {
            lvEntityTypes.DataSource = DAC_EntityType.Get();
            lvEntityTypes.DataBind();
        }

        public bool IsLastItem(int index)
        {
            return (index == ItemTypes.Count - 1);
        }

        private void CurrentMenu()
        {
            CurrentMenuURL = GetCurrentPageName();
        }
        public string GetCurrentPageName()
        {
            string sPath = Request.Url.AbsolutePath;
            System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
            string sRet = oInfo.Name;
            return sRet;
        }
    }
}
