﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PropertyDiaryPrint.aspx.cs" Inherits="CMS.PropertyDiaryPrint" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        body {
            color: black;
            margin: 0;
            padding: 0;
        }

        table tr td {
            font-size: 12px;
        }

        /*@media print {

            body {
                display: none;
                margin: 0;
                padding: 0;
                color: black;
            }

                body .printable {
                    display: block;
                }

            table tr td {
                font-size: 2pt !important;
            }

            .ContentTable {
                width: 100%;
            }
        }*/
        @media print {
            .no-print, .no-print * {
                display: none !important;
            }
        }
    </style>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
    <script language="javascript" type="text/javascript">

        function PrintPage() {

            //var printContent = document.getElementById('pnlGridView');

            //   var printWindow = window.open("All Records", "Print Panel", 'left=50000,top=50000,width=0,height=0');

            //   printWindow.document.write(printContent.innerHTML);

            //   printWindow.document.close();




        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="Printable">


            <div id="pnlGridView" class="ContentTable">

                <h2>

                    <asp:Label Text="" ID="Label_PropertyName" runat="server" />
                    &nbsp;
          <asp:Label Text="" ID="Label_PropertyId" runat="server" />


                </h2>
                <asp:GridView runat="server" ID="GridView_tPropertyDiaryEntry" Width="100%" DataKeyNames="intDiaryEntry" DataSourceID="SqlDataSource_tPropertyDiaryEntry" AutoGenerateColumns="False" GridLines="None" Font-Names="Arial,Helvetica,sans-serif" Font-Size="12px" AllowSorting="True" BackColor="#ececec" AllowPaging="false">
                    <RowStyle BorderStyle="None" />
                    <EmptyDataTemplate>
                        <center>No data available...</center>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <table width="100%" class="itemTable" style="margin: 0 !important;">
                                    <thead>
                                        <tr>
                                            <th style="width: 20%; text-align: left; font-size: 12px">Date Entry</td>
                                                        <th style="width: 20%; text-align: left; font-size: 12px">User</td>
                                                        <th style="width: 50%; text-align: left; font-size: 12px">
                                            Notes</td>
                                                      
                                        </tr>
                                    </thead>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <table width="100%" class="itemTable" style="margin: 0 !important;">
                                    <tr>
                                        <td style="width: 20%; text-align: left; font-size: 12px">
                                            <asp:Label CssClass="" runat="server" ID="Label_datEntry" Text='<%# String.Format("{0:ddd, dd-MMM-yyyy HH:mm}",Eval("datEntry")) %>'></asp:Label></td>
                                        <td style="width: 20%; text-align: left; font-size: 12px">
                                            <asp:Label CssClass="" runat="server" ID="Label_chrUser" Text='<%# Eval("chrUser") %>'></asp:Label></td>
                                        <td style="width: 50%; text-align: left; font-size: 12px">
                                            <asp:Label CssClass="" runat="server" ID="Label_chrNotes" Text='<%# Eval("chrNotes") %>'></asp:Label></td>


                                    </tr>

                                </table>
                            </ItemTemplate>

                        </asp:TemplateField>

                    </Columns>

                    <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle BackColor="#66a89f" Font-Bold="True" ForeColor="Black" />
                    <AlternatingRowStyle BackColor="White" BorderStyle="None" />
                </asp:GridView>




                <asp:SqlDataSource ID="SqlDataSource_tPropertyDiaryEntry" runat="server" SelectCommand=" SELECT [intDiaryEntry]
,[datEntry]
,[chrUser]
,[chrNotes]
,[intPropertyID]
FROM [tPropertyDiaryEntry] WHERE intPropertyID = @propertyID order by datEntry desc"
                    ConnectionString="<%$ ConnectionStrings:SQLCONN %>">
                    <SelectParameters>
                        <asp:QueryStringParameter QueryStringField="pID" Name="propertyID" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </div>

        </div>
        <br />
        <center class="no-print">
     <%--<input id="btnSubmit" type="submit" value="Print" onclick="PrintPage();" />--%>
        <input type="button" value="Print" onclick="window.print()" />
        &nbsp;
       <asp:Button ID="button_Cancel" runat="server" OnClick="Button_Cancel_Click" Text="Cancel" CssClass="" /><br />


        </center>
    </form>
</body>
</html>
