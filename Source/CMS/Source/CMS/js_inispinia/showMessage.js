﻿function showMessage(MessageText, messageType) {
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "progressBar": true,
        "preventDuplicates": false,
        "positionClass": "toast-top-full-width",
        "onclick": null,
        "showDuration": "400",
        "hideDuration": "1000",
        "timeOut": "7000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
    switch (messageType) {
        case 'error':
            toastr.error(MessageText);
            break;
        case 'warning':
            toastr.warning(MessageText);
            break;
        case 'info':
            toastr.info(MessageText);
            break;
        case 'success':
            toastr.success(MessageText);
            break;
    }
}
