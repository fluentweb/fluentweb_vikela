using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Delete
{
    public class ItemDelete : DataAccessBase
    {
        private Item _item;

        public ItemDelete()
        {
            StoredProcedureName = StoredProcedure.Name.spItem_Delete.ToString();
        }

        public void Delete()
        {
            ItemDeleteParameters _itemdeleteparameters = new ItemDeleteParameters(Item);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            dbhelper.Parameters = _itemdeleteparameters.Parameters;
            dbhelper.Run();
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }
    }


    public class ItemDeleteParameters
    {
        private Item _item;
        private SqlParameter[] _parameters;

        public ItemDeleteParameters(Item item)
        {
            Item = item;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@ItemID" , _item.ItemID),
            };
            Parameters = parameters;
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

