using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Delete
{
    public class LinkDelete : DataAccessBase
    {
        private Link _link;

        public LinkDelete()
        {
            StoredProcedureName = StoredProcedure.Name.spLink_Delete.ToString();
        }

        public void Delete()
        {
            LinkDeleteParameters _linkdeleteparameters = new LinkDeleteParameters(Link);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            dbhelper.Parameters = _linkdeleteparameters.Parameters;
            dbhelper.Run();
        }

        public Link Link
        {
            get { return _link; }
            set { _link = value; }
        }
    }


    public class LinkDeleteParameters
    {
        private Link _link;
        private SqlParameter[] _parameters;

        public LinkDeleteParameters(Link link)
        {
            Link = link;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@LinkID" , _link.LinkID),
            };
            Parameters = parameters;
        }

        public Link Link
        {
            get { return _link; }
            set { _link = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

