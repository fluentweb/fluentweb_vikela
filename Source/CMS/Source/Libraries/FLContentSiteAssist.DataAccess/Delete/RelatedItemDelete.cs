using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Delete
{
    public class RelatedItemDelete : DataAccessBase
    {
        private RelatedItem _relateditem;

        public RelatedItemDelete()
        {
            StoredProcedureName = StoredProcedure.Name.spRelatedItem_Delete.ToString();
        }

        public void Delete()
        {
            RelatedItemDeleteParameters _relateditemdeleteparameters = new RelatedItemDeleteParameters(RelatedItem);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            dbhelper.Parameters = _relateditemdeleteparameters.Parameters;
            dbhelper.Run();
        }

        public RelatedItem RelatedItem
        {
            get { return _relateditem; }
            set { _relateditem = value; }
        }
    }


    public class RelatedItemDeleteParameters
    {
        private RelatedItem _relateditem;
        private SqlParameter[] _parameters;

        public RelatedItemDeleteParameters(RelatedItem relateditem)
        {
            RelatedItem = relateditem;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@RelationshipID" , _relateditem.RelationshipID),
            };
            Parameters = parameters;
        }

        public RelatedItem RelatedItem
        {
            get { return _relateditem; }
            set { _relateditem = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

