using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Delete
{
    public class PageDelete : DataAccessBase
    {
        private Page _page;

        public PageDelete()
        {
            StoredProcedureName = StoredProcedure.Name.spPage_Delete.ToString();
        }

        public void Delete()
        {
            PageDeleteParameters _pagedeleteparameters = new PageDeleteParameters(Page);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            dbhelper.Parameters = _pagedeleteparameters.Parameters;
            dbhelper.Run();
        }

        public Page Page
        {
            get { return _page; }
            set { _page = value; }
        }
    }


    public class PageDeleteParameters
    {
        private Page _page;
        private SqlParameter[] _parameters;

        public PageDeleteParameters(Page page)
        {
            Page = page;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@PageID" , _page.PageID),
            };
            Parameters = parameters;
        }

        public Page Page
        {
            get { return _page; }
            set { _page = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

