using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Update
{
    public class ItemTextUpdate : DataAccessBase
    {
        private ItemText _itemtext;

        public ItemTextUpdate()
        {
            StoredProcedureName = StoredProcedure.Name.spItemText_Update.ToString();
        }

        public void Update()
        {
            ItemTextUpdateParameters _itemtexttextupdateparameters = new ItemTextUpdateParameters(ItemText);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            dbhelper.Parameters = _itemtexttextupdateparameters.Parameters;
            dbhelper.Run();
        }

        public ItemText ItemText
        {
            get { return _itemtext; }
            set { _itemtext = value; }
        }
    }


    public class ItemTextUpdateParameters
    {
        private ItemText _itemtext;
        private SqlParameter[] _parameters;

        public ItemTextUpdateParameters(ItemText itemtext)
        {
            ItemText = itemtext;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@ItemID" , _itemtext.ItemID),
                new SqlParameter( "@TextTypeID" , _itemtext.TextTypeID),
                new SqlParameter( "@TextTitle" , _itemtext.TextTitle),
                new SqlParameter( "@TextExtract" , _itemtext.TextExtract),
                new SqlParameter( "@Text" , _itemtext.Text),
            };
            Parameters = parameters;
        }

        public ItemText ItemText
        {
            get { return _itemtext; }
            set { _itemtext = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

