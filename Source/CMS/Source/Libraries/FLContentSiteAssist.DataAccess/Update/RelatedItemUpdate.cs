using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Update
{
    public class RelatedItemUpdate : DataAccessBase
    {
        private RelatedItem _relateditem;

        public RelatedItemUpdate()
        {
            StoredProcedureName = StoredProcedure.Name.spRelatedItem_Update.ToString();
        }

        public void Update()
        {
            RelatedItemUpdateParameters _itemtextupdateparameters = new RelatedItemUpdateParameters(RelatedItem);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            dbhelper.Parameters = _itemtextupdateparameters.Parameters;
            dbhelper.Run();
        }

        public RelatedItem RelatedItem
        {
            get { return _relateditem; }
            set { _relateditem = value; }
        }
    }


    public class RelatedItemUpdateParameters
    {
        private RelatedItem _relateditem;
        private SqlParameter[] _parameters;

        public RelatedItemUpdateParameters(RelatedItem relateditem)
        {
            RelatedItem = relateditem;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@Sequence" , _relateditem.Sequence),
                new SqlParameter( "@intItemRelateID" , _relateditem.RelationshipID),
            };
            Parameters = parameters;
        }

        public RelatedItem RelatedItem
        {
            get { return _relateditem; }
            set { _relateditem = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

