using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Update
{
    public class ItemCompleteUpdate : DataAccessBase
    {
        private Item _item;

        public ItemCompleteUpdate()
        {
            StoredProcedureName = StoredProcedure.Name.spItemComplete_Update.ToString();
        }

        public void Update()
        {
            ItemCompleteUpdateParameters _itemtextupdateparameters = new ItemCompleteUpdateParameters(Item);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            dbhelper.Parameters = _itemtextupdateparameters.Parameters;
            dbhelper.Run();
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }
    }


    public class ItemCompleteUpdateParameters
    {
        private Item _item;
        private SqlParameter[] _parameters;

        public ItemCompleteUpdateParameters(Item item)
        {
            Item = item;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@ItemID" , _item.ItemID),
                new SqlParameter( "@ItemTitle" , _item.Title),
                new SqlParameter( "@ItemURL" , _item.ItemURL),
                new SqlParameter( "@ImageURL" , _item.ImageURL),
                new SqlParameter( "@ImageCaption" , _item.ImageCaption),
                new SqlParameter( "@ImageAlt" , _item.ImageAlt),
                new SqlParameter( "@ImageTitle" , _item.ImageTitle),
                new SqlParameter( "@UserID" , _item.UserID),
                new SqlParameter( "@Sequence" , _item.Sequence),
                new SqlParameter( "@Enabled" , _item.Enabled),
                new SqlParameter( "@Link" , _item.Link),
                new SqlParameter( "@DeleteImage" , _item.DeleteImage),
                new SqlParameter( "@DateFr" , _item.DateFrom),
                new SqlParameter( "@DateTo" , _item.DateTo),
                new SqlParameter( "@ParentItemID" , _item.ParentItemID),
                new SqlParameter( "@TimeFrom" , _item.TimeFrom),
                new SqlParameter( "@TimeTo" , _item.TimeTo),
                new SqlParameter( "@Text1" , _item.Text1),
                new SqlParameter( "@Text2" , _item.Text2),
                new SqlParameter( "@Text3" , _item.Text3),
                new SqlParameter( "@Text4" , _item.Text4),
                new SqlParameter( "@Text5" , _item.Text5),
                new SqlParameter( "@Text6" , _item.Text6),
                new SqlParameter( "@Text7" , _item.Text7),
                new SqlParameter( "@Text8" , _item.Text8),
                new SqlParameter( "@Text9" , _item.Text9),
                new SqlParameter( "@Text10" , _item.Text10),
                new SqlParameter( "@Text11" , _item.Text11),
                new SqlParameter( "@Text12" , _item.Text12),
                new SqlParameter( "@Image2URL" , _item.Image2URL),
                new SqlParameter( "@Image2Caption" , _item.Image2Caption),
                new SqlParameter( "@Image2Alt" , _item.Image2Alt),
                new SqlParameter( "@Image2Title" , _item.Image2Title),
                new SqlParameter( "@FileURL" , _item.FileURL),
                new SqlParameter( "@FileTypeID" , _item.FileTypeID),
                new SqlParameter( "@File2URL" , _item.File2URL),
                new SqlParameter( "@File2TypeID" , _item.File2TypeID),
                new SqlParameter( "@DeleteImage2" , _item.DeleteImage2),
                new SqlParameter( "@DeleteFile" , _item.DeleteFile),
                new SqlParameter( "@DeleteFile2" , _item.DeleteFile2),
                new SqlParameter( "@Price1" , _item.price1),
                new SqlParameter( "@Price2" , _item.price2),
                new SqlParameter( "@Price3" , _item.price3),
                new SqlParameter( "@Value1" , _item.value1),
                new SqlParameter( "@Checkbox1", _item.Checkbox1),
                new SqlParameter( "@Checkbox2", _item.Checkbox2),
                new SqlParameter( "@Checkbox3", _item.Checkbox3),
                new SqlParameter( "@DataType", _item.DataType),
            };
            Parameters = parameters;
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

