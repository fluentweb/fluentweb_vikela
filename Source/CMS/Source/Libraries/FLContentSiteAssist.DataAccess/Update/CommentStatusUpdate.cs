using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Update
{
    public class CommentStatusUpdate : DataAccessBase
    {
        private Comment _comment;

        public CommentStatusUpdate()
        {
            StoredProcedureName = StoredProcedure.Name.spCommentStatus_Update.ToString();
        }

        public void Update()
        {
            CommentStatusUpdateParameters _commenttextupdateparameters = new CommentStatusUpdateParameters(Comment);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            dbhelper.Parameters = _commenttextupdateparameters.Parameters;
            dbhelper.Run();
        }

        public Comment Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }
    }


    public class CommentStatusUpdateParameters
    {
        private Comment _comment;
        private SqlParameter[] _parameters;

        public CommentStatusUpdateParameters(Comment comment)
        {
            Comment = comment;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@CommentID" , _comment.CommentID),
                new SqlParameter( "@Status" , _comment.Status),
            };
            Parameters = parameters;
        }

        public Comment Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

