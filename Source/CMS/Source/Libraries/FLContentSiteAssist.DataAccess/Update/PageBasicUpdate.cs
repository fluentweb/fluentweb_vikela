using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Update
{
    public class PageBasicUpdate : DataAccessBase
    {
        private Page _page;

        public PageBasicUpdate()
        {
            StoredProcedureName = StoredProcedure.Name.spPageBasic_Update.ToString();
        }

        public void Update()
        {
            PageBasicUpdateParameters _pageupdateparameters = new PageBasicUpdateParameters(Page);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            dbhelper.Parameters = _pageupdateparameters.Parameters;
            dbhelper.Run();
        }

        public Page Page
        {
            get { return _page; }
            set { _page = value; }
        }
    }


    public class PageBasicUpdateParameters
    {
        private Page _page;
        private SqlParameter[] _parameters;

        public PageBasicUpdateParameters(Page page)
        {
            Page = page;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@PageID" , _page.PageID),
                new SqlParameter( "@Name" , _page.PageName),
                new SqlParameter( "@URL" , _page.PageURL),
            };
            Parameters = parameters;
        }

        public Page Page
        {
            get { return _page; }
            set { _page = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

