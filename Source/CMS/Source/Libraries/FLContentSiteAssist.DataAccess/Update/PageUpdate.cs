using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Update
{
    public class PageUpdate : DataAccessBase
    {
        private Page _page;

        public PageUpdate()
        {
            StoredProcedureName = StoredProcedure.Name.spPage_Update.ToString();
        }

        public void Update()
        {
            PageUpdateParameters _pageupdateparameters = new PageUpdateParameters(Page);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            dbhelper.Parameters = _pageupdateparameters.Parameters;
            dbhelper.Run();
        }

        public Page Page
        {
            get { return _page; }
            set { _page = value; }
        }
    }


    public class PageUpdateParameters
    {
        private Page _page;
        private SqlParameter[] _parameters;

        public PageUpdateParameters(Page page)
        {
            Page = page;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@PageID" , _page.PageID),
                new SqlParameter( "@Name" , _page.PageName),
                new SqlParameter( "@Title" , _page.PageTitle),
                new SqlParameter( "@Keywords" , _page.PageKeywords),
                new SqlParameter( "@URL" , _page.PageURL),
                new SqlParameter( "@Description" , _page.PageDesc),
            };
            Parameters = parameters;
        }

        public Page Page
        {
            get { return _page; }
            set { _page = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

