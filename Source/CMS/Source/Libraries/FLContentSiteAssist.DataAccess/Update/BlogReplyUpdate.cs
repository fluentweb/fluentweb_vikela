using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Update
{
    public class BlogReplyUpdate : DataAccessBase
    {
        private Blog _blog;

        public BlogReplyUpdate()
        {
            StoredProcedureName = StoredProcedure.Name.spBlogReply_Update.ToString();
        }

        public void Update()
        {
            BlogReplyParameters _blogreplyparameters = new BlogReplyParameters(Blog);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            dbhelper.Parameters = _blogreplyparameters.Parameters;
            dbhelper.Run();
        }

        public Blog Blog
        {
            get { return _blog; }
            set { _blog = value; }
        }
    }


    public class BlogReplyParameters
    {
        private Blog _blog;
        private SqlParameter[] _parameters;

        public BlogReplyParameters(Blog blog)
        {
            Blog = blog;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@ReplyID" , _blog.ReplyID),
                new SqlParameter( "@Comment" , _blog.Comment),
                new SqlParameter( "@Status" , _blog.Status),
            };
            Parameters = parameters;
        }

        public Blog Blog
        {
            get { return _blog; }
            set { _blog = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

