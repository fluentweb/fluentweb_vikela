using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Update
{
    public class BlogReplyDisplayUpdate : DataAccessBase
    {
        private Blog _blog;

        public BlogReplyDisplayUpdate()
        {
            StoredProcedureName = StoredProcedure.Name.spBlogReplyDisplay_Update.ToString();
        }

        public void Update()
        {
            BlogReplyDisplayParameters _blogthreaddisplayparameters = new BlogReplyDisplayParameters(Blog);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            dbhelper.Parameters = _blogthreaddisplayparameters.Parameters;
            dbhelper.Run();
        }

        public Blog Blog
        {
            get { return _blog; }
            set { _blog = value; }
        }
    }


    public class BlogReplyDisplayParameters
    {
        private Blog _blog;
        private SqlParameter[] _parameters;

        public BlogReplyDisplayParameters(Blog blog)
        {
            Blog = blog;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@DisplayID" , _blog.DisplayID),
                new SqlParameter( "@OrderBy" , _blog.OrderBy),
                new SqlParameter( "@MaxItems" , _blog.MaxItems),
                new SqlParameter( "@ItemsPerPage" , _blog.MaxItemsPerPage),
            };
            Parameters = parameters;
        }

        public Blog Blog
        {
            get { return _blog; }
            set { _blog = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

