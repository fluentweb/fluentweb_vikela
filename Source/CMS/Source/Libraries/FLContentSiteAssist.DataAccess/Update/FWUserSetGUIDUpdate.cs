using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Update
{
    public class FWUserSetGUIDUpdate : DataAccessBase
    {
        private FWUser _fwuser;

        public FWUserSetGUIDUpdate()
        {
            StoredProcedureName = StoredProcedure.Name.spFWUserSetSessionGUID_Update.ToString();
        }

        public void Update()
        {
            FWUserSetGUIDUpdateParameters _fwusertextupdateparameters = new FWUserSetGUIDUpdateParameters(FWUser);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            dbhelper.Parameters = _fwusertextupdateparameters.Parameters;
            dbhelper.Run();
        }

        public FWUser FWUser
        {
            get { return _fwuser; }
            set { _fwuser = value; }
        }
    }


    public class FWUserSetGUIDUpdateParameters
    {
        private FWUser _fwuser;
        private SqlParameter[] _parameters;

        public FWUserSetGUIDUpdateParameters(FWUser fwuser)
        {
            FWUser = fwuser;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@intUserID" , _fwuser.UserID),
                new SqlParameter( "@resetSessionGUID" , _fwuser.ResetGUID),
            };
            Parameters = parameters;
        }

        public FWUser FWUser
        {
            get { return _fwuser; }
            set { _fwuser = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

