﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FLContentSiteAssist.Common;
using System.Data.SqlClient;

namespace FLContentSiteAssist.DataAccess.Update
{
    public class PageImageUpdate : DataAccessBase
    {
        private PageImage _pageImage;

        public PageImageUpdate()
        {
            StoredProcedureName = StoredProcedure.Name.spPageImage_Update.ToString();
        }

        public void Update()
        {
            PageImageUpdateParameters _itemimageimageupdateparameters = new PageImageUpdateParameters(PageImage);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            dbhelper.Parameters = _itemimageimageupdateparameters.Parameters;
            dbhelper.Run();
        }

        public PageImage PageImage
        {
            get { return _pageImage; }
            set { _pageImage = value; }
        }
    }


    public class PageImageUpdateParameters
    {
        private PageImage _pageImage;
        private SqlParameter[] _parameters;

        public PageImageUpdateParameters(PageImage pageImage)
        {
            ItemImage = pageImage;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@PageImageID" , _pageImage.PageImageID),
                new SqlParameter( "@ImageURL" , _pageImage.ImageURL),
                new SqlParameter( "@ImageALT" , _pageImage.ImageAlt),
                new SqlParameter( "@ImageLinkURL" , _pageImage.ImageLinkURL),
                new SqlParameter( "@ImageTitle" , _pageImage.ImageTitle),
                new SqlParameter( "@ImageCaption" , _pageImage.ImageCaption),
                new SqlParameter( "@ImageText" , _pageImage.ImageText),
                new SqlParameter( "@ImageThumbURL" , _pageImage.ImageThumbURL),
                new SqlParameter( "@DeleteImage" , _pageImage.DeleteImage),
                new SqlParameter( "@DeleteThumbnailImage" , _pageImage.DeleteThumbnailImage),
            };
            Parameters = parameters;
        }

        public PageImage ItemImage
        {
            get { return _pageImage; }
            set { _pageImage = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}
