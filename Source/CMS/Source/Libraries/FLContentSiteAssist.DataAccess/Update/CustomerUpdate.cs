using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Update
{
    public class CustomerUpdate : DataAccessBase
    {
        private Customer _customer;
        private CustomerUpdateParameters _customerupdateparameters;

        public CustomerUpdate()
        {
            StoredProcedureName = StoredProcedure.Name.spCustomer_Update.ToString();
        }

        public void Update()
        {
            _customerupdateparameters = new CustomerUpdateParameters(Customer);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            dbhelper.Parameters = _customerupdateparameters.Parameters;
            dbhelper.Run();
        }

        public Customer Customer
        {
            get { return _customer; }
            set { _customer = value; }
        }
    }

    public class CustomerUpdateParameters
    {
        private Customer _customer;
        private SqlParameter[] _parameters;

        public CustomerUpdateParameters(Customer customer)
        {
            Customer = customer;
            Build();
        }

        private void Build()
        {
            if (Customer.Password == "")
            {
                SqlParameter[] parameters =
                    {
                    new SqlParameter("@FirstName"  ,   Customer.FirstName) ,
                    new SqlParameter("@SecondName"  ,   Customer.SecondName) ,
                    new SqlParameter("@TitleID"  ,   Customer.TitleID) ,
                    new SqlParameter("@Email"  ,   Customer.Email) ,
                    new SqlParameter("@Password"  ,   null) , 
                    new SqlParameter("@IsSubscribed"  ,   Customer.IsSubscribed) ,
                    new SqlParameter("@IsSubscribedTP"  ,   Customer.IsTPSubscribed),
                    new SqlParameter("@IsSubscribed1"  ,   Customer.IsSubscribed1) ,
                    new SqlParameter("@IsSubscribed2"  ,   Customer.IsSubscribed2) ,
                    new SqlParameter("@IsSubscribed3"  ,   Customer.IsSubscribed3) ,
                    new SqlParameter("@IsSubscribed4"  ,   Customer.IsSubscribed4) ,
                    new SqlParameter("@IsSubscribed5"  ,   Customer.IsSubscribed5) ,
                    new SqlParameter("@ClientID"  ,   Client.ClientID),
                    new SqlParameter("@SessionGUID" , Customer.SessionGUID),
                    new SqlParameter("@YearOfBirth"  ,   Customer.YearOfBirth),
                    new SqlParameter("@Sex"  ,   Customer.Sex),
                    new SqlParameter("@Telephone"  ,   Customer.Telephone),
                    new SqlParameter("@Attr1"  ,   Customer.Attribute1),
                    new SqlParameter("@Attr2"  ,   Customer.Attribute2),
                    new SqlParameter("@Attr3"  ,   Customer.Attribute3),
                    new SqlParameter("@Attr4"  ,   Customer.Attribute4),
                    new SqlParameter("@Attr5"  ,   Customer.Attribute5),
                    };
                Parameters = parameters;
            }
            else
            {
                SqlParameter[] parameters =
                    {
                    new SqlParameter("@FirstName"  ,   Customer.FirstName) ,
                    new SqlParameter("@SecondName"  ,   Customer.SecondName) ,
                    new SqlParameter("@TitleID"  ,   Customer.TitleID) ,
                    new SqlParameter("@Email"  ,   Customer.Email) ,
                    new SqlParameter("@Password"  ,   Customer.Password),
                    new SqlParameter("@IsSubscribed"  ,   Customer.IsSubscribed) ,
                    new SqlParameter("@IsSubscribedTP"  ,   Customer.IsTPSubscribed),
                    new SqlParameter("@IsSubscribed1"  ,   Customer.IsSubscribed1) ,
                    new SqlParameter("@IsSubscribed2"  ,   Customer.IsSubscribed2) ,
                    new SqlParameter("@IsSubscribed3"  ,   Customer.IsSubscribed3) ,
                    new SqlParameter("@IsSubscribed4"  ,   Customer.IsSubscribed4) ,
                    new SqlParameter("@IsSubscribed5"  ,   Customer.IsSubscribed5) ,
                    new SqlParameter("@ClientID"  ,   Client.ClientID),
                    new SqlParameter("@SessionGUID" , Customer.SessionGUID),
                    new SqlParameter("@YearOfBirth"  ,   Customer.YearOfBirth),
                    new SqlParameter("@Sex"  ,   Customer.Sex),
                    new SqlParameter("@Telephone"  ,   Customer.Telephone),
                    new SqlParameter("@Attr1"  ,   Customer.Attribute1),
                    new SqlParameter("@Attr2"  ,   Customer.Attribute2),
                    new SqlParameter("@Attr3"  ,   Customer.Attribute3),
                    new SqlParameter("@Attr4"  ,   Customer.Attribute4),
                    new SqlParameter("@Attr5"  ,   Customer.Attribute5),
                    };
                Parameters = parameters;
            }
        }

        public Customer Customer
        {
            get { return _customer; }
            set { _customer = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }
    }
}

