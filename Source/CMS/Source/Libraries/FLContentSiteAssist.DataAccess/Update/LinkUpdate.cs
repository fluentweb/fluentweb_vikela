using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Update
{
    public class LinkUpdate : DataAccessBase
    {
        private Link _link;

        public LinkUpdate()
        {
            StoredProcedureName = StoredProcedure.Name.spLink_Update.ToString();
        }

        public void Update()
        {
            LinkUpdateParameters _linkupdateparameters = new LinkUpdateParameters(Link);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            dbhelper.Parameters = _linkupdateparameters.Parameters;
            dbhelper.Run();
        }

        public Link Link
        {
            get { return _link; }
            set { _link = value; }
        }
    }


    public class LinkUpdateParameters
    {
        private Link _link;
        private SqlParameter[] _parameters;

        public LinkUpdateParameters(Link link)
        {
            Link = link;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@LinkID" , _link.LinkID),
                new SqlParameter( "@Title" , _link.Title),
                new SqlParameter( "@URL" , _link.Url),
                new SqlParameter( "@Enabled" , _link.Enabled),
                new SqlParameter( "@Sequence" , _link.Sequence),
            };
            Parameters = parameters;
        }

        public Link Link
        {
            get { return _link; }
            set { _link = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

