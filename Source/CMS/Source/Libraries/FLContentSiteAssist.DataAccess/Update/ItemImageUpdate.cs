using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Update
{
    public class ItemImageUpdate : DataAccessBase
    {
        private ItemImage _itemimage;

        public ItemImageUpdate()
        {
            StoredProcedureName = StoredProcedure.Name.spItemImage_Update.ToString();
        }

        public void Update()
        {
            ItemImageUpdateParameters _itemimageimageupdateparameters = new ItemImageUpdateParameters(ItemImage);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            dbhelper.Parameters = _itemimageimageupdateparameters.Parameters;
            dbhelper.Run();
        }

        public ItemImage ItemImage
        {
            get { return _itemimage; }
            set { _itemimage = value; }
        }
    }


    public class ItemImageUpdateParameters
    {
        private ItemImage _itemimage;
        private SqlParameter[] _parameters;

        public ItemImageUpdateParameters(ItemImage itemimage)
        {
            ItemImage = itemimage;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@ItemImageID", _itemimage.ID ),
                new SqlParameter( "@ItemID" , _itemimage.ItemID),
                new SqlParameter( "@ImageTypeID" , _itemimage.ImageTypeID),
                new SqlParameter( "@ImageURL" , _itemimage.ImageURL),
                new SqlParameter( "@ImageLinkURL" , _itemimage.ImageLinkURL),
                new SqlParameter( "@ImageALT" , _itemimage.ImageAlt),
                new SqlParameter( "@ImageCaption" , _itemimage.ImageCaption),
                new SqlParameter( "@ImageTitle" , _itemimage.ImageTitle),
                new SqlParameter( "@ImageText" , _itemimage.ImageText),
                new SqlParameter( "@DeleteImage" , _itemimage.DeleteImg),
            };
            Parameters = parameters;
        }

        public ItemImage ItemImage
        {
            get { return _itemimage; }
            set { _itemimage = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

