using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Update
{
    public class PageTextUpdate : DataAccessBase
    {
        private PageText _pagetext;

        public PageTextUpdate()
        {
            StoredProcedureName = StoredProcedure.Name.spPageText_Update.ToString();
        }

        public void Update()
        {
            PageTextUpdateParameters _pagetexttextupdateparameters = new PageTextUpdateParameters(PageText);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            dbhelper.Parameters = _pagetexttextupdateparameters.Parameters;
            dbhelper.Run();
        }

        public PageText PageText
        {
            get { return _pagetext; }
            set { _pagetext = value; }
        }
    }


    public class PageTextUpdateParameters
    {
        private PageText _pagetext;
        private SqlParameter[] _parameters;

        public PageTextUpdateParameters(PageText pagetext)
        {
            PageText = pagetext;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@TextID" , _pagetext.TextID),
                new SqlParameter( "@Text" , _pagetext.Text),
            };
            Parameters = parameters;
        }

        public PageText PageText
        {
            get { return _pagetext; }
            set { _pagetext = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

