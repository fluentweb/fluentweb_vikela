using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Update
{
    public class CustomerSessionLogoutUpdate : DataAccessBase
    {
        private Customer _customer;
        private CustomerSessionLogoutUpdateParameters _customersessionlogoutupdateparameters;

        public CustomerSessionLogoutUpdate()
        {
            StoredProcedureName = StoredProcedure.Name.spRemoveCustomerSession_Update.ToString();
        }

        public void Update()
        {
            _customersessionlogoutupdateparameters = new CustomerSessionLogoutUpdateParameters(Customer);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            dbhelper.Parameters = _customersessionlogoutupdateparameters.Parameters;
            dbhelper.Run();
        }

        public Customer Customer
        {
            get { return _customer; }
            set { _customer = value; }
        }
    }

    public class CustomerSessionLogoutUpdateParameters
    {
        private Customer _customer;
        private SqlParameter[] _parameters;

        public CustomerSessionLogoutUpdateParameters(Customer customer)
        {
            Customer = customer;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
		    {
			new SqlParameter( "@ClientID" , Client.ClientID),
            new SqlParameter( "@SessionGUID", Customer.SessionGUID),
		    };

            Parameters = parameters;
        }

        public Customer Customer
        {
            get { return _customer; }
            set { _customer = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }
    }
}

