using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Update
{
    public class BlogThreadUpdate : DataAccessBase
    {
        private Blog _blog;

        public BlogThreadUpdate()
        {
            StoredProcedureName = StoredProcedure.Name.spBlogThread_Update.ToString();
        }

        public void Update()
        {
            BlogThreadParameters _blogthreadparameters = new BlogThreadParameters(Blog);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            dbhelper.Parameters = _blogthreadparameters.Parameters;
            dbhelper.Run();
        }

        public Blog Blog
        {
            get { return _blog; }
            set { _blog = value; }
        }
    }


    public class BlogThreadParameters
    {
        private Blog _blog;
        private SqlParameter[] _parameters;

        public BlogThreadParameters(Blog blog)
        {
            Blog = blog;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@ThreadID" , _blog.ThreadID),
                new SqlParameter( "@Title" , _blog.Title),
                new SqlParameter( "@Comment" , _blog.Comment),
                new SqlParameter( "@URL" , _blog.URL),
                new SqlParameter( "@Status" , _blog.Status),
                new SqlParameter( "@UserID" , _blog.UserID),
            };
            Parameters = parameters;
        }

        public Blog Blog
        {
            get { return _blog; }
            set { _blog = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

