using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace FLContentSiteAssist.DataAccess.Update
{
    public class HitCountUpdate : DataAccessBase
    {
        private HitCountUpdateParameters _hitcountupdateparameters;

        public HitCountUpdate()
        {
            StoredProcedureName = StoredProcedure.Name.spHitCount_Update.ToString();
        }

        public void Update()
        {
            _hitcountupdateparameters = new HitCountUpdateParameters();
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            dbhelper.Parameters = _hitcountupdateparameters.Parameters;
            dbhelper.Run();
        }

    }

    public class HitCountUpdateParameters
    {
        private SqlParameter[] _parameters;

        public HitCountUpdateParameters()
        {
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
                {
                new SqlParameter("@Client" , Client.ClientID),
                };
            Parameters = parameters;
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }
    }
}

