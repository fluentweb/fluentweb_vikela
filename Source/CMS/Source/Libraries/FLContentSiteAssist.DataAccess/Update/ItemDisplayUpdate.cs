using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Update
{
    public class ItemDisplayUpdate : DataAccessBase
    {
        private Item _itemdisplay;

        public ItemDisplayUpdate()
        {
            StoredProcedureName = StoredProcedure.Name.spItemDisplay_Update.ToString();
        }

        public void Update()
        {
            ItemDisplayUpdateParameters _itemdisplaytextupdateparameters = new ItemDisplayUpdateParameters(Item);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            dbhelper.Parameters = _itemdisplaytextupdateparameters.Parameters;
            dbhelper.Run();
        }

        public Item Item
        {
            get { return _itemdisplay; }
            set { _itemdisplay = value; }
        }
    }


    public class ItemDisplayUpdateParameters
    {
        private Item _itemdisplay;
        private SqlParameter[] _parameters;

        public ItemDisplayUpdateParameters(Item item)
        {
            Item = item;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@DisplayID" , _itemdisplay.ItemDisplayID),
                new SqlParameter( "@MaxItems" , _itemdisplay.MaxItems),
                new SqlParameter( "@OrderBy" , _itemdisplay.OrderBy),
                new SqlParameter( "@Paged", _itemdisplay.IsPaged),
                new SqlParameter( "@PageSize", _itemdisplay.PageSize)
            };
            Parameters = parameters;
        }

        public Item Item
        {
            get { return _itemdisplay; }
            set { _itemdisplay = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

