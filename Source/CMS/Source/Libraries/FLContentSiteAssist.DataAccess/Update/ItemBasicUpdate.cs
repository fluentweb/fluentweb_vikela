using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Update
{
    public class ItemBasicUpdate : DataAccessBase
    {
        private Item _item;

        public ItemBasicUpdate()
        {
            StoredProcedureName = StoredProcedure.Name.spItemBasic_Update.ToString();
        }

        public void Update()
        {
            ItemBasicUpdateParameters _itemtextupdateparameters = new ItemBasicUpdateParameters(Item);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            dbhelper.Parameters = _itemtextupdateparameters.Parameters;
            dbhelper.Run();
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }
    }


    public class ItemBasicUpdateParameters
    {
        private Item _item;
        private SqlParameter[] _parameters;

        public ItemBasicUpdateParameters(Item item)
        {
            Item = item;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@ItemID" , _item.ItemID),
                new SqlParameter( "@Sequence" , _item.Sequence),
                new SqlParameter( "@Enabled" , _item.Enabled),
                new SqlParameter( "@Title" , _item.Title),
            };
            Parameters = parameters;
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

