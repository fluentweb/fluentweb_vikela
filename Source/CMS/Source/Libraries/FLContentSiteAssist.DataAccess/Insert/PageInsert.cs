using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Insert
{
    public class PageInsert : DataAccessBase
    {
        private Page _page;

        public PageInsert()
        {
            StoredProcedureName = StoredProcedure.Name.spPage_Insert.ToString();
        }

        public void Insert()
        {
            PageInsertParameters _pageinsertparameters = new PageInsertParameters(Page);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            dbhelper.Parameters = _pageinsertparameters.Parameters;
            dbhelper.Run();
        }

        public Page Page
        {
            get { return _page; }
            set { _page = value; }
        }
    }


    public class PageInsertParameters
    {
        private Page _page;
        private SqlParameter[] _parameters;

        public PageInsertParameters(Page page)
        {
            Page = page;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@Client" , Client.ClientID),
                new SqlParameter( "@Name" , _page.PageName),
                new SqlParameter( "@Title" , _page.PageTitle),
                new SqlParameter( "@Keywords" , _page.PageKeywords),
                new SqlParameter( "@URL" , _page.PageURL),
                new SqlParameter( "@Description" , _page.PageDesc),
                new SqlParameter( "@TemplateID" , _page.TemplateID),
            };
            Parameters = parameters;
        }

        public Page Page
        {
            get { return _page; }
            set { _page = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

