using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Insert
{
    public class RelatedItemInsert : DataAccessBase
    {
        private RelatedItem _relateditem;

        public RelatedItemInsert()
        {
            StoredProcedureName = StoredProcedure.Name.spRelatedItem_Insert.ToString();
        }

        public void Insert()
        {
            RelatedItemInsertParameters _relatediteminsertparameters = new RelatedItemInsertParameters(RelatedItem);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            dbhelper.Parameters = _relatediteminsertparameters.Parameters;
            dbhelper.Run();
        }

        public RelatedItem RelatedItem
        {
            get { return _relateditem; }
            set { _relateditem = value; }
        }
    }


    public class RelatedItemInsertParameters
    {
        private RelatedItem _relateditem;
        private SqlParameter[] _parameters;

        public RelatedItemInsertParameters(RelatedItem relateditem)
        {
            RelatedItem = relateditem;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@RelationshipType" , _relateditem.RelationshipType),
                new SqlParameter( "@ParentItemID" , _relateditem.ParentItemID),
                new SqlParameter( "@ChildItemID" , _relateditem.ChildItemID),
                new SqlParameter( "@Sequence" , _relateditem.Sequence),
            };
            Parameters = parameters;
        }

        public RelatedItem RelatedItem
        {
            get { return _relateditem; }
            set { _relateditem = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

