using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Insert
{
    public class BlogThreadInsert : DataAccessBase
    {
        private Blog _blog;

        public BlogThreadInsert()
        {
            StoredProcedureName = StoredProcedure.Name.spBlogThread_Insert.ToString();
        }

        public void Insert()
        {
            BlogThreadInsertParameters _blogthreadinsertparameters = new BlogThreadInsertParameters(Blog);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            dbhelper.Parameters = _blogthreadinsertparameters.Parameters;
            dbhelper.Run();
        }

        public Blog Blog
        {
            get { return _blog; }
            set { _blog = value; }
        }
    }


    public class BlogThreadInsertParameters
    {
        private Blog _blog;
        private SqlParameter[] _parameters;

        public BlogThreadInsertParameters(Blog blog)
        {
            Blog = blog;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@BlogTypeID" , _blog.BlogTypeID),
                new SqlParameter( "@UserID" , _blog.UserID),
                new SqlParameter( "@Title" , _blog.Title),
                new SqlParameter( "@Comment" , _blog.Comment),
                new SqlParameter( "@URL" , _blog.URL),
                new SqlParameter( "@Status" , _blog.Status),
            };
            Parameters = parameters;
        }

        public Blog Blog
        {
            get { return _blog; }
            set { _blog = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

