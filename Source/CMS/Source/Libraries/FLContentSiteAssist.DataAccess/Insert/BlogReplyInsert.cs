using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Insert
{
    public class BlogReplyInsert : DataAccessBase
    {
        private Blog _blog;

        public BlogReplyInsert()
        {
            StoredProcedureName = StoredProcedure.Name.spBlogReply_Insert.ToString();
        }

        public void Insert()
        {
            BlogReplyInsertParameters _blogreplyinsertparameters = new BlogReplyInsertParameters(Blog);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            object id = dbhelper.RunScalar(base.ConnectionString, _blogreplyinsertparameters.Parameters);
            if (id.ToString() == "")
            {
                this.Blog.ReplyID = 0;
            }
            else
            {
                this.Blog.ReplyID = int.Parse(id.ToString());
            }
                    
        }

        public Blog Blog
        {
            get { return _blog; }
            set { _blog = value; }
        }
    }


    public class BlogReplyInsertParameters
    {
        private Blog _blog;
        private SqlParameter[] _parameters;

        public BlogReplyInsertParameters(Blog blog)
        {
            Blog = blog;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@ThreadID" , _blog.ThreadID),
                new SqlParameter( "@CustomerID" , _blog.CustomerID),
                new SqlParameter( "@Comment" , _blog.Comment),
            };
            Parameters = parameters;
        }

        public Blog Blog
        {
            get { return _blog; }
            set { _blog = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

