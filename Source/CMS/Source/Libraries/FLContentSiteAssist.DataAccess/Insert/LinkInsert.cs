using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Insert
{
    public class LinkInsert : DataAccessBase
    {
        private Link _link;

        public LinkInsert()
        {
            StoredProcedureName = StoredProcedure.Name.spLink_Insert.ToString();
        }

        public void Insert()
        {
            LinkInsertParameters _linkinsertparameters = new LinkInsertParameters(Link);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            dbhelper.Parameters = _linkinsertparameters.Parameters;
            dbhelper.Run();
        }

        public Link Link
        {
            get { return _link; }
            set { _link = value; }
        }
    }


    public class LinkInsertParameters
    {
        private Link _link;
        private SqlParameter[] _parameters;

        public LinkInsertParameters(Link link)
        {
            Link = link;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@LocationID" , _link.LocationID),
                new SqlParameter( "@LinkTitle" , _link.Title),
                new SqlParameter( "@LinkURL" , _link.Url),
                new SqlParameter( "@SequenceID" , _link.Sequence),
                new SqlParameter( "@Enabled" , _link.Enabled),
            };
            Parameters = parameters;
        }

        public Link Link
        {
            get { return _link; }
            set { _link = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

