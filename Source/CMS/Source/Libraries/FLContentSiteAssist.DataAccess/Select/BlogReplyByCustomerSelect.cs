using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class BlogReplyByCustomerSelect : DataAccessBase
    {
        private Blog _blog;

        public BlogReplyByCustomerSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spBlogReplyByCustomer_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            BlogReplyByCustomerSelectParameters _blogselectparameters = new BlogReplyByCustomerSelectParameters(Blog);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _blogselectparameters.Parameters);

            return ds;
        }

        public Blog Blog
        {
            get { return _blog; }
            set { _blog = value; }
        }
    }


    public class BlogReplyByCustomerSelectParameters
    {
        private Blog _blog;
        private SqlParameter[] _parameters;

        public BlogReplyByCustomerSelectParameters(Blog blog)
        {
            Blog = blog;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@CustomerID" , Blog.CustomerID),
                new SqlParameter( "@BlogTypeID" , Blog.BlogTypeID),
            };
            Parameters = parameters;
        }

        public Blog Blog
        {
            get { return _blog; }
            set { _blog = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}
