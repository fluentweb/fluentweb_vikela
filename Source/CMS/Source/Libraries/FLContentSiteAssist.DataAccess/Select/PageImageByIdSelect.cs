using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class PageImageByIDSelect : DataAccessBase
    {
        private PageImage _pageimage;

        public PageImageByIDSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spPageImageByID_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            PageImageByIdSelectParameters _pageimageimagebyidselectparameters = new PageImageByIdSelectParameters(PageImage);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _pageimageimagebyidselectparameters.Parameters);

            return ds;
        }

        public PageImage PageImage
        {
            get { return _pageimage; }
            set { _pageimage = value; }
        }
    }


    public class PageImageByIdSelectParameters
    {
        private PageImage _pageimage;
        private SqlParameter[] _parameters;

        public PageImageByIdSelectParameters(PageImage pageimage)
        {
            PageImage = pageimage;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@PageImageId" , _pageimage.PageImageID),
            };
            Parameters = parameters;
        }

        public PageImage PageImage
        {
            get { return _pageimage; }
            set { _pageimage = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

