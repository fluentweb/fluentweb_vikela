using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class ItemTitleTextSearchSelect : DataAccessBase
    {
        private Item _item;

        public ItemTitleTextSearchSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spItemTitleSearch_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            ItemTitleTextSearchSelectParameters _itemtitletextsearchselectparameters = new ItemTitleTextSearchSelectParameters(Item);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _itemtitletextsearchselectparameters.Parameters);

            return ds;
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }
    }


    public class ItemTitleTextSearchSelectParameters
    {
        private Item _item;
        private SqlParameter[] _parameters;

        public ItemTitleTextSearchSelectParameters(Item item)
        {
            Item = item;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@Displaye" , _item.Display),
                new SqlParameter( "@Client" , Client.ClientID),
                new SqlParameter( "@SearchText" , _item.SearchText),
            };
            Parameters = parameters;
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

