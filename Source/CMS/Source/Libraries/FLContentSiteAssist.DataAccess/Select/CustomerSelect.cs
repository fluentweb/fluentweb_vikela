using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class CustomerSelect : DataAccessBase
    {
        private Customer _customer;

        public CustomerSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spCustomer_select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            CustomerSelectParameters _customerselectparameters = new CustomerSelectParameters(Customer);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _customerselectparameters.Parameters);

            return ds;
        }

        public Customer Customer
        {
            get { return _customer; }
            set { _customer = value; }
        }


        public class CustomerSelectParameters
        {
            private Customer _customer;
            private SqlParameter[] _parameters;

            public CustomerSelectParameters(Customer customer)
            {
                Customer = customer;
                Build();
            }

            private void Build()
            {
                SqlParameter[] parameters =
                {
                new SqlParameter( "@Email" , Customer.Email),
                new SqlParameter( "@ClientID" , Client.ClientID),
                };
                Parameters = parameters;
            }

            public Customer Customer
            {
                get { return _customer; }
                set { _customer = value; }
            }

            public SqlParameter[] Parameters
            {
                get { return _parameters; }
                set { _parameters = value; }
            }

        }


    }
}
