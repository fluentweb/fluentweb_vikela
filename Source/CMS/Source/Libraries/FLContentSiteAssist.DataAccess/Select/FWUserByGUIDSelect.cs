using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class FWUserByGUIDSelect : DataAccessBase
    {
        private FWUser _fwuser;

        public FWUserByGUIDSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spFWUserByGUID_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            FWUserByGUIDSelectParameters _fwuserbyguidselectparameters = new FWUserByGUIDSelectParameters(FWUser);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _fwuserbyguidselectparameters.Parameters);

            return ds;
        }

        public FWUser FWUser
        {
            get { return _fwuser; }
            set { _fwuser = value; }
        }
    }


    public class FWUserByGUIDSelectParameters
    {
        private FWUser _fwuser;
        private SqlParameter[] _parameters;

        public FWUserByGUIDSelectParameters(FWUser fwuser)
        {
            FWUser = fwuser;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@chrSessionGUID" , _fwuser.SessionGUID),
            };
            Parameters = parameters;
        }

        public FWUser FWUser
        {
            get { return _fwuser; }
            set { _fwuser = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

