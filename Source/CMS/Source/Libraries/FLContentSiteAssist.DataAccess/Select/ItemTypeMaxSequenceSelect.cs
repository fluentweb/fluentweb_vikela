using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class ItemTypeMaxSequenceSelect : DataAccessBase
    {
        private Item _item;

        public ItemTypeMaxSequenceSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spItemTypeSequence_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            ItemTypeMaxSequenceSelectParameters _itemtypemaxsequenceselectparameters = new ItemTypeMaxSequenceSelectParameters(Item);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _itemtypemaxsequenceselectparameters.Parameters);

            return ds;
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }
    }


    public class ItemTypeMaxSequenceSelectParameters
    {
        private Item _item;
        private SqlParameter[] _parameters;

        public ItemTypeMaxSequenceSelectParameters(Item item)
        {
            Item = item;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@itemTypeID" , _item.ItemTypeID),
                new SqlParameter( "@ParentItemID" , _item.ParentItemID),
            };
            Parameters = parameters;
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

