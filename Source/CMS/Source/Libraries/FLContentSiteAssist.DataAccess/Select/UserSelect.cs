using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class UserSelect : DataAccessBase
    {
        private string _sitename;

        public UserSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spUser_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            UserSelectParameters _userselectparameters = new UserSelectParameters(Sitename);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _userselectparameters.Parameters);

            return ds;
        }

        public String Sitename
        {
            get { return _sitename; }
            set { _sitename = value; }
        }
    }


    public class UserSelectParameters
    {
        private string _sitename;
        private SqlParameter[] _parameters;

        public UserSelectParameters(String sitename)
        {
            SiteName = sitename;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@SiteName" , SiteName),
            };
            Parameters = parameters;
        }

        public string SiteName
        {
            get { return _sitename; }
            set { _sitename = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

