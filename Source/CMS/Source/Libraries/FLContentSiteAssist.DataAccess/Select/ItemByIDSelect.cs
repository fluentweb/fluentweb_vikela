using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class ItemByIDSelect : DataAccessBase
    {
        private Item _item;

        public ItemByIDSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spItemByID_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            ItemByIDSelectParameters _itembyidselectparameters = new ItemByIDSelectParameters(Item);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _itembyidselectparameters.Parameters);

            return ds;
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }
    }


    public class ItemByIDSelectParameters
    {
        private Item _item;
        private SqlParameter[] _parameters;

        public ItemByIDSelectParameters(Item item)
        {
            Item = item;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@ItemID" , _item.ItemID),
            };
            Parameters = parameters;
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

