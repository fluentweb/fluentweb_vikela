using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class RelatedItemAvailableSelect : DataAccessBase
    {
        private RelatedItem _relateditem;

        public RelatedItemAvailableSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spRelatedItemsAvailable_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            RelatedItemAvailableSelectParameters _relateditemselectparameters = new RelatedItemAvailableSelectParameters(RelatedItem);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _relateditemselectparameters.Parameters);

            return ds;
        }

        public RelatedItem RelatedItem
        {
            get { return _relateditem; }
            set { _relateditem = value; }
        }
    }


    public class RelatedItemAvailableSelectParameters
    {
        private RelatedItem _relateditem;
        private SqlParameter[] _parameters;

        public RelatedItemAvailableSelectParameters(RelatedItem relateditem)
        {
            RelatedItem = relateditem;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@RelationshipType" , _relateditem.RelationshipType),
                new SqlParameter( "@ItemID" , _relateditem.ItemID),
            };
            Parameters = parameters;
        }

        public RelatedItem RelatedItem
        {
            get { return _relateditem; }
            set { _relateditem = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

