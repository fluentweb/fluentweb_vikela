using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;


namespace FLContentSiteAssist.DataAccess.Select
{
    public class TitleSelect : DataAccessBase
    {


        public TitleSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spTitle_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString);

            return ds;
        }

    }


}
