using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using FLContentSiteAssist.Common;


namespace FLContentSiteAssist.DataAccess.Select
{
    public class LoginCustomerSelect : DataAccessBase
    {
        private Customer _customer;

        public LoginCustomerSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spLoginCustomer_select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            LoginCustomerSelectParameters _logincustomerselectparameters = new LoginCustomerSelectParameters(Customer);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _logincustomerselectparameters.Parameters);

            return ds;
        }

        public Customer Customer
        {
            get { return _customer; }
            set { _customer = value; }
        }


        public class LoginCustomerSelectParameters
        {
            private Customer _customer;
            private SqlParameter[] _parameters;

            public LoginCustomerSelectParameters(Customer customer)
            {
                Customer = customer;
                Build();
            }

            private void Build()
            {
                SqlParameter[] parameters =
                {
                new SqlParameter( "@ClientID" , Client.ClientID),
                new SqlParameter( "@Email" , Customer.Email),
                new SqlParameter( "@Password" , Customer.Password),
                new SqlParameter( "@SessionGUID" , Customer.SessionGUID),
                };
                Parameters = parameters;
            }

            public Customer Customer
            {
                get { return _customer; }
                set { _customer = value; }
            }

            public SqlParameter[] Parameters
            {
                get { return _parameters; }
                set { _parameters = value; }
            }

        }


    }
}
