using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class PageTemplateSelect : DataAccessBase
    {

        public PageTemplateSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spPageTemplateByClient_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            PageTemplateSelectParameters _userselectparameters = new PageTemplateSelectParameters();
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _userselectparameters.Parameters);

            return ds;
        }

    }


    public class PageTemplateSelectParameters
    {
        private SqlParameter[] _parameters;

        public PageTemplateSelectParameters()
        {
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@ClientID" , Client.ClientID),
            };
            Parameters = parameters;
        }


        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

