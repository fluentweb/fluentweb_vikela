using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class BlogThreadDisplaySelect : DataAccessBase
    {
        private Blog _blog;

        public BlogThreadDisplaySelect()
        {
            StoredProcedureName = StoredProcedure.Name.spBlogThreadDisplay_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            BlogThreadDisplaySelectParameters _blogselectparameters = new BlogThreadDisplaySelectParameters(Blog);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _blogselectparameters.Parameters);

            return ds;
        }

        public Blog Blog
        {
            get { return _blog; }
            set { _blog = value; }
        }
    }


    public class BlogThreadDisplaySelectParameters
    {
        private Blog _blog;
        private SqlParameter[] _parameters;

        public BlogThreadDisplaySelectParameters(Blog blog)
        {
            Blog = blog;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@ClientID" , Client.ClientID),
                new SqlParameter( "@DisplayID" , Blog.DisplayID),
            };
            Parameters = parameters;
        }

        public Blog Blog
        {
            get { return _blog; }
            set { _blog = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}
