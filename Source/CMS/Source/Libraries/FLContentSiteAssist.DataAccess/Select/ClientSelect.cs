using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class ClientSelect : DataAccessBase
    {
        public ClientSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spClient_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            ClientSelectParameters _clientselectparameters = new ClientSelectParameters();
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _clientselectparameters.Parameters);

            return ds;
        }

    }


    public class ClientSelectParameters
    {
        private SqlParameter[] _parameters;

        public ClientSelectParameters()
        {
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@ClientID" , Client.ClientID),
            };
            Parameters = parameters;
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}
