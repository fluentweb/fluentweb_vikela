using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class BlogThreadByDisplaySelect : DataAccessBase
    {
        private Blog _blog;

        public BlogThreadByDisplaySelect()
        {
            StoredProcedureName = StoredProcedure.Name.spBlogThreadByDisplay_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            BlogThreadByDisplaySelectParameters _blogselectparameters = new BlogThreadByDisplaySelectParameters(Blog);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _blogselectparameters.Parameters);

            return ds;
        }

        public Blog Blog
        {
            get { return _blog; }
            set { _blog = value; }
        }
    }


    public class BlogThreadByDisplaySelectParameters
    {
        private Blog _blog;
        private SqlParameter[] _parameters;

        public BlogThreadByDisplaySelectParameters(Blog blog)
        {
            Blog = blog;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@ClientID" , Client.ClientID),
                new SqlParameter( "@Display" , Blog.Display),
            };
            Parameters = parameters;
        }

        public Blog Blog
        {
            get { return _blog; }
            set { _blog = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}
