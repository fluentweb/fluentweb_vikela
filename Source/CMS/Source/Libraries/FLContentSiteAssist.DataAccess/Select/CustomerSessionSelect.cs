using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class CustomerSessionSelect : DataAccessBase
    {
        private Customer _customer;

        public CustomerSessionSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spCustomerSessionGuid_select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            CustomerSessionSelectParameters _customersessionselectparameters = new CustomerSessionSelectParameters(Customer);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _customersessionselectparameters.Parameters);

            return ds;
        }

        public Customer Customer
        {
            get { return _customer; }
            set { _customer = value; }
        }


        public class CustomerSessionSelectParameters
        {
            private Customer _customer;
            private SqlParameter[] _parameters;

            public CustomerSessionSelectParameters(Customer customer)
            {
                Customer = customer;
                Build();
            }

            private void Build()
            {
                SqlParameter[] parameters =
                {
                new SqlParameter( "@SessionGUID" , Customer.SessionGUID),
                new SqlParameter( "@ClientID" , Client.ClientID),
                };
                Parameters = parameters;
            }

            public Customer Customer
            {
                get { return _customer; }
                set { _customer = value; }
            }

            public SqlParameter[] Parameters
            {
                get { return _parameters; }
                set { _parameters = value; }
            }

        }


    }
}
