using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class CommentsSelect : DataAccessBase
    {
        private Comment _comments;

        public CommentsSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spComments_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            CommentsSelectParameters _commentsselectparameters = new CommentsSelectParameters(Comments);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _commentsselectparameters.Parameters);

            return ds;
        }

        public Comment Comments
        {
            get { return _comments; }
            set { _comments = value; }
        }
    }


    public class CommentsSelectParameters
    {
        private Comment _comments;
        private SqlParameter[] _parameters;

        public CommentsSelectParameters(Comment comments)
        {
            Comments = comments;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@Client" , Client.ClientID),
                new SqlParameter( "@CommentType" , _comments.CommentType),
                new SqlParameter( "@Status" , _comments.Status),
            };
            Parameters = parameters;
        }

        public Comment Comments
        {
            get { return _comments; }
            set { _comments = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

