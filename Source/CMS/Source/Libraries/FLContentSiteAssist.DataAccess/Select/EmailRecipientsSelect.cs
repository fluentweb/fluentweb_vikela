using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class EmailRecipientsSelect : DataAccessBase
    {
        private string _email;

        public EmailRecipientsSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spEmailRecipients_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            EmailSelectParameters _emailselectparameters = new EmailSelectParameters(Email);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _emailselectparameters.Parameters);

            return ds;
        }

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
    }

    public class EmailSelectParameters
    {
        private string _email;
        private SqlParameter[] _parameters;

        public EmailSelectParameters(string email)
        {
            Email = email;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
	            new SqlParameter( "@Client" , Client.ClientID),
	            new SqlParameter( "@Email" , Email),
            };
            Parameters = parameters;
        }

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}
