using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class RelatedItemSelect : DataAccessBase
    {
        private RelatedItem _relateditem;

        public RelatedItemSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spRelatedItems_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            RelatedItemSelectParameters _relateditemselectparameters = new RelatedItemSelectParameters(RelatedItem);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _relateditemselectparameters.Parameters);

            return ds;
        }

        public RelatedItem RelatedItem
        {
            get { return _relateditem; }
            set { _relateditem = value; }
        }
    }


    public class RelatedItemSelectParameters
    {
        private RelatedItem _relateditem;
        private SqlParameter[] _parameters;

        public RelatedItemSelectParameters(RelatedItem relateditem)
        {
            RelatedItem = relateditem;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@RelationshipType" , _relateditem.RelationshipType),
                new SqlParameter( "@ItemID" , _relateditem.ItemID),
            };
            Parameters = parameters;
        }

        public RelatedItem RelatedItem
        {
            get { return _relateditem; }
            set { _relateditem = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

