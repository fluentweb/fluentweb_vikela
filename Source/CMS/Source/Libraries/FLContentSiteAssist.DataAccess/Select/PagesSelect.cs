using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class PagesSelect : DataAccessBase
    {

        public PagesSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spPagesByClient_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            PagesSelectParameters _userselectparameters = new PagesSelectParameters();
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _userselectparameters.Parameters);

            return ds;
        }

    }


    public class PagesSelectParameters
    {
        private SqlParameter[] _parameters;

        public PagesSelectParameters()
        {
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@ClientID" , Client.ClientID),
            };
            Parameters = parameters;
        }


        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

