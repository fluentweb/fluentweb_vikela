using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class PageByIDSelect : DataAccessBase
    {
        private Page _page;

        public PageByIDSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spPageByID_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            PageIDSelectParameters _pageselectparameters = new PageIDSelectParameters(Page);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _pageselectparameters.Parameters);

            return ds;
        }

        public Page Page
        {
            get { return _page; }
            set { _page = value; }
        }
    }


    public class PageIDSelectParameters
    {
        private Page _page;
        private SqlParameter[] _parameters;

        public PageIDSelectParameters(Page page)
        {
            Page = page;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@PageID" , Page.PageID),
            };
            Parameters = parameters;
        }

        public Page Page
        {
            get { return _page; }
            set { _page = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}
