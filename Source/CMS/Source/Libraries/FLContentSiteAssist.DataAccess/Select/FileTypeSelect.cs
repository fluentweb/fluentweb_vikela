﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class FileTypeSelect : DataAccessBase
    {

        public FileTypeSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spFileType_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            FileTypeSelectParameters _userselectparameters = new FileTypeSelectParameters();
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _userselectparameters.Parameters);

            return ds;
        }

    }


    public class FileTypeSelectParameters
    {
        private SqlParameter[] _parameters;

        public FileTypeSelectParameters()
        {
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@ClientID" , Client.ClientID),
            };
            Parameters = parameters;
        }


        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

