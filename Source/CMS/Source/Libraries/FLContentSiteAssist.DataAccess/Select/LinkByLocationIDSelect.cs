using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class LinkByLocationIDSelect : DataAccessBase
    {
        private Link _link;

        public LinkByLocationIDSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spLinkByLocationID_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            LinkByLocationIDSelectParameters _linkbylocationidselectparameters = new LinkByLocationIDSelectParameters(Link);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _linkbylocationidselectparameters.Parameters);

            return ds;
        }

        public Link Link
        {
            get { return _link; }
            set { _link = value; }
        }
    }


    public class LinkByLocationIDSelectParameters
    {
        private Link _link;
        private SqlParameter[] _parameters;

        public LinkByLocationIDSelectParameters(Link link)
        {
            Link = link;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@LocationID" , Link.LocationID),
                new SqlParameter( "@ViewDisabled" , Link.ViewDisabled),
            };
            Parameters = parameters;
        }

        public Link Link
        {
            get { return _link; }
            set { _link = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}