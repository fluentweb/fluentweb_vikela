using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class PageByURLSelect : DataAccessBase
    {
        private Page _page;

        public PageByURLSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spPageByURL_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            PageURLSelectParameters _pageselectparameters = new PageURLSelectParameters(Page);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _pageselectparameters.Parameters);

            return ds;
        }

        public Page Page
        {
            get { return _page; }
            set { _page = value; }
        }
    }


    public class PageURLSelectParameters
    {
        private Page _page;
        private SqlParameter[] _parameters;

        public PageURLSelectParameters(Page page)
        {
            Page = page;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@URL" , Page.PageURL),
                new SqlParameter( "@Client" , Client.ClientID),
            };
            Parameters = parameters;
        }

        public Page Page
        {
            get { return _page; }
            set { _page = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}
