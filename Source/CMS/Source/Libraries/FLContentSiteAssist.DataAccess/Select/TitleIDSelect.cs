using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class TitleIDSelect : DataAccessBase
    {
        private string _title;

        public TitleIDSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spTitleID_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            TitleIDSelectParameters _titleidselectparameters = new TitleIDSelectParameters(Title);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _titleidselectparameters.Parameters);

            return ds;
        }

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }
    }

    public class TitleIDSelectParameters
    {
        private string _title;
        private SqlParameter[] _parameters;

        public TitleIDSelectParameters(string title)
        {
            Title = title;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@Title" , Title),
            };
            Parameters = parameters;
        }

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

