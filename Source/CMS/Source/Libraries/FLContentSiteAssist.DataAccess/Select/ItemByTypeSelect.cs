using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class ItemByTypeSelect : DataAccessBase
    {
        private Item _item;

        public ItemByTypeSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spItemByTypeID_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            ItemByTypeSelectParameters _itembytypeselectparameters = new ItemByTypeSelectParameters(Item);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _itembytypeselectparameters.Parameters);

            return ds;
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }
    }


    public class ItemByTypeSelectParameters
    {
        private Item _item;
        private SqlParameter[] _parameters;

        public ItemByTypeSelectParameters(Item item)
        {
            Item = item;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@ItemTypeID" , _item.ItemTypeID),
                new SqlParameter( "@ViewDisabled" , _item.ViewDisabled),
                new SqlParameter( "@ParentItemID" , _item.ParentItemID),
            };
            Parameters = parameters;
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

