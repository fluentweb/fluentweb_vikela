using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class PasswordSelect : DataAccessBase
    {
        private string _email;

        public PasswordSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spRetrievePassword_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            PasswordSelectParameters _passwordselectparameters = new PasswordSelectParameters(Email);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _passwordselectparameters.Parameters);

            return ds;
        }

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

    }


    public class PasswordSelectParameters
    {
        private string _email;
        private SqlParameter[] _parameters;

        public PasswordSelectParameters(string email)
        {
            Email = email;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
	            new SqlParameter( "@Email" , Email),
            };
            Parameters = parameters;
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

    }

}
