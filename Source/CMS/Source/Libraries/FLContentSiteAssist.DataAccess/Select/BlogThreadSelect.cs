using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class BlogThreadSelect : DataAccessBase
    {
        private Blog _blog;

        public BlogThreadSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spBlogThread_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            BlogThreadSelectParameters _blogselectparameters = new BlogThreadSelectParameters(Blog);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _blogselectparameters.Parameters);

            return ds;
        }

        public Blog Blog
        {
            get { return _blog; }
            set { _blog = value; }
        }
    }


    public class BlogThreadSelectParameters
    {
        private Blog _blog;
        private SqlParameter[] _parameters;

        public BlogThreadSelectParameters(Blog blog)
        {
            Blog = blog;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@ThreadID" , Blog.ThreadID),
            };
            Parameters = parameters;
        }

        public Blog Blog
        {
            get { return _blog; }
            set { _blog = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}
