using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class ItemImageSelect: DataAccessBase
    {
        private Item _item;

        public ItemImageSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spItemImage_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            ItemImageSelectParameters _itemimageselectparameters = new ItemImageSelectParameters(Item);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _itemimageselectparameters.Parameters);

            return ds;
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }
    }


    public class ItemImageSelectParameters
    {
        private Item _item;
        private SqlParameter[] _parameters;

        public ItemImageSelectParameters(Item item)
        {
            Item = item;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@ItemID" , _item.ItemID),
            };
            Parameters = parameters;
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }

}
