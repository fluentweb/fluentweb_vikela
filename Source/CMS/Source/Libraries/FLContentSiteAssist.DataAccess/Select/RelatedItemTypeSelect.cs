using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class RelatedItemTypeSelect : DataAccessBase
    {
        private RelatedItem _relateditem;

        public RelatedItemTypeSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spRelatedItemType_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            RelatedItemTypeSelectParameters _relateditemselectparameters = new RelatedItemTypeSelectParameters(RelatedItem);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _relateditemselectparameters.Parameters);

            return ds;
        }

        public RelatedItem RelatedItem
        {
            get { return _relateditem; }
            set { _relateditem = value; }
        }
    }


    public class RelatedItemTypeSelectParameters
    {
        private RelatedItem _relateditem;
        private SqlParameter[] _parameters;

        public RelatedItemTypeSelectParameters(RelatedItem relateditem)
        {
            RelatedItem = relateditem;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@ItemTypeID" , _relateditem.ItemTypeID),
            };
            Parameters = parameters;
        }

        public RelatedItem RelatedItem
        {
            get { return _relateditem; }
            set { _relateditem = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

