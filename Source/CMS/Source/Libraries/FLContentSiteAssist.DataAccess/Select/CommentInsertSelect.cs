using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class CommentInsertSelect : DataAccessBase
    {
        private Comment _comments;

        public CommentInsertSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spCommentCreate_Select.ToString();
        }

        public Int32 Get()
        {
            DataSet ds;

            CommentInsertSelectParameters _commentinsertselectparameters = new CommentInsertSelectParameters(Comments);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _commentinsertselectparameters.Parameters);

            return Convert.ToInt32(ds.Tables[0].Rows[0]["ID"]);
        }

        public Comment Comments
        {
            get { return _comments; }
            set { _comments = value; }
        }
    }


    public class CommentInsertSelectParameters
    {
        private Comment _comments;
        private SqlParameter[] _parameters;

        public CommentInsertSelectParameters(Comment comments)
        {
            Comments = comments;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@Client" , Client.ClientID),
                new SqlParameter( "@CommentType" , _comments.CommentType),
                new SqlParameter( "@Comment" , _comments.CommentText),
                new SqlParameter( "@Author" , _comments.Author),
            };
            Parameters = parameters;
        }

        public Comment Comments
        {
            get { return _comments; }
            set { _comments = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

