using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class ItemDisplaySelect : DataAccessBase
    {
        private Item _item;

        public ItemDisplaySelect()
        {
            StoredProcedureName = StoredProcedure.Name.spItemDisplay_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            ItemDisplaySelectParameters _itemdisplayselectparameters = new ItemDisplaySelectParameters(Item);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _itemdisplayselectparameters.Parameters);

            return ds;
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }
    }


    public class ItemDisplaySelectParameters
    {
        private Item _item;
        private SqlParameter[] _parameters;

        public ItemDisplaySelectParameters(Item item)
        {
            Item = item;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@Display" , _item.Display),
                new SqlParameter( "@Client" , Client.ClientID),
                new SqlParameter( "@ParentItemID" , _item.ParentItemID),
                new SqlParameter( "@CurrentPage", _item.CurrentPage == 0 ? (object)DBNull.Value : _item.CurrentPage),
                new SqlParameter( "@CategoryID1", _item.CategoryID1 == 0 ?(object)DBNull.Value : _item.CategoryID1),
                new SqlParameter( "@CategoryID2", _item.CategoryID2 == 0 ?(object)DBNull.Value : _item.CategoryID2),
            };
                       
            Parameters = parameters;
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

