using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class ItemTextByTypeSelect : DataAccessBase
    {
        private Item _item;

        public ItemTextByTypeSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spItemTextByType_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            ItemTextByTypeSelectParameters _itemtextbytypeselectparameters = new ItemTextByTypeSelectParameters(Item);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _itemtextbytypeselectparameters.Parameters);

            return ds;
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }
    }


    public class ItemTextByTypeSelectParameters
    {
        private Item _item;
        private SqlParameter[] _parameters;

        public ItemTextByTypeSelectParameters(Item item)
        {
            Item = item;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@TextType" , _item.TextType),
                new SqlParameter( "@ItemType" , _item.ItemType),
            };
            Parameters = parameters;
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }

}
