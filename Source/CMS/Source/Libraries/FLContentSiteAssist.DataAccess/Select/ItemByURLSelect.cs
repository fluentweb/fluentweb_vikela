using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class ItemByURLSelect : DataAccessBase
    {
        private Item _item;

        public ItemByURLSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spItemByURL_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            ItemByURLSelectParameters _itembyurlselectparameters = new ItemByURLSelectParameters(Item);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _itembyurlselectparameters.Parameters);

            return ds;
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }
    }


    public class ItemByURLSelectParameters
    {
        private Item _item;
        private SqlParameter[] _parameters;

        public ItemByURLSelectParameters(Item item)
        {
            Item = item;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@ItemType" , _item.ItemType),
                new SqlParameter( "@URL" , _item.ItemURL),
                new SqlParameter( "@Client" , Client.ClientID),
            };
            Parameters = parameters;
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }

}
