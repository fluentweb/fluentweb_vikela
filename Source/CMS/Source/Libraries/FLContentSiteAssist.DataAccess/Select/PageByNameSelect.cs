using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class PageByNameSelect : DataAccessBase
    {
        private Page _page;

        public PageByNameSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spPageByName_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            PageSelectParameters _pageselectparameters = new PageSelectParameters(Page);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _pageselectparameters.Parameters);

            return ds;
        }

        public Page Page
        {
            get { return _page; }
            set { _page = value; }
        }
    }


    public class PageSelectParameters
    {
        private Page _page;
        private SqlParameter[] _parameters;

        public PageSelectParameters(Page page)
        {
            Page = page;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@Location" , Page.PageName),
                new SqlParameter( "@Client" , Client.ClientID),
            };
            Parameters = parameters;
        }

        public Page Page
        {
            get { return _page; }
            set { _page = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}
