using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class ItemTextSearchSelect : DataAccessBase
    {
        private Item _item;

        public ItemTextSearchSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spItemTextSearch_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            ItemTextSearchSelectParameters _itemtextsearchselectparameters = new ItemTextSearchSelectParameters(Item);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _itemtextsearchselectparameters.Parameters);

            return ds;
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }
    }


    public class ItemTextSearchSelectParameters
    {
        private Item _item;
        private SqlParameter[] _parameters;

        public ItemTextSearchSelectParameters(Item item)
        {
            Item = item;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@Display" , _item.Display),
                new SqlParameter( "@Client" , Client.ClientID),
                new SqlParameter( "@SearchText" , _item.SearchText),
            };
            Parameters = parameters;
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

