using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class LinkSectionsSelect : DataAccessBase
    {
        private string _location;

        public LinkSectionsSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spLinkSections_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            LinkSectionsSelectParameters _linksectionsselectparameters = new LinkSectionsSelectParameters(Location);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _linksectionsselectparameters.Parameters);

            return ds;
        }

        public string Location
        {
            get { return _location; }
            set { _location = value; }
        }
    }


    public class LinkSectionsSelectParameters
    {
        private string _location;
        private SqlParameter[] _parameters;

        public LinkSectionsSelectParameters(String location)
        {
            Location = location;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@Client" , Client.ClientID),
                new SqlParameter( "@Location" , Location),
            };
            Parameters = parameters;
        }

        public string Location
        {
            get { return _location; }
            set { _location = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

