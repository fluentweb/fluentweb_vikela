using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class BlogReplySelect : DataAccessBase
    {
        private Blog _blog;

        public BlogReplySelect()
        {
            StoredProcedureName = StoredProcedure.Name.spBlogReply_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            BlogReplySelectParameters _blogselectparameters = new BlogReplySelectParameters(Blog);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _blogselectparameters.Parameters);

            return ds;
        }

        public Blog Blog
        {
            get { return _blog; }
            set { _blog = value; }
        }
    }


    public class BlogReplySelectParameters
    {
        private Blog _blog;
        private SqlParameter[] _parameters;

        public BlogReplySelectParameters(Blog blog)
        {
            Blog = blog;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@ReplyID" , Blog.ReplyID),
            };
            Parameters = parameters;
        }

        public Blog Blog
        {
            get { return _blog; }
            set { _blog = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}
