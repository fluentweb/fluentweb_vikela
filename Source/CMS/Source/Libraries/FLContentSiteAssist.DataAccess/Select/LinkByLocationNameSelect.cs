using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class LinkByLocationNameSelect : DataAccessBase
    {
        private Link _link;

        public LinkByLocationNameSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spLinkByLocationName_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            LinkLocationSelectParameters _linklocationselectparameters = new LinkLocationSelectParameters(Link);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _linklocationselectparameters.Parameters);

            return ds;
        }

        public Link Link
        {
            get { return _link; }
            set { _link = value; }
        }
    }


    public class LinkLocationSelectParameters
    {
        private Link _link;
        private SqlParameter[] _parameters;

        public LinkLocationSelectParameters(Link link)
        {
            Link = link;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@Client" , Client.ClientID),
                new SqlParameter( "@Location" , Link.Location),
                new SqlParameter( "@ViewDisabled" , Link.ViewDisabled),
            };
            Parameters = parameters;
        }

        public Link Link
        {
            get { return _link; }
            set { _link = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

