using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class ItemImageByItemSelect : DataAccessBase
    {
        private Item _item;

        public ItemImageByItemSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spItemImageByItem_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            ItemImageByItemTypeSelectParameters _itemimagebyitemtypeselectparameters = new ItemImageByItemTypeSelectParameters(Item);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _itemimagebyitemtypeselectparameters.Parameters);

            return ds;
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }
    }


    public class ItemImageByItemTypeSelectParameters
    {
        private Item _item;
        private SqlParameter[] _parameters;

        public ItemImageByItemTypeSelectParameters(Item item)
        {
            Item = item;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@ItemId" , _item.ItemID),
            };
            Parameters = parameters;
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }

}
