using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class PageTextSelect : DataAccessBase
    {
        private SitePage _sitepage;

        public PageTextSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spPageText_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            PageTextSelectParameters _sitepagetextselectparameters = new PageTextSelectParameters(SitePage);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _sitepagetextselectparameters.Parameters);

            return ds;
        }

        public SitePage SitePage
        {
            get { return _sitepage; }
            set { _sitepage = value; }
        }
    }


    public class PageTextSelectParameters
    {
        private SitePage _sitepage;
        private SqlParameter[] _parameters;

        public PageTextSelectParameters(SitePage sitepage)
        {
            SitePage = sitepage;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@PageName" , _sitepage.Name),
                new SqlParameter( "@Client" , Client.ClientID),
            };
            Parameters = parameters;
        }

        public SitePage SitePage
        {
            get { return _sitepage; }
            set { _sitepage = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

