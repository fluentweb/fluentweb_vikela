using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class ItemSelect : DataAccessBase
    {
        private Item _item;

        public ItemSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spItem_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            ItemSelectParameters _itemselectparameters = new ItemSelectParameters(Item);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _itemselectparameters.Parameters);

            return ds;
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }
    }


    public class ItemSelectParameters
    {
        private Item _item;
        private SqlParameter[] _parameters;

        public ItemSelectParameters(Item item)
        {
            Item = item;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@ItemType" , _item.ItemType),
                new SqlParameter( "@Client" , Client.ClientID),
                new SqlParameter( "@ViewDisabled" , _item.ViewDisabled),
            };
            Parameters = parameters;
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

