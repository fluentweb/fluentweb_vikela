﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class ItemTypeSelect : DataAccessBase
    {
        private Item _item;

        public ItemTypeSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spItemType_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            ItemTypeSelectParameters _itemtextselectparameters = new ItemTypeSelectParameters(Item);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _itemtextselectparameters.Parameters);

            return ds;
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }
    }


    public class ItemTypeSelectParameters
    {
        private Item _item;
        private SqlParameter[] _parameters;

        public ItemTypeSelectParameters(Item item)
        {
            Item = item;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@ItemTypeID" , _item.ItemTypeID),
                new SqlParameter( "@IsAdmin", _item.IsAdmin)
            };
            Parameters = parameters;
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }

}
