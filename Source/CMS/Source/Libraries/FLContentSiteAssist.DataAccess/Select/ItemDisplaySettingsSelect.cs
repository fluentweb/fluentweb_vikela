using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class ItemDisplaySettingsSelect : DataAccessBase
    {
        private Item _item;

        public ItemDisplaySettingsSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spItemDisplaySettings_select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            ItemDisplaySettingsSelectParameters _itemdisplaysettingsselectparameters = new ItemDisplaySettingsSelectParameters(Item);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _itemdisplaysettingsselectparameters.Parameters);

            return ds;
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }
    }


    public class ItemDisplaySettingsSelectParameters
    {
        private Item _item;
        private SqlParameter[] _parameters;

        public ItemDisplaySettingsSelectParameters(Item item)
        {
            Item = item;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@DisplayID" , _item.ItemDisplayID),
            };
            Parameters = parameters;
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

