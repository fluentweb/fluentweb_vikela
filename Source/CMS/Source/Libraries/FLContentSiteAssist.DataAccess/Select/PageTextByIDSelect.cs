using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class PageTextByIDSelect : DataAccessBase
    {
        private PageText _pagetext;

        public PageTextByIDSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spPageTextByID_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            PageTextByIDSelectParameters _pagetexttextbyidselectparameters = new PageTextByIDSelectParameters(PageText);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _pagetexttextbyidselectparameters.Parameters);

            return ds;
        }

        public PageText PageText
        {
            get { return _pagetext; }
            set { _pagetext = value; }
        }
    }


    public class PageTextByIDSelectParameters
    {
        private PageText _pagetext;
        private SqlParameter[] _parameters;

        public PageTextByIDSelectParameters(PageText pagetext)
        {
            PageText = pagetext;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@TextID" , _pagetext.TextID),
            };
            Parameters = parameters;
        }

        public PageText PageText
        {
            get { return _pagetext; }
            set { _pagetext = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

