using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class PageImageSelect : DataAccessBase
    {
        private SitePage _sitepage;

        public PageImageSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spPageImage_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            PageImageSelectParameters _sitepageimageselectparameters = new PageImageSelectParameters(SitePage);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _sitepageimageselectparameters.Parameters);

            return ds;
        }

        public SitePage SitePage
        {
            get { return _sitepage; }
            set { _sitepage = value; }
        }
    }


    public class PageImageSelectParameters
    {
        private SitePage _sitepage;
        private SqlParameter[] _parameters;

        public PageImageSelectParameters(SitePage sitepage)
        {
            SitePage = sitepage;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@PageName" , _sitepage.Name),
                new SqlParameter( "@Client" , Client.ClientID),
            };
            Parameters = parameters;
        }

        public SitePage SitePage
        {
            get { return _sitepage; }
            set { _sitepage = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

