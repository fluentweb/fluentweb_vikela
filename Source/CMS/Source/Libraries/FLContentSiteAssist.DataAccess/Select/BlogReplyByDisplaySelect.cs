using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class BlogReplyByDisplaySelect : DataAccessBase
    {
        private Blog _blog;

        public BlogReplyByDisplaySelect()
        {
            StoredProcedureName = StoredProcedure.Name.spBlogReplyByDisplay_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            BlogReplyByDisplaySelectParameters _blogselectparameters = new BlogReplyByDisplaySelectParameters(Blog);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _blogselectparameters.Parameters);

            return ds;
        }

        public Blog Blog
        {
            get { return _blog; }
            set { _blog = value; }
        }
    }


    public class BlogReplyByDisplaySelectParameters
    {
        private Blog _blog;
        private SqlParameter[] _parameters;

        public BlogReplyByDisplaySelectParameters(Blog blog)
        {
            Blog = blog;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@ThreadID" , Blog.ThreadID),
                new SqlParameter( "@Display" , Blog.Display),
            };
            Parameters = parameters;
        }

        public Blog Blog
        {
            get { return _blog; }
            set { _blog = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}
