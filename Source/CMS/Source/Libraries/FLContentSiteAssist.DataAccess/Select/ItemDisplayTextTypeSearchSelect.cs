using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class ItemDisplayTextTypeSearchSelect : DataAccessBase
    {
        private Item _item;

        public ItemDisplayTextTypeSearchSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spItemDisplayTextTypeSearch_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            ItemDisplayTextTypeSearchSelectParameters _itemdisplaytexttypesearchselectparameters = new ItemDisplayTextTypeSearchSelectParameters(Item);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _itemdisplaytexttypesearchselectparameters.Parameters);

            return ds;
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }
    }


    public class ItemDisplayTextTypeSearchSelectParameters
    {
        private Item _item;
        private SqlParameter[] _parameters;

        public ItemDisplayTextTypeSearchSelectParameters(Item item)
        {
            Item = item;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@SearchText" , _item.SearchText),
                new SqlParameter( "@Display" , _item.Display),
                new SqlParameter( "@TextType" , _item.TextType),
                new SqlParameter( "@Client" , Client.ClientID),
            };
            Parameters = parameters;
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

