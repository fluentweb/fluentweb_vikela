using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class LinksSelect : DataAccessBase
    {
        private int _location;

        public LinksSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spLink_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            LinksSelectParameters _linksselectparameters = new LinksSelectParameters(Location);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _linksselectparameters.Parameters);

            return ds;
        }

        public int Location
        {
            get { return _location; }
            set { _location = value; }
        }
    }


    public class LinksSelectParameters
    {
        private int _location;
        private SqlParameter[] _parameters;

        public LinksSelectParameters(int location)
        {
            Location = location;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@Location" , Location),
            };
            Parameters = parameters;
        }

        public int Location
        {
            get { return _location; }
            set { _location = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}

