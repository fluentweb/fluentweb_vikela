using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class ItemTextSelect: DataAccessBase
    {
        private Item _item;

        public ItemTextSelect()
        {
            StoredProcedureName = StoredProcedure.Name.spItemText_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            ItemTextSelectParameters _itemtextselectparameters = new ItemTextSelectParameters(Item);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _itemtextselectparameters.Parameters);

            return ds;
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }
    }


    public class ItemTextSelectParameters
    {
        private Item _item;
        private SqlParameter[] _parameters;

        public ItemTextSelectParameters(Item item)
        {
            Item = item;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@ItemID" , _item.ItemID),
            };
            Parameters = parameters;
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }

}
