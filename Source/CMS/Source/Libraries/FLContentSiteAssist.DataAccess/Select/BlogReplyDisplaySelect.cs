using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.DataAccess.Select
{
    public class BlogReplyDisplaySelect : DataAccessBase
    {
        private Blog _blog;

        public BlogReplyDisplaySelect()
        {
            StoredProcedureName = StoredProcedure.Name.spBlogReplyDisplay_Select.ToString();
        }

        public DataSet Get()
        {
            DataSet ds;

            BlogReplyDisplaySelectParameters _blogselectparameters = new BlogReplyDisplaySelectParameters(Blog);
            DataBaseHelper dbhelper = new DataBaseHelper(StoredProcedureName);
            ds = dbhelper.Run(base.ConnectionString, _blogselectparameters.Parameters);

            return ds;
        }

        public Blog Blog
        {
            get { return _blog; }
            set { _blog = value; }
        }
    }


    public class BlogReplyDisplaySelectParameters
    {
        private Blog _blog;
        private SqlParameter[] _parameters;

        public BlogReplyDisplaySelectParameters(Blog blog)
        {
            Blog = blog;
            Build();
        }

        private void Build()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter( "@DisplayID" , Blog.DisplayID),
            };
            Parameters = parameters;
        }

        public Blog Blog
        {
            get { return _blog; }
            set { _blog = value; }
        }

        public SqlParameter[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }

    }
}
