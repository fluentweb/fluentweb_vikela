using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.Common;
using FLContentSiteAssist.DataAccess.Select;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessGetItemImage : IBusinessLogic
    {
        private DataSet _resultset;
        private Item _item;

        public ProcessGetItemImage()
        {

        }

        public void Invoke()
        {
            ItemImageSelect data = new ItemImageSelect();
            data.Item = Item;
            ResultSet = data.Get();
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }

        public DataSet ResultSet
        {
            get { return _resultset; }
            set { _resultset = value; }
        }
    }
}
