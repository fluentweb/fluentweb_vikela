using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.Common;
using FLContentSiteAssist.BusinessLogic;

namespace FLContentSiteAssist.BusinessLogic
{
    public class InterfaceBuildItemDisplay : IInterfaceBuild
    {
        private Item _item;
        private String _menuhtml;
        private AdminMenu _adminmenu;
        private String _onmouseover;
        private String _onmouseout;
        
        public InterfaceBuildItemDisplay()
        {
        }

        public void Build()
        {

            if (CMS.IsAdmin)
            {


                string _menuid = "itd" + _item.Display + _item.ItemID;

                StringBuilder _html = new StringBuilder();

                _html.Append("<div class=\"edit_menu\" style=\"left:" + AdminMenu.XLoc + "px; top:" + AdminMenu.YLoc + "px;\" id=\"edit_button" + _menuid + "\" onclick=\"show_tools('visible','" + _menuid + "');\" onmouseover=\"edit('visible','" + _menuid + "');\">");
                _html.Append("<img src=\"" + AdminMenu.RootPath + "flcontent/images/spanneroff.jpg\" alt=\"Click to modify\" />");
                _html.Append("<div class=\"edit_menu_drop\" id=\"tools" + _menuid + "\" onmouseout=\"show_tools('hidden','" + _menuid + "');\" onmouseover=\"edit('visible','" + _menuid + "');show_tools('visible','" + _menuid + "');\" >");

                //Display level menu options
                _html.Append("<span class=\"edit_menu_drop_title\">" + _item.Display + "</span>");
                _html.Append("<a href=\"#\" onclick=\"show_tools('hidden','" + _menuid + "');openDialog('itemdisplay',''," + _item.ItemDisplayID + ");return false;\" >Edit display settings</a><br />");
                _html.Append("<a href=\"#\" onclick=\"show_tools('hidden','" + _menuid + "');openDialog('items',''," + _item.ItemTypeID + ");return false;\" >Edit items in this display</a><br />");
                _html.Append("<a href=\"#\" onclick=\"show_tools('hidden','" + _menuid + "');openDialog('itemnew',''," + _item.ItemTypeID + ");return false;\" >Create a new item in this display</a><br />");

                if (_item.ItemID != 0)
                {
                    //Specific item level options
                    _html.Append("<span class=\"edit_menu_drop_title\">" + _item.Title + "</span>");
                    _html.Append("<a href=\"#\" onclick=\"show_tools('hidden','" + _menuid + "');openDialog('itemedit',''," + _item.ItemID + ");return false;\" >Edit this item</a><br />");
                    _html.Append("<a href=\"#\" onclick=\"show_tools('hidden','" + _menuid + "');openDialog('itemtext',''," + _item.ItemID + ");return false;\" >Edit additional text</a><br />");
                    _html.Append("<a href=\"#\" onclick=\"show_tools('hidden','" + _menuid + "');openDialog('itemimage',''," + _item.ItemID + ");return false;\" >Edit additional images</a><br />");
                }

                /*
                string _menuid = "itd" + _item.Display + _item.ItemID;

                StringBuilder _html = new StringBuilder();

                _html.Append("<div class=\"edit_menu\" style=\"left:" + AdminMenu.XLoc + "px; top:" + AdminMenu.YLoc + "px;\" id=\"edit_button" + _menuid + "\" onclick=\"show_tools('visible','" + _menuid + "');\" onmouseover=\"edit('visible','" + _menuid + "');\">");
                _html.Append("<img src=\"" + AdminMenu.RootPath + "flcontent/images/spanneroff.jpg\" alt=\"Click to modify\" />");
                _html.Append("<div class=\"edit_menu_drop\" id=\"tools" + _menuid + "\" onmouseout=\"show_tools('hidden','" + _menuid + "');\" onmouseover=\"edit('visible','" + _menuid + "');show_tools('visible','" + _menuid + "');\" >");

                //Display level menu options
                _html.Append("<span class=\"edit_menu_drop_title\">" + _item.Display + "</span>");
                _html.Append("<a href=\"#\" onclick=\"show_tools('hidden','" + _menuid + "');openDialog('itemdisplay'," + _item.ItemDisplayID + ");\" >Edit display settings</a><br />");
                _html.Append("<a href=\"#\" onclick=\"show_tools('hidden','" + _menuid + "');openDialog('items'," + _item.ItemTypeID + ");\" >Edit items in this display</a><br />");
                _html.Append("<a href=\"#\" onclick=\"show_tools('hidden','" + _menuid + "');openDialog('itemnew'," + _item.ItemTypeID + ");\" >Create a new item in this display</a><br />");

                if (_item.ItemID != 0)
                {
                    //Specific item level options
                    _html.Append("<span class=\"edit_menu_drop_title\">" + _item.Title + "</span>");
                    _html.Append("<a href=\"#\" onclick=\"show_tools('hidden','" + _menuid + "');openDialog('itemedit',0," + _item.ItemID + ");\" >Edit this item</a><br />");
                    _html.Append("<a href=\"#\" onclick=\"show_tools('hidden','" + _menuid + "');openDialog('itemtext',0," + _item.ItemID + ");\" >Edit additional text</a><br />");
                    _html.Append("<a href=\"#\" onclick=\"show_tools('hidden','" + _menuid + "');openDialog('itemimage',0," + _item.ItemID + ");\" >Edit additional images</a><br />");
                }

                //Divs and script to load the dialogs
                //Only required for the first occurrance of multiple items in a diplay
                if (!Item.doNotInit)
                {
                    _html.Append("<div id=\"fliframedivitemdisplay" + _item.ItemDisplayID + "\" title=\"Modify item display '" + _item.Display + "'\" ></div>");
                    _html.Append("<script type=\"text/javascript\">initDialog('itemdisplay'," + _item.ItemDisplayID + ");</script>");
                    _html.Append("<div id=\"fliframedivitems" + _item.ItemTypeID + "\" title=\"Modify items '" + _item.Display + "'\" ></div>");
                    _html.Append("<script type=\"text/javascript\">initDialog('items'," + _item.ItemTypeID + ");</script>");
                    _html.Append("<div id=\"fliframedivitemnew" + _item.ItemTypeID + "\" title=\"Create new item '" + _item.Display + "'\" ></div>");
                    _html.Append("<script type=\"text/javascript\">initDialog('itemnew'," + _item.ItemTypeID + ");</script>");
                    _html.Append("<div id=\"fliframedivitemedit0\" title=\"Edit item\" ></div>");
                    _html.Append("<script type=\"text/javascript\">initDialog('itemedit',0);</script>");

                    if (_item.ItemID != 0)
                    {
                        //Blank id initially for edit item div
                        _html.Append("<div id=\"fliframedivitemtext0\" title=\"Edit item text\" ></div>");
                        _html.Append("<script type=\"text/javascript\">initDialog('itemtext',0);</script>");
                        _html.Append("<div id=\"fliframedivitemimage0\" title=\"Edit item image\" ></div>");
                        _html.Append("<script type=\"text/javascript\">initDialog('itemimage',0);</script>");
                    }
                }
                */
                _html.Append("</div>");
                _html.Append("</div>");
                MenuHTML = _html.ToString();

                OnMouseOver = "edit('visible','" + _menuid + "');";
                OnMouseOut = "edit('hidden','" + _menuid + "');show_tools('hidden','" + _menuid + "');";

            }
        }

        public AdminMenu AdminMenu
        {
            get { return _adminmenu; }
            set { _adminmenu = value; }
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }

        public String MenuHTML
        {
            get { return _menuhtml; }
            set { _menuhtml = value; }
        }

        public String OnMouseOver
        {
            get { return _onmouseover; }
            set { _onmouseover = value; }
        }

        public String OnMouseOut
        {
            get { return _onmouseout; }
            set { _onmouseout = value; }
        } 
    }
}
