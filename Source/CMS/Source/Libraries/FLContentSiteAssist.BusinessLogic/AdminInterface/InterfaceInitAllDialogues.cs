using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.Common;
using FLContentSiteAssist.BusinessLogic;

namespace FLContentSiteAssist.BusinessLogic
{
    public class InterfaceInitAllDialogues : IInterfaceBuild
    {
        private string _initDialogHTML;

        public InterfaceInitAllDialogues()
        {
        }

        public void Build()
        {
            if (CMS.IsAdmin)
            {
                
                StringBuilder _html = new StringBuilder();

                _html.Append("<div id=\"fliframedivitemdisplay\" title=\"Modify item display\" ></div>");
                _html.Append("<script type=\"text/javascript\">initDialog('itemdisplay');</script>");
                
                _html.Append("<div id=\"fliframedivitems\" title=\"Modify items\" ></div>");
                _html.Append("<script type=\"text/javascript\">initDialog('items');</script>");
                
                _html.Append("<div id=\"fliframedivitemnew\" title=\"Create new item\" ></div>");
                _html.Append("<script type=\"text/javascript\">initDialog('itemnew');</script>");
                
                _html.Append("<div id=\"fliframedivitemedit\" title=\"Edit item\" ></div>");
                _html.Append("<script type=\"text/javascript\">initDialog('itemedit');</script>");
                
                _html.Append("<div id=\"fliframedivitemtext\" title=\"Edit item text\" ></div>");
                _html.Append("<script type=\"text/javascript\">initDialog('itemtext');</script>");
                
                _html.Append("<div id=\"fliframedivitemimage\" title=\"Edit item image\" ></div>");
                _html.Append("<script type=\"text/javascript\">initDialog('itemimage');</script>");
                
                _html.Append("<div id=\"fliframedivlinkloc\" title=\"Modify links\" ></div>");
                _html.Append("<script type=\"text/javascript\">initDialog('linkloc');</script>");
                
                _html.Append("<div id=\"fliframedivpage\" title=\"Modify details for this page\" ></div>");
                _html.Append("<script type=\"text/javascript\">initDialog('page');</script>");
                
                _html.Append("<div id=\"fliframedivpagetext\" title=\"Modify text\" ></div>");
                _html.Append("<script type=\"text/javascript\">initDialog('pagetext');</script>");

                _html.Append("<div id=\"fliframedivanythingdelete\" title=\"Delete\" ></div>");
                _html.Append("<script type=\"text/javascript\">initDialog('anythingdelete');</script>");

                _html.Append("<div id=\"fliframedivblogthreaddisplay\" title=\"Edit blog thread display\" ></div>");
                _html.Append("<script type=\"text/javascript\">initDialog('blogthreaddisplay');</script>");

                _html.Append("<div id=\"fliframedivblogthreadnew\" title=\"Create new blog thread\" ></div>");
                _html.Append("<script type=\"text/javascript\">initDialog('blogthreadnew');</script>");

                _html.Append("<div id=\"fliframedivblogthreadedit\" title=\"Edit blog thread\" ></div>");
                _html.Append("<script type=\"text/javascript\">initDialog('blogthreadedit');</script>");

                _html.Append("<div id=\"fliframedivblogreplydisplay\" title=\"Edit blog thread reply display\" ></div>");
                _html.Append("<script type=\"text/javascript\">initDialog('blogreplydisplay');</script>");
                    
                InitDialogHTML = _html.ToString();

            }
        }

        public string InitDialogHTML
        {
            get { return _initDialogHTML; }
            set { _initDialogHTML = value; }
        }

    }
}
