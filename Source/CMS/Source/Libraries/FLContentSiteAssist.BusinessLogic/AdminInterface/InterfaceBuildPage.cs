using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.Common;
using FLContentSiteAssist.BusinessLogic;

namespace FLContentSiteAssist.BusinessLogic
{
    public class InterfaceBuildPage : IInterfaceBuild
    {
        private Page _page;
        private String _menuhtml;
        private AdminMenu _adminmenu;
        private String _onmouseover;
        private String _onmouseout;

        public InterfaceBuildPage()
        {

        }

        public void Build()
        {
            if (CMS.IsAdmin)
            {

                ProcessGetPageByID _getpage = new ProcessGetPageByID();
                _getpage.Page = Page;
                _getpage.Invoke();

                if (_getpage.ResultSet.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = _getpage.ResultSet.Tables[0].Rows[0];

                    string _menuid = "pg" + Page.PageName;

                    StringBuilder _html = new StringBuilder();

                    _html.Append("<div class=\"edit_menu\" style=\"left:" + AdminMenu.XLoc + "px; top:" + AdminMenu.YLoc + "px;\" id=\"edit_button" + _menuid + "\" onclick=\"show_tools('visible','" + _menuid + "');\" onmouseover=\"edit('visible','" + _menuid + "');\">");
                    _html.Append("<img src=\"" + AdminMenu.RootPath + "flcontent/images/spanneroff.jpg\" alt=\"Click to modify\" />");
                    _html.Append("<div class=\"edit_menu_drop\" id=\"tools" + _menuid + "\" onmouseout=\"show_tools('hidden','" + _menuid + "');\" onmouseover=\"edit('visible','" + _menuid + "');show_tools('visible','" + _menuid + "');\" >");
                    _html.Append("<a href=\"#\" onclick=\"show_tools('hidden','" + _menuid + "');openDialog('page',''," + dr["intPageID"] + ");return false;\" >Edit page details</a><br />");
                    _html.Append("</div>");
                    _html.Append("</div>");
                    MenuHTML = _html.ToString();

                    OnMouseOver = "edit('visible','" + _menuid + "');";
                    OnMouseOut = "edit('hidden','" + _menuid + "');show_tools('hidden','" + _menuid + "');";
                }
                else
                {
                    MenuHTML = "";
                }
            }
        }

        public AdminMenu AdminMenu
        {
            get { return _adminmenu; }
            set { _adminmenu = value; }
        }

        public Page Page
        {
            get { return _page; }
            set { _page = value; }
        }

        public String MenuHTML
        {
            get { return _menuhtml; }
            set { _menuhtml = value; }
        }

        public String OnMouseOver
        {
            get { return _onmouseover; }
            set { _onmouseover = value; }
        }

        public String OnMouseOut
        {
            get { return _onmouseout; }
            set { _onmouseout = value; }
        } 
    }
}
