using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.Common;
using FLContentSiteAssist.BusinessLogic;

namespace FLContentSiteAssist.BusinessLogic
{
    public class InterfaceBuildLinkLocation : IInterfaceBuild
    {
        private Link _link;
        private String _menuhtml;
        private AdminMenu _adminmenu;
        private String _onmouseover;
        private String _onmouseout;

        public InterfaceBuildLinkLocation()
        {

        }

        public void Build()
        {

            if (CMS.IsAdmin)
            {

                ProcessGetLinkByLocationName _getlink = new ProcessGetLinkByLocationName();
                _getlink.Link = Link;
                _getlink.Invoke();

                if (_getlink.ResultSet.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = _getlink.ResultSet.Tables[0].Rows[0];

                    StringBuilder _html = new StringBuilder();

                    string _menuid = "ll" + Link.Location;

                    _html.Append("<div class=\"edit_menu\" style=\"left:" + AdminMenu.XLoc + "px; top:" + AdminMenu.YLoc + "px;\" id=\"edit_button" + _menuid + "\" onclick=\"show_tools('visible','" + _menuid + "');\" onmouseover=\"edit('visible','" + _menuid + "');\">");
                    _html.Append("<img src=\"" + AdminMenu.RootPath + "flcontent/images/spanneroff.jpg\" alt=\"Click to modify\" />");
                    _html.Append("<div class=\"edit_menu_drop\" id=\"tools" + _menuid + "\" onmouseout=\"show_tools('hidden','" + _menuid + "');\" onmouseover=\"edit('visible','" + _menuid + "');show_tools('visible','" + _menuid + "');\" >");
                    _html.Append("<a href=\"#\" onclick=\"show_tools('hidden','" + _menuid + "');openDialog('linkloc',''," + dr["intLinkLocationID"] + ");return false;\" >Edit these links</a><br />");
                    _html.Append("</div>");
                    _html.Append("</div>");
                    MenuHTML = _html.ToString();

                    OnMouseOver = "edit('visible','" + _menuid + "');";
                    OnMouseOut = "edit('hidden','" + _menuid + "');show_tools('hidden','" + _menuid + "');";

                }
                else
                {
                    MenuHTML = "";
                }
            }
        }


        public AdminMenu AdminMenu
    	{
		    get { return _adminmenu; }
            set { _adminmenu = value; }
	    } 

        public Link Link
    	{
		    get { return _link; }
            set { _link = value; }
	    } 
    
        public String MenuHTML
    	{
		    get { return _menuhtml; }
		    set { _menuhtml = value; }
	    }

        public String OnMouseOver
        {
            get { return _onmouseover; }
            set { _onmouseover = value; }
        }

        public String OnMouseOut
        {
            get { return _onmouseout; }
            set { _onmouseout = value; }
        } 

    }
}
