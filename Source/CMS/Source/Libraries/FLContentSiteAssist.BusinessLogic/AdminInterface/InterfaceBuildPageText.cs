using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.Common;
using FLContentSiteAssist.BusinessLogic;

namespace FLContentSiteAssist.BusinessLogic
{
    public class InterfaceBuildPageText : IInterfaceBuild
    {
        private PageText _pagetext;
        private String _menuhtml;
        private AdminMenu _adminmenu;
        private String _onmouseover;
        private String _onmouseout;
        
        public InterfaceBuildPageText()
        {
        }

        public void Build()
        {

            if (CMS.IsAdmin)
            {

                string _menuid = "pt" + PageText.TextType;

                StringBuilder _html = new StringBuilder();

                _html.Append("<div class=\"edit_menu\" style=\"left:" + AdminMenu.XLoc + "px; top:" + AdminMenu.YLoc + "px;\" id=\"edit_button" + _menuid + "\" onclick=\"show_tools('visible','" + _menuid + "');\" onmouseover=\"edit('visible','" + _menuid + "');\">");
                _html.Append("<img src=\"" + AdminMenu.RootPath + "flcontent/images/spanneroff.jpg\" alt=\"Click to modify\" />");
                _html.Append("<div class=\"edit_menu_drop\" id=\"tools" + _menuid + "\" onmouseout=\"show_tools('hidden','" + _menuid + "');\" onmouseover=\"edit('visible','" + _menuid + "');show_tools('visible','" + _menuid + "');\" >");
                _html.Append("<a href=\"#\" onclick=\"show_tools('hidden','" + _menuid + "');openDialog('pagetext',''," + _pagetext.TextID + ");return false;\" >Edit this text</a><br />");
                _html.Append("</div>");
                _html.Append("</div>");
                MenuHTML = _html.ToString();

                OnMouseOver = "edit('visible','" + _menuid + "');";
                OnMouseOut = "edit('hidden','" + _menuid + "');show_tools('hidden','" + _menuid + "');";

            }
        }

        public AdminMenu AdminMenu
        {
            get { return _adminmenu; }
            set { _adminmenu = value; }
        }

        public PageText PageText
        {
            get { return _pagetext; }
            set { _pagetext = value; }
        }

        public String MenuHTML
        {
            get { return _menuhtml; }
            set { _menuhtml = value; }
        }

        public String OnMouseOver
        {
            get { return _onmouseover; }
            set { _onmouseover = value; }
        }

        public String OnMouseOut
        {
            get { return _onmouseout; }
            set { _onmouseout = value; }
        } 
    }
}
