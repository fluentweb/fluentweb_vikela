using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.Common;
using FLContentSiteAssist.BusinessLogic;

namespace FLContentSiteAssist.BusinessLogic
{
    public class InterfaceBuildBlogReplyDisplay : IInterfaceBuild
    {
        private Blog _blog;
        private String _menuhtml;
        private AdminMenu _adminmenu;
        private String _onmouseover;
        private String _onmouseout;

        public InterfaceBuildBlogReplyDisplay()
        {
        }

        public void Build()
        {

            if (CMS.IsAdmin)
            {


                string _menuid = "itd" + _blog.Display + _blog.ReplyID;

                StringBuilder _html = new StringBuilder();

                _html.Append("<div class=\"edit_menu\" style=\"left:" + AdminMenu.XLoc + "px; top:" + AdminMenu.YLoc + "px;\" id=\"edit_button" + _menuid + "\" onclick=\"show_tools('visible','" + _menuid + "');\" onmouseover=\"edit('visible','" + _menuid + "');\">");
                _html.Append("<img src=\"" + AdminMenu.RootPath + "flcontent/images/spanneroff.jpg\" alt=\"Click to modify\" />");
                _html.Append("<div class=\"edit_menu_drop\" id=\"tools" + _menuid + "\" onmouseout=\"show_tools('hidden','" + _menuid + "');\" onmouseover=\"edit('visible','" + _menuid + "');show_tools('visible','" + _menuid + "');\" >");

                //Display level menu options
                _html.Append("<span class=\"edit_menu_drop_title\">" + _blog.Display + "</span>");
                _html.Append("<a href=\"#\" onclick=\"show_tools('hidden','" + _menuid + "');openDialog('blogreplydisplay',''," + _blog.DisplayID + ");return false;\" >Edit display settings</a><br />");

                if (_blog.ThreadID != 0)
                {
                    //Specific thread reply level options
                    _html.Append("<span class=\"edit_menu_drop_title\">" + _blog.Title + "</span>");
                    _html.Append("<a href=\"#\" onclick=\"show_tools('hidden','" + _menuid + "');openDialog('anythingdelete','thrrep'," + _blog.ReplyID + ");return false;\" >Delete this reply</a><br />");
                }

                _html.Append("</div>");
                _html.Append("</div>");
                MenuHTML = _html.ToString();

                OnMouseOver = "edit('visible','" + _menuid + "');";
                OnMouseOut = "edit('hidden','" + _menuid + "');show_tools('hidden','" + _menuid + "');";

            }
        }

        public AdminMenu AdminMenu
        {
            get { return _adminmenu; }
            set { _adminmenu = value; }
        }

        public Blog Blog
        {
            get { return _blog; }
            set { _blog = value; }
        }

        public String MenuHTML
        {
            get { return _menuhtml; }
            set { _menuhtml = value; }
        }

        public String OnMouseOver
        {
            get { return _onmouseover; }
            set { _onmouseover = value; }
        }

        public String OnMouseOut
        {
            get { return _onmouseout; }
            set { _onmouseout = value; }
        }
    }
}
