using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.DataAccess.Select;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessGetClient
    {
        private DataSet _resultset;

        public ProcessGetClient()
        {
        }

        public void Invoke()
        {
            ClientSelect client = new ClientSelect();
            ResultSet = client.Get();
        }

        public DataSet ResultSet
        {
            get { return _resultset; }
            set { _resultset = value; }
        }
    }
}
