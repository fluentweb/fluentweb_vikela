using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.Common;
using FLContentSiteAssist.DataAccess.Select;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessGetPageText : IBusinessLogic
    {
        private DataSet _resultset;
        private SitePage _sitepage;

        public ProcessGetPageText()
        {

        }

        public void Invoke()
        {
            PageTextSelect data = new PageTextSelect();
            data.SitePage = SitePage;
            ResultSet = data.Get();
        }

        public SitePage SitePage
        {
            get { return _sitepage; }
            set { _sitepage = value; }
        }

        public DataSet ResultSet
        {
            get { return _resultset; }
            set { _resultset = value; }
        }
    }
}

