using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.DataAccess.Select;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessGetLinkLocation
    {
        private DataSet _resultset;
        private string _location;

        public ProcessGetLinkLocation()
        {
        }

        public void Invoke()
        {
            LinkLocationSelect linklocation = new LinkLocationSelect();
            linklocation.Location = Location;
            ResultSet = linklocation.Get();
        }

        public string Location
        {
            get { return _location; }
            set { _location = value; }
        }

        public DataSet ResultSet
        {
            get { return _resultset; }
            set { _resultset = value; }
        }
    }
}
