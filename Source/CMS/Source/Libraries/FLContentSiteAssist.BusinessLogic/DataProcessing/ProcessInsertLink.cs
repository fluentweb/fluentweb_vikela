using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.DataAccess.Insert;
using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessInsertLink
    {
        private Link _link;

        public ProcessInsertLink()
        {
        }

        public void Invoke()
        {
            LinkInsert insertlink = new LinkInsert();
            insertlink.Link = Link;
            insertlink.Insert();
        }

        public Link Link
        {
            get { return _link; }
            set { _link = value; }
        }

    }
}

