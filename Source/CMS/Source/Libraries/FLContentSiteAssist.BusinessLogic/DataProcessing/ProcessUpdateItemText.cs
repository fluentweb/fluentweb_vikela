using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.DataAccess.Update;
using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessUpdateItemText
    {
        private ItemText _itemtext;

        public ProcessUpdateItemText()
        {
        }

        public void Invoke()
        {
            ItemTextUpdate updatelink = new ItemTextUpdate();
            updatelink.ItemText = ItemText;
            updatelink.Update();
        }

        public ItemText ItemText
        {
            get { return _itemtext; }
            set { _itemtext = value; }
        }

    }
}

