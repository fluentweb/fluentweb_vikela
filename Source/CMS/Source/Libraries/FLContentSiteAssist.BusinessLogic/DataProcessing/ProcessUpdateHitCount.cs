using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.DataAccess.Update;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessUpdateHitCount : IBusinessLogic
    {
        private DataSet _resultset;

        public ProcessUpdateHitCount()
        {

        }

        public void Invoke()
        {
            HitCountUpdate hitcount = new HitCountUpdate();
            hitcount.Update();
        }

        public DataSet ResultSet
        {
            get { return _resultset; }
            set { _resultset = value; }
        }

    }
}
