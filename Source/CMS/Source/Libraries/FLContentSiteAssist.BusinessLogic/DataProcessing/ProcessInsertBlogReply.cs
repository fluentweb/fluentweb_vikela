using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.DataAccess.Insert;
using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessInsertBlogReply
    {
        private Blog _blog;

        public ProcessInsertBlogReply()
        {
        }

        public void Invoke()
        {
            BlogReplyInsert insertblogreply = new BlogReplyInsert();
            insertblogreply.Blog = Blog;
            insertblogreply.Insert();
        }

        public Blog Blog
        {
            get { return _blog; }
            set { _blog = value; }
        }

    }
}

