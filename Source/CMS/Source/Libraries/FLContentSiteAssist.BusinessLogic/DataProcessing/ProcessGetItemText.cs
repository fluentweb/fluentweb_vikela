using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.Common;
using FLContentSiteAssist.DataAccess.Select;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessGetItemText : IBusinessLogic
    {
        private DataSet _resultset;
        private Item _item;

        public ProcessGetItemText()
        {

        }

        public void Invoke()
        {
            ItemTextSelect data = new ItemTextSelect();
            data.Item = Item;
            ResultSet = data.Get();
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }

        public DataSet ResultSet
        {
            get { return _resultset; }
            set { _resultset = value; }
        }
    }
}
