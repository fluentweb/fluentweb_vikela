using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.Common;
using FLContentSiteAssist.DataAccess.Select;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessGetPassword : IBusinessLogic
    {
        private DataSet _resultset;
        private string _email;
        private string _password;

        public ProcessGetPassword()
        {
        }

        public void Invoke()
        {
            PasswordSelect passworddata = new PasswordSelect();
            passworddata.Email = Email;

            _resultset = passworddata.Get();

            Password = _resultset.Tables[0].Rows[0]["chPassword"].ToString();
        }

        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

    }
}
