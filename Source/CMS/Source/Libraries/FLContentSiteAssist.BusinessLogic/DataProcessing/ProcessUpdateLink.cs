using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.DataAccess.Update;
using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessUpdateLink
    {
        private  Link _link;

        public ProcessUpdateLink()
        {
        }

        public void Invoke()
        {
            LinkUpdate updatelink = new LinkUpdate();
            updatelink.Link = Link;
            updatelink.Update();
        }

        public Link Link
        {
            get { return _link; }
            set { _link = value; }
        }

    }
}

