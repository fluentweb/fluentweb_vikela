using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.Common;
using FLContentSiteAssist.DataAccess.Select;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessGetPageImageByID : IBusinessLogic
    {
        private DataSet _resultset;
        private PageImage _pageimage;

        public ProcessGetPageImageByID()
        {

        }

        public void Invoke()
        {
            PageImageByIDSelect data = new PageImageByIDSelect();
            data.PageImage = PageImage;
            ResultSet = data.Get();
        }

        public PageImage PageImage
        {
            get { return _pageimage; }
            set { _pageimage = value; }
        }

        public DataSet ResultSet
        {
            get { return _resultset; }
            set { _resultset = value; }
        }
    }
}

