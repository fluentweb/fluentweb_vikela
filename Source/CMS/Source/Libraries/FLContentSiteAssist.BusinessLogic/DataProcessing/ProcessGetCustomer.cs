using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.Common;
using FLContentSiteAssist.DataAccess.Select;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessGetCustomer : IBusinessLogic
    {
        private DataSet _resultset;
        private Customer _customer;

        public ProcessGetCustomer()
        {

        }

        public void Invoke()
        {
            CustomerSelect customerdata = new CustomerSelect();
            customerdata.Customer = Customer;
            ResultSet = customerdata.Get();
        }

        public Customer Customer
        {
            get { return _customer; }
            set { _customer = value; }
        }

        public DataSet ResultSet
        {
            get { return _resultset; }
            set { _resultset = value; }
        }

    }
}
