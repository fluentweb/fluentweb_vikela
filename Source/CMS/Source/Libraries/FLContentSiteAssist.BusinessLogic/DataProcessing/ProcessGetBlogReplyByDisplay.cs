using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.DataAccess.Select;
using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessGetBlogReplyByDisplay
    {
        private DataSet _resultset;
        private Blog _blog;

        public ProcessGetBlogReplyByDisplay()
        {
        }

        public void Invoke()
        {
            BlogReplyByDisplaySelect blogdata = new BlogReplyByDisplaySelect();
            blogdata.Blog = Blog;
            ResultSet = blogdata.Get();
        }

        public Blog Blog
        {
            get { return _blog; }
            set { _blog = value; }
        }

        public DataSet ResultSet
        {
            get { return _resultset; }
            set { _resultset = value; }
        }
    }
}
