using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.DataAccess.Select;
using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessGetPageByURL
    {
        private DataSet _resultset;
        private Page _page;

        public ProcessGetPageByURL()
        {
        }

        public void Invoke()
        {
            PageByURLSelect pagedata = new PageByURLSelect();
            pagedata.Page = Page;
            ResultSet = pagedata.Get();
        }

        public Page Page
        {
            get { return _page; }
            set { _page = value; }
        }

        public DataSet ResultSet
        {
            get { return _resultset; }
            set { _resultset = value; }
        }
    }
}
