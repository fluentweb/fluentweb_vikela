using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.DataAccess.Update;
using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessUpdatePageText
    {
        private PageText _pagetext;

        public ProcessUpdatePageText()
        {
        }

        public void Invoke()
        {
            PageTextUpdate updatepagetext = new PageTextUpdate();
            updatepagetext.PageText = PageText;
            updatepagetext.Update();
        }

        public PageText PageText
        {
            get { return _pagetext; }
            set { _pagetext = value; }
        }

    }
}

