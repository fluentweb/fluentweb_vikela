using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.Common;
using FLContentSiteAssist.DataAccess.Select;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessGetPageTextByID : IBusinessLogic
    {
        private DataSet _resultset;
        private PageText _pagetext;

        public ProcessGetPageTextByID()
        {

        }

        public void Invoke()
        {
            PageTextByIDSelect data = new PageTextByIDSelect();
            data.PageText = PageText;
            ResultSet = data.Get();
        }

        public PageText PageText
        {
            get { return _pagetext; }
            set { _pagetext = value; }
        }

        public DataSet ResultSet
        {
            get { return _resultset; }
            set { _resultset = value; }
        }
    }
}

