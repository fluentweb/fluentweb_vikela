using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.DataAccess.Update;
using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessUpdateItemBasic
    {
        private Item _item;

        public ProcessUpdateItemBasic()
        {
        }

        public void Invoke()
        {
            ItemBasicUpdate updateitem = new ItemBasicUpdate();
            updateitem.Item = Item;
            updateitem.Update();
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }

    }
}

