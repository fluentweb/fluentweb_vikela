using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.DataAccess.Delete;
using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessDeletePage
    {
        private Page _page;

        public ProcessDeletePage()
        {
        }

        public void Invoke()
        {
            PageDelete deletepage = new PageDelete();
            deletepage.Page = Page;
            deletepage.Delete();
        }

        public Page Page
        {
            get { return _page; }
            set { _page = value; }
        }

    }
}

