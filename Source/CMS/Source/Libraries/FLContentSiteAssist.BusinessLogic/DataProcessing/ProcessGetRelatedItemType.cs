﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using FLContentSiteAssist.Common;
using FLContentSiteAssist.DataAccess.Select;

namespace FLContentSiteAssist.BusinessLogic.DataProcessing
{
    public class ProcessGetRelatedItemType: IBusinessLogic
    {
        private DataSet _resultset;
        private RelatedItem _relateditem;

        public ProcessGetRelatedItemType()
        {

        }

        public void Invoke()
        {
            RelatedItemTypeSelect data = new RelatedItemTypeSelect();
            data.RelatedItem = RelatedItem;
            ResultSet = data.Get();
        }

        public RelatedItem RelatedItem
        {
            get { return _relateditem; }
            set { _relateditem = value; }
        }

        public DataSet ResultSet
        {
            get { return _resultset; }
            set { _resultset = value; }
        }
    }
}
