using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.DataAccess.Insert;
using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessInsertRelatedItem
    {
        private RelatedItem _relateditem;

        public ProcessInsertRelatedItem()
        {
        }

        public void Invoke()
        {
            RelatedItemInsert insertrelateditem = new RelatedItemInsert();
            insertrelateditem.RelatedItem = RelatedItem;
            insertrelateditem.Insert();
        }

        public RelatedItem RelatedItem
        {
            get { return _relateditem; }
            set { _relateditem = value; }
        }

    }
}

