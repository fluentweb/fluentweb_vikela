using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.DataAccess.Select;
using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessGetLinkByLocationName
    {
        private DataSet _resultset;
        private Link _link;

        public ProcessGetLinkByLocationName()
        {
        }

        public void Invoke()
        {
            LinkByLocationNameSelect linklocation = new LinkByLocationNameSelect();
            linklocation.Link = Link;
            ResultSet = linklocation.Get();
        }

        public Link Link
        {
            get { return _link; }
            set { _link = value; }
        }

        public DataSet ResultSet
        {
            get { return _resultset; }
            set { _resultset = value; }
        }
    }
}
