using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.Common;
using FLContentSiteAssist.DataAccess.Update;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessUpdateCustomer : IBusinessLogic
    {
        private DataSet _resultset;
        private Customer _customer;

        public ProcessUpdateCustomer()
        {

        }

        public void Invoke()
        {
            CustomerUpdate customerupdate = new CustomerUpdate();
            customerupdate.Customer = Customer;
            customerupdate.Update();
        }

        public Customer Customer
        {
            get { return _customer; }
            set { _customer = value; }
        }

        public DataSet ResultSet
        {
            get { return _resultset; }
            set { _resultset = value; }
        }

    }
}
