using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.DataAccess.Update;
using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessUpdateItemComplete
    {
        private Item _item;

        public ProcessUpdateItemComplete()
        {
        }

        public void Invoke()
        {
            ItemCompleteUpdate updateitem = new ItemCompleteUpdate();
            updateitem.Item = Item;
            updateitem.Update();
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }

    }
}

