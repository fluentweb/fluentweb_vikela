using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.Common;
using FLContentSiteAssist.DataAccess.Select;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessGetTitles : IBusinessLogic
    {
        private DataSet _resultset;

        public ProcessGetTitles()
        {
        }

        public void Invoke()
        {
            TitleSelect titleselect = new TitleSelect();

            ResultSet = titleselect.Get();
        }

        public DataSet ResultSet
        {
            get { return _resultset; }
            set { _resultset = value; }
        }
    }
}
