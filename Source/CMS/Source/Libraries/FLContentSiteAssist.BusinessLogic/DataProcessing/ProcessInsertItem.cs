using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.DataAccess.Insert;
using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessInsertItem
    {
        private Item _item;

        public ProcessInsertItem()
        {
        }

        public void Invoke()
        {
            ItemInsert insertitem = new ItemInsert();
            insertitem.Item = Item;
            insertitem.Insert();
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }

    }
}

