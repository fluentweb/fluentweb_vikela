using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.Common;
using FLContentSiteAssist.DataAccess.Select;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessGetPageTemplates : IBusinessLogic
    {
        private DataSet _resultset;

        public ProcessGetPageTemplates()
        {
        }

        public void Invoke()
        {
            PageTemplateSelect titleselect = new PageTemplateSelect();

            ResultSet = titleselect.Get();
        }

        public DataSet ResultSet
        {
            get { return _resultset; }
            set { _resultset = value; }
        }
    }
}
