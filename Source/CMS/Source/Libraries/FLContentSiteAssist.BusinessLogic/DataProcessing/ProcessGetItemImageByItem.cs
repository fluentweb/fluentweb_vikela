using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.Common;
using FLContentSiteAssist.DataAccess.Select;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessGetItemImageByItem : IBusinessLogic
    {
        private DataSet _resultset;
        private Item _item;

        public ProcessGetItemImageByItem()
        {

        }

        public void Invoke()
        {
            ItemImageByItemSelect data = new ItemImageByItemSelect();
            data.Item = Item;
            ResultSet = data.Get();
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }

        public DataSet ResultSet
        {
            get { return _resultset; }
            set { _resultset = value; }
        }
    }
}
