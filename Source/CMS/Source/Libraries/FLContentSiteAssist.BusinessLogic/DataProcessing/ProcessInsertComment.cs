using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.Common;
using FLContentSiteAssist.DataAccess.Select;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessInsertComment : IBusinessLogic
    {
        private Int32 _id;
        private Comment _comment;

        public ProcessInsertComment()
        {

        }

        public void Invoke()
        {
            CommentInsertSelect data = new CommentInsertSelect();
            data.Comments = Comment;
            ID = data.Get();
        }

        public Comment Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        public Int32 ID
        {
            get { return _id; }
            set { _id = value; }
        }
    }
}

