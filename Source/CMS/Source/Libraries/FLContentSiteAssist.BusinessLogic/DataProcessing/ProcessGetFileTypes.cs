﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.Common;
using FLContentSiteAssist.DataAccess.Select;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessGetFileTypes : IBusinessLogic
    {
        private DataSet _resultset;

        public ProcessGetFileTypes()
        {
        }

        public void Invoke()
        {
            FileTypeSelect titleselect = new FileTypeSelect();

            ResultSet = titleselect.Get();
        }

        public DataSet ResultSet
        {
            get { return _resultset; }
            set { _resultset = value; }
        }
    }
}
