using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.DataAccess.Delete;
using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessDeleteItem
    {
        private Item _item;

        public ProcessDeleteItem()
        {
        }

        public void Invoke()
        {
            ItemDelete deleteitem = new ItemDelete();
            deleteitem.Item = Item;
            deleteitem.Delete();
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }

    }
}

