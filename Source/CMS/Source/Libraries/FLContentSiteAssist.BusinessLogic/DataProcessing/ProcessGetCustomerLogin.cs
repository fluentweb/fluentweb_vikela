using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.Common;
using FLContentSiteAssist.DataAccess.Select;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessGetCustomerLogin : IBusinessLogic
    {
        private DataSet _resultset;
        private Customer _customer;

        public ProcessGetCustomerLogin()
        {

        }

        public void Invoke()
        {
            LoginCustomerSelect logindata = new LoginCustomerSelect();
            logindata.Customer = Customer;
            ResultSet = logindata.Get();
        }

        public Customer Customer
        {
            get { return _customer; }
            set { _customer = value; }
        }

        public DataSet ResultSet
        {
            get { return _resultset; }
            set { _resultset = value; }
        }

    }
}
