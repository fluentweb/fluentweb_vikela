using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.Common;
using FLContentSiteAssist.DataAccess.Select;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessGetTitleID
    {
        private DataSet _resultset;
        private string _title;

        public ProcessGetTitleID()
        {
        }

        public void Invoke()
        {
            TitleIDSelect titleid = new TitleIDSelect();
            titleid.Title = Title;
            ResultSet = titleid.Get();
        }

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        public DataSet ResultSet
        {
            get { return _resultset; }
            set { _resultset = value; }
        }
    }
}
