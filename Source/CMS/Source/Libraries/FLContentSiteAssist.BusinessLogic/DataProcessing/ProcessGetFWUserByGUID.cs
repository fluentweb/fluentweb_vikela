using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.Common;
using FLContentSiteAssist.DataAccess.Select;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessGetFWUserByGUID : IBusinessLogic
    {
        private DataSet _resultset;
        private FWUser _fwuser;

        public ProcessGetFWUserByGUID()
        {

        }

        public void Invoke()
        {
            FWUserByGUIDSelect data = new FWUserByGUIDSelect();
            data.FWUser = FWUser;
            ResultSet = data.Get();
        }

        public FWUser FWUser
        {
            get { return _fwuser; }
            set { _fwuser = value; }
        }

        public DataSet ResultSet
        {
            get { return _resultset; }
            set { _resultset = value; }
        }
    }
}

