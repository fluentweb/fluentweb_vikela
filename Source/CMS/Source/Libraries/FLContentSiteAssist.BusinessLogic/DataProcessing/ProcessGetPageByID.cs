using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.DataAccess.Select;
using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessGetPageByID
    {
        private DataSet _resultset;
        private Page _page;

        public ProcessGetPageByID()
        {
        }

        public void Invoke()
        {
            PageByIDSelect pagedata = new PageByIDSelect();
            pagedata.Page = Page;
            ResultSet = pagedata.Get();
        }

        public Page Page
        {
            get { return _page; }
            set { _page = value; }
        }

        public DataSet ResultSet
        {
            get { return _resultset; }
            set { _resultset = value; }
        }
    }
}
