using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.Common;
using FLContentSiteAssist.DataAccess.Select;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessGetRelatedItemAvailable : IBusinessLogic
    {
        private DataSet _resultset;
        private RelatedItem _relateditem;

        public ProcessGetRelatedItemAvailable()
        {

        }

        public void Invoke()
        {
            RelatedItemAvailableSelect data = new RelatedItemAvailableSelect();
            data.RelatedItem = RelatedItem;
            ResultSet = data.Get();
        }

        public RelatedItem RelatedItem
        {
            get { return _relateditem; }
            set { _relateditem = value; }
        }

        public DataSet ResultSet
        {
            get { return _resultset; }
            set { _resultset = value; }
        }
    }
}

