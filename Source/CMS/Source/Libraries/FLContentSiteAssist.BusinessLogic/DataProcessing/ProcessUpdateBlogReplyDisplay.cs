using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.DataAccess.Update;
using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessUpdateBlogReplyDisplay
    {
        private Blog _blog;

        public ProcessUpdateBlogReplyDisplay()
        {
        }

        public void Invoke()
        {
            BlogReplyDisplayUpdate updateblogthread = new BlogReplyDisplayUpdate();
            updateblogthread.Blog = Blog;
            updateblogthread.Update();
        }

        public Blog Blog
        {
            get { return _blog; }
            set { _blog = value; }
        }

    }
}

