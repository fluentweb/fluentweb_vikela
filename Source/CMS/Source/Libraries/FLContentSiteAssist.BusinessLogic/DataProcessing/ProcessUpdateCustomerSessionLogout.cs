using System;
using System.Collections.Generic;
using System.Text;

using FLContentSiteAssist.Common;
using FLContentSiteAssist.DataAccess.Update;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessUpdateCustomerSessionLogout : IBusinessLogic
    {
        private Customer _customer;

        public ProcessUpdateCustomerSessionLogout()
        {

        }

        public void Invoke()
        {
            CustomerSessionLogoutUpdate customerdata = new CustomerSessionLogoutUpdate();
            customerdata.Customer = this.Customer;
            customerdata.Update();
        }

        public Customer Customer
        {
            get { return _customer; }
            set { _customer = value; }
        }
    }
}