using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.DataAccess.Update;
using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessUpdateBlogThreadDisplay
    {
        private Blog _blog;

        public ProcessUpdateBlogThreadDisplay()
        {
        }

        public void Invoke()
        {
            BlogThreadDisplayUpdate updateblogthread = new BlogThreadDisplayUpdate();
            updateblogthread.Blog = Blog;
            updateblogthread.Update();
        }

        public Blog Blog
        {
            get { return _blog; }
            set { _blog = value; }
        }

    }
}

