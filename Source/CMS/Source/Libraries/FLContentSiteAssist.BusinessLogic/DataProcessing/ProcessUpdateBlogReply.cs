using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.DataAccess.Update;
using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessUpdateBlogReply
    {
        private Blog _blog;

        public ProcessUpdateBlogReply()
        {
        }

        public void Invoke()
        {
            BlogReplyUpdate updateblogreply = new BlogReplyUpdate();
            updateblogreply.Blog = Blog;
            updateblogreply.Update();
        }

        public Blog Blog
        {
            get { return _blog; }
            set { _blog = value; }
        }

    }
}

