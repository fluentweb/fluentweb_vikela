using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.DataAccess.Update;
using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessUpdatePageBasic
    {
        private Page _page;

        public ProcessUpdatePageBasic()
        {
        }

        public void Invoke()
        {
            PageBasicUpdate updatepage = new PageBasicUpdate();
            updatepage.Page = Page;
            updatepage.Update();
        }

        public Page Page
        {
            get { return _page; }
            set { _page = value; }
        }

    }
}

