using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.Common;
using FLContentSiteAssist.DataAccess.Select;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessGetPages : IBusinessLogic
    {
        private DataSet _resultset;

        public ProcessGetPages()
        {
        }

        public void Invoke()
        {
            PagesSelect titleselect = new PagesSelect();

            ResultSet = titleselect.Get();
        }

        public DataSet ResultSet
        {
            get { return _resultset; }
            set { _resultset = value; }
        }
    }
}
