﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FLContentSiteAssist.Common;
using FLContentSiteAssist.DataAccess.Update;

namespace FLContentSiteAssist.BusinessLogic.DataProcessing
{
    public class ProcessUpdatePageImage
    {
        private PageImage _pageImage;

        public ProcessUpdatePageImage()
        {
        }

        public void Invoke()
        {
            PageImageUpdate updateImage = new PageImageUpdate();
            updateImage.PageImage = PageImage;
            updateImage.Update();
        }

        public PageImage PageImage
        {
            get { return _pageImage; }
            set { _pageImage = value; }
        }
    }
}
