using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.DataAccess.Delete;
using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessDeleteLink
    {
        private Link _link;

        public ProcessDeleteLink()
        {
        }

        public void Invoke()
        {
            LinkDelete deletelink = new LinkDelete();
            deletelink.Link = Link;
            deletelink.Delete();
        }

        public Link Link
        {
            get { return _link; }
            set { _link = value; }
        }

    }
}

