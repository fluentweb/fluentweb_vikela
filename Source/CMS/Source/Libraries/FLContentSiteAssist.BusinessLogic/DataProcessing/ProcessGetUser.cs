using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.Common;
using FLContentSiteAssist.DataAccess.Select;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessGetUser : IBusinessLogic
    {
        private DataSet _resultset;
        private string _sitename;

        public ProcessGetUser()
        {

        }

        public void Invoke()
        {
            UserSelect data = new UserSelect();
            data.Sitename = SiteName;
            ResultSet = data.Get();
        }

        public string SiteName
        {
            get { return _sitename; }
            set { _sitename = value; }
        }

        public DataSet ResultSet
        {
            get { return _resultset; }
            set { _resultset = value; }
        }
    }
}

