using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.DataAccess.Update;
using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessUpdatePage
    {
        private Page _page;

        public ProcessUpdatePage()
        {
        }

        public void Invoke()
        {
            PageUpdate updatepage = new PageUpdate();
            updatepage.Page = Page;
            updatepage.Update();
        }

        public Page Page
        {
            get { return _page; }
            set { _page = value; }
        }

    }
}

