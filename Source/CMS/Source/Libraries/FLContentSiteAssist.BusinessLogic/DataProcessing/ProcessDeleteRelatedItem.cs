using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.DataAccess.Delete;
using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessDeleteRelatedItem
    {
        private RelatedItem _relateditem;

        public ProcessDeleteRelatedItem()
        {
        }

        public void Invoke()
        {
            RelatedItemDelete deleterelateditem = new RelatedItemDelete();
            deleterelateditem.RelatedItem = RelatedItem;
            deleterelateditem.Delete();
        }

        public RelatedItem RelatedItem
        {
            get { return _relateditem; }
            set { _relateditem = value; }
        }

    }
}

