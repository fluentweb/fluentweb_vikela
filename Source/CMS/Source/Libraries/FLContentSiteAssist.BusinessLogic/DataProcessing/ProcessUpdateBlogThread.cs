using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.DataAccess.Update;
using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessUpdateBlogThread
    {
        private Blog _blog;

        public ProcessUpdateBlogThread()
        {
        }

        public void Invoke()
        {
            BlogThreadUpdate updateblogthread = new BlogThreadUpdate();
            updateblogthread.Blog = Blog;
            updateblogthread.Update();
        }

        public Blog Blog
        {
            get { return _blog; }
            set { _blog = value; }
        }

    }
}

