using System;
using System.Collections.Generic;
using System.Text;

using FLContentSiteAssist.Common;
using FLContentSiteAssist.DataAccess.Insert;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessInsertCustomer : IBusinessLogic
    {
        private Customer _customer;

        public ProcessInsertCustomer()
        {
        }

        public void Invoke()
        {
            CustomerInsert customerdata = new CustomerInsert();
            customerdata.Customer = this.Customer;
            customerdata.Add();
        }

        public Customer Customer
        {
            get { return _customer; }
            set { _customer = value; }
        }
    }
}
