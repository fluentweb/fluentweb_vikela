using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.DataAccess.Select;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessGetLink
    {
        private DataSet _resultset;
        private int _location;

        public ProcessGetLink()
        {
        }

        public void Invoke()
        {
            LinksSelect link = new LinksSelect();
            link.Location = Location;
            ResultSet = link.Get();
        }

        public int Location
        {
            get { return _location; }
            set { _location = value; }
        }

        public DataSet ResultSet
        {
            get { return _resultset; }
            set { _resultset = value; }
        }
    }
}
