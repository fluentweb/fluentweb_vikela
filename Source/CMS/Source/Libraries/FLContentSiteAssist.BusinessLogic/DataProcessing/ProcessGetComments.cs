using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.Common;
using FLContentSiteAssist.DataAccess.Select;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessGetComment : IBusinessLogic
    {
        private DataSet _resultset;
        private Comment _comment;

        public ProcessGetComment()
        {

        }

        public void Invoke()
        {
            CommentsSelect data = new CommentsSelect();
            data.Comments = Comment;
            ResultSet = data.Get();
        }

        public Comment Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        public DataSet ResultSet
        {
            get { return _resultset; }
            set { _resultset = value; }
        }
    }
}

