using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.DataAccess.Update;
using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessUpdateItemDisplay
    {
        private Item _item;

        public ProcessUpdateItemDisplay()
        {
        }

        public void Invoke()
        {
            ItemDisplayUpdate updatelink = new ItemDisplayUpdate();
            updatelink.Item = Item;
            updatelink.Update();
        }

        public Item Item
        {
            get { return _item; }
            set { _item = value; }
        }

    }
}

