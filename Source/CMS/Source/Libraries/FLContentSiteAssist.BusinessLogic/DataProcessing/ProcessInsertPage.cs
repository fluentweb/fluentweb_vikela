using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.DataAccess.Insert;
using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessInsertPage
    {
        private Page _page;

        public ProcessInsertPage()
        {
        }

        public void Invoke()
        {
            PageInsert insertpage = new PageInsert();
            insertpage.Page = Page;
            insertpage.Insert();
        }

        public Page Page
        {
            get { return _page; }
            set { _page = value; }
        }

    }
}

