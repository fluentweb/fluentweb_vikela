using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.DataAccess.Update;
using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessUpdateRelatedItem
    {
        private RelatedItem _relateditem;

        public ProcessUpdateRelatedItem()
        {
        }

        public void Invoke()
        {
            RelatedItemUpdate updateitem = new RelatedItemUpdate();
            updateitem.RelatedItem = RelatedItem;
            updateitem.Update();
        }

        public RelatedItem RelatedItem
        {
            get { return _relateditem; }
            set { _relateditem = value; }
        }

    }
}

