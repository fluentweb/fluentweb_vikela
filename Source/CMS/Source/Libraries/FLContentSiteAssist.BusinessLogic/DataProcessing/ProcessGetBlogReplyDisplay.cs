using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.DataAccess.Select;
using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessGetBlogReplyDisplay
    {
        private DataSet _resultset;
        private Blog _blog;

        public ProcessGetBlogReplyDisplay()
        {
        }

        public void Invoke()
        {
            BlogReplyDisplaySelect blogdata = new BlogReplyDisplaySelect();
            blogdata.Blog = Blog;
            ResultSet = blogdata.Get();
        }

        public Blog Blog
        {
            get { return _blog; }
            set { _blog = value; }
        }

        public DataSet ResultSet
        {
            get { return _resultset; }
            set { _resultset = value; }
        }
    }
}
