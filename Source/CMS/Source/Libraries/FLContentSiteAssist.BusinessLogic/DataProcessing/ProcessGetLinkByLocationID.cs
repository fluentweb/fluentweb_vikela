using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.DataAccess.Select;
using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessGetLinkByLocationID
    {
        private DataSet _resultset;
        private Link _link;

        public ProcessGetLinkByLocationID()
        {
        }

        public void Invoke()
        {
            LinkByLocationIDSelect linklocation = new LinkByLocationIDSelect();
            linklocation.Link = Link;
            ResultSet = linklocation.Get();
        }

        public Link Link
        {
            get { return _link; }
            set { _link = value; }
        }

        public DataSet ResultSet
        {
            get { return _resultset; }
            set { _resultset = value; }
        }

    }
}
