using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.DataAccess.Update;
using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessUpdateItemImage
    {
        private ItemImage _itemimage;

        public ProcessUpdateItemImage()
        {
        }

        public void Invoke()
        {
            ItemImageUpdate updatelink = new ItemImageUpdate();
            updatelink.ItemImage = ItemImage;
            updatelink.Update();
        }

        public ItemImage ItemImage
        {
            get { return _itemimage; }
            set { _itemimage = value; }
        }

    }
}

