using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.Common;
using FLContentSiteAssist.DataAccess.Select;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessGetEmailRecipients : IBusinessLogic
    {
        private DataSet _resultset;
        private string _email;

        public ProcessGetEmailRecipients()
        {

        }

        public void Invoke()
        {
            EmailRecipientsSelect data = new EmailRecipientsSelect();
            data.Email = Email;
            ResultSet = data.Get();
        }

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        public DataSet ResultSet
        {
            get { return _resultset; }
            set { _resultset = value; }
        }
    }
}
