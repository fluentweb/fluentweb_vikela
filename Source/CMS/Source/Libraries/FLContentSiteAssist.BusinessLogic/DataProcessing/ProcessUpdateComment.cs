using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.DataAccess.Update;
using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessUpdateCommentStatus
    {
        private Comment _comment;

        public ProcessUpdateCommentStatus()
        {
        }

        public void Invoke()
        {
            CommentStatusUpdate updatecomment = new CommentStatusUpdate();
            updatecomment.Comment = Comment;
            updatecomment.Update();
        }

        public Comment Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

    }
}

