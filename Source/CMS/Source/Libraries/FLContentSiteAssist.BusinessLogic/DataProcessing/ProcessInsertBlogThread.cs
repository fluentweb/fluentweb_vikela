using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.DataAccess.Insert;
using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessInsertBlogThread
    {
        private Blog _blog;

        public ProcessInsertBlogThread()
        {
        }

        public void Invoke()
        {
            BlogThreadInsert insertblogthread = new BlogThreadInsert();
            insertblogthread.Blog = Blog;
            insertblogthread.Insert();
        }

        public Blog Blog
        {
            get { return _blog; }
            set { _blog = value; }
        }

    }
}

