using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.DataAccess.Update;
using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessUpdateFWUserGUID
    {
        private FWUser _fwuser;

        public ProcessUpdateFWUserGUID()
        {
        }

        public void Invoke()
        {
            FWUserSetGUIDUpdate updateitem = new FWUserSetGUIDUpdate();
            updateitem.FWUser = FWUser;
            updateitem.Update();
        }

        public FWUser FWUser
        {
            get { return _fwuser; }
            set { _fwuser = value; }
        }

    }
}

