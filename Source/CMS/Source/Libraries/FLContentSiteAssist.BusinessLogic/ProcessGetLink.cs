using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.DataAccess.Select;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessGetLink
    {
        private DataSet _resultset;
        private int _section;

        public ProcessGetLink()
        {
        }

        public void Invoke()
        {
            LinksSelect link = new LinksSelect();
            link.Section = Section;
            ResultSet = link.Get();
        }

        public int Section
        {
            get { return _section; }
            set { _section = value; }
        }

        public DataSet ResultSet
        {
            get { return _resultset; }
            set { _resultset = value; }
        }
    }
}
