using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using FLContentSiteAssist.DataAccess.Select;

namespace FLContentSiteAssist.BusinessLogic
{
    public class ProcessGetLinkSection
    {
        private DataSet _resultset;
        private string _location;

        public ProcessGetLinkSection()
        {
        }

        public void Invoke()
        {
            LinkSectionsSelect linksection = new LinkSectionsSelect();
            linksection.Location = Location;
            ResultSet = linksection.Get();
        }

        public string Location
        {
            get { return _location; }
            set { _location = value; }
        }

        public DataSet ResultSet
        {
            get { return _resultset; }
            set { _resultset = value; }
        }
    }
}
