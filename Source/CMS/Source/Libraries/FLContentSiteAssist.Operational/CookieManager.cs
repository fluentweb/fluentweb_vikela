using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Web;

using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.Operational
{
    public class CookieManager
    {
        private static HttpCookie cookie;

        public CookieManager()
        {
        }

        public static string GetBasketGUID()
        {
            if (HttpContext.Current.Request.Cookies["Basket"] != null)
            {
                if (HttpContext.Current.Request.Cookies["Basket"]["BasketID"] != null)
                {
                    return HttpContext.Current.Request.Cookies["Basket"]["BasketID"].ToString();
                }
                else
                {
                    cookie = HttpContext.Current.Request.Cookies["Basket"];
                };
            }
            else
            {
                cookie = new HttpCookie("Basket");
            };
            Guid BasketGUID = Guid.NewGuid();
            cookie.Values.Remove("BasketID");
            cookie.Values.Add("BasketID", BasketGUID.ToString());
            cookie.Expires = DateTime.Now.AddDays(365);
            HttpContext.Current.Response.AppendCookie(cookie);
            return BasketGUID.ToString();
        }

        public static string GetExpiringBasketGUID()
        {
            if (HttpContext.Current.Request.Cookies["Basket"] != null)
            {
                if (HttpContext.Current.Request.Cookies["Basket"]["BasketID"] != null)
                {
                    /* Overwrite existing cookie as a session cookie */
                    string _guid = HttpContext.Current.Request.Cookies["Basket"]["BasketID"].ToString();
                    cookie = new HttpCookie("Basket");
                    cookie.Values.Add("BasketID", _guid);
                    HttpContext.Current.Response.AppendCookie(cookie);
                    /* And return the ID */
                    return _guid;
                }
                else
                {
                    cookie = HttpContext.Current.Request.Cookies["Basket"];
                };
            }
            else
            {
                cookie = new HttpCookie("Basket");
            };
            Guid BasketGUID = Guid.NewGuid();
            cookie.Values.Remove("BasketID");
            cookie.Values.Add("BasketID", BasketGUID.ToString());
            HttpContext.Current.Response.AppendCookie(cookie);
            return BasketGUID.ToString();
        }

        public void DeleteBasketCookie()
        {
            cookie = new HttpCookie("Basket");
            cookie.Expires = DateTime.Now.AddDays(-1);
            HttpContext.Current.Response.AppendCookie(cookie);
        }

        public static string SetSessionGUID(Boolean _keeploggedin)
        {
            if (HttpContext.Current.Request.Cookies["Session"] != null)
            {
                cookie = HttpContext.Current.Request.Cookies["Session"];
            }
            else
            {
                cookie = new HttpCookie("Session");
            };
            Guid SessionGUID = Guid.NewGuid();
            cookie.Values.Remove("SessionID");
            cookie.Values.Add("SessionID", SessionGUID.ToString());
            if (_keeploggedin)
            {
                cookie.Expires = DateTime.Now.AddDays(60);
            }
            HttpContext.Current.Response.AppendCookie(cookie);

            return SessionGUID.ToString();
        }

        public static string GetSessionGUID()
        {
            if (HttpContext.Current.Request.Cookies["Session"] != null)
            {
                if (HttpContext.Current.Request.Cookies["Session"]["SessionID"] != null)
                {
                    return HttpContext.Current.Request.Cookies["Session"]["SessionID"].ToString();
                }
                else
                {
                    return "";
                };
            }
            else
            {
                return "";
            }
        }

        public void SetCookieValue(string _cookiename, string _cookiekey, string _cookievalue)
        {
            if (HttpContext.Current.Request.Cookies[_cookiename] != null)
            {
                cookie = HttpContext.Current.Request.Cookies[_cookiename];
            }
            else
            {
                cookie = new HttpCookie(_cookiename);
            };
            cookie.Values.Remove(_cookiekey);
            cookie.Values.Add(_cookiekey, _cookievalue);
            HttpContext.Current.Response.AppendCookie(cookie);
        }

        public static string GetCookieValue(string _cookiename, string _cookiekey)
        {
            if (HttpContext.Current.Request.Cookies[_cookiename] != null)
            {
                if (HttpContext.Current.Request.Cookies[_cookiename][_cookiekey] != null)
                {
                    if (HttpContext.Current.Request.Cookies[_cookiename][_cookiekey].ToString() != "")
                    {
                        return HttpContext.Current.Request.Cookies[_cookiename][_cookiekey].ToString();
                    }
                    else
                    {
                        return "#none#";
                    };
                }
                else
                {
                    return "#none#";
                };
            }
            else
            {
                return "#none#";
            };
        }

        public static string GetJavaCookie(string _cookiename)
        {
            if (HttpContext.Current.Request.Cookies[_cookiename] != null)
            {
                return HttpContext.Current.Request.Cookies[_cookiename].Value.ToString();
            }
            else
            {
                return "";
            }
        }


    }
}
