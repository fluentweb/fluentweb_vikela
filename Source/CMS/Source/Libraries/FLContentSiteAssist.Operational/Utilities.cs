using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Web;

using FLContentSiteAssist.BusinessLogic;
using FLContentSiteAssist.Common;

namespace FLContentSiteAssist.Operational
{
    public class Utilities
    {

        public static bool isLoggedIn()
        {
            Customer LoggedInCustomer = new Customer();
            ProcessGetCustomerSession CustomerSession = new ProcessGetCustomerSession();
            LoggedInCustomer.SessionGUID = CookieManager.GetSessionGUID();
            CustomerSession.Customer = LoggedInCustomer;
            try
            {
                CustomerSession.Invoke();
            }
            catch
            {
                return false;
            }

            if (CustomerSession.ResultSet.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            };
        }

        public static bool isValidEmail(string email)
        {

            if (email == null || email == "")
            {
                throw new ArgumentNullException("inputEmail");
            }

            string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" + @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" + @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            System.Text.RegularExpressions.Regex re = new System.Text.RegularExpressions.Regex(strRegex);
            if (re.IsMatch(email))
                return (true);
            else
                return (false);
        }

        public static string RemoveSpecialChars(string text)
        {
            StringBuilder sb = new StringBuilder();
            string invalid = "<>\"�&$%^*():;~#+=/\\";

            foreach (char c in text)
            {
                if (!invalid.Contains(c.ToString()))
                {
                    sb.Append(c);
                };
            }
            return sb.ToString().Trim();
        }

        public static bool CheckForSpecialChars(string text)
        {
            string invalid = "<>\"�&$%^*();~#+=/\\";

            foreach (char c in invalid)
            {
                if (text.Contains(c.ToString()))
                {
                    return true;
                };
            }
            return false;
        }

        public static bool IsNumeric(object numberString)
        {
            char[] ca = numberString.ToString().ToCharArray();
            for (int i = 0; i < ca.Length; i++)
            {
                if (!char.IsNumber(ca[i]))
                    if (ca[i] != '.')
                        return false;
            }
            if (numberString.ToString().Trim() == "")
                return false;
            return true;
        }

        public static bool IsNumericInt(object numberString)
        {
            char[] ca = numberString.ToString().ToCharArray();
            for (int i = 0; i < ca.Length; i++)
            {
                if (!char.IsNumber(ca[i]))
                    return false;
            }
            if (numberString.ToString().Trim() == "")
                return false;
            return true;
        }

        public static string FormatText(string text, bool allow)
        {
            string formatted = "";

            StringBuilder sb = new StringBuilder(text);
            sb.Replace("  ", " &nbsp;");

            if (!allow)
            {
                sb.Replace("<br>", Environment.NewLine);
                sb.Replace("&nbsp;", " ");
                formatted = sb.ToString();
            }
            else
            {
                StringReader sr = new StringReader(sb.ToString());
                StringWriter sw = new StringWriter();

                while (sr.Peek() > -1)
                {
                    string temp = sr.ReadLine();
                    sw.Write(temp + "<br>");
                }

                formatted = sw.GetStringBuilder().ToString();
            }

            return formatted;
        }

        public static string UrlBaseSSL(string _url)
        {
            return _url.Replace("http://", "https://");
        }

        public static string UrlBaseNONSSL(string _url)
        {
            return _url.Replace("https://", "http://");
        }
    }
}
