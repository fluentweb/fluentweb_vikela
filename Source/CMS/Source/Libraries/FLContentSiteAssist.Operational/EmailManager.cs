using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;
using System.Configuration;
using System.Data;

using FLContentSiteAssist.BusinessLogic;

namespace FLContentSiteAssist.Operational
{
    public class EmailManager
    {
        private bool _issent;

        public EmailManager()
        {
        }

        public static string GetRecipients(string _email, string _type)
        {
            ProcessGetEmailRecipients recipients = new ProcessGetEmailRecipients();
            recipients.Email = _email;
            recipients.Invoke();

            DataSet _recipients = recipients.ResultSet;

            if (_type == "TO")
            {
                return _recipients.Tables[0].Rows[0]["chrTo"].ToString();
            }
            else if (_type == "CC")
            {
                return _recipients.Tables[0].Rows[0]["chrCc"].ToString();
            }
            else if (_type == "BCC")
            {
                return _recipients.Tables[0].Rows[0]["chrBcc"].ToString();
            }
            else
            {
                return "";
            }
        }

        public void Send(EmailContents emailcontents)
        {
            SmtpClient client = new SmtpClient(SMTPServerName);
            client.UseDefaultCredentials = true;
            MailMessage message = new MailMessage();

            message.From = new MailAddress(emailcontents.FromEmailAddress, emailcontents.FromName);
            message.Subject = emailcontents.Subject;
            message.Body = emailcontents.Body;
            message.IsBodyHtml = true;

            if (emailcontents.To != null && emailcontents.To != "")
            {
                string[] tomails = emailcontents.To.Split(';');
                foreach (string tomail in tomails)
                {
                    message.To.Add(tomail);
                }
            };

            if (emailcontents.Cc != null && emailcontents.Cc != "")
            {
                string[] ccemails = emailcontents.Cc.Split(';');
                foreach (string ccemail in ccemails)
                {
                    message.CC.Add(ccemail);
                }
            };

            if (emailcontents.Bcc != null && emailcontents.Bcc != "")
            {
                string[] bccemails = emailcontents.Bcc.Split(';');
                foreach (string bccemail in bccemails)
                {
                    message.Bcc.Add(bccemail);
                }
            };

            client.Send(message);
            IsSent = true;

            try
            {
            }
            catch /*(Exception ex)*/
            {
                /*throw ex;*/
            }
        }

        public bool IsSent
        {
            get { return _issent; }
            set { _issent = value; }
        }

        private string SMTPServerName
        {
            get { return ConfigurationManager.AppSettings["SMTPServer"]; }
        }

        private string ToAddress
        {
            get { return ConfigurationManager.AppSettings["ToAddress"]; }

        }
    }

    public struct EmailContents
    {
        public string To;
        public string FromName;
        public string FromEmailAddress;
        public string Subject;
        public string Body;
        public string Bcc;
        public string Cc;
    }
}
