using System;
using System.Collections.Generic;
using System.Text;

namespace FLContentSiteAssist.Common
{
    public class RelatedItem
    {
        private int _parentitemid;
        private int _childitemid;
        private int _relationshipid;
        private int _relationshiptypeid;
        private string _relationshiptype;
        private int _itemid;
        private int _itemtypeid;
        private int _sequence;
        
        public RelatedItem()
        {
        }

        public int ParentItemID
        {
            get { return _parentitemid; }
            set { _parentitemid = value; }
        }

        public int ChildItemID
        {
            get { return _childitemid; }
            set { _childitemid = value; }
        }

        public int RelationshipID
        {
            get { return _relationshipid; }
            set { _relationshipid = value; }
        }

        public int RelationshipTypeID
        {
            get { return _relationshiptypeid; }
            set { _relationshiptypeid = value; }
        }

        public string RelationshipType
        {
            get { return _relationshiptype; }
            set { _relationshiptype = value; }
        }

        public int ItemID
        {
            get { return _itemid; }
            set { _itemid = value; }
        }

        public int ItemTypeID
        {
            get { return _itemtypeid; }
            set { _itemtypeid = value; }
        }

        public int Sequence
        {
            get { return _sequence; }
            set { _sequence = value; }
        }

    }
}
