using System;
using System.Collections.Generic;
using System.Text;

namespace FLContentSiteAssist.Common
{
    public class Blog
    {

        private int _threadid;
        private int _replyid;
        private int _blogtypeid;
        private int _customerid;
        private int _userid;
        private int _displayid;
        private int _maxitems;
        private int _maxitemsperpage;
        private string _display;
        private string _status;
        private string _title;
        private string _comment;
        private string _orderby;
        private string _url;

        public Blog()
        {
        }

        public int ThreadID
        {
            get { return _threadid; }
            set { _threadid = value; }
        }

        public int ReplyID
        {
            get { return _replyid; }
            set { _replyid = value; }
        }

        public int BlogTypeID
        {
            get { return _blogtypeid; }
            set { _blogtypeid = value; }
        }

        public int CustomerID
        {
            get { return _customerid; }
            set { _customerid = value; }
        }

        public int UserID
        {
            get { return _userid; }
            set { _userid = value; }
        }

        public int DisplayID
        {
            get { return _displayid; }
            set { _displayid = value; }
        }

        public int MaxItems
        {
            get { return _maxitems; }
            set { _maxitems = value; }
        }

        public int MaxItemsPerPage
        {
            get { return _maxitemsperpage; }
            set { _maxitemsperpage = value; }
        }
        
        public string Display
        {
            get { return _display; }
            set { _display = value; }
        }

        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        public string OrderBy
        {
            get { return _orderby; }
            set { _orderby = value; }
        }

        public string URL
        {
            get { return _url; }
            set { _url = value; }
        }
    }
}
