using System;
using System.Collections.Generic;
using System.Text;

namespace FLContentSiteAssist.Common
{
    public class Customer
    {
        private int _customerid;
        private string _firstname;
        private string _secondname;
        private int _titleid;
        private string _email;
        private string _password;
        private bool _issubscribed;
        private bool _istpsubscribed;
        private bool _issubscribed1;
        private bool _issubscribed2;
        private bool _issubscribed3;
        private bool _issubscribed4;
        private bool _issubscribed5;
        private string _sessionguid;
        private int _yearofbirth;
        private string _sex;
        private string _telephone;
        private string _referral;
        private string _attr1;
        private string _attr2;
        private string _attr3;
        private string _attr4;
        private string _attr5;

        public Customer()
        {

        }

        public string Referral
        {
            get { return _referral; }
            set { _referral = value; }
        }

        public string Attribute1
        {
            get { return _attr1; }
            set { _attr1 = value; }
        }

        public string Attribute2
        {
            get { return _attr2; }
            set { _attr2 = value; }
        }

        public string Attribute3
        {
            get { return _attr3; }
            set { _attr3 = value; }
        }
        public string Attribute4
        {
            get { return _attr4; }
            set { _attr4 = value; }
        }
        public string Attribute5
        {
            get { return _attr5; }
            set { _attr5 = value; }
        }

        public int CustomerID
        {
            get { return _customerid; }
            set { _customerid = value; }
        }

        public string FirstName
        {
            get { return _firstname; }
            set { _firstname = value; }
        }

        public string SecondName
        {
            get { return _secondname; }
            set { _secondname = value; }
        }

        public int TitleID
        {
            get { return _titleid; }
            set { _titleid = value; }
        }

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        public bool IsSubscribed
        {
            get { return _issubscribed; }
            set { _issubscribed = value; }
        }

        public bool IsTPSubscribed
        {
            get { return _istpsubscribed; }
            set { _istpsubscribed = value; }
        }

        public bool IsSubscribed1
        {
            get { return _issubscribed1; }
            set { _issubscribed1 = value; }
        }

        public bool IsSubscribed2
        {
            get { return _issubscribed2; }
            set { _issubscribed2 = value; }
        }

        public bool IsSubscribed3
        {
            get { return _issubscribed3; }
            set { _issubscribed3 = value; }
        }

        public bool IsSubscribed4
        {
            get { return _issubscribed4; }
            set { _issubscribed4 = value; }
        }

        public bool IsSubscribed5
        {
            get { return _issubscribed5; }
            set { _issubscribed5 = value; }
        }

        public string SessionGUID
        {
            get { return _sessionguid; }
            set { _sessionguid = value; }
        }

        public int YearOfBirth
        {
            get { return _yearofbirth; }
            set { _yearofbirth = value; }
        }

        public string Sex
        {
            get { return _sex; }
            set { _sex = value; }
        }

        public string Telephone
        {
            get { return _telephone; }
            set { _telephone = value; }
        }
    }
}
