using System;
using System.Collections.Generic;
using System.Text;

namespace FLContentSiteAssist.Common
{
    public class PageText
    {
        private string _text;
        private int _textid;
        private string _texttype;

        public PageText()
        {
        }

        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        public string TextType
        {
            get { return _texttype; }
            set { _texttype = value; }
        }

        public int TextID
        {
            get { return _textid; }
            set { _textid = value; }
        }

    }
}
