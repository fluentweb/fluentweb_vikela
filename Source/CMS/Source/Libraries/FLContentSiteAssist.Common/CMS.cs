using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Web;

namespace FLContentSiteAssist.Common
{
    public class CMS
    {
        public static bool AdminOverride { get; set; }

        public static bool IsAdmin
        {
            get
            {
                if (AdminOverride)
                {
                    return true;
                }
                else
                {
                    if (HttpContext.Current.Session["Admin"] != null)
                    {
                        string _chk = HttpContext.Current.Session["Admin"].ToString();
                        if (_chk.ToLower() == "true")
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }

                }
            }
        }
    }
}