using System;
using System.Collections.Generic;
using System.Text;

namespace FLContentSiteAssist.Common
{
    public class AdminMenu
    {
    
        private string _menuhtml;
        private int _xloc;
        private int _yloc;
        private string _rootpath;
        private int _menuid;
        private string _menutype;

        public AdminMenu()
        {
        }

        public int MenuId
        {
            get { return _menuid; }
            set { _menuid = value; }
        }

        public string MenuHTML
        {
            get { return _menuhtml; }
            set { _menuhtml = value; }
        }

        public int XLoc
        {
            get { return _xloc; }
            set { _xloc = value; }
        }

        public int YLoc
        {
            get { return _yloc; }
            set { _yloc = value; }
        }

        public string RootPath
        {
            get { return _rootpath; }
            set { _rootpath = value; }
        }

        public string MenuType
        {
            get { return _menutype; }
            set { _menutype = value; }
        }
    
    }
}

