﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FLContentSiteAssist.Common
{
    public class TMCountry
    {
        private int _countryid;
        private bool _available;
        private bool _powerofattorney;
        private bool _conventionpriority;
        private bool _colourrestrictionbw;
        private bool _colourrestrictioncc;
        private bool _colourrestrictioncsm;
        private bool _colourrestrictioncl;
        private int _tmclassificationid;
        private int _translationlanguageid;

        public int CountryID { get { return _countryid; } set { _countryid = value; } }
        public bool Available { get { return _available; } set { _available = value; } }
        public bool PowerOfAttorney { get { return _powerofattorney; } set { _powerofattorney = value; } }
        public bool ConventionPriority { get { return _conventionpriority; } set { _conventionpriority = value; } }
        public bool ColourRestrictionBW { get { return _colourrestrictionbw; } set { _colourrestrictionbw = value; } }
        public bool ColourRestrictionCC { get { return _colourrestrictioncc; } set { _colourrestrictioncc = value; } }
        public bool ColourRestrictionCSM { get { return _colourrestrictioncsm; } set { _colourrestrictioncsm = value; } }
        public bool ColourRestrictionCL { get { return _colourrestrictioncl; } set { _colourrestrictioncl = value; } }
        public int TMClassificationID { get { return _tmclassificationid; } set { _tmclassificationid = value; } }
        public int TranslationLanguageID { get { return _translationlanguageid; } set { _translationlanguageid = value; } }

    }
}
