using System;
using System.Collections.Generic;
using System.Text;

namespace FLContentSiteAssist.Common
{
    public class ItemImage
    {
        public int ID { get; set; }

        private string _imageURL;
        private string _imageLinkURL;
        private string _imageAlt;
        private string _imageCaption;
        private string _imageTitle;
        private string _imageText;
        private int _itemid;
        private int _imagetypeid;
        private bool _deleteimage;

        public ItemImage()
        {
            ID = -1;
        }

        public bool DeleteImg
        {
            get { return _deleteimage; }
            set { _deleteimage = value; }
        }

        public string ImageURL
        {
            get { return _imageURL; }
            set { _imageURL = value; }
        }

        public string ImageLinkURL
        {
            get { return _imageLinkURL; }
            set { _imageLinkURL = value; }
        }

        public string ImageAlt
        {
            get { return _imageAlt; }
            set { _imageAlt = value; }
        }

        public string ImageTitle
        {
            get { return _imageTitle; }
            set { _imageTitle = value; }
        }

        public string ImageCaption
        {
            get { return _imageCaption; }
            set { _imageCaption = value; }
        }

        public string ImageText
        {
            get { return _imageText; }
            set { _imageText = value; }
        }


        public int ItemID
        {
            get { return _itemid; }
            set { _itemid = value; }
        }

        public int ImageTypeID
        {
            get { return _imagetypeid; }
            set { _imagetypeid = value; }
        }

    }
}
