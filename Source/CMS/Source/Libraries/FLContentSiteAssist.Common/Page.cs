using System;
using System.Collections.Generic;
using System.Text;

namespace FLContentSiteAssist.Common
{
    public class Page
    {

        private string _pagename;
        private string _pagetitle;
        private string _pagedesc;
        private string _pagekeywords;
        private string _pageurl;
        private int _pageID;
        private int _clientID;
        private int _templateID;

        public Page()
        {
        }

        public string PageName
        {
            get { return _pagename; }
            set { _pagename = value; }
        }

        public string PageTitle
        {
            get { return _pagetitle; }
            set { _pagetitle = value; }
        }

        public string PageDesc
        {
            get { return _pagedesc; }
            set { _pagedesc = value; }
        }

        public string PageKeywords
        {
            get { return _pagekeywords; }
            set { _pagekeywords = value; }
        }

        public string PageURL
        {
            get { return _pageurl; }
            set { _pageurl = value; }
        }

        public int PageID
        {
            get { return _pageID; }
            set { _pageID = value; }
        }

        public int ClientID
        {
            get { return _clientID; }
            set { _clientID = value; }
        }

        public int TemplateID
        {
            get { return _templateID; }
            set { _templateID = value; }
        }

    }
}
