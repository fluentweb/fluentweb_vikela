using System;
using System.Collections.Generic;
using System.Text;

namespace FLContentSiteAssist.Common
{
    public class ItemText
    {
        private string _textextract;
        private string _texttitle;
        private string _text;
        private int _itemid;
        private int _texttypeid;

        public ItemText()
        {
        }

        public string TextExtract
        {
            get { return _textextract; }
            set { _textextract = value; }
        }

        public string TextTitle
        {
            get { return _texttitle; }
            set { _texttitle = value; }
        }

        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        public int ItemID
        {
            get { return _itemid; }
            set { _itemid = value; }
        }

        public int TextTypeID
        {
            get { return _texttypeid; }
            set { _texttypeid = value; }
        }
                
    }
}
