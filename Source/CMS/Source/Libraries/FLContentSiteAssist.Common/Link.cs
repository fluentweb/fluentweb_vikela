using System;
using System.Collections.Generic;
using System.Text;

namespace FLContentSiteAssist.Common
{
    public class Link
    {
        private string _location;
        private string _section;
        private string _page;
        private string _title;
        private string _url;
        private bool _enabled;
        private int _locationID;
        private int _sequence;
        private int _sectionID;
        private int _linkID;
        private bool _viewdisabled;

        public Link()
        {
        }

        public string Location
        {
            get { return _location; }
            set { _location = value; }
        }

        public string Section
        {
            get { return _section; }
            set { _section = value; }
        }

        public string Page
        {
            get { return _page; }
            set { _page = value; }
        }

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        public string Url
        {
            get { return _url; }
            set { _url = value; }
        }

        public bool Enabled
        {
            get { return _enabled; }
            set { _enabled = value; }
        }

        public int LocationID
        {
            get { return _locationID; }
            set { _locationID = value; }
        }

        public int Sequence
        {
            get { return _sequence; }
            set { _sequence = value; }
        }

        public int LinkID
        {
            get { return _linkID; }
            set { _linkID = value; }
        }

        public int SectionID
        {
            get { return _sectionID; }
            set { _sectionID = value; }
        }

        public bool ViewDisabled
        {
            get { return _viewdisabled; }
            set { _viewdisabled = value; }
        }
    
    }
}
