using System;
using System.Collections.Generic;
using System.Text;

namespace FLContentSiteAssist.Common
{
    public class SitePage
    {
        private string _name;

        public SitePage()
        {
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

    }
}
