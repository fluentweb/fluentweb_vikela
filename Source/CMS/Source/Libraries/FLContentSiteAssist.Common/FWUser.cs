using System;
using System.Collections.Generic;
using System.Text;

namespace FLContentSiteAssist.Common
{
    public class FWUser
    {
        private string _sessionguid;
        private int _userid;
        private bool _resetguid;

        public FWUser() {}

        public string SessionGUID
        {
            get { return _sessionguid; }
            set { _sessionguid = value; }
        }

        public int UserID
        {
            get { return _userid; }
            set { _userid = value; }
        }

        public bool ResetGUID
        {
            get { return _resetguid; }
            set { _resetguid = value; }
        }

    }
}
