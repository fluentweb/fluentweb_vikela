using System;
using System.Collections.Generic;
using System.Text;

namespace FLContentSiteAssist.Common
{
    public class Item
    {
        private string _itemtype;
        private int _itemid;
        private string _display;
        private int _itemdisplayid;
        private int _itemtypeid;
        private int _maxitems;
        private bool _randomise;
        private bool _viewdisabled;
        private int _sequence;
        private bool _enabled;
        private string _title;
        private string _extract;
        private string _text;
        private string _imageurl;
        private string _imagecaption;
        private string _imagealt;
        private string _imagetitle;
        private string _imagetext;
        private int _userid;
        private string _link;
        private bool _deleteimage;
        private bool _deleteimage2;
        private string _texttype;
        private string _searchtext;
        private string _itemurl;
        private bool _donotinit;
        private DateTime _datefrom;
        private DateTime _dateto;
        private string _orderby;
        private int _parentitemid;
        private string _relatetype;
        private string _timefrom;
        private string _timeto;
        private string _text1;
        private string _text2;
        private string _text3;
        private string _text4;
        private string _text5;
        private string _text6;
        private string _text7;
        private string _text8;
        private string _text9;
        private string _text10;
        private string _text11;
        private string _text12;
        private string _image2url;
        private string _image2caption;
        private string _image2alt;
        private string _image2title;
        private string _fileurl;
        private int _filetypeid;
        private string _file2url;
        private int _file2typeid;
        private bool _deletefile;
        private bool _deletefile2;

        public Item()
        {
        }

        public bool doNotInit
        {
            get { return _donotinit; }
            set { _donotinit = value; }
        }

        public string Extract
        {
            get { return _extract; }
            set { _extract = value; }
        }

        public string SearchText
        {
            get { return _searchtext; }
            set { _searchtext = value; }
        }

        public string TextType
        {
            get { return _texttype; }
            set { _texttype = value; }
        }

        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        public string ItemURL
        {
            get { return _itemurl; }
            set { _itemurl = value; }
        }

        public string ImageURL
        {
            get { return _imageurl; }
            set { _imageurl = value; }
        }

        public string ImageCaption
        {
            get { return _imagecaption; }
            set { _imagecaption = value; }
        }

        public string ImageAlt
        {
            get { return _imagealt; }
            set { _imagealt = value; }
        }

        public string ImageTitle
        {
            get { return _imagetitle; }
            set { _imagetitle = value; }
        }

        public string ImageText
        {
            get { return _imagetext; }
            set { _imagetext = value; }
        }

        public int UserID
        {
            get { return _userid; }
            set { _userid = value; }
        }


        public string ItemType
        {
            get { return _itemtype; }
            set { _itemtype = value; }
        }

        public int ItemID
        {
            get { return _itemid; }
            set { _itemid = value; }
        }

        public string Display
        {
            get { return _display; }
            set { _display = value; }
        }

        public int ItemDisplayID
        {
            get { return _itemdisplayid; }
            set { _itemdisplayid = value; }
        }

        public int ItemTypeID
        {
            get { return _itemtypeid; }
            set { _itemtypeid = value; }
        }

        public int MaxItems
        {
            get { return _maxitems; }
            set { _maxitems = value; }
        }

        public bool Randomise
        {
            get { return _randomise; }
            set { _randomise = value; }
        }

        public bool ViewDisabled
        {
            get { return _viewdisabled; }
            set { _viewdisabled = value; }
        }

        public int Sequence
        {
            get { return _sequence; }
            set { _sequence = value; }
        }

        public bool Enabled
        {
            get { return _enabled; }
            set { _enabled = value; }
        }

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        public string Link
        {
            get { return _link; }
            set { _link = value; }
        }

        public bool DeleteImage
        {
            get { return _deleteimage; }
            set { _deleteimage = value; }
        }

        public bool DeleteImage2
        {
            get { return _deleteimage2; }
            set { _deleteimage2 = value; }
        }

        public DateTime DateFrom
        {
            get { return _datefrom; }
            set { _datefrom = value; }
        }

        public DateTime DateTo
        {
            get { return _dateto; }
            set { _dateto = value; }
        }

        public string OrderBy
        {
            get { return _orderby; }
            set { _orderby = value; }
        }

        public int ParentItemID
        {
            get { return _parentitemid; }
            set { _parentitemid = value; }
        }

        public string RelateType
        {
            get { return _relatetype; }
            set { _relatetype = value; }
        }

        public string TimeFrom
        {
            get { return _timefrom; }
            set { _timefrom = value; }
        }

        public string TimeTo
        {
            get { return _timeto; }
            set { _timeto = value; }
        }

        public string Text1
        {
            get { return _text1; }
            set { _text1 = value; }
        }

        public string Text2
        {
            get { return _text2; }
            set { _text2 = value; }
        }

        public string Text3
        {
            get { return _text3; }
            set { _text3 = value; }
        }

        public string Text4
        {
            get { return _text4; }
            set { _text4 = value; }
        }

        public string Text5
        {
            get { return _text5; }
            set { _text5 = value; }
        }

        public string Text6
        {
            get { return _text6; }
            set { _text6 = value; }
        }

        public string Text7
        {
            get { return _text7; }
            set { _text7 = value; }
        }

        public string Text8
        {
            get { return _text8; }
            set { _text8 = value; }
        }

        public string Text9
        {
            get { return _text9; }
            set { _text9 = value; }
        }

        public string Text10
        {
            get { return _text10; }
            set { _text10 = value; }
        }

        public string Text11
        {
            get { return _text11; }
            set { _text11 = value; }
        }

        public string Text12
        {
            get { return _text12; }
            set { _text12 = value; }
        }


        public string Image2URL
        {
            get { return _image2url; }
            set { _image2url = value; }
        }

        public string Image2Caption
        {
            get { return _image2caption; }
            set { _image2caption = value; }
        }

        public string Image2Alt
        {
            get { return _image2alt; }
            set { _image2alt = value; }
        }

        public string Image2Title
        {
            get { return _image2title; }
            set { _image2title = value; }
        }


        public string FileURL
        {
            get { return _fileurl; }
            set { _fileurl = value; }
        }

        public int FileTypeID
        {
            get { return _filetypeid; }
            set { _filetypeid = value; }
        }

        public string File2URL
        {
            get { return _file2url; }
            set { _file2url = value; }
        }

        public int File2TypeID
        {
            get { return _file2typeid; }
            set { _file2typeid = value; }
        }

        public bool DeleteFile
        {
            get { return _deletefile; }
            set { _deletefile = value; }
        }

        public bool DeleteFile2
        {
            get { return _deletefile2; }
            set { _deletefile2 = value; }
        }

        public bool IsAdmin { get; set; }

        public decimal price1 { get; set; }
        public decimal price2 { get; set; }
        public decimal price3 { get; set; }
        public int value1 { get; set; }

        public bool Checkbox1 { get; set; }
        public bool Checkbox2 { get; set; }
        public bool Checkbox3 { get; set; }

        public string DataType { get; set; }

        public bool IsPaged { get; set; }
        public int PageSize { get; set; }
        public int CurrentPage { get; set; }
        public int CategoryID1 { get; set; }
        public int CategoryID2 { get; set; }
    }
}
