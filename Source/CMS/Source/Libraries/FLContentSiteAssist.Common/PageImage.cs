﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FLContentSiteAssist.Common
{
    public class PageImage
    {
        private string _imageURL;
        private string _imageLinkURL;
        private string _imageAlt;
        private string _imageCaption;
        private string _imageTitle;
        private string _imageText;
        private int _pageimageid;
        private string _imageThumbURL;
        private string _imagetype;
        private bool _deleteimage;
        private bool _deletethumbnailimage;

        public PageImage()
        { }

        public string ImageType
        {
            get { return _imagetype; }
            set { _imagetype = value; }
        }

        public string ImageURL
        {
            get { return _imageURL; }
            set { _imageURL = value; }
        }

        public string ImageThumbURL
        {
            get { return _imageThumbURL; }
            set { _imageThumbURL = value; }
        }

        public string ImageLinkURL
        {
            get { return _imageLinkURL; }
            set { _imageLinkURL = value; }
        }

        public string ImageAlt
        {
            get { return _imageAlt; }
            set { _imageAlt = value; }
        }

        public string ImageTitle
        {
            get { return _imageTitle; }
            set { _imageTitle = value; }
        }

        public string ImageCaption
        {
            get { return _imageCaption; }
            set { _imageCaption = value; }
        }

        public string ImageText
        {
            get { return _imageText; }
            set { _imageText = value; }
        }

        public int PageImageID
        {
            get { return _pageimageid; }
            set { _pageimageid = value; }
        }

        public bool DeleteImage
        {
            get { return _deleteimage; }
            set { _deleteimage = value; }
        }

        public bool DeleteThumbnailImage
        {
            get { return _deletethumbnailimage; }
            set { _deletethumbnailimage = value; }
        }
    }
}
