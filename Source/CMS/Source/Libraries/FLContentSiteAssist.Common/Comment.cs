using System;
using System.Collections.Generic;
using System.Text;

namespace FLContentSiteAssist.Common
{
    public class Comment
    {
        private string _status;
        private string _commenttype;
        private string _comment;
        private string _author;
        private Int32 _commentid;

        public Comment()
        {

        }

        public Int32 CommentID
        {
            get { return _commentid; }
            set { _commentid = value; }
        }

        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public string CommentType
        {
            get { return _commenttype; }
            set { _commenttype = value; }
        }

        public string CommentText
        {
            get { return _comment; }
            set { _comment = value; }
        }

        public string Author
        {
            get { return _author; }
            set { _author = value; }
        }

    }

}
