﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlTypes;
using DataAccess.Setup;
using System.Data;


namespace DataAccess
{
    public class DbFactory
    {
        #region Properties

        public static DbConnectionSettings ConnectionSettings { get; set; }

        #endregion

        #region Factory Objects

        private static DbProviderFactory GetProviderFactory()
        {
            return DbProviderFactories.GetFactory(ConnectionSettings.ProviderName);
        }

        private static DbConnection CreateDbConnection()
        {
            DbConnection connection = null;
            try
            {
                connection = GetProviderFactory().CreateConnection();
                connection.ConnectionString = ConnectionSettings.ConnectionString;
            }
            catch (Exception)
            {
                if (connection != null) connection = null;
                throw new Exception("Failed to create connection");
            }
            return connection;
        }

        private static DbParameter[] CreateDbParametersAdHoc(object[] parameters)
        {
            if (parameters == null || parameters.Length == 0)
                throw new Exception("No parameters were specified, please pass at least one parameter");

            DbParameter p = null;
            DbProviderFactory factory = GetProviderFactory();
            DbParameter[] dbParamters = new DbParameter[parameters.Length];

            try
            {
                for (int i = 0; i < parameters.Length; i++)
                {
                    // sql server doesn't accept DateTime.MinValue
                    if (ConnectionSettings.ProviderName.ToLower() == "system.data.sqlclient" && ((DateTime)parameters[i]) == DateTime.MinValue)
                        parameters[i] = SqlDateTime.MinValue;
                    p = factory.CreateParameter();
                    p.ParameterName = String.Format("param_{0}", i + 1);
                    p.Value = parameters[i] ?? DBNull.Value;
                    dbParamters[i] = p;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to create DbParameters", ex);
            }
            return dbParamters;
        }

        public static List<DbParameter> CreateDbParameters(Dictionary<string, object> parameterValues)
        {
            if (parameterValues.Count < 1) throw new Exception("No parameterValues specified. Please pass at least one parameterValue");

            DbProviderFactory providerFactory = GetProviderFactory();
            List<DbParameter> parameterList = new List<DbParameter>();
            DbParameter parameter = null;
            try
            {
                foreach (KeyValuePair<string, object> p in parameterValues)
                {
                    parameter = providerFactory.CreateParameter();
                    parameter.ParameterName = GetParameterName(p.Key);
                    parameter.Value = (GetParameterValue(p)) ?? DBNull.Value;
                    parameterList.Add(parameter);
                }

            }
            catch (Exception)
            {
                throw;
            }

            return parameterList;
        }

        // Helpers

        private static string GetParameterName(string name)
        {
            switch (ConnectionSettings.ProviderName.ToLower())
            {
                case "system.data.sqlclient":
                    return String.Format("@{0}", name);
                default:
                    return String.Format("?{0}", name);
            }
        }

        private static object GetParameterValue(KeyValuePair<string, object> p)
        {
            if (ConnectionSettings.ProviderName.ToLower() == "system.data.sqlclient")
            {
                if (p.Value is DateTime && ((DateTime)p.Value) == DateTime.MinValue)
                    return System.Data.SqlTypes.SqlDateTime.MinValue;
            }
            return p.Value;
        }

        #endregion

        #region Public Methods

        public static DbDataReader ExecuteReader(string procedureName)
        {
            return ExecuteReader(procedureName, default(Dictionary<string, object>));
        }

        public static DbDataReader ExecuteReader(string procedureName, Dictionary<string, object> parameters)
        {
            if (String.IsNullOrEmpty(procedureName)) throw new Exception("The procedure name parameter cannot be null or empty");

            DbConnection connection = null;
            DbCommand command = null;
            try
            {
                connection = DbFactory.CreateDbConnection();
                connection.Open();
                command = connection.CreateCommand();
                command.CommandText = procedureName;
                command.CommandType = CommandType.StoredProcedure;

                if (parameters != null && parameters.Count > 0)
                {
                    command.Parameters.AddRange(CreateDbParameters(parameters).ToArray());
                }

                return command.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (command != null) command.Dispose();
            }
        }

        // Execute Reader

        public static DbDataReader ExecuteReader(string procedureName, DbConnection openDbConnection)
        {
            return ExecuteReader(procedureName, openDbConnection, default(DbTransaction), default(Dictionary<string, object>));
        }

        public static DbDataReader ExecuteReader(string procedureName, DbConnection openDbConnection, Dictionary<string, object> parameters)
        {
            return ExecuteReader(procedureName, openDbConnection, default(DbTransaction), parameters);
        }

        public static DbDataReader ExecuteReader(string procedureName, DbConnection openDbConnection, DbTransaction transaction)
        {
            return ExecuteReader(procedureName, openDbConnection, transaction, default(Dictionary<string, object>));
        }

        public static DbDataReader ExecuteReader(string procedureName, DbConnection openDbConnection, DbTransaction transaction, Dictionary<string, object> parameters)
        {
            if (String.IsNullOrEmpty(procedureName)) throw new Exception("The procedure name parameter cannot be null or empty");
            if (openDbConnection == null) throw new Exception("The open DbConnection object cannot be null");

            DbCommand command = null;
            try
            {
                command = openDbConnection.CreateCommand();
                command.CommandText = procedureName;
                command.CommandType = System.Data.CommandType.StoredProcedure;
                if (transaction != null) command.Transaction = transaction;

                if (parameters != null && parameters.Count > 0)
                {
                    command.Parameters.AddRange(CreateDbParameters(parameters).ToArray());
                }

                return command.ExecuteReader();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (command != null) command.Dispose();
            }
        }


        // Execute Non-Query

        public static int ExecuteNonQuery(string procedureName, DbConnection openDbConnection, DbTransaction transaction, Dictionary<string, object> paramters)
        {
            if (String.IsNullOrEmpty(procedureName)) throw new Exception("The procedure name parameter cannot be null or empty");
            if (openDbConnection == null) throw new Exception("The open DbConnection object cannot be null");

            DbCommand command = null;
            int result = 0;
            try
            {
                command = openDbConnection.CreateCommand();
                command.CommandText = procedureName;
                command.CommandType = System.Data.CommandType.StoredProcedure;
                if (transaction != null) command.Transaction = transaction;

                if (paramters != null && paramters.Count > 0)
                {
                    command.Parameters.AddRange(CreateDbParameters(paramters).ToArray());
                }
                result = command.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (command != null) command.Dispose();
            }

            return result;
        }


        // Execute Scalar

        public static T ExecuteScalar<T>(string procedureName, DbConnection openDbConnection, DbTransaction transaction, Dictionary<string, object> parameters)
        {
            if (String.IsNullOrEmpty(procedureName)) throw new Exception("The procedure name parameter cannot be null or empty");
            if (openDbConnection == null) throw new Exception("The open DbConnection object cannot be null");

            DbCommand command = null;
            T result = default(T);

            try
            {
                command = openDbConnection.CreateCommand();
                command.CommandText = procedureName;
                command.CommandType = System.Data.CommandType.StoredProcedure;
                if (transaction != null) command.Transaction = transaction;

                if (parameters != null && parameters.Count > 0)
                {
                    command.Parameters.AddRange(CreateDbParameters(parameters).ToArray());
                }

                object o = command.ExecuteScalar();
                if (o == null) return result; // return default value for T
                result = (T)Convert.ChangeType(o, typeof(T)); // return o converted to type of T
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (command != null) command.Dispose();
            }

            return result;
        }

        public static T ExecuteScalar<T>(string procedureName, DbConnection openDbConnection, DbTransaction transaction)
        {
            return ExecuteScalar<T>(procedureName, openDbConnection, transaction, default(Dictionary<string, object>));
        }


        // DataTable

        public static DataTable GetDataTable(string procedureName, DbConnection openDbConnection)
        {
            return GetDataTable(procedureName, openDbConnection, default(DbTransaction), default(Dictionary<string, object>));
        }

        public static DataTable GetDataTable(string procedureName, DbConnection openDbConnection, Dictionary<string, object> parameters)
        {
            return GetDataTable(procedureName, openDbConnection, default(DbTransaction), parameters);
        }

        public static DataTable GetDataTable(string procedureName, DbConnection openDbConnection, DbTransaction transaction)
        {
            return GetDataTable(procedureName, openDbConnection, transaction, default(Dictionary<string, object>));
        }

        public static DataTable GetDataTable(string procedureName, DbConnection openDbConnection, DbTransaction transaction, Dictionary<string, object> parameters)
        {
            if (String.IsNullOrEmpty(procedureName)) throw new Exception("The procedure name parameter cannot be null or empty");
            if (openDbConnection == null) throw new Exception("The open DbConnection object cannot be null");

            DataTable dtResult = new DataTable();
            DbDataReader reader = null;
            DbCommand command = null;

            try
            {
                command = openDbConnection.CreateCommand();
                command.CommandText = procedureName;
                command.CommandType = CommandType.StoredProcedure;
                if (transaction != null) command.Transaction = transaction;

                if (parameters != null && parameters.Count > 0)
                {
                    command.Parameters.AddRange(CreateDbParameters(parameters).ToArray());
                }

                reader = command.ExecuteReader();

                if (reader.HasRows) dtResult.Load(reader);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (reader != null) reader.Close();
            }

            return dtResult;
        }


        // DataSet

        public static DataSet GetDataSet(string procedureName, DbConnection openDbConnection)
        {
            return GetDataSet(procedureName, openDbConnection, default(DbTransaction), default(Dictionary<string, object>));
        }

        public static DataSet GetDataSet(string procedureName, DbConnection openDbConnection, Dictionary<string, object> parameters)
        {
            return GetDataSet(procedureName, openDbConnection, default(DbTransaction), parameters);
        }

        public static DataSet GetDataSet(string procedureName, DbConnection openDbConnection, DbTransaction transaction)
        {
            return GetDataSet(procedureName, openDbConnection, transaction, default(Dictionary<string, object>));
        }

        public static DataSet GetDataSet(string procedureName, DbConnection openDbConnection, DbTransaction transaction, Dictionary<string, object> parameters)
        {
            if (String.IsNullOrEmpty(procedureName)) throw new Exception("The procedure name parameter cannot be null or empty");
            if (openDbConnection == null) throw new Exception("The open DbConnection object cannot be null");

            DbCommand command = null;
            DbDataAdapter adapter = null;
            DataSet dsResult = new DataSet();

            try
            {
                adapter = GetProviderFactory().CreateDataAdapter();
                command = openDbConnection.CreateCommand();
                command.CommandText = procedureName;
                command.CommandType = CommandType.StoredProcedure;
                if (transaction != null) command.Transaction = transaction;

                if (parameters != null && parameters.Count > 0)
                {
                    command.Parameters.AddRange(CreateDbParameters(parameters).ToArray());
                }

                adapter.SelectCommand = command;
                adapter.Fill(dsResult);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (command != null) command.Dispose();
            }

            return dsResult;
        }

        #endregion

        #region Connection Methods

        public static void Connect(Action<DbConnection> action)
        {
            DbConnection connection = null;
            try
            {
                connection = DbFactory.CreateDbConnection();
                connection.Open();

                // custom logic runs here
                action(connection);

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connection != null) connection.Close();
            }
        }

        public static void Connect(Action<DbConnection, DbTransaction> action)
        {
            DbConnection connection = null;
            DbTransaction transaction = null;
            try
            {
                connection = DbFactory.CreateDbConnection();
                connection.Open();
                transaction = connection.BeginTransaction();

                // custom logic runs here
                action(connection, transaction);

                if (transaction != null) transaction.Commit();
            }
            catch (Exception)
            {
                if (transaction != null) transaction.Rollback();
                throw;
            }
            finally
            {
                if (transaction != null) transaction.Dispose();
                if (connection != null) connection.Close();
            }
        }


        // Connection exposed

        public static T Connect<T>(Func<DbConnection, T> func)
        {
            DbConnection connection = null;
            T result = default(T);
            try
            {
                connection = DbFactory.CreateDbConnection();
                connection.Open();

                // custom logic runs here
                result = func(connection);

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connection != null) connection.Close();
            }
            return result;
        }

        public static T Connect<T, P>(Func<DbConnection, P, T> func, P parameter)
        {
            DbConnection connection = null;
            T result = default(T);
            try
            {
                connection = DbFactory.CreateDbConnection();
                connection.Open();

                // custom logic runs here
                result = func(connection, parameter);

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connection != null) connection.Close();
            }
            return result;
        }

        public static T Connect<T, P1, P2>(Func<DbConnection, P1, P2, T> func, P1 parameter1, P2 parameter2)
        {
            DbConnection connection = null;
            T result = default(T);
            try
            {
                connection = DbFactory.CreateDbConnection();
                connection.Open();

                // custom logic runs here
                result = func(connection, parameter1, parameter2);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connection != null) connection.Close();
            }
            return result;
        }

        public static T Connect<T, P1, P2, P3>(Func<DbConnection, P1, P2, P3, T> func, P1 parameter1, P2 parameter2, P3 parameter3)
        {
            DbConnection connection = null;
            T result = default(T);
            try
            {
                connection = DbFactory.CreateDbConnection();
                connection.Open();

                // custom logic runs here
                result = func(connection, parameter1, parameter2, parameter3);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connection != null) connection.Close();
            }
            return result;
        }


        // Connection & Transaction exposed

        public static T Connect<T>(Func<DbConnection, DbTransaction, T> func)
        {
            DbConnection connection = null;
            DbTransaction transaction = null;
            T result = default(T);
            try
            {
                connection = DbFactory.CreateDbConnection();
                connection.Open();
                transaction = connection.BeginTransaction();

                // custom logic runs here
                result = func(connection, transaction);

                if (transaction != null) transaction.Commit();
            }
            catch (Exception)
            {
                if (transaction != null) transaction.Rollback();
                throw;
            }
            finally
            {
                if (transaction != null) transaction.Dispose();
                if (connection != null) connection.Close();
            }
            return result;
        }

        public static T Connect<T, P>(Func<DbConnection, DbTransaction, P, T> func, P parameter)
        {
            DbConnection connection = null;
            DbTransaction transaction = null;
            T result = default(T);
            try
            {
                connection = DbFactory.CreateDbConnection();
                connection.Open();
                transaction = connection.BeginTransaction();

                // custom logic runs here
                result = func(connection, transaction, parameter);

                if (transaction != null) transaction.Commit();
            }
            catch (Exception)
            {
                if (transaction != null) transaction.Rollback();
                throw;
            }
            finally
            {
                if (transaction != null) transaction.Dispose();
                if (connection != null) connection.Close();
            }
            return result;
        }

        public static T Connect<T, P1, P2>(Func<DbConnection, DbTransaction, P1, P2, T> func, P1 parameter1, P2 parameter2)
        {
            DbConnection connection = null;
            DbTransaction transaction = null;
            T result = default(T);
            try
            {
                connection = DbFactory.CreateDbConnection();
                connection.Open();
                transaction = connection.BeginTransaction();

                // custom logic runs here
                result = func(connection, transaction, parameter1, parameter2);

                if (transaction != null) transaction.Commit();
            }
            catch (Exception)
            {
                if (transaction != null) transaction.Rollback();
                throw;
            }
            finally
            {
                if (transaction != null) transaction.Dispose();
                if (connection != null) connection.Close();
            }
            return result;
        }

        #endregion

        public static T Connect<T, P1, P2, P3>(Func<DbConnection, DbTransaction, P1, P2, P3, T> func, P1 parameter1, P2 parameter2, P3 paramter3)
        {
            DbConnection connection = null;
            DbTransaction transaction = null;
            T result = default(T);
            try
            {
                connection = DbFactory.CreateDbConnection();
                connection.Open();
                transaction = connection.BeginTransaction();

                // custom logic runs here
                result = func(connection, transaction, parameter1, parameter2, paramter3);

                if (transaction != null) transaction.Commit();
            }
            catch (Exception)
            {
                if (transaction != null) transaction.Rollback();
                throw;
            }
            finally
            {
                if (transaction != null) transaction.Dispose();
                if (connection != null) connection.Close();
            }
            return result;
        }

        public static T Connect<T, P1, P2, P3, P4>(Func<DbConnection, DbTransaction, P1, P2, P3, P4, T> func, P1 parameter1, P2 parameter2, P3 paramter3, P4 parameter4)
        {
            DbConnection connection = null;
            DbTransaction transaction = null;
            T result = default(T);
            try
            {
                connection = DbFactory.CreateDbConnection();
                connection.Open();
                transaction = connection.BeginTransaction();

                // custom logic runs here
                result = func(connection, transaction, parameter1, parameter2, paramter3, parameter4);

                if (transaction != null) transaction.Commit();
            }
            catch (Exception)
            {
                if (transaction != null) transaction.Rollback();
                throw;
            }
            finally
            {
                if (transaction != null) transaction.Dispose();
                if (connection != null) connection.Close();
            }
            return result;
        }

        public static T Connect<T, P1, P2, P3, P4, P5>(Func<DbConnection, DbTransaction, P1, P2, P3, P4, P5, T> func, P1 parameter1, P2 parameter2, P3 paramter3, P4 parameter4, P5 parameter5)
        {
            DbConnection connection = null;
            DbTransaction transaction = null;
            T result = default(T);
            try
            {
                connection = DbFactory.CreateDbConnection();
                connection.Open();
                transaction = connection.BeginTransaction();

                // custom logic runs here
                result = func(connection, transaction, parameter1, parameter2, paramter3, parameter4, parameter5);

                if (transaction != null) transaction.Commit();
            }
            catch (Exception)
            {
                if (transaction != null) transaction.Rollback();
                throw;
            }
            finally
            {
                if (transaction != null) transaction.Dispose();
                if (connection != null) connection.Close();
            }
            return result;
        }

    }
}
