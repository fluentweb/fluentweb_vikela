﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Setup
{
    public class DbConnectionSettings
    {
        public string ConnectionString { get; set; }
        public string ProviderName { get; set; }

        public DbConnectionSettings(string connectionString, string providerName)
        {
            ConnectionString = connectionString;
            ProviderName = providerName;
        }        
    }
}
