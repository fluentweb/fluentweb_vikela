﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Collections.Specialized;

namespace Common.Url
{
    public class URLHelper
    {
        public static string ReplaceQueryString(string url, string param, string value)
        {
            var re = new Regex("([?|&])" + param + "=.*?(&|$)", RegexOptions.IgnoreCase);

            if (re.IsMatch(url))
                return re.Replace(url, "$1" + param + "=" + value + "$2");
            else
                return url + ((url.IndexOf('?') == -1) ? '?' : '&') + param + "=" + value;
        }

        public static string ReplaceQueryString(string url, string param, string value, bool resetPager)
        {
            if (resetPager)
            {
                NameValueCollection p = HttpContext.Current.Request.QueryString;
                for (int i = 0; i < p.Count; i++)
                {
                    if (p.Keys[i].ToLower() == "page")
                    {
                        url = url.Replace(String.Format("{0}{1}{2}", (i == 0 ? "?" : "&"), p.Keys[i].ToString(), "=" + p.Get(i)), String.Empty);
                    }
                }

            }

            var re = new Regex("([?|&])" + param + "=.*?(&|$)", RegexOptions.IgnoreCase);

            if (re.IsMatch(url))
                return re.Replace(url, "$1" + param + "=" + value + "$2");
            else
                return url + ((url.IndexOf('?') == -1) ? '?' : '&') + param + "=" + value;
        }

        public static string RemoveQueryString(string url, string param)
        {
            NameValueCollection p = HttpContext.Current.Request.QueryString;
            for (int i = 0; i < p.Count; i++)
            {
                if (p.Keys[i].ToLower() == param)
                {
                    url = url.Replace(String.Format("{0}{1}{2}", (i == 0 ? "?" : "&"), p.Keys[i].ToString(), "=" + p.Get(i)), String.Empty);
                }
            }
            return url;
        }                               
    }
}
