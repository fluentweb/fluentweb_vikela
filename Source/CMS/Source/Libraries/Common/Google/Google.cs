﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using System.Text;

/// <summary>
/// Summary description for Google Analytics
/// Version:    2.0
/// Author:     Sarin Na Wangkanai 
/// Mods:       David Scott: re-formatted to be compatible with .net framework 2.0
/// Website:    www.sarin.mobi  
/// License:    GPL 
/// </summary>
namespace Common.Google
{
    public class Analytics
    {
        private string _ua;

        #region BrowserSettingsPrivate
        private bool _clientinfo;
        private string _domainname;
        private bool _allowlinker;
        private bool _allowhash;
        private bool _detectflash;
        private bool _detecttitle;
        #endregion

        #region CampaignPrivate
        private int _campaigncookietimeout;
        private bool _campaigntrack;
        private string _campaignname;
        private string _campaignmedium;
        private string _campaignsource;
        private string _campaignterm;
        private string _campaigncontent;
        private string _campaignnooverride;
        #endregion

        /// <summary>
        /// Google Analytics allows you to define custom segments and analyze the behavior of each segment.
        /// </summary>
        /// <remarks>http://www.google.com/support/googleanalytics/bin/answer.py?answer=57045</remarks>
        private string _visitorsegment;

        private string GoogleJavascriptLibrary
        {
            get
            {
                StringBuilder builder = new StringBuilder();
                builder.AppendLine("(function() {");
                builder.AppendLine("\tvar ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;");
                builder.AppendLine("\tga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';");
                builder.AppendLine("\tvar s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);");
                builder.AppendLine("})();");
                return builder.ToString();
            }
        }
        private Transaction trans;

        /// <summary>
        /// Create the google analytics tracking code instance
        /// </summary>
        /// <param name="ua">(UA-XXXXX-YY) Your web property ID, informally referred to as UA number, can be found by clicking the "check status" link or by searching for "UA-" in the source code of your web page.</param>
        public Analytics(string ua, string dom)
        {
            this.UA = ua;
            this.DomainName = dom;
            this.ClientInfo = true;
            this.AllowHash = true;
            this.DetectFlash = true;
            this.DetectTitle = true;
            this.CampaignTrack = true;  
        }

        /// <summary>
        /// Add transaction tracking to the google analytics tracker code
        /// </summary>
        /// <param name="transaction">transaction object</param>
        public void AddTrans(Transaction transaction)
        {
            this.trans = transaction;
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine("<script type=\"text/javascript\">");
            builder.AppendLine("\tvar _gaq = _gaq || [];");
            builder.AppendLine(string.Format("\t_gaq.push(['_setAccount', '{0}']);", UA));
            if (!ClientInfo) builder.AppendLine("\t_gaq.push(['_setClientInfo', false]);");
            if (DomainName != null) builder.AppendLine(string.Format("\t_gaq.push(['_setDomainName', '{0}']);", DomainName));
            if (AllowLinker) builder.AppendLine("\t_gaq.push(['_setAllowLinker', true]);");
            if (!AllowHash) builder.AppendLine("\t_gaq.push(['_setAllowHash', false]);");
            if (!DetectFlash) builder.AppendLine("\t_gaq.push(['_setDetectFlash', false]);");
            if (!DetectTitle) builder.AppendLine("\t_gaq.push(['_setDetectTitle', false]);");

            if (!CampaignTrack) builder.AppendLine("\t_gaq.push(['_setCampaignTrack', false]);");
            if (CampaignCookieTimeout > 0) builder.AppendLine(string.Format("\t_gaq.push(['_setCampaignCookieTimeout', {0}]);", CampaignCookieTimeout));
            if (CampaignName != null) builder.AppendLine(string.Format("\t_gaq.push(['_setCampNameKey', '{0}']);", CampaignName));
            if (CampaignMedium != null) builder.AppendLine(string.Format("\t_gaq.push(['_setCampMediumKey', '{0}']);",CampaignMedium));
            if (CampaignSource != null) builder.AppendLine(string.Format("\t_gaq.push(['_setCampSourceKey', '{0}']);", CampaignSource));
            if (CampaignTerm != null) builder.AppendLine(string.Format("\t_gaq.push(['_setCampTermKey', '{0}']);", CampaignTerm));
            if (CampaignContent != null) builder.AppendLine(string.Format("\t_gaq.push(['_setCampContentKey', '{0}']);", CampaignContent));
            if (CampaignNoOverride != null) builder.AppendLine(string.Format("\t_gaq.push(['_setCampNOKey', '{0}']);", CampaignNoOverride));

            if (VisitorSegment != null) builder.AppendLine(string.Format("\t_gaq.push(['_setVar','{0}']);", VisitorSegment));

            builder.AppendLine("\t_gaq.push(['_trackPageview']);");
            if (trans != null)
            {
                builder.AppendLine("\t" + trans.AddTrans());
                foreach (Item item in trans.Items)
                    builder.AppendLine("\t" + item.ToScript(trans.OrderID));
                builder.AppendLine("\t" + trans.Submit());
            }
            builder.AppendLine();
            builder.AppendLine(GoogleJavascriptLibrary);
            builder.AppendLine("</script>");
            return builder.ToString();
        }

        public string UA
        {
            get { return _ua; }
            set { _ua = value; }
        }

        public string VisitorSegment
        {
            get { return _visitorsegment; }
            set { _visitorsegment = value; }
        }

        #region BrowserSettingsPublic
        public bool ClientInfo
        {
            get { return _clientinfo; }
            set { _clientinfo = value; }
        }
        public string DomainName
        {
            get { return _domainname; }
            set { _domainname = value; }
        }
        public bool AllowLinker
        {
            get { return _allowlinker; }
            set { _allowlinker = value; }
        }
        public bool AllowHash
        {
            get { return _allowhash; }
            set { _allowhash = value; }
        }
        public bool DetectFlash
        {
            get { return _detectflash; }
            set { _detectflash = value; }
        }
        public bool DetectTitle
        {
            get { return _detecttitle; }
            set { _detecttitle = value; }
        }
        #endregion

        #region CampaignPublic
        public int CampaignCookieTimeout
        {
            get { return _campaigncookietimeout; }
            set { _campaigncookietimeout = value; }
        }
        public bool CampaignTrack
        {
            get { return _campaigntrack; }
            set { _campaigntrack = value; }
        }
        public string CampaignName
        {
            get { return _campaignname; }
            set { _campaignname = value; }
        }
        public string CampaignMedium
        {
            get { return _campaignmedium; }
            set { _campaignmedium = value; }
        }
        public string CampaignSource
        {
            get { return _campaignsource; }
            set { _campaignsource = value; }
        }
        public string CampaignTerm
        {
            get { return _campaignterm; }
            set { _campaignterm = value; }
        }
        public string CampaignContent
        {
            get { return _campaigncontent; }
            set { _campaigncontent = value; }
        }
        public string CampaignNoOverride
        {
            get { return _campaignnooverride; }
            set { _campaignnooverride = value; }
        }
        #endregion
    }
    public class Event
    {
        private string _category;
        private string _action;
        private string _label;
        private string _value;

        public Event(string category, string action)
        {
            this.Category = category;
            this.Action = action;
        }

        public Event(string category, string action, string label, string value):this(category, action)
        {
            this.Label = label;
            this.Value = value;
        }

        public override string ToString()
        {
            return string.Format("_gaq.push(['_trackEvent', '{0}', '{1}', '{2}', '{3}]);",
                Category, Action, Label, Value);
        }

        public string Category
        {
            get { return _category; }
            set { _category = value; }
        }
        public string Action
        {
            get { return _action; }
            set { _action = value; }
        }
        public string Label
        {
            get { return _label; }
            set { _label = value; }
        }
        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
    
    }
    public class Transaction
    {
        private string _orderid;     // order ID - required
        private string _storename;   // affiliation or store name
        private decimal _total;      // total - required
        private decimal _tax;        // tax
        private decimal _shipping;   // shipping cost
        private string _city;        // city
        private string _state;       // state or province
        private string _country;     // country

        private List<Item> _items;   // List of itmes in this invoice transaction

        /// <summary>
        /// The transaction object stores all the related information about a single transaction, such as the order ID, shipping charges, and billing address.
        /// </summary>
        /// <param name="orderID">transaction order id (required)</param>
        /// <param name="storename">affiliation or store name</param>
        /// <param name="tax">tax</param>
        /// <param name="shipping">shipping cost</param>
        /// <param name="city">city</param>
        /// <param name="state">state or province</param>
        /// <param name="country">country</param>
        public Transaction(string orderID, string storename, decimal tax, decimal shipping, string city, string state, string country)
        {
            this.OrderID = orderID;
            this.StoreName = storename;
            this.Tax = tax;
            this.Shipping = shipping;
            this.City = city;
            this.State = state;
            this.Country = country;
            this.Items = new List<Item>();
        }

        /// <summary>
        /// add item might be called for every item in the shopping cart
        /// where your ecommerce engine loops through each item in the cart and
        /// prints out _addItem for each
        /// </summary>
        /// <param name="item">item object</param>
        public void Add(Item item)
        {
            this.Total += (item.Price * item.Quantity);
            this.Items.Add(item);
        }

        #region googlejavascript
        /// <summary>
        /// create a transaction javascript method call
        /// </summary>
        /// <returns>google javascript codes</returns>
        internal string AddTrans()
        {            
            return string.Format("_gaq.push(['_addTrans', '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}']);",
                OrderID, StoreName, Total, Tax, Shipping, City, State, Country);
        }
        /// <summary>
        /// submits transaction to the Analytics servers
        /// </summary>
        /// <returns>google javascript codes</returns>
        internal string Submit()
        {
            return "_gaq.push(['_trackTrans']);";
        }
        #endregion

        public string OrderID
        {
            get { return _orderid; }
            set { _orderid = value; }
        }
        public string StoreName
        {
            get { return _storename; }
            set { _storename = value; }
        }
        public decimal Total
        {
            get { return _total; }
            set { _total = value; }
        }
        public decimal Tax
        {
            get { return _tax; }
            set { _tax = value; }
        }
        public decimal Shipping
        {
            get { return _shipping; }
            set { _shipping = value; }
        }
        public string City
        {
            get { return _city; }
            set { _city = value; }
        }
        public string State
        {
            get { return _state; }
            set { _state = value; }
        }
        public string Country
        {
            get { return _country; }
            set { _country = value; }
        }

        public List<Item> Items
        {
            get { return _items; }
            set { _items = value; }
        }

    }
    public class Item
    {
        private string _orderid;     // order ID - required
        private string _sku;         // SKU/code - required
        private string _name;        // product name
        private string _category;    // category or variation
        private decimal _price;      // unit price - required
        private int _quantity;       // quantity - required

        /// <summary>
        /// tracks information about each individual item in the user's shopping cart 
        /// and associates the item with each transaction.
        /// </summary>
        /// <param name="sku">SKU/Code (required)</param>
        /// <param name="name">Product name (optional)</param>
        /// <param name="category">Category (optional)</param>
        /// <param name="price">unit price (required)</param>
        /// <param name="quantity">quantity (required)</param>
        public Item(string orderID, string sku, string name, string category, decimal price, int quantity)
        {
            this.OrderID = orderID;
            this.SKU = sku;
            this.Name = name;
            this.Category = category;
            this.Price = price;
            this.Quantity = quantity;
        }

        /// <summary>
        /// Create a item javascript method call
        /// </summary>
        /// <param name="orderid">order ID for the transcation</param>
        /// <returns>google javascript codes</returns>
        internal string ToScript(string orderid)
        {
            return string.Format("_gaq.push(['_addItem', '{0}', '{1}', '{2}', '{3}', '{4}', '{5}']);",
                OrderID, SKU, Name, Category, Price, Quantity);
        }

        public string OrderID
        {
            get { return _orderid; }
            set { _orderid = value; }
        }
        public string SKU
        {
            get { return _sku; }
            set { _sku = value; }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public string Category
        {
            get { return _category; }
            set { _category = value; }
        }
        public decimal Price
        {
            get { return _price; }
            set { _price = value; }
        }
        public int Quantity
        {
            get { return _quantity; }
            set { _quantity = value; }
        }

    }
}