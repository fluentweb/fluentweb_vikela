﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Google.AdWords
{
    public class ConversionTracking
    {
        public string ConversionID { get; set; }
        public string ConversionLabel { get; set; }
        public string ConversionFormat { get; set; }
        public string ConversionLanguage { get; set; }
        public string ConversionColour { get; set; }
        public string ConversionTotalValue { get; set; }

        public ConversionTracking() { }              

        public override string ToString()
        {
            if (this == null) return String.Empty;

            /*Build google conversion code - This is for AdWords only*/
            StringBuilder googleCode = new StringBuilder();
            googleCode.AppendFormat(
                @"<script type='text/javascript'>
                      /* <![CDATA[ */
                      var google_conversion_id = {0};
                      var google_conversion_language = '{1}';
                      var google_conversion_format = '{2}';
                      var google_conversion_color = '{3}';
                      var google_conversion_label = '{4}';
                      var google_conversion_value = {5};
                      /* ]]> */
                 </script>
                 <script type='text/javascript' src='https://www.googleadservices.com/pagead/conversion.js'>
                 </script>
                 <noscript>
                     <div style='display:inline;'>
                     <img height='1' width='1' style='border-style:none;' alt='' src='https://www.googleadservices.com/pagead/conversion/{6}/?value={7}&amp;label={8}&amp;guid=ON&amp;script=0'/>
                     </div>
                </noscript>", this.ConversionID, this.ConversionLanguage, this.ConversionFormat, this.ConversionColour, this.ConversionLabel,
                            this.ConversionTotalValue, this.ConversionID, this.ConversionTotalValue, this.ConversionLabel);
            return googleCode.ToString();
        }
    }
}
