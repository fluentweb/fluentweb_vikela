﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Common.Modules.Helpers
{
    public class RewriteModuleExtensionHelper
    {
        public const string OriginalUrlFilenameKey = "ORIGINAL_PAGE_NAME";

        public static string GetOriginalUrlFilename()
        {
            return HttpContext.Current.Items.Contains(OriginalUrlFilenameKey) ? HttpContext.Current.Items[OriginalUrlFilenameKey].ToString() : "";  
        }
    }
}
