﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.IO;
using Common.Modules.Helpers;

namespace Common.Modules
{
    public class RewriteModuleExtension : IHttpModule
    {
        #region IHttpModule Members

        public void Dispose()
        {

        }

        public void Init(HttpApplication context)
        {
            context.BeginRequest += new EventHandler(context_BeginRequest);
        }

        void context_BeginRequest(object sender, EventArgs e)
        {
            string fullPath = HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath;
            HttpContext.Current.Items.Add(RewriteModuleExtensionHelper.OriginalUrlFilenameKey, Path.GetFileName(fullPath));
        }

        #endregion
    }
}
