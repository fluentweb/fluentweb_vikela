﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Common.Session
{
    public class SessionObjects
    {
        public static T GetSession<T>(string sessionKey) where T : class
        {
            if (HttpContext.Current.Session == null) return null;
            return HttpContext.Current.Session[sessionKey] as T;
        }

        public static void SetSession<T>(string sessionKey, T value)
        {
            HttpContext.Current.Session[sessionKey] = value;
        }

        public static void ClearSession(string sessionKey)
        {
            HttpContext.Current.Session[sessionKey] = null;
        }
    }
}
