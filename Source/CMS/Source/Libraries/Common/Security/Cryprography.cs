﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Common.Security
{
    public class Cryprography
    {
        public static string GetMD5Hash(string value)
        {
            MD5 algorithm = MD5.Create();
            byte[] data = algorithm.ComputeHash(Encoding.UTF8.GetBytes(value));
            string md5 = String.Empty;
            for (int i = 0; i < data.Length; i++)
            {
                md5 += data[i].ToString("x2").ToUpperInvariant();
            }
            return md5;
        }

        public static string GetSHA1Hash(string value)
        {
            SHA1 algorithm = SHA1.Create();
            byte[] data = algorithm.ComputeHash(Encoding.UTF8.GetBytes(value));
            string sha1 = String.Empty;
            for (int i = 0; i < data.Length; i++)
            {
                sha1 += data[i].ToString("x2").ToUpperInvariant();
            }
            return sha1;
        }
    }
}
