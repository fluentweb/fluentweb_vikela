﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Web;
using Common.Email;
using DataAccess;
using Common.Email.Entities;
using Common.Exceptions.DAC;
using Common.Email.DAC;
using Common.HttpUtils;

namespace Common.Exceptions.Entities.Base
{
    public class BaseException : System.Exception
    {
        public BaseException(string message, Exception inner)
        {
            SendErrorReport(message, inner);
        }

        public static void SendErrorReport(string message, Exception exc)
        {
            StringBuilder sbEmailTemplate = new StringBuilder();
            StackTrace stackTrace = new StackTrace(exc, true);

            string emailRecipient = Setup.CommonSettings.ErrorEmailRecipientAddress;
            string emailSender = Setup.CommonSettings.ErrorEmailSenderAddress;
            string displayName = Setup.CommonSettings.ErrorEmailDisplayName;
            string errorApplicationName = Setup.CommonSettings.ErrorEmailApplicationName;
            string errorEmailPassword = Setup.CommonSettings.ErrorEmailPassword;

            try
            {

                message = !String.IsNullOrEmpty(message) ? message : "na";
                string exception = exc != null ? exc.Message : "na";

                StringBuilder sbFrames = new StringBuilder();
                if (stackTrace != null && stackTrace.FrameCount > 0)
                {
                    StackFrame[] frames = stackTrace.GetFrames();
                    if (frames != null && frames.Length > 0)
                    {
                        sbFrames.Append("<hr/>");
                        for (int i = 0; i < frames.Length; i++)
                        {
                            sbFrames.AppendFormat("<label>Filename:</label> {0}<br/>", (frames[i].GetFileName() ?? "na"));
                            sbFrames.AppendFormat("<label>Line no.:</label> {0}<br/>", frames[i].GetFileLineNumber());
                            sbFrames.AppendFormat("<label>Method:</label> {0}<br/>", frames[i].GetMethod());
                            sbFrames.Append("<hr/>");
                        }
                    }
                }

                string trace = exc != null ? exc.StackTrace : "na";
                string innerEx = exc != null && exc.InnerException != null ? exc.InnerException.Message : "na";
                string targetSite = exc != null ? (exc.TargetSite != null ? exc.TargetSite.ToString() : "na") : "na";
                string userAgent = HttpContext.Current != null && HttpContext.Current.Request != null ? HttpContext.Current.Request.UserAgent : "na";

                // Insert exception
                DbFactory.Connect<long, BaseExceptionLog>(DAC_BaseException.Insert, new BaseExceptionLog
                {
                    ErrorMessage = message,
                    ExceptionMessage = exception,
                    ExceptionStackTrace = trace
                });

                string IPAddress = "N/A";

                if (HttpContext.Current != null && HttpContext.Current.Request != null) 
                {
                    IPAddress = IPHelper.GetIPAddress(HttpContext.Current.Request.ServerVariables["HTTP_VIA"], HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"],
                        HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]);
                }

                sbEmailTemplate.AppendFormat(Common.Exceptions.Resources.ErrorMessages.ErrorEmailTemplate,
                    message, exception, sbFrames.ToString(), trace, innerEx, targetSite, userAgent, IPAddress);

                // email exception
                EmailManager.SendEmail(new List<string>() { emailRecipient }, emailSender, displayName, String.Format("{0} Error", errorApplicationName),
                sbEmailTemplate.ToString(), true, null, null, null, new System.Net.NetworkCredential { Password = errorEmailPassword, UserName = emailSender });


            }
            catch (Exception ex)
            {
                // Save failed email
                DbFactory.Connect<long, FailedEmail>(DAC_FailedEmail.InsertFailedEmail, new FailedEmail
                {
                    EmailMessage = sbEmailTemplate.ToString(),
                    EmailSubject = "Failed to send/save error report",
                    ErrorMessage = ex.Message,
                    Recipient = emailRecipient,
                    Sender = emailSender
                });
                //throw;
            }

        }
    }
}
