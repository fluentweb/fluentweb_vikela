﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using DataAccess;
using Common.Exceptions.Entities;

namespace Common.Exceptions.DAC
{
    public class DAC_BaseException
    {
        public static long Insert(DbConnection connection, DbTransaction transaction, BaseExceptionLog log)
        {
            return DbFactory.ExecuteScalar<long>("spInsertExceptionLog", connection, transaction, new Dictionary<string, object> 
            {
                {"ErrorMessage", log.ErrorMessage},
                {"ExceptionMessage", log.ExceptionMessage },
                {"ExceptionStackTrace",log.ExceptionStackTrace }
            });
        }
    }
}
