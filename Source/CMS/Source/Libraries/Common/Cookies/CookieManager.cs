﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Common.Cookies
{
    public class CookieManager
    {
        public static string TryGetCookieValue(string cookieName, string key)
        {
            if (!CookieExists(cookieName)) return String.Empty;
            string value = HttpContext.Current.Request.Cookies[cookieName][key];
            if (value == null) 
                return String.Empty;
            return value;
        }

        public static bool CookieExists(string cookieName)
        {
            return HttpContext.Current.Request.Cookies[cookieName] != null;
        }
    }
}
