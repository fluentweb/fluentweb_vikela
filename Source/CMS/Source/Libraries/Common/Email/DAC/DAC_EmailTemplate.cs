﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess.Base;
using Common.Email.Entities;
using System.Data.Common;
using DataAccess;

namespace Common.Email.DAC
{
    public class DAC_EmailTemplate
    {
        // Public Methods

        /// <summary>
        /// Gets email template
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static EmailTemplate Get(DbConnection connection, DbTransaction transaction, string key)
        {
            DbReader r = null;
            EmailTemplate t = null;
            try
            {
                r = new DbReader("spGetEmailTemplate", connection, transaction, new Dictionary<string, object> { { "key", key } });
                if (r.Read())
                {
                    t = ReadEmailTemplate(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return t;
        }

        public static EmailTemplate Get(string key)
        {
            return DbFactory.Connect<EmailTemplate, string>(Get, key);
        }


        // Private Methods

        private static EmailTemplate ReadEmailTemplate(DbReader r)
        {
            return new EmailTemplate
            {
                Body = r.Get<string>("chrBody"),
                DisplayName = r.Get<string>("chrDisplayName"),
                From = r.Get<string>("chrSender"),
                ID = r.Get<int>("intEmailTemplateID"),
                Key = r.Get<string>("chrKey"),
                Subject = r.Get<string>("chrSubject"),
                To = r.Get<string>("chrRecipient"),
                Bcc = r.Get<string>("chrBCC"),
                Cc = r.Get<string>("chrCC"),
                Credentials = new System.Net.NetworkCredential
                {
                    UserName = r.Get<string>("chrSender"),
                    Password = r.Get<string>("chrPassword")
                }
            };
        }
    }

}
