﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Setup
{
    public class CommonSettings
    {
        public static string ErrorEmailSenderAddress { get; set; }
        public static string ErrorEmailRecipientAddress { get; set; }
        public static string ErrorEmailApplicationName { get; set; }
        public static string ErrorEmailDisplayName { get; set; }
        public static string ErrorEmailPassword { get; set; }
		
		public static bool UseSecureLayer { get; set; }
    }
}
