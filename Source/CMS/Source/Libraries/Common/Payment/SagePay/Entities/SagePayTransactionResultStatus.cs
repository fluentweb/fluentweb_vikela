﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Payment.SagePay.Entities
{
    public class SagePayTransactionResultStatus
    {
        /// <summary>
        /// Transaction completed successfully with authorisation. 
        /// </summary>
        public const string OK = "OK";

        /// <summary>
        ///  The Sage Pay system could not authorise the transaction because the
        ///  details provided by the Customer were incorrect, or not authenticated by the acquiring bank.
        /// </summary>
        public const string NOT_AUTHED = "NOTAUTHED";

        /// <summary>
        /// The Transaction could not be completed  because the user 
        /// clicked the CANCEL button on the payment pages, or went inactive for 15 minutes or longer.
        /// </summary>
        public const string ABORT = "ABORT";

        /// <summary>
        ///  The Sage Pay System rejected the transaction because of the rules you have set on your account.
        /// </summary>
        public const string REJECTED = "REJECTED";

        /// <summary>
        ///  The 3D-Secure checks were performed successfully and the card details secured at Sage Pay.
        /// </summary>
        public const string AUTHENTICATED = "AUTHENTICATED";

        /// <summary>
        ///   3D-Secure checks failed or were not performed, but the card details are still secured at Sage Pay. 
        /// </summary>
        public const string REGISTERED = "REGISTERED";

        /// <summary>
        /// An error occurred at Sage Pay which meant the transaction could not be complete d successfully 
        /// </summary>
        public const string ERROR = "ERROR";        

    }
}
