﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Payment.SagePay.Enums;

namespace Common.Payment.SagePay.Entities
{
    public class SagePayResponse
    {
        /// <summary>
        /// Version number of the protocol of the system. This release will return 2.23
        /// </summary>
        public string VPSProtocol { get; set; }
        /// <summary>
        /// OK – Process executed without error
        /// MALFORMED – Input message was missing fields or badly formatted – normally will only occur during development.
        /// INVALID – Transaction was not registered because although the POST format was valid, some information supplied was invalid. E.g. incorrect vendor name or currency.
        /// ERROR – A problem occurred at Sage Pay which prevented transaction registration.
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Human-readable text providing extra detail for the Status message.
        /// </summary>
        public string StatusDetail { get; set; }

        /// <summary>
        /// Sage Pay’s ID to uniquely identify the Transaction on our system.
        /// </summary>
        public string VPSTxId { get; set; }

        /// <summary>
        /// A Security key which SAGE PAY uses to generate a MD5 Hash for to sign the Notification message. The signature is called VPSSignature.
        /// </summary>
        public string SecurityKey { get; set; }

        /// <summary>
        /// URL to which the Vendor must redirect the Customer to continue the Transaction
        /// </summary>
        public string NextURL { get; set; }

        /// <summary>
        /// Custom field to store request data for log purposes
        /// </summary>
        public string RequestData { get; set; }

        /// <summary>
        /// Custom field to store request purchase reference for log purposes
        /// </summary>
        public string VendorTxCode { get; set; }

        // The following proprty are used by the AUTHORISE and REFUND  Transaction types

        /// <summary>
        /// The Sage Pay  authorisation code (also called VPSAuthCode) for this Authorise.   
        /// returned from AUTHORISE and REFUND transactions
        /// </summary>
        public string TxAuthNo { get; set; }
    }
}
