﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Payment.SagePay.Enums;

namespace Common.Payment.SagePay.Entities
{
    public class SagePayTransactionResult
    {
        public string VPSProtocol { get; set; }
        public SagePayTxType TxType { get; set; }
        public string VendorTxCode { get; set; }
        public string VPSTxId { get; set; }
        public string Status { get; set; }
        public string StatusDetail { get; set; }
        public string TxAuthNo { get; set; }
        public string AVSCV2 { get; set; }
        public string AddressResult { get; set; }
        public string PostCodeResult { get; set; }
        public string CV2Result { get; set; }
        public string GiftAid { get; set; }
        public string ThreeDSecureStatus { get; set; }
        public string CAVV { get; set; }
        public string AddressStatus { get; set; }
        public string PayerStatus { get; set; }
        public string CardType { get; set; }
        public string Last4Digits { get; set; }
        public string VPSSignature { get; set; }
    }
}
