﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Payment.SagePay.Entities
{
    public class SagePayResponseEventArgs: EventArgs
    {
        public SagePayResponse Response { get; set; }

        public SagePayResponseEventArgs(SagePayResponse response)
        {
            Response = response;
        }
    }
}
