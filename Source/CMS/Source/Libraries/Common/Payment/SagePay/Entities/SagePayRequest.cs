﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Payment.SagePay.Enums;
using System.Collections.Specialized;
using System.Reflection;
using Common.Payment.SagePay.Interfaces;

namespace Common.Payment.SagePay.Entities
{
    public class SagePayRequest
    {

        /// <summary>
        /// Protocol version  2.23 current release
        /// </summary>
        public string VPSProtocol { get; set; }

        /// <summary>
        /// Transaction Type
        /// </summary>
        public SagePayTxType TxType { get; set; }

        /// <summary>
        /// Vendor login name  max 15 chars
        /// </summary>
        public string Vendor { get; set; }

        /// <summary>
        /// Custom transaction reference code transaction max 40 chars
        /// </summary>
        public string VendorTxCode { get; set; }

        /// <summary>
        /// Transaction Amount limited to 2 decimal places; can have thousand delimiters
        /// from 0.01 to 100,000.00
        /// </summary>
        public string Amount { get; set; }

        /// <summary>
        /// Three-letter currency code to ISO 4217 like: GBP, EUR and USD 
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// Free text description of goods or services being purchased max 100 chars
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Callback URL to which Notification POSTs are sent including http:// or https://  max 255 chars
        /// </summary>
        public string NotificationURL { get; set; }


        #region Billing Details

        /// <summary>
        /// Max 20 chars
        /// </summary>
        public string BillingSurname { get; set; }

        /// <summary>
        /// Max 20 chars
        /// </summary>
        public string BillingFirstNames { get; set; }
        /// <summary>
        /// Max 100 chars
        /// </summary>
        public string BillingAddress1 { get; set; }

        /// <summary>
        /// *Optional Max 100 chars
        /// </summary>
        public string BillingAddress2 { get; set; }

        /// <summary>
        /// Max 40 chars
        /// </summary>
        public string BillingCity { get; set; }

        /// <summary>
        /// Max 10 chars
        /// </summary>
        public string BillingPostCode { get; set; }

        /// <summary>
        /// ISO 3166-1 country code max 2 chars
        /// </summary>
        public string BillingCountry { get; set; }

        /// <summary>
        /// *Optional US only max 2 chars
        /// </summary>
        public string BillingState { get; set; }

        /// <summary>
        /// *Optional Max 20 chars
        /// </summary>
        public string BillingPhone { get; set; }

        #endregion
        
        #region Delivery Details

        /// <summary>
        /// Max 20 chars
        /// </summary>
        public string DeliverySurname { get; set; }

        /// <summary>
        /// Max 20 chars
        /// </summary>
        public string DeliveryFirstNames { get; set; }
        /// <summary>
        /// Max 100 chars
        /// </summary>
        public string DeliveryAddress1 { get; set; }

        /// <summary>
        /// *Optional Max 100 chars
        /// </summary>
        public string DeliveryAddress2 { get; set; }

        /// <summary>
        /// Max 40 chars
        /// </summary>
        public string DeliveryCity { get; set; }

        /// <summary>
        /// Max 10 chars
        /// </summary>
        public string DeliveryPostCode { get; set; }

        /// <summary>
        /// ISO 3166-1 country code max 2 chars
        /// </summary>
        public string DeliveryCountry { get; set; }

        /// <summary>
        /// *Optional US only max 2 chars
        /// </summary>
        public string DeliveryState { get; set; }

        /// <summary>
        /// *Optional Max 20 chars
        /// </summary>
        public string DeliveryPhone { get; set; }

        /// <summary>
        /// *Optional
        /// Optional mutliple addresses to be split by ':' colon char max 255 chars
        /// </summary>
        public string CustomerEmail { get; set; }

        /// <summary>
        /// *Optional
        /// Optional HTML format max 7500 chars
        /// </summary>
        public string Basket { get; set; }

        /// <summary>
        /// *Optional
        /// 1 or 0 to show gift aid box on payment screen
        /// </summary>
        public char AllowGiftAid { get; set; }

        /// <summary>
        /// *Optional
        /// 0 = If AVS/CV2 enabled then check them. If rules apply, use rules. (default)
        /// 1 = Force AVS/CV2 checks even if not enabled for the account. If rules apply, use rules.
        /// 2 = Force NO AVS/CV2 checks even if enabled on account.
        /// 3 = Force AVS/CV2 checks even if not enabled for the account but DON’T apply any rules.
        /// ignored for paypal transacitons
        /// </summary>
        public char ApplyAVSCV2 { get; set; }

        /// <summary>
        /// *Optional
        /// 0 = If 3D-Secure checks are possible and rules allow, perform the checks and apply the authorisation rules. (default)
        /// 1 = Force 3D-Secure checks for this transaction if possible and apply rules for authorisation.
        /// 2 = Do not perform 3D-Secure checks for this transaction and always authorise.
        /// 3 = Force 3D-Secure checks for this transaction if possible but ALWAYS obtain an auth code, irrespective of rule base.
        /// This field is ignored for PAYPAL transactions
        /// </summary>
        public char Apply3DSecure { get; set; }

        /// <summary>
        /// *Optional
        /// NORMAL (DEFAULT) or LOW
        /// A profile of LOW returns the new simpler payment pages which have only
        /// one step and minimal formatting. Designed to run in i-Frames. 
        /// Omitting this field or sending NORMAL renders the normal card selection screen.
        /// </summary>
        public string Profile { get; set; }

        /// <summary>
        /// *Optional
        /// 0 = This is a normal PayPal transaction, not the first in a series of payments (default)
        /// 1 = This is the first in a series of PayPal payments. Subsequent payments can be taken using REPEAT.
        /// </summary>
        public char BillingAgreement { get; set; }

        /// <summary>
        /// *Optional
        /// E = Use the e-commerce merchant account (default).
        /// C = Use the continuous authority merchant account (if present).
        /// M = Use the mail order, telephone order account (if present).
        /// </summary>
        public char AccountType { get; set; }

        #endregion


        /// <summary>
        /// VPSTxId of the AUTHENTICATE transaction against which the authorisation is required
        /// </summary>
        public string RelatedVPSTxId { get; set; }

        /// <summary>
        /// VendorTxCode of the AUTHENTICATE transaction against which the authorisation is required  
        /// </summary>
        public string RelatedVendorTxCode { get; set; }

        /// <summary>
        /// The SecurityKey of the AUTHENTICATE transaction sent back by the Sage Pay System when the transaction was registered.
        /// </summary>
        public string RelatedSecurityKey { get; set; }

        /// <summary>
        /// Use for REFUND
        /// The TxAuthNo of the original transaction as returned by the Sage Pay system when it was authorised.  
        /// </summary>
        public string RelatedTxAuthNo { get; set; }

        /// <summary>
        /// Used with CANCEL command
        /// As sent back by the Sage Pay system when the AUTHENTICATE  transaction was registered
        /// </summary>
        public string VPSTxId { get; set; }

        /// <summary>
        /// The SecurityKey of the AUTHENTICATE transaction as sent back by the Sage Pay System when the transaction occurred.
        /// </summary>
        public string SecurityKey { get; set; }

        public SagePayRequest()
        {

        }
    }
}
