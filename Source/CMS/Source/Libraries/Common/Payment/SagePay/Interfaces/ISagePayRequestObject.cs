﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Payment.SagePay.Enums;

namespace Common.Payment.SagePay.Interfaces
{
    public interface ISagePayRequestObject
    {
        #region Properties

        /// <summary>
        /// Custom transaction reference code transaction max 40 chars
        /// </summary>
        string VendorTxCode { get;}

        /// <summary>
        /// Transaction Amount limited to 2 decimal places; can have thousand delimiters
        /// from 0.01 to 100,000.00
        /// </summary>
        string Amount { get; }

        // Billing details

        /// <summary>
        /// Max 20 chars
        /// </summary>
        string Surname { get; }

        /// <summary>
        /// Max 20 chars
        /// </summary>
        string FirstName { get; }
        /// <summary>
        /// Max 100 chars
        /// </summary>
        string AddressLine1 { get; }

        /// <summary>
        /// *Optional Max 100 chars
        /// </summary>
        string AddressLine2 { get; }

        /// <summary>
        /// Max 40 chars
        /// </summary>
        string City { get; }

        /// <summary>
        /// Max 10 chars
        /// </summary>
        string PostalCode { get; }

        /// <summary>
        /// ISO 3166-1 country code max 2 chars
        /// </summary>
        string Country { get; }

        /// <summary>
        /// *Optional US only max 2 chars
        /// </summary>
        string State { get; }

        /// <summary>
        /// *Optional Max 20 chars
        /// </summary>
        string Phone { get; }

        #endregion
    }
}
