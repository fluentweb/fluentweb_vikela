﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Payment.SagePay.Enums
{
    /// <summary>
    /// Trasaction Type
    /// </summary>
    public enum SagePayTxType
    {
        
        /// <summary>
        /// Immediate payment
        /// </summary>
        PAYMENT,
        /// <summary>
        /// Transaction not sent to bank for completion the following morning.
        /// It is not sent until a RELEASE command is issued in My Sage Pay interface.
        /// </summary>
        DEFERRED,
        /// <summary>
        ///  Authenticate card
        /// </summary>
        AUTHENTICATE,
        /// <summary>
        /// Authorise payment
        /// </summary>
        AUTHORISE,
        /// <summary>
        ///  Prevent any other authorisations agains authenticated card (automatically happens after 90 days)
        /// </summary>
        CANCEL,        
        /// <summary>
        /// Refund partial or total amount of transaction
        /// </summary>
        REFUND
        ///// <summary>
        ///// ?? Can this be done from here or only My Sage Pay interface
        ///// </summary>
        //RELEASE, 
        //// <summary>
        //// Cancels an authorised payment before it is settled with bank the following morning.
        //// CAUTION: Voided transaction can never be reactivated. A transaction cannot be voided if it has been settled.
        //// </summary>
        //VOID
    }
}
