﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Net;
using System.Collections.Specialized;
using Common.Payment.SagePay.Entities;
using Common.Extensions;
using System.IO;
using System.Web.UI;
using Common.Payment.SagePay.Enums;
using System.Data.Common;
using Common.Payment.SagePay.DAC;
using Common.HttpUtils;

namespace Common.Payment.SagePay
{
    public class SagePayManager
    {
        public static string ORDER_SESSION_KEY = "ORDER_SESSION";


        public static string RegisterRequestURL { get; set; }
        public static string GatewayVendor { get; set; }
        public static string GatewayNotificationURL { get; set; }
        
        public static string AuthoriseRequestURL { get; set; }
        public static string CancelRequestURL { get; set; }
        public static string RefundRequestURL { get; set; }

        public static bool ImmediatePayment { get; set; }

        public static SagePayResponse InitializeGateway(SagePayRequest spr)
        {
            Dictionary<string, string> responseValues = new Dictionary<string, string>();
            SagePayResponse spResponse = null;

            // prepare data
            string dataString = String.Empty;

            #region Prepare request

            spr.NotificationURL = GatewayNotificationURL;
            spr.Vendor = GatewayVendor;
            spr.VPSProtocol = "2.23";
            if (ImmediatePayment)
                spr.TxType = Common.Payment.SagePay.Enums.SagePayTxType.PAYMENT;
            else
                spr.TxType = Common.Payment.SagePay.Enums.SagePayTxType.AUTHENTICATE;
                

            spr.Profile = "LOW"; // iframe mode

            #endregion

            try
            {
                NameValueCollection nvc = spr.ToNameValueCollection();
                dataString = nvc.ToQueryString(true);

                PostResult result = HttpUtilsManager.Post(RegisterRequestURL, nvc);

                using (StringReader sr = new StringReader(result.ResponseString))
                {
                    string line = String.Empty;
                    while ((line = sr.ReadLine()) != null)
                    {
                        string key = line.Substring(0, line.IndexOf('='));
                        string value = line.Substring(line.IndexOf('=') + 1);
                        responseValues.Add(key, value);
                    }
                }

                // populate response object
                spResponse = new SagePayResponse();
                spResponse.RequestData = dataString;
                spResponse.VendorTxCode = spr.VendorTxCode;

                spResponse.NextURL = responseValues.GetDictionaryValue("NextURL", String.Empty);
                spResponse.SecurityKey = responseValues.GetDictionaryValue("SecurityKey", String.Empty);
                spResponse.Status = responseValues.GetDictionaryValue("Status", SagePayResponseStatus.WEBSITE_ERROR);
                spResponse.StatusDetail = responseValues.GetDictionaryValue("StatusDetail", String.Empty);
                spResponse.VPSProtocol = responseValues.GetDictionaryValue("VPSProtocol", String.Empty);
                spResponse.VPSTxId = responseValues.GetDictionaryValue("VPSTxId", String.Empty);

            }
            catch (Exception ex)
            {
                return new SagePayResponse
                {
                    RequestData = dataString,
                    Status = SagePayResponseStatus.WEBSITE_ERROR,
                    StatusDetail = String.Format("{0}, {1}", ex.Message, ex.StackTrace)
                };
            }

            return spResponse;
        }

        public static SagePayResponse SendRequest(SagePayRequest spr)
        {
            Dictionary<string, string> responseValues = new Dictionary<string, string>();
            SagePayResponse spResponse = null;
            string dataString = String.Empty;

            spr.Vendor = GatewayVendor;
            spr.VPSProtocol = "2.23";
            spr.Currency = "GBP";
            try
            {
                NameValueCollection nvc = spr.ToNameValueCollection();
                dataString = nvc.ToQueryString(true);

                string urlToPostTo = String.Empty;
                switch (spr.TxType)
                {
                    case SagePayTxType.AUTHORISE:
                        urlToPostTo = AuthoriseRequestURL;
                        break;
                    case SagePayTxType.CANCEL:
                        urlToPostTo = CancelRequestURL;
                        break;
                    case SagePayTxType.REFUND:
                        urlToPostTo = RefundRequestURL;
                        break;
                    default:
                        break;
                }
                PostResult result = HttpUtilsManager.Post(urlToPostTo, nvc);

                using (StringReader sr = new StringReader(result.ResponseString))
                {
                    string line = String.Empty;
                    while ((line = sr.ReadLine()) != null)
                    {
                        string key = line.Substring(0, line.IndexOf('='));
                        string value = line.Substring(line.IndexOf('=') + 1);
                        responseValues.Add(key, value);
                    }
                }

                // populate response object
                spResponse = new SagePayResponse();
                spResponse.RequestData = dataString;
                spResponse.VendorTxCode = spr.VendorTxCode;
               
                spResponse.VPSProtocol = responseValues.GetDictionaryValue("VPSProtocol", String.Empty);
                spResponse.Status = responseValues.GetDictionaryValue("Status", SagePayResponseStatus.WEBSITE_ERROR);
                spResponse.StatusDetail = responseValues.GetDictionaryValue("StatusDetail", String.Empty);
                spResponse.TxAuthNo = responseValues.GetDictionaryValue("TxAuthNo", String.Empty);
                spResponse.VPSTxId = responseValues.GetDictionaryValue("VPSTxId", String.Empty);
                spResponse.SecurityKey = responseValues.GetDictionaryValue("SecurityKey", String.Empty); 

            }
            catch (Exception ex)
            {
                return new SagePayResponse
                {
                    RequestData = dataString,
                    Status = SagePayResponseStatus.WEBSITE_ERROR,
                    StatusDetail = String.Format("{0}, {1}", ex.Message, ex.StackTrace)
                };
            }

            return spResponse;
        }

    }
}
