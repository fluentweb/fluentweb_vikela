﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Drawing;

namespace Common.FileUtils
{
    public class FileUtilsManager
    {
        public static bool LargerThan4MB(HttpPostedFile file)
        {
            return file.ContentLength > 4194304; //4MB            
        }                
    }
}
