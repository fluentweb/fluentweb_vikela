﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Common.FileUtils.Export
{
    public class CSV
    {
        public static string WriteCommaDelimitedString(DataTable dtInput)
        {
            if (dtInput == null) return null;
            if (dtInput.Columns == null || dtInput.Columns.Count == 0) return null;
            if (dtInput.Rows == null || dtInput.Rows.Count == 0) return null;

            StringBuilder sb = new StringBuilder();

            //for (int i = 0; i < dtInput.Columns.Count; i++)
            //{
            //    // write column name in first row
            //    sb.Append(dtInput.Columns[i].ColumnName);
            //    // delimit if not last column else add new line
            //    sb.Append(i < (dtInput.Columns.Count - 1) ? "," : Environment.NewLine);
            //}

            var columnNames = from DataColumn dc in dtInput.Columns select dc.ColumnName;

            sb.AppendLine(String.Join(",", columnNames.ToArray()));


            foreach (DataRow dr in dtInput.Rows)
            {
                var currentRow = from string column in columnNames
                                 select dr[column].ToString().IndexOf(',') > -1 ?
                                     String.Format("\"{0}\"", dr[column]) :
                                     dr[column].ToString();
                sb.AppendLine(String.Join(",", currentRow.ToArray()));
            }





            return sb.ToString();

        }
    }
}
