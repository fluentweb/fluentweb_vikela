﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Website.Helpers;
using Website.Helpers.Base;
using Common.Email;
using FluentContent.DAC;
using Common.Extensions;

namespace Website
{
    public partial class Contact : PageBase
    {
        public override string PageUrl
        {
            get
            {
                return "Contact";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string EncodedResponse = Request.Form["g-Recaptcha-Response"];
            bool IsCaptchaValid = (reCaptchaGoogle.Validate(EncodedResponse) == "True" ? true : false);

            if (!IsCaptchaValid) return;

            bool result = false;


            DataTable dtValuationEmail = DAC_Item.GetItemByTitle("Email templates", "Contact Form");
            if (dtValuationEmail != null && dtValuationEmail.Rows.Count > 0)
            {

                string recipients = dtValuationEmail.GetDataTableValue(0, "chrText1", String.Empty);
                string from = dtValuationEmail.GetDataTableValue(0, "chrText2", String.Empty);
                string displayName = dtValuationEmail.GetDataTableValue(0, "chrText3", String.Empty);
                string cc = dtValuationEmail.GetDataTableValue(0, "chrText4", String.Empty);
                string bcc = dtValuationEmail.GetDataTableValue(0, "chrText5", String.Empty);
                string body = dtValuationEmail.GetDataTableValue(0, "chrText11", String.Empty);
                string subject = dtValuationEmail.GetDataTableValue(0, "chrText6", String.Empty);
                string password = dtValuationEmail.GetDataTableValue(0, "chrText7", String.Empty);

                // replace placeholders
                body = EmailManager.ReplaceKeys(body, new Dictionary<string, string> 
                 {              
                    { "##FullName##", txtFullname.Text.Trim() },
                    { "##CompanyName##", txtCompname.Text.Trim() },
                    { "##ContactNumber##",txtTelephone.Text.Trim() },
                    { "##EmailAddress##",txtEmail.Text.Trim() },
                    { "##Enquery##",txtEnquiry.Text.Trim() }
                });

                body = String.Format("<html><body>{0}</body></html>", body);
                NetworkCredential cred = new NetworkCredential(from, password);

                try
                {
                    result = EmailManager.SendEmail(recipients, from, displayName, subject, body, true, cc, bcc, null, cred);

                    Response.Redirect("~/contact?submitted=1#contact-form");
                }
                catch { }

            }
        }
    }
}