﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="Website.About" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="placeholderBody" runat="server">
    <div class="container-fluid main_banner inner about-us">
        <div class="row relative">
            <div class="banner">
                <div class="back-panel">
                    <img src="/<%=FluentContentSettings.ImageUploadFolder + CurrentPage.GetImageByKey("Banner")["chrPageImageURL"] %>" class="img-responsive" alt="<%=CurrentPage.GetImageByKey("Banner")["chrPageImageALT"] %>" />
                </div>
            </div>
            <div class="front-panel">
                <%=CurrentPage.GetTextByKey("Title")%>
            </div>
        </div>
    </div>
    <!-- main banner end -->

    <section class="about-block block1">
        <div class="container text-center">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-lg-offset-3">
                    <%=CurrentPage.GetTextByKey("Block1")%>
                </div>
            </div>
        </div>
    </section>
    <section class="highlight-about-block block2">
        <div class="container text-center">
            <div class="row-fluid">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-offset-2">
                    <%=CurrentPage.GetTextByKey("Block2")%>
                    <a href="/contact" class="btn btn-lg btn-default">CONTACT VEKILA</a>
                </div>
            </div>
    </section>
    <section class="about-application-proccess">
        <div class="container text-center">
            <div class="row-fluid">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                    <%=CurrentPage.GetTextByKey("Block3")%>
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <asp:ListView ID="lvGetStarted" runat="server">
                                <LayoutTemplate>
                                    <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <div class="item <%#Container.DataItemIndex == 0 ? " active" : string.Empty%>">
                                        <p>
                                            <span><%#Eval("chrText6") %></span>
                                            <%#Eval("chrText10") %>
                                        </p>
                                    </div>
                                </ItemTemplate>
                            </asp:ListView>
                        </div>
                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                            <span class="fa fa-angle-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                            <span class="fa fa-angle-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="accreditations-block">
        <div class="container text-center">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <%=CurrentPage.GetTextByKey("Block4")%>
                    <div class="row">
                        <ul class="accreditations">

                             <asp:ListView ID="lvAboutBlock4" runat="server">
                                <LayoutTemplate>
                                    <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <li>
                                        <%#(Eval("chrItemURL") != null) ? "<img src=\"" + ConfigurationManager.AppSettings["ImageUploadFolder"] + Eval("chrItemImageURL") + "\" alt=\"" + Eval("chrItemImageALT") + "\">"
                                         : "<a href=\"" + Eval("chrItemURL") + "\"><img src=\"" + ConfigurationManager.AppSettings["ImageUploadFolder"] + Eval("chrItemImageURL") + "\" alt=\"" + Eval("chrItemImageALT") + "\"></a>"%>
                                    </li>
                                </ItemTemplate>
                            </asp:ListView>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderBottom" runat="server">
</asp:Content>
