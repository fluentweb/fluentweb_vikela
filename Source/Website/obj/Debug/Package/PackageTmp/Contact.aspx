﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="Website.Contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="placeholderBody" runat="server">
    <!-- main banner start -->
    <div class="container-fluid main_banner inner about-us">
        <div class="row relative">
            <div class="banner">
                <div class="back-panel">
                    <img src="/<%=FluentContentSettings.ImageUploadFolder + CurrentPage.GetImageByKey("Banner")["chrPageImageURL"] %>" class="img-responsive" alt="<%=CurrentPage.GetImageByKey("Banner")["chrPageImageALT"] %>" />
                </div>
            </div>
            <div class="front-panel">
                <%=CurrentPage.GetTextByKey("Title")%>
            </div>
        </div>
    </div>
    <!-- main banner end -->

    <section class="about-block fifth-block block3">
        <div class="container text-center">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-lg-offset-3">
                    <img src="<%="/uploaded/images/" + CurrentPage.GetImageByKey("Logo")["chrPageImageURL"] %>" alt="<%=CurrentPage.GetImageByKey("Logo")["chrPageImageALT"] %>" />
                    <br>
                    <div class="nk-bottom2">
                        <div class="mylist"><a href="mailto:<%=CurrentPage.GetTextByKey("Email")%>"><i class="fa fa-envelope"></i><%=CurrentPage.GetTextByKey("Email")%></a></div>
                        <div class="mylist"><a href="tel:<%=CurrentPage.GetTextByKey("Tell")%>"><i class="fa fa-phone-d"></i><%=CurrentPage.GetTextByKey("Tell")%></a></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="information-block fifth-block">
        <div class="container text-center">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-lg-offset-3">
                    <%=CurrentPage.GetTextByKey("Block2")%>
                    <br>
                    <ul class="nk-social">
                        <li><a href="<%=CurrentPage.GetTextByKey("UrlFaceBook")%>"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="<%=CurrentPage.GetTextByKey("UrlTwitter")%>"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="<%=CurrentPage.GetTextByKey("UrlLinkedIn")%>"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="contact-block">
        <div class="container text-center">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-lg-offset-3">
                    <%=CurrentPage.GetTextByKey("Block3")%>
                    <div role="form contact-form">

                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <asp:TextBox type="text" name="full_name" ID="txtFullname" CssClass="form-control input-lg" placeholder="Full Name" TabIndex="1" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <asp:TextBox type="text" name="comp_name" ID="txtCompname" CssClass="form-control input-lg" placeholder="Company Name" TabIndex="2" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <asp:TextBox type="tel" name="telephone" ID="txtTelephone" CssClass="form-control input-lg" placeholder="Contact Number" TabIndex="5" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <asp:TextBox type="email" name="email" ID="txtEmail" CssClass="form-control input-lg" placeholder="Email Address" TabIndex="4" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:TextBox CssClass="form-control" Rows="5" ID="txtEnquiry" TextMode="MultiLine" placeholder="Enquiry" runat="server"></asp:TextBox>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="g-recaptcha" data-sitekey="6Ld3zx8TAAAAACWRRv01yLVQQ2mML0Lvgs_Nb2r7"></div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 text-right">
                                <asp:Button ID="btnSubmit" OnClick="btnSubmit_Click"  CssClass="btn btn-lg btn-default nk-btn" runat="server" Text="SUBMIT" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderBottom" runat="server">
</asp:Content>
