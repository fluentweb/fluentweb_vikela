﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TermsAndConditions.aspx.cs" Inherits="Website.TermsAndConditions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="placeholderBody" runat="server">
    <!-- main banner start -->
    <div class="container-fluid main_banner inner about-us">
        <div class="row relative">
            <div class="banner">
                <div class="back-panel">
                    <img src="/<%=FluentContentSettings.ImageUploadFolder + CurrentPage.GetImageByKey("Banner")["chrPageImageURL"] %>" class="img-responsive" alt="<%=CurrentPage.GetImageByKey("Banner")["chrPageImageALT"] %>" />
                </div>
            </div>
            <div class="front-panel">
                <%=CurrentPage.GetTextByKey("Title")%>
            </div>
        </div>
    </div>
    <!-- main banner end -->
    <section class="about-block fourth-block block5">
        <div class="container text-center">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-lg-offset-3">
                    <%=CurrentPage.GetTextByKey("Block1")%>
                </div>
            </div>
            <br>
            <br>
            <a runat="server" href="~/contact" class="btn btn-lg btn-default">CONTACT VEKILA</a>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderBottom" runat="server">
</asp:Content>
