﻿var pin_config = {
	'default':{
		'pinShadow':'#000', //shadow color below the points
		'pinShadowOpacity':'50', //shadow opacity, value, 0-100
	},
	'points':[
	// {
	// 	'shape':'rectangle',//choose the shape of the pin circle or rectangle
	// 	'hover': '<u><b>Lagos</b></u><br>This pin when clicked will open<br>the URL in a <span style="color:black; background-color:#a9f038;"><b>NEW</b></span> window.',//the content of the hover popup
	// 	'pos_X':155,//location of the pin on X axis
	// 	'pos_Y':255,//location of the pin on Y axis
	// 	'width':8,//width of the pin if rectangle (if circle use diameter)
	// 	'height':8,//height of the pin if rectangle (if circle delete this line)
	// 	'outline':'#FFF',//outline color of the pin
	// 	'thickness':1,//thickness of the line (0 to hide the line)
	// 	'upColor':'#0000FF',//color of the pin when map loads
	// 	'overColor':'#3399ff',//color of the pin when mouse hover
	// 	'downColor':'#00ffff',//color of the pin when clicked 
	// 	//(trick, if you make this pin un-clickable > make the overColor and downColor the same)
	// 	'url':'#',//URL of this pin
	// 	'target':'new_window',//'new_window' opens URL in new window//'same_window' opens URL in the same window //'none' pin is not clickable
	// 	'enable':true,//true/false to enable/disable this pin
	// },
	// {
	// 	'shape':'circle',
	// 	'hover': '<u><b>Cairo</b></u><br><img src="example.png">Here you can write some <b>HTML</b><br>formated text.<br>This pin when clicked will open<br>the URL in the <span style="color:black; background-color:#a9f038;"><b>SAME</b></span> window.',
	// 	'pos_X':346,
	// 	'pos_Y':68,
	// 	'diameter':8,
	// 	'outline':'#FFF',
	// 	'thickness':1,
	// 	'upColor':'#FF0000',
	// 	'overColor':'#FFEE88',
	// 	'downColor':'#00ffff',
	// 	'url':'#',
	// 	'target':'same_window',
	// 	'enable':true,
	// },
	// {
	// 	'shape':'circle',
	// 	'hover': 'Johannesburg',
	// 	'pos_X':330,
	// 	'pos_Y':521,
	// 	'diameter':8,
	// 	'outline':'#FFF',
	// 	'thickness':1,
	// 	'upColor':'#FF0000',
	// 	'overColor':'#FFEE88',
	// 	'downColor':'#00ffff',
	// 	'url':'#',
	// 	'target':'same_window',
	// 	'enable':true,
	// },
	// {
	// 	'shape':'circle',
	// 	'hover': 'Khartoum',
	// 	'pos_X':360,
	// 	'pos_Y':175,
	// 	'diameter':8,
	// 	'outline':'#FFF',
	// 	'thickness':1,
	// 	'upColor':'#FF0000',
	// 	'overColor':'#FFEE88',
	// 	'downColor':'#00ffff',
	// 	'url':'#',
	// 	'target':'same_window',
	// 	'enable':true,
	// },
	// {
	// 	'shape':'circle',
	// 	'hover': 'Alexandria',
	// 	'pos_X':338,
	// 	'pos_Y':57,
	// 	'diameter':8,
	// 	'outline':'#FFF',
	// 	'thickness':1,
	// 	'upColor':'#FF0000',
	// 	'overColor':'#FFEE88',
	// 	'downColor':'#00ffff',
	// 	'url':'#',
	// 	'target':'same_window',
	// 	'enable':true,
	// },
	// {
	// 	'shape':'circle',
	// 	'hover': 'Abidjan',
	// 	'pos_X':98,
	// 	'pos_Y':265,
	// 	'diameter':8,
	// 	'outline':'#FFF',
	// 	'thickness':1,
	// 	'upColor':'#FF0000',
	// 	'overColor':'#FFEE88',
	// 	'downColor':'#00ffff',
	// 	'url':'#',
	// 	'target':'same_window',
	// 	'enable':true,
	// },
	// {
	// 	'shape':'circle',
	// 	'hover': 'Casablanca',
	// 	'pos_X':80,
	// 	'pos_Y':37,
	// 	'diameter':8,
	// 	'outline':'#FFF',
	// 	'thickness':1,
	// 	'upColor':'#FF0000',
	// 	'overColor':'#FFEE88',
	// 	'downColor':'#00ffff',
	// 	'url':'#',
	// 	'target':'same_window',
	// 	'enable':true,
	// },
	// {
	// 	'shape':'circle',
	// 	'hover': 'Cape Town',
	// 	'pos_X':257,
	// 	'pos_Y':586,
	// 	'diameter':8,
	// 	'outline':'#FFF',
	// 	'thickness':1,
	// 	'upColor':'#FF0000',
	// 	'overColor':'#FFEE88',
	// 	'downColor':'#00ffff',
	// 	'url':'#',
	// 	'target':'same_window',
	// 	'enable':true,
	// },
	// {
	// 	'shape':'circle',
	// 	'hover': 'Accra',
	// 	'pos_X':125,
	// 	'pos_Y':264,
	// 	'diameter':8,
	// 	'outline':'#FFF',
	// 	'thickness':1,
	// 	'upColor':'#FF0000',
	// 	'overColor':'#FFEE88',
	// 	'downColor':'#00ffff',
	// 	'url':'#',
	// 	'target':'same_window',
	// 	'enable':true,
	// },
	// {
	// 	'shape':'circle',
	// 	'hover': 'Tunis',
	// 	'pos_X':199,
	// 	'pos_Y':7,
	// 	'diameter':8,
	// 	'outline':'#FFF',
	// 	'thickness':1,
	// 	'upColor':'#FF0000',
	// 	'overColor':'#FFEE88',
	// 	'downColor':'#00ffff',
	// 	'url':'#',
	// 	'target':'same_window',
	// 	'enable':true,
	// },
	]
}
