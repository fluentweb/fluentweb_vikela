﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Website.index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="placeholderBody" runat="server">
    <!-- main banner start -->
    <div class="container-fluid main_banner">
        <div class="row relative">
            <div class="banner">
                <div class="back-panel">
                    <img src="/<%=FluentContentSettings.ImageUploadFolder + CurrentPage.GetImageByKey("Banner")["chrPageImageURL"] %>" class="img-responsive" alt="<%=CurrentPage.GetImageByKey("Banner")["chrPageImageALT"] %>" />
                </div>
            </div>
            <div class="front-panel">
                <%=CurrentPage.GetTextByKey("Title")%>

                <div class="cdropdown" data-cdropdown>
                    <div class="block">
                        <h2 class="Get_started"><a href="javascript:;"><%=CurrentPage.GetTextByKey("MenuText")%> <i class="fa fa-angle-down pull-right"></i></a></h2>
                        <input type="hidden" id="selectbox" name="" value="">
                    </div>
                    <div class="dropdown-list">
                        <ul>
                            <asp:ListView ID="lvGetStarted" runat="server">
                                <LayoutTemplate>
                                    <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <li><a href="<%#Eval("chrLinkURL") %>"><%#Eval("chrLinkTitle") %></a></li>
                                </ItemTemplate>
                            </asp:ListView>
                            <li class="info">
                                <%=CurrentPage.GetTextByKey("MenuFooterText")%>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- main banner end -->
    <section class="regions-block">
        <div class="container text-center">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-lg-offset-3">
                    <%=CurrentPage.GetTextByKey("Block1")%>
                    <a runat="server" href="~/about" class="btn btn-default">about vikela</a>
                </div>
            </div>
        </div>
    </section>
    <section class="highlight-block home">
        <div class="container home text-center">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-offset-2">
                    <img src="<%="/uploaded/images/" + CurrentPage.GetImageByKey("Logo")["chrPageImageURL"] %>" alt="<%=CurrentPage.GetImageByKey("Logo")["chrPageImageALT"] %>" />
                    <%=CurrentPage.GetTextByKey("Block2")%>
                </div>
            </div>
        </div>
    </section>
    <section class="regions-block">
        <div class="container text-center">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-offset-2">
                    <%=CurrentPage.GetTextByKey("Block3")%>
                    <a href="#" class="btn btn-lg btn-default">get started</a>
                </div>
            </div>

        </div>
    </section>
    <section class="block application-proccess">
        <div class="container text-center">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                    <%=CurrentPage.GetTextByKey("Block4")%>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                    <ul class="application-list">
                        <asp:ListView ID="lvApplicationSteps" runat="server">
                            <LayoutTemplate>
                                <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <li>
                                    <a href="<%#Eval("chrItemURL") %>" data-toggle="tooltip" data-placement="top" data-parent="body" title="<%#Eval("chrText1") %>">
                                        <%#Eval("chrText10") %>
                                        <span class="text"><%#Eval("chrItemTitle") %></span>
                                    </a>
                                </li>
                            </ItemTemplate>
                        </asp:ListView>
                        <%--                        <li>
                            <a href="#" data-toggle="tooltip" data-placement="top" data-parent="body" title="Choose Log In">
                                <span class="picture ic ic-user"></span>
                                <span class="text">Log In</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" data-toggle="tooltip" data-placement="top" data-parent="body" title="Choose your Details">
                                <span class="picture ic ic-applicant"></span>
                                <span class="text">Details</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" data-toggle="tooltip" data-placement="top" data-parent="body" title="Choose your Trademark">
                                <span class="picture ic ic-trademark"></span>
                                <span class="text">Trademark</span>
                            </a>
                        </li>

                        <li>
                            <a href="#" data-toggle="tooltip" data-placement="top" data-parent="body" title="Choose your Classification">
                                <span class="picture ic ic-classify-cl"></span>
                                <span class="text">Classification</span>
                            </a>
                        </li>
                        <!--<li>
						<a href="#" data-toggle="tooltip" data-placement="top"  data-parent="body" title="Choose your Classify P">
							<span class="picture ic ic-classify-p"></span>
							<span class="text">Classify P</span>
						</a>
					</li>-->
                        <li>
                            <a href="#" data-toggle="tooltip" data-placement="top" data-parent="body" title="Add your Info">
                                <span class="picture ic ic-info"></span>
                                <span class="text">Add Info</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" data-toggle="tooltip" data-placement="top" data-parent="body" title="Choose your POA">
                                <span class="picture ic ic-attorney"></span>
                                <span class="text">POA</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" data-toggle="tooltip" data-placement="top" data-parent="body" title="Choose your Payment">
                                <span class="picture ic ic-wallet"></span>
                                <span class="text">Payment</span>
                            </a>
                        </li>

                        <li>
                            <a href="#" data-toggle="tooltip" data-placement="top" data-parent="body" title="Timeline">
                                <span class="picture ic ic-clock"></span>
                                <span class="text">Timeline</span>
                            </a>
                        </li>--%>
                    </ul>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderBottom" runat="server">
</asp:Content>
