﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NewsDetail.aspx.cs" Inherits="Website.NewsDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="placeholderBody" runat="server">
    <div class="container-fluid main_banner inner about-us">
        <div class="row relative">
            <div class="banner">
                <div class="back-panel">
                    <img src="/<%=FluentContentSettings.ImageUploadFolder + CurrentPage.GetImageByKey("Banner")["chrPageImageURL"] %>" class="img-responsive" alt="<%=CurrentPage.GetImageByKey("Banner")["chrPageImageALT"] %>" />
                </div>
            </div>
            <div class="front-panel">
                <%=CurrentPage.GetTextByKey("Title")%>
            </div>
        </div>
    </div>
    <!-- main banner end -->
    <asp:ListView ID="lvNewsDetail" runat="server">
        <LayoutTemplate>
            <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
        </LayoutTemplate>
        <ItemTemplate>
            <section class="highlight-news-block third-block filter single_new">
                <div class="container text-center">
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-lg-offset-3 nopadding">
                            <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2 nopadding-left">
                                <div class="nk-top pull-left">
                                    <a href="#" class="pull-left">
                                        <div class="mylist"><i class="fa fa-angle-left"></i></div>
                                        <div class="mylist"><asp:LinkButton ID="lnBackT" OnClick="lnBack_Click" runat="server">Back</asp:LinkButton></div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-6 col-md-6 col-lg-8 hidden-xs">
                                <div class="nk-top">
                                    <div class="cat_no mylist"><%=ParentTitle %></div>
                                    <div class="pot_date mylist"><%#String.Format("{0:d MMMM yyyy}", Convert.ToDateTime(Eval("datStart"))) %></div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2 nopadding-right">
                                <div class="nk-top pull-right">
                                    <div class="pull-right top_social">
                                <div class="fb mylist"><a href="<%=ClientSettingsHelper.Get("facebook_url").Value%>"><i class="fa fa-facebook"></i></a></div>
                                <div class="twt mylist"><a href="<%=ClientSettingsHelper.Get("twitter_url").Value%>""><i class="fa fa-twitter"></i></a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="highlight-news-block third-block single_new">
                <div class="container text-center">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-offset-2">
                            <div class="clearfix"></div>
                            <%--../images/single_news_img.png--%>
                            <img src="/<%#ConfigurationManager.AppSettings["ImageUploadFolder"] + Eval("chrItemImageURL") %>" alt="<%#Eval("chrItemImageALT") %>" class="img-responsive center-block" />
                            <br>
                            <br>
                            <h2><%#Eval("chrItemTitle")%></h2>
                            <br>
                            <p>
                                <%#Eval("chrText1")%>
                                <br>
                                <br>
                                <span class="detail"><%# Eval("chrText2")%>
                                </span>
                            </p>
                            <hr class="seperater">
                            <h4><span class="detail"><%# Eval("chrText3")%>
                                <br>

                                <%#Eval("chrText5")%></h4>
                            <p>
                                <span class="detail"><%#Eval("chrText6")%>
                                </span>
                            </p>
                        </div>
                    </div>
                </div>
            </section>
        </ItemTemplate>
    </asp:ListView>

    <section class="highlight-news-block third-block filter single_new bottom">
        <div class="container text-center">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-offset-2">
                    <div class="nk-top">
                        <a href="#">
                            <div class="mylist"><i class="fa fa-angle-left"></i></div>
                            <div class="mylist"><asp:LinkButton ID="lnBackB" OnClick="lnBack_Click" runat="server">Back</asp:LinkButton></div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderBottom" runat="server">
</asp:Content>
