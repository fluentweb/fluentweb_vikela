'use strict';
(function($) {

    $('header ul.nav li.dropdown').hover(function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
    }, function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
    });


    /**** following plugin ************/

    $.fn.followTo = function(pos) {
        var $this = this,
            $window = $(window);

        $window.scroll(function(e) {
            if ($window.scrollTop() > pos) {
                $this.css({
                    position: 'absolute',
                    top: pos
                });
            } else {
                $this.css({
                    position: 'fixed',
                    top: 0
                });
            }
        });
    };

    $('.followTo').followTo(250);

    $.fn.fixnav = function(obj) {
        var $default = {
                classname: '',
                scrollpx: 50
            },
            f = $.extend({}, $default, obj);
        var $this = this,
            $window = $(window);

        $window.scroll(function(e) {
            var c = f.classname;
            if ($window.scrollTop() > f.scrollpx) {
                $this.addClass(c);
                console.log(c);
            } else {
                $this.removeClass(c);
            }
        });
    }
    $('header').fixnav({
        classname: "header-fix-top",
        scrollpx: 100
    });

    //tooltip 

    $('[data-toggle="tooltip"]').tooltip();

    // $.fn.cdropdown = function(obj) {

    //     return $(this).each(function() {
    //         var _this = $(this);
    //         $(this).click(function() {
    //             $(this).find('i').removeClass('fa-angle-up').addClass('fa-angle-down')
    //             _this.toggleClass('open');
    //             if (_this.hasClass("open")) {
    //                 $(this).find('i').removeClass('fa-angle-down').addClass('fa-angle-up')
    //                 console.log(1);
    //             }
    //         });
    //     })

    // }

    /****** given by client - 20052016 **************/
    $.fn.cdropdown = function(e) {        
        return $(this).each(function() {
            var _this = $(this);
            $(this).click(function() {
                $(this).find('i').removeClass('fa-angle-up').addClass('fa-angle-down')
                _this.toggleClass("open", 1000, "easeOutSine" );
                if (_this.hasClass("open")) {
                    $(this).find('i').removeClass('fa-angle-down').addClass('fa-angle-up')
                    console.log(1);
                }
            });
        })

    }


    $('[data-cdropdown]').cdropdown();

    // push menu js

    var showLeftPush = document.getElementById('showLeftPush'),
        //     menuRight = document.getElementById( 'cbp-spmenu-s2' ),
        //     menuTop = document.getElementById( 'cbp-spmenu-s3' ),
        //     menuBottom = document.getElementById( 'cbp-spmenu-s4' ),
        //     showLeft = document.getElementById( 'showLeft' ),
        //     showRight = document.getElementById( 'showRight' ),
        //     showTop = document.getElementById( 'showTop' ),
        //     showBottom = document.getElementById( 'showBottom' ),
        menuLeft = document.getElementById('cbp-spmenu-s1'),
        //     
        //     showRightPush = document.getElementById( 'showRightPush' ),
        body = document.body;

    // showLeft.onclick = function() {
    //     classie.toggle( this, 'active' );
    //     classie.toggle( menuLeft, 'cbp-spmenu-open' );
    //     disableOther( 'showLeft' );
    // };
    // showRight.onclick = function() {
    //     classie.toggle( this, 'active' );
    //     classie.toggle( menuRight, 'cbp-spmenu-open' );
    //     disableOther( 'showRight' );
    // };
    // showTop.onclick = function() {
    //     classie.toggle( this, 'active' );
    //     classie.toggle( menuTop, 'cbp-spmenu-open' );
    //     disableOther( 'showTop' );
    // };
    // showBottom.onclick = function() {
    //     classie.toggle( this, 'active' );
    //     classie.toggle( menuBottom, 'cbp-spmenu-open' );
    //     disableOther( 'showBottom' );
    // };
    showLeftPush.onclick = function() {
        classie.toggle(this, 'active');
        classie.toggle(body, 'cbp-spmenu-push-toright');
        classie.toggle(menuLeft, 'cbp-spmenu-open');
        // disableOther( 'showLeftPush' );
    };
    // showRightPush.onclick = function() {
    //     classie.toggle( this, 'active' );
    //     classie.toggle( body, 'cbp-spmenu-push-toleft' );
    //     classie.toggle( menuRight, 'cbp-spmenu-open' );
    //     disableOther( 'showRightPush' );
    // };

    // function disableOther( button ) {
    //     if( button !== 'showLeft' ) {
    //         classie.toggle( showLeft, 'disabled' );
    //     }
    //     if( button !== 'showRight' ) {
    //         classie.toggle( showRight, 'disabled' );
    //     }
    //     if( button !== 'showTop' ) {
    //         classie.toggle( showTop, 'disabled' );
    //     }
    //     if( button !== 'showBottom' ) {
    //         classie.toggle( showBottom, 'disabled' );
    //     }
    //     if( button !== 'showLeftPush' ) {
    //         classie.toggle( showLeftPush, 'disabled' );
    //     }
    //     if( button !== 'showRightPush' ) {
    //         classie.toggle( showRightPush, 'disabled' );
    //     }
    // }

    // var bannerAutoSet = function(w,h,target){
    //         var totm = Math.max(w,h);
    //         var tot = totm/2;
    //         h = h+54;
    //         target.css({
    //             "height":h*2,
    //             "background-position":"center "+(h/2)+"px",
    //             "margin-top":"-"+(h/2)+"px",
    //         });
    //         console.log(tot/2);
    // };


    var bannerAutoSet = function(w, h, target, img,margin, minus) {
        var H = $(img).height(),
            W = $(img).width();

        var totm = Math.max(W, H);
        // console.log($(img).height());

        if($(target).hasClass('about-us')) {
            // console.log('hhh');
            return;
        }
        var margin =margin;
        target.css({
            "height": (totm-minus),
            "background-position": "center " + (totm / 2) + "px",
            "margin-top": "-"+margin+ "%",
        });
    };

    function onloadscript() {
        var w = $(window).width(),
            h = $(window).height(),
            margin = 50,
            minus = 0,
            target = $('.main_banner .banner .back-panel');
            
            if($('.main_banner').hasClass('inner')) {
               margin = 61.0;
               minus = 30;
            } 
            
        var img = $('.main_banner .banner .back-panel > img')

        bannerAutoSet(w, h, target, img,margin,minus);
    }

    $(window).on('resize', function() {
        // console.log('hello');
        onloadscript();
    });

    $('.main_banner .banner .back-panel').each(function() {
        var img = $(this).find('img:eq(0)');
        var imgPath = img.attr('src');
        $(img).css('display', "none");
        console.log(imgPath);
        $(this).css({
            "background": "url(" + imgPath + ") no-repeat scroll center center /83.3% auto"
        });
    })

    onloadscript();
    /****** custom_search on click *********/

    $('.custom_search').on('click',function(){
        $(this).attr('value','');
    });

    console.log('Document ready!!!'); // this not need to live site.
}(jQuery));