﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Website.Helpers;
using Website.Helpers.Base;

namespace Website
{
    public partial class index : PageBase
    {
        public override string PageUrl
        {
            get
            {
                return "Index";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                LoadGetStartedMenu();
                LoadApplicationSteps();
            }
        }

        private void LoadGetStartedMenu()
        {
            lvGetStarted.DataSource = MenuHelper.LoadMenu("Get Started Menu");
            lvGetStarted.DataBind();
        }

        private void LoadApplicationSteps()
        {
            lvApplicationSteps.DataSource = ItemHelper.LoadItemDisplay("Application Steps");
            lvApplicationSteps.DataBind();
        }

    }
}