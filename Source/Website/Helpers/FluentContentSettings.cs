﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Website.Helpers
{
    public class FluentContentSettings
    {
        public static string ImageUploadFolder { get; set; }
        public static string DocumentUploadFolder { get; set; }
    }
}
