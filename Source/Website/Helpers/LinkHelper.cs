﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Website.Helpers
{
    public class LinkHelper
    {
        public static string ResolveLink(object url)
        {
            return VirtualPathUtility.ToAbsolute(String.Format("~/{0}", url.ToString()));
        }
    }
}