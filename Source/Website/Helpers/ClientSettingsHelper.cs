﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAccess.Base;
using DataAccess;
using System.Data.Common;

namespace Website.Helpers
{
    public class ClientSetting
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public int ClientID { get; set; }

    }

    public class ClientSettingsHelper
    {
        public static ClientSetting Get(string key)
        {
            return DbFactory.Connect<ClientSetting, string>(Get, key);
        }

        public static ClientSetting Get(DbConnection connection, DbTransaction transaction, string key)
        {
            ClientSetting item = null;
            DbReader r = null;
            try
            {
                r = new DbReader("spClientSetting_Select", connection, transaction, new Dictionary<string, object> 
                {
                    { "ClientID", FluentContent.Settings.ClientID },
                    { "Key", key }
                });
                while (r.Read())
                {
                    item = ReadClientSetting(r);
                }
            }
            finally
            {
                if (r != null) r.Close();
            }

            return item;
        }

        private static ClientSetting ReadClientSetting(DbReader r)
        {
            return new ClientSetting
            {
                ClientID = r.Get<int>("intClientID"),
                Key = r.Get<string>("chrSettingKey"),
                Value = r.Get<string>("chrSettingValue")
            };
        }
    }
}
