﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Website.Helpers
{
    public class SessionKeys
    {
        public const string USER_SESSION = "USER_SESSION";
        public const string ENTITY_SESSION = "ENTITY_SESSION";
        public const string ORDER_SESSION = "ORDER_SESSION";
    }
}
