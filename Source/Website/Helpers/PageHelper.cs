﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using FLContentSiteAssist.BusinessLogic;
using Common.Extensions;

namespace Website.Helpers
{
    public class PageObject
    {
        public DataTable Page { get; set; }
        public DataTable PageText { get; set; }
        public DataTable PageImages { get; set; }

        public string GetTextByKey(string key)
        {
            if (PageText == null) return String.Empty;
            DataRow[] result = PageText.Select("chrPageTemplateTextTypeKey = '" + key + "'");
            if (result == null || result.Length == 0) return String.Empty;
            return CommonHelper.ReplaceDomainName(result[0]["chrPageText"].ToString());
        }

        public DataRow GetImageByKey(string key)
        {
            if (PageImages == null) return null;
            DataRow[] result = PageImages.Select("chrPageTemplateImageTypeKey = '" + key + "'");
            if (result == null || result.Length == 0) return null;
            return result[0];
        }
    }

    public class PageHelper
    {        

        public static PageObject LoadPage(string url)
        {
            ProcessGetPageByURL getPage = new ProcessGetPageByURL();
            getPage.Page = new FLContentSiteAssist.Common.Page { PageURL = url };
            getPage.Invoke();

            if (getPage.ResultSet.HasRows(0))
            {
                PageObject p = new PageObject();
                string pageName = getPage.ResultSet.Tables[0].GetDataTableValue<string>(0, "chrPage", String.Empty);

                p.Page = getPage.ResultSet.Tables[0];
                p.PageText = LoadPageText(pageName);
                p.PageImages = LoadPageImages(pageName);

                return p;
            }
            return new PageObject();
        }

        public static DataTable LoadPageText(string pageName)
        {
            ProcessGetPageText getText = new ProcessGetPageText();
            getText.SitePage = new FLContentSiteAssist.Common.SitePage { Name = pageName };
            getText.Invoke();

            if (getText.ResultSet.HasRows(0))
                return getText.ResultSet.Tables[0];
            return null;
        }

        public static DataTable LoadPageImages(string pageName)
        {
            ProcessGetPageImage getImage = new ProcessGetPageImage();
            getImage.SitePage = new FLContentSiteAssist.Common.SitePage { Name = pageName };
            getImage.Invoke();

            if (getImage.ResultSet.HasRows(0))
                return getImage.ResultSet.Tables[0];
            return null;
        }
    }
}
