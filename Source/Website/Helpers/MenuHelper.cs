﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using FLContentSiteAssist.BusinessLogic;

namespace Website.Helpers
{
    public class MenuHelper
    {
        public static DataSet LoadMenu(string linkLocation)
        {
            ProcessGetLinkByLocationName links = new ProcessGetLinkByLocationName();
            links.Link = new FLContentSiteAssist.Common.Link
            {
                Location = linkLocation
            };
            links.Invoke();
            return links.ResultSet;
        }
    }
}
