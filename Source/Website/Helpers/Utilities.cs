﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web;
using Common.Extensions;
using System.IO;

namespace Website.Helpers
{
    public class Utilities
    {
        public static string GetPageLink(HttpRequest Request, int PageNumber, int Pages)
        {
            if (Pages == 0) return string.Empty;
            NameValueCollection collection = HttpUtility.ParseQueryString(Request.Url.Query);
            collection.Remove("page");
            string _newline = "\r\n";
            //string pageName = new FileInfo(Request.Url.LocalPath).Name;
            string pageName = "news";
            StringBuilder _html = new StringBuilder();
            _html.Append("<ul class=\"pagination\">");
            if (PageNumber == 1)
            {
                _html.Append("<li id=\"editable_previous\" class=\"previous disabled\"> <a href=\"#\" aria-controls=\"editable\" data-dt-idx=\"0\" tabindex=\"0\"><</a></li>" + _newline);
            }
            else
            {
                _html.Append(String.Format("<li id=\"editable_previous\" class=\"previous\"><a aria-controls=\"editable\" data-dt-idx=\"\0\" tabindex=\"0\" href=\"" + pageName + "?page={0}{1}\"><</a></li>", PageNumber - 1, collection.Count > 0 ? collection.Keys[0] != null ? collection.Keys[0] != null ? ("&" + collection.ToQueryString()) : String.Empty : String.Empty : String.Empty) + _newline);
            }
            if (Pages <= 11)
            {
                for (int _pagetolist = 1; _pagetolist <= Pages; _pagetolist++)
                {
                    if (_pagetolist == PageNumber)
                    {
                        _html.Append("<li class=\"active\"><a href=\"#\">" + _pagetolist + "</a></li>" + _newline);
                    }
                    else
                    {
                        _html.Append(CreateCellNumber(_pagetolist, collection, pageName));
                    }
                }
            }
            else
            {
                if (PageNumber == 1 || PageNumber == 2 || PageNumber == 3)
                {
                    for (int _pagetolist = 1; _pagetolist <= 5; _pagetolist++)
                    {
                        if (_pagetolist == PageNumber)
                        {
                            _html.Append("<li class=\"active\"><a href=\"#\">" + _pagetolist + "</a></li>" + _newline);
                        }
                        else
                        {
                            _html.Append(CreateCellNumber(_pagetolist, collection, pageName));
                        }
                    }
                    _html.Append("<li class=\"active\"><a aria-controls=\"editable\" data-dt-idx=\"0\" tabindex=\"0\" href=\"#\">...</a></li>" + _newline);
                    for (int _pagetolist = Pages - 2; _pagetolist <= Pages; _pagetolist++)
                    {
                        _html.Append(CreateCellNumber(_pagetolist, collection, pageName));
                    }
                }
                else
                {
                    for (int _pagetolist = 1; _pagetolist <= 2; _pagetolist++)
                    {
                        _html.Append(CreateCellNumber(_pagetolist, collection, pageName));
                    }
                    if ((PageNumber - 5) >= 2)
                    {
                        _html.Append("<li class=\"active\"><a aria-controls=\"editable\" data-dt-idx=\"0\" tabindex=\"0\" href=\"#\">...</a></li>" + _newline);
                        if ((Pages - PageNumber) > 2)
                        {
                            for (int _pagetolist = PageNumber - 2; _pagetolist <= PageNumber + 2; _pagetolist++)
                            {
                                if (_pagetolist == PageNumber)
                                {
                                    _html.Append("<li class=\"active\"><a href=\"#\">" + _pagetolist + "</a></li>" + _newline);
                                }
                                else
                                {
                                    _html.Append(CreateCellNumber(_pagetolist, collection, pageName));
                                }
                            }
                            if ((Pages - (PageNumber + 3)) >= 2)
                            {
                                _html.Append("<li class=\"active\"><a aria-controls=\"editable\" data-dt-idx=\"0\" tabindex=\"0\" href=\"#\">...</a></li>" + _newline);
                                for (int _pagetolist = Pages - 1; _pagetolist <= Pages; _pagetolist++)
                                {
                                    _html.Append(CreateCellNumber(_pagetolist, collection, pageName));
                                }
                            }
                            else
                            {
                                for (int _pagetolist = PageNumber + 3; _pagetolist <= Pages; _pagetolist++)
                                {
                                    _html.Append(CreateCellNumber(_pagetolist, collection, pageName));
                                }
                            }
                        }
                        else
                        {
                            for (int _pagetolist = PageNumber - 2; _pagetolist <= Pages; _pagetolist++)
                            {
                                if (_pagetolist == PageNumber)
                                {
                                    _html.Append("<li class=\"active\"><a href=\"#\">" + _pagetolist + "</a></li>" + _newline);
                                }
                                else
                                {
                                    _html.Append(CreateCellNumber(_pagetolist, collection, pageName));
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int _pagetolist = 3; _pagetolist <= PageNumber + 2; _pagetolist++)
                        {
                            if (_pagetolist == PageNumber)
                            {
                                _html.Append("<li class=\"active\"><a href=\"#\">" + _pagetolist + "</a></li>" + _newline);
                            }
                            else
                            {
                                _html.Append(CreateCellNumber(_pagetolist, collection, pageName));
                            }
                        }
                        _html.Append("<li class=\"active\"><a aria-controls=\"editable\" data-dt-idx=\"0\" tabindex=\"0\" href=\"#\">...</a></li>" + _newline);
                        for (int _pagetolist = Pages - 1; _pagetolist <= Pages; _pagetolist++)
                        {
                            _html.Append(CreateCellNumber(_pagetolist, collection, pageName));
                        }
                    }
                }
            }
            if (PageNumber == Pages)
            {
                _html.Append("<li id=\"editable_next\" class=\"next disabled\"> <a href=\"#\" aria-controls=\"editable\" data-dt-idx=\"0\" tabindex=\"0\">></a></li>" + _newline);
            }
            else
            {
                _html.Append(String.Format("<li id=\"editable_next\" class=\"next\"><a aria-controls=\"editable\" data-dt-idx=\"0\" tabindex=\"0\" href=\"" + pageName + "?page={0}{1}\">></a></li>", PageNumber + 1, collection.Count > 0 ? collection.Keys[0] != null ? ("&" + collection.ToQueryString()) : String.Empty : String.Empty) + _newline);
            }
            _html.Append("</ul>");
            return _html.ToString();
        }

        private static string CreateCellNumber(int cellnum, NameValueCollection collection, string pageName)
        {
            string _newline = "\r\n";
            return String.Format("<li class=\"\"><a aria-controls=\"editable\" data-dt-idx=\"{0}\" tabindex=\"0\" href=\"" + pageName + "?page={1}{2}\">{3}</a></li>", cellnum, cellnum, collection.Count > 0 ? collection.Keys[0] != null ? ("&" + collection.ToQueryString()) : String.Empty : String.Empty, cellnum) + _newline;
        }

    }
}