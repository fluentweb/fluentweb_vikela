﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Common.Extensions;
using System.Web.UI.HtmlControls;
using Common.SEO.Entities;
using System.Web.UI.WebControls;


namespace Website.Helpers.Base
{
    public abstract class MasterPageBase : MasterPage
    {        
        public PageObject CurrentPage { get; set; }
        public SEOData SEOData { get; private set; }

        public abstract string DefaultPage { get; }
        public abstract Literal SEOLiteral { get; }

        private PageBase _page;

        public delegate void OnPageDataLoadedHandler(object sender, PageObject pageData);
        public event OnPageDataLoadedHandler OnPageDataLoaded;



        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            LoadPageDetail();
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // load SEO data
            LoadSEOData(_page);
        }

        private void LoadPageDetail()
        {
            _page = Page as PageBase;

            if (_page == null)
            {
                throw new Exception(String.Format("Failed to initialise. {0} does not inherit PageBase", this.Page.ToString()));
            }

            string url = _page.PageUrl;

            if (url == String.Empty)
                url = DefaultPage;

            CurrentPage = PageHelper.LoadPage(url);

            _page.CurrentPage = CurrentPage;


            // execute any custom event
            if (OnPageDataLoaded != null)
                OnPageDataLoaded(this, CurrentPage);
        }

        private void LoadSEOData(PageBase page)
        {
            SEOData pageSEOData = page.GetSEOData();
            SEOData defaultSEOData = DefaultSEOData();

            if (pageSEOData == null)
                pageSEOData = defaultSEOData;

            pageSEOData.Title = String.IsNullOrEmpty(pageSEOData.Title) ? defaultSEOData.Title : pageSEOData.Title;
            pageSEOData.Description = String.IsNullOrEmpty(pageSEOData.Description) ? defaultSEOData.Description : pageSEOData.Description;
            pageSEOData.Keywords = String.IsNullOrEmpty(pageSEOData.Keywords) ? defaultSEOData.Keywords : pageSEOData.Keywords;

            string returnValue = @"
    <title>{0}</title>
    <meta name='description' content='{1}' />
    <meta name='keywords' content='{2}' />";

            SEOLiteral.Text = String.Format(returnValue, pageSEOData.Title, pageSEOData.Description.TrimDownTo(150, String.Empty), pageSEOData.Keywords);
        }

        private SEOData DefaultSEOData()
        {
            return new SEOData
             {
                 Description = CurrentPage.Page.GetDataTableValue(0, "chrPageDescription", String.Empty),
                 Keywords = CurrentPage.Page.GetDataTableValue(0, "chrPageKeywords", String.Empty),
                 Title = CurrentPage.Page.GetDataTableValue(0, "chrPageTitle", String.Empty)
             };
        }
    }
}
