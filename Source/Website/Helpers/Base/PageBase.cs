﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Common.SEO.Entities;
using System.IO;
using Common.Session;

namespace Website.Helpers.Base
{
    public abstract class PageBase : System.Web.UI.Page
    {
        /// <summary>
        /// returns to current page URL or default page to load
        /// </summary>
        //public abstract string PageUrl { get; }
        public virtual string PageUrl { get { return Path.GetFileName(Request.PhysicalPath); } }
        public PageObject CurrentPage { get; set; }
        public virtual SEOData GetSEOData() { return null; }
    }
}
