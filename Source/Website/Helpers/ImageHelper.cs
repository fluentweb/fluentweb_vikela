﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace Website.Helpers
{
    public class ImageHelper
    {
        public static string LoadImage(object imageURL, object imageTitle, object imageAlt)
        {
            if (imageURL == null) return String.Empty;
            if (imageTitle == null) imageTitle = String.Empty;
            if (imageAlt == null) imageAlt = String.Empty;

            string url = imageURL.ToString();
            string title = imageTitle.ToString();
            string alt = imageAlt.ToString();

            if (url.Length == 0) return String.Empty;
            string toReturn = "<img  class=\"img-responsive\" src='{0}'{1}{2} />";

            // prepare url
            url = VirtualPathUtility.ToAbsolute("~/" + FluentContentSettings.ImageUploadFolder + "/" + url);
            // prepare title
            title = title.Trim().Length == 0 ? String.Empty : String.Format(" title='{0}'", title.Trim());
            // prepare alt
            alt = alt.Trim().Length == 0 ? String.Empty : String.Format(" alt='{0}'", alt.Trim());

            return String.Format(toReturn, url, title, alt);

        }

        public static string LoadImageBackground(object imageURL)
        {
            if (imageURL == null) return String.Empty;
            string url = imageURL.ToString();

            if (url.Length == 0) return String.Empty;
            url = VirtualPathUtility.ToAbsolute("~/" + FluentContentSettings.ImageUploadFolder + "/" + url);
            string toReturn = "style='background:url(\"{0}\") no-repeat;'";
            return String.Format(toReturn, url);
        }

        public static string LoadDynamicImageTag(string formattedURL, object imageURL)
        {
            if (imageURL == null) return String.Empty;

            string url = String.Format(formattedURL, imageURL.ToString());

            if (url.Length == 0) return String.Empty;
            string toReturn = "<img src='{0}' />";

            // prepare url
            url = VirtualPathUtility.ToAbsolute("~/" + url);

            return String.Format(toReturn, url);
        }

        public static string LoadDynamicImageTag(string formattedURL, object imageURL, string placeholderURL)
        {
            if (imageURL == null) return String.Format("<img src='{0}' />", VirtualPathUtility.ToAbsolute("~/" + placeholderURL));

            string url = String.Format(formattedURL, imageURL.ToString());

            if (url.Length == 0) return String.Empty;
            string toReturn = "<img src='{0}' />";

            // prepare url
            url = VirtualPathUtility.ToAbsolute("~/" + url);

            return String.Format(toReturn, url);
        }
    }
}
