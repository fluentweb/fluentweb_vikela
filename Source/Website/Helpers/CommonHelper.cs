﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Website.Helpers
{
    public class CommonHelper
    {
        public static string ReplaceDomainName(string input)
        {
            return input.Replace("##DOMAIN##/", FluentContent.Settings.WebsiteRoot);
        }
    }
}
