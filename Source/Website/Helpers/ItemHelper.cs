﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using FLContentSiteAssist.BusinessLogic;
using System.Data;
using Common.Extensions;
using Common.SEO.Entities;
using FLContentSiteAssist.Common;
using FluentContent.DAC;

namespace Website.Helpers
{
    public class DownloadAttachment
    {
        public string Filename { get; set; }
        public string IconUrl { get; set; }
    }

    public class ItemHelper
    {
        public static void LoadItem(string itemType, ListView lv)
        {
            ProcessGetItem getItem = new ProcessGetItem();
            getItem.Item = new FLContentSiteAssist.Common.Item { ItemType = itemType, ViewDisabled = false };
            getItem.Invoke();
            lv.DataSource = ReplaceDomainNames(getItem.ResultSet);
            lv.DataBind();
        }

        public static DataTable LoadItem(string itemType)
        {
            ProcessGetItem getItem = new ProcessGetItem();
            getItem.Item = new FLContentSiteAssist.Common.Item { ItemType = itemType, ViewDisabled = false };
            getItem.Invoke();
            if (getItem.ResultSet.HasRows(0))
                return ReplaceDomainNames(getItem.ResultSet).Tables[0];            
            return null;
        }

        public static DataTable LoadItemDisplay(string itemDisplay)
        {
            ProcessGetItemDisplay getDisplay = new ProcessGetItemDisplay();
            getDisplay.Item = new FLContentSiteAssist.Common.Item { Display = itemDisplay };
            getDisplay.Invoke();

            if (getDisplay.ResultSet.HasRows(0))
                return ReplaceDomainNames(getDisplay.ResultSet).Tables[0];
            return null;
        }

        public static DataTable LoadItemByUrl(string itemType, string url)
        {
            ProcessGetItemByURL getItemByUrl = new ProcessGetItemByURL();
            getItemByUrl.Item = new FLContentSiteAssist.Common.Item { ItemType = itemType, ItemURL = url };
            getItemByUrl.Invoke();

            if (getItemByUrl.ResultSet.HasRows(0))
                return ReplaceDomainNames(getItemByUrl.ResultSet).Tables[0];
            return null;
        }

        public static DataTable LoadItemDisplay(string itemDisplay, int parentID)
        {
            ProcessGetItemDisplay getItemDisplay = new ProcessGetItemDisplay();
            getItemDisplay.Item = new FLContentSiteAssist.Common.Item { Display = itemDisplay, ParentItemID = parentID };
            getItemDisplay.Invoke();

            if (getItemDisplay.ResultSet.HasRows(0))
                return ReplaceDomainNames(getItemDisplay.ResultSet).Tables[0];
            return null;
        }

        public static DataTable LoadItemDisplay(string itemDisplay, string parentType, string parentUrl, out DataTable parentItem)
        {
            ProcessGetItemByURL getItemByUrl = new ProcessGetItemByURL();
            getItemByUrl.Item = new FLContentSiteAssist.Common.Item { ItemType = parentType, ItemURL = parentUrl };
            getItemByUrl.Invoke();

            parentItem = null;

            if (!getItemByUrl.ResultSet.HasRows(0)) return null;
            parentItem = getItemByUrl.ResultSet.Tables[0];

            int parentID = getItemByUrl.ResultSet.Tables[0].GetDataTableValue<int>(0, "intItemID", 0);

            ProcessGetItemDisplay getItemDisplay = new ProcessGetItemDisplay();
            getItemDisplay.Item = new FLContentSiteAssist.Common.Item { Display = itemDisplay, ParentItemID = parentID };
            getItemDisplay.Invoke();

            if (getItemDisplay.ResultSet.HasRows(0))
                return ReplaceDomainNames(getItemDisplay.ResultSet).Tables[0];
            return null;
        }

        public static DataTable LoadItemDisplaySearch(string Display, int ParentItemID, int CurrentPage, string TextSearch, string Parameter1)
        {
            DataTable dt = DAC_Item.GetItemDisplaySearch(Display, ParentItemID, CurrentPage, TextSearch, Parameter1);

            if (dt.Rows.Count == 0) return null;
            return dt;
        }
        
        public static SEOData LoadItemSEOData(DataTable item)
        {
            return new SEOData
            {
                Title = item.GetDataTableValue(0, "chrItemTitle", String.Empty),
                Description = item.GetDataTableValue(0, "chrText5", String.Empty)
            };
        }

        private  static DataSet ReplaceDomainNames(DataSet ds)
        {
            DataTable dt = ds.Tables[0];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i].BeginEdit();
                dt.Rows[i]["chrText11"] = CommonHelper.ReplaceDomainName(dt.Rows[i]["chrText11"].ToString());
                dt.Rows[i]["chrText12"] = CommonHelper.ReplaceDomainName(dt.Rows[i]["chrText12"].ToString());
                dt.Rows[i].EndEdit();
            }
            ds.AcceptChanges();
            return ds;

        }

        public static DataTable LoadItemRelated(string RelationshipType, int ParentItemID)
        {
            ProcessGetRelatedItem itemsChild = new ProcessGetRelatedItem();
            itemsChild.RelatedItem = new RelatedItem { RelationshipType = RelationshipType, ItemID = ParentItemID };
            itemsChild.Invoke();
            if (itemsChild.ResultSet.HasRows(0))
                return ReplaceDomainNames(itemsChild.ResultSet).Tables[0];
            return null;

        }
    }
}
