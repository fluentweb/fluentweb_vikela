﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Website.Helpers;
using Website.Helpers.Base;
using Common.Extensions;
using System.Data;

namespace Website
{
    public partial class Information : PageBase
    {
        public int ParentID { get { return Request.GetQueryString<int>("sectionId", 0); } }

        public string ParentURL { get { return Request.GetQueryString<string>("sectionName", string.Empty); } }

        public override string PageUrl
        {
            get
            {
                return "Information";
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadDownloadSections();
                LoadDownloadSection();
                LoadDownloadFiles();
            }
        }

        private void LoadDownloadSections()
        {
            lvDownloadSection.DataSource = ItemHelper.LoadItemDisplay("Download Section");
            lvDownloadSection.DataBind();
        }

        private void LoadDownloadSection()
        {
            if (!string.IsNullOrEmpty(ParentURL))
            {
                DataTable dtDownloadSection = ItemHelper.LoadItemByUrl("Download Section", ParentURL);
                if(dtDownloadSection.Rows.Count > 0)
                {
                    litSectionName.Text = dtDownloadSection.Rows[0]["chrItemTitle"].ToString();
                }
            }
        }


        private void LoadDownloadFiles()
        {
            if (ParentID > 0)
            {
                lvFiles.DataSource = ItemHelper.LoadItemRelated("Downloads", ParentID);
                lvFiles.DataBind();
            }
        }
    }
}