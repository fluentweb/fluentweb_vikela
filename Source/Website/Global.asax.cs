﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using DataAccess;
using System.Configuration;
using Website.Helpers;
using Common.Exceptions.Entities.Base;
using System.Reflection;

namespace Website
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            // setup DataAccess
            DbFactory.ConnectionSettings = new DataAccess.Setup.DbConnectionSettings(
                ConfigurationManager.ConnectionStrings["SQLCONN"].ConnectionString,
                ConfigurationManager.ConnectionStrings["SQLCONN"].ProviderName);

            // setup Web.Common
            Common.Setup.CommonSettings.ErrorEmailApplicationName = ConfigurationManager.AppSettings["ErrorEmailApplicationName"];
            Common.Setup.CommonSettings.ErrorEmailDisplayName = ConfigurationManager.AppSettings["ErrorEmailDisplayName"];
            Common.Setup.CommonSettings.ErrorEmailRecipientAddress = ConfigurationManager.AppSettings["ErrorEmailRecipientAddress"];
            Common.Setup.CommonSettings.ErrorEmailSenderAddress = ConfigurationManager.AppSettings["ErrorEmailSenderAddress"];
            Common.Setup.CommonSettings.ErrorEmailPassword = ConfigurationManager.AppSettings["ErrorEmailPassword"];
            
            // setup Fluent Content             
            FLContentSiteAssist.DataAccess.Client.ClientID = Convert.ToInt32(ConfigurationManager.AppSettings["FluentContentClientID"]);

            // setup Fluent Content new
            FluentContent.Settings.ClientID = FLContentSiteAssist.DataAccess.Client.ClientID;
            FluentContent.Settings.WebsiteRoot = ConfigurationManager.AppSettings["WebsiteRoot"];

            FluentContentSettings.DocumentUploadFolder = ConfigurationManager.AppSettings["DocumentUploadFolder"];
            FluentContentSettings.ImageUploadFolder = ConfigurationManager.AppSettings["ImageUploadFolder"];
  
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            //Log Error
            if (Request.Url.IsLoopback) return;

            if (Server.GetLastError().InnerException != null && Server.GetLastError().InnerException.GetType() == typeof(BaseException))
                return;

            HttpException exception = Server.GetLastError() as HttpException;
            if (exception.GetHttpCode() == 404)
                return;
            if (Server.GetLastError() is HttpUnhandledException && Server.GetLastError().InnerException != null)
                BaseException.SendErrorReport(String.Format("An error occured in {0} at method {1}", this.ToString(), MethodInfo.GetCurrentMethod().Name), Server.GetLastError().InnerException);
            else
                BaseException.SendErrorReport(String.Format("An error occured in {0} at method {1}", this.ToString(), MethodInfo.GetCurrentMethod().Name), Server.GetLastError());
        }
    }
}