﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Website.Helpers;
using Website.Helpers.Base;
using Common.Extensions;
using System.Collections.Specialized;
using System.Data;
using FluentContent.Entities;

namespace Website
{
    public partial class News : PageBase
    {
        public int SectionID { get { return Request.GetQueryString<int>("section", 0); } }
        public string Archive { get { return Request.GetQueryString<string>("archive", string.Empty); } }
        public string TextSearch { get { return Request.GetQueryString<string>("text", string.Empty); } }
        public int NewsPageNumber { get { return Request.GetQueryString<int>("page", 1); } }

        public override string PageUrl
        {
            get
            {
                return "News";
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadNewsSections();
                LoadNewsArchive();
                LoadNews();

            }

        }
        private void LoadNewsSections()
        {
            lvNewsSection.DataSource = ItemHelper.LoadItemDisplay("News Section");
            lvNewsSection.DataBind();
        }
        private void LoadNewsArchive()
        {
            lvArchive.DataSource = ItemHelper.LoadItemDisplay("News Archive");
            lvArchive.DataBind();
        }

        private void LoadNews()
        {
            DataTable dt = ItemHelper.LoadItemDisplaySearch("News Articles", SectionID, NewsPageNumber, TextSearch, Archive);
            lvNews.DataSource = dt;
            lvNews.DataBind();

            int Pages = 0;
            DisplaySetting setting = FluentContent.DAC.DAC_DisplaySetting.GetByDisplayName("News Articles");
            if (setting != null)
                if (setting.PageSize >= 0)
                    if (dt != null)
                        if (dt.Rows.Count > 0)
                            Pages = (Convert.ToInt32(dt.Rows[0]["TotalRows"]) % setting.PageSize) == 0 ? (Convert.ToInt32(dt.Rows[0]["TotalRows"]) / setting.PageSize) : 
                                ((Convert.ToInt32(dt.Rows[0]["TotalRows"]) / setting.PageSize) + 1);
            litPaginationDown.Text = Utilities.GetPageLink(Request, NewsPageNumber, Pages);

        }

        protected void btnArchive_Click(object sender, EventArgs e)
        {
            string arg = ((LinkButton)sender).CommandArgument;
            NameValueCollection collection = HttpUtility.ParseQueryString(Request.Url.Query);
            collection.Remove("archive");
            Response.Redirect(FluentContent.Settings.WebsiteRoot + "news?archive=" + arg + "&" + collection);
        }

        protected void btnSection_Click(object sender, EventArgs e)
        {
            string arg = ((LinkButton)sender).CommandArgument;
            NameValueCollection collection = HttpUtility.ParseQueryString(Request.Url.Query);
            collection.Remove("section");
            Response.Redirect(FluentContent.Settings.WebsiteRoot + "news?section=" + arg + "&" + collection);
        }

        protected void btnSearchText_Click(object sender, EventArgs e)
        {
            NameValueCollection collection = HttpUtility.ParseQueryString(Request.Url.Query);
            collection.Remove("text");
            Response.Redirect(FluentContent.Settings.WebsiteRoot + "news?text=" + txtSearch.Text.Trim() + "&" + collection);
        }
    }
}