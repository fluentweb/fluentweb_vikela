﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Website.Helpers;
using Website.Helpers.Base;
using Common.Extensions;

namespace Website
{
    public partial class NewsDetail : PageBase
    {
        public string newsUrl { get { return Request.GetQueryString<string>("newsUrl", string.Empty); } }
        public string ParentTitle { get { return Request.GetQueryString<string>("parenttitle", string.Empty); } }
        public override string PageUrl
        {
            get
            {
                return "NewsDetail";
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //OIS.MHS 09-06-2016
                if (Session["previousURL"] == null)
                    if (Request.UrlReferrer != null)
                        Session["previousURL"] = Request.UrlReferrer.ToString();
                LoadNewsDetail();
            }
        }

        private void LoadNewsDetail()
        {
            if (!string.IsNullOrEmpty(newsUrl))
            {
                lvNewsDetail.DataSource = ItemHelper.LoadItemByUrl("News Articles", newsUrl);
                lvNewsDetail.DataBind();
            }
            else
            {
                Response.Redirect(Request.UrlReferrer.ToString());
            }
        }

        protected void lnBack_Click(object sender, EventArgs e)
        {
            string previousURL = Session["previousURL"].ToString();
            Response.Redirect(previousURL);
        }
    }
}