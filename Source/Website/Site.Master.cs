﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Website.Helpers.Base;
using Website.Helpers;
using FluentContent.Entities;
using Common.Session;
using System.Data;
using Common.Extensions;

namespace Website
{
    public partial class Site : MasterPageBase
    {
        public Entity EntitySession { get { return SessionObjects.GetSession<Entity>(SessionKeys.ENTITY_SESSION); } }

        public int FooterItemCount { get; set; }
        public int HeaderItemCount { get; set; }

        public string RedirectionURL { get { return Request.GetQueryString<string>("redirect", null); } }

        //public MultiView PreferencesView { get { return this.mvPreferences; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            form1.Action = Request.RawUrl;

            if(!IsPostBack)
            {
                LoadGetStartedMenu();
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            // overrised standard asp.net validation
            string scriptUrl = ResolveUrl("~/js/validators.js");
            string validatorOverride = String.Format("<script src='{0}' type='text/javascript'></script>", scriptUrl);
            Page.ClientScript.RegisterStartupScript(this.GetType(), "ValidatorOverrideScripts", validatorOverride, false);
            base.Render(writer);
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            LoadMasterContent();
        }

        public override string DefaultPage
        {
            get { return "Index.aspx"; }
        }

        public override Literal SEOLiteral
        {
            get { return litSEOData; }
        }

        //// custom logic

        public DataSet _customPageMenu1 { get; set; }
        public DataSet _thirdColumnMenu { get; set; }

        private void LoadMasterContent()
        {
            // global page
            CurrentPage = PageHelper.LoadPage("Master");
        }
        private void LoadGetStartedMenu()
        {
            lvGetStarted.DataSource = MenuHelper.LoadMenu("Get Started Menu");
            lvGetStarted.DataBind();
        }
    }
}
