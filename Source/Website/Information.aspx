﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Information.aspx.cs" Inherits="Website.Information" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="placeholderBody" runat="server">
    <div class="container-fluid main_banner inner information">
        <div class="row relative">
            <div class="banner">
                <div class="back-panel">
                    <img src="/<%=FluentContentSettings.ImageUploadFolder + CurrentPage.GetImageByKey("Banner")["chrPageImageURL"] %>" class="img-responsive" alt="<%=CurrentPage.GetImageByKey("Banner")["chrPageImageALT"] %>" />
                </div>
            </div>
            <div class="front-panel">
                <%=CurrentPage.GetTextByKey("Title")%>
            </div>
        </div>
    </div>
    <!-- main banner end -->
    <section class="information-block">
        <div class="container text-center">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-offset-2">
                    <%=CurrentPage.GetTextByKey("Block1")%>
                </div>
            </div>
            <br>
            <br>
            <a href="contact.aspx" class="btn btn-lg btn-success">CONTACT VEKILA</a>
        </div>
    </section>
    <section class="highlight-information-block">
        <div class="container text-center">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-offset-2">
                    <%=CurrentPage.GetTextByKey("Block2")%>
                </div>
                <div class="clearfix"></div>
                <div class="row download_dropdown navbar navbar-default">
                    <!-- custom dropdown  -->
                    <div class="cdropdown2 white noshadow" data-cdropdown>
                        <div class="block dropdown">
                            <h2 class="Get_started"><a href="javascript:;" class="dropdown-toggle" data-target="dropdown">Download Section
						<i class="fa fa-angle-down pull-right"></i></a></h2>
                            <input type="hidden" id="selectbox" name="" value="">
                        </div>
                        <ul class="dropdown-menu">
                            <asp:ListView ID="lvDownloadSection" runat="server">
                                <LayoutTemplate>
                                    <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <li><a href="/information/<%#Eval("chrItemURL") %>/<%#Eval("intItemID") %>"><%#Eval("chrItemTitle") %></a></li>
                                </ItemTemplate>
                            </asp:ListView>
                        </ul>
                    </div>
                    <!-- //custom dropdown -->
                </div>
                <h3 class="subtitle2">
                    <asp:Literal ID="litSectionName" runat="server"></asp:Literal></h3>
                <div class="row">
                    <asp:ListView ID="lvFiles" runat="server">
                        <LayoutTemplate>
                            <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                        </LayoutTemplate>
                        <ItemTemplate>
                           <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center">
                            <div class="nk_list">
                                <i class="icon-pdf"></i>
                                <h4><a target="_blank" href="<%#FluentContent.Settings.WebsiteRoot + FluentContentSettings.DocumentUploadFolder + Eval("chrItemFileURL")  %>"><%#Eval("chrItemTitle") %><span><%#Eval("chrText1") %></span></a></h4>
                            </div>
                            </div>
                        </ItemTemplate>
                        <EmptyItemTemplate>
                            No available file for download
                        </EmptyItemTemplate>
                    </asp:ListView>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderBottom" runat="server">
</asp:Content>
