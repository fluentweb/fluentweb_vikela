﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="News.aspx.cs" Inherits="Website.News" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="placeholderBody" runat="server">
    <% Session["previousURL"] = null; %>
    <div class="container-fluid main_banner inner about-us">
        <div class="row relative">
            <div class="banner">
                <div class="back-panel">

                    <img src="/<%=FluentContentSettings.ImageUploadFolder + CurrentPage.GetImageByKey("Banner")["chrPageImageURL"] %>" class="img-responsive" alt="<%=CurrentPage.GetImageByKey("Banner")["chrPageImageALT"] %>" />

                </div>
            </div>
            <div class="front-panel">
                <%=CurrentPage.GetTextByKey("Title")%>
            </div>
        </div>
    </div>
    <!-- main banner end -->

    <section class="highlight-news-block second-block filter">
        <div class="container text-center">
            <div class="clearfix"></div>
            <div class="row download_dropdown">
                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 col-lg-offset-2">
                    <div class="row navbar navbar-default">
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <!-- custom dropdown  -->
                            <div class="cdropdown2 white noshadow dropdown">
                                <div class="block dropdown innerDropdown dropdown-toggle" data-toggle="dropdown">
                                    <h2 class="Get_started"><a href="javascript:;" class="dropdown-toggle" data-target="dropdown">Category <i class="fa fa-angle-down pull-right"></i></a></h2>
                                    <input type="hidden" id="selectbox" name="" value="">
                                </div>
                                <ul class="dropdown-menu">
                                    <asp:ListView ID="lvNewsSection" runat="server">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <li>
                                                <asp:LinkButton ID="btnSection" OnClick="btnSection_Click" CommandArgument='<%#Eval("intItemID") %>' Text='<%#Eval("chrItemTitle") %>' runat="server"></asp:LinkButton>
                                            </li>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </ul>
                            </div>

                            <!-- //custom dropdown -->
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="search_fun">
                                <asp:LinkButton ID="btnSearchText" OnClick="btnSearchText_Click" runat="server"><i class="icon-search"></i></asp:LinkButton>
                                <asp:TextBox type="text" name="search" placeholder="Search" value="Search" CssClass="custom_search" ID="txtSearch" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <!-- custom dropdown  -->
                            <div class="cdropdown2 white noshadow desktop-set dropdown">
                                <div class="block dropdown innerDropdown dropdown-toggle" data-toggle="dropdown">
                                    <h2 class="Get_started"><a href="javascript:;">Archive <i class="fa fa-angle-down pull-right"></i></a></h2>
                                    <input type="hidden" id="selectbox" name="" value="">
                                </div>
                                <ul class="dropdown-menu">
                                    <asp:ListView ID="lvArchive" runat="server">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <li>
                                                <asp:LinkButton ID="btnArchive" OnClick="btnArchive_Click" CommandArgument='<%#Eval("chrItemTitle") %>' Text='<%#Eval("chrItemTitle") %>' runat="server"></asp:LinkButton>
                                            </li>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </ul>
                            </div>
                            <!-- //custom dropdown -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </section>
    <asp:ListView ID="lvNews" runat="server">
        <LayoutTemplate>
            <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
        </LayoutTemplate>
        <ItemTemplate>
            <section class="<%#Container.DisplayIndex % 2 == 0 ? "highlight-news-block" : "information-block" %> second-block">
                <div class="container text-center">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-lg-offset-3">
                            <div class="nk-top">
                                <div class="cat_no mylist"><%#Eval("chrItemTitleParent") %></div>
                                <div class="pot_date mylist"><%#String.Format("{0:d MMMM yyyy}", Convert.ToDateTime(Eval("datStart"))) %></div>
                            </div>
                            <h2>
                                <a href="/news-detail/<%#Eval("chrItemURL") %>?parenttitle=<%#Eval("chrItemTitleParent") %>" class="newsclick"><%#Eval("chrItemTitle") %></a>
                            </h2>
                            <br>
                            <p>
                                <%#Eval("chrText1") %>
                            </p>
                            <br>
                            <div class="nk-bottom">
                                <div class="fb mylist"><a href="<%#ClientSettingsHelper.Get("facebook_url").Value%>"><i class="fa fa-facebook"></i></a></div>
                                <div class="twt mylist"><a href="<%#ClientSettingsHelper.Get("twitter_url").Value%>"><i class="fa fa-twitter"></i></a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </ItemTemplate>
        <EmptyDataTemplate>
            <div class="container text-center">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-lg-offset-3">
                        No news yet...
                    </div>
                </div>
            </div>
        </EmptyDataTemplate>
    </asp:ListView>
    <section class="highlight-news-block second-block nopadding-top">
        <div class="container text-center">
            <div class="row download_dropdown">
                <asp:Literal ID="litPaginationDown" runat="server"></asp:Literal>
                <%--   <ul class="pagination">
                    <li class="disabled"><a href="#">&laquo;</a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                </ul>--%>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderBottom" runat="server">
</asp:Content>
