﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Website.Helpers;
using Website.Helpers.Base;

namespace Website
{
    public partial class About : PageBase
    {
        public override string PageUrl
        {
            get
            {
                return "About";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadFAQ();
                loadimage();
            }
        }

        private void LoadFAQ()
        {
            lvGetStarted.DataSource = ItemHelper.LoadItemDisplay("FAQ");
            lvGetStarted.DataBind();
        }

        private void loadimage()
        {
            lvAboutBlock4.DataSource = ItemHelper.LoadItemDisplay("About Block 4");
            lvAboutBlock4.DataBind();
        }
    }
}